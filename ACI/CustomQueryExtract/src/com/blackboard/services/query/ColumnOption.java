/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.query;

import java.io.Serializable;

/**
 *
 * @author crusnak
 */
public class ColumnOption implements Serializable
{
    private static final long serialVersionUID = 65396023934423L;
    private static final String EXTRACT_INDICATOR =         "*";

    private String column;
    private boolean selected = false;
    
    public ColumnOption(String col)
    {
        column = col;
    }

    public ColumnOption(String col, boolean sel)
    {
        column = col;
        selected = sel;
    }

    public String getColumnName()
    {
        return column;
    }

    public boolean isSelected()
    {
        return selected;
    }

    public String getColumnNameWithIndicator()
    {
        String col = getColumnName();
        if ( isSelected() )
        {
            col = EXTRACT_INDICATOR + " " + col;
        }

        return col;
    }

    public void toggleSelection()
    {
        selected = !selected;
    }

    public String toString()
    {
        return getColumnName() + (selected ? "(SELECT)":"");
    }
}
