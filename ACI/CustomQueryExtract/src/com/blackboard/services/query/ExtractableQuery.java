/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.query;

import com.blackboard.services.security.object.NamedKey;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author crusnak
 */
public class ExtractableQuery extends Query
{
    private static final long serialVersionUID = 3476938744312L;

    protected boolean sqlStatement = true;
    protected boolean displayHeader = false;
    protected boolean displayFooter = false;
    protected char delimitter = ',';
    protected LinkedHashSet<ColumnOption> extractColumns = new LinkedHashSet<ColumnOption>();

    public ExtractableQuery()
    {
        super();
    }

    public ExtractableQuery(NamedKey connkey, String n, String s, boolean h, boolean f, String d)
    {
        super(connkey, n, s);
        displayHeader = h;
        displayFooter = f;
        setDelimitter(d);
    }

    public ExtractableQuery(NamedKey connkey, String n, String s, boolean h, boolean f, String d, String columns)
    {
        super(connkey, n, s);
        displayHeader = h;
        displayFooter = f;
        setDelimitter(d);
        setExtractColumns(columns);
    }

    public void addColumnToExtract(String label)
    {
        if ( label != null )
        {
            extractColumns.add(new ColumnOption(label.trim(), true));
        }
    }

    public void setExtractColumns(String columns)
    {
        if ( columns != null )
        {
            extractColumns.clear();
            String[] cols = columns.split(",");
            for ( int i=0; i<cols.length; i++ )
            {
                extractColumns.add(new ColumnOption(cols[i].trim(), true));
            }
        }
    }

    public ColumnOption[] getExtractColumnOptions()
    {
        return extractColumns.toArray(new ColumnOption[0]);
    }

    public List<String> getExtractColumns()
    {
        ArrayList<String> cols = new ArrayList();
        Iterator<ColumnOption> it = extractColumns.iterator();
        while ( it.hasNext() )
        {
            ColumnOption option = it.next();
            if ( option.isSelected() )
            {
                cols.add(option.getColumnName());
            }
        }
        return cols;
    }

    public boolean isDisplayFooter()
    {
        return displayFooter;
    }

    public void setDisplayFooter(boolean displayFooter)
    {
        this.displayFooter = displayFooter;
    }

    public boolean isDisplayHeader()
    {
        return displayHeader;
    }

    public void setDisplayHeader(boolean displayHeader)
    {
        this.displayHeader = displayHeader;
    }

    public char getDelimitter()
    {
        return delimitter;
    }

    public void setDelimitter(String delimitter)
    {
        if ( delimitter != null && delimitter.trim().length() > 0 )
        {
            this.delimitter = String.valueOf(delimitter).charAt(0);
        }
    }

    public boolean isSqlStatement()
    {
        return sqlStatement;
    }

    public void setSqlStatement(boolean sqlStatement)
    {
        this.sqlStatement = sqlStatement;
    }

    public void addAllColumnsToExtractFromStatement()
    {
        extractColumns.clear();
        if ( statement != null )
        {
            String[] labels = getColumnLabelsFromStatement();
            for ( int i=0; i<labels.length; i++ )
            {
                addColumnToExtract(labels[i]);
            }
        }
    }

    public String[] getColumnLabelsFromStatement()
    {
        ArrayList<String> labels = new ArrayList<String>();
        Pattern labelExtract = Pattern.compile("[Aa][Ss]\\s+(\\w+)\\s*,?");

        if ( statement != null )
        {
            Matcher m = labelExtract.matcher(statement);
            while ( m.find() )
            {
                labels.add(m.group(1));
            }

        }

        return labels.toArray(new String[0]);
    }

    public String toString()
    {
        StringBuffer out = new StringBuffer();

        out.append("{" + connection);
        out.append(",'" + delimitter);
        out.append("'," + displayHeader);
        out.append("," + displayFooter);

        out.append("," + extractColumns);
        out.append(",[" + (statement != null ? statement.replaceAll("\\s+", " ") : null) + "]}");

        return out.toString();
    }

}
