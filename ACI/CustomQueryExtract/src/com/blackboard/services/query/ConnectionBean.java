/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.query;

import com.blackboard.services.security.object.DBConnection;
import com.blackboard.services.security.object.NamedKey;

/**
 *
 * @author crusnak
 */
public class ConnectionBean
{
    private NamedKey key = null;
    private DBConnection connectionObject = null;

    public ConnectionBean() {}

    public ConnectionBean(NamedKey k, DBConnection o)
    {
        key = k;
        connectionObject = o;
    }

    public NamedKey getKey()
    {
        return key;
    }

    public void setKey(NamedKey connectionName)
    {
        this.key = connectionName;
    }

    public DBConnection getObject()
    {
        return connectionObject;
    }

    public void setObject(DBConnection connectionObject)
    {
        this.connectionObject = connectionObject;
    }

    public String toString()
    {
        return key + ": " + connectionObject;
    }
}
