/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.query;

import com.blackboard.services.utils.BeanScript;
import com.blackboard.services.security.ConnectionManager;
import com.blackboard.services.security.SecurityManager;
import com.blackboard.services.security.exception.AccessException;
import com.blackboard.services.security.exception.BbTSSecurityException;
import com.blackboard.services.security.object.NamedKey;
import com.blackboard.services.security.object.RegisterableApplication;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

import org.apache.log4j.Logger;

/**
 *
 * @author crusnak
 */
public class QueryExtract
{
    protected static Logger logger = null;

    protected ExtractableQuery query = null;
    protected File outputFile = null;
    protected String appName = "";
    protected String queryName = "";

    protected SimpleDateFormat fileFormat = new SimpleDateFormat("yyyyMMddHHmmss");

    protected QueryExtract(String an)
    {
        appName = an;
    }
    
    public QueryExtract(String an, String qn, String output)
        throws BbTSSecurityException
    {
        appName = an;
        queryName = qn;
        logger = Logger.getLogger(this.getClass());
        query = loadExtractableQuery(appName, queryName);
        outputFile = new File(output);

        logger.warn("--------------------------------------------------------------");
        logger.info("CustomQuery: " + new NamedKey(appName, queryName));
        logger.info("Query: " + query);
        logger.info("Output destination: " + outputFile);
    }

    public List<Map<String,String>> startExtract()
        throws BbTSSecurityException, SQLException, IOException
    {
        RegisterableApplication app = new RegisterableApplication(appName);
        ConnectionManager conman = ConnectionManager.getInstance(app);
        ArrayList<String> columnHeaders = new ArrayList();
        List<Map<String,String>> rowdata = new ArrayList();
        Connection conn = null;
        PreparedStatement ps = null;

        try
        {
            conn = conman.instantiateDBConnection(query.getConnectionKey());
            ps = conn.prepareStatement(query.getStatement());
            ResultSet rs = ps.executeQuery();
            rowdata = processResultSet(rs);

            logger.info("Extracted " + rowdata.size() + " rows of data");
            logger.info("Outputing data to: " + outputFile.getCanonicalPath());
            logger.info("Extracting columns: " + query.getExtractColumns());
            writeExtract(outputFile, query, rowdata);
        }
        finally
        {
            if ( ps != null ) ps.close();
            if ( conn != null ) conn.close();
        }

        return rowdata;
    }

    protected File addDateToFile(File file)
    {
        String path = file.getPath();
        int extidx = path.lastIndexOf('.');
        String extension = "";
        if ( extidx >=0 )
        {
            extension = path.substring(extidx);
            path = path.substring(0, extidx);
        }

        path += "_" + fileFormat.format(new Date()) + extension;
        return new File(path);
    }

    protected void writeExtract(File file, ExtractableQuery query, List<Map<String,String>> rowdata, boolean addDate)
        throws IOException
    {
        if ( addDate )
        {
            file = addDateToFile(file);
        }
        
        FileWriter writer = new FileWriter(file);

        try
        {
            List<String> extractColumns = query.getExtractColumns();
            logger.info("Extracted " + rowdata.size() + " rows of data");
            logger.info("Outputing to: " + file.getCanonicalPath());
            logger.info("Extracting columns: " + extractColumns);

            // Write header if requested
            Iterator<String> it = extractColumns.iterator();
            if ( query.isDisplayHeader() )
            {
                while ( it.hasNext() )
                {
                    String name = it.next();
                    writer.write(name);
                    if ( it.hasNext() )
                    {
                        writer.write(query.getDelimitter());
                    }
                }
                writer.append('\n');
            }

            // Write each row data
            Iterator<Map<String,String>> it2 = rowdata.iterator();
            while ( it2.hasNext() )
            {
                Map row = it2.next();
                it = extractColumns.iterator();
                while ( it.hasNext() )
                {
                    String name = it.next();
                    Object val = row.get(name);
                    if ( val == null ) val = "";
                    writer.write(String.valueOf(val));
                    if ( it.hasNext() )
                    {
                        writer.write(query.getDelimitter());
                    }
                }
                writer.append('\n');
            }

            // TODO: Handle footer
            if ( query.isDisplayFooter() )
            {
            }
        }
        finally
        {
            writer.close();
        }
    }

    protected void writeExtract(File file, ExtractableQuery query, List<Map<String,String>> rowdata)
        throws IOException
    {
        writeExtract(file, query, rowdata, false);
    }

    protected Map<String,String> processResultSetRow(ResultSet rs)
        throws SQLException
    {
        // Ensure cursor is on the correct row before calling
        HashMap<String,String> data = new HashMap();
        ArrayList<String> columnHeaders = new ArrayList();
        ResultSetMetaData meta = rs.getMetaData();
        for ( int i=1; i<=meta.getColumnCount(); i++ )
        {
            columnHeaders.add(meta.getColumnLabel(i));
        }

        Iterator<String> it = columnHeaders.iterator();
        while ( it.hasNext() )
        {
            String name = it.next();
            data.put(name, rs.getString(name));
        }

        return data;
    }

    protected List<Map<String,String>> processResultSet(ResultSet rs)
        throws SQLException
    {
        ArrayList<Map<String,String>> rowdata = new ArrayList();
        while ( rs.next() )
        {
            rowdata.add(processResultSetRow(rs));
        }

        return rowdata;
    }

    protected Query loadQuery(String appName, String queryName)
        throws BbTSSecurityException
    {
        Query q = null;
        RegisterableApplication app = new RegisterableApplication(appName);
        QueryManager qman = QueryManager.getInstance(app);
        NamedKey nk = new NamedKey(appName, queryName);
        q = qman.getRegisteredQuery(nk);
        if ( q == null )
        {
            throw new AccessException("No registered Query object found for " + nk);
        }

        return q;
    }

    protected ExtractableQuery loadExtractableQuery(String appName, String queryName)
        throws BbTSSecurityException
    {
        ExtractableQuery eq = null;
        RegisterableApplication app = new RegisterableApplication(appName);
        QueryManager qman = QueryManager.getInstance(app);
        NamedKey nk = new NamedKey(appName, queryName);
        Query q = qman.getRegisteredQuery(nk);
        if ( q == null || !(q instanceof ExtractableQuery) )
        {
            throw new AccessException("No registered ExtractableQuery object found for " + nk);
        }
        else
        {
            eq = (ExtractableQuery)q;
        }

        return eq;
    }

    protected List<Map<String,String>> addQueryResultsToData(Query query, List<Map<String,String>> rowdata)
        throws SQLException, BbTSSecurityException
    {
        RegisterableApplication app = new RegisterableApplication(appName);
        ConnectionManager conman = ConnectionManager.getInstance(app);
        Connection conn = null;
        PreparedStatement ps = null;

        try
        {
            conn = conman.instantiateDBConnection(query.getConnectionKey());
            BeanScript[] params = extractParameters(query);
            ps = conn.prepareStatement(query.getStatement());
            Iterator<Map<String,String>> rows = rowdata.iterator();
            for ( int i=1; rows.hasNext(); i++ )
            {
                Map<String,String> row = rows.next();
                logger.debug("Processing row #" + i + ": " + row);
                String val = "";
                for ( int j=0; j<params.length; j++ )
                {
                    String accessor = params[j].getAccessorText();
                    if ( row.containsKey(accessor) )
                    {
                        val = String.valueOf(row.get(accessor));
                    }
                    ps.setString(j+1, val);
                    ResultSet rs = ps.executeQuery();

                    // Add all the values if a row was returned
                    if ( rs.next() )
                    {
                        row.putAll(processResultSetRow(rs));
                        logger.debug("Updated data: " + row);
                    }
                }
            }
        }
        finally
        {
            if ( ps != null ) ps.close();
            if ( conn != null ) conn.close();
        }

        return rowdata;
    }

    protected BeanScript[] extractParameters(Query query)
    {
        ArrayList<BeanScript> cols = new ArrayList<BeanScript>();

        String s = query.getStatement();
        Matcher m = BeanScript.embeddedPattern.matcher(s);
        while ( m.find() )
        {
            String update = "";
            update += query.getStatement().substring(0, m.start());
            update += '?' + query.getStatement().substring(m.end());
            query.setStatement(update);
            cols.add(new BeanScript(m.group(1)));
        }

        return cols.toArray(new BeanScript[0]);
    }

    public static void main(String[] args)
    {
        try
        {
            if ( args.length != 4 )
            {
                System.out.println("Usage: QueryExtract <app name> <query name> <output file> <application root>");
                System.exit(-1);
            }
            else
            {
                File root = new File(args[3]);
                SecurityManager.initialize(root);
                QueryExtract qe = new QueryExtract(args[0], args[1], args[2]);
                logger.error(qe.startExtract());
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
