/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.query;

import com.blackboard.services.installer.PasswordField;
import com.blackboard.services.security.ConnectionManager;
import com.blackboard.services.security.SecuredObjectManager;
import com.blackboard.services.security.exception.AccessException;
import com.blackboard.services.security.exception.BbTSSecurityException;
import com.blackboard.services.security.exception.RegisterableCastException;
import com.blackboard.services.security.object.DBConnection;
import com.blackboard.services.security.object.NamedKey;
import com.blackboard.services.security.object.Registerable;
import com.blackboard.services.security.object.RegisterableApplication;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import java.sql.ResultSet;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author crusnak
 */
public class QueryManager
{
    private static final String[] menuOptions = { "Register Query",
                                                  "DeRegister Query",
                                                  "Test Query",
                                                  "List Registered Queries",
                                                  "Exit" };

    private static QueryManager instance = null;
    private SecuredObjectManager secman = null;

    private QueryManager()
    {
    }

    public static QueryManager getInstance(PasswordField password)
        throws BbTSSecurityException
    {
        if ( instance == null )
        {
            instance = new QueryManager();
        }
        instance.secman = SecuredObjectManager.getInstance(password);
        return instance;
    }

    public static QueryManager getInstance(RegisterableApplication app)
        throws BbTSSecurityException
    {
        if ( instance == null )
        {
            instance = new QueryManager();
        }

        instance.secman = SecuredObjectManager.getInstance(app);
        return instance;
    }

    public boolean deregisterQuery(String owner, String key)
        throws BbTSSecurityException
    {
        return secman.deregisterObject(owner, key);
    }

    public List<Query> getRegisteredQueries()
        throws BbTSSecurityException
    {
        return (List<Query>)secman.getRegisteredObjects(Query.class);
    }

    public ResultSet testQuery(Query q)
    {
        return null;
    }

    public Query getRegisteredQuery(NamedKey queryKey)
        throws BbTSSecurityException
    {
        Query query = null;
        Object robj = secman.getRegisteredObject(queryKey);
        if ( robj instanceof Query )
        {
            query = (Query)robj;
        }
        else
        {
            throw new RegisterableCastException("Registerable object stored as " +
                        queryKey + " is not of type Query: " +
                        (robj==null ? null:robj.getClass().getName()));
        }

        return query;
    }

    public Query getRegisteredQuery(String appName, String queryName)
        throws BbTSSecurityException
    {
        return getRegisteredQuery(new NamedKey(appName, queryName));
    }

    public ConnectionBean[] getRegisteredConnections()
    {
        ConnectionBean[] connections = null;

        try
        {
            List<NamedKey> keys = secman.getRegisteredNamedKeys(DBConnection.class);
            connections = new ConnectionBean[keys.size()];
            Iterator<NamedKey> it = keys.iterator();
            for ( int i=0; it.hasNext(); i++ )
            {
                NamedKey key = it.next();
                DBConnection conn = (DBConnection)secman.getRegisteredObject(key);
                connections[i] = new ConnectionBean(key, conn);
            }
        }
        catch (BbTSSecurityException e)
        {
            e.printStackTrace();
        }

        return connections;
    }

    public static String readQueryFile(String file)
        throws IOException
    {
        String query = "";
        URL url = ClassLoader.getSystemResource(file);
        if ( url != null )
        {
            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
            String line = "";
            while ( (line= reader.readLine()) != null )
            {
                query += line;
            }
        }
        else
        {
            throw new FileNotFoundException("Unable to locate resource " + file + ". Ensure it exists within the classpaths.");
        }

        return query;
    }

    public boolean registerQuery(String owner, String key, Query query)
        throws BbTSSecurityException
    {
        return secman.registerObject(owner, key, query);
    }

    public boolean registerQuery(String owner, String key, ExtractableQuery query)
        throws BbTSSecurityException
    {
        return secman.registerObject(owner, key, query);
    }

    private static int readMenuOption(BufferedReader entry)
        throws IOException
    {
        System.out.println();
        for( int i=1; i<=menuOptions.length; i++ )
        {
            System.out.println("\t" + i + ")" + menuOptions[i-1]);
        }
        System.out.println();
        System.out.print("Choose your option: ");

        boolean validOption = false;
        int option = 0;
        while ( !validOption )
        {
            String answer = entry.readLine();
            try
            {
                option = Integer.parseInt(answer);
            }
            catch (Exception e) {}

            if ( option > 0 && option <= menuOptions.length )
            {
                validOption = true;
            }
            else
            {
                System.out.print("Invalid option, choose again: ");
            }
        }

        return option;
    }

    private static boolean processMenuOption(int option, BufferedReader entry)
        throws IOException, BbTSSecurityException
    {
        boolean exit = false;
        QueryManager qman = null;//QueryManager.getInstance();

        switch(option)
        {
            case 1:
            {
                //interactiveRegistration();
                break;
            }
            case 2:
            {
                //interactiveDeRegistration();
                break;
            }
            case 3:
            {
                //interactiveQueryTest();
                break;
            }
            case 4:
            {
                System.out.println();
                System.out.println("Not implemented yet");
                /*System.out.println("Querying against a database connection. Please answer the prompts below.");
                System.out.println();
                System.out.print("Name of the application: ");
                String appname = entry.readLine();
                System.out.print("Name of the connection: ");
                String connname = entry.readLine();
                System.out.print("Username for the connection: ");
                String user = entry.readLine();
                System.out.print("Password for the connection: ");
                String pass = entry.readLine();*/
                break;
            }
            case 5:
            {
                //interactiveListQueries();
                break;
            }
            default:
                exit = true;
        }

        return exit;
    }

    public static void main(String[] args)
    {
        try
        {
            BufferedReader entry = new BufferedReader(new InputStreamReader(System.in));

            System.out.println();
            System.out.println("BbTS Connection Manager");
            boolean exit = false;
            while ( !exit )
            {
                int option = readMenuOption(entry);
                exit = processMenuOption(option, entry);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
