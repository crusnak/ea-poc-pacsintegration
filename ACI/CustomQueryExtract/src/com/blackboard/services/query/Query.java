/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.query;

import com.blackboard.services.security.object.NamedKey;
import com.blackboard.services.security.object.RegisterableObject;
import java.io.Serializable;
import java.sql.ResultSet;

/**
 *
 * @author crusnak
 */
public class Query implements Serializable, RegisterableObject
{
    private static final long serialVersionUID = 87093275322095L;

    protected String name = "";
    protected NamedKey connection = null;
    protected String statement = null;

    public Query() {}

    public Query(NamedKey connkey, String n, String s)
    {
        connection = connkey;
        name = n;
        statement = s;
    }

    public NamedKey getConnectionKey()
    {
        return connection;
    }

    public NamedKey getKey()
    {
        return getConnectionKey();
    }

    public void setConnectionKey(NamedKey connection)
    {
        this.connection = connection;
    }

    public void setKey(NamedKey connection)
    {
        setConnectionKey(connection);
    }

    public void setKey(String name, String connection)
    {
        setKey(new NamedKey(name, connection));
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getStatement()
    {
        return statement;
    }

    public String getTrimmedStatement()
    {
        String trim = statement;
        trim = trim.replaceAll("\\\\n", " \\\\n ");
        trim = trim.replaceAll("\\s+", " ");
        return trim;
    }

    public void setStatement(String statement)
    {
        this.statement = statement;
    }

    private ResultSet executeQuery()
    {
        return null;
    }

    private ResultSet executeQuery(String[] params)
    {
        return null;
    }

    public String toString()
    {
        StringBuffer out = new StringBuffer();

        out.append("{" + connection);
        out.append(",[" + statement.replaceAll("\\s+", " ") + "]}");

        return out.toString();
    }
}
