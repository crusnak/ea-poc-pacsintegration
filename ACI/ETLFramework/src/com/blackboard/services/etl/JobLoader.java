/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl;

import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.jaxb.DataComplexType;
import com.blackboard.services.etl.jaxb.EtlJob;
import com.blackboard.services.etl.jaxb.InputTaskComplexType;
import com.blackboard.services.etl.jaxb.MappingTaskComplexType;
import com.blackboard.services.etl.jaxb.ObjectFactory;
import com.blackboard.services.etl.jaxb.OutputTaskComplexType;
import com.blackboard.services.etl.process.Processor;
import com.blackboard.services.utils.ResourceLoader;
import com.blackboard.services.utils.exception.CompilationException;
import com.blackboard.services.utils.exception.CompilerError;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author crusnak
 */
public class JobLoader
{
    File xml = null;
    EtlJob job = null;
    HashSet<DataComplexType> masterData = new HashSet();
    HashSet<InputTaskComplexType> inputs = new HashSet();
    HashSet<OutputTaskComplexType> outputs = new HashSet();
    HashSet<MappingTaskComplexType> mappings = new HashSet();
    File etlRoot = null;

    /**
     * Use the other constructor to look for a job name within the classpath.
     * @param f
     * @throws ETLException 
     */
    public JobLoader(File root, File f)
        throws ETLException
    {
        try
        {
            etlRoot = root;
            xml = f;
            if ( !xml.exists() )
            {
                throw new IOException("Unable to locate file: " + xml);
            }

            JAXBContext jc = JAXBContext.newInstance("com.blackboard.services.etl.jaxb");
            Unmarshaller um = jc.createUnmarshaller();
            Object j = um.unmarshal(xml);
            if ( j instanceof EtlJob )
            {
                job = (EtlJob)j;
                validateJob();
            }
            else
            {
                throw new ETLException("XML file " + f.getAbsolutePath() + " is not an EtlJob");
            }
        }
        catch (IOException e)
        {
            throw new ETLException("Fatal Error", e);
        }
        catch (JAXBException e)
        {
            throw new ETLException("Fatal Error", e);
        }
    }

    public JobLoader(File root, String f)
        throws ETLException
    {
        try
        {
            etlRoot = root;
            xml = ResourceLoader.findResourceAsFile(null, root, f);
            JAXBContext jc = JAXBContext.newInstance("com.blackboard.services.etl.jaxb");
            Unmarshaller um = jc.createUnmarshaller();
            job = (EtlJob)um.unmarshal(xml);
            validateJob();
        }
        catch (IOException e)
        {
            throw new ETLException("Fatal Error", e);
        }
        catch (JAXBException e)
        {
            throw new ETLException("Fatal Error", e);
        }
    }

    public JobLoader(String f)
        throws ETLException
    {
        try
        {
            URL metaURL = ClassLoader.getSystemResource(f);
            if ( metaURL == null )
            {
                throw new ETLException("Cannot find etl job: " + f);
            }

            xml = new File(metaURL.getFile());
            JAXBContext jc = JAXBContext.newInstance("com.blackboard.services.etl.jaxb");
            Unmarshaller um = jc.createUnmarshaller();
            job = (EtlJob)um.unmarshal(xml);
            validateJob();
        }
        catch (JAXBException e)
        {
            throw new ETLException("Fatal Error", e);
        }
    }

    public ETLJobWrapper getETLJobWrapper(Map<String,String> args, String jobnum)
        throws ETLException
    {
        ETLJobWrapper w = null;
        if ( etlRoot != null )
        {
            w = new ETLJobWrapper(job, etlRoot, xml, args, jobnum);
        }
        else
        {
            //w = new ETLJobWrapper(job, xml);
            throw new ETLException("ETL Job root not set");
        }
        return w;
    }
    

    public File getLoadedFile()
    {
        return xml;
    }
    
    public static String convertXMLToString(Object xml)
        throws ETLException
    {
        String output = null;
        try
        {
            javax.xml.bind.JAXBContext context = javax.xml.bind.JAXBContext.newInstance("com.blackboard.services.etl.jaxb");
            Marshaller m = context.createMarshaller();
            StringWriter writer = new StringWriter();
            m.marshal(new ObjectFactory().createXml(xml), writer);
            output = writer.toString();
        }
        catch (javax.xml.bind.JAXBException e)
        {
            throw new ETLException("Unable to convert XML to string", e);
        }

        return output;
    }

    private void validateJob()
        throws ETLException
    {
        ArrayList<CompilerError> errorList = new ArrayList();
        errorList.addAll(validateMasterData(job.getMasterData()));
        errorList.addAll(validateJobDefinition(job.getJobDefinition()));

        if ( errorList.size() > 0 )
        {
            CompilationException exception = new CompilationException("ETL job for " + xml + " contains errors:");
            exception.addErrors(errorList);
            throw new ETLException(exception);
        }
    }

    private List<CompilerError> validateMasterData(EtlJob.MasterData md)
    {
        List<CompilerError> errors = new ArrayList<CompilerError>();
        List<EtlJob.MasterData.Data> data = md.getData();

        return errors;
    }

    private List<CompilerError> validateJobDefinition(EtlJob.JobDefinition def)
    {
        List<CompilerError> errors = new ArrayList<CompilerError>();
        /*InputTaskComplexType input = def.getInputAdapter();
        inputs.add(input);

        List<MappingTaskComplexType> maplist = def.getDataMapping();
        mappings.addAll(maplist);

        List<OutputTaskComplexType> outlist = def.getOutputAdapter();
        outputs.addAll(outlist);*/
        //List<JavaTaskComplexType> programs = def.get

        return errors;
    }

}
