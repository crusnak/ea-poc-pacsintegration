/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl;

import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.process.Processor;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.blackboard.services.security.object.NamedKey;
import com.blackboard.services.utils.BaseLogger;
import com.blackboard.services.utils.BbBaseProperties;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author crusnak
 */
public class ExtractControlConfiguration extends BbBaseProperties
{
    public static final String propertyPath =           "resources/";
    
    public static final String PROP_THIS_FROM =             "date.this.from";
    public static final String PROP_THIS_TO =               "date.this.to";
    public static final String PROP_LAST_FROM =             "date.last.from";
    public static final String PROP_LAST_TO =               "date.last.to";
    
    private static Logger logger = null;
    
    private String ecName = null;

    public ExtractControlConfiguration(File root, String name)
        throws IOException
    {
        super(null, root, propertyPath + name + Processor.TXT_EXT, true);
        ecName = name;
        
        logger = Logger.getLogger(ETLConfiguration.class);
        logger.debug("ExtractControlConfiguration app root: " + appRoot);
        logger.debug("ExtractControlConfiguration loaded: " + propertyURL);
        if ( propertyURL != null )
        {
            logger.debug("ExtractControlConfiguration path: " + propertyURL.getFile());
        }
    }

    public String getThisFromString()
    {
        return getProperty(PROP_THIS_FROM);
    }
    
    public Date getThisFrom()
        throws ParseException
    {
        return ETLJobWrapper.extractControlFormat.parse(getThisFromString());
    }

    public void setThisFrom(String date)
    {
        if ( date == null )
        {
            setProperty(PROP_THIS_FROM, "");
        }
        else
        {
            setProperty(PROP_THIS_FROM, date);
        }
    }
    
    public void setThisFrom(Date date)
    {
        if ( date == null )
        {
            setThisFrom("");
        }
        else
        {
            setThisFrom(ETLJobWrapper.extractControlFormat.format(date));
        }
    }

    public String getThisToString()
    {
        return getProperty(PROP_THIS_TO);
    }
    
    public Date getThisTo()
        throws ParseException
    {
        return ETLJobWrapper.extractControlFormat.parse(getThisToString());
    }

    public void setThisTo(String date)
    {
        if ( date == null )
        {
            setProperty(PROP_THIS_TO, "");
        }
        else
        {
            setProperty(PROP_THIS_TO, date);
        }
    }
    
    public void setThisTo(Date date)
    {
        if ( date == null )
        {
            setThisTo("");
        }
        else
        {
            setThisTo(ETLJobWrapper.extractControlFormat.format(date));
        }
    }

    public String getLastFromString()
    {
        return getProperty(PROP_LAST_FROM);
    }
    
    public Date getLastFrom()
        throws ParseException
    {
        return ETLJobWrapper.extractControlFormat.parse(getLastFromString());
    }

    public void setLastFrom(String date)
    {
        if ( date == null )
        {
            setProperty(PROP_LAST_FROM, "");
        }
        else
        {
            setProperty(PROP_LAST_FROM, date);
        }
    }
    
    public void setLastFrom(Date date)
    {
        if ( date == null )
        {
            setLastFrom("");
        }
        else
        {
            setLastFrom(ETLJobWrapper.extractControlFormat.format(date));
        }
    }    
    
    public String getLastToString()
    {
        return getProperty(PROP_LAST_TO);
    }
    
    public Date getLastTo()
        throws ParseException
    {
        return ETLJobWrapper.extractControlFormat.parse(getLastToString());
    }

    public void setLastTo(String date)
    {
        if ( date == null )
        {
            setProperty(PROP_LAST_TO, "");
        }
        else
        {
            setProperty(PROP_LAST_TO, date);
        }
    }
    
    public void setLastTo(Date date)
    {
        if ( date == null )
        {
            setLastTo("");
        }
        else
        {
            setLastTo(ETLJobWrapper.extractControlFormat.format(date));
        }
    }      

    @Override
    public synchronized String toString() 
    {
        StringBuilder out = new StringBuilder();
        
        out.append("file=" + propertyURL.getFile());
        out.append(", date.this.from=" + getThisFromString());
        out.append(", date.this.to=" + getThisToString());
        out.append(", date.last.from=" + getLastFromString());
        out.append(", date.last.to=" + getLastToString());
        
        return out.toString();
    }
    
    
}
