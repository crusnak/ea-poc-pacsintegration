/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.exception;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author crusnak
 */
public class FilterException extends ETLException
{
    public FilterException()
    {
        super();
    }

    public FilterException(String message)
    {
        super(message);
    }

    public FilterException(Throwable cause)
    {
        super(cause);
    }

    public FilterException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
