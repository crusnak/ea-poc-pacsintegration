/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.exception;

import com.blackboard.services.etl.data.DataIndex;

/**
 *
 * @author crusnak
 */
public class IndexException extends ETLException
{
    private DataIndex index = null;

    public IndexException(DataIndex i)
    {
        super();
        index = i;
    }

    public IndexException(DataIndex i, String message)
    {
        super(message);
        index = i;
    }

    public IndexException(DataIndex i, Throwable cause)
    {
        super(cause);
        index = i;
    }

    public IndexException(DataIndex i, String message, Throwable cause)
    {
        super(message, cause);
        index = i;
    }

    /*@Override
    public String toString()
    {
        String s = getClass().getName();
        String message = getLocalizedMessage();
        String mismatch = (index != null) ? "Expecting type " + cell.getColumn().getDataType() : "";
        String cellname = (cell != null) ? "Unable to set value for column '" + cell.getColumn().getColumnName() + "'" : "";
        return (message != null) ? (s + ": " + cellname + ": " + message + ": " + mismatch) : s + ": " + cellname + ": " + mismatch;
    }*/
}
