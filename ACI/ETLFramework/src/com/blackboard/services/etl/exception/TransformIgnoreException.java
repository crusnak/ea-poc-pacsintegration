/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.exception;

import com.blackboard.services.etl.data.DataCell;

/**
 *
 * @author crusnak
 */
public class TransformIgnoreException extends TransformException
{
    private DataCell cell = null;

    public TransformIgnoreException(DataCell c)
    {
        super(c);
    }

    public TransformIgnoreException(DataCell c, String message)
    {
        super(c, message);
    }

    public TransformIgnoreException(DataCell c, Throwable cause)
    {
        super(c, cause);
    }

    public TransformIgnoreException(DataCell c, String message, Throwable cause)
    {
        super(c, message, cause);
    }
}
