/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.exception;

import com.blackboard.services.etl.data.DataCell;

/**
 *
 * @author crusnak
 */
public class TransformException extends RowLevelException
{
    private DataCell cell = null;

    public TransformException(DataCell c)
    {
        super();
        cell = c;
    }

    public TransformException(DataCell c, String message)
    {
        super(message);
        cell = c;
    }

    public TransformException(DataCell c, Throwable cause)
    {
        super(cause);
        cell = c;
    }

    public TransformException(DataCell c, String message, Throwable cause)
    {
        super(message, cause);
        cell = c;
    }

    public DataCell getDataCell()
    {
        return cell;
    }

    public void setDataCell(DataCell cell)
    {
        this.cell = cell;
    }

    @Override
    public String toString()
    {
        String s = getClass().getName();
        String message = getLocalizedMessage();
        String mismatch = (cell != null) ? "Transform error on: " + cell : "";
        return (message != null) ? (s + ": " + message + ": " + mismatch) : s + ": " + mismatch;
    }
}
