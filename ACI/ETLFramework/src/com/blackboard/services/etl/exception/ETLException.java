/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.exception;

/**
 *
 * @author crusnak
 */
public class ETLException extends Exception
{
    Object metadata = null;
    
    public ETLException()
    {
        super();
    }

    public ETLException(String message)
    {
        super(message);
    }

    public ETLException(Throwable cause)
    {
        super(cause);
    }

    public ETLException(String message, Throwable cause)
    {
        super(message, cause);
    }
    
    public ETLException(String message, Object meta)
    {
        super(message);
        metadata = meta;
    }
    
    public ETLException(Throwable cause, Object meta)
    {
        super(cause);
        metadata = meta;
    }
     
    public ETLException(String message, Throwable cause, Object meta)
    {
        super(message, cause);
        metadata = meta;
    }
    
    public void setMetadata(Object o)
    {
        metadata = o;
    }
    
    public Object getMetadata()
    {
        return metadata;
    }
    
    @Override
    public String toString()
    {
        String s = getClass().getName();
        String message = getLocalizedMessage();
        
        return (message != null) ? (s + ": " + message + (metadata!=null?": " + metadata:"")) : s + (metadata!=null?": " + metadata:"");
    }
}
