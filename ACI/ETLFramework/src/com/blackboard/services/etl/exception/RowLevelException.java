/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.exception;

/**
 *
 * @author crusnak
 */
public class RowLevelException extends ETLException
{
    public RowLevelException() 
    {
    }
    
    public RowLevelException(String message)
    {
        super(message);
    }
    
    public RowLevelException(Throwable cause)
    {
        super(cause);
    }
    
    public RowLevelException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
