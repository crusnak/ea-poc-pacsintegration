/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.exception;

/**
 *
 * @author crusnak
 */
public class SortException extends ETLException
{
    public SortException()
    {
        super();
    }

    public SortException(String message)
    {
        super(message);
    }

    public SortException(Throwable cause)
    {
        super(cause);
    }

    public SortException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
