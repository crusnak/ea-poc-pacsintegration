/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.exception;

import com.blackboard.services.etl.data.DataCell;

/**
 *
 * @author crusnak
 */
public class VariableTypeMismatchException extends ETLException
{
    private DataCell cell = null;

    public VariableTypeMismatchException(DataCell c)
    {
        super();
        cell = c;
    }

    public VariableTypeMismatchException(DataCell c, String message)
    {
        super(message);
        cell = c;
    }

    public VariableTypeMismatchException(DataCell c, Throwable cause)
    {
        super(cause);
        cell = c;
    }

    public VariableTypeMismatchException(DataCell c, String message, Throwable cause)
    {
        super(message, cause);
        cell = c;
    }

    @Override
    public String toString()
    {
        String s = getClass().getName();
        String message = getLocalizedMessage();
        String mismatch = (cell != null) ? "Expecting type " + cell.getColumn().getDataType() : "";
        String cellname = (cell != null) ? "Unable to set value for column '" + cell.getColumn().getColumnName() + "'" : "";
        return (message != null) ? (s + ": " + cellname + ": " + message + ": " + mismatch) : s + ": " + cellname + ": " + mismatch;
    }
}
