/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.exception;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;

/**
 *
 * @author crusnak
 */
public class JacksonNodeException extends JsonProcessingException
{
    JsonNode node;
    
    public JacksonNodeException(String msg)
    {
        super(msg);
    }
    
    public JacksonNodeException(String msg, JsonNode n)
    {
        super(msg);
        node = n;
    }
    
    public JacksonNodeException(String msg, Throwable t)
    {
        super(msg, t);
    }
    
    public JacksonNodeException(String msg, JsonNode n, Throwable t)
    {
        super(msg, t);
        node = n;
    }
    
    public JsonNode getNode()
    {
        return node;
    }
}
