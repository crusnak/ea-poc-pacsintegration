/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.exception;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author crusnak
 */
public class ProcessingException extends ETLException
{
    public ProcessingException()
    {
        super();
    }

    public ProcessingException(String message)
    {
        super(message);
    }

    public ProcessingException(Throwable cause)
    {
        super(cause);
    }

    public ProcessingException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
