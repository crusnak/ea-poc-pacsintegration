/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.wrapper;

import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.utils.BeanScript;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.jaxb.BinaryInputTaskComplexType;
import com.blackboard.services.etl.jaxb.BinaryOutputTaskComplexType;
import com.blackboard.services.etl.jaxb.DatabaseInputTaskComplexType;
import com.blackboard.services.etl.jaxb.DatabaseOutputTaskComplexType;
import com.blackboard.services.etl.jaxb.EmailOutputTaskComplexType;
import com.blackboard.services.etl.jaxb.FileInputTaskComplexType;
import com.blackboard.services.etl.jaxb.FileOutputTaskComplexType;
import com.blackboard.services.etl.jaxb.MultipleFileInputTaskComplexType;
import com.blackboard.services.etl.jaxb.MultipleFileOutputTaskComplexType;
import com.blackboard.services.etl.jaxb.TiaConfigComplexType;
import com.blackboard.services.etl.jaxb.WebAPITaskComplexType;
import com.blackboard.services.etl.jaxb.WebConfigComplexType;
import com.blackboard.services.etl.tasks.AbstractTask;
import com.blackboard.services.security.SecurityManager;
import com.blackboard.services.security.SecuredObjectManager;
import com.blackboard.services.security.ConnectionManager;
import com.blackboard.services.security.exception.BbTSSecurityException;
import com.blackboard.services.security.object.DBConnection;
import com.blackboard.services.security.object.EmailConnection;
import com.blackboard.services.security.object.EncryptionObject;
import com.blackboard.services.security.object.NamedKey;
import com.blackboard.services.security.object.RegisterableApplication;
import com.blackboard.services.security.object.SSHConnection;
import com.blackboard.services.security.object.TIAConnection;
import com.blackboard.services.security.object.WebConnection;
import java.io.File;
import java.net.URISyntaxException;
import org.apache.log4j.Logger;

/**
 *
 * @author crusnak
 */
public class ConnectionFactory
{
    public static DBConnection getDatabaseConnection(DatabaseInputTaskComplexType.ConnectionInfo info, AbstractTask task)
        throws ETLException
    {
        DBConnection dbinfo = null;
        if ( info != null && info.getDbConnection() != null )
        {
            try
            {
                SecurityManager.initialize(null, task.getETLJobWrapper().getETLRoot());
                String appname = String.valueOf(getGlobalVariableValue(info.getDbConnection().getApplication(), task));
                String key = String.valueOf(getGlobalVariableValue(info.getDbConnection().getKey(), task));

                RegisterableApplication app = new RegisterableApplication(appname);
                NamedKey nkey = new NamedKey(appname, key);
                ConnectionManager conman = ConnectionManager.getInstance(app);
                dbinfo = conman.getDBConnection(nkey);
                if ( dbinfo == null )
                {
                    throw new ETLException("Database connection not registered with security manager: " + nkey);
                }
            }
            catch (BbTSSecurityException e)
            {
                throw new ETLException("Unable to access SecurityManger to get database connection", e);
            }
        }
        else if ( info != null && info.getDefineConnection() != null )
        {
            DatabaseInputTaskComplexType.ConnectionInfo.DefineConnection define = info.getDefineConnection();
            String driver = String.valueOf(getGlobalVariableValue(define.getJdbcDriver(), task));
            String host = String.valueOf(getGlobalVariableValue(define.getHost(), task));
            String port = String.valueOf(getGlobalVariableValue(String.valueOf(define.getPort()), task));
            String instance = String.valueOf(getGlobalVariableValue(define.getInstance(), task));
            String user = String.valueOf(getGlobalVariableValue(define.getUsername(), task));
            String pass = String.valueOf(getGlobalVariableValue(define.getPassword(), task));

            dbinfo = new DBConnection("", "");
            dbinfo.setJdbcDriver(driver);
            dbinfo.setDatabaseHost(host);
            dbinfo.setDatabasePort(port);
            dbinfo.setDatabaseInstance(instance);
            dbinfo.setUsername(user);
            dbinfo.setPassword(pass);
        }

        // Indicate when a db connection is needed
        if ( dbinfo != null )
        {
            Logger.getLogger(ConnectionFactory.class).info("Using DB connection: " + dbinfo);
        }
     
        return dbinfo;
    }
    
    public static DBConnection getDatabaseConnection(DatabaseOutputTaskComplexType.ConnectionInfo info, AbstractTask task)
        throws ETLException
    {
        DBConnection dbinfo = null;
        if ( info != null && info.getDbConnection() != null )
        {
            try
            {
                SecurityManager.initialize(null, task.getETLJobWrapper().getETLRoot());
                String appname = String.valueOf(getGlobalVariableValue(info.getDbConnection().getApplication(), task));
                String key = String.valueOf(getGlobalVariableValue(info.getDbConnection().getKey(), task));

                RegisterableApplication app = new RegisterableApplication(appname);
                NamedKey nkey = new NamedKey(appname, key);
                ConnectionManager conman = ConnectionManager.getInstance(app);
                dbinfo = conman.getDBConnection(nkey);
                if ( dbinfo == null )
                {
                    throw new ETLException("Database connection not registered with security manager: " + nkey);
                }
            }
            catch (BbTSSecurityException e)
            {
                throw new ETLException("Unable to access SecurityManger to get database connection", e);
            }
        }
        else if ( info != null && info.getDefineConnection() != null )
        {
            DatabaseOutputTaskComplexType.ConnectionInfo.DefineConnection define = info.getDefineConnection();
            String driver = String.valueOf(getGlobalVariableValue(define.getJdbcDriver(), task));
            String host = String.valueOf(getGlobalVariableValue(define.getHost(), task));
            String port = String.valueOf(getGlobalVariableValue(String.valueOf(define.getPort()), task));
            String instance = String.valueOf(getGlobalVariableValue(define.getInstance(), task));
            String user = String.valueOf(getGlobalVariableValue(define.getUsername(), task));
            String pass = String.valueOf(getGlobalVariableValue(define.getPassword(), task));

            dbinfo = new DBConnection("", "");
            dbinfo.setJdbcDriver(driver);
            dbinfo.setDatabaseHost(host);
            dbinfo.setDatabasePort(port);
            dbinfo.setDatabaseInstance(instance);
            dbinfo.setUsername(user);
            dbinfo.setPassword(pass);
        }

        // Indicate when a db connection is needed
        if ( dbinfo != null )
        {
            Logger.getLogger(ConnectionFactory.class).info("Using DB connection: " + dbinfo);
        }
     
        return dbinfo;
    }
    
    public static DBConnection getDatabaseConnection(NamedKey nk, File root)
        throws ETLException
    {
        DBConnection dbinfo = null;
        
            try
            {
                SecurityManager.initialize(null, root);
                RegisterableApplication app = new RegisterableApplication(nk.getName());
                ConnectionManager conman = ConnectionManager.getInstance(app);
                dbinfo = conman.getDBConnection(nk);
                if ( dbinfo == null )
                {
                    throw new ETLException("Database connection not registered with security manager: " + nk);
                }
            }
            catch (BbTSSecurityException e)
            {
                throw new ETLException("Unable to access SecurityManger to get database connection", e);
            }

            return dbinfo;
    }
    
    public static SSHConnection getSSHConnection(FileInputTaskComplexType.File info, AbstractTask task)
        throws ETLException
    {
        SSHConnection sftpinfo = null;
        if ( info != null && info.getSftpConnection() != null )
        {
            try
            {
                SecurityManager.initialize(null, task.getETLJobWrapper().getETLRoot());
                String appname = String.valueOf(getGlobalVariableValue(info.getSftpConnection().getApplication(), task));
                String key = String.valueOf(getGlobalVariableValue(info.getSftpConnection().getKey(), task));

                RegisterableApplication app = new RegisterableApplication(appname);
                NamedKey nkey = new NamedKey(appname, key);
                ConnectionManager conman = ConnectionManager.getInstance(app);
                sftpinfo = conman.getConnection(SSHConnection.class, nkey);
                if ( sftpinfo == null )
                {
                    throw new ETLException("SSH connection not registered with security manager: " + nkey);
                }
            }
            catch (BbTSSecurityException e)
            {
                throw new ETLException("Unable to access SecurityManger to get ssh connection", e);
            }
        }
        else if ( info != null && info.getDefineConnection() != null )
        {
            FileInputTaskComplexType.File.DefineConnection define = info.getDefineConnection();
            String host = String.valueOf(getGlobalVariableValue(define.getHost(), task));
            String port = String.valueOf(getGlobalVariableValue(String.valueOf(define.getPort()), task));
            String user = String.valueOf(getGlobalVariableValue(define.getUsername(), task));
            String pass = String.valueOf(getGlobalVariableValue(define.getPassword(), task));

            sftpinfo = new SSHConnection("", "");
            sftpinfo.setHost(host);
            sftpinfo.setPort(Integer.parseInt(port));
            sftpinfo.setUsername(user);
            sftpinfo.setPassword(pass);
        }

        // Indicate when an sftp connection is needed
        if ( sftpinfo != null )
        {
            Logger.getLogger(ConnectionFactory.class).info("Using SFTP connection: " + sftpinfo);
        }
        
        return sftpinfo;
    }

    public static SSHConnection getSSHConnection(BinaryInputTaskComplexType.Connection info, AbstractTask task)
        throws ETLException
    {
        SSHConnection sftpinfo = null;
        if ( info != null && info.getSftpConnection() != null )
        {
            try
            {
                SecurityManager.initialize(null, task.getETLJobWrapper().getETLRoot());
                String appname = String.valueOf(getGlobalVariableValue(info.getSftpConnection().getApplication(), task));
                String key = String.valueOf(getGlobalVariableValue(info.getSftpConnection().getKey(), task));

                RegisterableApplication app = new RegisterableApplication(appname);
                NamedKey nkey = new NamedKey(appname, key);
                ConnectionManager conman = ConnectionManager.getInstance(app);
                sftpinfo = conman.getConnection(SSHConnection.class, nkey);
                if ( sftpinfo == null )
                {
                    throw new ETLException("SSH connection not registered with security manager: " + nkey);
                }
            }
            catch (BbTSSecurityException e)
            {
                throw new ETLException("Unable to access SecurityManger to get ssh connection", e);
            }
        }
        else if ( info != null && info.getDefineConnection() != null )
        {
            BinaryInputTaskComplexType.Connection.DefineConnection define = info.getDefineConnection();
            String host = String.valueOf(getGlobalVariableValue(define.getHost(), task));
            String port = String.valueOf(getGlobalVariableValue(String.valueOf(define.getPort()), task));
            String user = String.valueOf(getGlobalVariableValue(define.getUsername(), task));
            String pass = String.valueOf(getGlobalVariableValue(define.getPassword(), task));

            sftpinfo = new SSHConnection("", "");
            sftpinfo.setHost(host);
            sftpinfo.setPort(Integer.parseInt(port));
            sftpinfo.setUsername(user);
            sftpinfo.setPassword(pass);
        }

        // Indicate when an sftp connection is needed
        if ( sftpinfo != null )
        {
            Logger.getLogger(ConnectionFactory.class).info("Using SFTP connection: " + sftpinfo);
        }
        
        return sftpinfo;
    }

    public static SSHConnection getSSHConnection(BinaryOutputTaskComplexType.Connection info, AbstractTask task)
        throws ETLException
    {
        SSHConnection sftpinfo = null;
        if ( info != null && info.getSftpConnection() != null )
        {
            try
            {
                SecurityManager.initialize(null, task.getETLJobWrapper().getETLRoot());
                String appname = String.valueOf(getGlobalVariableValue(info.getSftpConnection().getApplication(), task));
                String key = String.valueOf(getGlobalVariableValue(info.getSftpConnection().getKey(), task));

                RegisterableApplication app = new RegisterableApplication(appname);
                NamedKey nkey = new NamedKey(appname, key);
                ConnectionManager conman = ConnectionManager.getInstance(app);
                sftpinfo = conman.getConnection(SSHConnection.class, nkey);
                if ( sftpinfo == null )
                {
                    throw new ETLException("SSH connection not registered with security manager: " + nkey);
                }
            }
            catch (BbTSSecurityException e)
            {
                throw new ETLException("Unable to access SecurityManger to get ssh connection", e);
            }
        }
        else if ( info != null && info.getDefineConnection() != null )
        {
            BinaryOutputTaskComplexType.Connection.DefineConnection define = info.getDefineConnection();
            String host = String.valueOf(getGlobalVariableValue(define.getHost(), task));
            String port = String.valueOf(getGlobalVariableValue(String.valueOf(define.getPort()), task));
            String user = String.valueOf(getGlobalVariableValue(define.getUsername(), task));
            String pass = String.valueOf(getGlobalVariableValue(define.getPassword(), task));

            sftpinfo = new SSHConnection("", "");
            sftpinfo.setHost(host);
            sftpinfo.setPort(Integer.parseInt(port));
            sftpinfo.setUsername(user);
            sftpinfo.setPassword(pass);
        }

        // Indicate when an sftp connection is needed
        if ( sftpinfo != null )
        {
            Logger.getLogger(ConnectionFactory.class).info("Using SFTP connection: " + sftpinfo);
        }
        
        return sftpinfo;
    }

    public static SSHConnection getSSHConnection(MultipleFileInputTaskComplexType.File info, AbstractTask task)
        throws ETLException
    {
        SSHConnection sftpinfo = null;
        if ( info != null && info.getSftpConnection() != null )
        {
            try
            {
                SecurityManager.initialize(null, task.getETLJobWrapper().getETLRoot());
                String appname = String.valueOf(getGlobalVariableValue(info.getSftpConnection().getApplication(), task));
                String key = String.valueOf(getGlobalVariableValue(info.getSftpConnection().getKey(), task));

                RegisterableApplication app = new RegisterableApplication(appname);
                NamedKey nkey = new NamedKey(appname, key);
                ConnectionManager conman = ConnectionManager.getInstance(app);
                sftpinfo = conman.getConnection(SSHConnection.class, nkey);
                if ( sftpinfo == null )
                {
                    throw new ETLException("SSH connection not registered with security manager: " + nkey);
                }
            }
            catch (BbTSSecurityException e)
            {
                throw new ETLException("Unable to access SecurityManger to get ssh connection", e);
            }
        }
        else if ( info != null && info.getDefineConnection() != null )
        {
            MultipleFileInputTaskComplexType.File.DefineConnection define = info.getDefineConnection();
            String host = String.valueOf(getGlobalVariableValue(define.getHost(), task));
            String port = String.valueOf(getGlobalVariableValue(String.valueOf(define.getPort()), task));
            String user = String.valueOf(getGlobalVariableValue(define.getUsername(), task));
            String pass = String.valueOf(getGlobalVariableValue(define.getPassword(), task));

            sftpinfo = new SSHConnection("", "");
            sftpinfo.setHost(host);
            sftpinfo.setPort(Integer.parseInt(port));
            sftpinfo.setUsername(user);
            sftpinfo.setPassword(pass);
        }

        // Indicate when an sftp connection is needed
        if ( sftpinfo != null )
        {
            Logger.getLogger(ConnectionFactory.class).info("Using SFTP connection: " + sftpinfo);
        }
        
        return sftpinfo;
    }

    public static SSHConnection getSSHConnection(FileOutputTaskComplexType.File info, AbstractTask task)
        throws ETLException
    {
        SSHConnection sftpinfo = null;
        if ( info != null && info.getSftpConnection() != null )
        {
            try
            {
                SecurityManager.initialize(null, task.getETLJobWrapper().getETLRoot());
                String appname = String.valueOf(getGlobalVariableValue(info.getSftpConnection().getApplication(), task));
                String key = String.valueOf(getGlobalVariableValue(info.getSftpConnection().getKey(), task));

                RegisterableApplication app = new RegisterableApplication(appname);
                NamedKey nkey = new NamedKey(appname, key);
                ConnectionManager conman = ConnectionManager.getInstance(app);
                sftpinfo = conman.getConnection(SSHConnection.class, nkey);
                if ( sftpinfo == null )
                {
                    throw new ETLException("SSH connection not registered with security manager: " + nkey);
                }
            }
            catch (BbTSSecurityException e)
            {
                throw new ETLException("Unable to access SecurityManger to get ssh connection", e);
            }
        }
        else if ( info != null && info.getDefineConnection() != null )
        {
            FileOutputTaskComplexType.File.DefineConnection define = info.getDefineConnection();
            String host = String.valueOf(getGlobalVariableValue(define.getHost(),task));
            String port = String.valueOf(getGlobalVariableValue(String.valueOf(define.getPort()), task));
            String user = String.valueOf(getGlobalVariableValue(define.getUsername(), task));
            String pass = String.valueOf(getGlobalVariableValue(define.getPassword(), task));

            sftpinfo = new SSHConnection("", "");
            sftpinfo.setHost(host);
            sftpinfo.setPort(Integer.parseInt(port));
            sftpinfo.setUsername(user);
            sftpinfo.setPassword(pass);
        }

        // Indicate when an sftp connection is needed
        if ( sftpinfo != null )
        {
            Logger.getLogger(ConnectionFactory.class).info("Using SFTP connection: " + sftpinfo);
        }
        
        return sftpinfo;
    }

    public static SSHConnection getSSHConnection(MultipleFileOutputTaskComplexType.File info, AbstractTask task)
        throws ETLException
    {
        SSHConnection sftpinfo = null;
        if ( info != null && info.getSftpConnection() != null )
        {
            try
            {
                SecurityManager.initialize(null, task.getETLJobWrapper().getETLRoot());
                String appname = String.valueOf(getGlobalVariableValue(info.getSftpConnection().getApplication(), task));
                String key = String.valueOf(getGlobalVariableValue(info.getSftpConnection().getKey(), task));

                RegisterableApplication app = new RegisterableApplication(appname);
                NamedKey nkey = new NamedKey(appname, key);
                ConnectionManager conman = ConnectionManager.getInstance(app);
                sftpinfo = conman.getConnection(SSHConnection.class, nkey);
                if ( sftpinfo == null )
                {
                    throw new ETLException("SSH connection not registered with security manager: " + nkey);
                }
            }
            catch (BbTSSecurityException e)
            {
                throw new ETLException("Unable to access SecurityManger to get ssh connection", e);
            }
        }
        else if ( info != null && info.getDefineConnection() != null )
        {
            MultipleFileOutputTaskComplexType.File.DefineConnection define = info.getDefineConnection();
            String host = String.valueOf(getGlobalVariableValue(define.getHost(),task));
            String port = String.valueOf(getGlobalVariableValue(String.valueOf(define.getPort()), task));
            String user = String.valueOf(getGlobalVariableValue(define.getUsername(), task));
            String pass = String.valueOf(getGlobalVariableValue(define.getPassword(), task));

            sftpinfo = new SSHConnection("", "");
            sftpinfo.setHost(host);
            sftpinfo.setPort(Integer.parseInt(port));
            sftpinfo.setUsername(user);
            sftpinfo.setPassword(pass);
        }

        // Indicate when an sftp connection is needed
        if ( sftpinfo != null )
        {
            Logger.getLogger(ConnectionFactory.class).info("Using SFTP connection: " + sftpinfo);
        }
        
        return sftpinfo;
    }

    public static EncryptionObject getEncryptionObject(FileInputTaskComplexType.File info, AbstractTask task)
        throws ETLException
    {
        EncryptionObject encryptinfo = null;
        if ( info != null && info.getDecryptionKey() != null )
        {
            try
            {
                SecurityManager.initialize(null, task.getETLJobWrapper().getETLRoot());
                String appname = String.valueOf(getGlobalVariableValue(info.getDecryptionKey().getApplication(), task));
                String key = String.valueOf(getGlobalVariableValue(info.getDecryptionKey().getKey(), task));

                RegisterableApplication app = new RegisterableApplication(appname);
                NamedKey nkey = new NamedKey(appname, key);
                SecuredObjectManager secman = SecuredObjectManager.getInstance(app);
                encryptinfo = secman.getRegisteredObject(EncryptionObject.class, nkey);
                if ( encryptinfo == null )
                {
                    throw new ETLException("Encryption key not registered with security manager: " + nkey);
                }
            }
            catch (BbTSSecurityException e)
            {
                throw new ETLException("Unable to access SecurityManger to get encryption key", e);
            }
        }
        else if ( info != null && info.getDefineKey() != null )
        {
            FileInputTaskComplexType.File.DefineKey define = info.getDefineKey();
            String type = String.valueOf(getGlobalVariableValue(define.getType(), task));
            String key = String.valueOf(getGlobalVariableValue(define.getKey(), task));
            String pfile = String.valueOf(getGlobalVariableValue(define.getPublicKeyFile(), task));
            String sfile = String.valueOf(getGlobalVariableValue(define.getSecretKeyFile(), task));
            String pass = String.valueOf(getGlobalVariableValue(define.getPassphrase(), task));

            encryptinfo = new EncryptionObject(new NamedKey("", ""), type, key, pfile, sfile, pass);
        }

        // Indicate when an encryption object is needed
        if ( encryptinfo != null )
        {
            Logger.getLogger(ConnectionFactory.class).info("Using encryption info: " + encryptinfo);
        }
        
        return encryptinfo;
    }

    public static EncryptionObject getEncryptionObject(MultipleFileInputTaskComplexType.File info, AbstractTask task)
        throws ETLException
    {
        EncryptionObject encryptinfo = null;
        if ( info != null && info.getDecryptionKey() != null )
        {
            try
            {
                SecurityManager.initialize(null, task.getETLJobWrapper().getETLRoot());
                String appname = String.valueOf(getGlobalVariableValue(info.getDecryptionKey().getApplication(), task));
                String key = String.valueOf(getGlobalVariableValue(info.getDecryptionKey().getKey(), task));

                RegisterableApplication app = new RegisterableApplication(appname);
                NamedKey nkey = new NamedKey(appname, key);
                SecuredObjectManager secman = SecuredObjectManager.getInstance(app);
                encryptinfo = secman.getRegisteredObject(EncryptionObject.class, nkey);
                if ( encryptinfo == null )
                {
                    throw new ETLException("Encryption key not registered with security manager: " + nkey);
                }
            }
            catch (BbTSSecurityException e)
            {
                throw new ETLException("Unable to access SecurityManger to get encryption key", e);
            }
        }
        else if ( info != null && info.getDefineKey() != null )
        {
            MultipleFileInputTaskComplexType.File.DefineKey define = info.getDefineKey();
            String type = String.valueOf(getGlobalVariableValue(define.getType(), task));
            String key = String.valueOf(getGlobalVariableValue(define.getKey(), task));
            String pfile = String.valueOf(getGlobalVariableValue(define.getPublicKeyFile(), task));
            String sfile = String.valueOf(getGlobalVariableValue(define.getSecretKeyFile(), task));
            String pass = String.valueOf(getGlobalVariableValue(define.getPassphrase(), task));

            encryptinfo = new EncryptionObject(new NamedKey("", ""), type, key, pfile, sfile, pass);
        }

        // Indicate when an encryption object is needed
        if ( encryptinfo != null )
        {
            Logger.getLogger(ConnectionFactory.class).info("Using encryption info: " + encryptinfo);
        }
        
        return encryptinfo;
    }
        
    public static EncryptionObject getEncryptionObject(FileOutputTaskComplexType.File info, AbstractTask task)
        throws ETLException
    {
        EncryptionObject encryptinfo = null;
        if ( info != null && info.getEncryptionKey() != null )
        {
            try
            {
                SecurityManager.initialize(null, task.getETLJobWrapper().getETLRoot());
                String appname = String.valueOf(getGlobalVariableValue(info.getEncryptionKey().getApplication(), task));
                String key = String.valueOf(getGlobalVariableValue(info.getEncryptionKey().getKey(), task));

                RegisterableApplication app = new RegisterableApplication(appname);
                NamedKey nkey = new NamedKey(appname, key);
                SecuredObjectManager secman = SecuredObjectManager.getInstance(app);
                encryptinfo = secman.getRegisteredObject(EncryptionObject.class, nkey);
                if ( encryptinfo == null )
                {
                    throw new ETLException("Encryption key not registered with security manager: " + nkey);
                }
            }
            catch (BbTSSecurityException e)
            {
                throw new ETLException("Unable to access SecurityManger to get encryption key", e);
            }
        }
        else if ( info != null && info.getDefineKey() != null )
        {
            FileOutputTaskComplexType.File.DefineKey define = info.getDefineKey();
            String type = String.valueOf(getGlobalVariableValue(define.getType(), task));
            String key = String.valueOf(getGlobalVariableValue(define.getKey(), task));
            String pfile = String.valueOf(getGlobalVariableValue(define.getPublicKeyFile(), task));
            String sfile = String.valueOf(getGlobalVariableValue(define.getSecretKeyFile(), task));
            String pass = String.valueOf(getGlobalVariableValue(define.getPassphrase(), task));

            encryptinfo = new EncryptionObject(new NamedKey("", ""), type, key, pfile, sfile, pass);
        }

        // Indicate when an encryption object is needed
        if ( encryptinfo != null )
        {
            Logger.getLogger(ConnectionFactory.class).info("Using encryption info: " + encryptinfo);
        }
        
        return encryptinfo;
    }
        
    public static EncryptionObject getEncryptionObject(MultipleFileOutputTaskComplexType.File info, AbstractTask task)
        throws ETLException
    {
        EncryptionObject encryptinfo = null;
        if ( info != null && info.getEncryptionKey() != null )
        {
            try
            {
                SecurityManager.initialize(null, task.getETLJobWrapper().getETLRoot());
                String appname = String.valueOf(getGlobalVariableValue(info.getEncryptionKey().getApplication(), task));
                String key = String.valueOf(getGlobalVariableValue(info.getEncryptionKey().getKey(), task));

                RegisterableApplication app = new RegisterableApplication(appname);
                NamedKey nkey = new NamedKey(appname, key);
                SecuredObjectManager secman = SecuredObjectManager.getInstance(app);
                encryptinfo = secman.getRegisteredObject(EncryptionObject.class, nkey);
                if ( encryptinfo == null )
                {
                    throw new ETLException("Encryption key not registered with security manager: " + nkey);
                }
            }
            catch (BbTSSecurityException e)
            {
                throw new ETLException("Unable to access SecurityManger to get encryption key", e);
            }
        }
        else if ( info != null && info.getDefineKey() != null )
        {
            MultipleFileOutputTaskComplexType.File.DefineKey define = info.getDefineKey();
            String type = String.valueOf(getGlobalVariableValue(define.getType(), task));
            String key = String.valueOf(getGlobalVariableValue(define.getKey(), task));
            String pfile = String.valueOf(getGlobalVariableValue(define.getPublicKeyFile(), task));
            String sfile = String.valueOf(getGlobalVariableValue(define.getSecretKeyFile(), task));
            String pass = String.valueOf(getGlobalVariableValue(define.getPassphrase(), task));

            encryptinfo = new EncryptionObject(new NamedKey("", ""), type, key, pfile, sfile, pass);
        }
    
        // Indicate when an encryption object is needed
        if ( encryptinfo != null )
        {
            Logger.getLogger(ConnectionFactory.class).info("Using encryption info: " + encryptinfo);
        }
        
        return encryptinfo;
    }

    public static EmailConnection getEmailConnection(EmailOutputTaskComplexType info, AbstractTask task)
        throws ETLException
    {
        EmailConnection emailinfo = null;
        if ( info != null && info.getServer() != null && info.getServer().getEmailConnection() != null )
        {
            try
            {
                SecurityManager.initialize(null, task.getETLJobWrapper().getETLRoot());
                String appname = String.valueOf(getGlobalVariableValue(info.getServer().getEmailConnection().getApplication(), task));
                String key = String.valueOf(getGlobalVariableValue(info.getServer().getEmailConnection().getKey(), task));

                RegisterableApplication app = new RegisterableApplication(appname);
                NamedKey nkey = new NamedKey(appname, key);
                ConnectionManager conman = ConnectionManager.getInstance(app);
                emailinfo = conman.getEmailConnection(nkey);
                if ( emailinfo == null )
                {
                    throw new ETLException("Email connection not registered with security manager: " + nkey);
                }
            }
            catch (BbTSSecurityException e)
            {
                throw new ETLException("Unable to access SecurityManger to get email connection", e);
            }
        }
        else if ( info != null && info.getServer() != null )
        {
            throw new UnsupportedOperationException("In-line email configuration for EmailConnections not supported yet.");
            /*try
            {
                EmailOutputTaskComplexType.Server define = info.getServer();
                String host = String.valueOf(getGlobalVariableValue(define.getHost(), task));
                String port = String.valueOf(getGlobalVariableValue(String.valueOf(define.getPort()), task));
                String user = String.valueOf(getGlobalVariableValue(define.getUsername(), task));
                String pass = String.valueOf(getGlobalVariableValue(define.getPassword(), task));
                String protocol = String.valueOf(getGlobalVariableValue(define.getProtocol(), task));
                boolean ssl = String.valueOf(getGlobalVariableValue(String.valuedefine.getSettings().isEnableSSL(), task));

                emailinfo = new TIAConnection("", "");
                emailinfo.setTIAHost(host);
                emailinfo.setTIAPort(port);
                emailinfo.setTerminalNumber(terminal);
                emailinfo.setVendorNumber(vendor);
                emailinfo.setEncryptionKey(encrypt);
            }
            catch (BbTSSecurityException e)
            {
                throw new ETLException("Unable to access SecurityManger to get tia connection", e);
            }*/
        }

        // Indicate when an tia connection is needed
        if ( emailinfo != null )
        {
            Logger.getLogger(ConnectionFactory.class).info("Using tia info: " + emailinfo);
        }
        
        return emailinfo;
    }

    public static TIAConnection getTIAConnection(TiaConfigComplexType info, AbstractTask task)
        throws ETLException
    {
        TIAConnection tiainfo = null;
        if ( info != null && info.getTiaConnection() != null )
        {
            try
            {
                SecurityManager.initialize(null, task.getETLJobWrapper().getETLRoot());
                String appname = String.valueOf(getGlobalVariableValue(info.getTiaConnection().getApplication(), task));
                String key = String.valueOf(getGlobalVariableValue(info.getTiaConnection().getKey(), task));

                RegisterableApplication app = new RegisterableApplication(appname);
                NamedKey nkey = new NamedKey(appname, key);
                ConnectionManager conman = ConnectionManager.getInstance(app);
                tiainfo = conman.getTIAConnection(nkey);
                if ( tiainfo == null )
                {
                    throw new ETLException("TIA connection not registered with security manager: " + nkey);
                }
            }
            catch (BbTSSecurityException e)
            {
                throw new ETLException("Unable to access SecurityManger to get tia connection", e);
            }
        }
        else if ( info != null && info.getDefineConnection() != null )
        {
            try
            {
                TiaConfigComplexType.DefineConnection define = info.getDefineConnection();
                String host = String.valueOf(getGlobalVariableValue(define.getHost(), task));
                String port = String.valueOf(getGlobalVariableValue(String.valueOf(define.getPort()), task));
                String terminal = String.valueOf(getGlobalVariableValue(define.getTerminalNumber(), task));
                String vendor = String.valueOf(getGlobalVariableValue(define.getVendorNumber(), task));
                String encrypt = String.valueOf(getGlobalVariableValue(define.getEncryptionKey(), task));

                tiainfo = new TIAConnection("", "");
                tiainfo.setTIAHost(host);
                tiainfo.setTIAPort(port);
                tiainfo.setTerminalNumber(terminal);
                tiainfo.setVendorNumber(vendor);
                tiainfo.setEncryptionKey(encrypt);
            }
            catch (BbTSSecurityException e)
            {
                throw new ETLException("Unable to access SecurityManger to get tia connection", e);
            }
        }

        // Indicate when an tia connection is needed
        if ( tiainfo != null )
        {
            Logger.getLogger(ConnectionFactory.class).info("Using tia info: " + tiainfo);
        }
        
        return tiainfo;
    }
    
    public static WebConnection getWebConnection(WebConfigComplexType info, AbstractTask task)
        throws ETLException
    {
        WebConnection webInfo = null;
        if ( info != null && info.getWebConnection() != null )
        {
            try
            {
                SecurityManager.initialize(null, task.getETLJobWrapper().getETLRoot());
                String appname = String.valueOf(getGlobalVariableValue(info.getWebConnection().getApplication(), task));
                String key = String.valueOf(getGlobalVariableValue(info.getWebConnection().getKey(), task));

                RegisterableApplication app = new RegisterableApplication(appname);
                NamedKey nkey = new NamedKey(appname, key);
                ConnectionManager conman = ConnectionManager.getInstance(app);
                webInfo = conman.getWebConnection(nkey);
                if ( webInfo == null )
                {
                    throw new ETLException("Web connection not registered with security manager: " + nkey);
                }
            }
            catch (BbTSSecurityException e)
            {
                throw new ETLException("Unable to access SecurityManger to get tia connection", e);
            }
        }
        else if ( info != null && info.getDefineConnection() != null )
        {
            try
            {
                WebConfigComplexType.DefineConnection define = info.getDefineConnection();
                if ( define.getBasic() != null )
                {
                    webInfo = new WebConnection("", "");
                    webInfo.setAuthType(WebConnection.AuthType.basic);
                    webInfo.setUsername(String.valueOf(getGlobalVariableValue(define.getBasic().getUsername(), task)));
                    webInfo.setPassword(String.valueOf(getGlobalVariableValue(define.getBasic().getPassword(), task)));
                }
                else if ( define.getOauth1() != null )
                {
                    webInfo = new WebConnection("", "");
                    webInfo.setAuthType(WebConnection.AuthType.oauth1);
                    webInfo.setTempTokenAuthorizationURI(String.valueOf(getGlobalVariableValue(define.getOauth1().getTempTokenURI(), task)));
                    webInfo.setRealTokenAuthorizationURI(String.valueOf(getGlobalVariableValue(define.getOauth1().getRealTokenURI(), task)));
                    webInfo.setConsumerKey(String.valueOf(getGlobalVariableValue(define.getOauth1().getConsumerKey(), task)));
                    webInfo.setConsumerSecret(String.valueOf(getGlobalVariableValue(define.getOauth1().getConsumerSecret(), task)));
                    webInfo.setSignatureMethod(String.valueOf(getGlobalVariableValue(define.getOauth1().getSignatureMethod(), task)));
                }
            }
            catch (IllegalArgumentException e)
            {
                throw new ETLException("Unable to access SecurityManger to get web connection", e);
            }
            catch (URISyntaxException e)
            {
                throw new ETLException("Invalid URI", e);
            }
        }

        // Indicate when a web connection is needed
        if ( webInfo != null )
        {
            Logger.getLogger(ConnectionFactory.class).info("Using web info: " + webInfo);
        }
        
        return webInfo;
    }

    protected static Object getGlobalVariableValue(String text, AbstractTask task)
        throws ETLException
    {
        if ( task != null )
        {
            return task.getGlobalVariableValue(text);
        }
        else
        {
            return text;
        }
    }
}
