/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.wrapper;

import com.blackboard.services.etl.ETLConfiguration;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.EtlJob;
import com.blackboard.services.etl.jaxb.FileOutputTaskComplexType;
import com.blackboard.services.etl.jaxb.ObjectFactory;
import com.blackboard.services.etl.process.Processor;
import com.blackboard.services.etl.tasks.AbstractTask;
import com.blackboard.services.etl.tasks.ChangeFilter;
import com.blackboard.services.etl.tasks.DataMapping;
import com.blackboard.services.etl.tasks.DataSorter;
import com.blackboard.services.etl.tasks.EmailAdapter;
import com.blackboard.services.etl.tasks.FileAdapter;
import com.blackboard.services.security.ConnectionManager;
import com.blackboard.services.security.exception.BbTSSecurityException;
import com.blackboard.services.security.object.EmailConnection;
import com.blackboard.services.security.object.NamedKey;
import com.blackboard.services.security.object.RegisterableApplication;
import com.blackboard.services.utils.BeanScript;
import com.blackboard.services.utils.VariableReplacement;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author crusnak
 */
public class DataArchiver
{
    public static final String EMAIL_DATA_NAME =    "MAILER";
    public static final String ARCHIVE_DIR =        "archive";
    private static final String LOCK_EXT =          ".LOCK";

    private static char nameSeparator = '_';
    private static char replaceChar = '-';
    private static ArrayList<Character> disallowed = new ArrayList();
    static
    {
        disallowed.add('<');
        disallowed.add('>');
        disallowed.add(':');
        disallowed.add('"');
        disallowed.add('/');
        disallowed.add('\\');
        disallowed.add('|');
        disallowed.add('?');
        disallowed.add('*');
    }

    private ETLJobWrapper wrapper = null;
    private boolean archiveSnapshot = false;
    private boolean archiveTaskData = false;
    private int maxArchivedJobs = -1;
    private int purgeSince = -1;
    private FileAdapter fileOutput = null;
    private EmailAdapter emailOutput = null;
    private Logger logger = null;
    private File lockFile = null;
    private File archiveDir = null;

    public DataArchiver(EtlJob.DataArchival da, EtlJob.GlobalVariables gvar, ETLJobWrapper w)
        throws ETLException
    {
        logger = Logger.getLogger(this.getClass());
        wrapper = w;
        setArchiveDirectory(gvar, w);
        setArchiver(da);
        setEmailAdapter();
    }

    public void destroy()
    {
        logger.info("Cleaning up DataArchiver");
        wrapper = null;
        logger = null;
        fileOutput = null;
        emailOutput = null;
    }
    
    private void setArchiver(EtlJob.DataArchival da)
        throws ETLException
    {
        if ( da != null )
        {
            archiveSnapshot = da.isArchiveSnapshotData();
            archiveTaskData = da.isArchiveTaskData();
            
            if ( da.getMaxArchivedJobs() != null )
            {
                maxArchivedJobs = da.getMaxArchivedJobs();
            }
            else if ( da.getPurgeArchivesSince() != null )
            {
                purgeSince = da.getPurgeArchivesSince();
            }

            if ( da.getTaskDataOutput() != null )
            {
                ObjectFactory factory = new ObjectFactory();
                FileOutputTaskComplexType ct = factory.createFileOutputTaskComplexType();
                FileOutputTaskComplexType.OutputData out = factory.createFileOutputTaskComplexTypeOutputData();

                out.setOutputAll(true);
                out.setWriteHeader(true);
                out.setDelimiter(da.getTaskDataOutput().getDelimiter());
                out.setType("delimitted");
                ct.setOutputData(out);
                
                fileOutput = new FileAdapter(ct, wrapper.getETLRoot(), true, da.getTaskDataOutput().isCreateEmptyOutput());
            }
            else
            {
                fileOutput = new FileAdapter(new RelationalContainer(null), null);
            }
        }
        else
        {
            fileOutput = new FileAdapter(new RelationalContainer(null), null);
        }
    }

    private void setArchiveDirectory(EtlJob.GlobalVariables gvar, ETLJobWrapper w)
        throws ETLException
    {
        File root = new File(w.getETLRoot(), ARCHIVE_DIR);
        File base = new File(root, w.getJobName());
        //DataContainer global = w.getGlobalVariableData();
        if ( gvar != null )
        {
            String name = "";
            ArrayList names = new ArrayList();
            List<String> args = wrapper.getJobArgumentNames();
            for ( String colname: args )
            {
                DataCell var = wrapper.getGlobalVariable(colname);
                if ( var != null && var.getColumn().getDataType() != DataTypes.JSON && var.getColumn().getDataType() != DataTypes.XML )
                {
                    names.add(var.getValue());
                }
            }
            
            name = escapeListForFilename(names);

            /*for ( EtlJob.GlobalVariables.Variable var: gvar.getVariable() )
            {
                // Only adding job argument variables to archive directory since only
                // those can be modified
                if ( var.isJobArgument() )
                {
                    args.add(var.getName());
                }
            }

            if ( global.getMappedData().size() > 0 )
            {
                Map<String,DataCell> data = global.getMappedData().get(0);

                // Iterate over each job argument to add it's value to the filename
                for ( int i=0; i<args.size(); i++ )
                {
                    String colname = args.get(i);
                    if ( data.get(colname) != null && data.get(colname).getValue() != null )
                    {
                        names.add(data.get(colname).getValue());
                    }
                }

                name = escapeListForFilename(names);
            }*/

            lockFile = new File(w.getETLRoot(), w.getJobName() + "_" + name + LOCK_EXT);
            archiveDir = new File(base, name);
        }
        else
        {
            lockFile = new File(w.getETLRoot(), w.getJobName() + LOCK_EXT);
            archiveDir = base;
        }

        try
        {
            System.out.println("ArchiveDirectory: " + archiveDir.getCanonicalPath());
            logger.info("ArchiveDirectory: " + archiveDir.getCanonicalPath());
        }
        catch (IOException e) {}
        
    }

    private void setEmailAdapter()
        throws ETLException
    {
        ETLConfiguration config = wrapper.getConfiguration();

        NamedKey key = config.getEmailConnectionKey();
        if ( key != null )
        {
            EmailConnection emailInfo = null;
            try
            {
                com.blackboard.services.security.SecurityManager.initialize(null, wrapper.getETLRoot());
                RegisterableApplication app = new RegisterableApplication(key.getName());
                ConnectionManager conman = ConnectionManager.getInstance(app);
                emailInfo = conman.getEmailConnection(key);
                if ( emailInfo == null )
                {
                    throw new ETLException("Email connection not registered with security manager, please reconfigure the integration: " + key);
                }
            }
            catch (BbTSSecurityException e)
            {
                throw new ETLException("Unable to access SecurityManger to get email connection", e);
            }
            
            emailOutput = new EmailAdapter(emailInfo, EMAIL_DATA_NAME);
        }
        
        /* ObjectFactory factory = new ObjectFactory();
        EmailOutputTaskComplexType.Server.Settings settings = factory.createEmailOutputTaskComplexTypeServerSettings();
        settings.setStartTLS(config.isStartTLSEnabled());
        settings.setSendPartial(config.isSendPartial());
        settings.setEnableSSL(config.isSSLEnabled());

        EmailOutputTaskComplexType.Server server = factory.createEmailOutputTaskComplexTypeServer();
        server.setHost(config.getSmtpHost());
        server.setPort(config.getSmtpPort());
        server.setUsername(config.getMailUsername());
        server.setPassword(config.getMailPassword());
        server.setSettings(settings);

        EmailOutputTaskComplexType.Recipients ctr = factory.createEmailOutputTaskComplexTypeRecipients();
        EmailOutputTaskComplexType ct = factory.createEmailOutputTaskComplexType();
        ct.setAttachData(false);
        ct.setIterate(false);
        ct.setServer(server);
        ct.setRecipients(ctr);
        
        emailOutput = new EmailAdapter(ct, wrapper);*/
    }

    public boolean containsBeanScript(String input)
    {
        return BeanScript.embeddedPattern.matcher(input).find();
    }
    
    public String processEmbeddedBeanScript(String input)
       throws ETLException
    {
        String value = input;
        logger.trace("BeanScript input: " + input);
        
        if ( value != null )
        {
            Matcher m = BeanScript.embeddedPattern.matcher(input);
            ArrayList<VariableReplacement> indices = new ArrayList();
            Map<String,Object> gvmap = new HashMap();
            for ( Map.Entry<String,DataCell> entry: wrapper.getGlobalVariableData().getMappedData().get(0).entrySet() )
            {
                gvmap.put(entry.getKey(), entry.getValue().getValue());
            }
            
            logger.trace("GVmap: " + gvmap);
            
            while ( m.find() )
            {
                value = "";
                VariableReplacement replace = new VariableReplacement(m.start(), m.end(), m.group(1), gvmap, false);
                indices.add(replace);
            }

            logger.trace("Indices: " + indices);
            
            Iterator<VariableReplacement> it = indices.iterator();
            int previdx = 0;
            while ( it.hasNext() )
            {
                VariableReplacement replace = it.next();
                value += input.substring(previdx, replace.getStartIndex());
                value += replace.getReplacementText(true);
                previdx = replace.getEndIndex();
            }

            if ( indices.size() > 0 )
            {
                value += input.substring(previdx);
            }

            logger.trace("BeanScript output: " + value);
        }
        
        return value;
    }
    
    public String processEmbeddedBeanScript(String input, AbstractTask t)
       throws ETLException
    {
        String value = input;
        logger.trace("BeanScript input: " + input);
        
        if ( value != null )
        {
            Matcher m = BeanScript.embeddedPattern.matcher(input);
            ArrayList<VariableReplacement> indices = new ArrayList();

            while ( m.find() )
            {
                logger.debug(m.group(1) + ", " + m.start() + ", " + m.end());
                value = "";
                VariableReplacement replace = new VariableReplacement(m.start(), m.end(), m.group(1), new HashMap(), false);
                indices.add(replace);
            }

            logger.trace("Indices: " + indices);
            
            Iterator<VariableReplacement> it = indices.iterator();
            int previdx = 0;
            while ( it.hasNext() )
            {
                VariableReplacement replace = it.next();
                value += input.substring(previdx, replace.getStartIndex());
                value += t.getGlobalVariableValue(replace.getBeanText());
                previdx = replace.getEndIndex();
            }

            if ( indices.size() > 0 )
            {
                value += input.substring(previdx);
            }

            logger.trace("BeanScript output: " + value);
        }
        
        return value;
    }

    public static String processEmbeddedBeanScript(String input, Map<String,DataCell> map, char c)
       throws ETLException
    {
        String value = input;
        //logger.trace("BeanScript input: " + input + ", char=" + c);
        
        if ( value != null )
        {
            Matcher m = BeanScript.getEmbeddedPattern(c).matcher(input);
            ArrayList<VariableReplacement> indices = new ArrayList();
            Map<String,Object> gvmap = new HashMap();
            for ( Map.Entry<String,DataCell> entry: map.entrySet() )
            {
                gvmap.put(entry.getKey(), entry.getValue().getValue());
            }
            
            //logger.trace("GVmap: " + gvmap);
            
            while ( m.find() )
            {
                value = "";
                VariableReplacement replace = new VariableReplacement(m.start(), m.end(), m.group(1), gvmap, false, c);
                indices.add(replace);
            }

            //logger.trace("Indices: " + indices);
            
            Iterator<VariableReplacement> it = indices.iterator();
            int previdx = 0;
            while ( it.hasNext() )
            {
                VariableReplacement replace = it.next();
                value += input.substring(previdx, replace.getStartIndex());
                value += replace.getReplacementText(true);
                previdx = replace.getEndIndex();
            }

            if ( indices.size() > 0 )
            {
                value += input.substring(previdx);
            }

            //logger.trace("BeanScript output: " + value);
        }
        
        return value;
    }

    public static String escapeListForFilename(Collection names)
    {
        String filename = "";
        if ( names != null )
        {
            Iterator it = names.iterator();
            while ( it.hasNext() )
            {
                String name = String.valueOf(it.next()).replaceAll(" ", "");
                for ( char c: disallowed )
                {
                    name = name.replace(c, replaceChar);
                }

                filename += name;
                if ( it.hasNext() )
                {
                    filename += nameSeparator;
                }
            }
        }

        return filename;
    }

    public File getArchiveDirectory()
    {
        return archiveDir;
    }

    public File getRunningArchiveDirectory(String jobnum)
    {
        return new File(archiveDir, jobnum);
    }

    public File getPreviousArchiveDirectory(String jobnum)
    {
        File dir = null;
        File jobDir = getRunningArchiveDirectory(jobnum);
        ArrayList<File> jobs = new ArrayList();

        for ( File job: archiveDir.listFiles() )
        {
            if ( job.isDirectory() )
            {
                jobs.add(job);
            }
        }

        Collections.sort(jobs);
        int index = jobs.indexOf(jobDir);
        if ( index >= 1 )
        {
            dir = jobs.get(index-1);
        }

        return dir;
    }

    public File getMostRecentArchiveDirectory()
    {
        File dir = null;
        ArrayList<File> jobs = new ArrayList();

        for ( File job: archiveDir.listFiles() )
        {
            if ( job.isDirectory() )
            {
                jobs.add(job);
            }
        }

        Collections.sort(jobs);
        dir = jobs.get(jobs.size()-1);

        return dir;
    }
    
    public FileAdapter getOutputAdapter()
    {
        return fileOutput;
    }

    public boolean isSnapshotDataToBeArchived()
    {
        return archiveSnapshot;
    }

    public boolean isTaskDataToBeArchived()
    {
        return archiveTaskData;
    }

    /*public void writeSnapshotDataToFile(File file)
        throws BbTSSecurityException, IOException, GeneralSecurityException
    {
        if ( archiveSnapshot )
        {
            EncryptionManager eman = EncryptionManager.getInstance();
            Cipher cipher = BbKeyGenerator.getSymmetricCipher(Cipher.ENCRYPT_MODE, eman.getSecretKey());
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
            CipherOutputStream cos = new CipherOutputStream(bos, cipher);
            ObjectOutputStream oos = new ObjectOutputStream(cos);

            try
            {
                oos.writeObject(wrapper.data);
                oos.flush();
            }
            finally
            {
                oos.close();
            }
        }
    }

    public Map<String,DataContainer> readSnapshotDataFromFile(File file)
        throws BbTSSecurityException, IOException, GeneralSecurityException
    {
        EncryptionManager eman = EncryptionManager.getInstance();
        Cipher cipher = BbKeyGenerator.getSymmetricCipher(Cipher.DECRYPT_MODE, eman.getSecretKey());
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
        CipherInputStream cis = new CipherInputStream(bis, cipher);
        ObjectInputStream ois = new ObjectInputStream(cis);

        try
        {
            wrapper.data = (HashMap<String,DataContainer>)ois.readObject();
        }
        catch (ClassNotFoundException e) {throw new GeneralSecurityException(e);}
        finally
        {
            ois.close();
        }

        return wrapper.data;
    }*/

    public DataSet readDataFromBinaryFile(File file)
        throws ETLException, IOException, ClassNotFoundException
    {
        DataSet ds = null;
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
        ObjectInputStream ois = new ObjectInputStream(bis);
        ds = (DataSet)ois.readObject();
        ois.close();
        return ds;
    }

    public void writeDataToBinaryFile(DataSet ds, File file)
        throws ETLException, IOException
    {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(ds);
        oos.flush();
        oos.close();
    }

    public void writeDataToTextFile(DataSet ds, File file)
        throws ETLException, IOException
    {
        logger.trace("Data set before file output: " + ds);
        if ( ds.getType() == DataSet.Type.relational )
        {
            fileOutput.setData((RelationalContainer)ds);
            fileOutput.setTextFile(file);
            fileOutput.writeFile(null);
            fileOutput.setData(null);
        }
        else
        {
            throw new UnsupportedOperationException("FileOutput for hierarchical data sets not supported yet");
        }
        logger.trace("Data set after file output: " + ds);
    }

    public void emailCompletedJobInfo(String jobnum, File logfile, File statsfile)
        throws ETLException
    {
        ETLConfiguration config = wrapper.getConfiguration();

        String names = "";
        for ( String arg: wrapper.getJobArgumentNames() )
        {
            DataCell val = wrapper.retrieveGlobalVariable(arg);
            names += "\n\t" + arg + " = " + (val != null?val.getValue():null);
        }
        wrapper.setGlobalVariable(Processor.GVAR_JOB_ARGS, new DataCell(new DataColumn(DataTypes.STRING, Processor.GVAR_JOB_ARGS), names));

        // Only send job info via email if configured
        if ( config.isEmailSentForJobInfo() )
        {
            emailOutput.setSender(config.getEmailAddressSender());
            emailOutput.setRecipients(config.getEmailAddressRecipients());

            String subject = processEmbeddedBeanScript(config.getJobInfoEmailSubject());
            String message = processEmbeddedBeanScript(config.getJobInfoEmailBody()); 
            
            emailOutput.setEmailSubject(subject);
            emailOutput.setEmailBody(message);

            // Only attach row data if configured to do so
            if ( config.isJobInfoLogFileAttached() || config.isJobInfoStatisticsFileAttached() )
            {
                ArrayList<File> attachments = new ArrayList();
                if ( config.isJobInfoLogFileAttached() && logfile != null && logfile.exists() )
                {
                    attachments.add(logfile);
                }
                if ( config.isJobInfoStatisticsFileAttached() && statsfile != null && statsfile.exists() )
                {
                    attachments.add(statsfile);
                }

                emailOutput.setFileAttachments(attachments);
            }

            // Send email message
            emailOutput.email(jobnum);
        }
    }
    
    public void emailRowLevelErrors(String jobnum, List<File> errorFiles)
        throws ETLException
    {
        ETLConfiguration config = wrapper.getConfiguration();

        String names = "";
        for ( String arg: wrapper.getJobArgumentNames() )
        {
            DataCell val = wrapper.retrieveGlobalVariable(arg);
            names += "\n\t" + arg + " = " + (val != null?val.getValue():null);
        }
        wrapper.setGlobalVariable(Processor.GVAR_JOB_ARGS, new DataCell(new DataColumn(DataTypes.STRING, Processor.GVAR_JOB_ARGS), names));

        String tasks = "";
        for ( File f: errorFiles )
        {
            String taskName = f.getName().substring(0, f.getName().indexOf(Processor.TXT_EXT));
            tasks += "\t" + taskName + "\n";
        }
        wrapper.setGlobalVariable(Processor.GVAR_ERROR_TASKS, new DataCell(new DataColumn(DataTypes.STRING, Processor.GVAR_ERROR_TASKS), tasks));

        // Only send error messages via email if configured
        if ( config.isEmailSentForRowErrors() )
        {
            logger.info("Attempting to email row level errors to recipient list: " + config.getEmailAddressRecipientString());
            
            emailOutput.setSender(config.getEmailAddressSender());
            emailOutput.setRecipients(config.getEmailAddressRecipients());

            String subject = processEmbeddedBeanScript(config.getRowErrorEmailSubject());
            String message = processEmbeddedBeanScript(config.getRowErrorEmailBody()); 
            
            /*String subject = wrapper.getJobName() + " encountered row level errors";
            String message = "Row level errors were encountered while running ETL job: " + wrapper.getJobName() + " #" + jobnum;
            message += "\n\nThe following job arguments were used to initialize the job: \n";

            for ( String arg: wrapper.getJobArgumentNames() )
            {
                DataCell val = wrapper.retrieveGlobalVariable(arg);
                message += "\n\t" + arg + " = " + (val != null?val.getValue():null);
            }

            message += "\n\nThe following tasks had errors: \n\n";
            for ( File f: errorFiles )
            {
                String taskName = f.getName().substring(0, f.getName().indexOf(Processor.TXT_EXT));
                message += "\t" + taskName + "\n";
            }

            message += "\n\nPlease see the attached files containing the row level errors for details.";*/

            emailOutput.setEmailSubject(subject);
            emailOutput.setEmailBody(message);

            // Only attach row data if configured to do so
            if ( config.isRowErrorDataAttached() )
            {
                emailOutput.setFileAttachments(errorFiles);
            }

            // TODO: need to have this be added to email config to set flag to send empty job completion emails or not
            emailOutput.setSendEmpty(config.isJobInfoSendEmailEmpty());

            // Send email message
            emailOutput.email(jobnum);
        }
    }

    public void emailFatalError(String jobnum, File logfile, Throwable t)
        throws ETLException
    {
        ETLConfiguration config = wrapper.getConfiguration();
        File newlog = archiveLogFile(jobnum, logfile);

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        t.printStackTrace(pw);
        wrapper.setGlobalVariable(Processor.GVAR_EXCEPTION, new DataCell(new DataColumn(DataTypes.STRING, Processor.GVAR_EXCEPTION), sw.toString()));

        String names = "";
        for ( String arg: wrapper.getJobArgumentNames() )
        {
            DataCell val = wrapper.retrieveGlobalVariable(arg);
            names += "\n\t" + arg + " = " + (val != null?val.getValue():null);
        }
        wrapper.setGlobalVariable(Processor.GVAR_JOB_ARGS, new DataCell(new DataColumn(DataTypes.STRING, Processor.GVAR_JOB_ARGS), names));

        // Only send error messages via email if configured
        if ( config.isEmailSentForFatalErrors() )
        {
            logger.info("Attempting to email row level errors to recipient list: " + config.getEmailAddressRecipientString());

            ArrayList<File> attachments = new ArrayList();
            emailOutput.setSender(config.getEmailAddressSender());
            emailOutput.setRecipients(config.getEmailAddressRecipients());

            String subject = processEmbeddedBeanScript(config.getFatalErrorEmailSubject());
            String message = processEmbeddedBeanScript(config.getFatalErrorEmailBody());
            message = message.replaceAll("\\\\n", "\n"); 
           
            /*String subject = wrapper.getJobName() + " FAILED to complete";
            String message = "A fatal error occurred while running ETL job: " + wrapper.getJobName() + " #" + jobnum;
            message += "\n\nThe following job arguments were used to initialize the job: \n";

            for ( String arg: wrapper.getJobArgumentNames() )
            {
                DataCell val = wrapper.retrieveGlobalVariable(arg);
                message += "\n\t" + arg + " = " + (val != null?val.getValue():null);
            }

            message += "\n\nThe following exception was thrown causing an unrecoverable error: \n\n";
            //StringWriter sw = new StringWriter();
            //PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            wrapper.setGlobalVariable(Processor.GVAR_EXCEPTION, new DataCell(new DataColumn(DataTypes.STRING, Processor.GVAR_EXCEPTION), sw.toString()));
            
            message += sw.toString();*/

            emailOutput.setEmailSubject(subject);
            emailOutput.setEmailBody(message);

            // Only attach the log file if configured to do so
            if ( config.isFatalErrorLogFileAttached() )
            {
                // Attach the log file
                if ( newlog.exists() )
                {
                    attachments.add(logfile);
                    emailOutput.setFileAttachments(attachments);
                }
                else if ( logfile.exists() )
                {
                    attachments.add(logfile);
                    emailOutput.setFileAttachments(attachments);
                }
            }
            
            // Send email message
            emailOutput.email(jobnum);
        }
    }

    public void archiveErrorData(String jobNumber)
        throws ETLException
    {
        Date now = new Date();
        ArrayList<File> files = new ArrayList();

        try
        {
            Map<String,RelationalContainer> errorMap = wrapper.getErrorDataMap();
            if ( !errorMap.isEmpty() )
            {
                logger.warn("Job run encountered row level errors");
                System.out.println("\nJob run encountered row level errors\n");
                
                File archiveDir = getRunningArchiveDirectory(jobNumber);
                File errorDir = new File(archiveDir, Processor.ERROR_DIR);
                errorDir.mkdirs();

                for ( Map.Entry<String,RelationalContainer> entry: errorMap.entrySet() )
                {
                    String errorTask = entry.getKey();
                    RelationalContainer errorData = entry.getValue();
                    File errorFile = new File(errorDir, errorTask + Processor.TXT_EXT);
                    files.add(errorFile);

                    writeDataToTextFile(errorData, errorFile);
                    logger.info("Error data for task '" + errorTask + "' contains " + errorData.rowCount() + " rows and is archived at: " + errorFile.getCanonicalPath());
                    System.out.println("Error data for task '" + errorTask + "' contains " + errorData.rowCount() + " rows and is archived at: " + errorFile.getCanonicalPath() + "\n");
                }

                // Email row level errors if configured
                emailRowLevelErrors(jobNumber, files);
            }
        }
        catch (IOException e)
        {
            throw new ETLException("Unable to archive error data", e);
        }

        if ( !wrapper.getErrorDataMap().isEmpty() )
        {
            logger.info("Archival of errors took " +
                        (double)(new Date().getTime() - now.getTime())/1000.0 +
                        " seconds to process.");
        }
    }

    public void archiveMasterData(String jobNumber)
        throws ETLException
    {
        Date now = new Date();

        try
        {
            if ( isTaskDataToBeArchived() )
            {
                List<String> names = wrapper.getMasterDataNames();
                File archiveDir = getRunningArchiveDirectory(jobNumber);
                File dataDir = new File(archiveDir, Processor.DATA_DIR);

                for ( String name: names )
                {
                    RelationalContainer md = wrapper.getMasterData(name);
                    String filename = md.getDataName();
                    File mdFile = new File(dataDir, filename + Processor.TXT_EXT);

                    writeDataToTextFile(md, mdFile);
                    logger.info("Master data '" + filename + "' archived at: " + dataDir.getCanonicalPath());
                }
            }
        }
        catch (IOException e)
        {
            throw new ETLException("Unable to archive master data", e);
        }

        logger.info("Archival of master data took " +
                    (double)(new Date().getTime() - now.getTime())/1000.0 +
                    " seconds to process.");
    }

    public void archiveTaskData(String jobNumber, String taskName)
        throws ETLException
    {
        archiveTaskData(jobNumber, wrapper.retrieveNamedDataTask(taskName));
    }
    
    public void archiveTaskData(String jobNumber, AbstractTask task)
        throws ETLException
    {
        Date now = new Date();

        try
        {

            // TODO: data archival must output the current state of all data from previous tasks
            // as well as the current task data.  For data mapping tasks, the data of the target
            // must also be output.  Also, for file input tasks, the read file must be placed in the
            // archive directory.

            //logger.debug("Archiving data for the following tasks: " + taskPath);

            File archiveDir = getRunningArchiveDirectory(jobNumber);
            archiveDir.mkdirs();

            File dataDir = new File(archiveDir, Processor.DATA_DIR);
            if ( isSnapshotDataToBeArchived() || isTaskDataToBeArchived() )
            {
                dataDir.mkdirs();
            }

            File datFile = new File(dataDir, task.getTaskName() + Processor.DAT_EXT);
            File txtFile = new File(dataDir, task.getTaskName() + Processor.TXT_EXT);

            // TODO: maybe i will output all data from all tasks regardless if there's anything there that
            // way loading the binary data puts the system in a state where it can continue easily

            // Only archive snapshot data stated in job description
            /*if ( archiver.isSnapshotDataToBeArchived() )
            {
                archiver.writeSnapshotDataToFile(datFile);
                logger.info("Snapshot data archived as binary at: " + datFile.getCanonicalPath());
            }*/

            // Only archive task data data stated in job description
            if ( isTaskDataToBeArchived() )
            {
                DataSet data = task.getData();
                switch (task.getClassType())
                {
                    case DataFilter:
                        // How do I want to handle data filter data? Output all data based on
                        // target output task?
                        break;
                    case DataMapping:
                        data = wrapper.retrieveNamedData(((DataMapping)task).getTarget());
                        if ( data == null )
                        {
                            data = task.getData();
                        }
                        break;
                    case DataSorter:
                        data = wrapper.retrieveNamedData(((DataSorter)task).getTarget());
                        if ( data == null )
                        {
                            data = task.getData();
                        }
                        break;
                }

                writeDataToTextFile(data, txtFile);
                logger.info("Task data archived as text at: " + txtFile.getCanonicalPath());
            }

            // Output all the data containers
            // Not needed now that data is archived to file
            /*for ( DataContainer dc: wrapper.getDataContainerMap().values() )
            {
                logger.debug(dc);
            }*/

        }
        /*catch (GeneralSecurityException e)
        {
            throw new ETLException("Error intializing cipher for encrypting archive data: " + task.getTaskName(), e);
        }
        catch (BbTSSecurityException e)
        {
            throw new ETLException("Error encrypting snapshot data for archiving: " + task.getTaskName(), e);
        }*/
        catch (IOException e)
        {
            throw new ETLException("Unable to archive task data for: " + task.getTaskName(), e);
        }

        if ( isSnapshotDataToBeArchived() || isTaskDataToBeArchived() )
        {
            logger.info("Archival of Task '" + task.getTaskName() + "' took " +
                        (double)(new Date().getTime() - now.getTime())/1000.0 +
                        " seconds to process.");
        }
    }

    public File outputDataStatistics(String jobNumber)
        throws ETLException
    {
        Date now = new Date();
        DataStatistics stats = wrapper.getDataStatistics();
        File statsFile = null;

        try
        {
            RelationalContainer data = stats.getData();
            File archiveDir = getRunningArchiveDirectory(jobNumber);
            statsFile = new File(archiveDir, stats.getJobName() + "-statistics" + Processor.TXT_EXT);
            writeDataToTextFile(data, statsFile);
        }
        catch (IOException e)
        {
            throw new ETLException("Unable to error data", e);
        }

        logger.info("Output of data statistics took " +
                    (double)(new Date().getTime() - now.getTime())/1000.0 +
                    " seconds to process.");
        
        return statsFile;
    }

    public File archiveLogFile(String jobnum, File logfile)
    {
        File archiveDir = getRunningArchiveDirectory(jobnum);
        File destfile = new File(archiveDir, wrapper.getJobName() + Processor.LOG_EXT);

        logger.info("Attempting to archive log file: " + logfile);

        try
        {
            if ( logfile != null && logfile.exists() )
            {
                logger.info("Copying to: " + destfile);
                FileUtils.copyFile(logfile, destfile);
            }
        }
        catch (IOException e)
        {
            logger.warn("Unable to archive log file to: " + destfile);
        }

        return destfile;
    }

    public void removeOldArchives()
    {
        if ( maxArchivedJobs > -1 )
        {
            if ( archiveDir != null && archiveDir.isDirectory() )
            {
                File[] archives = archiveDir.listFiles();

                // Encountered issues where directories are unable to be deleted and the loop below runs
                // indefinitely.
                for ( int i=0; i<archives.length-maxArchivedJobs; i++ )
                {
                    boolean deleted = FileUtils.deleteQuietly(archives[i]);
                    if ( deleted )
                    {
                        logger.info("Removed archived job: " + wrapper.getJobName() + "/" + archives[i].getName());
                    }
                    else
                    {
                        logger.warn("Unable to remove archived job: " + wrapper.getJobName() + "/" + archives[i].getName());
                    }
                }
            }
        }
        else if ( purgeSince > -1 )
        {
            if ( archiveDir != null && archiveDir.isDirectory() )
            {
                logger.info("Removing archived jobs more than " + purgeSince + " days from today");
                File[] archives = archiveDir.listFiles();
                Calendar now = Calendar.getInstance();

                // Encountered issues where directories are unable to be deleted and the loop below runs
                // indefinitely.
                for ( int i=0; i<archives.length; i++ )
                {
                    try
                    {
                        Calendar filedate = Calendar.getInstance();
                        filedate.setTime(wrapper.jobNumberFormat.parse(archives[i].getName()));
                        filedate.add(Calendar.DAY_OF_MONTH, purgeSince);
                        
                        // If the purge days added to the archive date is still less than the current date
                        // then the archive directory must be deleted
                        if ( filedate.getTimeInMillis() < now.getTimeInMillis() )
                        {
                            boolean deleted = FileUtils.deleteQuietly(archives[i]);
                            if ( deleted )
                            {
                                logger.info("Removed archived job: " + wrapper.getJobName() + "/" + archives[i].getName());
                            }
                            else
                            {
                                logger.warn("Unable to remove archived job: " + wrapper.getJobName() + "/" + archives[i].getName());
                            }
                        }
                    }
                    catch ( ParseException e)
                    {
                        logger.warn("Unable to convert archive directory into a job number date: " + archives[i].getName());
                    }
                }
            }
        }
    }

    private DataArchiver getArchiverForComparison()
    {
        DataArchiver archiver = null;
        if ( wrapper.getParent() == null )
        {
            archiver = wrapper.getDataArchiver();
            logger.info("Getting data archiver for comparison from current job wrapper: " + wrapper);
        }
        else
        {
            archiver = wrapper.getFirstAncestor().getDataArchiver();
            //archiver = wrapper.getDataArchiver();
            logger.info("Getting data archiver for comparison from ancestral job wrapper: " + wrapper.getFirstAncestor());
        }
        
        return archiver;
    }

    public void copyPreviousComparisonData(String jobnum)
    {
        Collection changeTasks = wrapper.getNestedJobTaskNamesByType(ChangeFilter.class);
        DataArchiver arch = getArchiverForComparison();
        logger.debug("Defined change filters: " + changeTasks);
        if ( changeTasks.size() > 0 )
        {
            logger.info("Retrieving comparison data from previous job run using current job number: " + jobnum);
            File archDir = arch.getRunningArchiveDirectory(jobnum);
            File prevDir = arch.getPreviousArchiveDirectory(jobnum);

            logger.debug("Current: " + archDir);
            logger.debug("Previous: " + prevDir);
            if ( prevDir != null )
            {
                File prevCDC = new File(prevDir, Processor.CDC_DIR);
                File archCDC = new File(archDir, Processor.CDC_DIR);
                try
                {
                    logger.debug("Copying previous comparison data from " + prevCDC + " to: " + archCDC);
                    FileUtils.copyDirectory(prevCDC, archCDC);
                }
                catch (IOException e) { logger.warn("Unable to load previous comparison data found, no data will be filtered out"); }
            }
        }
    }
    
    public boolean isJobLocked()
    {
        return lockFile.exists();
    }
    
    public String getLockedJobnum()
    {
        String jobnum = null;
        BufferedReader r = null;
        try
        {
            if ( isJobLocked() )
            {
                logger.debug("Attempting to read job: " + lockFile);
                r = new BufferedReader(new FileReader(lockFile));
                String read = "";
                while ( r.ready() )
                {
                    read += r.readLine();
                }
                r.close();
                jobnum = read;
            }
        }
        catch (IOException e)
        {
            logger.warn("Unable to read LOCK file", e);
        }
        
        return jobnum;
    }

    public void lockJob(String jobnum)
    {
        BufferedWriter w = null;
        try
        {
            logger.debug("Attempting to lock job: " + lockFile);
            lockFile.createNewFile();
            w = new BufferedWriter(new FileWriter(lockFile));
            w.write(jobnum);
        }
        catch (IOException e)
        {
            logger.warn("Unable to create LOCK file", e);
        }
        finally
        {
            try
            {
                if ( w != null )
                {
                    w.close();
                }
            }
            catch (IOException e)
            {
                logger.warn("Unable to close lock file after writing", e);
            }
        }
    }

    public void unlockJob(String jobnum)
    {
        BufferedReader r = null;
        try
        {
            logger.debug("Attempting to unlock job: " + lockFile);
            r = new BufferedReader(new FileReader(lockFile));
            String read = "";
            while ( r.ready() )
            {
                read += r.readLine();
            }
            r.close();
            if ( jobnum.equals(read) )
            {
                lockFile.delete();
            }
        }
        catch (IOException e)
        {
            logger.warn("Unable to delete LOCK file", e);
        }
    }
}
