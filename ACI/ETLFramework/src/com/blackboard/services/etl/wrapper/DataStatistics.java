/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.wrapper;

import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.tasks.AbstractTask;
import com.blackboard.services.etl.tasks.ETLTask;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author crusnak
 */
public class DataStatistics
{
    private static final String NAME_TASK =         "TaskName";
    private static final String NAME_IN =           "IncomingRows";
    private static final String NAME_OUT =          "OutgoingRows";
    private static final String NAME_ERR =          "ErrorRows";
    private static final String NAME_TIME =         "ExecutionTime (s)";

    private static DataColumn COL_TASK_NAME =       new DataColumn(DataTypes.STRING, NAME_TASK);
    private static DataColumn COL_IN_CNT =          new DataColumn(DataTypes.INT, NAME_IN);
    private static DataColumn COL_OUT_CNT =         new DataColumn(DataTypes.INT, NAME_OUT);
    private static DataColumn COL_ERR_CNT =         new DataColumn(DataTypes.INT, NAME_ERR);
    private static DataColumn COL_TIME =            new DataColumn(DataTypes.DOUBLE, NAME_TIME);


    private Logger logger = null;
    private ETLJobWrapper wrapper = null;
    private File archiveDir = null;
    private String jobName = "";
    private RelationalContainer data = null;
    private HashMap<String,Integer> tasks = new HashMap();

    // need to store for each task
    // - number of input rows
    // - number of output rows
    // - number of error rows
    // - task execution time

    public DataStatistics(String name, ETLJobWrapper w)
        throws ETLException
    {
        logger = Logger.getLogger(this.getClass());
        wrapper = w;
        jobName = name;
        archiveDir = wrapper.getDataArchiver().getArchiveDirectory();

        data = new RelationalContainer(name);
        data.addColumnDefinition(COL_TASK_NAME);
        data.addColumnDefinition(COL_IN_CNT);
        data.addColumnDefinition(COL_OUT_CNT);
        data.addColumnDefinition(COL_ERR_CNT);
        data.addColumnDefinition(COL_TIME);
    }
    
    public void destroy()
    {
        logger.info("Cleaning up DataStatistics");
        wrapper = null;
        data.clear();
        tasks.clear();
    }

    public String getJobName()
    {
        return jobName;
    }

    public File getArchiveDirectory()
    {
        return archiveDir;
    }

    public RelationalContainer getData()
    {
        return data;
    }

    private Map<String,DataCell> getTaskRow(String tname)
        throws ETLException
    {
        Map<String,DataCell> row = null;
        Integer idx = tasks.get(tname);
        if ( idx == null )
        {
            row = new HashMap<String,DataCell>();
            row.put(NAME_TASK, new DataCell(COL_TASK_NAME, tname));
            data.addDataRow(row);
            tasks.put(tname, data.rowCount()-1);
        }
        else
        {
            row = data.getMappedData().get(idx);
        }

        return row;
    }

    public void assignTaskInputRowCount(ETLTask task, int count)
        throws ETLException
    {
        Map<String,DataCell> row = getTaskRow(task.getTaskName());
        row.put(NAME_IN, new DataCell(COL_IN_CNT, count));
        int idx = tasks.get(task.getTaskName());
        data.setDataRow(row, idx);
    }

    public void assignTaskOutputRowCount(ETLTask task, int count)
        throws ETLException
    {
        Map<String,DataCell> row = getTaskRow(task.getTaskName());
        row.put(NAME_OUT, new DataCell(COL_OUT_CNT, count));
        int idx = tasks.get(task.getTaskName());
        data.setDataRow(row, idx);
    }

    public void assignTaskErrorRowCount(ETLTask task, int count)
        throws ETLException
    {
        Map<String,DataCell> row = getTaskRow(task.getTaskName());
        row.put(NAME_ERR, new DataCell(COL_ERR_CNT, count));
        int idx = tasks.get(task.getTaskName());
        data.setDataRow(row, idx);
    }

    public void assignTaskExecutionTime(ETLTask task, long millis)
        throws ETLException
    {
        double time = ((double)millis/1000.0);
        Map<String,DataCell> row = getTaskRow(task.getTaskName());
        row.put(NAME_TIME, new DataCell(COL_TIME, time));
        int idx = tasks.get(task.getTaskName());
        data.setDataRow(row, idx);
    }
}
