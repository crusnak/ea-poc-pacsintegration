/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.wrapper;

import com.blackboard.services.etl.ETLConfiguration;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.data.DataIndex;
import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.etl.data.LookupTable;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.tasks.AbstractTask;
import com.blackboard.services.etl.tasks.CustomProcess;
import com.blackboard.services.etl.tasks.DataMapping;
import com.blackboard.services.etl.tasks.DatabaseAdapter;
import com.blackboard.services.etl.tasks.ChangeFilter;
import com.blackboard.services.etl.tasks.DataAggregation;
import com.blackboard.services.etl.tasks.DataFilter;
import com.blackboard.services.etl.tasks.DataCorrelation;
import com.blackboard.services.etl.tasks.DefinedDataAdapter;
import com.blackboard.services.etl.tasks.FileAdapter;
import com.blackboard.services.etl.tasks.MasterDataTask;
import com.blackboard.services.etl.tasks.NullTask;
import com.blackboard.services.etl.tasks.SubJob;
import com.blackboard.services.etl.tasks.TIAAdapter;
import com.blackboard.services.etl.jaxb.AggregateDataComplexType;
import com.blackboard.services.etl.jaxb.ChangeFilterComplexType;
import com.blackboard.services.etl.jaxb.ControlTaskComplexType;
import com.blackboard.services.etl.jaxb.CorrelateDataComplexType;
import com.blackboard.services.etl.jaxb.CustomTaskComplexType;
import com.blackboard.services.etl.jaxb.DataComplexType;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.ErrorTaskComplexType;
import com.blackboard.services.etl.jaxb.EtlJob;
import com.blackboard.services.etl.jaxb.EtlTaskComplexType;
import com.blackboard.services.etl.jaxb.ExtractControlComplexType;
import com.blackboard.services.etl.jaxb.FilterTaskComplexType;
import com.blackboard.services.etl.jaxb.HierarchyMappingTaskComplexType;
import com.blackboard.services.etl.jaxb.IndexComplexType;
import com.blackboard.services.etl.jaxb.InputTaskComplexType;
import com.blackboard.services.etl.jaxb.MappingTaskComplexType;
import com.blackboard.services.etl.jaxb.OutputTaskComplexType;
import com.blackboard.services.etl.jaxb.SortDataComplexType;
import com.blackboard.services.etl.jaxb.SplitRowsComplexType;
import com.blackboard.services.etl.jaxb.SubJobComplexType;
import com.blackboard.services.etl.jaxb.SwitchTaskComplexType;
import com.blackboard.services.etl.jaxb.WebAPITaskComplexType;
import com.blackboard.services.etl.process.Processor;
import com.blackboard.services.etl.tasks.BinaryAdapter;
import com.blackboard.services.etl.tasks.ControlTask;
import com.blackboard.services.etl.tasks.DataSorter;
import com.blackboard.services.etl.tasks.ExpansionInterval;
import com.blackboard.services.etl.tasks.EmailAdapter;
import com.blackboard.services.etl.tasks.ErrorTask;
import com.blackboard.services.etl.tasks.ExtractControl;
import com.blackboard.services.etl.tasks.HierarchyMapping;
import com.blackboard.services.etl.tasks.RowSplitter;
import com.blackboard.services.etl.tasks.WebAPIDispatcher;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import org.apache.log4j.Logger;

/**
 * TODO: Need to provide accessors for the following:
 * TODO: getVariableByName
 * TODO: getMenuOptionsByName
 * TODO: loadMenuOptions
 * @author crusnak
 */
public class ETLJobWrapper
{
    public static final String TASK_START_KEY =             "<startTask>";
    public static final String TASK_END_KEY =               "<endTask>";
    public static final String MASTER_DATA_NAME =           "MASTER";
    public static final String GLOBAL_VAR_DATA_NAME =       "GLOBAL";
    public static final String TABLE_KEY_SEPARATOR =        "|";

    private ETLJobWrapper parent = null;
    private EtlJob job = null;
    private File jobfile = null;
    private File etlRoot = null;
    private ETLConfiguration config = null;
    private DataArchiver archiver = null;
    private DataStatistics stats = null;
    private RelationalContainer global = null;
    private ArrayList<String> arguments = new ArrayList();
    private LinkedHashMap<String,EtlJob.GlobalVariables.Variable> usage = new LinkedHashMap();
    private HashMap<String,LookupTable> tables = new HashMap();
    private HashMap<String,AbstractTask> tasks = new HashMap();
    private HashMap<String,RelationalContainer> errors = new HashMap();
    private HashMap<String,Set<String>> sourceDataMap = new HashMap();
    private ArrayList<String> masterDataNames = new ArrayList();
    private HashMap<String,List<String>> outputChangeMap = new HashMap();
    private boolean fatalIndexExceptions = false;
    private boolean handlersDisabled = false;
    private String defaultEncoding;
    private String localTimeZone = Calendar.getInstance().getTimeZone().getID();
    private boolean suppressArchiving = false;

    public static SimpleDateFormat jobNumberFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    public static SimpleDateFormat dateOutputFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss.SSS zzz yyyy");
    public static SimpleDateFormat dataCellFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
    public static SimpleDateFormat extractControlFormat = new SimpleDateFormat("MMM dd HH:mm:ss zzz yyyy");
    
    //HashMap<String,DataContainer> data = new HashMap();
    //private HashMap<String, ETLJobWrapper> includedTasks = new HashMap();
    //private HashMap<String, MenuOptionsWrapper> menuOptions = new HashMap();
    //private HashMap<String, VariableWrapper> variables = new HashMap();
    //private HashMap<String, TaskWrapper> tasks = new HashMap();
    //private ArrayList<String> outputVars = new ArrayList();
    private Logger logger = null;

    public ETLJobWrapper() {}

    public ETLJobWrapper(EtlJob j, File root, File xml, Map<String,String> args, String jobnum)
        throws ETLException
    {
        logger = Logger.getLogger(this.getClass());
        if ( j == null )
        {
            throw new ETLException("Wrapper cannot be created with null job");
        }

        try
        {
            config = ETLConfiguration.initialize(root);
            logger.info("-----------------------Loading ETL properties---------------------------------");
            logger.info(config);
        }
        catch (IOException e)
        {
            throw new ETLException("Unable to access " + ETLConfiguration.propertyPath, e);
        }

        job = j;
        etlRoot = root;
        jobfile = xml;
        init(args, jobnum);
    }
    
    public void destroy()
    {
        logger.info("Cleaning up ETLJobWrapper for job " + getJobName());
        for ( AbstractTask task: tasks.values() )
        {
            task.destroy();
        }
        
        archiver.destroy();
        stats.destroy();
        global.clear();
        arguments.clear();
        tables.clear();
        tasks.clear();
        errors.clear();
        sourceDataMap.clear();
        masterDataNames.clear();
        outputChangeMap.clear();
    }
    
    public boolean indexExceptionsAreFatal()
    {
        return fatalIndexExceptions;
    }

    public boolean areHandlersDisabled()
    {
        return handlersDisabled;
    }
    
    public void setParent(ETLJobWrapper p)
    {
        parent = p;
    }

    public ETLJobWrapper getParent()
    {
        return parent;
    }
    
    public ETLJobWrapper getFirstAncestor()
    {
        ETLJobWrapper ancestor = null;
        if ( parent != null )
        {
            ancestor = parent.getFirstAncestor();
        }
        else
        {
            ancestor = this;
        }
        
        return ancestor;
    }

    public boolean isArchivingSuppressed() 
    {
        return suppressArchiving;
    }

    public void setSuppressArchiving(boolean suppressArchiving) 
    {
        this.suppressArchiving = suppressArchiving;
    }
    
    private void init(Map<String,String> args, String jobnum)
        throws ETLException
    {
        loadGlobalVariables(jobnum);
        assignJobArguments(args);
        loadDataArchiver(jobnum);
        loadLookupTables();
        loadMasterData();
        loadTasks();
        stats = new DataStatistics(getJobName(), this);
    }

    private void loadDataArchiver(String jobnum)
        throws ETLException
    {
        EtlJob.DataArchival da = job.getDataArchival();
        logger.info("-----------------------Loading data archiver---------------------------------");
        archiver = new DataArchiver(da, job.getGlobalVariables(), this);
        setGlobalVariable(Processor.GVAR_ARCHIVE_DIR, new DataCell(new DataColumn(DataTypes.STRING, Processor.GVAR_ARCHIVE_DIR), archiver.getRunningArchiveDirectory(jobnum)));
        logger.info("DataArchiver loaded");
    }

    private void loadGlobalVariables(String jobNumber)
        throws ETLException
    {
        EtlJob.GlobalVariables gv = job.getGlobalVariables();
        
        global = new RelationalContainer(GLOBAL_VAR_DATA_NAME);
        global.addDataRow(new HashMap());
        
        if ( gv != null && gv.getVariable().size() > 0 )
        {
            logger.info("-----------------------Loading global variables---------------------------------");
            for ( EtlJob.GlobalVariables.Variable var: job.getGlobalVariables().getVariable() )
            {
                DataColumn col = new DataColumn(var.getDataType(), var.getName());
                DataCell cell = new DataCell(col, var.getValue());
                setGlobalVariable(var.getName(), cell);

                if ( var.isJobArgument() )
                {
                    arguments.add(var.getName());
                    usage.put(var.getName(), var);
                    logger.debug("is a JobArgument. Usage: " + var.getUsage());
                }
            }
        }

        // Assign reserved global variable job name
        setGlobalVariable(Processor.GVAR_JOB_NAME, new DataCell(new DataColumn(DataTypes.STRING, Processor.GVAR_JOB_NAME), getJobName()));
        setGlobalVariable(Processor.GVAR_JOB_NUM, new DataCell(new DataColumn(DataTypes.STRING, Processor.GVAR_JOB_NUM), jobNumber));
        logger.debug("Default global variables: " + getGlobalVariableData());
    }

    public void assignJobArguments(Map<String,String> args)
        throws ETLException
    {
        logger.info("-----------------------Assigning Job Arguments---------------------------------");
        logger.debug("Passed arguments: " + args);
        List<String> jobargs = getJobArgumentNames();
        for ( Map.Entry<String,String> entry: args.entrySet() )
        {
            String name = entry.getKey();
            String val = entry.getValue();
            if ( jobargs.contains(name) )
            {
                setGlobalVariable(name, val);
            }
            else
            {
                logger.warn("Global variable is not declared as a job argument, no assignment performed: " + name);
            }
        }
        logger.info(getGlobalVariableData());
    }

    private void loadLookupTables()
        throws ETLException
    {
        EtlJob.LookupTables ltables = job.getLookupTables();
        if ( ltables != null && ltables.getTable().size() > 0 )
        {
            logger.info("-----------------------Loading lookup tables---------------------------------");
            for ( EtlJob.LookupTables.Table table: ltables.getTable() )
            {
                LookupTable lt = new LookupTable(table.getName());
                for ( EtlJob.LookupTables.Table.Entry entry: table.getEntry() )
                {
                    String composite = "";
                    for ( int i=0; i<entry.getKey().size(); i++ )
                    {
                        composite += entry.getKey().get(i);
                        if ( i < entry.getKey().size()-1 )
                        {
                            composite += TABLE_KEY_SEPARATOR;
                        }
                    }
                    
                    lt.put(composite, entry.getValue());
                }

                tables.put(lt.getTableName(), lt);
                logger.debug("LookupTable: " + lt);
            }
        }
    }

    private void loadMasterData()
        throws ETLException
    {
        EtlJob.MasterData md = job.getMasterData();
        if ( md != null )
        {
            logger.info("-----------------------Loading master data---------------------------------");
            for ( EtlJob.MasterData.Data dt: md.getData() )
            {
                String name = dt.getName();
                if ( md.getData().size() > 1 && ( name == null || name.trim().length() == 0 ) )
                {
                    throw new ETLException("Master data sets must be named if more than one is declared");
                }

                RelationalContainer masterData = new RelationalContainer(getMasterDataName(name));
                masterDataNames.add(getMasterDataName(name));
                for ( DataComplexType d: dt.getDataElement() )
                {
                    DataColumn mdcol = masterData.addColumnDefinition(String.valueOf(d.getType()), d.getValue(), d.getDefaultValue());
                    logger.debug(masterData.getDataName() + ": " + mdcol);
                }

                // TODO: handle index creation on master data
                if ( dt.getIndices() != null )
                {
                    for ( IndexComplexType.Index idx: dt.getIndices().getIndex() )
                    {
                        DataIndex di = new DataIndex(idx);//, masterData.getColumnDefinitions());
                        masterData.addDataIndex(di);
                        logger.debug("DataIndex: " + di);
                    }
                }

                MasterDataTask task = new MasterDataTask(masterData.getDataName(), this);
                task.setData(masterData);
                tasks.put(masterData.getDataName(), task);
                //data.put(masterData.getDataName(), masterData);
            }
        }
    }

    private void loadTasks()
        throws ETLException
    {
        EtlJob.JobDefinition def = job.getJobDefinition();
        
        if ( def != null )
        {
            logger.info("-----------------------Loading ETL tasks---------------------------------");
            
            fatalIndexExceptions = def.isIndexExceptionAsFatal();
            handlersDisabled = def.isDisableHandlers();
            defaultEncoding = String.valueOf(def.getDefaultEncoding());
            DataCell.setDefaultEncoding(defaultEncoding);
            logger.info("Setting default character set for binary encoding: " + defaultEncoding);
            
            // Set the date output format for archived tasks to the defined value if it's not null
            if ( def.getOutputDateFormat() != null )
            {
                dateOutputFormat = new SimpleDateFormat(archiver.processEmbeddedBeanScript(def.getOutputDateFormat()));
            }

            // Set the local timezone if it's not null
            if ( def.getLocalTimeZone() != null )
            {
                localTimeZone = def.getLocalTimeZone();
                dateOutputFormat.setTimeZone(TimeZone.getTimeZone(localTimeZone));
            }
            
            List<EtlTaskComplexType> jtasks = def.getTask();
            for ( EtlTaskComplexType task: jtasks )
            {
                // Notify if task has been previously defined
                if ( tasks.containsKey(task.getName()) )
                {
                    //System.err.println("WARNING: Task already exists with name: " + task.getName());
                    //logger.warn("WARNING: Task already exists with name: " + task.getName());
                    throw new ETLException("Task already exists with name: " + task.getName() + ". Fix before running again.");
                }
                
                logger.info("Parsing task: " + task.getName());
                AbstractTask et = null;
                if ( task.getInputAdapter() != null )
                {
                    et = loadInput(task);
                }
                else if ( task.getDataMapping() != null )
                {
                    et = loadDataMapping(task);
                }
                else if ( task.getHierarchyMapping()!= null )
                {
                    et = loadHierarchyMapping(task);
                }
                else if ( task.getOutputAdapter() != null )
                {
                    et = loadOutput(task);
                }
                else if ( task.getCustomTask() != null )
                {
                    et = loadCustomProcess(task);
                }
                else if ( task.getFilterTask() != null )
                {
                    et = loadFilterTask(task);
                }
                else if ( task.getDataFilter() != null )
                {
                    et = loadDataFilter(task);
                }
                else if ( task.getDataSwitch() != null )
                {
                    et = loadDataSwitch(task);
                }
                else if ( task.getSubJob() != null )
                {
                    et = loadSubJob(task);
                }
                else if ( task.getCorrelateData() != null )
                {
                    et = loadCorrelateData(task);
                }
                else if ( task.getAggregateData() != null )
                {
                    et = loadAggregateData(task);
                }
                else if ( task.getChangeFilter() != null )
                {
                    et = loadChangeFilter(task);
                }
                else if ( task.getErrorTask() != null )
                {
                    et = loadErrorTask(task);
                }
                else if ( task.getSortData() != null )
                {
                    et = loadDataSorter(task);
                }
                else if ( task.getProcessControl() != null )
                {
                    et = loadControlTask(task);
                }
                else if ( task.getWebApi() != null )
                {
                    et = loadWebApiTask(task);
                }
                else if ( task.getExtractControl() != null )
                {
                    et = loadExtractControl(task);
                }
                else if ( task.getSplitRows()!= null )
                {
                    et = loadRowSplitter(task);
                }
                else
                {
                    et = new NullTask(task, this);
                    tasks.put(et.getTaskName(), et);
                }
                
                switch (et.getTaskType())
                {
                    case start:
                        tasks.put(TASK_START_KEY, et);
                        break;
                    case end:
                        tasks.put(TASK_END_KEY, et);
                        break;
                    default:
                }

                //data.put(et.getTaskName(), et.getData());
                addSourceDataMap(et);
            }
        }

        logger.debug("ChangeFilterTaskMap: " + outputChangeMap);
        logger.debug("SourceDataReferences: " + sourceDataMap);
    }

    private void addSourceDataMap(AbstractTask task)
    {
        Set<String> refs = sourceDataMap.get(task.getTaskName());
        if ( refs == null )
        {
            refs = new HashSet();
            sourceDataMap.put(task.getTaskName(), refs);
        }

        // Add the task level source data name.
        if ( task.getSourceDataTaskName() != null && task.getSourceDataTaskName().length() > 0 )
        {
            refs.add(task.getSourceDataTaskName());
        }

        switch (task.getClassType())
        {
            case DataMapping:
                DataMapping dm = (DataMapping)task;
                if ( dm.getSource() != null && dm.getSource().length() > 0 )
                {
                    refs.add(dm.getSource());
                }
                if ( dm.getTarget() != null && dm.getTarget().length() > 0 )
                {
                    refs.add(dm.getTarget());
                }
                break;

            case DataCorrelation:
                DataCorrelation dc = (DataCorrelation)task;
                if ( dc.getIteratorDataName() != null && dc.getIteratorDataName().length() > 0 )
                {
                    refs.add(dc.getIteratorDataName());
                }
                if ( dc.getQueryDataName() != null && dc.getQueryDataName().length() >  0 )
                {
                    refs.add(dc.getQueryDataName());
                }
                break;

            case DataAggregation:
                DataAggregation da = (DataAggregation)task;
                if ( da.getSource() != null && da.getSource().length() > 0 )
                {
                    refs.add(da.getSource());
                }
                break;
        }
    }

    private AbstractTask loadInput(EtlTaskComplexType task)
        throws ETLException
    {
        AbstractTask etltask = null;
        if ( task != null )
        {
            InputTaskComplexType in = task.getInputAdapter();
            if ( in != null )
            {
                // Create DatabaseAdapter
                if ( in.getDatabaseInput() != null )
                {
                    logger.debug("Parsing input task: " + task.getName() + "=" + in.getClass().getSimpleName());
                    etltask = new DatabaseAdapter(task, in, this);
                    tasks.put(task.getName(), etltask);
                }
                // Create FileAdapter
                else if ( in.getFileInput() != null || in.getFilesInput() != null )
                {
                    logger.debug("Parsing input task: " + task.getName() + "=" + in.getClass().getSimpleName());
                    etltask = new FileAdapter(task, in, etlRoot, this);
                    tasks.put(task.getName(), etltask);
                }
                // Create DefinedDataAdapter
                else if ( in.getDefineInput() != null )
                {
                    logger.debug("Parsing input task: " + task.getName() + "=" + in.getClass().getSimpleName());
                    etltask = new DefinedDataAdapter(task, in, this);
                    tasks.put(task.getName(), etltask);
                }
                // Create TIAAdapter
                else if ( in.getTiaInput() != null )
                {
                    logger.debug("Parsing input task: " + task.getName() + "=" + in.getClass().getSimpleName());
                    etltask = new TIAAdapter(task, in, this);
                    tasks.put(task.getName(), etltask);
                }
                // Create BinaryAdapter
                else if ( in.getBinaryInput() != null )
                {
                    logger.debug("Parsing input task: " + task.getName() + "=" + in.getClass().getSimpleName());
                    etltask = new BinaryAdapter(task, in, etlRoot, this);
                    tasks.put(task.getName(), etltask);
                }
                // Create ExpansionInterval
                else if ( in.getExpandInterval() != null )
                {
                    logger.debug("Parsing input task: " + task.getName() + "=" + in.getClass().getSimpleName());
                    etltask = new ExpansionInterval(task, in, this);
                    tasks.put(task.getName(), etltask);
                }
                else
                {
                    throw new ETLException("Unsupported input adapter in etl wrapper for: " + task.getName());
                }
            }
        }
        return etltask;
    }

    private AbstractTask loadDataMapping(EtlTaskComplexType task)
        throws ETLException
    {
        AbstractTask etltask = null;
        if ( task != null )
        {
            MappingTaskComplexType map = task.getDataMapping();
            if ( map != null )
            {
                logger.debug("Parsing data mapping task: " + task.getName() + "=" + map.getClass().getSimpleName());
                // Create DataMapping
                etltask = new DataMapping(task, map, this);
                tasks.put(task.getName(), etltask);
            }
        }
        return etltask;
    }

    private AbstractTask loadHierarchyMapping(EtlTaskComplexType task)
        throws ETLException
    {
        AbstractTask etltask = null;
        if ( task != null )
        {
            HierarchyMappingTaskComplexType map = task.getHierarchyMapping();
            if ( map != null )
            {
                logger.debug("Parsing hierarchy mapping task: " + task.getName() + "=" + map.getClass().getSimpleName());
                // Create DataMapping
                etltask = new HierarchyMapping(task, map, this, etlRoot);
                tasks.put(task.getName(), etltask);
            }
        }
        return etltask;
    }

    private AbstractTask loadOutput(EtlTaskComplexType task)
        throws ETLException
    {
        AbstractTask etltask = null;
        if ( task != null )
        {
            OutputTaskComplexType out = task.getOutputAdapter();
            if ( out != null )
            {
                // Create DatabaseAdapter
                if ( out.getDatabaseOutput() != null )
                {
                    logger.debug("Parsing output task: " + task.getName() + "=" + out.getClass().getSimpleName());
                    etltask = new DatabaseAdapter(task, out, this);
                    tasks.put(task.getName(), etltask);
                }
                // Create FileAdapter
                else if ( out.getFileOutput() != null || out.getFilesOutput() != null )
                {
                    logger.debug("Parsing output task: " + task.getName() + "=" + out.getClass().getSimpleName());
                    etltask = new FileAdapter(task, out, etlRoot, this);
                    tasks.put(task.getName(), etltask);
                }
                // Create TIAAdapter
                else if ( out.getTiaOutput() != null )
                {
                    logger.debug("Parsing output task: " + task.getName() + "=" + out.getClass().getSimpleName());
                    etltask = new TIAAdapter(task, out, this);
                    tasks.put(task.getName(), etltask);
                }
                // Create BinaryAdapter
                else if ( out.getBinaryOutput() != null )
                {
                    logger.debug("Parsing output task: " + task.getName() + "=" + out.getClass().getSimpleName());
                    etltask = new BinaryAdapter(task, out, etlRoot, this);
                    tasks.put(task.getName(), etltask);
                }
                else if ( out.getEmailOutput() != null )
                {
                    logger.debug("Parsing output task: " + task.getName() + "=" + out.getClass().getSimpleName());
                    etltask = new EmailAdapter(task, out, etlRoot, this);
                    tasks.put(task.getName(), etltask);
                }
                else
                {
                    throw new ETLException("Unsupported output adapter in etl wrapper for: " + task.getName());
                }
            }
        }

        return etltask;
    }

    private AbstractTask loadCustomProcess(EtlTaskComplexType task)
        throws ETLException
    {
        AbstractTask etltask = null;
        if ( task != null )
        {
            CustomTaskComplexType java = task.getCustomTask();
            if ( java != null )
            {
                logger.debug("Parsing custom task: " + task.getName() + "=" + java.getClass().getSimpleName());
                // Create CustomTask
                etltask = new CustomProcess(task, java, this);
                tasks.put(task.getName(), etltask);
            }
        }
        return etltask;
    }

    private AbstractTask loadFilterTask(EtlTaskComplexType task)
        throws ETLException
    {
        AbstractTask etltask = null;
        if ( task != null )
        {
            FilterTaskComplexType filter = task.getFilterTask();
            if ( filter != null )
            {
                logger.debug("Parsing data filter: " + task.getName() + "=" + filter.getClass().getSimpleName());
                // Create CustomTask
                etltask = new DataFilter(task, filter, this);
                tasks.put(task.getName(), etltask);
            }
        }
        return etltask;
    }

    private AbstractTask loadDataFilter(EtlTaskComplexType task)
        throws ETLException
    {
        AbstractTask etltask = null;
        if ( task != null )
        {
            FilterTaskComplexType filter = task.getDataFilter();
            if ( filter != null )
            {
                logger.debug("Parsing data filter: " + task.getName() + "=" + filter.getClass().getSimpleName());
                // Create CustomTask
                etltask = new DataFilter(task, filter, this);
                tasks.put(task.getName(), etltask);
            }
        }
        return etltask;
    }

    private AbstractTask loadDataSwitch(EtlTaskComplexType task)
        throws ETLException
    {
        AbstractTask etltask = null;
        if ( task != null )
        {
            SwitchTaskComplexType filter = task.getDataSwitch();
            if ( filter != null )
            {
                logger.debug("Parsing data swtich: " + task.getName() + "=" + filter.getClass().getSimpleName());
                // Create CustomTask
                etltask = new DataFilter(task, filter, this);
                tasks.put(task.getName(), etltask);
            }
        }
        return etltask;
    }

    private AbstractTask loadSubJob(EtlTaskComplexType task)
        throws ETLException
    {
        AbstractTask etltask = null;
        if ( task != null )
        {
            SubJobComplexType subjob = task.getSubJob();
            if ( subjob != null )
            {
                logger.debug("Parsing subjob: " + task.getName() + "=" + subjob.getClass().getSimpleName());
                // Create SubTask
                etltask = new SubJob(task, subjob, this, etlRoot);
                tasks.put(task.getName(), etltask);
            }
        }
        return etltask;
    }

    private AbstractTask loadCorrelateData(EtlTaskComplexType task)
        throws ETLException
    {
        AbstractTask etltask = null;
        if ( task != null )
        {
            CorrelateDataComplexType cd = task.getCorrelateData();
            if ( cd != null )
            {
                logger.debug("Parsing data process: " + task.getName() + "=" + cd.getClass().getSimpleName());
                // Create Data Correlation
                etltask = new DataCorrelation(task, cd, this);
                tasks.put(task.getName(), etltask);
            }
        }
        return etltask;
    }

    private AbstractTask loadAggregateData(EtlTaskComplexType task)
        throws ETLException
    {
        AbstractTask etltask = null;
        if ( task != null )
        {
            AggregateDataComplexType ad = task.getAggregateData();
            if ( ad != null )
            {
                logger.debug("Parsing aggregate data: " + task.getName() + "=" + ad.getClass().getSimpleName());
                // Create Data Aggregation
                etltask = new DataAggregation(task, ad, this);
                tasks.put(task.getName(), etltask);
            }
        }
        return etltask;
    }

    private AbstractTask loadErrorTask(EtlTaskComplexType task)
        throws ETLException
    {
        AbstractTask etltask = null;
        if ( task != null )
        {
            ErrorTaskComplexType et = task.getErrorTask();
            if ( et != null )
            {
                logger.debug("Parsing error task: " + task.getName() + "=" + et.getClass().getSimpleName());
                // Create Error Task
                etltask = new ErrorTask(task, et, this);
                tasks.put(task.getName(), etltask);
            }
        }
        return etltask;
    }

    private AbstractTask loadChangeFilter(EtlTaskComplexType task)
        throws ETLException
    {
        AbstractTask etltask = null;
        if ( task != null )
        {
            ChangeFilterComplexType cf = task.getChangeFilter();
            if ( cf != null )
            {
                logger.debug("Parsing change filter: " + task.getName() + "=" + cf.getClass().getSimpleName());
                // Create Change Filter
                etltask = new ChangeFilter(task, cf, this);
                tasks.put(task.getName(), etltask);

                ChangeFilter filter = (ChangeFilter)etltask;
                for ( String otask: filter.getOutputTaskNames() )
                {
                    List<String> list = outputChangeMap.get(otask);
                    if ( list == null )
                    {
                        list = new ArrayList();
                        outputChangeMap.put(otask, list);
                    }
                    list.add(task.getName());
                }
            }
        }
        return etltask;
    }

    private AbstractTask loadDataSorter(EtlTaskComplexType task)
        throws ETLException
    {
        AbstractTask etltask = null;
        if ( task != null )
        {
            SortDataComplexType s = task.getSortData();
            if ( s != null )
            {
                logger.debug("Parsing sort data task: " + task.getName() + "=" + s.getClass().getSimpleName());
                // Create DataSorter
                etltask = new DataSorter(task, s, this);
                tasks.put(task.getName(), etltask);
            }
        }
        return etltask;
    }

    private AbstractTask loadControlTask(EtlTaskComplexType task)
        throws ETLException
    {
        AbstractTask etltask = null;
        if ( task != null )
        {
            ControlTaskComplexType c = task.getProcessControl();
            if ( c != null )
            {
                logger.debug("Parsing control task: " + task.getName() + "=" + c.getClass().getSimpleName());
                // Create ControlTask
                etltask = new ControlTask(task, c, this);
                tasks.put(task.getName(), etltask);
            }
        }
        return etltask;
    }

    private AbstractTask loadExtractControl(EtlTaskComplexType task)
        throws ETLException
    {
        AbstractTask etltask = null;
        if ( task != null )
        {
            ExtractControlComplexType c = task.getExtractControl();
            if ( c != null )
            {
                logger.debug("Parsing extract control task: " + task.getName() + "=" + c.getClass().getSimpleName());
                // Create ExtractControlTask
                etltask = new ExtractControl(task, c, etlRoot, this);
                tasks.put(task.getName(), etltask);
            }
        }
        return etltask;
    }

    private AbstractTask loadRowSplitter(EtlTaskComplexType task)
        throws ETLException
    {
        AbstractTask etltask = null;
        if ( task != null )
        {
            SplitRowsComplexType c = task.getSplitRows();
            if ( c != null )
            {
                logger.debug("Parsing row splitter task: " + task.getName() + "=" + c.getClass().getSimpleName());
                // Create ExtractControlTask
                etltask = new RowSplitter(task, c, etlRoot, this);
                tasks.put(task.getName(), etltask);
            }
        }
        return etltask;
    }

    private AbstractTask loadWebApiTask(EtlTaskComplexType task)
        throws ETLException
    {
        AbstractTask etltask = null;
        if ( task != null )
        {
            WebAPITaskComplexType c = task.getWebApi();
            if ( c != null )
            {
                logger.debug("Parsing web api task: " + task.getName() + "=" + c.getClass().getSimpleName());
                // Create ControlTask
                etltask = new WebAPIDispatcher(task, c, this);
                tasks.put(task.getName(), etltask);
            }
        }
        return etltask;
    }

    public EtlJob getBaseObject()
    {
        return job;
    }

    public ETLConfiguration getConfiguration()
    {
        return config;
    }

    public File getETLRoot()
    {
        return etlRoot;
    }

    public File getJobFile()
    {
        return jobfile;
    }

    public String getJobName()
    {
        return jobfile.getName().substring(0, jobfile.getName().indexOf("."));
    }
    
    public String getLocalTimeZone()
    {
        return localTimeZone;
    }

    public String getMasterDataName(String name)
    {
        String mdn = MASTER_DATA_NAME;
        if ( name != null && name.trim().length() > 0 )
        {
            mdn += "." + name;
        }
        return mdn;
    }

    public Map<String, AbstractTask> getJobTasks()
    {
        return tasks;
    }

    public AbstractTask getStartJobTask()
    {
        return tasks.get(TASK_START_KEY);
    }

    public AbstractTask getJobTaskByName(String name)
    {
        return tasks.get(name);
    }

    public <E extends AbstractTask> List<AbstractTask> getJobTasksByType(Class<E> type)
    {
        ArrayList<AbstractTask> taskList = new ArrayList();
        for ( AbstractTask task: tasks.values() )
        {
            if ( type.isInstance(task) )
            {
                taskList.add(task);
            }
        }

        return taskList;
    }

    public <E extends AbstractTask> List<String> getJobTaskNamesByType(Class<E> type)
    {
        ArrayList<String> taskList = new ArrayList();
        for ( AbstractTask task: tasks.values() )
        {
            if ( type.isInstance(task) )
            {
                taskList.add(task.getTaskName());
            }
        }

        return taskList;
    }

    public <E extends AbstractTask> Set<String> getNestedJobTaskNamesByType(Class<E> type)
    {
        // Look through all tasks defined in this job for the given class
        HashSet<String> taskList = new HashSet();
        for ( AbstractTask task: tasks.values() )
        {
            if ( type.isInstance(task) )
            {
                taskList.add(getJobName() + "." + task.getTaskName());
            }
        }
        
        // Look through all the subjob tasks for the given class for each subjob defined in this job (recursive)
        ArrayList<SubJob> subjobs = new ArrayList(getJobTasksByType(SubJob.class));
        for ( SubJob subjob: subjobs )
        {
            try
            {
                ETLJobWrapper subjobWrapper = subjob.getSubJobWrapper("", new HashMap<String,String>(), this);
                taskList.addAll(subjobWrapper.getNestedJobTaskNamesByType(type));
            }
            catch (ETLException e) { logger.warn("Failed to load job for: " + subjob.getTargetJobName()); }
        }

        logger.debug("Nested task names for " + getJobName() + ": " + taskList);
        return taskList;
    }

    public RelationalContainer getMasterData()
        throws ETLException
    {
        return (RelationalContainer)retrieveNamedData(masterDataNames.get(0));
    }

    public RelationalContainer getMasterData(String name)
        throws ETLException
    {
        return (RelationalContainer)retrieveNamedData(name);
    }

    public List<String> getMasterDataNames()
    {
        return masterDataNames;
    }

    public RelationalContainer getNamedMasterData(String name)
        throws ETLException
    {
        return (RelationalContainer)retrieveNamedData(getMasterDataName(name));
    }

    public RelationalContainer getGlobalVariableData()
        throws ETLException
    {
        return (RelationalContainer)retrieveNamedData(GLOBAL_VAR_DATA_NAME);
    }
    
    public DataCell getGlobalVariable(String name)
        throws ETLException
    {
        DataCell var = null;
        RelationalContainer dc = getGlobalVariableData();
        if ( dc != null && dc.rowCount() > 0 )
        {
            Map<String,DataCell> data = dc.getMappedData().get(0);
            var = data.get(name);
        }
        return var;
    }

    public Map<String,EtlJob.GlobalVariables.Variable> getArgumentUsage()
    {
        return usage;
    }
    
    public Map<String,RelationalContainer> getErrorDataMap()
    {
        return errors;
    }

    public RelationalContainer retrieveErrorData(String taskName)
    {
        return errors.get(taskName + AbstractTask.ERROR_DATA_NAME);
    }

    public RelationalContainer removeErrorData(String taskName)
    {
        return errors.remove(taskName + AbstractTask.ERROR_DATA_NAME);
    }
    
    public void setErrorData(String taskName, RelationalContainer data)
    {
        errors.put(taskName + AbstractTask.ERROR_DATA_NAME, data);
    }

    public AbstractTask retrieveNamedDataTask(String name)
    {
        AbstractTask task = null;
        if ( GLOBAL_VAR_DATA_NAME.equals(name) )
        {
            task = null;
        }
        else if ( MASTER_DATA_NAME.equals(name) )
        {
            String mname = name;
            if ( MASTER_DATA_NAME.equals(name) && tasks.get(name) == null && masterDataNames.size() > 0 )
            {
                mname = masterDataNames.get(0);
            }

            task = tasks.get(mname);
        }
        else
        {
            task = tasks.get(name);
        }

        return task;
    }

    public DataSet retrieveNamedData(String name)
        throws ETLException
    {
        DataSet ds = null;
        if ( GLOBAL_VAR_DATA_NAME.equals(name) )
        {
            ds = global;
        }
        else
        {
            AbstractTask task = retrieveNamedDataTask(name);
            if ( task != null )
            {
                ds = task.getData();
            }
        }

        return ds;
    }

    public Map<String,LookupTable> getLookupTablesMap()
    {
        return tables;
    }

    public LookupTable retrieveLookupTable(String name)
    {
        return tables.get(name);
    }

    public DataCell retrieveGlobalVariable(String name)
        throws ETLException
    {
        return getGlobalVariableData().getMappedData().get(0).get(name);
    }

    public void setGlobalVariable(String name, String val)
        throws ETLException
    {
        DataCell var = retrieveGlobalVariable(name);
        if ( var == null )
        {
            logger.warn("Global variable not defined, no assignment performed: " + name + "=" + val);
        }
        else
        {
            var.setValue(val);
            logger.debug("Global variable assigned: " + name + "=" + var);
        }
    }
    
    public void setGlobalVariable(String name, DataCell cell)
        throws ETLException
    {
        DataCell var = retrieveGlobalVariable(name);
        if ( var == null && cell != null )
        {
            logger.debug("Adding global variable: " + name + "=" + cell);
            global.addColumnDefinition(cell.getColumn());
            Map<String,DataCell> rowdata = global.getMappedData().get(0);
            rowdata.put(name, cell);
        }
        else if ( cell != null )
        {
            setGlobalVariable(name, String.valueOf(cell.getValue()));
        }
        else
        {
            setGlobalVariable(name, (String)null);
        }
    }

    public Map<String,List<String>> getChangeFilterTaskMap()
    {
        return outputChangeMap;
    }
    
    public List<String> getChangeFilterTasksByOutput(String otask)
    {
        List<String> list = new ArrayList();
        if ( outputChangeMap.containsKey(otask) )
        {
            list = outputChangeMap.get(otask);
        }
        return list;
    }

    public DataArchiver getDataArchiver()
    {
        return archiver;
    }

    public DataStatistics getDataStatistics()
    {
        return stats;
    }

    public List<String> getJobArgumentNames()
    {
        return arguments;
    }
    
    public <E extends AbstractTask> List<E> getTasksByType(Class<E> clazz)
    {
        List<E> ftasks = new ArrayList();
        if ( clazz != null )
        {
            for ( AbstractTask task: tasks.values() )
            {
                if ( clazz.isAssignableFrom(task.getClass()) )
                {
                    E ctask = (E)task;
                    ftasks.add(ctask);
                }
            }
        }
        
        return ftasks;
    }

    public void archiveTransientMasterData(String jobnum)
        throws ETLException
    {
        for (String mname: masterDataNames )
        {
            // Need to load the data in case the current task didn't use it or the data will be overwritten
            // as empty and the master data will be lost.
            AbstractTask task = getJobTaskByName(mname);
            task.getData();
            task.archiveTransientData(jobnum);
        }
    }

    public void removeTransientData(String jobnum)
        throws ETLException
    {
        for ( AbstractTask task: tasks.values() )
        {
        }
    }

    @Override
    public String toString()
    {
        StringBuffer out = new StringBuffer();
        out.append("{");
        out.append("\nJob=" + jobfile);
        out.append("\nArchive=" + archiver.getArchiveDirectory());
        out.append("\nTasks=" + tasks);
        out.append("}");
        return out.toString();
    }

}
