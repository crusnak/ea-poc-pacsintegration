/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.transforms;

import com.blackboard.services.etl.data.Argument;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.exception.TransformException;
import com.blackboard.services.etl.jaxb.ArgumentComplexType;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.DateFields;
import com.blackboard.services.etl.jaxb.DateTransformComplexType;
import com.blackboard.services.etl.jaxb.TransformComplexType;
import com.blackboard.services.etl.tasks.AbstractTask;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.blackboard.services.utils.BBTSUtils;
import com.blackboard.services.utils.BeanScript;
import com.blackboard.services.utils.JavaUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

/**
 *
 * @author crusnak
 */
public class DateTransform extends AbstractTransform
{
    public static enum Action { dateToString, unixTimestamp, epochMilliseconds, bbtsToDate, dateToBBTS, extractField, convertToField, setField, mathOnField, removeTimeFromDate, resetDateToEpoch, greaterThanOnField, lessThanOnField, convertToInterval };
    public static enum ActionModifier { add, subtract };
    public static enum NullTo { NULL, epoch, sysdate, sysdatetime, bbtsZero };

    /**
     * Constant value indicating that the source value of the transform should be the current date.
     */
    public static final String VAL_SYSDATE =            "SYSDATE";
    public static final String VAL_SYSDATETIME =        "SYSDATETIME";
    public static final String VAL_EPOCHDATE =          "EPOCHDATE";
    public static final String VAL_BBTSZERODATE =       "BBTSZERODATE";

    private static HashMap<DateFields,Integer> fieldMap = new HashMap();
    static
    {
        fieldMap.put(DateFields.YEAR, Calendar.YEAR);
        fieldMap.put(DateFields.MONTH, Calendar.MONTH);
        fieldMap.put(DateFields.DAY, Calendar.DATE);
        fieldMap.put(DateFields.DAYOFWEEK, Calendar.DAY_OF_WEEK);
        fieldMap.put(DateFields.HOUR_12, Calendar.HOUR);
        fieldMap.put(DateFields.HOUR_24, Calendar.HOUR_OF_DAY);
        fieldMap.put(DateFields.AM_PM, Calendar.AM_PM);
        fieldMap.put(DateFields.MINUTE, Calendar.MINUTE);
        fieldMap.put(DateFields.SECOND, Calendar.SECOND);
        fieldMap.put(DateFields.MILLISECOND, Calendar.MILLISECOND);
        fieldMap.put(DateFields.WEEKOFYEAR, Calendar.WEEK_OF_YEAR);
        fieldMap.put(DateFields.DST_OFFSET, Calendar.DST_OFFSET);
        fieldMap.put(DateFields.TZ_OFFSET, Calendar.ZONE_OFFSET);
    }

    private Action action = null;
    private ActionModifier modifier = null;
    private DateFields field = null;
    private NullTo nullsTo = null;
    private SimpleDateFormat format = null;
    private String globalDateFormat = null;

    public DateTransform(DateTransformComplexType ct, AbstractTask task)
        throws ETLException
    {
        super(ct, task);
        setAction(ct.getAction());
        setActionModifier(ct.getActionModifier());
        setField(ct.getField());
        setNullsTo(ct.getNullTo());
        
        if ( ct.getDateFormat() != null )
        {
            BeanScript bs = new BeanScript(ct.getDateFormat());
            if ( bs.getAccessorText() == null )
            {
                setDateFormat(ct.getDateFormat());
            }      
            else
            {
                globalDateFormat = ct.getDateFormat();
            }
        }
    }

    @Override
    public Type getTransformType()
    {
        return Type.date;
    }

    public Action getAction()
    {
        return action;
    }

    private void setAction(Action action)
    {
        this.action = action;
    }

    private void setAction(String action)
        throws ETLException
    {
        if ( action != null && action.trim().length() > 0 )
        {
            try
            {
                setAction(Action.valueOf(action));
            }
            catch (IllegalArgumentException e)
            {
                throw new ETLException("Unsupported date transform action: " + action + ", expected: " + JavaUtils.enumToList(Action.class), e);
            }

        }
        else
        {
            setAction((Action)null);
        }
    }

    public ActionModifier getActionModifier()
    {
        return modifier;
    }

    private void setActionModifier(ActionModifier mod)
    {
        this.modifier = mod;
    }

    private void setActionModifier(String mod)
        throws ETLException
    {
        if ( action != null )
        {
            switch(action)
            {
                case mathOnField:
                case greaterThanOnField:
                case lessThanOnField:
                    if ( mod != null && mod.trim().length() > 0 )
                    {
                        try
                        {
                            setActionModifier(ActionModifier.valueOf(mod));
                        }
                        catch (IllegalArgumentException e)
                        {
                            throw new ETLException("Unsupported date transform action modifier: " + mod + ", expected: " + JavaUtils.enumToList(ActionModifier.class), e);
                        }
                    }
                    else
                    {
                        throw new ETLException("Action mathOnField/greaterThanOnField/lessThanOnField requires actionModifier to be declared, expected: " + JavaUtils.enumToList(ActionModifier.class));
                    }
                break;
                default:
                    setActionModifier((ActionModifier)null);
                    break;
            }
        }
        else
        {
            setActionModifier((ActionModifier)null);
        }
    }

    public DateFields getField()
    {
        return field;
    }

    private void setField(DateFields field)
    {
        this.field = field;
    }

    private void setField(String field)
        throws ETLException
    {
        if ( action != null )
        {
            switch (action)
            {
                case extractField:
                case convertToField:
                case convertToInterval:
                case mathOnField:
                case lessThanOnField:
                case greaterThanOnField:
                    try
                    {
                        if ( field != null && field.trim().length() > 0 )
                        {
                            setField(DateFields.valueOf(field));
                        }
                        else
                        {
                            throw new ETLException("Missing field attribute for date transform of action: " + Action.extractField);
                        }
                    }
                    catch (IllegalArgumentException e)
                    {
                        throw new ETLException("Unsupported date transform field: " + field + ", expected: " + JavaUtils.enumToList(DateFields.class), e);
                    }
                    break;
            }
        }
    }

    public NullTo getNullsTo()
    {
        return nullsTo;
    }

    private void setNullsTo(NullTo nulls)
    {
        this.nullsTo = nulls;
    }

    private void setNullsTo(String nulls)
        throws ETLException
    {
        if ( nulls != null && nulls.trim().length() > 0 )
        {
            try
            {
                setNullsTo(NullTo.valueOf(nulls));
            }
            catch (IllegalArgumentException e)
            {
                throw new ETLException("Unsupported date transform nullTo: " + nulls + ", expected: " + JavaUtils.enumToList(NullTo.class), e);
            }

        }
        else
        {
            setNullsTo((NullTo)null);
        }
    }

    @Override
    protected void setSourceColumns(List<TransformComplexType.SourceColumn> tct)
        throws ETLException
    {
        super.setSourceColumns(tct);
        for ( Argument arg: sources )
        {
            if ( arg.getArgumentType() == Argument.Type.constant && VAL_SYSDATE.equals(arg.getValue()) )
            {
                DataColumn col = new DataColumn(DataTypes.DATE, arg.getColumn().getColumnName());
                arg.setColumn(col);
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
                arg.setValue(new Date(cal.getTimeInMillis()));
            }
            if ( arg.getArgumentType() == Argument.Type.constant && VAL_SYSDATETIME.equals(arg.getValue()) )
            {
                DataColumn col = new DataColumn(DataTypes.DATE, arg.getColumn().getColumnName());
                arg.setColumn(col);
                arg.setValue(new Date());
            }
            else if ( arg.getArgumentType() == Argument.Type.constant && VAL_EPOCHDATE.equals(arg.getValue()) )
            {
                DataColumn col = new DataColumn(DataTypes.DATE, arg.getColumn().getColumnName());
                arg.setColumn(col);
                arg.setValue(new Date(0));
            }
            else if ( arg.getArgumentType() == Argument.Type.column )
            {
                DataColumn col = arg.getColumn();
                if ( col.getDataType() != DataTypes.STRING )
                {
                    throw new ETLException("Cannot instantiate DateTransform with source value '" + arg.getValue() + "' of type '" +
                                           col.getDataType() + "'. If value is not " + VAL_SYSDATE + " only string types are supported.");
                }
            }
        }
        logger.debug("Source columns: " + sources);
    }

    public SimpleDateFormat getDateFormat()
    {
        return format;
    }

    private void setDateFormat(SimpleDateFormat format)
    {
        this.format = format;
        this.format.setTimeZone(TimeZone.getTimeZone(task.getETLJobWrapper().getLocalTimeZone()));
    }

    private void setDateFormat(String format)
        throws ETLException
    {
        if ( format != null && format.trim().length() > 0 )
        {
            try
            {
                SimpleDateFormat f = new SimpleDateFormat(format.trim());

                setDateFormat(f);
            }
            catch (IllegalArgumentException e)
            {
                throw new ETLException("DateFormat is not valid: " + format, e);
            }
        }
        else
        {
            setDateFormat((SimpleDateFormat)null);
        }
    }

    @Override
    public boolean supportsMultipleSources()
    {
        boolean multiple = false;
        if ( action != null )
        {
            switch (action)
            {
                case convertToInterval:
                case bbtsToDate:
                case dateToString:
                case mathOnField:
                case lessThanOnField:
                case greaterThanOnField:
                case setField:
                    multiple = true;
                    break;
            }
        }
        return multiple;
    }
    
    public boolean supportsInlineDateFormatting()
    {
        boolean format = true;
        if ( action != null )
        {
            switch (action)
            {
                case convertToInterval:
                case mathOnField:
                case lessThanOnField:
                case greaterThanOnField:
                case setField:
                    format = false;
                    break;
            }
        }
        return format;
    }

    @Override
    public boolean supportsMultipleTargets()
    {
        return false;
    }

    @Override
    public int getExpectedSourceCount()
    {
        int count = 1;
        if ( action != null )
        {
            switch (action)
            {
                case lessThanOnField:
                case greaterThanOnField:
                    count = 3;
                    break;
                case convertToInterval:
                case mathOnField:
                case setField:
                    count = 2;
                    break;
                case bbtsToDate:
                case dateToString:
                    count = -1;
                    break;
            }
        }
        return count;
    }

    @Override
    public List<DataCell> transform(List<DataColumn> cols, List<DataCell> incoming)
        throws ETLException
    {
        ArrayList<DataCell> targets = new ArrayList();
        DataCell source = null;
        DataCell out = null;
        DataColumn tcol = null;
        Date date = null;

        validateIncomingSources(incoming);
        source = incoming.get(0);
        tcol = getTransformDataColumn(cols, incoming, 0);
        
        logger.debug("DateTransform source=" + source);

        try
        {
            if ( source == null || source.getValue() == null )
            {
                switch (nullsTo)
                {
                    case epoch:
                        logger.debug("Source value is null, epoch time assigned to date");
                        date = new Date(0);
                        break;
                    case sysdate:
                        logger.debug("Source value is null, current date assigned to date");
                        Calendar cal = Calendar.getInstance();
                        cal.set(Calendar.HOUR_OF_DAY, 0);
                        cal.set(Calendar.MINUTE, 0);
                        cal.set(Calendar.SECOND, 0);
                        cal.set(Calendar.MILLISECOND, 0);
                        date = new Date(cal.getTimeInMillis());
                        break;
                    case sysdatetime:
                        logger.debug("Source value is null, current date/time assigned to date");
                        date = new Date();
                        break;
                    case bbtsZero:
                        logger.debug("Source value is null, bbts zero time assigned to date");
                        date = BBTSUtils.convertDoubleToDate(0);
                        break;
                    default:
                        logger.debug("Source value is null, no transformation taking place");
                        date = null;

                }
            }
            else
            {
                logger.debug("Incoming cell class: " + source.getCellClass());
                if ( supportsInlineDateFormatting() && incoming.size() > 1 )
                {
                    DataCell df = incoming.get(1);
                    if ( df.getValue() == null )
                    {
                        throw new TransformException(null, "Second source cell value is null, cannot use null as date format");
                    }
                    else
                    {
                        logger.info("Overwriting attribute dateFormat with source cell 2: " + df.getValue());
                        setDateFormat(String.valueOf(df.getValue()));
                    }
                }
                
                // If the date format uses a global variable reference assign at runtime
                if ( globalDateFormat != null )
                {
                    setDateFormat((String)task.getGlobalVariableValue(globalDateFormat));
                }
                
                // Parse the incoming date string into a Date
                if ( source.getCellClass() == String.class )
                {
                    if ( format == null )
                    {
                        throw new TransformException(null, "DateFormat is empty, cannot parse date strings into date objects");
                    }

                    date = format.parse((String)source.getValue());
                    logger.debug("Incoming source value to Date transform is string, transforming to Date using (" + getDateFormat().toPattern() + "): " + date);
                }
                else if ( source.getCellClass() == Date.class )
                {
                    date = new Date(((Date)source.getValue()).getTime());
                }
                else if ( source.getCellClass() == Double.class && action != Action.bbtsToDate )
                {
                    throw new TransformException(null, "Incoming data type is unsupported: " + incoming +
                                                 ". Must be either a String or a Date.");
                }
                else
                {
                    if ( source.getValue() != null && format != null )
                    {
                        date = format.parse(String.valueOf(source.getValue()));
                        logger.debug("Incoming source value to Date transform is converted to string, transforming to Date: " + date);
                    }
                }
            }

            if ( action != null )
            {
                switch (action)
                {
                    case dateToString:
                        tcol.setDataType(DataTypes.STRING);
                        if ( format == null )
                        {
                            throw new TransformException(null, "Dateformat is empty, cannot convert date to string");
                        }

                        if ( date != null )
                        {
                            out = new DataCell(tcol, format.format(date));
                        }
                        else
                        {
                            out = new DataCell(tcol, null);
                        }
                        break;
                    case epochMilliseconds:
                        tcol.setDataType(DataTypes.LONG);
                        if ( date != null )
                        {
                            out = new DataCell(tcol, date.getTime());
                        }
                        else
                        {
                            out = new DataCell(tcol, 0L);
                        }
                        break;
                    case unixTimestamp:
                        tcol.setDataType(DataTypes.LONG);
                        if ( date != null )
                        {
                            out = new DataCell(tcol, date.getTime()/1000);
                        }
                        else
                        {
                            out = new DataCell(tcol, 0L);
                        }
                        break;
                    case bbtsToDate:
                        tcol.setDataType(DataTypes.DATE);
                        if ( source.getValue() != null )
                        {
                            out = new DataCell(tcol, BBTSUtils.convertDoubleToDate((Double)source.getValue()));
                        }
                        else
                        {
                            out = new DataCell(tcol, null);
                        }
                        break;
                    case dateToBBTS:
                        tcol.setDataType(DataTypes.DOUBLE);
                        if ( date != null )
                        {
                            double d = BBTSUtils.convertDateToDouble(date, false);
                            out = new DataCell(tcol, d);
                        }
                        else
                        {
                            out = new DataCell(tcol, null);
                        }
                        break;
                    case extractField:
                        tcol.setDataType(DataTypes.INT);
                        if ( date != null )
                        {
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(date);
                            Object val = cal.get(fieldMap.get(field));
                            switch (field)
                            {
                                case MONTH:
                                    val = (Integer)val + 1;
                                    break;
                                case HOUR_12:
                                    if ( (Integer)val == 0 )
                                    {
                                        val = (Integer)12;
                                    }
                                    break;
                                case AM_PM:
                                    tcol.setDataType(DataTypes.STRING);
                                    switch ((Integer)val)
                                    {
                                        case Calendar.AM:
                                            val = "AM";
                                            break;
                                        case Calendar.PM:
                                            val = "PM";
                                            break;
                                    }
                                    break;
                            }
                            out = new DataCell(tcol, val);
                        }
                        else
                        {
                            out = new DataCell(tcol, null);
                        }
                        break;
                    case convertToField:
                        tcol.setDataType(DataTypes.DOUBLE);
                        if ( date != null )
                        {
                            double val = (double)date.getTime();
                            switch (field)
                            {
                                case MILLISECOND:
                                    break;
                                case SECOND:
                                    val = (double)(date.getTime()/1000.0);
                                    break;
                                case MINUTE:
                                    val = (double)(date.getTime()/(1000.0*60));
                                    break;
                                case HOUR_12:
                                case HOUR_24:
                                    val = (double)(date.getTime()/(1000.0*60*60));
                                    break;
                                case DAY:
                                    val = (double)(date.getTime()/(1000.0*60*60*24));
                                    break;
                                default:
                                    EnumSet set = EnumSet.of(DateFields.MILLISECOND, DateFields.SECOND, DateFields.MINUTE, DateFields.HOUR_12, DateFields.HOUR_24, DateFields.DAY);
                                    throw new UnsupportedOperationException("Conversion to date fields not currently supported, only time fields allowed: " + JavaUtils.enumToList(set));
                            }
                            out = new DataCell(tcol, val);
                        }
                        else
                        {
                            out = new DataCell(tcol, date);
                        }
                        break;
                    case mathOnField:
                        tcol.setDataType(DataTypes.DATE);
                        DataCell operand = incoming.get(1);
                        if ( date != null )
                        {
                            switch (field)
                            {
                                case TIMEZONE:
                                    throw new UnsupportedOperationException("Math for timezones fields not supported");
                                case DST_OFFSET:
                                case TZ_OFFSET:
                                    Integer offset = Integer.valueOf(String.valueOf(operand.getValue()));
                                    offset *= 60 * 60 * 1000; // 60 minutes in an hour, 60 seconds in a minute, 1000 ms in a second
                                    operand.setValue(offset); // fall through to the assignment section below
                                default:
                                    Calendar cal = Calendar.getInstance();
                                    cal.setTime(date);
                                    Object val = cal.get(fieldMap.get(field));
                                    Integer mathval = null;
                                    switch (modifier)
                                    {
                                        case add:
                                            logger.debug("Adding to date: " + operand);
                                            mathval = add(val, operand.getValue());
                                            break;
                                        case subtract:
                                            logger.debug("Subtracting from date: " + operand);
                                            mathval = subtract(val, operand.getValue());
                                            break;
                                    }

                                    cal.set(fieldMap.get(field), mathval);
                                    date.setTime(cal.getTimeInMillis());
                            }
                        
                            out = new DataCell(tcol, date);
                        }
                        else
                        {
                            out = new DataCell(tcol, null);
                        }
                        break;
                    case convertToInterval:
                        tcol.setDataType(DataTypes.INT);
                        Date date2 = null;
                        Integer mathval = null;
                        
                        try
                        {
                            if ( incoming.get(1) != null && incoming.get(1).getValue() != null )
                            {
                                date2 = (Date)incoming.get(1).getValue();
                            }
                        }
                        catch (ClassCastException e)
                        {
                            throw new UnsupportedOperationException("convertToInterval transforms requires two date sources", e);
                        }    
                        
                        if ( date != null && date2 != null )
                        {
                            switch (field)
                            {
                                case DST_OFFSET:
                                case TZ_OFFSET:
                                case TIMEZONE:
                                    throw new UnsupportedOperationException("convertToInterval for timezones fields not supported");
                                default:
                                    int div = 1;
                                    switch (field)
                                    {
                                        case MILLISECOND:
                                            break;
                                        case SECOND:
                                            div = 1000;
                                            break;
                                        case MINUTE:
                                            div = 1000*60;
                                            break;
                                        case HOUR_12:
                                        case HOUR_24:
                                            div = 1000*60*60;
                                            break;
                                        case DAY:
                                            div = 1000*60*60*24;
                                            break;
                                    default:
                                        EnumSet set = EnumSet.of(DateFields.MILLISECOND, DateFields.SECOND, DateFields.MINUTE, DateFields.HOUR_12, DateFields.HOUR_24, DateFields.DAY);
                                        throw new UnsupportedOperationException("Interval conversion to date fields not currently supported, only time fields allowed: " + JavaUtils.enumToList(set));
                                    }
                                    
                                    logger.debug("Calculating difference on " + field + " between date field values: " + date + " and " + date2 + ": denominator=" + div);
                                    Integer diff = subtract(date.getTime()/div, date2.getTime()/div);
                                    //Object val = cal.get(fieldMap.get(field));
                                    //Object val2 = cal2.get(fieldMap.get(field));
                                    //Object val = cal.get(fieldMap.get(field));
                                    mathval = diff;
                                    
                                    //mathval = subtract(val, val2);
                            }
                        
                            out = new DataCell(tcol, mathval);
                        }
                        else
                        {
                            out = new DataCell(tcol, null);
                        }
                        break;
                    case setField:
                        tcol.setDataType(DataTypes.DATE);
                        operand = incoming.get(1);
                        if ( date != null )
                        {
                            switch (field)
                            {
                                case TIMEZONE:
                                    Calendar cal = Calendar.getInstance();
                                    TimeZone tz = TimeZone.getTimeZone(String.valueOf(operand.getValue()));
                                    logger.debug("Setting tz: " + tz);
                                    cal.setTimeZone(tz);
                                    date = cal.getTime();
                                    out = new DataCell(tcol, date);
                                    logger.debug("New tz date: " + date);
                                    break;
                                default:
                                    cal = Calendar.getInstance();
                                    cal.setTime(date);
                                    mathval = add(0, operand.getValue());
                                    cal.set(fieldMap.get(field), mathval);
                                    date.setTime(cal.getTimeInMillis());
                                    out = new DataCell(tcol, date);
                            }
                        }
                        else
                        {
                            out = new DataCell(tcol, null);
                        }
                        break;
                    case removeTimeFromDate:
                        tcol.setDataType(DataTypes.DATE);
                        if ( date != null )
                        {
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(date);
                            cal.set(Calendar.HOUR_OF_DAY, 0);
                            cal.set(Calendar.MINUTE, 0);
                            cal.set(Calendar.SECOND, 0);
                            cal.set(Calendar.MILLISECOND, 0);
                            out = new DataCell(tcol, new Date(cal.getTimeInMillis()));
                        }
                        else
                        {
                            out = new DataCell(tcol, null);
                        }
                        break;
                    case resetDateToEpoch:
                        tcol.setDataType(DataTypes.DATE);
                        if ( date != null )
                        {
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(date);
                            cal.set(Calendar.YEAR, 1970);
                            cal.set(Calendar.DAY_OF_MONTH, 1);
                            cal.set(Calendar.MONTH, Calendar.JANUARY);
                            out = new DataCell(tcol, new Date(cal.getTimeInMillis()));
                        }
                        else
                        {
                            out = new DataCell(tcol, null);
                        }
                        break;
                    case greaterThanOnField:
                        tcol.setDataType(DataTypes.BOOLEAN);
                        operand = incoming.get(1);
                        DataCell otherdate = incoming.get(2);
                        if ( !(otherdate.getValue() instanceof Date) )
                        {
                            throw new TransformException(otherdate, "Operand3 must be a date for greaterThanOnField actions");
                        }
                        
                        if ( date != null )
                        {
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(date);
                            switch (modifier)
                            {
                                case add:
                                    cal.add(fieldMap.get(field), (Integer)operand.getValue());
                                    break;
                                case subtract:
                                    cal.add(fieldMap.get(field), -(Integer)operand.getValue());
                                    break;
                            }
                            
                            Date other = (Date)otherdate.getValue();
                            out = new DataCell(tcol, (other!=null?cal.getTimeInMillis() > other.getTime():false));
                        }
                        else
                        {
                            out = new DataCell(tcol, false);
                        }
                        break;
                    case lessThanOnField:
                        tcol.setDataType(DataTypes.BOOLEAN);
                        operand = incoming.get(1);
                        otherdate = incoming.get(2);
                        if ( !(otherdate.getValue() instanceof Date) )
                        {
                            throw new TransformException(otherdate, "Operand3 must be a date for lessThanOnField actions");
                        }
                        
                        if ( date != null )
                        {
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(date);
                            switch (modifier)
                            {
                                case add:
                                    cal.add(fieldMap.get(field), (Integer)operand.getValue());
                                    break;
                                case subtract:
                                    cal.add(fieldMap.get(field), -(Integer)operand.getValue());
                                    break;
                            }
                            
                            Date other = (Date)otherdate.getValue();
                            out = new DataCell(tcol, (other!=null?cal.getTimeInMillis() < other.getTime():false));
                        }
                        else
                        {
                            out = new DataCell(tcol, false);
                        }
                        break;
                    default:
                        tcol.setDataType(DataTypes.DATE);
                        out = new DataCell(tcol, date);
                        break;
                }
            }
            else
            {
                tcol.setDataType(DataTypes.DATE);
                out = new DataCell(tcol, date);
            }
        }
        catch (ParseException e)
        {
            throw new TransformException(source, "Unable to process Date object using format: " + format.toPattern(), e);
        }
        catch (TransformException e)
        {
            e.setDataCell(source);
            throw e;
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            throw new TransformException(source, "Invalid field to extractField from: " + field, e);
        }
        catch (ETLException e)
        {
            throw new TransformException(source, e);
        }

        targets.add(out);
        return targets;
    }

    private Integer add(Object op1, Object op2)
        throws TransformException, ETLException
    {
        Integer val = null;
        try
        {
            int do1 = Integer.parseInt(String.valueOf(op1));
            int do2 = Integer.parseInt(String.valueOf(op2));
            val = do1 + do2;
        }
        catch (NumberFormatException e)
        {
            throw new TransformException(null, "Unable to convert one of " + op1 + ", " + op2 + " to: INTEGER for addition.", e);
        }
        return val;
    }    
    
    private Integer subtract(Object op1, Object op2)
        throws TransformException, ETLException
    {
        Integer val = null;
        try
        {
            int do1 = Integer.parseInt(String.valueOf(op1));
            int do2 = Integer.parseInt(String.valueOf(op2));
            val = do1 - do2;
        }
        catch (NumberFormatException e)
        {
            throw new TransformException(null, "Unable to convert one of " + op1 + ", " + op2 + " to: INTEGER for subtraction.", e);
        }
        return val;
    }    

    private Long subtractLong(Object op1, Object op2)
        throws TransformException, ETLException
    {
        Long val = null;
        try
        {
            long do1 = Long.parseLong(String.valueOf(op1));
            long do2 = Long.parseLong(String.valueOf(op2));
            val = do1 - do2;
        }
        catch (NumberFormatException e)
        {
            throw new TransformException(null, "Unable to convert one of " + op1 + ", " + op2 + " to: LONG for subtraction.", e);
        }
        return val;
    }    

    @Override
    public String toString()
    {
        Argument src = getSourceColumns().get(0);
        return action + (field != null?"[" + field + "]":"") + "(" + src + ")" + "->" + getTargetColumns().get(0);
    }
}
