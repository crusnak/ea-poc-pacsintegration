/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.transforms;

import com.blackboard.services.etl.data.Argument;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.exception.TransformException;
import com.blackboard.services.etl.exception.TransformIgnoreException;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.ObjectFactory;
import com.blackboard.services.etl.jaxb.StringTransformComplexType;
import com.blackboard.services.etl.jaxb.TransformComplexType.SourceColumn;
import com.blackboard.services.etl.jaxb.TransformComplexType.TargetColumn;
import com.blackboard.services.etl.tasks.AbstractTask;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.blackboard.services.logging.DataType;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 *
 * @author crusnak
 */
public class StringTransform extends AbstractTransform
{
    public static enum Action { split, regexsplit, concatenate, pad, truncate, length, padortruncate, normalize, uppercase, lowercase, capitalize, decimalFormat, trim, ltrim, rtrim, replace, replaceAll, replaceFirst, replaceChars, substring, indexOf, addDelimitedValue, removeDelimitedValue, splitToRows, regexSplitToRows };
    public static enum ActionDirection { left, right };
    public static enum Replace { remove, operand2 };

    private Action action = null;
    private String modifier = null;
    private ActionDirection direction = null;
    private char padChar = 0x00;
    private boolean ignoreRegexErrors = false;
    private boolean ignoreNulls = false;
    private boolean addDupValue = false;
    private Replace replaceWith = Replace.operand2;

    public StringTransform(StringTransformComplexType ct, AbstractTask task)
        throws ETLException
    {
        super(ct, task);
        setStringAction(ct);
        setStringActionDirection(ct);
        setReplaceWith(ct);
        ignoreNulls = ct.isIgnoreNulls();
        if ( action == Action.pad || action == Action.padortruncate || action == Action.normalize )
        {
            padChar = getCharacter(ct.getPadCharacter());
        }
        else if ( action == Action.regexsplit )
        {
            ignoreRegexErrors = ct.isRegexErrorsAsNull();
        }
        else if ( action == Action.addDelimitedValue )
        {
            addDupValue = ct.isAddDuplicateValue();
        }
    }

    public Type getTransformType()
    {
        return Type.string;
    }

    @Override
    public boolean supportsMultipleSources()
    {
        boolean mult = false;
        switch (action)
        {
            case concatenate:
            case replace:
            case replaceAll:
            case replaceFirst:
            case replaceChars:
            case substring:
            case indexOf:
            case addDelimitedValue:
            case removeDelimitedValue:
            case normalize:
            case padortruncate:
                mult = true;
                break;
            case splitToRows:
            case regexSplitToRows:
            case regexsplit:
            case split:
            case pad:
            case truncate:
            case uppercase:
            case lowercase:
            case capitalize:
            case trim:
            case ltrim:
            case rtrim:
            case length:
                mult = false;
                break;
        }
        return mult;
    }

    @Override
    public boolean supportsMultipleTargets()
    {
        boolean mult = false;
        switch (action)
        {
            case trim:
            case ltrim:
            case rtrim:
            case replace:
            case replaceAll:
            case replaceFirst:
            case replaceChars:
            case concatenate:
            case pad:
            case truncate:
            case padortruncate:
            case normalize:
            case uppercase:
            case lowercase:
            case capitalize:
            case substring:
            case indexOf:
            case addDelimitedValue:
            case removeDelimitedValue:
            case splitToRows:
            case regexSplitToRows:
            case length:
                mult = false;
                break;
            case regexsplit:
            case split:
                mult = true;
                break;
        }
        return mult;
    }

    @Override
    public int getExpectedSourceCount()
    {
        int count = 0;
        switch (action)
        {
            case replace:
            case replaceAll:
            case replaceFirst:
            case replaceChars:
            case substring:
            case indexOf:
            case concatenate:
            case normalize:
            case padortruncate:
                count = -1;
                break;
            case splitToRows:
            case regexSplitToRows:
            case regexsplit:
            case split:
            case pad:
            case truncate:
            case uppercase:
            case lowercase:
            case capitalize:
            case trim:
            case ltrim:
            case rtrim:
            case length:
                count = 1;
                break;
            case addDelimitedValue:
            case removeDelimitedValue:
                count = 2;
                break;
        }
        return count;
    }

    private char getCharacter(String d)
        throws ETLException
    {
        char c = 0x00;
        if ( d == null || d.trim().length() == 0 || d.trim().length() > 2 )
        {
            throw new ETLException("Pad character must exist and be a single character: '" + d + "'");
        }

        if ( d.trim().length() == 1 )
        {
            c = d.charAt(0);
        }
        else
        {
            if ( d.trim().equals("\\s") )
            {
                c = '\u0020';
            }
            else
            {
                throw new ETLException("Only escape characters supported as pad characters are: \\s");
            }
        }

        return c;
    }

    private void setStringAction(StringTransformComplexType ct)
        throws ETLException
    {
        if ( ct != null )
        {
            action = Action.valueOf(ct.getAction());
            modifier = ct.getActionModifier();
            if ( modifier == null )
            {
                modifier = "";
            }

            // Ensure a number was passed to actionModifier for pad or truncation actions.
            if ( action == Action.pad || action == Action.truncate || ( action == Action.padortruncate && getSourceColumns().size() == 1 ) || 
                 ( action == Action.normalize && getSourceColumns().size() == 1 ))
            {
                try
                {
                    Integer.parseInt(modifier);
                }
                catch (NumberFormatException e)
                {
                    if ( !task.getETLJobWrapper().getDataArchiver().containsBeanScript(modifier) )
                    {
                        throw new ETLException("Expecting a number in actionModifier (indicating number of characters)", e);
                    }
                }

            }
        }
    }

    private void setStringActionDirection(StringTransformComplexType ct)
    {
        if ( ct != null )
        {
            switch (action)
            {
                case pad:
                case truncate:
                case padortruncate:
                case normalize:
                case indexOf:
                case addDelimitedValue:
                case removeDelimitedValue:
                    direction = ActionDirection.valueOf(ct.getActionDirection());  
                    break;
            }
        }
    }

    private void setReplaceWith(StringTransformComplexType ct)
    {
        if ( ct != null )
        {
            switch (action)
            {
                case replace:
                case replaceAll:
                case replaceChars:
                    replaceWith = Replace.valueOf(ct.getReplaceWith());  
                    break;
            }
        }
    }

    public List<DataCell> transform(List<DataColumn> cols, List<DataCell> incoming)
        throws ETLException
    {
        ArrayList<DataCell> tlist = new ArrayList();
        DataCell source = null;
        DataColumn tcol = null;

        validateIncomingSources(incoming);

        try
        {
            source = incoming.get(0);
            if ( ignoreNulls && ( source == null || source.getValue() == null ) )
            {
                logger.debug("Source is null and ignoreNulls=true, not performing String transform.");
                tcol = getTransformDataColumn(cols, incoming, 0);
                tcol.setDataType(DataTypes.STRING);
                tlist.add(new DataCell(tcol, null));
            }
            else
            {
                switch (action)
                {
                    case concatenate:
                        // Can only concatenate to a single target column
                        String cat = "";
                        tcol = getTransformDataColumn(cols, incoming, 0);
                        tcol.setDataType(DataTypes.STRING);
                        for ( int i=0; i<incoming.size(); i++ )
                        {
                            source = incoming.get(i);
                            if ( source.getValue() != null )
                            {
                                cat += task.getETLJobWrapper().getDataArchiver().processEmbeddedBeanScript(String.valueOf(source.getValue()));
                            }

                            if ( i < incoming.size()-1 )
                            {
                                cat += modifier;
                            }
                        }

                        tlist.add(new DataCell(tcol, cat));
                        break;
                    case regexsplit:
                        // Can only split on a single incoming column
                        
                        Pattern p = Pattern.compile(task.getETLJobWrapper().getDataArchiver().processEmbeddedBeanScript(modifier));
                        if ( source != null && source.getValue() != null )
                        {
                            Matcher m = p.matcher(String.valueOf(source.getValue()));
                            if ( m.matches() )
                            {
                                for ( int i=1; i<=targets.size(); i++ )
                                {
                                    tcol = getTransformDataColumn(cols, incoming, i-1);
                                    tcol.setDataType(DataTypes.STRING);
                                    if ( i <= m.groupCount() )
                                    {
                                        tlist.add(new DataCell(tcol, m.group(i)));
                                    }
                                    else
                                    {
                                        tlist.add(new DataCell(tcol, null));
                                    }
                                }
                            }
                            else if ( ignoreRegexErrors )
                            {
                                for ( int i=1; i<=targets.size(); i++ )
                                {
                                    tcol = getTransformDataColumn(cols, incoming, i-1);
                                    tcol.setDataType(DataTypes.STRING);
                                    tlist.add(new DataCell(tcol, null));
                                }
                            }
                            else
                            {
                                throw new TransformException(source, "Incoming string did not match regex expression: " + modifier);
                            }
                        }
                        else
                        {
                            for ( int i=1; i<=targets.size(); i++ )
                            {
                                tcol = getTransformDataColumn(cols, incoming, i-1);
                                tcol.setDataType(DataTypes.STRING);
                                tlist.add(new DataCell(tcol, null));
                            }
                        }
                        break;
                    case pad:
                        tcol = getTransformDataColumn(cols, incoming, 0);
                        tcol.setDataType(DataTypes.STRING);
                        String string = "";
                        if ( source != null && source.getValue() != null )
                        {
                            string = String.valueOf(source.getValue());
                        }

                        if ( task != null )
                        {
                            modifier = task.getETLJobWrapper().getDataArchiver().processEmbeddedBeanScript(modifier);
                        }
                        
                        int size = Integer.parseInt(modifier);
                        if ( string.length() > size )
                        {
                            throw new TransformException(source, "Incoming string length is greater than the specified size: " + modifier);
                        }
                        else
                        {
                            while ( string.length() < size )
                            {
                                switch (direction)
                                {
                                    case left:
                                        string = padChar + string;
                                        break;
                                    case right:
                                        string += padChar;
                                        break;
                                }
                            }
                        }

                        tlist.add(new DataCell(tcol, string));
                        break;
                    case truncate:
                        tcol = getTransformDataColumn(cols, incoming, 0);
                        tcol.setDataType(DataTypes.STRING);
                        string = "";
                        if ( source != null && source.getValue() != null )
                        {
                            string = String.valueOf(source.getValue());
                        }

                        if ( task != null )
                        {
                            modifier = task.getETLJobWrapper().getDataArchiver().processEmbeddedBeanScript(modifier);
                        }
                        
                        int amount = Integer.parseInt(modifier);
                        int strlen = string.length();
                        logger.debug("Truncate to: " + amount);
                        logger.debug(string + " length=" + strlen);
                        if ( string.length() > amount )
                        {
                            for ( int i=0; i<strlen-amount; i++ )
                            {
                                switch (direction)
                                {
                                    case left:
                                        string = string.substring(1);
                                        break;
                                    case right:
                                        string = string.substring(0, string.length()-1);
                                        break;
                                }
                            }
                        }

                        tlist.add(new DataCell(tcol, string));
                        break;
                    case normalize:
                    case padortruncate:
                        tcol = getTransformDataColumn(cols, incoming, 0);
                        tcol.setDataType(DataTypes.STRING);
                        string = "";
                        amount = 0;
                        
                        if ( incoming.size() == 2 )
                        {
                            if ( source != null && source.getValue() != null )
                            {
                                amount = Integer.parseInt(String.valueOf(source.getValue()));
                            }

                            if ( incoming.get(1) != null && incoming.get(1).getValue() != null )
                            {
                                string = String.valueOf(incoming.get(1).getValue());
                            }
                        }
                        else if ( incoming.size() == 1 )
                        {
                            // Pad string to desired length if needed
                            if ( task != null )
                            {
                                modifier = task.getETLJobWrapper().getDataArchiver().processEmbeddedBeanScript(modifier);
                            }

                            amount = Integer.parseInt(modifier);

                            if ( source != null && source.getValue() != null )
                            {
                                string = String.valueOf(source.getValue());
                            }
                        }
                        else
                        {
                            throw new ETLException("Unable to perform normalize/padortruncate transform since the source count is not 1 or 2: " + incoming.size());
                        }

                        logger.debug("Normalize to: " + amount);
                        logger.debug("Padding if needed: " + string + ": " + string.length());
                        if ( string.length() < amount )
                        {
                            while ( string.length() < amount )
                            {
                                switch (direction)
                                {
                                    case left:
                                        string = padChar + string;
                                        break;
                                    case right:
                                        string += padChar;
                                        break;
                                }
                            }
                        }

                        logger.debug("Truncating if needed: " + string + ": " + string.length());

                        // Truncate extra characters over desired length if needed
                        if ( string.length() > amount )
                        {
                            int orig = string.length();
                            for ( int i=0; i<orig-amount; i++ )
                            {
                                switch (direction)
                                {
                                    case left:
                                        string = string.substring(1);
                                        break;
                                    case right:
                                        string = string.substring(0, string.length()-1);
                                        break;
                                }
                            }
                        }

                        tlist.add(new DataCell(tcol, string));
                        break;
                    case length:
                        tcol = getTransformDataColumn(cols, incoming, 0);
                        tcol.setDataType(DataTypes.INT);
                        int len = 0;
                        if ( source != null && source.getValue() != null )
                        {
                            len = String.valueOf(source.getValue()).length();
                        }
                        
                        tlist.add(new DataCell(tcol, len));
                        break;
                    case uppercase:
                        tcol = getTransformDataColumn(cols, incoming, 0);
                        tcol.setDataType(DataTypes.STRING);
                        string = "";
                        if ( source != null && source.getValue() != null )
                        {
                            string = String.valueOf(source.getValue());
                        }

                        tlist.add(new DataCell(tcol, string.toUpperCase()));
                        break;
                    case lowercase:
                        tcol = getTransformDataColumn(cols, incoming, 0);
                        tcol.setDataType(DataTypes.STRING);
                        string = "";
                        if ( source != null && source.getValue() != null )
                        {
                            string = String.valueOf(source.getValue());
                        }

                        tlist.add(new DataCell(tcol, string.toLowerCase()));
                        break;
                    case capitalize:
                        tcol = getTransformDataColumn(cols, incoming, 0);
                        tcol.setDataType(DataTypes.STRING);
                        string = "";
                        if ( source != null && source.getValue() != null )
                        {
                            string = String.valueOf(source.getValue());
                        }

                        char[] chars = string.toCharArray();
                        string = "";
                        boolean space = true;
                        for ( char c: chars )
                        {
                            if ( !Character.isWhitespace(c) && space )
                            {
                                string += Character.toTitleCase(c);
                                space = false;
                            }
                            else if ( !Character.isWhitespace(c) )
                            {
                                string += Character.toLowerCase(c);
                            }
                            else
                            {
                                string += c;
                                space = true;
                            }
                        }

                        tlist.add(new DataCell(tcol, string));
                        break;
                    case decimalFormat:
                        DecimalFormat df = new DecimalFormat(modifier);
                        tcol = getTransformDataColumn(cols, incoming, 0);
                        tcol.setDataType(DataTypes.STRING);
                        string = df.format(source.getValue());
                        tlist.add(new DataCell(tcol, string));
                        break;
                    case trim:
                        tcol = getTransformDataColumn(cols, incoming, 0);
                        tcol.setDataType(DataTypes.STRING);
                        string = null;
                        if ( source.getValue() != null )
                        {
                            string = String.valueOf(source.getValue()).trim();
                        }
                        tlist.add(new DataCell(tcol, string));
                        break;
                    case replace:
                    case replaceAll:
                        tcol = getTransformDataColumn(cols, incoming, 0);
                        tcol.setDataType(DataTypes.STRING);
                        String replace = modifier;
                        String with = null;

                        if ( incoming.size() == 3 )
                        {
                            if ( incoming.get(1) != null && incoming.get(1).getValue() != null )
                            {
                                replace = String.valueOf(incoming.get(1).getValue());
                            }

                            if ( incoming.get(2) != null && incoming.get(2).getValue() != null )
                            {
                                with = String.valueOf(incoming.get(2).getValue());
                            }
                        }
                        else if ( incoming.size() == 2 ) 
                        {
                            if ( modifier == null )
                            {
                                throw new ETLException("Unable to perform replace transform since required attribute actionModifier is null with source count=2.");
                            }
                            else if ( incoming.get(1) != null && incoming.get(1).getValue() != null )
                            {
                                with = String.valueOf(incoming.get(1).getValue());
                            }
                        }
                        else
                        {
                            throw new ETLException("Unable to perform replace transform since the source count is not 2 or 3: " + incoming.size());
                        }

                        if ( replace == null )
                        {
                            throw new TransformIgnoreException(null, "Replace value is null, not performing replace transformation");
                        }

                        if ( with == null )
                        {
                            with = "";
                        }

                        string = null;
                        if ( source.getValue() != null )
                        {
                            string = String.valueOf(source.getValue()).replaceAll(replace, with);
                        }
                        tlist.add(new DataCell(tcol,string));
                        break;
                    case replaceFirst:
                        tcol = getTransformDataColumn(cols, incoming, 0);
                        tcol.setDataType(DataTypes.STRING);
                        replace = modifier;
                        with = null;

                        if ( incoming.size() == 3 )
                        {
                            if ( incoming.get(1) != null && incoming.get(1).getValue() != null )
                            {
                                replace = String.valueOf(incoming.get(1).getValue());
                            }

                            if ( incoming.get(2) != null && incoming.get(2).getValue() != null )
                            {
                                with = String.valueOf(incoming.get(2).getValue());
                            }
                        }
                        else if ( incoming.size() == 2 ) 
                        {
                            if ( modifier == null )
                            {
                                throw new ETLException("Unable to perform replace transform since required attribute actionModifier is null with source count=2.");
                            }
                            else if ( incoming.get(1) != null && incoming.get(1).getValue() != null )
                            {
                                with = String.valueOf(incoming.get(1).getValue());
                            }
                        }
                        else
                        {
                            throw new ETLException("Unable to perform replace transform since the source count is not 2 or 3: " + incoming.size());
                        }

                        if ( replace == null )
                        {
                            throw new TransformIgnoreException(null, "Replace value is null, not performing replace transformation");
                        }

                        if ( with == null )
                        {
                            with = "";
                        }

                        string = null;
                        if ( source.getValue() != null )
                        {
                            string = String.valueOf(source.getValue()).replaceFirst(replace, with);
                        }
                        tlist.add(new DataCell(tcol,string));
                        break;
                    case replaceChars:
                        tcol = getTransformDataColumn(cols, incoming, 0);
                        tcol.setDataType(DataTypes.STRING);
                        char w = 0x00;

                        if ( modifier == null )
                        {
                            throw new ETLException("Unable to perform replaceEach transform since required attribute actionModifier is null.");
                        }

                        if ( replaceWith == Replace.remove )
                        {
                            string = null;
                            String temp = "";
                            if ( source.getValue() != null )
                            {
                                ArrayList<Character> charlist = new ArrayList();
                                for ( char c: modifier.toCharArray() )
                                {
                                    charlist.add(c);
                                }
                                
                                // Only add a character if it isn't in the remove list
                                for ( char c: String.valueOf(source.getValue()).toCharArray() )
                                {
                                    if ( !charlist.contains(c) )
                                    {
                                        temp += c;
                                    }
                                }
                                
                                // If after removing characters there are characters remaining, assign this
                                // as the output, otherwise assign null
                                if ( temp.trim().length() > 0 )
                                {
                                    string = temp;
                                }
                            }
                        }
                        else
                        {
                            if ( incoming.get(1) != null && incoming.get(1).getValue() != null )
                            {
                                w = String.valueOf(incoming.get(1).getValue()).charAt(0);
                            }

                            string = null;
                            if ( source.getValue() != null )
                            {
                                string = String.valueOf(source.getValue());
                                for ( char c: modifier.toCharArray() )
                                {
                                    string = string.replace(c, w);
                                }
                            }
                        }
                        
                        tlist.add(new DataCell(tcol,string));
                        break;
                    case substring:
                        tcol = getTransformDataColumn(cols, incoming, 0);
                        tcol.setDataType(DataTypes.STRING);
                        int start = 0;
                        int end = -1;

                        if ( incoming.size() == 3 )
                        {
                            DataCell s2 = incoming.get(1);
                            DataCell s3 = incoming.get(2);

                            if ( s2 == null )
                            {
                                throw new TransformException(s2, "Second source for substring transform must be a number and not null");
                            }

                            if ( s3 == null )
                            {
                                throw new TransformException(s3, "Third source for substring transform must be a number and not null");
                            }
                                
                            try
                            {
                                start = Integer.parseInt(String.valueOf(s2.getValue()));
                            }
                            catch (NumberFormatException e)
                            {
                                throw new TransformException(s2, "Second source for substring transform must be a number", e);
                            }

                            try
                            {
                                end = Integer.parseInt(String.valueOf(s3.getValue()));
                            }
                            catch (NumberFormatException e)
                            {
                                throw new TransformException(s3, "Second source for substring transform must be a number", e);
                            }
                        }
                        else if ( incoming.size() == 2 )
                        {
                            DataCell s2 = incoming.get(1);

                            if ( s2 == null )
                            {
                                throw new TransformException(s2, "Second source for substring transform must be a number and not null");
                            }

                            try
                            {
                                start = Integer.parseInt(String.valueOf(s2.getValue()));
                            }
                            catch (NumberFormatException e)
                            {
                                throw new TransformException(s2, "Second source for substring transform must be a number", e);
                            }
                        }
                        else
                        {
                            throw new ETLException("Unable to perform substring transform since the source count is not 2 or 3: " + incoming.size());
                        }

                        string = null;
                        if ( start - end != 0 )
                        {
                            if ( source.getValue() != null && end < 0 )
                            {
                                string = String.valueOf(source.getValue()).substring(start);
                            }
                            else if ( source.getValue() != null )
                            {
                                string = String.valueOf(source.getValue()).substring(start, end);
                            }
                        }

                        tlist.add(new DataCell(tcol,string));
                        break;
                    case indexOf:
                        tcol = getTransformDataColumn(cols, incoming, 0);
                        tcol.setDataType(DataTypes.INT);

                        if ( incoming.size() == 2 )
                        {
                            DataCell s2 = incoming.get(1);

                            if ( s2 == null )
                            {
                                throw new TransformException(s2, "Second source for indexOf transform must be a string and not null");
                            }
                            
                            modifier = String.valueOf(s2.getValue());
                        }

                        int index = -1;
                        if ( source.getValue() != null )
                        {
                            switch ( direction )
                            {
                                case left:
                                    index = String.valueOf(source.getValue()).indexOf(modifier);
                                    break;
                                case right:
                                    index = String.valueOf(source.getValue()).lastIndexOf(modifier);
                                    break;
                            }
                        }

                        tlist.add(new DataCell(tcol,index));
                        break;
                    case addDelimitedValue:
                        tcol = getTransformDataColumn(cols, incoming, 0);
                        tcol.setDataType(DataTypes.STRING);

                        if ( modifier == null )
                        {
                            throw new ETLException("Unable to perform addDelimitedValue transform since required attribute actionModifier is null.");
                        }
                        
                        DataCell list = incoming.get(0);
                        DataCell val = incoming.get(1);
                        
                        logger.debug("List=" + list);
                        logger.debug("Adding=" + val);
                        
                        if ( list == null || list.getValue() == null )
                        {
                            string = (val != null && val.getValue() != null?String.valueOf(val.getValue()):"");
                        }
                        else
                        { 
                            string = String.valueOf(list.getValue());

                            String[] ids = string.split(modifier);
                            ArrayList<String> idlist = new ArrayList(Arrays.asList(ids));
                            logger.debug("List before add: " + idlist);

                            // If the direction is on the left add from the beginning of the list
                            if ( direction == ActionDirection.left )
                            {
                                Collections.reverse(idlist);
                            }

                            if ( val != null && val.getValue() != null )
                            {
                                if ( addDupValue || !idlist.contains(String.valueOf(val.getValue())) )
                                {
                                    idlist.add(String.valueOf(val.getValue()));
                                }

                                // If the direction is on the left add reverse the list back to normal after add
                                if ( direction == ActionDirection.left )
                                {
                                    Collections.reverse(idlist);
                                }
                            }

                            string = "";
                            for ( int i=0; i<idlist.size(); i++ )
                            {
                                string += idlist.get(i);
                                if ( i < idlist.size() - 1 )
                                {
                                    string += modifier;
                                }
                            }
                        }
                        
                        tlist.add(new DataCell(tcol, string));
                        break;
                    case removeDelimitedValue:
                        tcol = getTransformDataColumn(cols, incoming, 0);
                        tcol.setDataType(DataTypes.STRING);

                        if ( modifier == null )
                        {
                            throw new ETLException("Unable to perform removeDelimitedValue transform since required attribute actionModifier is null.");
                        }
                        
                        list = incoming.get(0);
                        val = incoming.get(1);
                        
                        logger.debug("List=" + list);
                        logger.debug("Removing=" + val);
                        
                        if ( list == null || list.getValue() == null )
                        {
                            string = null;
                        }
                        else
                        { 
                            string = String.valueOf(list.getValue());

                            String[] ids = string.split(modifier);
                            ArrayList<String> idlist = new ArrayList(Arrays.asList(ids));
                            logger.debug("List before removal: " + idlist);
                            
                            // If the direction is on the right remove from the end of the list
                            if ( direction == ActionDirection.right )
                            {
                                Collections.reverse(idlist);
                            }
                            
                            // Remove the provided value from the list
                            if ( val != null && val.getValue() != null )
                            {
                                idlist.remove(String.valueOf(val.getValue()));

                                // If the direction is on the right reverse the list back to normal after removal
                                if ( direction == ActionDirection.right )
                                {
                                    Collections.reverse(idlist);
                                }
                            }
                            
                            string = "";
                            for ( int i=0; i<idlist.size(); i++ )
                            {
                                string += idlist.get(i);
                                if ( i < idlist.size() - 1 )
                                {
                                    string += modifier;
                                }
                            }
                        }
                        
                        tlist.add(new DataCell(tcol, string));
                        break;
                    case split:
                        // Can only split on a single incoming column
                        if ( source != null && source.getValue() != null )
                        {
                            String[] split = String.valueOf(source.getValue()).split(modifier);
                            for ( int i=0; i<targets.size(); i++ )
                            {
                                tcol = getTransformDataColumn(cols, incoming, i);
                                tcol.setDataType(DataTypes.STRING);
                                try
                                {
                                    tlist.add(new DataCell(tcol, split[i]));
                                }
                                catch (ArrayIndexOutOfBoundsException e)
                                {
                                    tlist.add(new DataCell(tcol, null));
                                }
                            }
                        }
                        else
                        {
                            for ( int i=0; i<targets.size(); i++ )
                            {
                                tcol = getTransformDataColumn(cols, incoming, i);
                                tcol.setDataType(DataTypes.STRING);
                                tlist.add(new DataCell(tcol, null));
                            }
                        }
                        break;
                    case splitToRows:
                        // Can only split on a single incoming column - for this to work properly this can be the only transform in a data mapping task
                        
                        /*logger.debug("splitToRows modifier: " + modifier);
                        if ( source != null && source.getValue() != null )
                        {
                            String[] items = String.valueOf(source.getValue()).split("\\" + modifier);
                            for ( int i=0; i<items.length; i++ )
                            {
                                logger.debug(items[i]);
                            }
                            tcol = getTransformDataColumn(cols, incoming, 0);
                            tcol.setDataType(DataTypes.OBJECT);
                            tlist.add(new DataCell(tcol, new ArrayList<String>(Arrays.asList(items))));
                        }
                        else
                        {
                            for ( int i=1; i<=targets.size(); i++ )
                            {
                                tcol = getTransformDataColumn(cols, incoming, 0);
                                tcol.setDataType(DataTypes.STRING);
                                tlist.add(new DataCell(tcol, null));
                            }
                        }*/

                    case ltrim:
                    case rtrim:
                    case regexSplitToRows:
                        throw new UnsupportedOperationException("Action ltrim/rtrim/splitToRows/regexSplitToRows not supported yet");
                        //break;
                }
            }
        }
        catch (TransformException e)
        {
            throw e;
        }
        catch (ETLException e)
        {
            throw new TransformException(source, e);
        }
        catch (IndexOutOfBoundsException e)
        {
            throw new TransformException(source, e);
        }
        catch (PatternSyntaxException e)
        {
            throw new TransformException(source, e);
        }
        catch (IllegalArgumentException e)
        {
            throw new TransformException(source, e);
        }
        return tlist;
    }

    public String toString()
    {
        return action + "(" + getSourceColumns() + ")->" + getTargetColumns();
    }
    
    public static void main(String[] args)
    {
        try
        {
            ObjectFactory factory = new ObjectFactory();
            StringTransformComplexType ct = factory.createStringTransformComplexType();
            ct.setAction("substring");
            SourceColumn sc = factory.createTransformComplexTypeSourceColumn();
            sc.setDataType(DataTypes.STRING);
            sc.setType("constant");
            sc.setValue("this is a test");
            ct.getSourceColumn().add(sc);

            sc = factory.createTransformComplexTypeSourceColumn();
            sc.setDataType(DataTypes.INT);
            sc.setType("constant");
            sc.setValue("0");
            ct.getSourceColumn().add(sc);

            sc = factory.createTransformComplexTypeSourceColumn();
            sc.setDataType(DataTypes.INT);
            sc.setType("constant");
            sc.setValue("5");
            ct.getSourceColumn().add(sc);
            
            TargetColumn tc = factory.createTransformComplexTypeTargetColumn();
            tc.setValue("OUTPUT");
            ct.getTargetColumn().add(tc);
            
            StringTransform st = new StringTransform(ct, null);
            System.out.println(st);
            
            ArrayList<DataCell> incoming = new ArrayList();
            incoming.add(new DataCell(new DataColumn(DataTypes.STRING, null), "this is a test"));
            incoming.add(new DataCell(new DataColumn(DataTypes.INT, null), "5"));
            incoming.add(new DataCell(new DataColumn(DataTypes.INT, null), "5"));
            
            System.out.println(st.transform(incoming));
        }
        catch (Exception e)
        {
            e.printStackTrace(System.err);
        }
    }
}
