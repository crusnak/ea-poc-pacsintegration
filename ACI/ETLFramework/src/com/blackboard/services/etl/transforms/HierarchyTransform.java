/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.transforms;

import com.blackboard.services.etl.data.BbJSONContainer;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.JacksonJsonContainer;
import com.blackboard.services.etl.data.TransformSource;
import com.blackboard.services.etl.data.TransformTarget;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.exception.TransformException;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.HierarchyTransformComplexType;
import com.blackboard.services.etl.jaxb.ObjectFactory;
import com.blackboard.services.etl.jaxb.TransformComplexType;
import com.blackboard.services.etl.tasks.AbstractTask;
import com.blackboard.services.etl.tasks.web.WebAPIDriver.NodeType;
import com.blackboard.services.utils.XMLUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import java.io.IOException;
import java.nio.file.InvalidPathException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author crusnak
 */
public class HierarchyTransform extends AbstractTransform
{
    public static enum Action { fromXML, fromJSON, toXML, toJSON, setField, getField, addField, removeField };
    public static enum InvalidPath { NULL, ERROR };
    public static enum HierarchyType { json, xml };

    protected static HashMap<Integer,NodeType> nodeTypeMap = new HashMap();
    
    private Action action = null;
    private InvalidPath invalid = InvalidPath.NULL;
    private HierarchyType htype = HierarchyType.json;
    private String root = null;
    private boolean arrayAsList = false;
    private char listDelimiter = Character.UNASSIGNED;
    
    static
    {
        nodeTypeMap.put(1, NodeType.ELEMENT_NODE);
        nodeTypeMap.put(2, NodeType.ATTRIBUTE_NODE);
        nodeTypeMap.put(3, NodeType.TEXT_NODE);
        nodeTypeMap.put(4, NodeType.CDATA_SECTION_NODE);
        nodeTypeMap.put(5, NodeType.ENTITY_REFERENCE_NODE);
        nodeTypeMap.put(6, NodeType.ENTITY_NODE);
        nodeTypeMap.put(7, NodeType.PROCESSING_INSTRUCTION_NODE);
        nodeTypeMap.put(8, NodeType.COMMENT_NODE);
        nodeTypeMap.put(9, NodeType.DOCUMENT_NODE);
        nodeTypeMap.put(10, NodeType.DOCUMENT_TYPE_NODE);
        nodeTypeMap.put(11, NodeType.DOCUMENT_FRAGMENT_NODE);
        nodeTypeMap.put(12, NodeType.NOTATION_NODE);
    }
    
    public HierarchyTransform(HierarchyTransformComplexType ct, AbstractTask task)
        throws ETLException
    {
        super(ct, task);
        setAction(ct);
        setInvalidPathHandler(ct);
        setHierarchyType(ct);
        setRootElement(ct.getRootElement());
        
        arrayAsList = ct.isArrayAsList();
        if ( arrayAsList == true )
        {
            String ld = ct.getListDelimiter();
            if ( ld == null )
            {
                throw new IllegalArgumentException("If arrayAsList=true, then listDelimiter must be defined for HierarchyTranforms");
            }
            
            listDelimiter = ld.charAt(0);
        }
    }
    
    @Override
    public Type getTransformType() 
    {
        return Type.hierarchy;
    }
    
    @Override
    public boolean supportsMultipleSources() 
    {
        boolean mult = false;
        switch ( action )
        {
            case setField:
            case toJSON:
            case toXML:
            case addField:
            case removeField:
                mult = true;
                break;
            case getField:
            case fromJSON:
            case fromXML:
                mult = false;
                break;
        }
        
        return mult;
    }

    @Override
    public boolean supportsMultipleTargets() 
    {
        boolean mult = false;
        switch ( action )
        {
            case setField:
            case toJSON:
            case toXML:
            case addField:
            case removeField:
                mult = false;
                break;
            case getField:
            case fromJSON:
            case fromXML:
                mult = true;
                break;
        }
        
        return mult;
    }
    
    @Override
    public int getExpectedSourceCount() 
    {
        int count = 0;
        switch ( action )
        {
            case setField:
            case toJSON:
            case toXML:
                count = -1;
                break;
            case getField:
            case fromJSON:
            case fromXML:
                count = 1;
                break;
            case addField:
            case removeField:
                count = 2;
                break;
        }
        
        return count;
    }
    
    private void setAction(HierarchyTransformComplexType ct)
        throws ETLException
    {
        if ( ct != null )
        {
            action = Action.valueOf(ct.getAction());
        }
    }
    
    public Action getAction()
    {
        return action;
    }
    
    private void setInvalidPathHandler(HierarchyTransformComplexType ct)
        throws ETLException
    {
        if ( ct != null )
        {
            invalid = InvalidPath.valueOf(ct.getInvalidPathAs());
        }
    }
    
    public InvalidPath getInvalidPathHandler()
    {
        return invalid;
    }

    private void setHierarchyType(HierarchyTransformComplexType ct)
        throws ETLException
    {
        if ( ct != null )
        {
            htype = HierarchyType.valueOf(ct.getType());
        }
    }
    
    public HierarchyType getHierarchyType()
    {
        return htype;
    }

    public void setRootElement(String elem)
        throws ETLException
    {
        //if ( elem == null )
        //{
        //    throw new ETLException("rootElement must be defined for HierarchyTransforms");
        //}
        
        root = elem;
    }
    
    public String getRootElement()
    {
        return root;
    }

    @Override
    public List<DataCell> transform(List<DataColumn> cols, List<DataCell> incoming) throws ETLException 
    {
        ArrayList<DataCell> tlist = new ArrayList();
        DataCell source = null;
        DataColumn tcol = null;
        Map<String,DataColumn> tcolMap = new HashMap();

        logger.debug("HierarchyTransform action=" + action);
        logger.debug("HierarchyTransform root=" + root);
        logger.debug("HierarchyTransform sources=" + incoming);
        logger.debug("HierarchyTransform targets=" + cols);

        // Create a lookup map by column name so the TransformTarget can find the DataColumn
        for ( int i=0; i<targets.size(); i++ )
        {
            DataColumn col = getTransformDataColumn(cols, incoming, i);
            tcolMap.put(col.getColumnName(), col);
        }
        
        logger.debug("ColumnMap: " + tcolMap);
        
        validateIncomingSources(incoming);
        
        try
        {
            source = incoming.get(0);
            switch ( action )
            {
                case fromJSON:
                    throw new UnsupportedOperationException("fromJSON hierarchy transforms not supported yet");
                case fromXML:
                    Document xml = XMLUtils.parseXML(String.valueOf(source.getValue()));
                    Map<String,List<TransformTarget>> nodeMap = new HashMap();
                    logger.debug("Incoming xml: " + XMLUtils.toString(xml, false));
                    
                    for ( int i=0; i<cols.size(); i++ )
                    {
                        TransformTarget t = getTargetColumns().get(i);
                        DataColumn col = tcolMap.get(t.getColumnName());
                        if ( col.getDataType() == DataTypes.NODE )
                        {
                            throw new UnsupportedOperationException("fromXML hierarchy transforms do not support NODE xpath targets yet");
                        }
                        else if ( t.getRefNode() != null )
                        {
                            throw new UnsupportedOperationException("fromXML hierarchy transforms do not support NODE xpath target references yet");
                        }
                        else
                        {
                            DataCell cell = extractXpathValue(t.getPath(), col, xml);
                            logger.debug("Xpath evaulated " + t.getPath() + "=" + cell);
                            tlist.add(cell);
                        }
                    }
                    break;
                case toJSON:
                    if ( source != null && source.getCellClass() == BbJSONContainer.class )
                    {
                        logger.debug("First source column value is json: " + source);
                        //logger.debug(((BbJSONContainer)source.getValue()).getXMLDocument(root));
                    }
                    break;
                case toXML:
                    throw new UnsupportedOperationException("toXML hierarchy transforms not supported yet");
                case getField:
                    String temproot = null;
                    
                    switch (htype)
                    {
                        case json:
                            // If first source column is not a JSON datatype, attempt to convert it to one
                            //if ( source != null & source.getValue() != null && source.getCellClass() != BbJSONContainer.class )
                            if ( source != null && source.getValue() != null && source.getCellClass() != JacksonJsonContainer.class )
                            {
                                //temproot = "json";
                                source.setColumn(new DataColumn(DataTypes.JSON, "source"));
                                source.setValue(new JacksonJsonContainer(String.valueOf(source.getValue())));
                            }

                            // Validate first source column is of JSON datatype
                            //if ( source != null && source.getValue() != null && source.getCellClass() == BbJSONContainer.class )
                            if ( source != null && source.getValue() != null && source.getCellClass() == JacksonJsonContainer.class )
                            {
                                JacksonJsonContainer sjson = (JacksonJsonContainer)source.getValue();
                                logger.debug("First source column value is json: " + sjson);
                                for ( int i=0; i<cols.size(); i++ )
                                {
                                    TransformTarget t = getTargetColumns().get(i);
                                    DataColumn col = tcolMap.get(t.getColumnName());
                                    col.setDataType(DataTypes.STRING);

                                    try
                                    {
                                        String path = (temproot ==  null ? t.getPath() : "/" + temproot + t.getPath());
                                        logger.trace("Finding json path: " + path);

                                        if ( arrayAsList == true )
                                        {
                                            JsonNode pn = sjson.getParentFieldValue(path);
                                            logger.trace("Parent node: " + pn);
                                            if ( pn.getNodeType() == JsonNodeType.ARRAY )
                                            {
                                                DataCell cell = new DataCell(col, sjson.getArrayedListFieldValue(path, listDelimiter));
                                                tlist.add(cell);
                                            }
                                            else
                                            {
                                                DataCell cell = new DataCell(col, sjson.getStringFieldValue(path));
                                                tlist.add(cell);
                                            }
                                        }
                                        else
                                        {
                                            DataCell cell = new DataCell(col, sjson.getStringFieldValue(path));
                                            tlist.add(cell);
                                        }
                                    }
                                    catch (InvalidPathException e)
                                    {
                                        switch (invalid)
                                        {
                                            case NULL:
                                                tlist.add(new DataCell(col, null));
                                                break;
                                            default:
                                                throw new TransformException(source, "Unable to access field using path: " + t.getPath() , e);
                                        }
                                    }
                                }
                            }
                            else if ( source == null || source.getValue() == null )
                            {
                                for ( int i=0; i<cols.size(); i++ )
                                {
                                    TransformTarget t = getTargetColumns().get(i);
                                    tlist.add(new DataCell(tcolMap.get(t.getColumnName()), null));
                                }
                            }
                            else
                            {
                                throw new TransformException(source, "sourceColumn must be of type json");
                            }
                        case xml:
                            
                            if ( source != null && source.getValue() != null )
                            {
                                xml = XMLUtils.parseXML(String.valueOf(source.getValue()));
                                nodeMap = new HashMap();
                                logger.debug("Incoming xml: " + XMLUtils.toString(xml, false));

                                for ( int i=0; i<cols.size(); i++ )
                                {
                                    TransformTarget t = getTargetColumns().get(i);
                                    DataColumn col = tcolMap.get(t.getColumnName());
                                    if ( col.getDataType() == DataTypes.NODE )
                                    {
                                        throw new UnsupportedOperationException("getField xml hierarchy transforms do not support NODE xpath targets yet");
                                    }
                                    else if ( t.getRefNode() != null )
                                    {
                                        throw new UnsupportedOperationException("getField xml hierarchy transforms do not support NODE xpath target references yet");
                                    }
                                    else
                                    {
                                        DataCell cell = extractXpathValue(t.getPath(), col, xml);
                                        logger.debug("Xpath evaulated " + t.getPath() + "=" + cell);
                                        tlist.add(cell);
                                    }
                                }
                            }
                            else if ( source == null || source.getValue() == null )
                            {
                                for ( int i=0; i<cols.size(); i++ )
                                {
                                    TransformTarget t = getTargetColumns().get(i);
                                    tlist.add(new DataCell(tcolMap.get(t.getColumnName()), null));
                                }
                            }
                            
                            break;
                        default:
                            throw new UnsupportedOperationException("Unknown HierarchyType set: " + htype);
                    }
            
                    break;
                case setField:
                    tcol = getTransformDataColumn(cols, incoming, 0);
                    tcol.setDataType(DataTypes.JSON);
                    
                    // If first source column is not a JSON datatype, attempt to convert it to one
                    if ( source != null && source.getValue() != null && source.getCellClass() != JacksonJsonContainer.class )
                    {
                        source.setColumn(new DataColumn(DataTypes.JSON, "source"));
                        source.setValue(new JacksonJsonContainer(String.valueOf(source.getValue())));
                    }

                    // Validate first source column is of JSON datatype
                    if ( source != null && source.getCellClass() == JacksonJsonContainer.class )
                    {
                        logger.debug("First source column value is json: " + source);
                        JacksonJsonContainer sjson = (JacksonJsonContainer)source.getValue();
                        JacksonJsonContainer tjson = new JacksonJsonContainer(sjson);
                        
                        // Iterate over the remaining sources
                        for ( int i=1; i<incoming.size(); i++ )
                        {
                            TransformSource ts = getSourceColumns().get(i);
                            source = incoming.get(i);
                            try
                            {
                                logger.debug("Setting field " + ts.getPath() + "=" + source);
                                tjson.setFieldValue(ts.getPath(), source.getValue());
                            }
                            catch (InvalidPathException e)
                            {
                                switch (invalid)
                                {
                                    case NULL:
                                        //Invalid path so cannot modify json
                                        break;
                                    default:
                                        throw new TransformException(source, "Unable to access parent field using path: " + ts.getPath() , e);
                                }
                            }
                        }

                        tlist.add(new DataCell(tcol, tjson)); 
                    }
                    else
                    {
                        throw new TransformException(source, "First sourceColumn must be of type json");
                    }
                    
                    break;
                case addField:
                    tcol = getTransformDataColumn(cols, incoming, 0);
                    tcol.setDataType(DataTypes.JSON);
                    
                    // If first source column is not a JSON datatype, attempt to convert it to one
                    if ( source != null && source.getValue() != null && source.getCellClass() != JacksonJsonContainer.class )
                    {
                        source.setColumn(new DataColumn(DataTypes.JSON, "source"));
                        source.setValue(new JacksonJsonContainer(String.valueOf(source.getValue())));
                    }

                    // Validate first source column is of JSON datatype
                    if ( source != null && source.getCellClass() == JacksonJsonContainer.class )
                    {
                        logger.debug("First source column value is json: " + source);
                        JacksonJsonContainer sjson = (JacksonJsonContainer)source.getValue();
                        JacksonJsonContainer tjson = new JacksonJsonContainer(sjson);
                        
                        // Iterate over the remaining sources
                        TransformSource ts = getSourceColumns().get(1);
                        source = incoming.get(1);
                        try
                        {
                            logger.debug("Adding field " + ts.getPath() + "/" + ts.getRefNode() + "=" + source);
                            tjson.addFieldValue(ts.getPath(), ts.getRefNode(), source.getValue());
                        }
                        catch (InvalidPathException e)
                        {
                            switch (invalid)
                            {
                                case NULL:
                                    //Invalid path so cannot modify json
                                    break;
                                default:
                                    throw new TransformException(source, "Unable to access parent field using path: " + ts.getPath() , e);
                            }
                        }

                        tlist.add(new DataCell(tcol, tjson)); 
                    }
                    else
                    {
                        throw new TransformException(source, "First sourceColumn must be of type json");
                    }

                    break;
                case removeField:
                    tcol = getTransformDataColumn(cols, incoming, 0);
                    tcol.setDataType(DataTypes.JSON);
                    
                    // If first source column is not a JSON datatype, attempt to convert it to one
                    if ( source != null && source.getValue() != null && source.getCellClass() != JacksonJsonContainer.class )
                    {
                        source.setColumn(new DataColumn(DataTypes.JSON, "source"));
                        source.setValue(new JacksonJsonContainer(String.valueOf(source.getValue())));
                    }

                    // Validate first source column is of JSON datatype
                    if ( source != null && source.getCellClass() == JacksonJsonContainer.class )
                    {
                        logger.debug("First source column value is json: " + source);
                        JacksonJsonContainer sjson = (JacksonJsonContainer)source.getValue();
                        JacksonJsonContainer tjson = new JacksonJsonContainer(sjson);
                        
                        TransformSource ts = getSourceColumns().get(1);
                        source = incoming.get(1);
                        
                        try
                        {
                            JacksonJsonContainer.ObjectFilter f = new JacksonJsonContainer.ObjectFilter(ts.getRefNode(), String.valueOf(source.getValue()) , JacksonJsonContainer.ObjectFilter.BooleanType.OR);
                            logger.debug("Removing field " + ts.getPath() + " WHERE " + f);
                            
                            List<JacksonJsonContainer.ObjectFilter> filters = new ArrayList();
                            filters.add(f);
                            List<Integer> indices = tjson.getArrayedIndexFromFilter(ts.getPath(), filters);
                            for ( Integer index: indices )
                            {
                                logger.debug("Removing arrayed index #" + index);
                                Object o = tjson.removeIndexedField(ts.getPath(), index);
                                logger.debug("Removed object: " + o);
                            }
                        }
                        catch (InvalidPathException e)
                        {
                            switch (invalid)
                            {
                                case NULL:
                                    //Invalid path so cannot modify json
                                    break;
                                default:
                                    throw new TransformException(source, "Unable to access parent field using path: " + ts.getPath() , e);
                            }
                        }

                        tlist.add(new DataCell(tcol, tjson)); 
                    }
                    else
                    {
                        throw new TransformException(source, "First sourceColumn must be of type json");
                    }

                    break;
                default:
                    throw new UnsupportedOperationException("Unsupported hierarchyTransform action: " + action);
            }
        }
        /*catch (ETLException e)
        {
            throw new TransformException(e);
        }*/
        catch (IOException e)
        {
            throw new TransformException(source, e);
        }
        catch (SAXException e)
        {
            throw new TransformException(source, e);
        }
        catch (InvalidPathException e)
        {
            throw new TransformException(source, e);
        }
        
        return tlist;
    }

    private DataCell extractXpathValue(String xpath, DataColumn col, Document doc)
        throws ETLException
    {
        XPath expr = XPathFactory.newInstance().newXPath();
        logger.trace("Evaluating xpath: " + xpath);
        DataCell cell = null;

        try
        {
            Node n = (Node)expr.evaluate(xpath, doc.getDocumentElement(), XPathConstants.NODE);
            if ( n != null )
            {
                NodeType ntype = nodeTypeMap.get((int)n.getNodeType());
                switch(ntype)
                {
                    case ELEMENT_NODE:
                        throw new ETLException("Xpath NODE type must be declared as a NODE DataType: " + xpath, col);
                    case ATTRIBUTE_NODE:
                    case TEXT_NODE:
                        cell = new DataCell(col, n.getTextContent());
                        break;
                    default:
                        throw new ETLException("Unsupported xpath node type returned: " + ntype, n);
                }
            }
            else
            {
                cell = new DataCell(col, null);
            }
        }
        catch (XPathExpressionException e)
        {
            throw new ETLException("Invalid xpath expression: " + xpath, col);
        }
        
        return cell;
    }

    private NodeList extractXpathNodeValue(String xpath, String refNode, DataColumn col, Document doc)
        throws ETLException
    {
        NodeList nl = null;
        XPath expr = XPathFactory.newInstance().newXPath();
        logger.trace("Evaluating xpath=" + xpath);

        if ( refNode != null )
        {
            try
            {
                nl = (NodeList)expr.evaluate(xpath, doc.getDocumentElement(), XPathConstants.NODESET);
                if ( nl != null )
                {
                    Node n = null;
                    if ( nl.getLength() > 0 && nl.item(0) != null )
                    {
                        n = nl.item(0);
                        logger.trace(xpath + "=" + n);
                        NodeType ntype = nodeTypeMap.get((int)n.getNodeType());
                        if ( ntype != NodeType.ELEMENT_NODE )
                        {
                            throw new ETLException("Unsupported xpath node type returned, expecting NODE: " + ntype, n);
                        }
                    }
                }
            }
            catch (XPathExpressionException e)
            {
                throw new ETLException("Invalid xpath expression: " + xpath, col);
            }
        }
        
        return nl;
    }
    
    @Override
    public String toString() 
    {
        return action + "(" + getSourceColumns() + ")->" + getTargetColumns();
    }
    
    
}
