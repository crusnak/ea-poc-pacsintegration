/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.transforms;

import com.blackboard.services.etl.data.Argument;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.exception.TransformException;
import com.blackboard.services.etl.exception.VariableTypeMismatchException;
import com.blackboard.services.etl.jaxb.BooleanTransformComplexType;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.tasks.AbstractTask;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.blackboard.services.utils.JavaUtils;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author crusnak
 */
public class BooleanTransform extends AbstractTransform
{
    public static enum Operator { eq, ne, gt, lt, contains, startswith, endswith };
    public static enum OutputType { longstring, shortstring, number, bool, yn };

    private Operator operator = null;
    private OutputType output = null;

    public BooleanTransform(BooleanTransformComplexType ct, AbstractTask task)
        throws ETLException
    {
        super(ct, task);
        setOperator(ct.getOperator().value());
        setOutputType(ct.getOutputType());
    }

    private void setOperator(String o)
        throws ETLException
    {
        if ( o == null )
        {
            throw new ETLException("Operator cannot be null for BooleanTransform, use one of: " + JavaUtils.enumToList(Operator.class));
        }

        try
        {
            operator = Operator.valueOf(o);
        }
        catch (IllegalArgumentException e)
        {
            throw new ETLException("Unsupported operator '" + o + "', expected: " + JavaUtils.enumToList(Operator.class), e);
        }
    }

    private void setOutputType(String o)
        throws ETLException
    {
        if ( o == null )
        {
            throw new ETLException("OutputType cannot be null for BooleanTransform, use one of: " + JavaUtils.enumToList(OutputType.class));
        }

        try
        {
            output = OutputType.valueOf(o);
        }
        catch (IllegalArgumentException e)
        {
            throw new ETLException("Unsupported operator '" + o + "', expected: " + JavaUtils.enumToList(OutputType.class), e);
        }
    }

    public Type getTransformType()
    {
        return Type.bool;
    }

    @Override
    public boolean supportsMultipleSources()
    {
        return true;
    }

    @Override
    public boolean supportsMultipleTargets()
    {
        return false;
    }

    @Override
    public int getExpectedSourceCount()
    {
        return 2;
    }

    public List<DataCell> transform(List<DataColumn> cols, List<DataCell> incoming)
        throws ETLException
    {
        ArrayList<DataCell> targets = new ArrayList();
        DataColumn scol = null;
        DataCell op1 = null;
        DataCell op2 = null;
        DataCell out = null;
        DataColumn tcol = null;
        boolean result = false;

        // TODO: add support for sum operator
        validateIncomingSources(incoming);
        op1 = incoming.get(0);
        op2 = incoming.get(1);
        tcol = getTransformDataColumn(cols, incoming, 0);

        try
        {
            Object o1 = null;
            Object o2 = null;
            if ( op1 != null )
            {
                o1 = op1.getValue();
            }
            if ( op2 != null )
            {
                o2 = op2.getValue();
            }

            switch (operator)
            {
                case eq:
                    if ( o1 != null && o2 != null )
                    {
                        result = o1.equals(o2);
                    }
                    else if ( o1 == null && o2 == null )
                    {
                        result = true;
                    }
                    break;
                case ne:
                    if ( o1 != null && o2 != null )
                    {
                        result = !o1.equals(o2);
                    }
                    else if ( (o1 == null && o2 != null) || (o1 != null && o2 == null) )
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                    break;
                case gt:
                    try
                    {
                        double d1 = Double.parseDouble(String.valueOf(o1));
                        double d2 = Double.parseDouble(String.valueOf(o2));
                        result = d1 > d2;
                    }
                    catch (NumberFormatException e)
                    {
                        logger.debug("Both operands for gt boolean operator are not numbers, result FALSE");
                    }
                    break;
                case lt:
                    try
                    {
                        double d1 = Double.parseDouble(String.valueOf(o1));
                        double d2 = Double.parseDouble(String.valueOf(o2));
                        result = d1 < d2;
                    }
                    catch (NumberFormatException e)
                    {
                        logger.debug("Both operands for lt boolean operator are not numbers, result FALSE");
                    }
                    break;
                case contains:
                    if ( o1 != null )
                    {
                        result = String.valueOf(o1).contains(String.valueOf(o2));
                    }
                    break;
                case startswith:
                    if ( o1 != null )
                    {
                        result = String.valueOf(o1).startsWith(String.valueOf(o2));
                    }
                    break;
                case endswith:
                        result = String.valueOf(o1).endsWith(String.valueOf(o2));
                    break;
                default:
                    throw new TransformException(op1, "Unsupported operator " + operator + ", expected: " + JavaUtils.enumToList(Operator.class));
            }

            out = convertOutput(tcol, result);
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            throw new TransformException(op1, "Expected two source columns from incoming data cells: " + incoming);
        }
        catch (TransformException e)
        {
            e.setDataCell(op1);
            throw e;
        }
        catch (ETLException e)
        {
            throw new TransformException(op1, e);
        }

        targets.add(out);
        return targets;
    }

    private DataCell convertOutput(DataColumn col, boolean result)
        throws VariableTypeMismatchException
    {
        DataCell out = null;
        logger.debug("converting output to type: " + output);
        switch (output)
        {
            case longstring:
                col.setDataType(DataTypes.STRING);
                out = new DataCell(col, String.valueOf(result));
                break;
            case shortstring:
                col.setDataType(DataTypes.STRING);
                if ( result )
                {
                    out = new DataCell(col, "T");
                }
                else
                {
                    out = new DataCell(col, "F");
                }
                break;
            case yn:
                col.setDataType(DataTypes.STRING);
                if ( result )
                {
                    out = new DataCell(col, "Y");
                }
                else
                {
                    out = new DataCell(col, "N");
                }
                break;
            case number:
                col.setDataType(DataTypes.INT);
                if ( result )
                {
                    out = new DataCell(col, 1);
                }
                else
                {
                    out = new DataCell(col, 0);
                }
                break;
            case bool:
                col.setDataType(DataTypes.BOOLEAN);
                out = new DataCell(col, result);
                break;
        }

        return out;
    }

    public String toString()
    {
        Argument op1 = getSourceColumns().get(0);
        Argument op2 = getSourceColumns().get(1);
        return op1 + " " + operator + " " + op2 + "->" + getTargetColumns().get(0);
    }
}
