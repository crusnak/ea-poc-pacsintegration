/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.transforms;

import com.blackboard.services.etl.data.Argument;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.TransformTarget;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.exception.TransformException;
import com.blackboard.services.etl.jaxb.ArithmeticTransformComplexType;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.tasks.AbstractTask;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.blackboard.services.utils.JavaUtils;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author crusnak
 */
public class ArithmeticTransform extends AbstractTransform
{
    public static enum Operator { add, digitAdd, subtract, multiply, divide, modulo, round, absolute, luhn, tap };
    public static enum RoundingMode { up, down, halfEven, halfUp, halfDown, ceiling, floor};
    public static enum DigitModifier { all, odd, even };
    public static enum DigitEndian { big, little };
    
    private static HashMap<Operator,String> operatorMap = new HashMap();
    private static HashMap<MathType,DataTypes> comboMap = new HashMap();

    private Operator operator = null;
    private boolean nullToZero = true;
    private DigitModifier dmod = null;
    private DigitEndian endian = null;
    private java.math.RoundingMode mode = java.math.RoundingMode.HALF_EVEN;

    static
    {
        operatorMap.put(Operator.add, "+");
        operatorMap.put(Operator.subtract, "-");
        operatorMap.put(Operator.multiply, "*");
        operatorMap.put(Operator.divide, "/");
        operatorMap.put(Operator.modulo, "%");
        operatorMap.put(Operator.round, "ROUND");
        operatorMap.put(Operator.absolute, "ABS");
        operatorMap.put(Operator.digitAdd, "addDigits");
        operatorMap.put(Operator.luhn, "LUHN");
        operatorMap.put(Operator.tap, "TAP");
    }

    public ArithmeticTransform(ArithmeticTransformComplexType ct, AbstractTask task)
        throws ETLException
    {
        super(ct, task);
        setOperator(ct.getOperator());
        nullToZero = ct.isNullAsZero();
        setRoundingMode(ct.getRoundingMode());
        setDigitModifier(ct.getDigitModifier());
        setDigitEndian(ct.getEndian());
    }

    @Override
    public Type getTransformType()
    {
        return Type.arithmetic;
    }

    @Override
    public boolean supportsMultipleSources()
    {
        boolean mult = false;
        switch (operator)
        {
            case absolute:
            case digitAdd:
            case luhn:
            case tap:
                mult = false;
                break;
            default:
                mult = true;
        }
        return mult;
    }

    @Override
    public boolean supportsMultipleTargets()
    {
        return false;
    }

    @Override
    public int getExpectedSourceCount()
    {
        int count = 0;
        switch (operator)
        {
            case absolute:
            case digitAdd:
            case luhn:
            case tap:
                count = 1;
                break;
            default:
                count = 2;
        }
        return count;
    }

    public List<DataCell> transform(List<DataColumn> cols, List<DataCell> incoming)
        throws ETLException
    {
        ArrayList<DataCell> targets = new ArrayList();
        DataColumn scol1 = null;
        DataColumn scol2 = null;
        DataCell op1 = null;
        DataCell op2 = null;
        DataCell out = null;
        DataColumn tcol = null;

        // TODO: add support for sum operator
        validateIncomingSources(incoming); 
        try
        {
            if ( getExpectedSourceCount() == 2 )
            {
                op1 = incoming.get(0);
                op2 = incoming.get(1);
                tcol = getTransformDataColumn(cols, incoming, 0);

                // If no target column is currently assigned we need to ensure that the greater numeric precision data type
                // is used as the target column.  If source column #2 has a numeric data type with greater precision
                // than source column 1, that column has to be used as the data type.
                if ( cols == null || cols.isEmpty() || cols.get(0) == null )
                {
                    scol1 = incoming.get(0).getColumn();
                    scol2 = incoming.get(1).getColumn();

                    if ( scol1.getDataType() != scol2.getDataType() )
                    {
                        logger.debug("No target column info passed and source columns have differing datatypes");
                        TransformTarget target = getTargetColumns().get(0);
                        DataTypes datatype = null;
                        
                        if ( target.isTargetDataTypeOverridden() )
                        {
                            logger.debug("Target column data type overriden: " + target.getOverriddenDataType());
                            datatype = target.getOverriddenDataType();
                        }
                        else
                        {
                            datatype = scol2.getDataType();
                        }
                        
                        switch (scol2.getDataType())
                        {
                            case DOUBLE:
                                tcol = new DataColumn(datatype, target.getColumnName(), target.getDefaultValue());
                                break;
                            case FLOAT:
                                if ( scol2.getDataType() != DataTypes.DOUBLE )
                                {
                                    tcol = new DataColumn(datatype, target.getColumnName(), target.getDefaultValue());
                                }
                                break;
                            case LONG:
                                if ( scol2.getDataType() != DataTypes.DOUBLE &&
                                     scol2.getDataType() != DataTypes.FLOAT )
                                {
                                    tcol = new DataColumn(datatype, target.getColumnName(), target.getDefaultValue());
                                }
                                break;
                        }
                    }
                }

                
                Object val1 = null;
                Object val2 = null;

                if ( op1 != null )
                {
                    val1 = op1.getValue();
                }
                
                if ( op1 != null )
                {
                    val2 = op2.getValue();
                }
                
                if ( op1 == null || op1.getValue() == null || op2 == null || op2.getValue() == null )
                {
                    if ( nullToZero && op1 != null && op2 != null )
                    {
                        if ( op1.getValue() == null )
                        {
                            val1 = 0;
                            logger.debug("Operand 1 is null, setting to zero");
                        }
                        if ( op2.getValue() == null )
                        {
                            val2 = 0;
                            logger.debug("Operand 2 is null, setting to zero");
                        }
                    }
                }

                logger.debug("TargetColumn: " + tcol);
                
                if ( op1 == null || val1 == null || op2 == null || val2 == null )
                {
                    logger.debug("At least one source operand is null, no transformation taking place");
                    out = new DataCell(tcol, null);
                }
                else
                {
                    switch (operator)
                    {
                        case add:
                            out = add(tcol, val1, val2);
                            break;
                        case subtract:
                            out = subtract(tcol, val1, val2);
                            break;
                        case multiply:
                            out = multiply(tcol, val1, val2);
                            break;
                        case divide:
                            out = divide(tcol, val1, val2);
                            break;
                        case modulo:
                            out = modulo(tcol, val1, val2);
                            break;
                        case round:
                            out = round(tcol, val1, val2);
                            break;

                        default:
                            throw new TransformException(op1, "Unsupported binary operator " + operator + ", expected: " + JavaUtils.enumToList(getBinaryOperators()));
                    }
                }
            }
            else if ( getExpectedSourceCount() == 1 )
            {
                Object val1 = null;
                op1 = incoming.get(0);
                tcol = getTransformDataColumn(cols, incoming, 0);

                if ( op1 != null )
                {
                    val1 = op1.getValue();
                }
                
                if ( op1 == null || op1.getValue() == null )
                {
                    if ( nullToZero && op1 != null )
                    {
                        if ( op1.getValue() == null )
                        {
                            val1 = 0;
                            logger.debug("Unary operand is null, setting to zero");
                        }
                    }
                }

                if ( op1 == null || val1 == null )
                {
                    logger.debug("Unary operand is null, no transformation taking place");
                    out = new DataCell(tcol, null);
                }
                else
                {
                    switch (operator)
                    {
                        case absolute:
                            out = abs(tcol, val1);
                            break;
                        case digitAdd:
                            out = digitAdd(tcol, val1);
                            break;
                        case luhn:
                            out = luhn(tcol, val1);
                            break;
                        case tap:
                            out = tap(tcol, val1);
                            break;
                        default:
                            throw new TransformException(op1, "Unsupported unary operator " + operator + ", expected: " + JavaUtils.enumToList(getUnaryOperators()));
                    }
                }
            }
            else
            {
                throw new TransformException(null, "Math operators only support 1 or 2 operands, expectings: " + getExpectedSourceCount());
            }
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            throw new TransformException(op1, "Expected two source columns from incoming data cells: " + incoming);
        }
        catch (TransformException e)
        {
            e.setDataCell(op1);
            throw e;
        }
        catch (ETLException e)
        {
            throw new TransformException(op1, e);
        }

        targets.add(out);
        return targets;
    }

    public Operator getOperator()
    {
        return operator;
    }

    private DataCell add(DataColumn col, Object op1, Object op2)
        throws TransformException, ETLException
    {
        DataCell cell = null;
        try
        {
            switch (col.getDataType())
            {
                case INT:
                    int io1 = Integer.parseInt(String.valueOf(op1));
                    int io2 = Integer.parseInt(String.valueOf(op2));
                    cell = new DataCell(col, io1+io2);
                    break;
                case LONG:
                    long lo1 = Long.parseLong(String.valueOf(op1));
                    long lo2 = Long.parseLong(String.valueOf(op2));
                    cell = new DataCell(col, lo1+lo2);
                    break;
                case FLOAT:
                    float fo1 = Float.parseFloat(String.valueOf(op1));
                    float fo2 = Float.parseFloat(String.valueOf(op2));
                    cell = new DataCell(col, fo1+fo2);
                    break;
                case DOUBLE:
                    double do1 = Double.parseDouble(String.valueOf(op1));
                    double do2 = Double.parseDouble(String.valueOf(op2));
                    cell = new DataCell(col, do1+do2);
                    break;
                case STRING:
                    if ( String.valueOf(op1).contains(".") || String.valueOf(op2).contains(".") )
                    {
                        do1 = Double.parseDouble(String.valueOf(op1));
                        do2 = Double.parseDouble(String.valueOf(op2));
                        cell = new DataCell(col, do1+do2);
                    }
                    else
                    {
                        lo1 = Long.parseLong(String.valueOf(op1));
                        lo2 = Long.parseLong(String.valueOf(op2));
                        cell = new DataCell(col, lo1+lo2);
                        break;
                    }
                    break;
                default:

                    throw new TransformException(null, "Cannot perform add on target data type: " + col.getDataType() +
                                                 ", allowed types: " + JavaUtils.enumToList(getAllowedDataTypes()));
            }
        }
        catch (NumberFormatException e)
        {
            throw new TransformException(null, "Unable to convert one of " + op1 + ", " + op2 + " to: " + col.getColumnClass(), e);
        }
        return cell;
    }

    private DataCell digitAdd(DataColumn col, Object op1)
        throws TransformException, ETLException
    {
        DataCell cell = null;
        try
        {
            switch (col.getDataType())
            {
                case INT:
                case LONG:
                    int value = 0;
                    String digits = String.valueOf(op1);
                    char[] cary = digits.toCharArray();
                    if ( endian == DigitEndian.little )
                    {
                        for ( int i=0; i<cary.length; i++ )
                        {
                            cary[i] = digits.toCharArray()[cary.length - 1 - i];
                        }
                    }
                    
                    switch (dmod)
                    {
                        case all:
                            for ( char c:  cary )
                            {
                                value += Integer.parseInt(String.valueOf(c));
                            }
                            break;
                        case even:
                            for ( int i=1; i<cary.length+1; i++ )
                            {
                                if ( i % 2 == 0 )
                                {
                                    value += Integer.parseInt(String.valueOf(cary[i-1]));
                                }
                            }
                            break;
                        case odd:
                            for ( int i=1; i<cary.length+1; i++ )
                            {
                                if ( i % 2 == 1 )
                                {
                                    value += Integer.parseInt(String.valueOf(cary[i-1]));
                                }
                            }
                            break;
                    }
                    
                    col.setDataType(DataTypes.INT);
                    cell = new DataCell(col, value);
                    break;
                default:

                    throw new TransformException(null, "Cannot perform digit additon on target data type: " + col.getDataType() +
                                                 ", allowed types: " + JavaUtils.enumToList(getAllowedDataTypes()));
            }
        }
        catch (NumberFormatException e)
        {
            throw new TransformException(null, "Unable to convert " + op1 + " to: " + col.getColumnClass(), e);
        }
        return cell;
    }

    private DataCell subtract(DataColumn col, Object op1, Object op2)
        throws TransformException, ETLException
    {
        DataCell cell = null;
        try
        {
            switch (col.getDataType())
            {
                case INT:
                    int io1 = Integer.parseInt(String.valueOf(op1));
                    int io2 = Integer.parseInt(String.valueOf(op2));
                    cell = new DataCell(col, io1-io2);
                    break;
                case LONG:
                    long lo1 = Long.parseLong(String.valueOf(op1));
                    long lo2 = Long.parseLong(String.valueOf(op2));
                    cell = new DataCell(col, lo1-lo2);
                    break;
                case FLOAT:
                    float fo1 = Float.parseFloat(String.valueOf(op1));
                    float fo2 = Float.parseFloat(String.valueOf(op2));
                    cell = new DataCell(col, fo1-fo2);
                    break;
                case DOUBLE:
                    double do1 = Double.parseDouble(String.valueOf(op1));
                    double do2 = Double.parseDouble(String.valueOf(op2));
                    cell = new DataCell(col, do1-do2);
                    break;
                case STRING:
                    if ( String.valueOf(op1).contains(".") || String.valueOf(op2).contains(".") )
                    {
                        do1 = Double.parseDouble(String.valueOf(op1));
                        do2 = Double.parseDouble(String.valueOf(op2));
                        cell = new DataCell(col, do1-do2);
                    }
                    else
                    {
                        lo1 = Long.parseLong(String.valueOf(op1));
                        lo2 = Long.parseLong(String.valueOf(op2));
                        cell = new DataCell(col, lo1-lo2);
                        break;
                    }
                    break;
                default:

                    throw new TransformException(null, "Cannot perform subtract on target data type: " + col.getDataType() +
                                                 ", allowed types: " + JavaUtils.enumToList(getAllowedDataTypes()));
            }
        }
        catch (NumberFormatException e)
        {
            throw new TransformException(null, "Unable to convert one of " + op1 + ", " + op2 + " to: " + col.getColumnClass(), e);
        }
        return cell;
    }

    private DataCell multiply(DataColumn col, Object op1, Object op2)
        throws TransformException, ETLException
    {
        DataCell cell = null;
        try
        {
            switch (col.getDataType())
            {
                case INT:
                    int io1 = Integer.parseInt(String.valueOf(op1));
                    int io2 = Integer.parseInt(String.valueOf(op2));
                    cell = new DataCell(col, io1*io2);
                    break;
                case LONG:
                    long lo1 = Long.parseLong(String.valueOf(op1));
                    long lo2 = Long.parseLong(String.valueOf(op2));
                    cell = new DataCell(col, lo1*lo2);
                    break;
                case FLOAT:
                    float fo1 = Float.parseFloat(String.valueOf(op1));
                    float fo2 = Float.parseFloat(String.valueOf(op2));
                    cell = new DataCell(col, fo1*fo2);
                    break;
                case DOUBLE:
                    double do1 = Double.parseDouble(String.valueOf(op1));
                    double do2 = Double.parseDouble(String.valueOf(op2));
                    cell = new DataCell(col, do1*do2);
                    break;
                case STRING:
                    if ( String.valueOf(op1).contains(".") || String.valueOf(op2).contains(".") )
                    {
                        do1 = Double.parseDouble(String.valueOf(op1));
                        do2 = Double.parseDouble(String.valueOf(op2));
                        cell = new DataCell(col, do1*do2);
                    }
                    else
                    {
                        lo1 = Long.parseLong(String.valueOf(op1));
                        lo2 = Long.parseLong(String.valueOf(op2));
                        cell = new DataCell(col, lo1*lo2);
                        break;
                    }
                    break;
                default:

                    throw new TransformException(null, "Cannot perform multiply on target data type: " + col.getDataType() +
                                                 ", allowed types: " + JavaUtils.enumToList(getAllowedDataTypes()));
            }
        }
        catch (NumberFormatException e)
        {
            throw new TransformException(null, "Unable to convert one of " + op1 + ", " + op2 + " to: " + col.getColumnClass(), e);
        }
        return cell;
    }

    private DataCell divide(DataColumn col, Object op1, Object op2)
        throws TransformException, ETLException
    {
        DataCell cell = null;
        try
        {
            switch (col.getDataType())
            {
                case INT:
                    int io1 = Integer.parseInt(String.valueOf(op1));
                    int io2 = Integer.parseInt(String.valueOf(op2));
                    cell = new DataCell(col, io1/io2);
                    break;
                case LONG:
                    long lo1 = Long.parseLong(String.valueOf(op1));
                    long lo2 = Long.parseLong(String.valueOf(op2));
                    cell = new DataCell(col, lo1/lo2);
                    break;
                case FLOAT:
                    float fo1 = Float.parseFloat(String.valueOf(op1));
                    float fo2 = Float.parseFloat(String.valueOf(op2));
                    cell = new DataCell(col, fo1/fo2);
                    break;
                case DOUBLE:
                    double do1 = Double.parseDouble(String.valueOf(op1));
                    double do2 = Double.parseDouble(String.valueOf(op2));
                    cell = new DataCell(col, do1/do2);
                    /*if ( operand.contains(".") )
                    {
                        cell = new DataCell(col, do1/do2);
                    }
                    else
                    {
                        int do2 = Integer.parseInt(operand);
                        cell = new DataCell(col, do1/do2);
                    }*/
                    
                    break;
                case STRING:
                    if ( String.valueOf(op1).contains(".") || String.valueOf(op2).contains(".") )
                    {
                        do1 = Double.parseDouble(String.valueOf(op1));
                        do2 = Double.parseDouble(String.valueOf(op2));
                        cell = new DataCell(col, do1/do2);
                    }
                    else
                    {
                        lo1 = Long.parseLong(String.valueOf(op1));
                        lo2 = Long.parseLong(String.valueOf(op2));
                        cell = new DataCell(col, lo1/lo2);
                        break;
                    }
                    break;
                default:

                    throw new TransformException(null, "Cannot perform divide on target data type: " + col.getDataType() +
                                                 ", allowed types: " + JavaUtils.enumToList(getAllowedDataTypes()));
            }
        }
        catch (NumberFormatException e)
        {
            throw new TransformException(null, "Unable to convert one of " + op1 + ", " + op2 + " to: " + col.getColumnClass(), e);
        }
        return cell;
    }

    private DataCell modulo(DataColumn col, Object op1, Object op2)
        throws TransformException, ETLException
    {
        DataCell cell = null;
        try
        {
            switch (col.getDataType())
            {
                case INT:
                    int io1 = Integer.parseInt(String.valueOf(op1));
                    int io2 = Integer.parseInt(String.valueOf(op2));
                    cell = new DataCell(col, io1%io2);
                    break;
                case LONG:
                    long lo1 = Long.parseLong(String.valueOf(op1));
                    long lo2 = Long.parseLong(String.valueOf(op2));
                    cell = new DataCell(col, lo1%lo2);
                    break;
                case STRING:
                    lo1 = Long.parseLong(String.valueOf(op1));
                    lo2 = Long.parseLong(String.valueOf(op2));
                    cell = new DataCell(col, lo1-lo2);
                    break;
                default:

                    throw new TransformException(null, "Cannot perform modulo on target data type: " + col.getDataType() +
                                                 ", allowed types: " + JavaUtils.enumToList(getAllowedDataTypes()));
            }
        }
        catch (NumberFormatException e)
        {
            throw new TransformException(null, "Unable to convert one of " + op1 + ", " + op2 + " to: " + col.getColumnClass(), e);
        }
        return cell;
    }

    private DataCell luhn(DataColumn col, Object op1)
        throws TransformException, ETLException
    {
        DataCell cell = null;
        try
        {
            switch (col.getDataType())
            {
                case INT:
                case LONG:
                    int value = 0;
                    String digits = String.valueOf(op1);
                    char[] cary = new char[digits.length()];
                    int[] temp = new int[cary.length];
                    
                    // Reverse the order of the digits since the 1st digit in a Luhn algorithm is always the rightmost.
                    // The order only affects even length numbers; odd numbers can be performed in either direction.
                    for ( int i=0; i<cary.length; i++ )
                    {
                        cary[i] = digits.toCharArray()[cary.length - 1 - i];
                    }
                    
                    // Multiply odd digits by 2
                    for ( int i=1; i<cary.length+1; i++ )
                    {
                        if ( i % 2 == 1 )
                        {
                            temp[i-1] = Integer.parseInt(String.valueOf(cary[i-1])) * 2;
                            logger.debug(cary[i-1] + " * 2 = " + temp[i-1]);
                        }
                    }
                    
                    // Sum all multiplied odd digits
                    dmod = DigitModifier.all;
                    for ( int i=1; i<temp.length+1; i++ )
                    {
                        if ( i % 2 == 1 )
                        {
                            int sum = (Integer)digitAdd(col, temp[i-1]).getValue();
                            logger.info(value + " + " + sum + " = " + (value+sum));
                            value += sum;
                        }
                    }
                    
                    logger.debug("Sum of multiplied odd digits: " + value);

                    // Sum all even digits
                    dmod = DigitModifier.even;
                    value += (Integer)digitAdd(col, op1).getValue();
                    logger.debug("Sum of even digits: " + value);
                    
                    // 9 * Sum modulo 10 is the check digit
                    value = (9 * value) % 10;
                    
                    col.setDataType(DataTypes.INT);
                    cell = new DataCell(col, value);
                    break;
                default:

                    throw new TransformException(null, "Cannot perform higher one's luhn algorithm on target data type: " + col.getDataType() +
                                                 ", allowed types: " + JavaUtils.enumToList(getAllowedDataTypes()));
            }
        }
        catch (NumberFormatException e)
        {
            throw new TransformException(null, "Unable to convert " + op1 + " to: " + col.getColumnClass(), e);
        }
        return cell;
    }
    
    private DataCell tap(DataColumn col, Object op1)
        throws TransformException, ETLException
    {
        DataCell cell = null;
        try
        {
            switch (col.getDataType())
            {
                case INT:
                case LONG:
                    long result1 = 0;
                    long result2 = 0;
                    String digits = String.valueOf(op1);
                    char[] cary = new char[digits.length()];
                    int[] temp = new int[cary.length];
                    
                    // Reverse the order of the digits since the 1st digit in a Luhn algorithm is always the rightmost.
                    // The order only affects even length numbers; odd numbers can be performed in either direction.
                    for ( int i=0; i<cary.length; i++ )
                    {
                        cary[i] = digits.toCharArray()[cary.length - 1 - i];
                    }
                    
                    // Check if even*2 > 9 then even*2-9 else even*2
                    for ( int i=1; i<cary.length+1; i++ )
                    {
                        if ( i % 2 == 1 )
                        {
                            temp[i-1] = Integer.parseInt(String.valueOf(cary[i-1]))*2;
                            
                            if ( temp[i-1] > 9 )
                            {
                                temp[i-1] -= 9;
                            }
                            logger.info(cary[i-1] + " * 2 (val>0?val-9:val) = " + temp[i-1]);
                            result1 += temp[i-1];
                        }
                    }
                    logger.debug("Sum of even digits: " + result1);
                    
                    // Sum all multiplied odd digits
                    dmod = DigitModifier.all;
                    for ( int i=1; i<cary.length+1; i++ )
                    {
                        if ( i % 2 == 0 )
                        {
                            int sum = (Integer)digitAdd(col, cary[i-1]).getValue();
                            logger.debug(result1 + " + " + sum + " = " + (result1+sum));
                            result1 += sum;
                        }
                    }                    
                    logger.debug("Sum of odd digits: " + result1);
                    
                    // Add 4 to summation
                    result1 += 4;
                    logger.debug("Tap result1: " + result1);
                    
                    // Divide result1 by 10 and roundup
                    result2 = result1 / 10;
                    if ( result1 % 10 > 0 )
                    {
                        result2 += 1;
                    }
                    
                    
                    // Multiply result2 by 10 and subtract result 1
                    result2 = result2 * 10 - result1;
                    logger.debug("Tap result2: " + result2);
                    
                    col.setDataType(DataTypes.INT);
                    cell = new DataCell(col, result2);
                    break;
                default:

                    throw new TransformException(null, "Cannot perform tap algorithm on target data type: " + col.getDataType() +
                                                 ", allowed types: " + JavaUtils.enumToList(getAllowedDataTypes()));
            }
        }
        catch (NumberFormatException e)
        {
            throw new TransformException(null, "Unable to convert " + op1 + " to: " + col.getColumnClass(), e);
        }
        return cell;
    }

    private DataCell round(DataColumn col, Object op1, Object op2)
        throws TransformException, ETLException
    {
        DataCell cell = null;
        try
        {
            int precision = Integer.parseInt(String.valueOf(op2));
            DecimalFormat format = new DecimalFormat(getRoundingPattern(precision));
            format.setRoundingMode(mode);
            switch (col.getDataType())
            {
                case FLOAT:
                case DOUBLE:
                    cell = new DataCell(col, format.format(op1));
                    logger.debug("Rounded to: " + format.format(op1));
                    break;
                case INT:
                case LONG:
                    if ( precision > 0 )
                    {
                        throw new TransformException(null, "Cannot perform decimal round on target data type with precision greater than 0: " + col.getDataType());
                    }
                    cell = new DataCell(col, format.format(op1));
                    logger.debug("Rounded to: " + format.format(op1));
                    break;
                default:

                    throw new TransformException(null, "Cannot perform decimal round on target data type: " + col.getDataType() +
                                                 ", allowed types: " + JavaUtils.enumToList(getAllowedDataTypes()));
            }
        }
        catch (IllegalArgumentException e)
        {
            throw new TransformException(null, "Unable to convert one of " + op1 + ", " + op2 + " to: " + col.getColumnClass(), e);
        }
        return cell;
    }

    private DataCell abs(DataColumn col, Object op1)
        throws TransformException, ETLException
    {
        DataCell cell = null;
        try
        {
            switch (col.getDataType())
            {
                case INT:
                    int io1 = Integer.parseInt(String.valueOf(op1));
                    cell = new DataCell(col, Math.abs(io1));
                    break;
                case LONG:
                    long lo1 = Long.parseLong(String.valueOf(op1));
                    cell = new DataCell(col, Math.abs(lo1));
                    break;
                case FLOAT:
                    float fo1 = Float.parseFloat(String.valueOf(op1));
                    cell = new DataCell(col, Math.abs(fo1));
                    break;
                case DOUBLE:
                    double do1 = Double.parseDouble(String.valueOf(op1));
                    cell = new DataCell(col, Math.abs(do1));

                    break;
                case STRING:
                    if ( String.valueOf(op1).contains(".") )
                    {
                        do1 = Double.parseDouble(String.valueOf(op1));
                        cell = new DataCell(col, Math.abs(do1));
                    }
                    else
                    {
                        lo1 = Long.parseLong(String.valueOf(op1));
                        cell = new DataCell(col, Math.abs(lo1));
                        break;
                    }
                    break;
                default:

                    throw new TransformException(null, "Cannot perform absolute on target data type: " + col.getDataType() +
                                                 ", allowed types: " + JavaUtils.enumToList(getAllowedDataTypes()));
            }
        }
        catch (IllegalArgumentException e)
        {
            throw new TransformException(null, "Unable to convert " + op1 + " to: " + col.getColumnClass(), e);
        }
        return cell;
    }

    private void setOperator(String o)
        throws ETLException
    {
        if ( o == null )
        {
            throw new ETLException("Operator cannot be null for ArithmeticTransforms, use one of: " + JavaUtils.enumToList(Operator.class));
        }

        try
        {
            operator = Operator.valueOf(o);
        }
        catch (IllegalArgumentException e)
        {
            throw new ETLException("Unsupported operator '" + o + "', expected: " + JavaUtils.enumToList(Operator.class), e);
        }
    }
    
    private void setRoundingMode(String c)
        throws ETLException
    {
        if ( operator == Operator.round )
        {
            try
            {
                RoundingMode rm = RoundingMode.valueOf(c);
                switch (rm) 
                {
                    case ceiling:
                        mode = java.math.RoundingMode.CEILING;
                        break;
                    case down:
                        mode = java.math.RoundingMode.DOWN;
                        break;
                    case floor:
                        mode = java.math.RoundingMode.FLOOR;
                        break;
                    case halfDown:
                        mode = java.math.RoundingMode.HALF_DOWN;
                        break;
                    case halfEven:
                        mode = java.math.RoundingMode.HALF_EVEN;
                        break;
                    case halfUp:
                        mode = java.math.RoundingMode.HALF_UP;
                        break;
                    case up:
                        mode = java.math.RoundingMode.UP;
                        break;
                }
            }
            catch (IllegalArgumentException e)
            {
                throw new ETLException("Unsupported rounding mode '" + mode + "', expected: " + JavaUtils.enumToList(RoundingMode.class), e);
            }
        }
    }

    private void setDigitModifier(String c)
        throws ETLException
    {
        switch (operator)
        {
            case digitAdd:
                try
                {
                    dmod = DigitModifier.valueOf(c);
                    switch ( dmod )
                    {
                        case all:
                            operatorMap.put(Operator.digitAdd, "addDigits");
                            break;
                        case even:
                            operatorMap.put(Operator.digitAdd, "addEvenDigits");
                            break;
                        case odd:
                            operatorMap.put(Operator.digitAdd, "addOddDigits");
                            break;
                    }
                }
                catch (IllegalArgumentException e)
                {
                    throw new ETLException("Unsupported digit modifier '" + c + "', expected: " + JavaUtils.enumToList(DigitModifier.class), e);
                }
                break;
        }
    }

    private void setDigitEndian(String c)
        throws ETLException
    {
        switch (operator)
        {
            case digitAdd:
                try
                {
                    endian = DigitEndian.valueOf(c);
                }
                catch (IllegalArgumentException e)
                {
                    throw new ETLException("Unsupported digit endian '" + c + "', expected: " + JavaUtils.enumToList(DigitEndian.class), e);
                }
                break;
        }
    }

    private double convertToDouble(String operand)
        throws TransformException
    {
        double d = 0.0;
        try
        {
            d = Double.parseDouble(operand);
        }
        catch (NumberFormatException e)
        {
            throw new TransformException(null, "Unable to convert to double: " + operand, e);
        }
        return d;
    }

    private String getRoundingPattern(int precision)
    {
        String pattern = "0";
        if ( precision > 0 )
        {
            pattern = "0.";
            for ( int i=0; i<precision; i++ )
            {
                pattern += "0";
            }
        }
        return pattern;
    }

    private EnumSet getUnaryOperators()
    {
        EnumSet allowed = null;
        allowed = EnumSet.of(Operator.absolute, Operator.digitAdd, Operator.luhn);
        return allowed;
    }
    
    private EnumSet getBinaryOperators()
    {
        return EnumSet.complementOf(getUnaryOperators());
    }
    
    private EnumSet getAllowedDataTypes()
    {
        EnumSet allowed = null;
        switch (operator)
        {
            case round:
                allowed = EnumSet.of(DataTypes.FLOAT, DataTypes.DOUBLE);
                break;
            case digitAdd:
            case modulo:
                allowed = EnumSet.of(DataTypes.INT, DataTypes.LONG);
                break;
            default:
                allowed = EnumSet.of(DataTypes.INT, DataTypes.LONG, DataTypes.FLOAT, DataTypes.DOUBLE);
        }
        return allowed;
    }
    
    private DataTypes getPreferredDataType(Object o1, Object o2)
    {
        DataTypes t = null;
        
        switch (operator)
        {
            case add:
            case subtract:
                if ( o1 instanceof Double || o2 instanceof Double )
                {
                    t = DataTypes.DOUBLE;
                }
                else if ( o1 instanceof Float || o2 instanceof Float )
                {
                    t = DataTypes.FLOAT;
                }
                else if ( o1 instanceof Long || o2 instanceof Long )
                {
                    t = DataTypes.LONG;
                }
                else
                {
                    t = DataTypes.INT;
                }
                break;
            case multiply:
                if ( o1 instanceof Double || o2 instanceof Double )
                {
                    t = DataTypes.DOUBLE;
                }
                else if ( o1 instanceof Float || o2 instanceof Float )
                {
                    t = DataTypes.DOUBLE;
                }
                else 
                {
                    t = DataTypes.LONG;
                }
                break;
            case divide:
                if ( o1 instanceof Double || o2 instanceof Double )
                {
                    t = DataTypes.DOUBLE;
                }
                else if ( o1 instanceof Float || o2 instanceof Float )
                {
                    t = DataTypes.FLOAT;
                }
                else 
                {
                    t = DataTypes.INT;
                }
                break;
        }
        
        return t;
    }

    public String toString()
    {
        String out = "";
        if ( getExpectedSourceCount() == 2 )
        {
            Argument op1 = getSourceColumns().get(0);
            Argument op2 = getSourceColumns().get(1);
            switch (operator)
            {
                case round:
                    int prec = Integer.parseInt(String.valueOf(op2.getValue()));
                    out = op1 + " " + operatorMap.get(operator) + "(" + getRoundingPattern(prec) + ")->" + getTargetColumns().get(0);
                    break;
                default:
                    out = op1 + " " + operatorMap.get(operator) + " " + op2 + "->" + getTargetColumns().get(0);
            }
        }
        else if ( getExpectedSourceCount() == 1 )
        {
            Argument op1 = getSourceColumns().get(0);
            out = operatorMap.get(operator) + "(" + op1 + ")->" + getTargetColumns().get(0);
        }
        return out;
    }
    
    class MathType
    {
        DataTypes type1 = null;
        DataTypes type2 = null;
        Operator op = null;
        
        public MathType(DataTypes op1, DataTypes op2, Operator o)
        {
            type1 = op1;
            type2 = op2;
            op = o;
        }

        public DataTypes getType1()
        {
            return type1;
        }

        public void setType1(DataTypes op1)
        {
            this.type1 = op1;
        }

        public DataTypes getType2()
        {
            return type2;
        }

        public void setType2(DataTypes op2)
        {
            this.type2 = op2;
        }

        public Operator getOperator()
        {
            return op;
        }

        public void setOperator(Operator op)
        {
            this.op = op;
        }
   }
}
