/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.transforms;

import com.blackboard.services.etl.data.Argument;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.exception.TransformException;
import com.blackboard.services.etl.exception.TransformIgnoreException;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.PassthruTransformComplexType;
import com.blackboard.services.etl.tasks.AbstractTask;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.blackboard.services.utils.JavaUtils;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author crusnak
 */
public class PassthruTransform extends AbstractTransform
{
    private enum Action { assign, firstof };
    
    private boolean ignoreNull = false;
    private boolean evaluate = false;
    private Action action = Action.assign;

    public PassthruTransform(PassthruTransformComplexType ct, AbstractTask task)
        throws ETLException
    {
        super(ct, task);
        ignoreNull = ct.isIgnoreNulls();
        evaluate = ct.isProcessVariables();
        setAction(ct.getAction());
    }

    private void setAction(String o)
        throws ETLException
    {
        try
        {
            action = Action.valueOf(o);
        }
        catch (IllegalArgumentException e)
        {
            throw new ETLException("Unsupported action '" + o + "', expected: " + JavaUtils.enumToList(Action.class), e);
        }
    }

    public Type getTransformType()
    {
        return Type.passthru;
    }

    @Override
    public boolean supportsMultipleSources()
    {
        boolean mult = false;
        switch (action)
        {
            case firstof:
                mult = true;
                break;
            case assign:
                mult = false;
                break;
        }
        return mult;
    }

    @Override
    public boolean supportsMultipleTargets()
    {
        return false;
    }

    @Override
    public int getExpectedSourceCount()
    {
        int count = 0;
        switch (action)
        {
            case firstof:
                count = -1;
                break;
            case assign:
                count = 1;
                break;
        }
        return count;
    }

    public List<DataCell> transform(List<DataColumn> cols, List<DataCell> incoming)
        throws ETLException
    {
        ArrayList<DataCell> targets = new ArrayList();
        DataCell source = null;
        DataCell out = null;
        DataColumn tcol = null;

        validateIncomingSources(incoming);
        source = incoming.get(0);
        tcol = getTransformDataColumn(cols, incoming, 0);
        logger.debug("Target col: " + tcol);

        try
        {
            Object val = null;
            switch (action)
            {
                case firstof:
                    for ( int i=0; i<incoming.size(); i++)
                    {
                        DataCell cell = incoming.get(i);
                        if ( cell != null && cell.getValue() != null )
                        {
                            logger.debug("Assigning source #" + (i+1) + " to target");
                            val = cell.getValue();
                            break;
                        }
                    }
                    
                    if ( val == null )
                    {
                        logger.debug("All source values are null, default or null assigned to target");
                    }
                    break;
                default:
                    if ( ignoreNull && (source == null || source.getValue() == null) )
                    {
                        throw new TransformIgnoreException(source, "Source value is null and ignoreNull is set, not assigning null to target");
                    }
                    else if ( source == null || source.getValue() == null )
                    {
                        logger.debug("Source value is null, default or null assigned to target");
                        /*if ( isDefaultValueDefined() )
                        {
                            val = getDefaultValue();
                        }*/
                        val = null;
                    }
                    else
                    {
                        val = source.getValue();
                        if ( evaluate && tcol.getDataType() == DataTypes.STRING )
                        {
                            logger.debug("Evaluating cell value with global variable values");
                            val = task.getETLJobWrapper().getDataArchiver().processEmbeddedBeanScript(String.valueOf(val), task);
                        }
                        /*if ( val == null && isDefaultValueDefined() )
                        {
                            val = getDefaultValue();
                        }*/
                    }

            }
            out = new DataCell(tcol, val);
        }
        catch (ETLException e)
        {
            if ( e instanceof TransformIgnoreException )
            {
                throw (TransformIgnoreException)e;
            }
            else
            {
                throw new TransformException(source, e);
            }
        }

        targets.add(out);
        return targets;
    }

    public String toString()
    {
        return action + "(" + getSourceColumns() + ")->" + getTargetColumns();
    }
}
