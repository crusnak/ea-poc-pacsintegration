/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.transforms;

import com.blackboard.services.etl.data.Argument;
import com.blackboard.services.etl.data.BinaryObject;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.exception.TransformException;
import com.blackboard.services.etl.exception.TransformIgnoreException;
import com.blackboard.services.etl.jaxb.BinaryTransformComplexType;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.ImageTypes;
import com.blackboard.services.etl.jaxb.PassthruTransformComplexType;
import com.blackboard.services.etl.tasks.AbstractTask;
import com.blackboard.services.etl.tasks.FileAdapter;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.blackboard.services.utils.JavaUtils;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;

/**
 *
 * @author crusnak
 */
public class BinaryTransform extends AbstractTransform
{
    public static enum Action { fileToBinary, binaryToFile, extractByteLength, extractChecksum, convertImage, deleteBinaryFile, changeEncoding };
    
    private Action action;
    private boolean referenceOriginal = false;
    
    public BinaryTransform(BinaryTransformComplexType ct, AbstractTask task)
        throws ETLException
    {
        super(ct, task);
        setBinaryAction(ct);
        referenceOriginal = ct.isReferenceOriginal();
    }

    public Type getTransformType()
    {
        return Type.binary;
    }

    @Override
    public boolean supportsMultipleSources()
    {
        boolean mult = false;
        
        switch (action)
        {
            case binaryToFile:
            case fileToBinary:
            case convertImage:
                mult = true;
                break;
            default:
                mult = false;
        }
        
        return mult;
    }

    @Override
    public boolean supportsMultipleTargets()
    {
        return false;
    }

    @Override
    public int getExpectedSourceCount()
    {
        int count = 1;
        switch (action)
        {
            // Both support up to 3 sources with 1 or 2 optional
            case binaryToFile:
            case fileToBinary:
                count = -1;
                break;
            case convertImage:
                count = 3;
                break;
            default:
                count = 1;
        }
        
        return count;
    }

    private void setBinaryAction(BinaryTransformComplexType ct)
        throws ETLException
    {
        if ( ct != null )
        {
            action = Action.valueOf(ct.getAction());
        }
    }
    
    public List<DataCell> transform(List<DataColumn> cols, List<DataCell> incoming)
        throws ETLException
    {
        ArrayList<DataCell> tlist = new ArrayList();
        DataCell source = null;
        DataColumn tcol = null;

        validateIncomingSources(incoming);
        
        try
        {
            switch (action)
            {
                case fileToBinary:
                    /*
                    tcol = getTransformDataColumn(cols, incoming, 0);
                    tcol.setDataType(DataTypes.BINARY);
                    String path = null;
                    int length = -1;
                    long checksum = 0;
                    Object val = null;
                    
                    for ( int i=0; i<incoming.size(); i++ )
                    {
                        switch (i)
                        {
                            case 0:
                                DataCell p = incoming.get(i);
                                if ( p.getValue() != null )
                                {
                                    path = String.valueOf(p.getValue());
                                }
                                break;
                            case 1:
                                DataCell l = incoming.get(i);
                                if ( l != null && l.getColumn().getDataType() == DataTypes.INT )
                                {
                                    try
                                    {
                                        length = Integer.parseInt(String.valueOf(l.getValue()));
                                    }
                                    catch (NumberFormatException e)
                                    {
                                        logger.warn("Incoming source column 2 cannot be converted to an integer, not assigned: " + l);
                                    }
                                }
                                break;
                            case 2:
                                DataCell c = incoming.get(i);
                                if ( c != null && c.getColumn().getDataType() == DataTypes.LONG )
                                {
                                    try
                                    {
                                        checksum = Long.parseLong(String.valueOf(c.getValue()));
                                    }
                                    catch (NumberFormatException e)
                                    {
                                        logger.warn("Incoming source column 3 cannot be converted to a long integer, not assigned: " + c);
                                    }
                                }
                                break;
                        }
                    }
                    
                    try
                    {
                        File binfile = new File(path);
                        if ( path != null && binfile.exists() )
                        {
                            BinaryObject original = new BinaryObject(binfile, length, checksum);
                            if (referenceOriginal)
                            {
                                val = original;
                            }
                            // Null file to create temp file so binary modifications don't modify original
                            else
                            {
                                BinaryObject copy = new BinaryObject(null, length, checksum);
                                copy.setData(original.getData());
                                val = copy;
                            }
                        }
                        else
                        {
                            val = null;
                        }
                        
                    }
                    catch (IOException e)
                    {
                        logger.warn("Unable to create BinaryObject: " + path, e);
                    }
                    
                    tlist.add(new DataCell(tcol, val));
                    break;*/
                    throw new UnsupportedOperationException("Transform fileToBinary no longer supported. Use a binary input adapter instead");
                case binaryToFile:
                    
                   /* tcol = getTransformDataColumn(cols, incoming, 0);
                    tcol.setDataType(DataTypes.STRING);
                    BinaryObject bo = null;
                    String root = null;
                    path = null;
                    File file = null;
                    DataCell b = null;
                    
                    for ( int i=0; i<incoming.size(); i++ )
                    {
                        switch (i)
                        {
                            // Binary data containing the data to output
                            case 0:
                                b = incoming.get(i);
                                if ( b != null && b.getColumn().getDataType() == DataTypes.BINARY )
                                {
                                    bo = (BinaryObject)b.getValue();
                                }
                                break;
                            // Filename that may or may not include full path
                            case 1:
                                DataCell p = incoming.get(i);
                                if ( p == null || p.getValue() == null )
                                {
                                    throw new TransformException(p, "Cannot convert to file when source column 2 is null");
                                }
                                path = String.valueOf(p.getValue());
                                break;
                            // Root directory where the filename parameter is based from
                            case 2:
                                DataCell r = incoming.get(i);
                                if ( r.getValue() != null )
                                {
                                    root = String.valueOf(r.getValue());
                                }
                                break;
                        }
                    }
                    
                    if ( path.matches(FileAdapter.unixAbsoluteDir.pattern()) ||
                         path.matches(FileAdapter.windowsAbsoluteDir.pattern()) )
                    {
                        // Absolute directory
                        file = new File(path);
                    }
                    else if ( root != null )
                    {
                        file = new File(root, path);
                    }
                    else 
                    {
                        file = new File(task.getETLJobWrapper().getETLRoot(), path);
                    }
                    
                    val = null;
                    if ( bo != null )
                    {
                        try
                        {
                            // Need to create an empty file or the destination file will be deleted on exit since it assumes it's temporary if the file doesn't exist
                            logger.debug("Creating destination file: " + file); 
                            file.getParentFile().mkdirs();
                            file.createNewFile();
                            logger.debug("File created: " + file.createNewFile());
                            
                            BinaryObject dest = new BinaryObject(file, -1, 0);
                            dest.setData(bo.getData());
                            val = file.getAbsolutePath();
                        }
                        catch (IOException e)
                        {
                            throw new TransformException(b, "Unable to convert binary to file", e);
                        }
                    }
                    
                    tlist.add(new DataCell(tcol, val));
                    break;*/
                    throw new UnsupportedOperationException("Transform binaryToFile no longer supported. Use a binary output adapater instead.");
                case extractByteLength:
                    tcol = getTransformDataColumn(cols, incoming, 0);
                    tcol.setDataType(DataTypes.INT);
                    source = incoming.get(0);
                    if ( source != null && source.getColumn().getDataType() != DataTypes.BINARY )
                    {
                        throw new TransformException(source, "Source column must be a binary data type");
                    }
                    
                    Integer len = null;
                    BinaryObject bo = (BinaryObject)source.getValue();
                    if ( bo != null )
                    {
                        len = bo.getBinaryDataLength();
                    }
                    
                    tlist.add(new DataCell(tcol, len));
                    break;
                case extractChecksum:
                    tcol = getTransformDataColumn(cols, incoming, 0);
                    tcol.setDataType(DataTypes.LONG);
                    source = incoming.get(0);
                    if ( source != null && source.getColumn().getDataType() != DataTypes.BINARY )
                    {
                        throw new TransformException(source, "Source column must be a binary data type");
                    }
                    
                    Long cs = null;
                    bo = (BinaryObject)source.getValue();
                    if ( bo != null )
                    {
                        cs = bo.getChecksum();
                    }
                    
                    tlist.add(new DataCell(tcol, cs));
                    break;
                case convertImage:
                    tcol = getTransformDataColumn(cols, incoming, 0);
                    tcol.setDataType(DataTypes.BINARY);
                    source = incoming.get(0);
                    DataCell sf = incoming.get(1);
                    DataCell tf = incoming.get(2);
                    ImageTypes sformat = null;
                    ImageTypes tformat = null;

                    if ( sf == null || sf.getValue() == null )
                    {
                        throw new TransformException(sf, "SourceImageFormat not declared, unable to transform");
                    }

                    if ( tf == null || tf.getValue() == null )
                    {
                        throw new TransformException(tf, "TargetImageFormat not declared, unable to transform");
                    }
                    
                    if ( source != null && source.getColumn().getDataType() != DataTypes.BINARY )
                    {
                        throw new TransformException(source, "Binary source is not of BINARY data type");
                    }
                    
                    Object val = null;
                    if ( source.getValue() != null )
                    {
                        try
                        {
                            sformat = ImageTypes.valueOf(String.valueOf(sf.getValue()));
                        }
                        catch (IllegalArgumentException e)
                        {
                            throw new TransformException(sf, "SourceImageFormat does not match expected formats: " + JavaUtils.enumToList(ImageTypes.class), e);
                        }

                        try
                        {
                            tformat = ImageTypes.valueOf(String.valueOf(tf.getValue()));
                        }
                        catch (IllegalArgumentException e)
                        {
                            throw new TransformException(tf, "TargetImageFormat does not match expected formats: " + JavaUtils.enumToList(ImageTypes.class), e);
                        }
                        
                        try
                        {
                            val = new BinaryObject(convertImage(sformat, tformat, ((BinaryObject)source.getValue()).getData()));
                        }
                        catch (IOException e)
                        {
                            throw new TransformException(source, "Failed to convert image to: " + tformat, e);
                        }
                    }
                    
                    tlist.add(new DataCell(tcol, val));
                    break;
                case deleteBinaryFile:
                    tcol = getTransformDataColumn(cols, incoming, 0);
                    tcol.setDataType(DataTypes.BOOLEAN);
                    source = incoming.get(0);
                    String path = "";
                    boolean success = false;
                    if ( source.getValue() != null )
                    {
                        path = String.valueOf(source.getValue());
                    }
                    
                    try
                    {
                        File binfile = new File(path);
                        if ( path != null && binfile.exists() )
                        {
                            new BinaryObject(binfile);
                            binfile.deleteOnExit();
                            success = true;
                        }
                        
                    }
                    catch (IOException e)
                    {
                        logger.warn("Unable to read BinaryObject to delete: " + path, e);
                    }
                    
                    tlist.add(new DataCell(tcol, success));
                    break;
                case changeEncoding:
                    break;
                default:
                    throw new TransformException(incoming.get(0), "Unsupported binary transform action: " + action);
            }
        }
        catch (TransformException e)
        {
            throw e;
        }
        catch (ETLException e)
        {
            throw new TransformException(source, e);
        }
        return tlist;
    }

    private byte[] convertImage(ImageTypes from, ImageTypes to, byte[] source)
        throws IOException
    {
        byte[] image = new byte[0];
        
        // Check if an image conversion is necessary
        if ( from != to )
        {
            logger.debug("Converting image from format " + from + " to " + to);

            ByteArrayInputStream bais = new ByteArrayInputStream(source);
            BufferedImage bimg = ImageIO.read(bais);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            try
            {

                boolean converted = ImageIO.write(bimg, String.valueOf(to), baos);
                if ( !converted )
                {
                    throw new IOException("Unable to convert input image to " + String.valueOf(to).toUpperCase());
                }

                logger.debug("Successfully converted input image to " + to);
                image = baos.toByteArray();
            }
            finally
            {
                baos.close();
            }

            //image = loadPhoto(temp.getAbsolutePath());
        }
        else
        {
            logger.debug("Image formats are identical, no conversion performed");
            image = source;
        }
        
        return image;
    }

    public String toString()
    {
        return action + "(" + getSourceColumns() + ")->" + getTargetColumns();
    }
}
