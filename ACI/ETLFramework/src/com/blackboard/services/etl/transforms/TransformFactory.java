/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.transforms;

import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.jaxb.AggregateTransformComplexType;
import com.blackboard.services.etl.jaxb.ArithmeticTransformComplexType;
import com.blackboard.services.etl.jaxb.BinaryTransformComplexType;
import com.blackboard.services.etl.jaxb.BooleanTransformComplexType;
import com.blackboard.services.etl.jaxb.DateTransformComplexType;
import com.blackboard.services.etl.jaxb.LookupTransformComplexType;
import com.blackboard.services.etl.jaxb.PassthruTransformComplexType;
import com.blackboard.services.etl.jaxb.StringTransformComplexType;
import com.blackboard.services.etl.jaxb.TransformComplexType;
import com.blackboard.services.etl.jaxb.HierarchyTransformComplexType;
import com.blackboard.services.etl.tasks.AbstractTask;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;

/**
 *
 * @author crusnak
 */
public class TransformFactory
{
    public static AbstractTransform createTransform(TransformComplexType tform, AbstractTask task)
        throws ETLException
    {
        AbstractTransform transform = null;
        if ( tform != null )
        {
            if ( tform instanceof PassthruTransformComplexType )
            {
                transform = new PassthruTransform((PassthruTransformComplexType)tform, task);
            }
            else if ( tform instanceof ArithmeticTransformComplexType )
            {
                transform = new ArithmeticTransform((ArithmeticTransformComplexType)tform, task);
            }
            else if ( tform instanceof DateTransformComplexType )
            {
                transform = new DateTransform((DateTransformComplexType)tform, task);
            }
            else if ( tform instanceof LookupTransformComplexType )
            {
                transform = new LookupTransform((LookupTransformComplexType)tform, task);
            }
            else if ( tform instanceof StringTransformComplexType )
            {
                transform = new StringTransform((StringTransformComplexType)tform, task);
            }
            else if ( tform instanceof BooleanTransformComplexType )
            {
                transform = new BooleanTransform((BooleanTransformComplexType)tform, task);
            }
            else if ( tform instanceof BinaryTransformComplexType )
            {
                transform = new BinaryTransform((BinaryTransformComplexType)tform, task);
            }
            else if ( tform instanceof HierarchyTransformComplexType )
            {
                transform = new HierarchyTransform((HierarchyTransformComplexType)tform, task);
            }
        }
        return transform;
    }
}
