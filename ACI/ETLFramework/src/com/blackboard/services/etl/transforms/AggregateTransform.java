/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.transforms;

import com.blackboard.services.etl.data.Argument;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.exception.TransformException;
import com.blackboard.services.etl.exception.TransformIgnoreException;
import com.blackboard.services.etl.jaxb.AggregateDataComplexType;
import com.blackboard.services.etl.jaxb.AggregateTransformComplexType;
import com.blackboard.services.etl.jaxb.ArgumentComplexType;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.ObjectFactory;
import com.blackboard.services.etl.jaxb.PassthruTransformComplexType;
import com.blackboard.services.etl.tasks.AbstractTask;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.blackboard.services.utils.JavaUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author crusnak
 */
public class AggregateTransform
{
    public static enum Function { sum, max, min, average, group, count, distinct, distinctCount, list, payment };
    public static enum Type { group, total, depletion };

    private Logger logger = null;
    private Function function = null;
    private Type type = null;
    private Argument source = null;
    private DataColumn target = null;
    private List<String> filterColumns = new ArrayList();
    private List<String> valueColumns = new ArrayList();
    private String modifier = "";


    public AggregateTransform(AggregateDataComplexType.Aggregate ct, AbstractTask task)
        throws ETLException
    {
        logger = Logger.getLogger(this.getClass());
        type = Type.group;
        setFunction(ct.getFunction());
        target = new DataColumn(ct.getTargetColumn().getType(), ct.getTargetColumn().getValue());
        modifier = ct.getModifier() != null ? ct.getModifier() : "";

        if ( ct.getSourceColumn() != null )
        {
            source = new Argument(ct.getSourceColumn(), task);
        }
    }

    public AggregateTransform(AggregateDataComplexType.Total ct, AbstractTask task)
        throws ETLException
    {
        logger = Logger.getLogger(this.getClass());
        type = Type.total;
        setFunction(ct.getFunction());
        target = new DataColumn(ct.getTargetColumn().getType(), ct.getTargetColumn().getValue());
        filterColumns.addAll(ct.getGroupColumn());

        if ( ct.getSourceColumn() != null )
        {
            source = new Argument(ct.getSourceColumn(), task);
        }
    }

    public AggregateTransform(AggregateDataComplexType.Depletion ct, AbstractTask task)
        throws ETLException
    {
        logger = Logger.getLogger(this.getClass());
        type = Type.depletion;
        setFunction(ct.getFunction());
        target = new DataColumn(ct.getTargetColumn().getType(), ct.getTargetColumn().getValue());
        
        filterColumns.add(ct.getDepletionIdentityColumn());
        filterColumns.add(ct.getSourceIdentityColumn());
        valueColumns.add(ct.getDepletionValueColumn());
        valueColumns.add(ct.getSourceValueColumn());
    }
    
    public Type getAggregateType()
    {
        return type;
    }

    public Function getFunction()
    {
        return function;
    }

    public List<String> getGroupFilterColumnNames()
    {
        return filterColumns;
    }

    public List<String> getGroupValueColumnNames()
    {
        return valueColumns;
    }
    
    private void setFunction(String o)
        throws ETLException
    {
        if ( o == null )
        {
            throw new ETLException("Function cannot be null for AggregateTransforms, use one of: " + JavaUtils.enumToList(Function.class));
        }

        try
        {
            function = Function.valueOf(o);
        }
        catch (IllegalArgumentException e)
        {
            throw new ETLException("Unsupported function '" + o + "', expected: " + JavaUtils.enumToList(Function.class), e);
        }
    }

    public Argument getSourceArgument()
    {
        return source;
    }

    public DataColumn getTargetColumn()
    {
        return target;
    }

    public DataCell transform(DataCell incoming, DataCell previous)
        throws ETLException
    {
        DataCell source = null;
        DataCell current = null;
        DataCell out = null;
        DataColumn tcol = null;

        source = incoming;
        current = previous;

        if ( previous == null )
        {
            tcol = target;
        }
        else
        {
            tcol = previous.getColumn();
        }

        if ( tcol == null )
        {
            tcol = source.getColumn();
            if ( tcol == null )
            {
                tcol = new DataColumn(source.getColumn().getDataType(), source.getColumn().getColumnName());
            }
            else
            {
                tcol = new DataColumn(tcol.getDataType(), tcol.getColumnName());
            }
        }

        logger.debug("Target col: " + tcol);
        if ( tcol.getDataType() == null )
        {
            throw new ETLException("Undefined dataType declared for aggregate targetColumn: " + tcol.getColumnName());
        }

        try
        {
            Object sval = null;
            Object cval = null;
            if ( (source == null || source.getValue() == null) && function == Function.count )
            {
                sval = 1;
                logger.debug("Assigning " + sval + " to source value for count function");
            }
            else if ( source == null || source.getValue() == null )
            {
                sval = getZero(tcol.getDataType());
                logger.debug("Source value is null, assigning null or zero");
            }
            else
            {
                sval = source.getValue();
            }

            if ( (current == null || current.getValue() == null) && function == Function.min )
            {
                if ( tcol.getDataType() == DataTypes.DATE )
                {
                    cval = new Date(Long.MAX_VALUE);
                }
                else
                {
                    cval = Long.MAX_VALUE;
                }
                logger.debug("Group value is null, assigning max value for min function");
            }
            else if ( current == null || current.getValue() == null )
            {
                cval = getZero(tcol.getDataType());
                logger.debug("Group value is null, assigning null or zero");
            }
            else
            {
                cval = current.getValue();
            }

            switch (function)
            {
                case average:
                    throw new UnsupportedOperationException("average function not yet supported");
                case max:
                    out = max(tcol, cval, sval);
                    break;
                case min:
                    out = min(tcol, cval, sval);
                    break;
                case sum:
                    out = sum(tcol, cval, sval);
                    break;
                case group:
                    out = group(tcol, cval, sval, modifier);
                    break;
                case list:
                    out = list(tcol, cval, sval);
                    break;
                case count:
                    out = sum(tcol, cval, sval);
                    break;
                case payment:
                    out = pay(tcol, cval, sval);
                    break;
                case distinct:
                    //out = new DataCell(tcol, sval);
                    throw new UnsupportedOperationException("No longer supported. Utilize a grouped aggregate to extract distinct values.");
                case distinctCount:
                    //out = sum(tcol, cval, sval);
                    throw new UnsupportedOperationException("No longer supported. Utilize an aggregate on distinct source data with a count function and no grouping.");
            }
        }
        catch (ETLException e)
        {
            if ( e instanceof TransformIgnoreException )
            {
                throw (TransformIgnoreException)e;
            }
            else
            {
                throw new TransformException(source, e);
            }
        }

        return out;
    }

    private Object getZero(DataTypes type)
    {
        Object zero = null;
        if ( type != null )
        {
            switch (type)
            {
                case DOUBLE:
                    zero = 0.0;
                    break;
                case FLOAT:
                    zero = 0.0;
                    break;
                case INT:
                    zero = 0L;
                    break;
                case LONG:
                    zero = 0L;
                    break;
                case DATE:
                    zero = new Date(0);
                    break;
            }
        }

        return zero;
    }

    private DataCell sum(DataColumn col, Object op1, Object op2)
        throws TransformException, ETLException
    {
        DataCell cell = null;
        try
        {
            switch (col.getDataType())
            {
                case INT:
                    long lo1 = Long.parseLong(String.valueOf(op1));
                    long lo2 = Long.parseLong(String.valueOf(op2));
                    cell = new DataCell(col, lo1+lo2);
                    break;
                case LONG:
                    lo1 = Long.parseLong(String.valueOf(op1));
                    lo2 = Long.parseLong(String.valueOf(op2));
                    cell = new DataCell(col, lo1+lo2);
                    break;
                case FLOAT:
                    double do1 = Double.parseDouble(String.valueOf(op1));
                    double do2 = Double.parseDouble(String.valueOf(op2));
                    cell = new DataCell(col, do1+do2);
                    break;
                case DOUBLE:
                    do1 = Double.parseDouble(String.valueOf(op1));
                    do2 = Double.parseDouble(String.valueOf(op2));
                    cell = new DataCell(col, do1+do2);
                    break;
                default:

                    throw new TransformException(null, "Cannot perform sum on source data type: " + col.getDataType() +
                                                 ", allowed types: " + JavaUtils.enumToList(getAllowedNumericDataTypes()));
            }
        }
        catch (NumberFormatException e)
        {
            throw new TransformException(null, "Unable to convert one of " + op1 + ", " + op2 + " to: " + col.getColumnClass(), e);
        }
        return cell;
    }

    private DataCell pay(DataColumn col, Object op1, Object op2)
        throws TransformException, ETLException
    {
        DataCell cell = null;
        try
        {
            logger.debug("Handling payment of " + op2 + " with " + op1);
            switch (col.getDataType())
            {
                case INT:
                    long lo1 = Long.parseLong(String.valueOf(op1));
                    long lo2 = Long.parseLong(String.valueOf(op2));
                    long v = lo1 - lo2;
                    if ( v <= 0 )
                    {
                        cell = new DataCell(col, 0);
                    }
                    else
                    {
                        cell = new DataCell(col, v);
                    }
                    break;
                case LONG:
                    lo1 = Long.parseLong(String.valueOf(op1));
                    lo2 = Long.parseLong(String.valueOf(op2));
                    v = lo1 - lo2;
                    if ( v <= 0 )
                    {
                        cell = new DataCell(col, 0);
                    }
                    else
                    {
                        cell = new DataCell(col, v);
                    }
                    break;
                case FLOAT:
                    double do1 = Double.parseDouble(String.valueOf(op1));
                    double do2 = Double.parseDouble(String.valueOf(op2));
                    double dv = do1 - do2;
                    if ( dv <= 0 )
                    {
                        cell = new DataCell(col, 0);
                    }
                    else
                    {
                        cell = new DataCell(col, dv);
                    }                    
                    break;
                case DOUBLE:
                    do1 = Double.parseDouble(String.valueOf(op1));
                    do2 = Double.parseDouble(String.valueOf(op2));
                    dv = do1 - do2;
                    if ( dv <= 0 )
                    {
                        cell = new DataCell(col, 0);
                    }
                    else
                    {
                        cell = new DataCell(col, dv);
                    }    
                    break;
                default:

                    throw new TransformException(null, "Cannot perform pay on source data type: " + col.getDataType() +
                                                 ", allowed types: " + JavaUtils.enumToList(getAllowedNumericDataTypes()));
            }
        }
        catch (NumberFormatException e)
        {
            throw new TransformException(null, "Unable to convert one of " + op1 + ", " + op2 + " to: " + col.getColumnClass(), e);
        }
        return cell;
    }

    private DataCell group(DataColumn col, Object op1, Object op2, String separator)
        throws TransformException, ETLException
    {
        DataCell cell = null;
        try
        {
            if ( op1 == null )
            {
                cell = new DataCell(col, String.valueOf(op2));
            }
            else
            {
                cell = new DataCell(col, String.valueOf(op1) + separator + String.valueOf(op2));
            }
            
        }
        catch (NumberFormatException e)
        {
            throw new TransformException(null, "Unable to convert one of " + op1 + ", " + op2 + " to: " + col.getColumnClass(), e);
        }
        return cell;
    }

    private DataCell list(DataColumn col, Object op1, Object op2)
        throws TransformException, ETLException
    {
        DataCell cell = null;
        if ( modifier != null && modifier.trim().length() > 0 )
        {
            cell = group(col, op1, modifier + op2 + modifier, ",");
        }
        else
        {
            cell = group(col, op1, op2, ",");
        }
        
        return cell;
    }

    private DataCell min(DataColumn col, Object op1, Object op2)
        throws TransformException, ETLException
    {
        DataCell cell = null;
        try
        {
            switch (col.getDataType())
            {
                case INT:
                    long lo1 = Long.parseLong(String.valueOf(op1));
                    long lo2 = Long.parseLong(String.valueOf(op2));
                    cell = new DataCell(col, Math.min(lo1, lo2));
                    break;
                case LONG:
                    lo1 = Long.parseLong(String.valueOf(op1));
                    lo2 = Long.parseLong(String.valueOf(op2));
                    cell = new DataCell(col, Math.min(lo1, lo2));
                    break;
                case FLOAT:
                    double do1 = Double.parseDouble(String.valueOf(op1));
                    double do2 = Double.parseDouble(String.valueOf(op2));
                    cell = new DataCell(col, Math.min(do1, do2));
                    break;
                case DOUBLE:
                    do1 = Double.parseDouble(String.valueOf(op1));
                    do2 = Double.parseDouble(String.valueOf(op2));
                    cell = new DataCell(col, Math.min(do1, do2));
                    break;
                case DATE:
                    lo1 = ((Date)op1).getTime();
                    lo2 = ((Date)op2).getTime();
                    cell = new DataCell(col, new Date(Math.min(lo1, lo2)));
                    break;
                default:

                    throw new TransformException(null, "Cannot perform min on source data type: " + col.getDataType() +
                                                 ", allowed types: " + JavaUtils.enumToList(getAllowedMinMaxDataTypes()));
            }
        }
        catch (NumberFormatException e)
        {
            throw new TransformException(null, "Unable to convert one of " + op1 + ", " + op2 + " to: " + col.getColumnClass(), e);
        }
        return cell;
    }

    private DataCell max(DataColumn col, Object op1, Object op2)
        throws TransformException, ETLException
    {
        DataCell cell = null;
        try
        {
            switch (col.getDataType())
            {
                case INT:
                    long lo1 = Long.parseLong(String.valueOf(op1));
                    long lo2 = Long.parseLong(String.valueOf(op2));
                    cell = new DataCell(col, Math.max(lo1, lo2));
                    break;
                case LONG:
                    lo1 = Long.parseLong(String.valueOf(op1));
                    lo2 = Long.parseLong(String.valueOf(op2));
                    cell = new DataCell(col, Math.max(lo1, lo2));
                    break;
                case FLOAT:
                    double do1 = Double.parseDouble(String.valueOf(op1));
                    double do2 = Double.parseDouble(String.valueOf(op2));
                    cell = new DataCell(col, Math.max(do1, do2));
                    break;
                case DOUBLE:
                    do1 = Double.parseDouble(String.valueOf(op1));
                    do2 = Double.parseDouble(String.valueOf(op2));
                    cell = new DataCell(col, Math.max(do1, do2));
                    break;
                case DATE:
                    lo1 = ((Date)op1).getTime();
                    lo2 = ((Date)op2).getTime();
                    cell = new DataCell(col, new Date(Math.max(lo1, lo2)));
                    break;
                case STRING:
                    if ( op1 != null && op2 != null && String.valueOf(op1).compareTo(String.valueOf(op2)) < 0 )
                    {
                        cell = new DataCell(col, String.valueOf(op2));
                    }
                    else if ( op1 == null && op2 != null )
                    {
                        cell = new DataCell(col, String.valueOf(op2));
                    }
                    else
                    {
                        cell = new DataCell(col, String.valueOf(op1));
                    }
                    break;
                default:

                    throw new TransformException(null, "Cannot perform max on source data type: " + col.getDataType() +
                                                 ", allowed types: " + JavaUtils.enumToList(getAllowedMinMaxDataTypes()));
            }
        }
        catch (NumberFormatException e)
        {
            throw new TransformException(null, "Unable to convert one of " + op1 + ", " + op2 + " to: " + col.getColumnClass(), e);
        }
        return cell;
    }

    private EnumSet getAllowedNumericDataTypes()
    {
        return EnumSet.of(DataTypes.INT, DataTypes.LONG, DataTypes.FLOAT, DataTypes.DOUBLE);
    }

    private EnumSet getAllowedMinMaxDataTypes()
    {
        return EnumSet.of(DataTypes.INT, DataTypes.LONG, DataTypes.FLOAT, DataTypes.DOUBLE, DataTypes.STRING);
    }

    public String toString()
    {
        return function + (source != null?"(" + source.getValue() + ")":"") + 
               (filterColumns.size()>0?"" + filterColumns:"") + "->" + target.getColumnName();
    }
}
