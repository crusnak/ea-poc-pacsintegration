/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.transforms;

import com.blackboard.services.etl.data.Argument;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.etl.data.LookupTable;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.exception.TransformException;
import com.blackboard.services.etl.exception.TransformIgnoreException;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.LookupTransformComplexType;
import com.blackboard.services.etl.tasks.AbstractTask;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author crusnak
 */
public class LookupTransform extends AbstractTransform
{
    private LookupTable table = null;
    private String dataName = null;
    private List<String> keyColumns = new ArrayList();
    private String valueColumn = null;
    private boolean ignoreNull = false;

    public LookupTransform(LookupTransformComplexType ct, AbstractTask task)
        throws ETLException
    {
        super(ct, task);
        setLookupTable(ct);
        setDataTable(ct);
        ignoreNull = ct.isIgnoreNulls();
    }

    private void setLookupTable(LookupTransformComplexType ct)
        throws ETLException
    {
        String tname = ct.getTableName();
        if ( tname != null )
        {
            table = task.getETLJobWrapper().retrieveLookupTable(tname);
            if ( table == null )
            {
                throw new ETLException("LookupTransform referencing lookup table that is not defined: " + tname);
            }
        }
    }

    private void setDataTable(LookupTransformComplexType ct)
        throws ETLException
    {
        if ( ct.getData() != null )
        {
            dataName = ct.getData().getName();
            if ( task.getETLJobWrapper().retrieveNamedData(dataName) == null )
            {
                throw new ETLException("Reference to undefined data source in lookup transform: " + dataName);
            }

            keyColumns = new ArrayList(ct.getData().getKeyColumn());
            if ( keyColumns.size() == 0 )
            {
                throw new ETLException("At least one column must be declared as a key in a lookup transform using data source as table");
            }

            valueColumn = ct.getData().getValueColumn();
            if ( valueColumn == null || valueColumn.trim().length() == 0 )
            {
                throw new ETLException("A column must be declared as a value in a lookup transform using data source as table");
            }
        }
    }
    
    public Type getTransformType()
    {
        return Type.lookup;
    }

    @Override
    public boolean supportsMultipleSources()
    {
        return true;
    }

    @Override
    public boolean supportsMultipleTargets()
    {
        return false;
    }

    @Override
    public int getExpectedSourceCount()
    {
        return -1;
    }

    public List<DataCell> transform(List<DataColumn> cols, List<DataCell> incoming)
        throws ETLException
    {
        ArrayList<DataCell> targets = new ArrayList();
        DataCell source = null;
        DataCell out = null;
        DataColumn tcol = null;

        validateIncomingSources(incoming);
        tcol = getTransformDataColumn(cols, incoming, 0);

        try
        {
            // If table is null, load the data source declared at runtime
            if ( table == null )
            {
                table = createLookupTableFromDataSource(dataName, keyColumns, valueColumn);
            }

            logger.debug("Lookup table: " + table);
            
            Object val = null;
            String key = "";
            for ( int i=0; i<incoming.size(); i++ )
            {
                source = incoming.get(i);
                if ( source.getValue() != null )
                {
                    key += String.valueOf(source.getValue());
                }

                if ( i < incoming.size()-1 )
                {
                    key += ETLJobWrapper.TABLE_KEY_SEPARATOR;
                }
            }

            logger.debug("Lookup key: " + key);
            logger.debug("Keys: " + table.keySet());
            val = table.get(key);
            
            // If null, the value of the cell should be picked up if a default value was defined for the column
            // in AbstractTransform.getTransformDataColumn
            /*if ( val == null && isDefaultValueDefined() )
            {
                val = getDefaultValue();
            }*/

            // If the result is null, no default value is defined, and ignoreNull is set, then no 
            // assignment is to take place
            if ( ignoreNull && !tcol.isDefaultValueDefined() && val == null )
            {
                throw new TransformIgnoreException(source, "Looked up value is null and ignoreNull is set, not assigning null to target");
            }

            tcol.setDataType(DataTypes.STRING);
            out = new DataCell(tcol, val);
        }
        catch (ETLException e)
        {
            if ( e instanceof TransformIgnoreException )
            {
                throw (TransformIgnoreException)e;
            }
            else
            {
                throw new TransformException(source, e);
            }
        }

        targets.add(out);
        return targets;
    }
    
    private LookupTable createLookupTableFromDataSource(String dsName, List<String> keycols, String valcol)
        throws ETLException
    {
        LookupTable table = new LookupTable(dsName);
        DataSet ds = task.getETLJobWrapper().retrieveNamedData(dsName);
        if ( ds.getType() != DataSet.Type.relational )
        {
            throw new UnsupportedOperationException("LookupTransforms do not support referencing hierarchical data sets yet");
        }
        
        RelationalContainer dc = (RelationalContainer)ds;
        for ( Map<String,DataCell> rowdata: dc.getMappedData() )
        {
            String val = null;
            DataCell vcell = rowdata.get(valcol);
            if ( vcell != null && vcell.getValue() != null )
            {
                val = String.valueOf(vcell.getValue());
            }

            String key = "";
            for ( int i=0; i<keycols.size(); i++ )
            {
                String keycol = keycols.get(i);
                DataCell kcell = rowdata.get(keycol);
                if ( kcell != null && kcell.getValue() != null )
                {
                    key += String.valueOf(kcell.getValue());
                }

                if ( i < keycols.size()-1 )
                {
                    key += ETLJobWrapper.TABLE_KEY_SEPARATOR;
                }
            }

            table.put(key, val);
        }
        
        return table;
    }

    public String toString()
    {
        return "lookup(" + (table!=null?table.getTableName():dataName) + "," + getSourceColumns() + ")->" + getTargetColumns().get(0);
    }
}
