/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.transforms;

import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.TransformSource;
import com.blackboard.services.etl.data.TransformTarget;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.exception.TransformException;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.TransformComplexType;
import com.blackboard.services.etl.tasks.AbstractTask;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author crusnak
 */
public abstract class AbstractTransform
{
    public static enum Type { passthru, arithmetic, date, lookup, string, aggregate, bool, binary, hierarchy };

    private static final String TARGET_DEFAULT =    "default";
    private static final String TARGET_TYPE =       "type";

    protected ArrayList<TransformSource> sources = new ArrayList();
    protected ArrayList<TransformTarget> targets = new ArrayList();
    protected AbstractTask task = null;
    protected Logger logger = null;
    protected boolean temporary = false;

    protected AbstractTransform(TransformComplexType ct, AbstractTask t)
        throws ETLException
    {
        logger = Logger.getLogger(this.getClass());
        task = t;
        if ( ct != null )
        {
            temporary = ct.isTemporary();
            setSourceColumns(ct.getSourceColumn());
            setTargetColumns(ct.getTargetColumn());
        }
    }

    /*protected AbstractTransform(String sc, String st, String sv, String tc, String def)
        throws ETLException
    {
        sourceColumn = sc;
        if ( "null".equals(sv) ) sv = null;
        setSourceValue(st, sv);
        targetColumn = tc;
        defaultValue = def;
        logger = Logger.getLogger(this.getClass());
    }*/

    public List<TransformSource> getSourceColumns()
    {
        return sources;
    }

    public AbstractTask getParentTask()
    {
        return task;
    }
    
    protected void setSourceColumns(List<TransformComplexType.SourceColumn> sct)
        throws ETLException
    {
        for ( TransformComplexType.SourceColumn scol: sct )
        {
            sources.add(new TransformSource(scol, task));
        }
    }

    public List<String> getTargetColumnNames()
    {
        ArrayList<String> names = new ArrayList();
        for ( TransformTarget target: targets )
        {
            names.add(target.getColumnName());
        }
        
        return names;
    }
    
    public List<TransformTarget> getTargetColumns()
    {
        return new ArrayList(targets);
    }
    
    public boolean isTemporary()
    {
        return temporary;
    }

    protected void setTargetColumns(List<TransformComplexType.TargetColumn> tct)
        throws ETLException
    {
        for ( TransformComplexType.TargetColumn tcol: tct )
        {
            targets.add(new TransformTarget(tcol));
        }
    }

    /*protected void setSourceValue(String type, String value)
        throws ETLException
    {
        if ( type != null && type.trim().length() > 0 &&
             value != null & value.trim().length() > 0 )
        {
            DataColumn col = new DataColumn(type, "");
            this.sourceValue = new DataCell(col, value);
        }
    }*/

    /*public String getDefaultValue()
    {
        return getDefaultValue(getTargetColumns().get(0));
    }

    protected String getDefaultValue(String target)
    {
        return targets.get(target).get(TARGET_DEFAULT);
    }

    protected String getOverridenDataType(String target)
    {
        return targets.get(target).get(TARGET_TYPE);
    }

    public boolean isDefaultValueDefined()
    {
        return isDefaultValueDefined(getTargetColumns().get(0));
    }

    public boolean isDefaultValueDefined(String target)
    {
        String defaultValue = getDefaultValue(target);
        return defaultValue != null && defaultValue.trim().length() > 0;
    }

    public boolean isDataTypeOverriden()
    {
        return isDataTypeOverriden(getTargetColumns().get(0));
    }

    public boolean isDataTypeOverriden(String target)
    {
        String dataType = getOverridenDataType(target);
        return dataType != null && dataType.trim().length() > 0;
    }*/

    public List<DataCell> transform(List<DataCell> incoming)
        throws ETLException
    {
        if ( incoming == null || incoming.size() == 0 )
        {
            throw new TransformException(null, "Incoming data is null, cannot transform.");
        }

        ArrayList<DataColumn> cols = new ArrayList();
        for ( DataCell c: incoming )
        {
            cols.add(c.getColumn());
        }

        return transform(cols, incoming);
    }

    protected void validateIncomingSources(List<DataCell> incoming)
        throws ETLException
    {
        if ( supportsMultipleSources() )
        {
            // A -1 source count indicates the number of sources can be dynamic (string transforms)
            if ( incoming == null || incoming.isEmpty() )
            {
                throw new ETLException("Source list must contain one or more DataCells for: " + this);
            }
            else if ( getExpectedSourceCount() > 0 && incoming.size() != getExpectedSourceCount() )
            {
                throw new ETLException("Source list must contain exactly " + getExpectedSourceCount() + " DataCells for: " + this);
            }
        }
        else
        {
            if ( incoming == null || incoming.size() != 1 )
            {
                throw new ETLException("Source list must contain one DataCell for: " + this);
            }
        }
    }

    protected DataColumn getTransformDataColumn(List<DataColumn> cols, List<DataCell> incoming, int index)
        throws TransformException
    {
        DataColumn scol = null;
        DataColumn tcol = null;
        DataCell source = null;

        try
        {
            logger.debug("Incoming: " + incoming);
            logger.debug("Targets: " + cols);
            if ( cols != null && !cols.isEmpty() )
            {
                tcol = cols.get(index);
            }

            if ( tcol == null )
            {
                TransformTarget target = getTargetColumns().get(index);

                try
                {
                    DataTypes datatype = null;

                    // If the target's data type is overridden use that, otherwise use the source column
                    // definition's data type
                    if ( target.isTargetDataTypeOverridden() )
                    {
                        logger.debug("Target column data type overriden: " + target.getOverriddenDataType());
                        datatype = target.getOverriddenDataType();
                    }
                    else
                    {
                        source = incoming.get(index);
                        scol = source.getColumn();

                        if ( scol == null )
                        {
                            datatype = source.getColumn().getDataType();
                        }
                        else
                        {
                            datatype = scol.getDataType();
                        }
                    }

                    tcol = new DataColumn(datatype, target.getColumnName(), target.getDefaultValue());
                }
                catch (IndexOutOfBoundsException e)
                {
                    logger.warn(e);
                    tcol = new DataColumn(DataTypes.STRING, target.getColumnName(), target.getDefaultValue());
                }
            }

            // If the data type is overriden in the definition, use that data type instead
            /*if ( isDataTypeOverriden(tcol.getColumnName()) )
            {
                logger.debug("Target column data type overriden: " + getOverridenDataType(tcol.getColumnName()));
                tcol = new DataColumn(getOverridenDataType(tcol.getColumnName()), tcol.getColumnName());
            }*/

        }
        catch (IndexOutOfBoundsException e)
        {
            throw new TransformException(source, "Unable to extract target column from transform", e);
        }

        return tcol;
    }

    public abstract List<DataCell> transform(List<DataColumn> cols, List<DataCell> incoming) throws ETLException;
    public abstract Type getTransformType();
    public abstract boolean supportsMultipleSources();
    public abstract boolean supportsMultipleTargets();
    public abstract int getExpectedSourceCount();
}
