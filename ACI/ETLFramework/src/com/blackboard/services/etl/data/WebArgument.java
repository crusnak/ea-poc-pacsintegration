/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.data;

import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.jaxb.ArgumentComplexType;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.WebAPITaskComplexType;
import com.blackboard.services.etl.tasks.AbstractTask;
import com.blackboard.services.utils.BeanScript;
import com.blackboard.services.utils.XMLUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author crusnak
 */
public class WebArgument extends Argument
{
    private Document dom = null;
    private String name = null;
    
    public WebArgument(AbstractTask t)
    {
        super(t);
    }
    
    public WebArgument(Object arg, AbstractTask t)
        throws ETLException
    {
        super(t);
        //logger.info("argument object=" + arg + "(" + arg.getClass().getName() + ")"); 
        if ( arg instanceof WebAPITaskComplexType.Http.Request.Params.Param )
        {
            //logger.debug("HTMLParam");
            WebAPITaskComplexType.Http.Request.Params.Param p = (WebAPITaskComplexType.Http.Request.Params.Param)arg;
            name = p.getName();
            setArgument(p.getType(), p.getDataType(), p.getValue(), null);
            getColumn().setColumnName(name);
        }
        else if ( arg instanceof WebAPITaskComplexType.Http.Request.Params.XmlParam )
        {
            //logger.debug("XMLParam");
            WebAPITaskComplexType.Http.Request.Params.XmlParam p = (WebAPITaskComplexType.Http.Request.Params.XmlParam)arg;
            name = p.getName();
            setArgumentType(p.getType());
            switch (type)
            {
                case column:
                    throw new UnsupportedOperationException("Web arguments do not curently support hierarchical column types");
                case constant:
                    try
                    {
                        targetDataType = DataTypes.XML;
                        setColumn(new DataColumn(targetDataType, name));
                        //logger.debug(p.getAny() + "(" + p.getAny().getClass().getName() + ")");
                        Element elem = (Element)p.getAny();
                        dom = XMLUtils.parseXML(elem); 
                        String xml = XMLUtils.toString(dom, false); 
                        //logger.debug("XML argument=" + xml); 

                        BeanScript bs = new BeanScript(xml);
                        if ( bs.getAccessorText() != null )
                        {
                            if ( !AbstractTask.CONSTANT_DATA_COUNT.equals(bs.getAccessorText()) &&
                                 !AbstractTask.CONSTANT_DATA_ROW.equals(bs.getAccessorText()) &&
                                 !bs.getAccessorText().contains(AbstractTask.CONSTANT_DATA_COUNT) )
                            {
                                DataCell gvar = task.getETLJobWrapper().retrieveGlobalVariable(bs.getAccessorText());
                                if ( gvar == null )
                                {
                                    throw new ETLException("Argument reference to undefined global variable: " + bs.getAccessorText());
                                }
                            }

                            setColumn(new DataColumn(DataTypes.XML, bs.getAccessorText()));
                            targetDataType = DataTypes.XML;

                        }
                        //System.out.println("Setting argument value to: '" + (arg.getValue()!=null?arg.getValue() + "'(" + arg.getValue().getClass() + ")":"NULL"));
                        setValue(xml);
                    }
                    catch (Exception e)
                    {
                        throw new ETLException("Failed to create xml parameter", e);
                    }
                    break;
            }
        }
        /*
        name = arg.getName();
        setArgumentType(arg.getType());
        switch (type)
        {
            case column:
                targetDataType = arg.getDataType();
                setColumn(new DataColumn(DataTypes.STRING, null));
                if ( targetDataType == DataTypes.XML || targetDataType == DataTypes.JSON )
                {
                    throw new UnsupportedOperationException("Web arguments do not curently support hierarchical column types");
                }
                else
                {
                    setValue(String.valueOf(arg.getAny()));
                }
                break;
            case output:
                throw new UnsupportedOperationException("Web arguments do not support output types");
            case constant:
                targetDataType = arg.getDataType();
                setColumn(new DataColumn(arg.getDataType(), null));

                BeanScript bs = new BeanScript(String.valueOf(arg.getAny()));
                if ( bs.getAccessorText() != null )
                {
                    if ( !AbstractTask.CONSTANT_DATA_COUNT.equals(bs.getAccessorText()) &&
                         !AbstractTask.CONSTANT_DATA_ROW.equals(bs.getAccessorText()) &&
                         !bs.getAccessorText().contains(AbstractTask.CONSTANT_DATA_COUNT) )
                    {
                        DataCell gvar = task.getETLJobWrapper().retrieveGlobalVariable(bs.getAccessorText());
                        if ( gvar == null )
                        {
                            throw new ETLException("Argument reference to undefined global variable: " + bs.getAccessorText());
                        }
                    }

                    setColumn(new DataColumn(DataTypes.STRING, bs.getAccessorText()));
                    targetDataType = arg.getDataType();
                    
                }
                //System.out.println("Setting argument value to: '" + (arg.getValue()!=null?arg.getValue() + "'(" + arg.getValue().getClass() + ")":"NULL"));
                setValue(String.valueOf(arg.getAny()));
                break;
        }
         */ 
    }
    

    public void setArgument(Argument.Type t, DataTypes dt, String val)
        throws ETLException
    {
        super.setArgument(String.valueOf(t), dt, val, null);
    }
    
    public String getArgumentName()
    {
        return name;
    }
    
    public void setArgumentName(String n)
    {
        name = n;
    }
    
    public Document getDOM()
    {
        return dom;
    }

    public String toString()
    {
        return name + "=" + getValue() + (type==Type.constant?"[" + targetDataType + "]":"");
    }
}
