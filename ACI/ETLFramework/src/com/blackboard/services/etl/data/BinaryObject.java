/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.data;

import com.blackboard.services.utils.RadixConversion;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.CRC32;
import org.apache.log4j.Logger;

/**
 *
 * @author crusnak
 */
public class BinaryObject implements Serializable
{
    public static final String STRING_FILE =            "File";
    public static final String STRING_LENGTH =          "DataLength";
    public static final String STRING_CHECKSUM =        "Checksum";
    public static final String STRING_TEMP =            "Temporary";
    
    private static final long serialVersionUID = 88375209016L;
    private static Pattern stringFormat = Pattern.compile("^\\s*\\{" + STRING_FILE + "\\=(.+\\.tmp)\\,\\s*" + STRING_LENGTH + "\\=([0-9]+)\\,\\s*" + STRING_CHECKSUM + "\\=([0-9]+).*$");
    
    private File binaryFile;
    private int byteLength = -1;
    private long checksum = 0;
    private boolean temp = false;
    private transient Logger logger = null;
    
    public BinaryObject() 
    {
        logger = Logger.getLogger(this.getClass());
    }
    
    public BinaryObject(byte[] data)
        throws IOException
    {
        this();
        setData(data);
    }
    
    public BinaryObject(File f)
        throws IOException
    {
        this();
        binaryFile = f;
        byte[] data = getData();
        data = new byte[0];
    }
    
    public BinaryObject(File f, int len, long cs)
        throws IOException
    {
        this();
        binaryFile = f;
        byteLength = len;
        checksum = cs;
        
        byte[] data = getData();
        data = new byte[0];
    }
    
    public BinaryObject(String formatted)
        throws IOException
    {
        this();
        Matcher m = stringFormat.matcher(formatted);
        
        if ( m.matches() )
        {
            try
            {
                binaryFile = new File(m.group(1));
                byteLength = Integer.parseInt(m.group(2));
                checksum = Long.parseLong(m.group(3));
            }
            catch (NumberFormatException e)
            {
                throw new IOException("Failed to parse numeric values from: " + formatted, e);
            }
        }
        else
        {
            binaryFile = new File(formatted);
            if ( !binaryFile.exists() )
            {
                throw new IOException("File does not exist: " + formatted);
            }
        }
        
        byte[] data = getData();
        data = new byte[0];
    }
    
    public byte[] getData()
        throws IOException
    {
        byte[] data = new byte[0];
        FileInputStream fis = null;
        try
        {
            if ( binaryFile != null && binaryFile.exists() )
            {
                if ( byteLength >= 0 )
                {
                    data = new byte[byteLength];
                    fis = new FileInputStream(binaryFile);
                    fis.read(data);
                }
                else
                {
                    fis = new FileInputStream(binaryFile);
                    while ( fis.available() > 0 )
                    {
                        byte[] target = new byte[data.length + fis.available()];
                        byte[] temp = new byte[fis.available()];
                        fis.read(temp);
                        System.arraycopy(data, 0, target, 0, data.length);
                        System.arraycopy(temp, 0, target, data.length, temp.length);
                        data = target;
                        target = new byte[0];
                        temp = new byte[0];
                    }

                    fis.close();
                    byteLength = data.length;
                }

                if ( checksum != 0 )
                {
                    CRC32 readChecksum = new CRC32();
                    readChecksum.update(data);
                    if ( readChecksum.getValue() != checksum )
                    {
                        throw new IOException("Checksum of " + binaryFile + " doesn't match expected value: " + checksum);
                    }
                }
                else
                {
                    CRC32 crc = new CRC32();
                    crc.update(data);
                    checksum = crc.getValue();
                }
            }
        }
        finally
        {
            if ( fis != null )
            {
                fis.close();
            }
        }
            
        return data;
    }
    
    public void setData(byte[] data)
        throws IOException
    {
        // If this object is not referring to an existing file, we must create a temp
        // file and delete it on exit since this data is transient
        if ( binaryFile == null )
        {
            UUID guid = UUID.randomUUID();
            binaryFile = File.createTempFile(guid.toString(), null);
            binaryFile.deleteOnExit();
            temp = true;
        }
        
        // If the data is null, create an empty file
        if ( data == null )
        {
            data = new byte[0];
        }
        
        FileOutputStream fos = null;
        try
        {
            fos = new FileOutputStream(binaryFile);
            fos.write(data);
            fos.flush();
        }
        finally
        {
            if ( fos != null )
            {
                fos.close();
            }
        }
        
        CRC32 crc = new CRC32();
        crc.update(data);
        checksum = crc.getValue();
        byteLength = data.length;
        data = new byte[0];
    }
    
    public void setData(String hex)
        throws IOException
    {
        setData(RadixConversion.convertHexStringToBytes(hex));
    }
    
    public int getBinaryDataLength()
    {
        return byteLength;
    }
    
    public void setBinaryDataLength(int len)
    {
        byteLength = len;
    }
    
    public long getChecksum()
    {
        return checksum;
    }
    
    public void setChecksum(long cs)
    {
        checksum = cs;
    }

    public File getTempFile()
    {
        return binaryFile;
    }

    public void setTempFile(File file)
    {
        binaryFile = file;
    }
    
    public boolean isTempFile()
    {
        return temp;
    }
    
    public String toString()
    {
        StringBuilder out = new StringBuilder();
        out.append("{");
        out.append(STRING_FILE + "=" + binaryFile);
        out.append(", " + STRING_LENGTH + "=" + byteLength);
        out.append(", " + STRING_CHECKSUM + "=" + checksum);
        out.append(", " + STRING_TEMP + "=" + temp);
        out.append("}");
        return out.toString();
    }

    @Override
    public boolean equals(Object obj)
    {
        boolean equal = false;
        if ( obj instanceof BinaryObject )
        {
            BinaryObject bo = (BinaryObject)obj;
            equal = byteLength == bo.byteLength && checksum == bo.checksum;
        }
        
        return equal;
    }
    
    public static void main(String[] args)
    {
        try
        {
            String s = "this is a test";
            BinaryObject bo = new BinaryObject(s.getBytes());
            System.out.println(bo);
            System.out.println("Retrieved=" + new String(bo.getData()));
            
            BinaryObject bo2 = new BinaryObject(bo.getTempFile(), bo.getBinaryDataLength(), bo.getChecksum());
            System.out.println(bo2);
            System.out.println("Retrieved2=" + new String(bo2.getData()));
            
            BinaryObject bo3 = new BinaryObject(String.valueOf(bo));
            System.out.println(bo3);
            System.out.println("Retrieved3=" + new String(bo3.getData()));
            
            BinaryObject bo4 = new BinaryObject(bo.getTempFile().getAbsolutePath());
            System.out.println(bo4);
            System.out.println("Retrieved4=" + new String(bo4.getData()));
            System.out.println(bo4);
            
            //bo4.setData(null);
            //System.out.println(bo4);
            //System.out.println("Retrieved4=" + new String(bo4.getData()));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
