/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.data;

import com.blackboard.services.utils.BbJSONObject;
import com.blackboard.services.utils.XMLUtils;
import java.io.Serializable;
import java.nio.file.InvalidPathException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.ParserConfigurationException;
import org.json.simple.JSONObject;
import org.w3c.dom.Document;

/**
 *
 * @author crusnak
 */
public class BbJSONContainer extends BbJSONObject implements Serializable
{
    public static final char PATH_SEPARATOR =          '/';
    private static final long serialVersionUID = 3395972364L;
    
    public BbJSONContainer() 
    {
        super();
    }
    
    public BbJSONContainer(JSONObject json)
    {
        super((json!=null?json.toJSONString():"{}"));
    }
    
    public BbJSONContainer(String json)
        throws IllegalArgumentException
    {
        super(json);
    }
    
    public BbJSONContainer(String json, String root)
        throws IllegalArgumentException
    {
        super(json, root);
    }
    
    public void setFieldValue(String nodepath, Object value)
       throws InvalidPathException
    {
        if ( nodepath == null || nodepath.trim().length() == 0 || nodepath.trim().charAt(0) != PATH_SEPARATOR )
        {
            throw new InvalidPathException(nodepath, "Invalid path. Paths must start with " + PATH_SEPARATOR);
        }

        Object source = getParentFieldValue(nodepath);
        String name = nodepath.substring(nodepath.lastIndexOf(PATH_SEPARATOR) + 1);
        if ( source instanceof BbJSONObject )
        {
            BbJSONObject json = (BbJSONObject)source;
            json.put(name, value);
            //System.out.println("Set " + name + " on " + nodepath + ": " + json);
        }
        else
        {
            throw new IllegalArgumentException("Unsupported parent source json object found via nodepath: " + nodepath + ": " + source.getClass().getName());
        }
    }
    
    public Object getFieldValue(String nodepath)
        throws InvalidPathException
    {
        if ( nodepath == null || nodepath.trim().length() == 0 || nodepath.trim().charAt(0) != PATH_SEPARATOR )
        {
            throw new InvalidPathException(nodepath, "Invalid path. Paths must start with " + PATH_SEPARATOR);
        }

        return getFieldValue(this, nodepath.trim().substring(1));
    }
    
    private Object getParentFieldValue(String nodepath)
        throws InvalidPathException
    {
        //System.out.println("Processing: '" + nodepath + "'");
        
        if ( nodepath == null || nodepath.trim().length() == 0 || nodepath.trim().charAt(0) != PATH_SEPARATOR )
        {
            throw new InvalidPathException(nodepath, "Invalid path. Paths must start with " + PATH_SEPARATOR);
        }

        // Remove everything after last path separator
        if ( nodepath.lastIndexOf(PATH_SEPARATOR) > 0 )
        {
            nodepath = nodepath.substring(0, nodepath.lastIndexOf(PATH_SEPARATOR));
        }
        else if ( nodepath.lastIndexOf(PATH_SEPARATOR) < 0 )
        {
            throw new InvalidPathException(nodepath, "Cannot get parent field as path does not include a path separator after root: " + PATH_SEPARATOR);
        }
        else
        {
            nodepath = String.valueOf(PATH_SEPARATOR);
        }
        
        return getFieldValue(nodepath);
    }
    
    private Object getFieldValue(Object val, String nodepath)
        throws InvalidPathException
    {
        //System.out.println("Processing: '" + nodepath + "'");
        //System.out.println("Source: " + val);
        
        if ( nodepath != null && nodepath.trim().length() > 0 )
        {
            nodepath = nodepath.trim();
            String[] p = nodepath.split(String.valueOf(PATH_SEPARATOR));
            if ( p.length > 0 )
            {
                String path = p[0];
                //System.out.println("Path: " + path);
                
                //System.out.println(val + " [" + val.getClass() + "]");
                if ( val instanceof BbJSONObject )
                {
                    BbJSONObject json = (BbJSONObject)val;
                    if ( json.containsKey(path) )
                    {
                        val = json.get(path);
                    }
                    else
                    {
                        throw new InvalidPathException(json.toJSONString(), "Cannot get json node from source with path: " + path);
                    }
                }
                else if ( val instanceof List )
                {
                    List list = (List)val;
                    if ( list.size() == 1 )
                    {
                        BbJSONObject json = new BbJSONObject(String.valueOf(list.get(0)));
                        if ( json.containsKey(path) )
                        {
                            val = json.get(path);
                        }
                        else
                        {
                            throw new InvalidPathException(json.toJSONString(), "Cannot get json node from source with path: " + path);
                        }
                    }
                    else if ( list.size() == 0 )
                    {
                        throw new InvalidPathException(null, "JSON node is an array with no data: " + path);
                    }
                    else 
                    {
                        throw new UnsupportedOperationException("JSON pathing for multiple JSON objects within a JSON array not currently supported");
                    }
                }
                else if ( val instanceof BbJSONArray )
                {
                    if ( ((BbJSONArray)val).getLength() == 1 )
                    {
                        BbJSONObject json = new BbJSONObject(((BbJSONArray)val).get(0).toJSONString());
                        if ( json.containsKey(path) )
                        {
                            val = json.get(path);
                        }
                        else
                        {
                            throw new InvalidPathException(json.toJSONString(), "Cannot get json node from source with path: " + path);
                        }
                    }
                    else if ( ((BbJSONArray)val).getLength() == 0 )
                    {
                        throw new InvalidPathException(null, "JSON node is an array with no data: " + path);
                    }
                    else 
                    {
                        throw new UnsupportedOperationException("JSON pathing for multiple JSON objects within a JSON array not currently supported");
                    }
                }
                else
                {
                    throw new InvalidPathException(String.valueOf(val), "Cannot get json node from source with path: " + path);
                }
                
                if ( !path.equals(nodepath) )
                {
                    String remain = nodepath.substring(p[0].length() + 1);
                    //System.out.println("Remain: " + remain);
                    val = getFieldValue(val, remain);
                }
            }
        }
            
        return val;
    }
    
    public Document getXMLDocument()
        throws ParserConfigurationException
    {
        return XMLUtils.parseJSON(this);
    }
    
    public Document getXMLDocument(String rootElement)
        throws ParserConfigurationException
    {
        return XMLUtils.parseJSON(this, rootElement);
    }
    
    public String toXMLString()
    {
        String out = "";
        try
        {
            Document xml = getXMLDocument(this.toJSONString());
            out = XMLUtils.toString(xml, false);
        }
        catch (ParserConfigurationException e)
        {
            out = e.getMessage() + ": " + this.toJSONString();
        }
        
        return out;
    }
    
    public static void main(String[] args)
    {
        try
        {
            String jstr = "[{\"CardholderId\":\"33200bfe-8918-406d-9ab3-1c95d64396e4\",\"LastName\":\"Allen\",\"FirstName\":\"Josh\",\"MiddleInitial\":\"y\",\"CompanyId\":null,\"MemberOfAllSites\":true,\"Notes\":null,\"CardholderStatus\":1,\"CardholderActiveDate\":null,\"CardholderExpireDate\":null,\"LastCardholderModRowVersion\":\"0x00000000000036BB\",\"LastModified\":\"2021-08-16T17:34:29.267\",\"Images\":[],\"CardholderAccessLevels\":[{\"CardholderAccessLevelID\":\"2d43b4cf-9c48-4b91-bed4-ec48df249250\",\"CardholderID\":\"33200bfe-8918-406d-9ab3-1c95d64396e4\",\"AccessLevelID\":\"58d7ab01-7535-4aab-bcb9-34025f92c7c3\",\"LastModified\":\"2021-08-16T17:34:29.267\",\"ActivateDate\":null,\"DeactivateDate\":null}],\"CardholderSites\":[],\"CardholderCards\":[{\"CardId\":\"a7d3672a-bef0-47b2-8eba-b5b4312952a9\",\"CardholderID\":\"33200bfe-8918-406d-9ab3-1c95d64396e4\",\"CardStatus\":1,\"LastModified\":\"2021-08-16T17:34:29.267\",\"ActiveDate\":\"1899-12-30T07:00:00\",\"ExpireDate\":\"2173-10-13T07:00:00\"}],\"UserColumns\":{\"UserText1\":\"0000000000000000800017\",\"UserText2\":\"c5e03480-5d9d-464b-9f28-808311bd83fb\",\"UserText3\":null,\"UserText4\":null,\"UserText5\":null,\"UserText6\":null,\"UserText7\":null,\"UserText8\":null,\"UserText9\":null,\"UserText10\":null,\"UserText11\":null,\"UserText12\":null,\"UserText13\":null,\"UserText14\":null,\"UserText15\":null,\"UserText16\":null,\"UserText17\":null,\"UserText18\":null,\"UserText19\":null,\"UserText20\":null,\"Department\":null,\"UserDate1\":null,\"UserDate2\":null,\"UserDate3\":null,\"UserDate4\":null,\"UserDate5\":null,\"UserNumeric1\":null,\"UserNumeric2\":null,\"UserNumeric3\":null,\"UserNumeric4\":null,\"UserNumeric5\":null}}]";
            
            BbJSONContainer json = new BbJSONContainer(jstr);
            System.out.println(json);
            
            //System.out.println("/UserColumns/UserText1=" + json.getFieldValue("/UserColumns/UserText1"));
            //System.out.println("/UserColumns/UserText1 parent=" + json.getParentFieldValue("/UserColumns/UserText1"));
            
            json.setFieldValue("/UserColumns/UserText1", "12345678");
            json.setFieldValue("/FirstName", "Chris");
            System.out.println(json);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
