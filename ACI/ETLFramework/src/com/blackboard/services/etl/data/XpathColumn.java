/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.data;

import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.exception.VariableTypeMismatchException;
import com.blackboard.services.etl.jaxb.DataTypes;

/**
 *
 * @author crusnak
 */
public class XpathColumn extends DataColumn
{
    protected String xpath = null;
    protected String refString = null;
    
    public XpathColumn(DataTypes type, String name, boolean primary)
    {
        super(type, name, primary, null);
    }
    
    public XpathColumn(DataTypes type, String name)
    {
        super(type, name, false);
    }
    
    public XpathColumn(DataTypes type, String name, String def)
    {
        super(type, name, false, def);
    }

    public XpathColumn(DataTypes type, String name, boolean u, String def)
    {
        super(type, name, u, def);
    }
    
    public XpathColumn(DataTypes type, String name, String xpath, String ref)
    {
        super(type, name, false);
        this.xpath = xpath;
        this.refString = ref;
    }

    public XpathColumn(String type, String name)
        throws ETLException
    {
        super(type, name, false);
    }

    public XpathColumn(String type, String name, boolean primary)
        throws ETLException
    {
        super(type, name, primary, null);
    }
    
    public XpathColumn(String type, String name, String def)
        throws ETLException
    {
        super(type, name, false, def);
    }

    public XpathColumn(String type, String name, boolean u, String def)
        throws ETLException
    {
        super(type, name, u, def);
    }

    public String getReferenceString() 
    {
        return refString;
    }

    public void setReferenceString(String refString) 
    {
        this.refString = refString;
    }

    public String getXpath() 
    {
        return xpath;
    }

    public void setXpath(String xpath) 
    {
        this.xpath = xpath;
    }
    
    @Override
    public String toString()
    {
        return xpath + ": " + super.toString();
    }
}
