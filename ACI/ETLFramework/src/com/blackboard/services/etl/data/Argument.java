/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.data;

import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.exception.VariableTypeMismatchException;
import com.blackboard.services.etl.jaxb.ArgumentComplexType;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.tasks.AbstractTask;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.blackboard.services.utils.BeanScript;
import org.apache.log4j.Logger;

/**
 *
 * @author crusnak
 */
public class Argument extends DataCell
{
    public static enum Type { column, constant, output, list };

    protected Logger logger = null;
    protected AbstractTask task = null;
    protected Type type = null;
    protected DataTypes targetDataType = null;
    protected Character delim = null;

    protected Argument(AbstractTask t)
    {
        logger = Logger.getLogger(this.getClass());
        task = t;
    }
    
    public Argument(ArgumentComplexType arg, AbstractTask t)
        throws ETLException
    {
        this(t);
        setArgument(arg.getType(), arg.getDataType(), arg.getValue(), arg.getListDelimiter());
    }

    public Argument(String type, String dt, String arg, String ld)
        throws ETLException
    {
        setArgument(type, DataTypes.valueOf(dt), arg, ld);
    }

    public Argument(Type type, DataTypes dt, String arg, String ld)
        throws ETLException
    {
        setArgument(String.valueOf(type), dt, arg, ld);
    }
    
    public Argument(String type, String dt, String arg)
        throws ETLException
    {
        this(type, dt, arg, null);
    }
    
    public Argument(Type type, DataTypes dt, String arg)
        throws ETLException
    {
        this(type, dt, arg, null);
    }
    
    protected void setArgument(String t, DataTypes dt, String v, String d)
        throws ETLException
    {
        setArgumentType(t);
        switch (type)
        {
            case column:
                targetDataType = dt;
                setColumn(new DataColumn(DataTypes.STRING, null));
                setValue(v);
                break;
            case output:
                setColumn(new DataColumn(DataTypes.STRING, v));
                targetDataType = dt;
                setValue(v);
                break;
            case constant:
                targetDataType = dt;
                setColumn(new DataColumn(dt, null));

                BeanScript bs = new BeanScript(v);
                if ( bs.getAccessorText() != null )
                {
                    if ( !AbstractTask.CONSTANT_DATA_COUNT.equals(bs.getAccessorText()) &&
                         !AbstractTask.CONSTANT_DATA_ROW.equals(bs.getAccessorText()) &&
                         !bs.getAccessorText().contains(AbstractTask.CONSTANT_DATA_COUNT) )
                    {
                        DataCell gvar = task.getETLJobWrapper().retrieveGlobalVariable(bs.getAccessorText());
                        if ( gvar == null )
                        {
                            throw new ETLException("Argument reference to undefined global variable: " + bs.getAccessorText());
                        }
                    }

                    setColumn(new DataColumn(DataTypes.STRING, bs.getAccessorText()));
                    targetDataType = dt;
                    
                }
                //System.out.println("Setting argument value to: '" + (arg.getValue()!=null?arg.getValue() + "'(" + arg.getValue().getClass() + ")":"NULL"));
                setValue(v);
                break;
            case list:
                targetDataType = dt;
                setColumn(new DataColumn(DataTypes.STRING, null));
                setValue(v);
                delim = getDelimiterChar(d);
                break;
        }
    }

    public Type getArgumentType()
    {
        return type;
    }

    @Override
    public void setColumn(DataColumn col)
    {
        super.setColumn(col);
        targetDataType = col.getDataType();
    }

    protected void setArgumentType(Type type)
    {
        this.type = type;
    }
    
    protected void setArgumentType(String type)
    {
        setArgumentType(Type.valueOf(type));
    }

    public DataTypes getTargetDataType()
    {
        return targetDataType;
    }
    
    private char getDelimiterChar(String d)
        throws ETLException
    {
        char delim = 0x40;
        if ( d == null || d.trim().length() == 0 || d.trim().length() > 2 )
        {
            throw new ETLException("Delimiter character must be a single character");
        }

        if ( d.trim().length() == 1 )
        {
            delim = d.charAt(0);
        }
        else
        {
            if ( d.trim().equals("\\t") )
            {
                delim = '\t';
            }
            else
            {
                throw new ETLException("Only escape characters supported as delimiters are: \\t");
            }
        }

        return delim;
    }
    
    public char getDelimiter()
    {
        return delim;
    }

    @Override
    public Object getValue()
    {
        Object out = super.getValue();
        BeanScript bs = new BeanScript(String.valueOf(out));
        if ( bs.getAccessorText() != null )
        {
            try
            {
                Object val = task.getGlobalVariableValue(String.valueOf(out));
                DataCell newcell = new DataCell(new DataColumn(targetDataType, null), val);
                out = newcell.getValue();
            }
            catch (ETLException e)
            {
                logger.warn("Unexpected error extracting global variable for argument assignment", e);
            }
        }
        
        return out;
    }

    public String toString()
    {
        return super.toString() + (type==Type.output?"[OUTPUT->" + targetDataType + "]":"") + (type==Type.constant?"[" + targetDataType + "]":"");
    }
}
