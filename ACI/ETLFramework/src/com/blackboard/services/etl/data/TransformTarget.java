/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.data;

import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.TransformComplexType;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;

/**
 *
 * @author crusnak
 */
public class TransformTarget 
{
    private String columnName = null;
    private DataTypes overrideType = null;
    private String defaultValue = null;
    private String path = null;
    private String refNode = null;

    public TransformTarget(TransformComplexType.TargetColumn arg)
        throws ETLException
    {
        columnName = arg.getValue();
        defaultValue = arg.getDefaultValue();
        overrideType = arg.getOverrideDataType();
        path = arg.getPath();
        refNode = arg.getRef();
    }

    public String getColumnName()
    {
        return columnName;
    }
    
    public String getDefaultValue()
    {
        return defaultValue;
    }
    
    public boolean isDefaultValueDefined()
    {
        return defaultValue != null;
    }
    
    public boolean isTargetDataTypeOverridden()
    {
        return overrideType != null;
    }
    
    public DataTypes getOverriddenDataType()
    {
        return overrideType;
    }
    
    public String getPath()
    {
        return path;
    }
    
    public String getRefNode()
    {
        return refNode;
    }
    
    @Override
    public String toString()
    {
        return "TransformTarget: " + columnName;
    }
}
