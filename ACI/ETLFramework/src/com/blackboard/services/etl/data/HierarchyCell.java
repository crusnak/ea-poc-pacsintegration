/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.data;

import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.exception.VariableTypeMismatchException;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.utils.schema.SchemaDocument;
import com.blackboard.services.utils.schema.SchemaElement;
import com.blackboard.services.utils.schema.SchemaNode;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author crusnak
 */
public class HierarchyCell implements Serializable
{
    public static enum Type { simple, complex, attribute };
    
    private static final long serialVersionUID = 804360203062364655L;
    private static Map<String,DataTypes> dataTypeMap = new HashMap();

    private Type type = Type.simple;
    private DataColumn xpath = null;
    private String xsdDataType = null;
    private HierarchyCell parent = null;
    private SchemaNode node = null;
    private List<HierarchyCell> children = null;
    private DataCell value = null;
    
    static
    {
        dataTypeMap.put(SchemaDocument.TYPE_64BINARY, DataTypes.BINARY);
        dataTypeMap.put(SchemaDocument.TYPE_BOOLEAN, DataTypes.BOOLEAN);
        dataTypeMap.put(SchemaDocument.TYPE_BYTE, DataTypes.INT);
        dataTypeMap.put(SchemaDocument.TYPE_DATE, DataTypes.DATE);
        dataTypeMap.put(SchemaDocument.TYPE_DATETIME, DataTypes.DATE);
        dataTypeMap.put(SchemaDocument.TYPE_DECIMAL, DataTypes.DOUBLE);
        dataTypeMap.put(SchemaDocument.TYPE_DOUBLE, DataTypes.DOUBLE);
        dataTypeMap.put(SchemaDocument.TYPE_FLOAT, DataTypes.FLOAT);
        dataTypeMap.put(SchemaDocument.TYPE_HEX_BINARY, DataTypes.BINARY);
        dataTypeMap.put(SchemaDocument.TYPE_INT, DataTypes.INT);
        dataTypeMap.put(SchemaDocument.TYPE_INTEGER, DataTypes.INT);
        dataTypeMap.put(SchemaDocument.TYPE_LONG, DataTypes.LONG);
        dataTypeMap.put(SchemaDocument.TYPE_SHORT, DataTypes.INT);
        dataTypeMap.put(SchemaDocument.TYPE_STRING, DataTypes.STRING);
        dataTypeMap.put(SchemaDocument.TYPE_TIME, DataTypes.DATE);
    }
    
    public HierarchyCell(String xpath, SchemaNode node)
        throws ETLException
    {
        this(xpath, node, null);
    }
    
    public HierarchyCell(String x, SchemaNode n, HierarchyCell p)
        throws ETLException
    {
        xpath = new DataColumn(lookupDataType(n), x, n.getDefaultValue());
        node = n;
        setParentCell(p);
        
        switch (n.getType())
        {
            case attribute:
                type = Type.attribute;
                break;
            case element:
                SchemaElement e = (SchemaElement)n;
                if ( e.getChildType() == SchemaElement.ChildType.simple )
                {
                    type = Type.simple;
                    value = new DataCell(xpath, null);
                }
                else
                {
                    type = Type.complex;
                    children = new ArrayList();
                }
                break;
        }
    }
    
    public Type getType()
    {
        return type;
    }

    public DataColumn getXpath()
    {
        return xpath;
    }

    public HierarchyCell getParentCell()
    {
        return parent;
    }

    private void setParentCell(HierarchyCell p)
       throws ETLException
    {
        parent = p;
        if ( p != null )
        {
            if ( p.getType() != Type.complex )
            {
                throw new ETLException("Parent HierarchyCell objects must be complex types to support children: " + p);
            }
            else
            {
                p.getChildren().add(this);
                if ( p.getNode().getMaxOccurs() != -1 && p.getNode().getMaxOccurs() < p.getChildren().size() )
                {
                    throw new ETLException("Parent HierarchyCell cannot accept any more child elements: " + p.getNode());
                }
            }
        }
    }
    
    public SchemaNode getNode()
    {
        return node;
    }

    public List<HierarchyCell> getChildren()
    {
        return children;
    }

    public Object getValue()
    {
        Object val = null;
        if ( type != Type.complex )
        {
            val = value.getValue();
        }
        return val;
    }

    public void setValue(Object obj)
        throws VariableTypeMismatchException
    {
        if ( type == Type.complex )
        {
            throw new VariableTypeMismatchException(null, "Not allowed to set value for HierarchyCell of complex type: " + obj);
        }
        
        value.setValue(obj);
    }
    
    private DataTypes lookupDataType(SchemaNode n)
        throws ETLException
    {
        DataTypes t = DataTypes.STRING;
        
        if ( n == null )
        {
            throw new ETLException("Associated schema node cannot be null during construction of HierarchyCell objects");
        }
        
        if ( n.getDataType() != null && n.getDataType().trim().length() > 0 )
        {
            if ( n.getDataType().contains(":") )
            {
                xsdDataType = n.getDataType().substring(n.getDataType().indexOf(":")+1);
            }
            else
            {
                xsdDataType = n.getDataType();
            }
            
            if ( dataTypeMap.containsKey(xsdDataType) )
            {
                t = dataTypeMap.get(xsdDataType);
            }
            else 
            {
                throw new UnsupportedOperationException("Unsupported xsd data type: " + n.getDataType());
            }
        }
        
        return t;
    }

    public String toString()
    {
        String out = xpath.getColumnName() + "=";
        if ( type == Type.complex )
        {
            out += children;
        }
        else
        {
            out += value.toString();
        }
        
        return out;
    }
}
