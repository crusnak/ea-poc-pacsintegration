/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.data;

import com.blackboard.services.etl.exception.VariableTypeMismatchException;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.transforms.DateTransform;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.blackboard.services.utils.BbJSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringWriter;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
//import oracle.sql.BLOB;
import oracle.sql.TIMESTAMP;
import net.sourceforge.jtds.jdbc.BlobImpl;
import oracle.sql.TIMESTAMPTZ;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

/**
 *
 * @author crusnak
 */
public class DataCell implements Serializable, Comparable<Object>
{
    private static final long serialVersionUID = 67638423656L;
    private static String DefaultEncoding = null;
    
    
    protected transient Logger logger = null;
    protected DataColumn column = null;
    protected Object value = null;
    //protected Object convertedValue = null;
    protected String encoding = null;

    public DataCell()
    {
        logger = Logger.getLogger(this.getClass());
    }

    public DataCell(DataColumn col, Object val)
        throws VariableTypeMismatchException
    {
        this();
        setColumn(col);
        setValue(val);
        setEncoding(DefaultEncoding);
    }

    public DataCell(DataColumn col, Object val, String encoding)
        throws VariableTypeMismatchException
    {
        this();
        setColumn(col);
        setValue(val);
        setEncoding(encoding);
    }
    
    public static void setDefaultEncoding(String enc)
    {
        DefaultEncoding = enc;
    }

    public DataColumn getColumn()
    {
        return column;
    }

    public void setColumn(DataColumn col)
    {
        this.column = col;
    }

    public String getEncoding()
    {
        return encoding;
    }

    public void setEncoding(String enc)
    {
        this.encoding = enc;
    }
    
    public Class getCellClass()
    {
        Class clazz = Object.class;
        if ( value != null )
        {
            clazz = getConvertedValue().getClass();
        }
        else
        {
            clazz = column.getColumnClass();
        }

        return clazz;
    }

    public Object getValue()
    {
        //Logger.getLogger(this.getClass()).trace("DataCell " + getColumn().getColumnName() + "=" + this);
        return getConvertedValue();
    }

    public Object getConvertedValue()
    {
        Object val = value;
        if ( DataTypes.BOOLEAN == column.getDataType() )
        {
            val = Boolean.valueOf(String.valueOf(value));
        }
        else if ( DataTypes.LONG == column.getDataType() )
        {
            try
            {
                val = Long.parseLong(String.valueOf(value));
            }
            catch (NumberFormatException e) {}
        }
        else if ( DataTypes.INT == column.getDataType() )
        {
            try
            {
                val = Integer.parseInt(String.valueOf(value));
            }
            catch (NumberFormatException e) {}
        }
        else if ( DataTypes.FLOAT == column.getDataType() )
        {
            try
            {
                val = Float.parseFloat(String.valueOf(value));
            }
            catch (NumberFormatException e) {}
        }
        else if ( DataTypes.DOUBLE == column.getDataType() )
        {
            try
            {
                val = Double.parseDouble(String.valueOf(value));
            }
            catch (NumberFormatException e) {}
        }
        else if ( DataTypes.BINARY == column.getDataType() )
        {
            // Do not want to extract the actual binary data and store in the data cell as that
            // can increase the memory footprint of the object greatly
            val = value;
        }
        
        return val;
    }

    public Object getCastValue()
    {
        Object value = null;
        Class cellclass = getCellClass();
        if ( cellclass == DataColumn.CLASS_TYPE_LONG )
        {
            value = Long.parseLong(String.valueOf(getConvertedValue()));
        }
        else if ( cellclass == DataColumn.CLASS_TYPE_BOOLEAN )
        {
            value = Boolean.valueOf(String.valueOf(getConvertedValue()));
        }
        if ( cellclass == DataColumn.CLASS_TYPE_DOUBLE )
        {
            value = Double.parseDouble(String.valueOf(getConvertedValue()));
        }
        else
        {
            value = getConvertedValue();
        }

        return value;
    }

    public void setValue(Object obj)
        throws VariableTypeMismatchException
    {
        //System.out.println("Setting " + column + " to: " + obj + (obj != null?"(" + obj.getClass().getName() + ")":""));
        if ( obj != null && column.getColumnClass().isInstance(obj.getClass()) )
        {
            logger.trace("Object already instanceof " + column.getColumnClass().getName() + ", no conversion necessary");
            value = obj;
        }
        else if ( (obj == null || String.valueOf(obj).length() == 0) && column.isDefaultValueDefined() )
        {
            setValue(column.getDefaultValue());
        }
        else if ( DataTypes.BOOLEAN == column.getDataType() )
        {
            String val = String.valueOf(obj);
            boolean bool = Boolean.valueOf(val) || 
                           "Y".equalsIgnoreCase(val) ||
                           "YES".equalsIgnoreCase(val) ||
                           "T".equalsIgnoreCase(val) ||
                           "1".equals(val);
            setPrimitiveValue(bool);
        }
        else if ( DataTypes.LONG == column.getDataType() )
        {
            try
            {
                if ( obj != null && String.valueOf(obj).trim().length() > 0 )
                {
                    String val = String.valueOf(obj);
                    long v = Long.parseLong(val);
                    setPrimitiveValue(v);
                }
                else
                {
                    value = null;
                }
            }
            catch (NumberFormatException e)
            {
                throw new VariableTypeMismatchException(this, "Cannot set long value: " + obj, e);
            }
        }
        else if ( DataTypes.INT == column.getDataType() )
        {
            try
            {
                String val = String.valueOf(obj);
                if ( obj != null && val.trim().length() > 0 )
                {
                    if ( val.contains(".") )
                    {
                        String trunc = val.substring(0, val.indexOf("."));
                        int v = Integer.parseInt(trunc);
                        setPrimitiveValue(v);
                    }
                    else
                    {
                        int v = Integer.parseInt(val);
                        setPrimitiveValue(v);
                    }
                }
                else
                {
                    value = null;
                }
            }
            catch (NumberFormatException e)
            {
                throw new VariableTypeMismatchException(this, "Cannot set int value: " + obj, e);
            }
        }
        else if ( DataTypes.FLOAT == column.getDataType() )
        {
            try
            {
                if ( obj != null && String.valueOf(obj).trim().length() > 0 )
                {
                    String val = String.valueOf(obj);
                    Float v = Float.parseFloat(val);
                    setPrimitiveValue(v);
                }
                else
                {
                    value = null;
                }
            }
            catch (NumberFormatException e)
            {
                throw new VariableTypeMismatchException(this, "Cannot set float value: " + obj, e);
            }
        }
        else if ( DataTypes.DOUBLE == column.getDataType() )
        {
            try
            {
                if ( obj != null && String.valueOf(obj).trim().length() > 0 )
                {
                    String val = String.valueOf(obj);
                    double v = Double.parseDouble(val);
                    setPrimitiveValue(v);
                }
                else
                {
                    value = null;
                }
            }
            catch (NumberFormatException e)
            {
                throw new VariableTypeMismatchException(this, "Cannot set double value: " + obj, e);
            }
        }
        else if ( DataTypes.DATE == column.getDataType() )
        {
            logger.trace("Date type instance: " + (obj!=null?obj.getClass().getName():"NULL"));
            if ( obj instanceof Timestamp )
            {
                setValue(new Date(((Timestamp)obj).getTime()));
            }
            else if ( obj instanceof TIMESTAMP )
            {
                try
                {
                    setValue(((TIMESTAMP)obj).timestampValue());
                }
                catch (SQLException e)
                {
                    throw new VariableTypeMismatchException(this, "Error extracting date from oracle TIMESTAMP", e);
                }
            }
            else if ( obj instanceof Date )
            {
                if ( obj instanceof java.sql.Date )
                {
                    value = new Date(((java.sql.Date)obj).getTime());
                }
                else
                {
                    value = obj;
                }
            }
            else if ( obj instanceof Integer || obj instanceof Long )
            {
                try
                {
                    value = new Date(Long.parseLong(String.valueOf(obj)));
                }
                catch (NumberFormatException e)
                {
                    value = null;
                }
            }
            else if ( obj instanceof String )
            {
                try
                {
                    if ( DateTransform.VAL_EPOCHDATE.equals(String.valueOf(obj)) )
                    {
                        value = new Date(0);
                    }
                    else if ( DateTransform.VAL_BBTSZERODATE.equals(String.valueOf(obj)) )
                    {
                        try
                        {
                            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                            value = sdf.parse("12/30/1899");
                        }
                        catch (ParseException e) { logger.warn(e); }
                    }
                    else if ( DateTransform.VAL_SYSDATETIME.equals(String.valueOf(obj)) )
                    {
                        value = new Date();
                    }
                    else if ( DateTransform.VAL_SYSDATE.equals(String.valueOf(obj)) )
                    {
                        Calendar cal = Calendar.getInstance();
                        cal.set(Calendar.HOUR_OF_DAY, 0);
                        cal.set(Calendar.MINUTE, 0);
                        cal.set(Calendar.SECOND, 0);
                        cal.set(Calendar.MILLISECOND, 0);
                        value = new Date(cal.getTimeInMillis());
                    }
                    else
                    {
                        // Replacing assigning date as time in milliseconds with SimpleDateFormat
                        //value = new Date(Long.parseLong(String.valueOf(obj)));
                        value = ETLJobWrapper.dataCellFormat.parse(String.valueOf(obj));
                    }
                }
                catch (ParseException e)
                {
                    value = null;
                }
            }
            else
            {
                value = null;
            }
        }
        else if ( DataTypes.BINARY == column.getDataType() )
        {
            logger.debug("Obj class=" + String.valueOf((obj!=null)?obj.getClass():null));
            try
            {
                if ( obj != null && obj instanceof BinaryObject )
                {
                    value = (BinaryObject)obj;
                }
                else if ( obj != null && obj instanceof Blob )
                {
                    BinaryObject bo = new BinaryObject();
                    Blob blob = (Blob)obj;
                    //BLOB blob = (BLOB)obj;
                    if ( blob.length() > Integer.MAX_VALUE )
                    {
                        throw new IOException("Size of BLOB in database is larger than can be supported (2Gb): " + blob.length());
                    }
                    
                    int len = (int)blob.length();
                    byte[] data = blob.getBytes(1, len);
                    bo.setData(data);
                    value = bo;
                }
                else if ( obj != null && obj instanceof BlobImpl )
                {
                    Blob blob = (Blob)obj;
                    String bstring = IOUtils.toString(blob.getBinaryStream(), encoding);
                    if ( bstring != null )
                    {
                        BinaryObject bo = new BinaryObject(bstring.getBytes(encoding));
                        value = bo;
                    }
                    else
                    {
                        value = null;
                    }
                }
                else if ( obj != null && obj instanceof byte[] )
                {
                    BinaryObject bo = new BinaryObject();
                    if ( ((byte[])obj).length > Integer.MAX_VALUE )
                    {
                        throw new IOException("Size of BLOB in database is larger than can be supported (2Gb): " + ((byte[])obj).length);
                    }

                    bo.setData((byte[])obj);
                    value = bo;                    
                }
                else if ( obj != null && String.valueOf(obj).trim().length() > 0 && String.valueOf(obj).trim().charAt(0) == '{' )
                {
                    value = new BinaryObject(String.valueOf(obj).trim());
                }
                else if ( obj != null && String.valueOf(obj).trim().length() > 0 )
                {
                    logger.trace("Assigning binary literal value as hex: " + obj);
                    BinaryObject bo = new BinaryObject();
                    bo.setData(String.valueOf(obj).trim());
                    value = bo;
                }
                else
                {
                    value = null;
                }
            }
            catch (IOException e)
            {
                throw new VariableTypeMismatchException(this, "Failed to parse as BinaryObject: " + obj, e);
            }
            catch (SQLException e)
            {
                throw new VariableTypeMismatchException(this, "Error extracting binary data  from oracle BLOB", e);
            }
        }
        else if ( DataTypes.JSON == column.getDataType() )
        {
            try
            {
                if ( obj instanceof BbJSONContainer )
                {
                    BbJSONContainer json = (BbJSONContainer)obj;
                    value = new JacksonJsonContainer(json.toJSONString());
                }
                else if ( obj instanceof BbJSONObject )
                {
                    BbJSONObject json = (BbJSONObject)obj;
                    value = new JacksonJsonContainer(json.toJSONString());
                }
                else if ( obj instanceof JSONObject )
                {
                    JSONObject json = (JSONObject)obj;
                    value = new JacksonJsonContainer(json.toJSONString());
                }
                else if ( obj instanceof JacksonJsonContainer )
                {
                    JacksonJsonContainer jjc = (JacksonJsonContainer)obj;
                    value = new JacksonJsonContainer(jjc);
                }
                else if ( obj instanceof JsonNode )
                {
                    JsonNode node = (JsonNode)obj;
                    value = new JacksonJsonContainer(node);
                }
                else if ( obj != null )
                {
                    JacksonJsonContainer json = new JacksonJsonContainer(String.valueOf(obj));
                    value = json;
                }
                else
                {
                    value = null;
                }
            }
            catch (JsonProcessingException e)
            {
                throw new VariableTypeMismatchException(this, "Failed to parse JSON string", e);
            }
            catch (IOException e)
            {
                throw new VariableTypeMismatchException(this, "Failed to parse JSON string", e);
            }
        }
        else if ( DataTypes.STRING == column.getDataType() )
        {
            if ( obj instanceof Clob )
            {
                Clob clob = null;
                try
                {
                    clob = (Clob)obj;
                    if ( clob.length() > 0 )
                    {
                        StringBuilder out = new StringBuilder((int)clob.length());
                        Reader r = clob.getCharacterStream();
                        for (int c=0; c >= 0; c=r.read() )
                        {
                            if ( c >- 0 )
                            {
                                out.append((char)c);
                            }
                        }
                        r.close();
                        value = out.toString();
                    }
                    else
                    {
                        value = null;
                    }
                }
                catch (Exception e)
                {
                    logger.error("Failed to read CLOB from ResultSet", e);
                    StringWriter sw = new StringWriter();
                    PrintWriter pw = new PrintWriter(sw);
                    e.printStackTrace(pw);
                    obj = sw.toString();
                }
                finally
                {
                    if ( clob != null )
                    {
                        try
                        {
                            clob.free();
                        }
                        catch (SQLException e)
                        {
                            logger.warn("Unable to free CLOB", e);
                        }
                    }
                }
            }
            else if ( obj != null && String.valueOf(obj).length() > 0 )
            {
                value = String.valueOf(obj);
            }
            else
            {
                value = null;
            }
        }
        // Anything else is an object
        else
        {
            value = obj;
        }

        //convertedValue = getConvertedValue();

        //System.out.println("Value: " + this);
    }

    public void setValue(boolean b)
        throws VariableTypeMismatchException
    {
        setValue((Boolean)b);
    }

    protected void setPrimitiveValue(boolean b)
        throws VariableTypeMismatchException
    {
        if ( DataTypes.BOOLEAN == column.getDataType() )
        {
            value = String.valueOf(b);
        }
        else
        {
            throw new VariableTypeMismatchException(this, "Cannot set boolean value " + b);
        }
    }

    public void setValue(long l)
        throws VariableTypeMismatchException
    {
        setValue((Long)l);
    }

    protected void setPrimitiveValue(long l)
        throws VariableTypeMismatchException
    {
        if ( DataTypes.LONG == column.getDataType() )
        {
            value = String.valueOf(l);
        }
        else
        {
            throw new VariableTypeMismatchException(this, "Cannot set long value: " + l);
        }
    }

    public void setValue(int i)
        throws VariableTypeMismatchException
    {
        setValue((Integer)i);
    }

    protected void setPrimitiveValue(int i)
        throws VariableTypeMismatchException
    {
        if ( DataTypes.INT == column.getDataType() )
        {
            value = String.valueOf(i);
        }
        else
        {
            throw new VariableTypeMismatchException(this, "Cannot set int value: " + i);
        }
    }

    public void setValue(double d)
        throws VariableTypeMismatchException
    {
        setValue((Double)d);
    }

    protected void setPrimitiveValue(double d)
        throws VariableTypeMismatchException
    {
        if ( DataTypes.DOUBLE == column.getDataType() )
        {
            value = String.valueOf(d);
        }
        else
        {
            throw new VariableTypeMismatchException(this, "Cannot set double value: " + d);
        }
    }

    public void setValue(float f)
        throws VariableTypeMismatchException
    {
        setValue((Float)f);
    }

    protected void setPrimitiveValue(float f)
        throws VariableTypeMismatchException
    {
        if ( DataTypes.FLOAT == column.getDataType() )
        {
            value = String.valueOf(f);
        }
        else
        {
            throw new VariableTypeMismatchException(this, "Cannot set float value: " + f);
        }
    }
    
    public void setValue(byte[] b)
       throws VariableTypeMismatchException, IOException
    {
        setValue(b);
    }
    
    protected void setPrimitiveValue(byte[] b)
        throws VariableTypeMismatchException, IOException
    {
        BinaryObject bo = new BinaryObject(b);
        if ( DataTypes.FLOAT == column.getDataType() || 
             DataTypes.BINARY == column.getDataType() )
        {
            value = String.valueOf(bo);
        }
        else
        {
            throw new VariableTypeMismatchException(this, "Cannot set binary value: " + bo);
        }
    }

    @Override
    public int compareTo(Object o)
    {
        int compare = 0;
        if ( o instanceof DataCell )
        {
            DataCell that = (DataCell)o;
            if ( this != null )
            {
                Object thisval = getValue();
                Object thatval = null;
                if ( that != null )
                {
                    thatval = that.getValue();
                }
                
                if ( thisval != null )
                {
                    
                }
                else if ( thatval != null )
                {
                    //compare = String)
                }
            }
            else if ( that != null )
            {
                Object thisval = null;
                Object thatval = that.getValue();
            }
        }
        
        return compare;
    }

    /**
     * Overridden to check for equality between DataCell objects.  
     * NOTE: Only the value of the data cell is compared.
     * @param obj
     * @return 
     */
    @Override
    public boolean equals(Object obj)
    {
        boolean equal = false;
        
        if ( obj instanceof DataCell )
        {
            equal = true;
            DataCell that = (DataCell)obj;
            if ( this.getValue() != null )
            {
                equal &= this.getValue().equals(that.getValue());
            }
            else
            {
                equal &= that.getValue() == null;
            }
        }
        
        return equal;
    }

    @Override
    public int hashCode()
    {
        return getValue().hashCode();
    }
    
    @Override
    public String toString()
    {
        String out = getConvertedValue() + "(" + getCellClass().getName() + ")";
        if ( value == null )
        {
            out = "NULL";
        }
        return out;
    }

}
