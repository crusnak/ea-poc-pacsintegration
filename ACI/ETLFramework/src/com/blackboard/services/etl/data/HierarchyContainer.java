/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.data;

import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.utils.schema.SchemaDocument;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author crusnak
 */
public class HierarchyContainer implements Serializable, DataSet
{
    private static final long serialVersionUID = 239823569004520L;

    private String name = null;
    private SchemaDocument schema = null;
    private Map<String,DataColumn> xpaths = new LinkedHashMap<String, DataColumn>();
    private List<HierarchyCell> rowdata = new ArrayList();
    private String rootXpath = null;
    private int currentRow = 0;
    
    public HierarchyContainer(String task, SchemaDocument s, String root)
        throws ETLException
    {
        name = task;
        schema = s;
        rootXpath = root;
        init();
    }

    @Override
    public Type getType()
    {
        return Type.hierarchical;
    }

    @Override
    public String getDataName()
    {
        return name;
    }

    @Override
    public void clear()
    {
        clearData();
    }

    public void clearData()
    {
        rowdata.clear();
    }
    
    public int rowCount()
    {
        return rowdata.size();
    }

    public int getCurrentProcessedRow()
    {
        return currentRow;
    }
    
    public void setCurrentProcessedRow(int rownum)
    {
        currentRow = rownum;
    }

    public SchemaDocument getSchema()
    {
        return schema;
    }

    public String getRootXpath()
    {
        return rootXpath;
    }

    @Override
    public Map<String,DataColumn> getColumnDefinitions()
    {
        return xpaths;
    }
    
    @Override
    public void setColumnDefinitions(Map<String,DataColumn> paths)
       throws ETLException
    {
        xpaths = paths;
    }

    public List<HierarchyCell> getPathedData()
    {
        return rowdata;
    }

    public void addAllPathedData(List<HierarchyCell> data)
        throws ETLException
    {
        rowdata.addAll(data);
    }
    
    @Override
    public void copyDataSet(DataSet data, boolean clearColumns, boolean clearData)
        throws ETLException
    {
        throw new UnsupportedOperationException("Copying of hierarchical data sets not supported yet");
    }
    
    private void init()
        throws ETLException
    {
        
    }
}
