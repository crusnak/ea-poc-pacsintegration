/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.data;

import java.util.LinkedHashMap;

/**
 *
 * @author crusnak
 */
public class LookupTable extends LinkedHashMap<String, String>
{
    private String name = "";

    public LookupTable(String n)
    {
        super();
        name = n;
    }

    public String getTableName()
    {
        return name;
    }

    public String toString()
    {
        StringBuffer out = new StringBuffer();
        out.append(name + "=" + super.toString());
        return out.toString();
    }
}
