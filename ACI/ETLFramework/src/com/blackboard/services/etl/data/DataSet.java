/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.data;

import com.blackboard.services.etl.exception.ETLException;
import java.util.Map;

/**
 *
 * @author crusnak
 */
public interface DataSet
{
    public static enum Type { relational, hierarchical };
    
    public Type getType();
    public void clear();
    public void clearData();
    public String getDataName();
    public int rowCount();
    public int getCurrentProcessedRow();
    public void setCurrentProcessedRow(int row);
    public Map<String,DataColumn> getColumnDefinitions();
    public void setColumnDefinitions(Map<String,DataColumn> xpaths) throws ETLException;

    public void copyDataSet(DataSet data, boolean clearColumns, boolean clearData) throws ETLException;
}
