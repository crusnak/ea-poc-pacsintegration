/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.data;

import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.jaxb.TransformComplexType;
import com.blackboard.services.etl.tasks.AbstractTask;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;

/**
 *
 * @author crusnak
 */
public class TransformSource extends Argument
{
    private boolean forwardColumn = false;
    private String path = null;
    private String refNode = null;

    public TransformSource(TransformComplexType.SourceColumn arg, AbstractTask t)
        throws ETLException
    {
        super(arg, t);
        forwardColumn = arg.isForwardColumn();
        path = arg.getPath();
        refNode = arg.getRef();
    }

    public boolean isSourceColumnForwarded()
    {
        return forwardColumn;
    }
    
    public String getPath()
    {
        return path;
    }
    
    public String getRefNode()
    {
        return refNode;
    }
}
