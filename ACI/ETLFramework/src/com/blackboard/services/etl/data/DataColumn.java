/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.data;

import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.utils.BbJSONObject;
import com.blackboard.services.utils.JavaUtils;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author crusnak
 */
public class DataColumn implements Serializable
{
    //public enum Type { string, shortint, longint, bool, floatsingle, floatdouble, date, object }

    public static final Class CLASS_TYPE_STRING =           String.class;
    public static final Class CLASS_TYPE_LONG =             Long.TYPE;
    public static final Class CLASS_TYPE_SHORT =            Integer.TYPE;
    public static final Class CLASS_TYPE_DOUBLE =           Double.TYPE;
    public static final Class CLASS_TYPE_FLOAT =            Float.TYPE;
    public static final Class CLASS_TYPE_BOOLEAN =          Boolean.TYPE;
    public static final Class CLASS_TYPE_DATE =             Date.class;
    public static final Class CLASS_TYPE_BINARY =           BinaryObject.class;
    public static final Class CLASS_TYPE_JSON =             JacksonJsonContainer.class;
    public static final Class CLASS_TYPE_OBJECT =           Object.class;

    private static final long serialVersionUID = 938023522036L;

    private DataTypes dataType = DataTypes.OBJECT;
    private String columnName = null;
    private String defaultValue = null;
    private boolean unique = false;

    public DataColumn(DataTypes type, String name, boolean primary)
    {
        this(type, name, primary, null);
    }
    
    public DataColumn(DataTypes type, String name)
    {
        this(type, name, false);
    }
    
    public DataColumn(DataTypes type, String name, String def)
    {
        this(type, name, false, def);
    }

    public DataColumn(DataTypes type, String name, boolean u, String def)
    {
        setDataType(type);
        setColumnName(name);
        setDefaultValue(def);
        unique = u;
    }

    public DataColumn(String type, String name)
        throws ETLException
    {
        this(type, name, false);
    }

    public DataColumn(String type, String name, boolean primary)
        throws ETLException
    {
        this(type, name, primary, null);
    }
    
    public DataColumn(String type, String name, String def)
        throws ETLException
    {
        this(type, name, false, def);
    }

    public DataColumn(String type, String name, boolean u, String def)
        throws ETLException
    {
        setDataType(type);
        setColumnName(name);
        setDefaultValue(def);
        unique = u;
    }
    
    public boolean isUniqueColumn()
    {
        return unique;
    }

    public void setUniqueColumn(boolean u)
    {
        unique = u;
    }
    
    public String getDefaultValue()
    {
        return defaultValue;
    }

    public boolean isDefaultValueDefined()
    {
        return defaultValue != null && defaultValue.trim().length() > 0;
    }

    public void setDefaultValue(String def)
    {
        defaultValue = def;
    }

    public String getColumnName()
    {
        return columnName;
    }

    public void setColumnName(String columnName)
    {
        this.columnName = columnName;
    }

    public DataTypes getDataType()
    {
        return dataType;
    }

    public void setDataType(DataTypes dataType)
    {
        this.dataType = dataType;
    }

    public void setDataType(String type)
        throws ETLException
    {
        try
        {
            if ( type != null )
            {
                setDataType(DataTypes.valueOf(type));
            }
            else
            {
                setDataType(DataTypes.STRING);
            }
        }
        catch (IllegalArgumentException e)
        {
            throw new ETLException("Unsupported data type: " + type + ". Supported types: " + JavaUtils.enumToList(DataTypes.class));
        }
    }

    public Class getColumnClass()
    {
        Class clazz = Object.class;
        if ( DataTypes.STRING == getDataType() )
        {
            clazz = CLASS_TYPE_STRING;
        }
        else if ( DataTypes.BOOLEAN == getDataType() )
        {
            clazz = CLASS_TYPE_BOOLEAN;
        }
        else if ( DataTypes.LONG == getDataType() )
        {
            clazz = CLASS_TYPE_LONG;
        }
        else if ( DataTypes.INT == getDataType() )
        {
            clazz = CLASS_TYPE_SHORT;
        }
        else if ( DataTypes.FLOAT == getDataType() )
        {
            clazz = CLASS_TYPE_FLOAT;
        }
        else if ( DataTypes.DOUBLE == getDataType() )
        {
            clazz = CLASS_TYPE_DOUBLE;
        }
        else if ( DataTypes.DATE == getDataType() )
        {
            clazz = CLASS_TYPE_DATE;
        }
        else if ( DataTypes.BINARY == getDataType() )
        {
            clazz = CLASS_TYPE_BINARY;
        }
        else if ( DataTypes.JSON == getDataType() )
        {
            clazz = CLASS_TYPE_JSON;
        }

        return clazz;
    }

    @Override
    public String toString()
    {
        return getColumnName() + "[" + getColumnClass().getName() + "]" + (isUniqueColumn()? " <PK>":"") + (isDefaultValueDefined()?" (" + defaultValue + ")":"");
    }

}
