/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.data;

import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.exception.IndexException;
import com.blackboard.services.etl.jaxb.IndexComplexType;
import com.blackboard.services.etl.tasks.BooleanTest;
import com.blackboard.services.etl.tasks.FilterBranch;
import com.blackboard.services.utils.JavaUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
 
// TODO: revamp this to only use sets and support comparison types other than equality. Boolean tests can be performed
// by intersecting results for AND boolean types and unioning results for OR boolean types.

/**
 *
 * @author crusnak
 */
public class DataIndex implements Serializable, Comparable<Object>
{
    private static final long serialVersionUID = 2350937847L;

    public static enum Type { unique, nonunique };

    private HashSet<String> columns = new LinkedHashSet();
    private HashMap<String,Set<Integer>> valueMap = new HashMap();
    private HashMap<Integer,Set<String>> indexMap = new HashMap();
    private Type type = null;
    private List<BooleanTest.BooleanType> operators = new ArrayList();

    public DataIndex(IndexComplexType.Index ct)//, Map<String,DataColumn> colmap)
        throws ETLException
    {
        setType(ct.getType());
        for ( String colname: ct.getColumn() )
        {
            /*DataColumn col = colmap.get(colname);
            if ( col == null )
            {
                throw new ETLException("Undefined column reference in data index: " + colname);
            }

            columns.put(colname, col);*/
            columns.add(colname);
        }
    }

    public DataIndex(Type t, FilterBranch filter)
        throws ETLException
    {
        type = t;
        for ( BooleanTest test: filter.getTests() )
        {
            if ( test.getTestType() == BooleanTest.TestType.BOOLEAN )
            {
                if ( test.getOperand1().getArgumentType() == Argument.Type.column )
                {
                    String colname = String.valueOf(test.getOperand1().getValue());
                    BooleanTest.BooleanType operator = test.getBooleanType();
                    columns.add(colname);
                    operators.add(operator);

                    // If any operators exist that aren't eq, then the index cannot be unique
                    if ( operator != BooleanTest.BooleanType.eq )
                    {
                        type = Type.nonunique;
                    }
                }
                else
                {
                    throw new ETLException("Operand1 of a where clause must be a column");
                }
            }
        }
    }
    

    public DataIndex(Type t, List<String> colnames)//, Map<String,DataColumn> colmap)
    {
        type = t;
        for ( String colname: colnames )
        {
            /*DataColumn col = colmap.get(colname);
            if ( col == null )
            {
                throw new ETLException("Undefined column reference in data index: " + colname);
            }

            columns.put(colname, col);*/
            columns.add(colname);
            operators.add(BooleanTest.BooleanType.eq);
        }
    }
    
    public Type getType()
    {
        return type;
    }

    private void setType(String t)
        throws ETLException
    {
        try
        {
            if ( t == null )
            {
                throw new IllegalArgumentException("DataIndex type cannot be null");
            }

            type = Type.valueOf(t);
        }
        catch (IllegalArgumentException e)
        {
            throw new ETLException("Unable to set type for DataIndex, expecting one of: " + JavaUtils.enumToList(Type.class));
        }
    }

    public List<String> getIndexedColumnNames()
    {
        return new ArrayList(columns);
    }

    public boolean containsColumn(String name)
    {
        return columns.contains(name);
    }

    public Map<String,Set<Integer>> getIndexedValueMap()
    {
        return valueMap;
    }
    
    public List<Integer> getIndexedRows(List vals)
    {
        ArrayList<Integer> indices = new ArrayList();
        Set<Integer> i = new HashSet<Integer>();
        int keycount = 0;

        org.apache.log4j.Logger logger = Logger.getLogger(this.getClass());
        logger.trace("Searching vals: " + vals);
        switch (type)
        {
            case unique:
                i = valueMap.get(generateUniqueKey(vals));
                break;
            case nonunique:
                for ( String key: generateNonUniqueKeys(vals) )
                {
                    logger.trace("non-unique key: " + key);
                    if ( keycount == 0 && valueMap.containsKey(key) )
                    {
                        i = new HashSet<Integer>(valueMap.get(key));
                        logger.trace("First non-unique matched: " + i);
                    }
                    else if ( valueMap.containsKey(key) )
                    {
                        //logger.trace(valueMap);
                        Set<Integer> vm = new HashSet<Integer>();
                        vm.addAll(valueMap.get(key));
                        i.retainAll(vm);
                        logger.trace("#" + (keycount+1) + " non-unique matched: " + i);
                    }
                    // If no match is found for the walking key then it will never find one continuing along the key so break
                    else 
                    {
                        logger.trace("NO MATCH for " + key + ". Removing all previous matches.");
                        if ( i != null )
                        {
                            i.clear();
                        }
                        break;
                    }
                    
                    keycount++;
                }
                break;
        }
        
        if ( i != null )
        {
            indices.addAll(i);
        }
        return indices;
    }

    public int getIndexedRow(List vals)
        throws IndexException
    {
        int index = -1;
        switch (type)
        {
            case unique:
                List<Integer> indices = getIndexedRows(vals);
                if ( indices.size() > 0 )
                {
                    index = indices.get(0);
                }
                break;
            default:
                throw new IndexException(this, "Cannot get a single index from a non-unique data index");
        }
        return index;
    }

    public void addIndex(List vals, int index)
        throws IndexException
    {
        Logger logger = Logger.getLogger(this.getClass());
        switch (type)
        {
            case unique:
                String key = generateUniqueKey(vals);
                if ( valueMap.containsKey(key) )
                {
                    int eidx = getIndexedRow(vals);
                    throw new IndexException(this, "Unable to assign unique index since an index for key '" + key +
                                             "' already exists in the data set at row (0 based): " + eidx);
                }
                
                HashSet<Integer> indices = new HashSet();
                indices.add(index);
                valueMap.put(key, indices);
                indexKey(index, key);
                break;
            case nonunique:
                List<String> keys = generateNonUniqueKeys(vals);
                for ( String k: keys )
                {
                    //logger.trace("non-unique key: " + k);
                    indices = new HashSet();
                    if ( valueMap.containsKey(k) )
                    {
                        indices.addAll(valueMap.get(k));
                    }
                    
                    indices.add(index);
                    valueMap.put(k, indices);
                    indexKey(index, k);

                    //logger.trace("indices: " + indices);
                    //logger.trace("current valueMap: " + valueMap);
                }
                break;
        }
    }

    private void indexKey(int index, String key)
    {
        HashSet<String> keys = new HashSet();
        if ( indexMap.containsKey(index) )
        {
            keys.addAll(indexMap.get(index));
        }
        keys.add(key);
        indexMap.put(index, keys);
    }

    public void removeIndex(int index)
    {
        for ( String key: indexMap.get(index) )
        {
            switch (type)
            {
                case unique:
                    valueMap.remove(key);
                    break;
                case nonunique:
                    Set<Integer> indices = valueMap.get(key);
                    if ( indices != null )
                    {
                        if ( indices.size() > 1 )
                        {
                            indices.remove(index);
                        }
                        else
                        {
                            valueMap.remove(key);
                        }
                    }

                    break;
            }
        }

        indexMap.remove(index);
    }

    public void clearIndices()
    {
        valueMap.clear();
        indexMap.clear();
    }

    public String generateUniqueKey(List vals)
    {
        String key = "";
        for ( int i=0; i<vals.size(); i++ )
        {
            key += String.valueOf(vals.get(i));
            if ( i <vals.size()-1 )
            {
                key += "|";
            }
        }
        return key;
    }

    public List<String> generateNonUniqueKeys(List vals)
    {
        List<String> keylist = new ArrayList();
        List<List<String>> keys = new ArrayList();
        for ( int i=0; i<vals.size(); i++ )
        {
            List<String> vkeys = new ArrayList();
            BooleanTest.BooleanType bool = operators.get(i);
            String val = String.valueOf(vals.get(i));
            switch (bool)
            {
                case eq:
                    vkeys.add(val);
                    break;
                case startswith:
                    String k = "";
                    for ( int j=0; j<val.length(); j++ )
                    {
                        k += val.charAt(j);
                        vkeys.add(k);
                    }
                    break;
                case endswith:
                    k = "";
                    for ( int j=val.length()-1; j>=0; j-- )
                    {
                        k = val.charAt(j) + k;
                        vkeys.add(k);
                    }
                case contains:
                    k = "";
                    for ( int j=0; j<val.length(); j++ )
                    {
                        k += val.charAt(j);
                        vkeys.add(k);
                    }
                    break;
                    
            }
            
            keys.add(vkeys);
        }

        for ( List<String> list: keys )
        {
            keylist = generateKeysFromList(keylist, list);
        }
        
        return keylist;
    }

    private List<String> generateKeysFromList(List<String> current, List<String> next)
    {
        List<String> out = new ArrayList();
        if ( current == null || current.isEmpty() )
        {
            for ( String key: next )
            {
                out.add(key);
            }
        }
        else
        {
            for ( String first: current )
            {
                for ( String second: next )
                {
                    out.add(first + "|" + second);
                }
            }
        }
        
        return out;
    }
    
    @Override
    public int hashCode()
    {
        int hc = super.hashCode();

        String agg = String.valueOf(columns) + type;
        hc = agg.hashCode();
        
        return hc;
    }

    @Override
    public boolean equals(Object obj)
    {
        boolean equal = true;
        equal &= obj != null && obj instanceof DataIndex;
        if ( equal )
        {
            DataIndex that = (DataIndex)obj;
            equal &= this.columns.size() == that.columns.size() && this.type == that.type;
            if ( equal )
            {
                for ( String colname: columns )
                {
                    equal &= that.columns.contains(colname);
                }
            }
        }

        return equal;
    }

    @Override
    public int compareTo(Object o)
    {
        int compare = 0;
        if ( o instanceof DataIndex )
        {
            DataIndex that = (DataIndex)o;
            String thisstr = "";
            String thatstr = "";
            if ( this != null )
            {
                thisstr += getType() + "|" + getIndexedColumnNames();
                Type thattype = null;
                String thatcols = null;
                if ( that != null )
                {
                    thattype = that.getType();
                    thatcols = String.valueOf(that.getIndexedColumnNames());
                }
                thatstr += thattype + "|" + thatcols;
                
                compare = thisstr.compareTo(thatstr);
            }
            else if ( that != null )
            {
                compare = 1;
            }
        }
        
        return compare;
    }

    @Override
    public String toString()
    {
        StringBuffer out = new StringBuffer();
        out.append("{");
        out.append("Type: " + type);
        out.append(", Columns: " + columns);
        out.append(", Indices: " + valueMap);
        out.append("}");
        return out.toString();
    }

    public static void main(String[] args)
    {
        try
        {
            ArrayList cols = new ArrayList();
            cols.add("Col1");
            cols.add("Col2");
            cols.add("Col2");
            DataIndex i1 = new DataIndex(Type.unique, cols);

            ArrayList cols2 = new ArrayList();
            cols2.add("Col1");
            cols2.add("Col2");
            cols2.add("Col2");
            cols2.add("Col1");
            DataIndex i2 = new DataIndex(Type.unique, cols2);
            
            System.out.println(i2.compareTo(i1));
            HashSet<DataIndex> indices = new LinkedHashSet();
            indices.add(i1);
            indices.add(i2);
            System.out.println("Index Set: " + indices);
            
            com.blackboard.services.etl.jaxb.ObjectFactory factory = new com.blackboard.services.etl.jaxb.ObjectFactory();
            com.blackboard.services.etl.jaxb.FilterTaskComplexType.Filter filter = factory.createFilterTaskComplexTypeFilter();
            com.blackboard.services.etl.jaxb.FilterTaskComplexType.Filter.NextTask nextTask = factory.createFilterTaskComplexTypeFilterNextTask();
            com.blackboard.services.etl.jaxb.BooleanComplexType bool = factory.createBooleanComplexType();
            com.blackboard.services.etl.jaxb.BooleanComplexType.Operand1 op1 = factory.createBooleanComplexTypeOperand1();
            com.blackboard.services.etl.jaxb.BooleanComplexType.Operand2 op2 = factory.createBooleanComplexTypeOperand2();

            op1.setType("column");
            op1.setValue("Column1");
            op2.setDataType(com.blackboard.services.etl.jaxb.DataTypes.STRING);
            op2.setType("constant");
            op2.setValue("B");

            bool.setType(com.blackboard.services.etl.jaxb.BooleanOperators.EQ);
            bool.setOperand1(op1);
            bool.setOperand2(op2);
            nextTask.setName("test");

            javax.xml.bind.JAXBElement elem = factory.createFilterTaskComplexTypeFilterIf(bool);
            filter.setType(FilterBranch.TYPE_TEST);
            filter.getIfAndAndOrOr().add(elem);
            
            bool = factory.createBooleanComplexType();
            op1 = factory.createBooleanComplexTypeOperand1();
            op2 = factory.createBooleanComplexTypeOperand2();
            
            op1.setType("column");
            op1.setValue("Column2");
            op2.setDataType(com.blackboard.services.etl.jaxb.DataTypes.STRING);
            op2.setType("constant");
            op2.setValue("215");
            
            bool.setType(com.blackboard.services.etl.jaxb.BooleanOperators.STARTSWITH);
            bool.setOperand1(op1);
            bool.setOperand2(op2);

            elem = factory.createFilterTaskComplexTypeFilterAnd(null);
            filter.getIfAndAndOrOr().add(elem);
            elem = factory.createFilterTaskComplexTypeFilterIf(bool);
            filter.getIfAndAndOrOr().add(elem);

            bool = factory.createBooleanComplexType();
            op1 = factory.createBooleanComplexTypeOperand1();
            op2 = factory.createBooleanComplexTypeOperand2();
            
            op1.setType("column");
            op1.setValue("Column3");
            op2.setDataType(com.blackboard.services.etl.jaxb.DataTypes.STRING);
            op2.setType("constant");
            op2.setValue("9321");
            
            bool.setType(com.blackboard.services.etl.jaxb.BooleanOperators.ENDSWITH);
            bool.setOperand1(op1);
            bool.setOperand2(op2);

            elem = factory.createFilterTaskComplexTypeFilterAnd(null);
            filter.getIfAndAndOrOr().add(elem);
            elem = factory.createFilterTaskComplexTypeFilterIf(bool);
            filter.getIfAndAndOrOr().add(elem);
            
            filter.setNextTask(nextTask);
            
            FilterBranch fb = new FilterBranch(filter, null, false);

            DataIndex i3 = new DataIndex(Type.nonunique, fb);
            ArrayList<String> vals = new ArrayList();
            vals.add("BA");
            vals.add("211");
            vals.add("2523");
            i3.addIndex(vals, 1);
            System.out.println(i3);
            System.out.println("IndexedRows: " + i3.getIndexedRows(vals));
            
            System.out.println("Cols1: " + cols);
            System.out.println("Cols2: " + cols2);
            System.out.println("DataIndex #1: " + i1 + "(" + i1.hashCode() + ")");
            System.out.println("DataIndex #2: " + i2 + "(" + i2.hashCode() + ")");
            System.out.println("Equal: " + i1.equals(i2));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
