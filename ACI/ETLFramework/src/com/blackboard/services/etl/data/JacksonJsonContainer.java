/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.data;

import com.blackboard.services.utils.XMLUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.InvalidPathException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.http.NameValuePair;
import org.w3c.dom.Document;

/**
 *
 * @author crusnak
 */
public class JacksonJsonContainer implements Serializable
{
    public static final char PATH_SEPARATOR =          '/';
    private static final long serialVersionUID = 8763923964L;
    private static final ObjectMapper mapper = new ObjectMapper();
    
    private JsonNode rootNode;
    
    public JacksonJsonContainer() 
    {
        super();
    }
    
    public JacksonJsonContainer(JsonNode json)
        throws JsonProcessingException
    {
        super();
        rootNode = mapper.readTree(json.toString());
    }
    
    public JacksonJsonContainer(JacksonJsonContainer jjc)
        throws JsonProcessingException
    {
        super();
        rootNode = mapper.readTree(jjc.toString());
    }

    public JacksonJsonContainer(String json)
        throws IOException
    {
        super();
        rootNode = mapper.readTree(json);
    }
    
    public JacksonJsonContainer(File json)
        throws IOException
    {
        super();
        rootNode = mapper.readTree(json);
    }

    public JacksonJsonContainer(String json, String root)
        throws IOException
    {
        this(json);
        ObjectNode on  = mapper.createObjectNode();
        on.set(root, rootNode);
        rootNode = on;
    }
    
    public JsonNode getRootNode()
    {
        return rootNode;
    }
    
    public void setFieldValue(String nodepath, Object value)
       throws InvalidPathException
    {
        JsonNode source = getParentFieldValue(nodepath);
        String name = nodepath.substring(nodepath.lastIndexOf(PATH_SEPARATOR) + 1);
        if ( source.getNodeType() == JsonNodeType.OBJECT )
        {
            setField((ObjectNode)source, name, value);
        }
        else
        {
            throw new IllegalArgumentException("Unsupported parent source json object found via nodepath: " + nodepath + ": " + source.getClass().getName());
        }
    }
    
    public void addFieldValue(String nodepath, String name, Object value)
    {
        JsonNode source = getFieldValue(nodepath);
        addField(source, name, value);
    }

    private void addField(JsonNode node, String name, Object val)
    {
        ArrayNode array = null;
        ObjectNode object = null;
        switch ( node.getNodeType() )
        {
            case ARRAY:
                array = (ArrayNode)node;
                break;
            case OBJECT:
                object = (ObjectNode)node;
                array = mapper.createArrayNode();
                object.set(name, array);
                break;
            default:
                throw new IllegalArgumentException("Cannot addField to a non-ARRAY/OBJECT JsonNode: " + node.getNodeType());
        }
        
        if ( val == null )
        {
            array.add((Integer)null);
        }
        else if ( val instanceof Integer )
        {
            array.add((Integer)val);
        }
        else if ( val instanceof Float )
        {
            array.add((Float)val);
        }
        else if ( val instanceof Double )
        {
            array.add((Double)val);
        }
        else if ( val instanceof Long )
        {
            array.add((Long)val);
        }
        else if ( val instanceof Short )
        {
            array.add((Short)val);
        }
        else if ( val instanceof Boolean )
        {
            array.add((Boolean)val);
        }
        else if ( val instanceof BigInteger )
        {
            array.add((BigInteger)val);
        }
        else if ( val instanceof BigDecimal )
        {
            array.add((BigDecimal)val);
        }
        else if ( val instanceof JacksonJsonContainer )
        {
            array.add(((JacksonJsonContainer)val).getRootNode());
        }
        else if ( val instanceof JsonNode )
        {
            array.add(((JsonNode)val));
        }
        else
        {
            array.add(String.valueOf(val));
        }
    }
    
    private void setField(ObjectNode node, String name, Object val)
    {
        if ( val == null )
        {
            node.put(name, (Integer)null);
        }
        else if ( val instanceof Integer )
        {
            node.put(name, (Integer)val);
        }
        else if ( val instanceof Float )
        {
            node.put(name, (Float)val);
        }
        else if ( val instanceof Double )
        {
            node.put(name, (Double)val);
        }
        else if ( val instanceof Long )
        {
            node.put(name, (Long)val);
        }
        else if ( val instanceof Short )
        {
            node.put(name, (Short)val);
        }
        else if ( val instanceof Boolean )
        {
            node.put(name, (Boolean)val);
        }
        else if ( val instanceof BigInteger )
        {
            node.put(name, (BigInteger)val);
        }
        else if ( val instanceof BigDecimal )
        {
            node.put(name, (BigDecimal)val);
        }
        else if ( val instanceof JacksonJsonContainer )
        {
            node.set(name, ((JacksonJsonContainer)val).getRootNode());
        }
        else if ( val instanceof JsonNode )
        {
            node.set(name, (JsonNode)val);
        }
        else
        {
            node.put(name, String.valueOf(val));
        }
    }
    
    public Object removeField(String nodepath)
    {
        Object val = null;
        JsonNode source = getParentFieldValue(nodepath);
        String name = nodepath.substring(nodepath.lastIndexOf(PATH_SEPARATOR) + 1);
        if ( source.getNodeType() == JsonNodeType.OBJECT )
        {
            val = ((ObjectNode)source).remove(name);
        }
        else
        {
            throw new IllegalArgumentException("Unsupported parent source json object found via nodepath: " + nodepath + ": " + source.getClass().getName());
        }
        
        return val;
    }
    
    public Object removeIndexedField(String nodepath, int index)
    {
        Object val = null;
        JsonNode source = getFieldValue(nodepath);
        if ( source.getNodeType() == JsonNodeType.ARRAY )
        {
            ArrayNode array = (ArrayNode)source;
            val = array.remove(index);
        }
        else
        {
            throw new IllegalArgumentException("Unsupported parent source json object found via nodepath: " + nodepath + ": " + source.getClass().getName());
        }
        
        return val;
    }

    public String getStringFieldValue(String nodepath)
        throws InvalidPathException, JsonProcessingException
    {
        String out = null;
        JsonNode node = getFieldValue(nodepath);
        
        switch ( node.getNodeType() )
        {
            case ARRAY:
            case OBJECT:
                out = mapper.writeValueAsString(node);
                break;
            case BOOLEAN:
            case NUMBER:
            case STRING:
                out = node.asText();
                break;
            case NULL:
                out = null;
                break;
            default:
                out = String.valueOf(node);
        }       
        
        return out;
    }
    
    public JsonNode getFieldValue(String nodepath)
       throws InvalidPathException
    {
        JsonNode node = rootNode.at(nodepath);
        if ( node.getNodeType() == JsonNodeType.MISSING )
        {
            throw new InvalidPathException(this.toString(), "Cannot get json node from source with path: " + nodepath);
        }
        
        return node;
    }
    
    public JsonNode getParentFieldValue(String nodepath)
        throws InvalidPathException
    {
        //System.out.println("Processing: '" + nodepath + "'");
        
        if ( nodepath == null || nodepath.trim().length() == 0 || nodepath.trim().charAt(0) != PATH_SEPARATOR )
        {
            throw new InvalidPathException(nodepath, "Invalid path. Paths must start with " + PATH_SEPARATOR);
        }

        // Remove everything after last path separator
        if ( nodepath.lastIndexOf(PATH_SEPARATOR) > 0 )
        {
            nodepath = nodepath.substring(0, nodepath.lastIndexOf(PATH_SEPARATOR));
        }
        else
        {
            nodepath = "";
        }
        
        if ( String.valueOf(PATH_SEPARATOR).equals(nodepath) )
        {
            nodepath = "";
        }
        
        return getFieldValue(nodepath);
    }
    
    public List<Integer> getArrayedIndexFromFilter(String nodepath, List<ObjectFilter> filters)
        throws InvalidPathException
    {
        List<Integer> indices = new ArrayList();
        
        JsonNode node = getFieldValue(nodepath);
        if ( node.getNodeType() == JsonNodeType.ARRAY )
        {
            Iterator<JsonNode> it = node.elements();
            for ( int i=0; it.hasNext(); i++ )
            {
                JsonNode n = it.next();
                if ( n.isObject() )
                {
                    // Iterate over all declared boolean filters to find a match for the current object
                    boolean match = true;
                    for ( int j=0; j<filters.size(); j++ )
                    {
                        ObjectFilter filter = filters.get(j);
                        String val = n.at(filter.getName()).asText("");
                        match = val.equals(filter.getValue());
                        
                        // No need to continue if false and AND or true and OR
                        if ( (filter.getBooleanType() == ObjectFilter.BooleanType.AND && !match) ||
                             (filter.getBooleanType() == ObjectFilter.BooleanType.OR && match) )
                        {
                            break;
                        }
                    }
                    
                    // If a match is found after all boolean filters are applied, add the array index
                    if ( match )
                    {
                        indices.add(i);
                    }
                }
                else
                {
                    throw new InvalidPathException(this.toString(), "Can only get arrayed index of objects: " + nodepath);
                }
            }
        }
        else
        {
            throw new InvalidPathException(this.toString(), "Parent of nodepath is not an array: " + nodepath);
        }
        
        return indices;
    }
    
    public String getArrayedListFieldValue(String nodepath, char delimiter)
        throws InvalidPathException
    {
        String out = null;
        ArrayList<String> vals = new ArrayList();
        JsonNode pn = getParentFieldValue(nodepath);
        if ( pn.getNodeType() == JsonNodeType.ARRAY )
        {
            String childpath = nodepath.substring(nodepath.lastIndexOf(JacksonJsonContainer.PATH_SEPARATOR));
            Iterator<JsonNode> it = pn.elements();
            while ( it.hasNext() )
            {
                JsonNode child = it.next();
                vals.add(child.at(childpath).asText());
            }
            
            out = "";
            for ( int i=0; i<vals.size(); i++ )
            {
                out += vals.get(i);
                if ( i < vals.size() - 1 )
                {
                    out += delimiter;
                }
            }
        }
        else
        {
            throw new InvalidPathException(this.toString(), "Parent of nodepath is not an array: " + nodepath);
        }
        
        return out;
    }
    
    public Document getXMLDocument()
        throws ParserConfigurationException
    {
        return XMLUtils.parseJSON(this.toString());
    }
    
    public Document getXMLDocument(String rootElement)
        throws ParserConfigurationException
    {
        return XMLUtils.parseJSON(this.toString(), rootElement);
    }

    public String toXMLString()
    {
        String out = "";
        try
        {
            Document xml = getXMLDocument(this.toString());
            out = XMLUtils.toString(xml, false);
        }
        catch (ParserConfigurationException e)
        {
            out = e.getMessage() + ": " + this.toString();
        }
        
        return out;
    }

    @Override
    public boolean equals(Object o) 
    {
        return rootNode.equals(o);
    }

    @Override
    public String toString() 
    {
        return rootNode.toString();
    }
    
    public String toPrettyString() 
    {
        return rootNode.toPrettyString();
    }
    
    public static class ObjectFilter
    {
        public static enum BooleanType { AND, OR };
        
        BooleanType bt = BooleanType.OR;
        String name;
        String value;
        
        public ObjectFilter(String n, String v, BooleanType b)
        {
            this.name = n;
            this.value = v;
            this.bt = b;
        }

        public BooleanType getBooleanType() 
        {
            return bt;
        }

        public void setBooleanType(BooleanType bt) 
        {
            this.bt = bt;
        }

        public String getName() 
        {
            return name;
        }

        public void setName(String name) 
        {
            this.name = name;
        }

        public String getValue() 
        {
            return value;
        }

        public void setValue(String value) 
        {
            this.value = value;
        }

        @Override
        public String toString() 
        {
            return name + "=" + value + " " + bt;
        }
        
        
    }

    public static void main(String[] args)
    {
        try
        {
            String jstr = "{\"CardholderId\":\"33200bfe-8918-406d-9ab3-1c95d64396e4\",\"LastName\":\"Allen\",\"FirstName\":\"Josh\",\"MiddleInitial\":\"y\",\"CompanyId\":null,\"MemberOfAllSites\":true,\"Notes\":null,\"CardholderStatus\":1,\"CardholderActiveDate\":null,\"CardholderExpireDate\":null,\"LastCardholderModRowVersion\":\"0x00000000000036BD\",\"LastModified\":\"2021-08-17T00:03:30.253\",\"Images\":[],\"CardholderAccessLevels\":[{\"CardholderAccessLevelID\":\"4e43d5cc-f23b-4d51-969c-0a0b90861f86\",\"CardholderID\":\"33200bfe-8918-406d-9ab3-1c95d64396e4\",\"AccessLevelID\":\"df589e20-842e-417b-9600-4aaad9e0de1b\",\"LastModified\":\"2021-08-17T00:03:30.257\",\"ActivateDate\":null,\"DeactivateDate\":null},{\"CardholderAccessLevelID\":\"2d43b4cf-9c48-4b91-bed4-ec48df249250\",\"CardholderID\":\"33200bfe-8918-406d-9ab3-1c95d64396e4\",\"AccessLevelID\":\"58d7ab01-7535-4aab-bcb9-34025f92c7c3\",\"LastModified\":\"2021-08-16T17:34:29.267\",\"ActivateDate\":null,\"DeactivateDate\":null}],\"CardholderSites\":[],\"CardholderCards\":[{\"CardId\":\"a7d3672a-bef0-47b2-8eba-b5b4312952a9\",\"CardholderID\":\"33200bfe-8918-406d-9ab3-1c95d64396e4\",\"CardStatus\":1,\"LastModified\":\"2021-08-17T00:03:30.26\",\"ActiveDate\":\"1899-12-30T07:00:00\",\"ExpireDate\":\"2173-10-13T07:00:00\"}],\"UserColumns\":{\"UserText1\":\"0000000000000000800017\",\"UserText2\":\"c5e03480-5d9d-464b-9f28-808311bd83fb\",\"UserText3\":null,\"UserText4\":null,\"UserText5\":null,\"UserText6\":null,\"UserText7\":null,\"UserText8\":null,\"UserText9\":null,\"UserText10\":null,\"UserText11\":null,\"UserText12\":null,\"UserText13\":null,\"UserText14\":null,\"UserText15\":null,\"UserText16\":null,\"UserText17\":null,\"UserText18\":null,\"UserText19\":null,\"UserText20\":null,\"Department\":null,\"UserDate1\":null,\"UserDate2\":null,\"UserDate3\":null,\"UserDate4\":null,\"UserDate5\":null,\"UserNumeric1\":null,\"UserNumeric2\":null,\"UserNumeric3\":null,\"UserNumeric4\":null,\"UserNumeric5\":null}}";
            String access = "{\"CardholderID\":\"33200bfe-8918-406d-9ab3-1c95d64396e4\",\"AccessLevelID\":\"0abd8860-38a4-4adc-9835-812b74fb442f\",\"LastModified\":\"2021-07-14T22:44:31.638Z\",\"ActivateDate\":\"2019-02-01T08:00:00Z\",\"DeactivateDate\":\"2019-05-15T17:00:00Z\"}";
            JacksonJsonContainer json = new JacksonJsonContainer(jstr);
            JacksonJsonContainer ac = new JacksonJsonContainer(access);
            
            System.out.println(json);
            json.addFieldValue("/CardholderAccessLevels", null, ac);
            json.addFieldValue("/CardholderSites", null, true);
            System.out.println(json.toPrettyString());
            
            List<ObjectFilter> filters = new ArrayList();
            filters.add(new ObjectFilter("/CardholderID", "33200bfe-8918-406d-9ab3-1c95d64396e4" , ObjectFilter.BooleanType.AND));
            filters.add(new ObjectFilter("/AccessLevelID", "58d7ab01-7535-4aab-bcb9-34025f92c7c3" , ObjectFilter.BooleanType.AND));
            List<Integer> indices = json.getArrayedIndexFromFilter("/CardholderAccessLevels", filters);
            System.out.println(indices);
            for ( Integer index: indices )
            {
                json.removeIndexedField("/CardholderAccessLevels", index);
            }
            System.out.println(json.toPrettyString());
            System.exit(0);
            
            new JacksonJsonContainer(json);
            
            //System.out.println("/UserColumns/UserText1=" + json.getFieldValue("/UserColumns/UserText1"));
            //System.out.println("/UserColumns/UserText1 parent=" + json.getParentFieldValue("/UserColumns/UserText1"));
            
            System.out.println(json.getFieldValue(""));
            json.setFieldValue("/UserColumns/UserText1", "12345678");
            json.setFieldValue("/FirstName", "Chris");
            System.out.println(json);
            
            JacksonJsonContainer json2 = new JacksonJsonContainer("{\"One\":1, \"Two\":2 }");
            System.out.println(json2.toPrettyString());
            json.setFieldValue("/UserColumns/UserText2", json2);
            System.out.println(json.toPrettyString());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
