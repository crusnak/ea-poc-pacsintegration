/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.data;

import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.exception.IndexException;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.vladium.logging.Logger;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author crusnak
 */
public class RelationalContainer implements Serializable, DataSet
{
    public static final String COL_ERROR_MSG =      "DATA_ERROR";

    private static final long serialVersionUID = 2397523420L;

    private String name = null;
    private Map<String,DataColumn> columns = new LinkedHashMap<String, DataColumn>();
    private List<Map<String,DataCell>> rowdata = new ArrayList();
    // TODO: add index support for quick lookup of row data by column
    private Set<DataIndex> indices = new HashSet();
    private DataIndex primaryKey = null;
    private int currentRow = 0;

    /*public DataContainer(String task, DataContainer data)
        throws ETLException
    {
        taskName = task;
        setData(data);
    }*/

    public RelationalContainer(String task)
    {
        name = task;
    }
    
    public RelationalContainer(String task, DataSet copy)
        throws ETLException
    {
        this(task);
        copyDataSet(copy, true, true);
    }

    @Override
    public Type getType()
    {
        return Type.relational;
    }
    
    @Override
    public String getDataName()
    {
        return name;
    }

    public void setDataName(String n)
    {
        name = n;
    }
    
    public void addAllRows(Collection<Map<String,DataCell>> rows)
        throws ETLException
    {
        rowdata.addAll(rows);
        indexAllData();
        /*for ( Map<String,DataCell> row: rows )
        {
            addDataRow(row);
        }*/
    }

    public List<Map<String,DataCell>> getMappedData()
    {
        return rowdata;
    }

    public int rowCount()
    {
        return rowdata.size();
    }

    public int getCurrentProcessedRow()
    {
        return currentRow;
    }
    
    public void setCurrentProcessedRow(int rownum)
    {
        currentRow = rownum;
    }
    
    public List<List<DataCell>> getRowValues()
    {
        ArrayList<List<DataCell>> rows = new ArrayList();
        for ( Map<String,DataCell> map: rowdata )
        {
            ArrayList row = new ArrayList();
            for ( DataCell cell: map.values() )
            {
                row.add(cell.getValue());
            }
            rows.add(row);
        }
        return rows;
    }

    public List<List> getColumnValues(String col)
    {
        ArrayList data = new ArrayList();
        if ( rowdata != null )
        {
            for ( Map<String,DataCell> map: rowdata )
            {
                data.add(map.get(col).getValue());
            }
        }

        return data;
    }

    public Map<String,DataCell> removeRow(int index)
    {
        return rowdata.remove(index);
    }

    public void addDataIndex(DataIndex idx)
    {
        indices.add(idx);
    }
    
    public void removeDataIndex(DataIndex idx)
    {
        indices.remove(idx);
    }

    public void setDataIndices(Set<DataIndex> idxs)
    {
        indices = idxs;
    }

    public Set<DataIndex> getDataIndices()
    {
        return indices;
    }

    public Set<DataIndex> getDataIndicesForColumn(String colname)
    {
        Set<DataIndex> set = new HashSet();
        if ( colname != null )
        {
            for ( DataIndex di: indices )
            {
                if ( di.containsColumn(colname) )
                {
                    set.add(di);
                }
            }
        }

        return set;
    }

    public List<List<String>> getAllIndexedColumnNames()
    {
        ArrayList<List<String>> names = new ArrayList();
        for ( DataIndex index: indices )
        {
            names.add(index.getIndexedColumnNames());
        }
        return names;
    }

    public void addColumnDefinitions(List<DataColumn> cols)
        throws ETLException
    {
        for ( DataColumn col: cols )
        {
            addColumnDefinition(new DataColumn(col.getDataType(), col.getColumnName(), col.isUniqueColumn(), col.getDefaultValue()));
        }
    }
    
    public void addColumnDefinitions(Map<String,DataColumn> cols)
        throws ETLException
    {
        for ( Map.Entry<String,DataColumn> entry: cols.entrySet() )
        {
            DataColumn col = entry.getValue();
            addColumnDefinition(new DataColumn(col.getDataType(), col.getColumnName(), col.isUniqueColumn(), col.getDefaultValue()));
        }
    }

    public DataColumn addColumnDefinition(String type, String name, boolean unique, String def)
        throws ETLException
    {
        DataColumn col = new DataColumn(type, name, unique, def);
        addColumnDefinition(col);
        return col;
    }
    
    public DataColumn addColumnDefinition(String type, String name, String def)
        throws ETLException
    {
        return addColumnDefinition(type, name, false, def);
    }

    public DataColumn addColumnDefinition(DataTypes type, String name, boolean unique, String def)
        throws ETLException
    {
        DataColumn col = new DataColumn(type, name, unique, def);
        addColumnDefinition(col);
        return col;
    }
    
    public DataColumn addColumnDefinition(DataTypes type, String name, String def)
        throws ETLException
    {
        return addColumnDefinition(type, name, false, def);
    }

    public void addColumnDefinition(DataColumn col)
        throws ETLException
    {
        if ( col != null )
        {
            try
            {
                columns.put(col.getColumnName(), col);
                if ( col.isUniqueColumn() )
                {
                    List<String> uniquecols = getUniqueColumnNames();
                    removeDataIndex(primaryKey);
                    primaryKey = new DataIndex(DataIndex.Type.unique, uniquecols);
                    addDataIndex(primaryKey);
                }
            }
            catch (Exception e)
            {
                throw new ETLException("Unable to add column to data container", e);
            }
        }
    }

    public List<String> getColumnNames()
    {
        return new ArrayList(columns.keySet());
    }

    public DataColumn getColumnDefinition(String name)
    {
        return columns.get(name);
    }

    @Override
    public Map<String,DataColumn> getColumnDefinitions()
    {
        return columns;
    }
    
    public int columnCount()
    {
        return columns.size();
    }
    
    public DataColumn getColumnDefinition(int idx)
    {
        String name = getColumnNames().get(idx);
        return columns.get(name);
    }
    
    public boolean containsUniqueColumns()
    {
        return primaryKey != null;
    }
    
    public List<DataColumn> getUniqueColumnDefinitions()
    {
        List<DataColumn> uniquecols = new ArrayList();
        for ( DataColumn dc: columns.values() )
        {
            if ( dc.isUniqueColumn() )
            {
                uniquecols.add(dc);
            }
        }
        return uniquecols;
    }

    public List<String> getUniqueColumnNames()
    {
        List<String> uniquenames = new ArrayList();
        for ( DataColumn dc: columns.values() )
        {
            if ( dc.isUniqueColumn() )
            {
                uniquenames.add(dc.getColumnName());
            }
        }
        return uniquenames;
    }

    @Override
    public void setColumnDefinitions(Map<String,DataColumn> cols)
        throws ETLException
    {
        org.apache.log4j.Logger.getLogger(this.getClass()).trace("Setting column definitions for " + getDataName() + ": " + cols);
        columns.clear();
        addColumnDefinitions(cols);
    }

    @Override
    public void clear()
    {
        clearColumns();
        clearData();
    }
    
    public void clearColumns()
    {
        columns.clear();
    }

    public void clearData()
    {
        rowdata.clear();
        currentRow = 0;
        for ( DataIndex idx: indices )
        {
            idx.clearIndices();
        }
    }

    public void clearIndices()
    {
        indices.clear();
    }

    public void setDataContainer(RelationalContainer data, boolean clearColumns, boolean clearData)
        throws ETLException
    {
        if ( clearColumns )
        {
            columns = data.columns;
        }
        else
        {
            columns.putAll(data.columns);
        }
        
        if ( clearData )
        {
            rowdata = data.rowdata;
        }
        else
        {
            rowdata.addAll(data.rowdata);
        }
    }
    
    @Override
    public void copyDataSet(DataSet data, boolean clearColumns, boolean clearData)
        throws ETLException
    {
        if ( data.getType() != DataSet.Type.relational )
        {
            throw new UnsupportedOperationException("Cannot copy a hierarchical data set into a relational data set");
        }

        org.apache.log4j.Logger.getLogger(this.getClass()).trace("Copying data set: " + data);
        
        RelationalContainer dc = (RelationalContainer)data;
        Iterator<Map.Entry<String,DataColumn>> it = dc.columns.entrySet().iterator();

        if ( clearColumns )
        {
            columns.clear();
        }

        while ( it.hasNext() )
        {
            Map.Entry<String,DataColumn> entry = it.next();
            DataColumn sc = entry.getValue();
            addColumnDefinition(new DataColumn(sc.getDataType(), sc.getColumnName(), sc.isUniqueColumn(), sc.getDefaultValue()));
        }

        if ( clearData )
        {
            rowdata.clear();
        }
        
        for ( Map<String,DataCell> rmap: dc.rowdata )
        {
            HashMap<String,DataCell> newmap = new HashMap();
            Iterator<Map.Entry<String,DataCell>> it2 = rmap.entrySet().iterator();
            while ( it2.hasNext() )
            {
                Map.Entry<String,DataCell> entry = it2.next();
                DataCell cell = entry.getValue();
                DataColumn col = getColumnDefinition(entry.getKey());
         org.apache.log4j.Logger.getLogger(this.getClass()).trace("Copied column: " + col);               
                if ( col == null )
                {
                    col = cell.getColumn();
                }
                newmap.put(entry.getKey(), new DataCell(col, cell.getValue()));
            }
            addDataRow(newmap);
        }

        org.apache.log4j.Logger.getLogger(this.getClass()).trace("Copied: " + this);
    }

    public void setDataRow(Map<String,DataCell> row, int index)
        throws ETLException
    {
        if ( index < rowCount() )
        {
            rowdata.set(index, row);
        }
        else
        {
            addDataRow(row);
        }
    }
    
    public void insertDataRow(Map<String,DataCell> row, int index)
        throws ETLException
    {
        if ( index < rowCount() )
        {
            rowdata.add(index, row);
        }
        else
        {
            addDataRow(row);
        }
    }

    public void addDataRow(Map<String,DataCell> row)
        throws ETLException
    {
        addIndexedData(row, rowCount());
        rowdata.add(row);
    }

    public void updateDataRow(int rownum, Map<String,DataCell> row)
        throws ETLException
    {
        Map<String,DataCell> map = rowdata.get(rownum);
        map.putAll(row);
        removeIndexedData(rownum);
        addIndexedData(row, rownum);
        rowdata.remove(rownum);
        rowdata.add(map);
    }

    public Map<String,DataCell> addQueriedDataRow(Map<String,Object> row)
        throws ETLException
    {
        HashMap<String,DataCell> map = new HashMap();
        Iterator<Map.Entry<String,Object>> it = row.entrySet().iterator();
        while ( it.hasNext() )
        {
            Map.Entry<String,Object> entry = it.next();
            DataColumn col = columns.get(entry.getKey());;
            if ( col == null )
            {
                col = new DataColumn(DataTypes.OBJECT, entry.getKey());
                // TODO: add column to column list since it doesn't exist
            }
            map.put(entry.getKey(), new DataCell(col, entry.getValue()));
        }

        addIndexedData(map, rowCount());
        rowdata.add(map);
        return map;
    }

    public Map<String,DataCell> addFileDataRow(Map<String,String> row)
        throws ETLException
    {
        HashMap<String,DataCell> map = new HashMap();
        Iterator<Map.Entry<String,String>> it = row.entrySet().iterator();
        while ( it.hasNext() )
        {
            Map.Entry<String,String> entry = it.next();
            DataColumn col = columns.get(entry.getKey());;
            if ( col == null )
            {
                col = new DataColumn(DataTypes.STRING, entry.getKey());
                // TODO: add column to column list since it doesn't exist
            }
            map.put(entry.getKey(), new DataCell(col, entry.getValue()));
        }

        addIndexedData(map, rowCount());
        rowdata.add(map);
        return map;
    }

    public Map<String,DataCell> updateQueriedDataRow(int rownum, Map<String,Object> row)
        throws ETLException
    {
        Map<String,DataCell> rowmap = rowdata.get(rownum);
        Iterator<Map.Entry<String,Object>> it = row.entrySet().iterator();
        while ( it.hasNext() )
        {
            Map.Entry<String,Object> entry = it.next();
            DataColumn col = columns.get(entry.getKey());;
            if ( col == null )
            {
                col = new DataColumn(DataTypes.OBJECT, entry.getKey());
                addColumnDefinition(col);
            }
            rowmap.put(entry.getKey(), new DataCell(col, entry.getValue()));
        }
        removeIndexedData(rownum);
        addIndexedData(rowmap, rownum);
        return rowmap;
    }

    public void setQueriedData(List<Map<String,Object>> rows)
        throws ETLException
    {
        rowdata.clear();
        for ( Map<String,Object> row: rows )
        {
            addQueriedDataRow(row);
        }
    }

    public void setData(List<Map<String,DataCell>> rows)
        throws ETLException
    {
        rowdata.clear();
        for ( Map<String,DataCell> row: rows )
        {
            addDataRow(row);
        }
    }

    public void setColumnsFromRowData(Map<String,DataCell> row)
        throws ETLException
    {
        columns.clear();
        addColumnsFromRowData(row);
    }

    public void addColumnsFromRowData(Map<String,DataCell> row)
        throws ETLException
    {
        for ( Map.Entry<String,DataCell> entry: row.entrySet() )
        {
            String colname = entry.getKey();
            DataCell cell = entry.getValue();
            if ( !columns.containsKey(colname) )
            {
                addColumnDefinition(cell.getColumn());
            }
        }
    }

    public void indexAllData()
        throws IndexException
    {
        // Clear all existing indices since we are going to add them all back
        for ( DataIndex di: indices )
        {
            di.clearIndices();
        }

        // Reindex all data
        for ( int i=0; i<rowCount(); i++ )
        {
            Map<String,DataCell> row = rowdata.get(i);
            addIndexedData(row, i);
        }
    }

    private void addIndexedData(Map<String,DataCell> rowdata, int rownum)
        throws IndexException
    {
        for ( DataIndex di: indices )
        {
            ArrayList vals = new ArrayList();
            for ( String colname: di.getIndexedColumnNames() )
            {
                Object val = null;
                if ( rowdata.containsKey(colname) )
                {
                    val = rowdata.get(colname).getValue();
                }
                vals.add(val);
            }
            di.addIndex(vals, rownum);
        }
    }

    private void removeIndexedData(int rownum)
    {
        for ( DataIndex di: indices )
        {
            di.removeIndex(rownum);
        }
    }

    public String toString()
    {
        StringBuffer out = new StringBuffer();
        out.append("{");
        out.append("Name: " + name);
        out.append(", Columns: " + columns);
        out.append(", IndexedColumns: " + getAllIndexedColumnNames());
        out.append(", RowCount: " + rowCount());
        out.append(", Data: " + rowdata);
        out.append("}");
        return out.toString();
    }
}
