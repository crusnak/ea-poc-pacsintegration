/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl;

import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.security.object.NamedKey;
import com.blackboard.services.utils.BaseLogger;
import com.blackboard.services.utils.BbBaseProperties;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import org.apache.log4j.Logger;

/**
 *
 * @author crusnak
 */
public class ETLConfiguration extends BbBaseProperties
{
    public static final String propertyPath =           "resources/etl.properties";
    
    // These were moved to the EmailConnection secured object
    public static final String PROP_SMTP_HOST =         "mail.smtp.host";
    public static final String PROP_SMTP_PORT =         "mail.smtp.port";
    public static final String PROP_MAIL_USER =         "mail.smtp.user";
    public static final String PROP_MAIL_PASS =         "mail.smtp.pass";
    public static final String PROP_MAIL_TLS =          "mail.smtp.starttls.enable";
    public static final String PROP_MAIL_PART =         "mail.smtp.sendpartial";
    public static final String PROP_MAIL_SSL =          "mail.smtp.ssl.enable";
    
    public static final String PROP_CONN_APP_NAME =     "mail.connect.appName";
    public static final String PROP_CONN_KEY =          "mail.connect.key";
    
    public static final String PROP_MAIL_FROM =         "mail.errors.sender";
    public static final String PROP_MAIL_TO =           "mail.errors.recipients";
    public static final String PROP_MAIL_FATAL_ERRORS = "mail.errors.fatal";
    public static final String PROP_FATAL_ATTACH =      "mail.errors.fatal.attachLog";
    public static final String PROP_FATAL_SUBJECT =     "mail.errors.fatal.subject";
    public static final String PROP_FATAL_BODY =        "mail.errors.fatal.body";
    public static final String PROP_MAIL_ROW_ERRORS =   "mail.errors.row";
    public static final String PROP_ROW_ATTACH =        "mail.errors.row.attachData";
    public static final String PROP_ROW_SUBJECT =       "mail.errors.row.subject";
    public static final String PROP_ROW_BODY =          "mail.errors.row.body";
    public static final String PROP_MAIL_JOB_INFO =     "mail.job.info";
    public static final String PROP_INFO_SEND_EMPTY =   "mail.job.info.sendEmpty";
    public static final String PROP_INFO_ATTACH_LOG =   "mail.job.info.attachLog";
    public static final String PROP_INFO_ATTACH_STATS = "mail.job.info.attachStats";
    public static final String PROP_INFO_SUBJECT =      "mail.job.info.subject";
    public static final String PROP_INFO_BODY =         "mail.job.info.body";
    
    private static Logger logger = null;
    private static ETLConfiguration instance = null;

    private ETLConfiguration(File root)
        throws IOException
    {
        super(null, root, propertyPath);
        logger = Logger.getLogger(ETLConfiguration.class);
        logger.debug("ETLConfiguration app root: " + appRoot);
        logger.debug("ETLConfiguration loaded: " + propertyURL);
        if ( propertyURL != null )
        {
            logger.debug("ETLConfiguration path: " + propertyURL.getFile());
        }
    }

    public static ETLConfiguration getConfiguration()
        throws ETLException
    {
        if ( instance == null )
        {
            throw new ETLException("ETLConfiguration not properly initialized. Please use " +
                                   "ETLConfiguration.initialize(File).");
        }
        return instance;
    }

    public static ETLConfiguration initialize(File root)
        throws IOException
    {
        BaseLogger.info("Retrieving ETLConfiguration from: " + propertyPath);
        if ( instance == null )
        {
            instance = new ETLConfiguration(root);
        }
        return instance;
    }

    public boolean isEmailSentForFatalErrors()
    {
        return Boolean.valueOf(getProperty(PROP_MAIL_FATAL_ERRORS));
    }

    public void setEmailSentForFatalErrors(boolean fatal)
    {
        setProperty(PROP_MAIL_FATAL_ERRORS, String.valueOf(fatal));
    }

    public boolean isFatalErrorLogFileAttached()
    {
        return Boolean.valueOf(getProperty(PROP_FATAL_ATTACH));
    }

    public void setFatalErrorLogFileAttached(boolean attach)
    {
        setProperty(PROP_FATAL_ATTACH, String.valueOf(attach));
    }

    public String getFatalErrorEmailSubject()
    {
        return getProperty(PROP_FATAL_SUBJECT);
    }
    
    public void setFatalErrorEmailSubject(String subject)
    {
        setProperty(PROP_FATAL_SUBJECT, subject);
    }
    
    public String getFatalErrorEmailBody()
    {
        return getProperty(PROP_FATAL_BODY);
    }
    
    public void setFatalErrorEmailBody(String body)
    {
        setProperty(PROP_FATAL_BODY, body);
    }

    public boolean isEmailSentForRowErrors()
    {
        return Boolean.valueOf(getProperty(PROP_MAIL_ROW_ERRORS));
    }

    public void setEmailSentForRowErrors(boolean row)
    {
        setProperty(PROP_MAIL_ROW_ERRORS, String.valueOf(row));
    }
    
    public boolean isRowErrorDataAttached()
    {
        return Boolean.valueOf(getProperty(PROP_ROW_ATTACH));
    }

    public void setRowErrorDataAttached(boolean attach)
    {
        setProperty(PROP_ROW_ATTACH, String.valueOf(attach));
    }
    
    public String getRowErrorEmailSubject()
    {
        return getProperty(PROP_ROW_SUBJECT);
    }
    
    public void setRowErrorEmailSubject(String subject)
    {
        setProperty(PROP_ROW_SUBJECT, subject);
    }
    
    public String getRowErrorEmailBody()
    {
        return getProperty(PROP_ROW_BODY);
    }
    
    public void setRowErrorEmailBody(String body)
    {
        setProperty(PROP_ROW_BODY, body);
    }

    public boolean isEmailSentForJobInfo()
    {
        return Boolean.valueOf(getProperty(PROP_MAIL_JOB_INFO));
    }

    public void setEmailSentForJobInfo(boolean job)
    {
        setProperty(PROP_MAIL_JOB_INFO, String.valueOf(job));
    }
    
    public boolean isJobInfoLogFileAttached()
    {
        return Boolean.valueOf(getProperty(PROP_INFO_ATTACH_LOG));
    }

    public void setJobInfoLogFileAttached(boolean attach)
    {
        setProperty(PROP_INFO_ATTACH_LOG, String.valueOf(attach));
    }
    
    public boolean isJobInfoSendEmailEmpty()
    {
        return Boolean.valueOf(getProperty(PROP_INFO_SEND_EMPTY));
    }

    public void setJobInfoSendEmailEmpty(boolean send)
    {
        setProperty(PROP_INFO_SEND_EMPTY, String.valueOf(send));
    }

    public boolean isJobInfoStatisticsFileAttached()
    {
        return Boolean.valueOf(getProperty(PROP_INFO_ATTACH_STATS));
    }

    public void setJobInfoStatisticsFileAttached(boolean attach)
    {
        setProperty(PROP_INFO_ATTACH_STATS, String.valueOf(attach));
    }

    public String getJobInfoEmailSubject()
    {
        return getProperty(PROP_INFO_SUBJECT);
    }
    
    public void setJobInfoEmailSubject(String subject)
    {
        setProperty(PROP_INFO_SUBJECT, subject);
    }
    
    public String getJobInfoEmailBody()
    {
        return getProperty(PROP_INFO_BODY);
    }
    
    public void setJobInfoEmailBody(String body)
    {
        setProperty(PROP_INFO_BODY, body);
    }

    public InternetAddress getEmailAddressSender()
    {
        InternetAddress from = null;
        try
        {
            from = new InternetAddress(getEmailAddressSenderString());
        }
        catch (AddressException e)
        {
            logger.warn("Unable parse email address sender for sending errors. Make sure the property is set in the etl.properties file.", e);
        }
        return from;
    }

    public String getEmailAddressSenderString()
    {
        return getProperty(PROP_MAIL_FROM);
    }

    public void setEmailAddressSender(InternetAddress addr)
    {
        if ( addr != null )
        {
            setEmailAddressSenderString(addr.getAddress());
        }
    }

    public void setEmailAddressSenderString(String from)
    {
        setProperty(PROP_MAIL_FROM, from);
    }

    public List<InternetAddress> getEmailAddressRecipients()
    {
        ArrayList<InternetAddress> recipients = new ArrayList();
        String toString = getEmailAddressRecipientString();
        if ( toString != null )
        {
            if ( toString.contains(",") || toString.contains(";") )
            {
                for ( String addr: toString.split(",|;") )
                {
                    try
                    {
                        recipients.add(new InternetAddress(addr));
                    }
                    catch (AddressException e)
                    {
                        logger.warn("Unable to parse email address recipient, not adding to recipient list: " + addr, e);
                    }
                }
            }
            else
            {
                try
                {
                    recipients.add(new InternetAddress(toString));
                }
                catch (AddressException e)
                {
                    logger.warn("Unable to parse email address recipient, not adding to recipient list: " + toString, e);
                }
            }
        }

        return recipients;
    }

    public String getEmailAddressRecipientString()
    {
        return getProperty(PROP_MAIL_TO);
    }

    public void setEmailAddressRecipients(List<InternetAddress> recipients)
    {
        String addresses = "";
        if ( recipients != null )
        {
            Iterator<InternetAddress> it = recipients.iterator();
            while ( it.hasNext() )
            {
                InternetAddress addr = it.next();
                addresses += addr.getAddress();
                if ( it.hasNext() )
                {
                    addresses += ",";
                }
            }

            setEmailAddressRecipientString(addresses);
        }
    }
    
    public void setEmailAddressRecipientString(String recipients)
    {
        setProperty(PROP_MAIL_TO, recipients);
    }

    public String getSmtpHost()
    {
        return getProperty(PROP_SMTP_HOST);
    }

    public void setSmtpHost(String host)
    {
        setProperty(PROP_SMTP_HOST, host);
    }

    public int getSmtpPort()
    {
        int port = 25;

        try
        {
            port = Integer.parseInt(getProperty(PROP_SMTP_PORT));
        }
        catch (NumberFormatException e) {}

        return port;
    }

    public void setSmtpPort(int port)
    {
        setProperty(PROP_SMTP_PORT, String.valueOf(port));
    }

    public String getMailUsername()
    {
        return getProperty(PROP_MAIL_USER);
    }

    public void setMailUsername(String user)
    {
        setProperty(PROP_MAIL_USER, user);
    }

    public String getMailPassword()
    {
        return getProperty(PROP_MAIL_PASS);
    }

    public void setMailPassword(String pass)
    {
        setProperty(PROP_MAIL_PASS, pass);
    }

    public boolean isStartTLSEnabled()
    {
        return Boolean.valueOf(getProperty(PROP_MAIL_TLS));
    }

    public void setStartTLSEnabled(boolean tls)
    {
        setProperty(PROP_MAIL_TLS, String.valueOf(tls));
    }

    public boolean isSendPartial()
    {
        return Boolean.valueOf(getProperty(PROP_MAIL_PART));
    }

    public void setSendPartial(boolean part)
    {
        setProperty(PROP_MAIL_PART, String.valueOf(part));
    }

    public boolean isSSLEnabled()
    {
        return Boolean.valueOf(getProperty(PROP_MAIL_SSL));
    }

    public void setSSLEnabled(boolean ssl)
    {
        setProperty(PROP_MAIL_SSL, String.valueOf(ssl));
    }

    public NamedKey getEmailConnectionKey()
    {
        NamedKey nk = null;
        String app = getProperty(PROP_CONN_APP_NAME);
        String key = getProperty(PROP_CONN_KEY);
        
        if ( app != null && app.trim().length() > 0 &&
             key != null && key.trim().length() > 0 )
        {
            nk = new NamedKey(app, key);
        }
        
        return nk;
    }
    
    public void setEmailConnectionKey(NamedKey key)
    {
        if ( key != null )
        {
            setProperty(PROP_CONN_APP_NAME, key.getName());
            setProperty(PROP_CONN_KEY, key.getKey());
        }
        else
        {
            remove(PROP_CONN_APP_NAME);
            remove(PROP_CONN_KEY);
        }
    }
}
