/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.tasks;

import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.EtlTaskComplexType;
import com.blackboard.services.etl.jaxb.WebAPITaskComplexType;
import com.blackboard.services.etl.jaxb.WebConfigComplexType;
import com.blackboard.services.etl.jaxb.XpathComplexType;
import com.blackboard.services.etl.tasks.web.AbstractDriver;
import com.blackboard.services.etl.tasks.web.WebAPIDriver;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.blackboard.services.security.object.WebConnection;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author crusnak
 */
public class WebAPIDispatcher extends AbstractTask
{
    public static enum JoinType { none, outer, inner };

    public static final String COL_HTTP_RESPONSE =      "HTTP_RESPONSE";
    public static final String COL_API_ERR =            "API_ERROR";
    public static final String COL_AP_REQUEST =         "API_REQUEST";

    private RelationalContainer data = null;
    private WebAPIDriver driver = null;
    private WebConfigComplexType wc = null;
    private List<DataColumn> declaredColumns = new ArrayList();
    
    public WebAPIDispatcher(EtlTaskComplexType ct, WebAPITaskComplexType xml, ETLJobWrapper w)
        throws ETLException
    {
        super(ct, w);
        setData(dataSet);

        driver = AbstractDriver.getDriver(xml, this); 
        wc = xml.getConnectionInfo();
        
        setResponseColumnDefinitions(xml.getHttp().getResponse());
        //setMappingType(map.getType());
        //setTransforms(map.getAssignOrDateOrLookup());
    }
    
    
    @Override
    public ClassType getClassType()
    {
        return ClassType.WebAPIDispatcher;
    }
    
    @Override
    public Object getBaseObject()
    {
        return driver.getBaseObject();
    }
    
    public WebAPIDriver.DriverType getDriverType()
    {
        return driver.getDriverType();
    }

    public WebAPIDriver.RequestMethod getRequestMethod()
    {
        return driver.getRequestMethod();
    }

    public WebAPIDriver.ContentType getContentType()
    {
        return driver.getContentType();
    }
    
    private void setResponseColumnDefinitions(WebAPITaskComplexType.Http.Response input)
        throws ETLException
    {
        if ( input != null )
        {
            for ( XpathComplexType ct: input.getDataElement() )
            {
                DataColumn col = data.addColumnDefinition(String.valueOf(ct.getType()), ct.getValue().toUpperCase(), ct.isPrimaryKey(), ct.getDefaultValue());
                declaredColumns.add(col);
            }
        }
    }
    
    
    public RelationalContainer execute()
        throws ETLException
    {
        DataColumn ecol = new DataColumn(DataTypes.STRING, COL_API_ERR);
        DataColumn scol = new DataColumn(DataTypes.STRING, COL_AP_REQUEST);
        DataColumn rcol = new DataColumn(DataTypes.STRING, COL_HTTP_RESPONSE);
        RelationalContainer errors = new RelationalContainer(data.getDataName() + ERROR_DATA_NAME);
        errors.setColumnDefinitions(data.getColumnDefinitions());
        errors.addColumnDefinition(ecol);
        errors.addColumnDefinition(scol);
        
        switch ( driver.getContentType() )
        {
            case application_json:
            case text_json:
                rcol = new DataColumn(DataTypes.JSON, COL_HTTP_RESPONSE);
                break;
            case application_xml:
            case text_xml:
                rcol = new DataColumn(DataTypes.XML, COL_HTTP_RESPONSE);
                break;
            default:
                break;
        }

        // Add all declared columns to the data set
        for ( DataColumn col: declaredColumns )
        {
            DataColumn dc = data.getColumnDefinition(col.getColumnName());
            if ( dc == null )
            {
                data.addColumnDefinition(col);
            }
        }
        
        data.addColumnDefinition(rcol);

        // Set the connection info at runtime (to support dynamic global variable modification)
        driver.setConnectionInfo(wc);

        try
        {
            int total = data.rowCount();
            Date start = new Date();

            Iterator<Map<String,DataCell>> it = data.getMappedData().iterator();
            List<Map<String,DataCell>> newdata = new ArrayList();
            Map<String,DataCell> map = null;
            for ( int i=1; it.hasNext(); i++ )
            {
                try
                {
                    data.setCurrentProcessedRow(i);
                    System.out.println(getTaskName() + ": Executing web api row #" + i + " out of " + total);
                    map = it.next();
                    start = new Date();

                    logger.debug("Processing web api request on row #" + i);
                    List<Map<String,DataCell>> response = driver.execute(map, true);
                    logger.debug("Web api request took " + (double)(new Date().getTime() - start.getTime())/1000.0 + " seconds to process.");
                    logger.debug("Web api response: " + response);

                    if ( response.size() > 0 )
                    {
                        // Add a copy of the current row to each returned row
                        for ( Map<String,DataCell> row: response )
                        {
                            Map<String,DataCell> newmap = new LinkedHashMap(map);
                            newmap.putAll(row);
                            newdata.add(newmap);
                        }
                    }
                    else
                    {
                        switch (driver.getJoinType())
                        {
                            case inner:
                                logger.debug("No rows returned, inner-join: removing referenced row data");
                                it.remove();
                                break;
                            case outer:
                                logger.debug("No rows returned, outer-join: assigning null to columns");
                                /*for ( DataColumn c: driver.getResponseColumns() )
                                {
                                    DataColumn dc = data.getColumnDefinition(c.getColumnName());
                                    if ( dc == null )
                                    {
                                        data.addColumnDefinition(col);
                                    }
                                    DataCell cell = new DataCell(col, null);
                                    map.put(col.getColumnName(), cell);
                                }*/
                                newdata.add(map);
                                break;
                        }
                    }
                }
                catch (ETLException e)
                {
                    logger.debug(e.getMessage(), e);
                    it.remove();
                    map.put(COL_API_ERR, new DataCell(ecol, e.getMessage()));
                    map.put(COL_AP_REQUEST, new DataCell(scol, "Headers=" + driver.getHeaders() + ", Parameters=" + driver.getParameters()));
                    newdata.add(map);
                    errors.addDataRow(map);
                }
            }
            
            // Add the updated data to the data container. This is necessary since more than one row
            // could be returned on a join so we cannot modify the current iterator.
            data.setData(newdata);

        }
        finally
        {
            try
            {
                driver.closeConnection();
            }
            catch (IOException e)
            {
                logger.warn("Unable to close http connection", e);
            }
        }

        return errors;
    }
    
    public WebAPIDriver getDriver()
    {
        return driver;
    }

    @Override
    public RelationalContainer getData()
        throws ETLException
    {
        setData(super.getData());
        return data;
    }

    @Override
    public void setData(DataSet d)
    {
        super.setData(d);
        if ( d.getType() != DataSet.Type.relational )
        {
            throw new UnsupportedOperationException("DataSet assigned to " + this.getClass().getSimpleName() + " must be relational");
        }
        data = (RelationalContainer)d;
    }
}
