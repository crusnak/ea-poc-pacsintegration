/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.tasks;

import com.blackboard.services.etl.data.Argument;
import com.blackboard.services.etl.data.BinaryObject;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.jaxb.ArgumentsComplexType;
import com.blackboard.services.etl.jaxb.DataComplexType;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.DatabaseInputTaskComplexType;
import com.blackboard.services.etl.jaxb.DatabaseOutputTaskComplexType;
import com.blackboard.services.etl.jaxb.EtlTaskComplexType;
import com.blackboard.services.etl.jaxb.InputTaskComplexType;
import com.blackboard.services.etl.jaxb.OutputTaskComplexType;
import com.blackboard.services.etl.wrapper.ConnectionFactory;
import com.blackboard.services.etl.wrapper.DataArchiver;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.blackboard.services.query.ExtractableQuery;
import com.blackboard.services.query.Query;
import com.blackboard.services.security.object.DBConnection;
import com.blackboard.services.security.object.NamedKey;
import java.io.File;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import oracle.jdbc.OracleTypes;
import oracle.sql.TIMESTAMPTZ;

/**
 *
 * @author crusnak
 */
public class DatabaseAdapter extends AbstractAdapter
{
    public static enum QueryType { select, storedprocedure };
    public static enum JoinType { none, outer, inner };
    public static enum StatementType { statement, storedprocedure };

    public static final String COL_DATABASE_ERR =       "DATABASE_ERROR";
    public static final String COL_STATEMENT_MSG =      "DATABASE_STATEMENT";
    private Object da = null;
    private RelationalContainer data = null;
    private DatabaseInputTaskComplexType.ConnectionInfo ici = null;
    private DatabaseOutputTaskComplexType.ConnectionInfo oci = null;
    private DBConnection dbinfo = null;
    private Query query = null;
    private QueryType qtype = QueryType.select;
    private JoinType jtype = JoinType.none;
    private StatementType stype = StatementType.statement;
    private List<Argument> arguments = new ArrayList();
    private List<DataColumn> declaredColumns = new ArrayList();
    private boolean includeAll = false;
    private boolean usesSecman = false;
    private boolean ignoreZeroExecuted = false;
    private boolean useAllOrderedArguments = false;
    private boolean inline = false; // used to call this task outside of etl context

    public DatabaseAdapter(EtlTaskComplexType ct, InputTaskComplexType xml, ETLJobWrapper w)
        throws ETLException
    {
        super(ct, xml, w);
        setData(dataSet);

        if ( xml.getDatabaseInput() == null )
        {
            throw new ETLException("Expecting DataBaseInput in InputAdapter");
        }

        DatabaseInputTaskComplexType in = xml.getDatabaseInput();
        da = in;
        ici = in.getConnectionInfo();
        //setConnectionInfo(xml.getConnectionInfo());
        setExtractableQuery(in);
    }

    public DatabaseAdapter(EtlTaskComplexType ct, OutputTaskComplexType xml, ETLJobWrapper w)
        throws ETLException
    {
        super(ct, xml, w);
        setData(dataSet);

        if ( xml.getDatabaseOutput() == null )
        {
            throw new ETLException("Expecting DataBaseOutput in OutputAdapter");
        }

        DatabaseOutputTaskComplexType out = xml.getDatabaseOutput();
        da = out;
        oci = out.getConnectionInfo();
        //setConnectionInfo(xml.getConnectionInfo());
        setStatement(out.getStatement(), out.getArguments());
    }

    public DatabaseAdapter(String n, DataDirection d)
    {
        name = n;
        inline = true;
        direction = d;
        setData(new RelationalContainer(""));
    }
    
    public Object getBaseObject()
    {
        return da;
    }

    public Adapter.Type getAdapterType()
    {
        return Adapter.Type.Database;
    }

    @Override
    public RelationalContainer getData()
        throws ETLException
    {
        setData(super.getData());
        return data;
    }

    @Override
    public void setData(DataSet d)
    {
        super.setData(d);
        if ( d.getType() != DataSet.Type.relational )
        {
            throw new UnsupportedOperationException("DataSet assigned to " + this.getClass().getSimpleName() + " must be relational");
        }
        data = (RelationalContainer)d;
    }

    public DBConnection getConnectionInfo()
    {
        return dbinfo;
    }

    public boolean usesSecurityManager()
    {
        return usesSecman;
    }

    /*public List<Map<String,Object>> getRowData()
    {
        return rowdata;
    }*/

    private void setConnectionInfo(DatabaseInputTaskComplexType.ConnectionInfo info)
        throws ETLException
    {
        usesSecman = info.isUseSecurityManager();
        dbinfo = ConnectionFactory.getDatabaseConnection(info, this);
    }

    private void setConnectionInfo(DatabaseOutputTaskComplexType.ConnectionInfo info)
        throws ETLException
    {
        usesSecman = info.isUseSecurityManager();
        dbinfo = ConnectionFactory.getDatabaseConnection(info, this);
    }
    
    public void setConnectionInfo(String app, String key, String rootDir)
        throws ETLException
    {
        setConnectionInfo(new NamedKey(app, key), rootDir);
    }
    
    public void setConnectionInfo(NamedKey nk, String rootDir)
        throws ETLException
    {
        dbinfo = ConnectionFactory.getDatabaseConnection(nk, new File(rootDir));
        /*usesSecman = true;
        if ( direction == DataDirection.Incoming )
        {
            DatabaseInputTaskComplexType.ConnectionInfo.DbConnection dbc = new DatabaseInputTaskComplexType.ConnectionInfo.DbConnection();
            dbc.setApplication(nk.getName());
            dbc.setKey(nk.getKey());
            
            ici = new DatabaseInputTaskComplexType.ConnectionInfo();
            ici.setDbConnection(dbc);
        }
        else if ( direction == DataDirection.Outgoing )
        {
            DatabaseOutputTaskComplexType.ConnectionInfo.DbConnection dbc = new DatabaseOutputTaskComplexType.ConnectionInfo.DbConnection();
            dbc.setApplication(nk.getName());
            dbc.setKey(nk.getKey());
            
            oci = new DatabaseOutputTaskComplexType.ConnectionInfo();
            oci.setDbConnection(dbc);
        }*/
    }

    public ExtractableQuery getExtractableQuery()
    {
        return (ExtractableQuery)query;
    }

    public Query getStatement()
    {
        return query;
    }
    
    public void setExtractableQuery(String statement, QueryType qt, JoinType jt)
        throws ETLException
    {
        query = new ExtractableQuery();
        query.setStatement(statement);
        qtype = qt;
        jtype = jt;
        
        includeAll = true;
        ((ExtractableQuery)query).addAllColumnsToExtractFromStatement();
        List<String> columns = ((ExtractableQuery)query).getExtractColumns();
        for ( String cname: columns )
        {
            DataColumn col = data.addColumnDefinition(String.valueOf(DataTypes.STRING), cname.toUpperCase(), null);
            declaredColumns.add(col);
        }
    }

    private void setExtractableQuery(DatabaseInputTaskComplexType ct)
        throws ETLException
    {
        if ( ct != null )
        {
            query = new ExtractableQuery();
            if ( ct.getQuery() != null )
            {
                query.setStatement(ct.getQuery().getValue());
                if ( "innerjoin".equals(ct.getQuery().getType()) )
                {
                    qtype = QueryType.select;
                    jtype = JoinType.inner;
                }
                else if ( "outerjoin".equals(ct.getQuery().getType()) )
                {
                    qtype = QueryType.select;
                    jtype = JoinType.outer;
                }
                else
                {
                    qtype = QueryType.valueOf(ct.getQuery().getType());
                    jtype = JoinType.valueOf(ct.getQuery().getJoinType());
                }
            }

            DatabaseInputTaskComplexType.InputData input = ct.getInputData();
            if ( input != null && input.isIncludeAllColumns() )
            {
                includeAll = input.isIncludeAllColumns();
                ((ExtractableQuery)query).addAllColumnsToExtractFromStatement();
                List<String> columns = ((ExtractableQuery)query).getExtractColumns();
                for ( String cname: columns )
                {
                    DataColumn col = data.addColumnDefinition(String.valueOf(DataTypes.STRING), cname.toUpperCase(), null);
                    declaredColumns.add(col);
                }
            }
            else
            {
                setInputColumnDefinitions(input);
            }

            setArguments(ct.getArguments());
        }
    }
    
    public void setStatement(String s, StatementType st)
    {
        query = new Query();
        query.setStatement(s);
        stype = st;
        ignoreZeroExecuted = false;
    }

    private void setStatement(DatabaseOutputTaskComplexType.Statement s, DatabaseOutputTaskComplexType.Arguments a)
        throws ETLException
    {
        query = new Query();
        query.setStatement(s.getValue());
        stype = StatementType.valueOf(s.getType());
        ignoreZeroExecuted = s.isIgnoreZeroExecuted();
        useAllOrderedArguments = a != null && a.isUseOrderedColumns();
        setArguments(a);
    }

    private void setArguments(ArgumentsComplexType act)
        throws ETLException
    {
        arguments.clear();
        if ( act != null )
        {
            setArguments(act.getArgument());
        }
    }

    private void setArguments(List<ArgumentsComplexType.Argument> args)
        throws ETLException
    {
        arguments.clear();
        for ( ArgumentsComplexType.Argument arg: args )
        {
            arguments.add(new Argument(arg, this));
        }
    }
    
    public void addArgument(Argument arg)
    {
        arguments.add(arg);
    }

    private void setInputColumnDefinitions(DatabaseInputTaskComplexType.InputData input)
        throws ETLException
    {
        if ( input != null )
        {
            includeAll = input.isIncludeAllColumns();
            for ( DatabaseInputTaskComplexType.InputData.DataElement ct: input.getDataElement() )
            {
                DataColumn col = data.addColumnDefinition(String.valueOf(ct.getType()), ct.getValue().toUpperCase(), ct.isPrimaryKey(), ct.getDefaultValue());
                declaredColumns.add(col);
            }
        }
    }

    /*private void setColumnDefinitions(List<DataComplexType> elements)
        throws ETLException
    {
        for ( DataComplexType ct: elements )
        {
            DataColumn col = data.addColumnDefinition(String.valueOf(ct.getType()), ct.getValue(), ct.getSchema());
            declaredColumns.add(col);
        }
    }*/

    private Connection initiateJDBCConnection()
        throws ETLException
    {
        Connection conn = null;
        Date start = new Date();
        try
        {
            Class.forName(dbinfo.getJdbcDriver());
            conn = DriverManager.getConnection(dbinfo.getConnectionURL(), dbinfo.getUsername(), dbinfo.getPassword());
        }
        catch (ClassNotFoundException e)
        {
            throw new ETLException("Cannot find database driver: " + dbinfo.getJdbcDriver(), e);
        }
        catch (SQLException e)
        {
            throw new ETLException("Unable to connect to database: " + dbinfo.getConnectionURL(), e);
        }
        finally
        {
            logger.debug("initiateJDBCConnection() took " + (double)(new Date().getTime() - start.getTime())/1000.0 +
                         " seconds to process.");

        }
        return conn;
    }

    public RelationalContainer executeQuery()
        throws ETLException
    {
        DataColumn ecol = new DataColumn(DataTypes.STRING, COL_DATABASE_ERR);
        DataColumn scol = new DataColumn(DataTypes.STRING, COL_STATEMENT_MSG);
        RelationalContainer errors = new RelationalContainer(data.getDataName() + ERROR_DATA_NAME);
        errors.setColumnDefinitions(data.getColumnDefinitions());
        errors.addColumnDefinition(ecol);
        errors.addColumnDefinition(scol);

        if ( !inline )
        {
            // Set the connection info at runtime (to support dynamic global variable modification)
            setConnectionInfo(ici);

            // Process any embedded bean script to update with global variable values
            DataArchiver da = wrapper.getDataArchiver();
            query.setStatement(da.processEmbeddedBeanScript(query.getStatement()));
        }

        Connection conn = null;
        PreparedStatement ps = null;
        CallableStatement cs = null;
        Date now = new Date();

        try
        {
            conn = initiateJDBCConnection();
            logger.debug("Query has following arguments: " + arguments);
            int total = data.rowCount();
            switch (qtype)
            {
                case select:
                    ps = conn.prepareStatement(query.getStatement());
                    switch (jtype)
                    {
                        case none:
                            String statement = query.getTrimmedStatement();
                            for ( int j=1; j<=arguments.size(); j++ )
                            {
                                Argument arg = arguments.get(j-1);
                                String argname = "";
                                Object val = null;
                                if ( arg.getArgumentType() == Argument.Type.column )
                                {
                                    throw new ETLException("Column arguments not supported in select queries, only joins.");
                                }
                                else if ( arg.getArgumentType() == Argument.Type.output )
                                {
                                    throw new ETLException("Output arguments not supported in select queries, only for stored procedure calls.");
                                }
                                else
                                {
                                    argname = arg.getColumn().getColumnName();
                                    val = arg.getValue();
                                }

                                logger.debug("Argument #" + j + " '" + argname + "'=" + val + (val!=null?"(" + val.getClass().getName() + ")":""));
                                statement = updateStatementString(statement, "[?]", String.valueOf(val));
                                if ( val instanceof Date )
                                {
                                    val = new Timestamp(((Date)val).getTime());
                                }
                                ps.setObject(j, val);
                            }

                            ResultSet rs = ps.executeQuery();
                            logger.info("Query '" + statement + "' took " + (new Date().getTime()-now.getTime()) + " ms");
                            List<Map<String,Object>> rowdata = processResultSet(conn, rs, true);
                            logger.trace("Rowdata: " + rowdata);
                            data.clearData();

                            // If including all, add the columns before processing each row in the result set
                            if ( includeAll )
                            {
                                Map<String,DataTypes> types = extractDataTypes(rs);
                                logger.trace("DataTypes: " + types);

                                for ( Map.Entry<String,DataTypes> entry: types.entrySet() )
                                {
                                    String colname = entry.getKey();
                                    DataTypes type = entry.getValue();
                                    DataColumn col = data.getColumnDefinition(colname);
                                    if ( col == null )
                                    {
                                        col = new DataColumn(type, colname);
                                        data.addColumnDefinition(col);
                                    }
                                }
                            }

                            logger.debug(getTaskName() + ": Creating data objects for " + rowdata.size() + " rows");
                            for ( int i=1; i<=rowdata.size(); i++ )
                            {
                                System.out.println(getTaskName() + ": Creating data objects for row #" + i + " of " + rowdata.size());
                                data.setCurrentProcessedRow(i);
                                Map<String,Object> row = rowdata.get(i-1);
                                HashMap<String,DataCell> map = new HashMap();
                                for ( DataColumn col: declaredColumns )
                                {
                                    DataColumn dc = data.getColumnDefinition(col.getColumnName());
                                    if ( dc == null )
                                    {
                                        data.addColumnDefinition(col);
                                    }
                                    DataCell cell = new DataCell(col, row.get(col.getColumnName()));
                                    map.put(col.getColumnName(), cell);
                                }

                                // Add remaining columns, setting data type to String for undeclared types
                                if ( includeAll )
                                {
                                    for ( Map.Entry<String,Object> entry: row.entrySet() )
                                    {
                                        String colname = entry.getKey();
                                        Object val = entry.getValue();
                                        DataColumn col = data.getColumnDefinition(colname);
                                        if ( col == null )
                                        {
                                            col = new DataColumn(DataTypes.STRING, String.valueOf(colname));
                                            data.addColumnDefinition(col);
                                        }

                                        DataCell cell = new DataCell(col, val);
                                        map.put(colname, cell);
                                    }
                                }

                                data.addDataRow(map);
                            }

                            logger.debug("Assigned column definitions: " + data.getColumnDefinitions());
                            break;

                        case inner:
                        case outer:
                            Iterator<Map<String,DataCell>> it = data.getMappedData().iterator();
                            List<Map<String,DataCell>> newdata = new ArrayList();
                            iterate2: for ( int i=1; it.hasNext(); i++ )
                            {
                                data.setCurrentProcessedRow(i);
                                System.out.println(getTaskName() + ": Joining row #" + i + " out of " + total);
                                Map<String,DataCell> map = it.next();
                                statement = query.getTrimmedStatement();
                                now = new Date();

                                logger.debug("Processing database statement on row #" + i);
                                for ( int j=1; j<=arguments.size(); j++ )
                                {
                                    Argument arg = arguments.get(j-1);
                                    String argname = "";
                                    Object val = null;
                                    if ( arg.getArgumentType() == Argument.Type.column )
                                    {
                                        argname = String.valueOf(arg.getValue());
                                        DataCell cell = map.get(argname);
                                        if ( cell != null )
                                        {
                                            val = cell.getValue();
                                        }
                                        else if ( data.getColumnDefinition(argname) != null )
                                        {
                                            val = null;
                                        }
                                        else
                                        {
                                            logger.debug("Undefined column " + arg + ", unable to execute query with correct data.");
                                            it.remove();
                                            map.put(COL_DATABASE_ERR, new DataCell(ecol, "Undefined column " + arg + ", unable to execute query with correct data."));
                                            map.put(COL_STATEMENT_MSG, new DataCell(scol, null));
                                            newdata.add(map);
                                            errors.addDataRow(map);
                                            continue iterate2;
                                        }
                                    }
                                    else
                                    {
                                        argname = arg.getColumn().getColumnName();
                                        val = arg.getValue();
                                    }

                                    logger.debug("Argument #" + j + " '" + argname + "'=" + val);
                                    addArgumentToStatement(ps, statement, j, val, null, false);
                                }

                                rs = ps.executeQuery();
                                logger.info("Query '" + statement + "' took " + (new Date().getTime()-now.getTime()) + " ms");
                                rowdata = processResultSet(conn, rs, false);
                                logger.debug("ResultSet: " + rowdata);

                                // If including all, add the columns before processing each row in the result set
                                if ( includeAll )
                                {
                                    Map<String,DataTypes> types = extractDataTypes(rs);
                                    logger.trace("DataTypes: " + types);

                                    for ( Map.Entry<String,DataTypes> entry: types.entrySet() )
                                    {
                                        String colname = entry.getKey();
                                        DataTypes type = entry.getValue();
                                        DataColumn col = data.getColumnDefinition(colname);
                                        if ( col == null )
                                        {
                                            col = new DataColumn(type, colname);
                                            data.addColumnDefinition(col);
                                        }
                                    }
                                }

                                if ( rowdata.size() > 0 )
                                {
                                    // Add a copy of the current row to each returned row
                                    for ( Map<String,Object> row: rowdata )
                                    {
                                        Map<String,DataCell> newmap = new LinkedHashMap(map);
                                        for ( DataColumn col: declaredColumns )
                                        {
                                            DataColumn dc = data.getColumnDefinition(col.getColumnName());
                                            if ( dc == null )
                                            {
                                                data.addColumnDefinition(col);
                                            }
                                            DataCell cell = new DataCell(col, row.get(col.getColumnName()));
                                            newmap.put(col.getColumnName(), cell);
                                        }

                                        // Add remaining columns, setting data type to String for undeclared types
                                        if ( includeAll )
                                        {
                                            for ( Map.Entry<String,Object> entry: row.entrySet() )
                                            {
                                                String colname = entry.getKey();
                                                Object val = entry.getValue();
                                                DataColumn col = data.getColumnDefinition(colname);
                                                if ( col == null )
                                                {
                                                    col = new DataColumn(DataTypes.STRING, String.valueOf(colname));
                                                    data.addColumnDefinition(col);
                                                }

                                                DataCell cell = new DataCell(col, val);
                                                newmap.put(colname, cell);
                                            }
                                        }
                                        
                                        newdata.add(newmap);
                                    }
                                }
                                else
                                {
                                    switch (jtype)
                                    {
                                        case inner:
                                            logger.debug("No rows returned, inner-join: removing referenced row data");
                                            it.remove();
                                            break;
                                        case outer:
                                            logger.debug("No rows returned, outer-join: assigning null to columns");
                                            for ( DataColumn col: declaredColumns )
                                            {
                                                DataColumn dc = data.getColumnDefinition(col.getColumnName());
                                                if ( dc == null )
                                                {
                                                    data.addColumnDefinition(col);
                                                }
                                                DataCell cell = new DataCell(col, null);
                                                map.put(col.getColumnName(), cell);
                                            }
                                            newdata.add(map);
                                            break;
                                    }
                                }
                            }

                            // Add the updated data to the data container. This is necessary since more than one row
                            // could be returned on a join so we cannot modify the current iterator.
                            data.setData(newdata);

                            break;
                    }
                    break;
                    
                case storedprocedure:
                    // Only support ref cursor stored procedure output from Oracle instances currently.
                    switch (dbinfo.getDriverType())
                    {
                        case Oracle:
                        case OracleRAC:
                        case SQLServer:
                        case jTDS:
                            cs = conn.prepareCall(query.getStatement());
                            break;
                        default:
                            throw new ETLException("Stored procedure query for given database type is not currently supported: " + dbinfo.getDriverType());
                    }
                    
                    switch (jtype)
                    {
                        case none:
                            boolean outRegistered = false;
                            String statement = query.getTrimmedStatement();
                            for ( int j=1; j<=arguments.size(); j++ )
                            {
                                Argument arg = arguments.get(j-1);
                                String argname = "";
                                Object val = null;
                                if ( dbinfo.getDriverType() == DBConnection.DriverType.Oracle && arg.getArgumentType() == Argument.Type.output && 
                                     arg.getTargetDataType() == DataTypes.REF_CURSOR )
                                {
                                    if ( !outRegistered )
                                    {
                                        cs.registerOutParameter(j, OracleTypes.CURSOR);
                                        outRegistered = true;
                                        val = "ref_cursor";
                                    }
                                    else
                                    {
                                        throw new ETLException("Cannot register more than one ref_cursor output argument, error on arg#" + j);
                                    }
                                }
                                else if ( arg.getArgumentType() == Argument.Type.output && arg.getTargetDataType() != DataTypes.REF_CURSOR )
                                {
                                    if ( !outRegistered )
                                    {
                                        cs.registerOutParameter(j, convertDataTypesToSQLType(arg.getTargetDataType()));
                                        outRegistered = true;
                                        val = "";
                                    }
                                }
                                else if ( arg.getArgumentType() == Argument.Type.column )
                                {
                                    throw new ETLException("Column arguments not supported in select queries, only joins.");
                                }
                                else
                                {
                                    argname = arg.getColumn().getColumnName();
                                    val = arg.getValue();
                                    if ( val instanceof Date )
                                    {
                                        val = new Timestamp(((Date)val).getTime());
                                    }
                                    cs.setObject(j, val);
                                }

                                logger.debug("Argument #" + j + " '" + argname + "'=" + val + (val!=null?"(" + val.getClass().getName() + ")":""));
                                statement = updateStatementString(statement, "[?]", String.valueOf(val));
                            }
                            
                            // If no OUT param registered then throw an error
                            /*if ( !outRegistered )
                            {
                                throw new ETLException("No ref_cursor type referenced in argument list, which is required for stored procedure inputs");
                            }*/

                            cs.execute();
                            ResultSet rs = (ResultSet)cs.getObject(1);
                            logger.info("Query '" + statement + "' took " + (new Date().getTime()-now.getTime()) + " ms");
                            List<Map<String,Object>> rowdata = processResultSet(conn, rs, true);
                            logger.trace("Rowdata: " + rowdata);
                            data.clearData();

                            // If including all, add the columns before processing each row in the result set
                            if ( includeAll )
                            {
                                Map<String,DataTypes> types = extractDataTypes(rs);
                                logger.trace("DataTypes: " + types);

                                for ( Map.Entry<String,DataTypes> entry: types.entrySet() )
                                {
                                    String colname = entry.getKey();
                                    DataTypes type = entry.getValue();
                                    DataColumn col = data.getColumnDefinition(colname);
                                    if ( col == null )
                                    {
                                        col = new DataColumn(type, colname);
                                        data.addColumnDefinition(col);
                                    }
                                }
                            }

                            for ( int i=1; i<=rowdata.size(); i++ )
                            {
                                data.setCurrentProcessedRow(i);
                                Map<String,Object> row = rowdata.get(i-1);
                                HashMap<String,DataCell> map = new HashMap();
                                for ( DataColumn col: declaredColumns )
                                {
                                    DataColumn dc = data.getColumnDefinition(col.getColumnName());
                                    if ( dc == null )
                                    {
                                        data.addColumnDefinition(col);
                                    }
                                    DataCell cell = new DataCell(col, row.get(col.getColumnName()));
                                    map.put(col.getColumnName(), cell);
                                }

                                // Add remaining columns, setting data type to String for undeclared types
                                if ( includeAll )
                                {
                                    for ( Map.Entry<String,Object> entry: row.entrySet() )
                                    {
                                        String colname = entry.getKey();
                                        Object val = entry.getValue();
                                        DataColumn col = data.getColumnDefinition(colname);
                                        if ( col == null )
                                        {
                                            col = new DataColumn(DataTypes.STRING, String.valueOf(colname));
                                            data.addColumnDefinition(col);
                                        }

                                        DataCell cell = new DataCell(col, val);
                                        map.put(colname, cell);
                                    }
                                }

                                data.addDataRow(map);
                            }

                            logger.debug("Assigned column definitions: " + data.getColumnDefinitions());
                            break;
                            
                        case inner:
                        case outer:
                            throw new ETLException("Stored procedure input queries cannot be joined");
                    }
                    break;
            }
        }
        catch (SQLException e)
        {
            throw new ETLException("Unable to execute query: " + query.getStatement(), e);
        }
        catch (IOException e)
        {
            throw new ETLException("Unable to execute query: " + query.getStatement(), e);
        }
        finally
        {
            try
            {
                if ( ps != null ) ps.close();
                if ( cs != null ) cs.close();
                if ( conn != null ) conn.close();
            }
            catch (SQLException e)
            {
                //logger.warn("Unable to close jdbc artifacts", e);
                System.err.println("Unable to close jdbc artifacts");
            }
        }

        return errors;
    }

    public RelationalContainer executeUpdate()
        throws ETLException
    {
        DataColumn ecol = new DataColumn(DataTypes.STRING, COL_DATABASE_ERR);
        DataColumn scol = new DataColumn(DataTypes.STRING, COL_STATEMENT_MSG);
        RelationalContainer errors = new RelationalContainer(data.getDataName() + ERROR_DATA_NAME);
        errors.setColumnDefinitions(data.getColumnDefinitions());
        errors.addColumnDefinition(ecol);
        errors.addColumnDefinition(scol);

        if ( !inline )
        {
            // Set the connection info at runtime (to support dynamic global variable modification)
            setConnectionInfo(oci);

            // Process any embedded bean script to update with global variable values
            DataArchiver da = wrapper.getDataArchiver();
            query.setStatement(da.processEmbeddedBeanScript(query.getStatement()));
        }

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Date now = new Date();

        try
        {
            conn = initiateJDBCConnection();
            logger.debug("Update has following arguments: " + arguments);
            switch (stype)
            {
                case statement:
                    ps = conn.prepareStatement(query.getStatement());
                    break;
                case storedprocedure:
                    ps = conn.prepareCall(query.getStatement());
            }
            
            Iterator<Map<String,DataCell>> it = data.getMappedData().iterator();
            int total = data.rowCount();
            for ( int i=1; it.hasNext(); i++ )
            {
                data.setCurrentProcessedRow(i);
                System.out.println(getTaskName() + ": Executing update on row #" + i + " out of " + total);
                Map<String,DataCell> map = it.next();
                String statement = query.getTrimmedStatement();

                try
                {
                    now = new Date();
                    logger.debug("Processing database statement on row #" + i);
                    HashMap<DataColumn,Integer> outParams = new HashMap();
                    
                    if ( !useAllOrderedArguments )
                    {
                        int offset = 0;
                        for ( int j=1; j<=arguments.size(); j++ )
                        {
                            Argument arg = arguments.get(j-1);
                            String argname = "";
                            Object val = null;
                            if ( arg.getArgumentType() == Argument.Type.column )
                            {
                                argname = String.valueOf(arg.getValue());
                                DataCell cell = map.get(argname);
                                if ( cell != null )
                                {
                                    val = cell.getValue();
                                }
                                else
                                {
                                    logger.warn("Undefined column " + arg + ", assigning NULL");
                                    val = null;
                                }
                            }
                            else if ( arg.getArgumentType() == Argument.Type.output )
                            {
                                argname = arg.getColumn().getColumnName();
                                DataColumn targetCol = new DataColumn(arg.getTargetDataType(), argname);
                                data.addColumnDefinition(targetCol); 
                                outParams.put(targetCol, j);
                                
                                DataCell cell = map.get(argname);
                                if ( cell != null )
                                {
                                    val = cell.getValue();
                                }
                                else
                                {
                                    val = null;
                                }
                                logger.info("Adding output column to stored procedure database output: " + targetCol);
                            }
                            else if ( arg.getArgumentType() == Argument.Type.list )
                            {
                                argname = String.valueOf(arg.getValue());
                                logger.debug("Reference to an argument list: " + argname);
                                String[] args = argname.split("\\" + String.valueOf(arg.getDelimiter()));
                                
                                for ( int x=0; x<args.length; x++ )
                                {
                                    String cname = args[x];
                                    DataCell cell = map.get(cname);
                                    DataColumn col = data.getColumnDefinition(cname);
                                    if ( col == null )
                                    {
                                        throw new ETLException("Reference to non-existant column in argument list: " + cname);
                                    }
                                    
                                    if ( cell != null )
                                    {
                                        val = cell.getValue();
                                    }
                                    else
                                    {
                                        logger.warn("Undefined column " + arg + ", assigning NULL");
                                        val = null;
                                    }
                                    
                                    logger.debug("Argument #" + (x+1) + " '" + cname + "'=" + val + (col != null?"(" + col.getColumnClass().getName() + ")":""));
                                    //statement = updateStatementString(statement, "[?]", String.valueOf(val));
                                    addArgumentToStatement(ps, statement, (x+1), val, col.getDataType(), arg.getArgumentType() == Argument.Type.output);  
                                    offset++;
                                }
                                
                                // Necessary as x is a 0 based index that needs to be added to a 1 based index and offset will equal arg length at this point
                                offset--;
                            }
                            else
                            {
                                argname = arg.getColumn().getColumnName();
                                val = arg.getValue();
                            }

                            // Since every argument type other than list deals with a single argument, list arguments cannot assign arguments to the statement here
                            if ( arg.getArgumentType() != Argument.Type.list )
                            {
                                logger.debug("Argument #" + (j+offset) + " '" + argname + "'=" + val + (val != null?"(" + val.getClass().getName() + ")":""));
                                //statement = updateStatementString(statement, "[?]", String.valueOf(val));
                                addArgumentToStatement(ps, statement, (j+offset), val, arg.getTargetDataType(), arg.getArgumentType() == Argument.Type.output);
                            }
                        }
                    }
                    else
                    {
                        logger.info("Adding all columns as arguments in order of declaration");
                        for ( int j=1; j<=data.columnCount(); j++ )
                        {
                            DataColumn col = data.getColumnDefinition(j-1); 
                            Object val = null;
                            DataCell cell = map.get(col.getColumnName());
                            if ( cell != null )
                            {
                                val = cell.getValue();
                            }
                            
                            logger.debug("Argument #" + j + " '" + col.getColumnName() + "'=" + val + (val != null?"(" + val.getClass().getName() + ")":""));
                            //statement = updateStatementString(statement, "[?]", String.valueOf(val));
                            addArgumentToStatement(ps, statement, j, val, null, false);
                        }
                    }

                    logger.info("Update '" + statement + "' took " + (new Date().getTime()-now.getTime()) + " ms");

                    switch (stype)
                    {
                        case statement:
                            // Execute the update
                            if ( ps.executeUpdate() == 0 && stype == StatementType.statement && !ignoreZeroExecuted )
                            {
                                throw new ETLException("Statement did not execute on any rows in the database");
                            }
                            break;
                        case storedprocedure:
                            // Execute the update
                            CallableStatement cs = (CallableStatement)ps;
                            cs.execute();
                            for ( Map.Entry<DataColumn,Integer> entry: outParams.entrySet() )
                            {
                                DataColumn col = entry.getKey();
                                DataCell cell = new DataCell(col, cs.getObject(entry.getValue()));
                                map.put(col.getColumnName(), cell);
                                logger.debug("Output param #" + entry.getValue() + ": " + cell);
                            }
                            break;
                    }
                    
                    logger.info("Statement '" + statement + "' took " + (new Date().getTime()-now.getTime()) + " ms");                   
                }
                catch (SQLException e)
                {
                    String msg = "Unable to execute statement because: " + e.getMessage();
                    logger.warn(msg + " | Rowdata: " + map, e);
                    
                    if ( !inline )
                    {
                        it.remove();
                        map.put(COL_DATABASE_ERR, new DataCell(ecol, msg));
                        map.put(COL_STATEMENT_MSG, new DataCell(scol, statement));
                        errors.addDataRow(map);
                    }
                    else
                    {
                        throw new ETLException(msg, e);
                    }
                }
                catch (ETLException e)
                {
                    String msg = "Unable to execute statement because: " + e.getMessage();
                    logger.warn(msg + " | Rowdata: " + map, e);
                    
                    if ( !inline )
                    {
                        it.remove();
                        map.put(COL_DATABASE_ERR, new DataCell(ecol, msg));
                        map.put(COL_STATEMENT_MSG, new DataCell(scol, statement));
                        errors.addDataRow(map);
                    }
                    else
                    {
                        throw new ETLException(msg, e);
                    }
                }
                catch (IOException e)
                {
                    String msg = "Unable to execute statement because: " + e.getMessage();
                    logger.warn(msg + " | Rowdata: " + map, e);
                    
                    if ( !inline )
                    {
                        it.remove();
                        map.put(COL_DATABASE_ERR, new DataCell(ecol, msg));
                        map.put(COL_STATEMENT_MSG, new DataCell(scol, statement));
                        errors.addDataRow(map);
                    }
                    else
                    {
                        throw new ETLException(msg, e);
                    }
                }
            }
        }
        catch (SQLException e)
        {
            throw new ETLException("Unable to initiate statement for execution: " + query.getStatement(), e);
        }
        finally
        {
            try
            {
                if ( rs != null ) rs.close();
                if ( ps != null ) ps.close();
                if ( conn != null ) conn.close();
            }
            catch (SQLException e)
            {
                //logger.warn("Unable to close jdbc artifacts", e);
                System.err.println("Unable to close jdbc artifacts");
            }
        }

        return errors;
    }

    private List<Map<String,Object>> processResultSet(Connection conn, ResultSet rs, boolean sysout)
        throws SQLException, ETLException
    {
        ArrayList<Map<String,Object>> rowdata = new ArrayList();
        List<String> labels = extractColumnLabels(rs);
        logger.trace("Column labels: " + labels);
        logger.trace("Column types:  " + extractColumnTypes(rs));
        if ( rs != null )
        {
            for ( int i=1; rs.next(); i++ )
            {
                if ( sysout )
                {
                    System.out.println(getTaskName() + ": Reading row #" + i);
                }
                logger.trace("Processing database row #" + i);
                rowdata.add(processResultSetRow(conn, rs, labels));
            }
        }

        return rowdata;
    }

    private Map<String,Object> processResultSetRow(Connection conn, ResultSet rs, List<String> columnHeaders)
        throws SQLException
    {
        HashMap<String,Object> data = new HashMap();
        if ( rs != null )
        {
            // Ensure cursor is on the correct row before calling
            for ( int i=0; i<columnHeaders.size(); i++ )
            {
                String name = columnHeaders.get(i);
                if ( !data.containsKey(name) )
                {
                    Object obj = rs.getObject(i+1);
                    
                    // Since this conversion to a usable date object for TIMESTAMPTZ requires a database connection it
                    // has to be done here instead of the DataCell class
                    if ( obj instanceof TIMESTAMPTZ )
                    {
                        logger.trace("object is oracle.sql.TIMESTAMPTZ, converting to java.sql.Timestamp");
                        obj = ((TIMESTAMPTZ)obj).timestampValue(conn);
                    }
                    
                    data.put(name, obj);
                }
                else
                {
                    logger.warn("Result set already contains a reference to column " + name + ", not overwriting");
                }
            }
        }
        logger.trace("DB row hash: " + data);
        
        return data;
    }

    private List<String> extractColumnLabels(ResultSet rs)
        throws SQLException
    {
        ArrayList<String> labels = new ArrayList();
        if ( rs != null )
        {
            ResultSetMetaData mdata = rs.getMetaData(); 
            for ( int i=1; i<=mdata.getColumnCount(); i++ )
            {
                String label = mdata.getColumnLabel(i).toUpperCase();
                labels.add(label);
            }
        }
        return labels;
    }
    
    private List<String> extractColumnTypes(ResultSet rs)
        throws SQLException
    {
        ArrayList<String> labels = new ArrayList();
        if ( rs != null )
        {
            ResultSetMetaData mdata = rs.getMetaData(); 
            for ( int i=1; i<=mdata.getColumnCount(); i++ )
            {
                String label = mdata.getColumnClassName(i);
                labels.add(label);
            }
        }
        return labels;
    }

    private Map<String,DataTypes> extractDataTypes(ResultSet rs)
        throws SQLException
    {
        Map<String,DataTypes> types = new LinkedHashMap();
        if ( rs != null )
        {
            ResultSetMetaData mdata = rs.getMetaData();
            for ( int i=1; i<=mdata.getColumnCount(); i++ )
            {
                String label = mdata.getColumnLabel(i).toUpperCase();
                String type = mdata.getColumnTypeName(i);
                int ttype = mdata.getColumnType(i);
                int precision = mdata.getPrecision(i);
                int scale = mdata.getScale(i);
                //labels.add(label);
                logger.trace("Type: " + label + "=" + type + "<" + ttype + "> (" + precision + ", " + scale + ")");
                types.put(label, convertSQLTypeToDataTypes(type, ttype, precision, scale));
            }
        }
        return types;
    }

    private DataTypes convertSQLTypeToDataTypes(String s_type, int i_type, int precision, int scale)
        throws SQLException
    {
        DataTypes t = null;
        switch (i_type)
        {
            case Types.DATE:
            case Types.TIMESTAMP:
            case Types.TIME:
                t = DataTypes.DATE;
                break;
            case Types.VARCHAR:
            case Types.CHAR:
            case Types.CLOB:
            case Types.LONGVARCHAR:
            case Types.NCHAR:
            case Types.NVARCHAR:
            case Types.NCLOB:
                t = DataTypes.STRING;
                break;
            case Types.INTEGER:
            case Types.SMALLINT:
            case Types.TINYINT:
                t = DataTypes.INT;
                break;
            case Types.BIGINT:
                t = DataTypes.LONG;
                break;
            case Types.BIT:
            case Types.BOOLEAN:
                t = DataTypes.BOOLEAN;
                break;
            case Types.BINARY:
            case Types.BLOB:
                t = DataTypes.BINARY;
                break;
            case Types.FLOAT:
                t = DataTypes.FLOAT;
                break;
            case Types.DECIMAL:
            case Types.DOUBLE:
                t = DataTypes.DOUBLE;
                break;
            case Types.JAVA_OBJECT:
                t = DataTypes.OBJECT;
                break;
            case Types.NUMERIC:
                if ( scale != 0 )
                {
                    t = DataTypes.DOUBLE;
                }
                else if ( precision >= 10 )
                {
                    t = DataTypes.LONG;
                }
                else 
                {
                    t = DataTypes.INT;
                }
                break;
            default:
                throw new SQLException("Unsupported sql data type: " + s_type + "<" + i_type + ">");
        }
        
        return t;
    }

    private int convertDataTypesToSQLType(DataTypes i_type)
        throws SQLException
    {
        int t = -1;
        switch (i_type)
        {
            case DATE:
                t = Types.TIMESTAMP;
                break;
            case STRING:
                t = Types.VARCHAR;
                break;
            case INT:
                t = Types.INTEGER;
                break;
            case LONG:
                t = Types.BIGINT;
                break;
            case BOOLEAN:
                t = Types.BOOLEAN;
                break;
            case BINARY:
                t = Types.BLOB;
                break;
            case FLOAT:
                t = Types.FLOAT;
                break;
            case DOUBLE:
                t = Types.DOUBLE;
                break;
            case OBJECT:
                t = Types.JAVA_OBJECT;
                break;
            default:
                throw new SQLException("Unable to convert data type to sql type: " + i_type);
        }
        
        return t;
    }

    /*protected List<String> extractColumnClasses(ResultSet rs)
        throws SQLException
    {
        ArrayList<String> labels = new ArrayList();
        if ( rs != null )
        {
            ResultSetMetaData mdata = rs.getMetaData();
            for ( int i=1; i<=mdata.getColumnCount(); i++ )
            {
                labels.add(mdata.get(i));
            }
        }
        return labels;
    }*/

    private Object getObject(ResultSet rs, int i)
        throws SQLException
    {
        Object o = null;
        //List<String> labels = labelMap.get(METADATA_LABELS);
        //List<String> columns = labelMap.get(METADATA_COLUMNS);
        /*if ( !labels.contains(label) && !columns.contains(column) )
        {
            //logger.warn("Undefined column label '" + label + "' in result set. Expecting: " + labels);
            System.err.println("Undefined column label '" + label + "' in result set. Expecting: " + labelMap);
        }
        else
        {*/
        o = rs.getObject(i);

        return o;
    }
    
    private void addArgumentToStatement(PreparedStatement ps, String statement, int idx, Object val, DataTypes type, boolean output)
        throws SQLException, IOException
    {
        statement = updateStatementString(statement, "[?]", String.valueOf(val));
        if ( val instanceof Date )
        {
            val = new Timestamp(((Date)val).getTime());
        }
        else if ( val instanceof BinaryObject )
        {
            val = ((BinaryObject)val).getData();
        }
        
        if ( output && ps instanceof CallableStatement )
        {
            int sqltype = convertDataTypesToSQLType(type);
            ((CallableStatement)ps).registerOutParameter(idx, sqltype);
            if ( val != null )
            {
                ps.setObject(idx, val);
            }
        }
        else
        {
            ps.setObject(idx, val);
        }
    }
    
    private String updateStatementString(String statement, String replaceString, String replaceValue)
    {
        logger.trace("updateStatementString(" + statement + ", " + replaceString + ", " + replaceValue + ")");
        String out = "";
        try
        {
           out = statement.replaceFirst(replaceString, escapeForRegex(replaceValue));
        }
        catch (Exception e)
        {
            out = statement.replaceFirst(replaceString, "Regex error");
        }

        logger.trace("updatedStatement: " + out);
        return out;

    }

    private String escapeForRegex(String input)
    {
        String val = input.replace("$", "\\$");
        val = val.replace("\\", "\\\\");
        return val;
    }

    public String toString()
    {
        StringBuffer out = new StringBuffer();
        out.append("{");
        out.append("TaskName=" + name);
        out.append(", NextTask=" + next);
        out.append(", AdapterType=" + getAdapterType());
        out.append(", Direction=" + direction);
        out.append(", Connection=" + dbinfo);
        out.append(", Query=" + query);
        out.append("}");
        return out.toString();
    }
}
