/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.tasks;

import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.data.DataIndex;
import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.exception.IndexException;
import com.blackboard.services.etl.exception.SortException;
import com.blackboard.services.etl.exception.VariableTypeMismatchException;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.EtlTaskComplexType;
import com.blackboard.services.etl.jaxb.SortDataComplexType;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.blackboard.services.utils.DelimittedFormat;
import com.blackboard.services.utils.JavaUtils;
import com.blackboard.services.utils.TextFormatException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author crusnak
 */
public class DataSorter extends AbstractTask
{
    private SortDataComplexType base = null;
    private RelationalContainer data = null;
    private String source = null;
    private String target = null;
    private Map<String,DataIndex> indexMap = new HashMap();
    private List<SortColumn> sortColumns = new ArrayList();
    private Map<String,DataColumn> columnMap = new HashMap();
    private DelimittedFormat format = new DelimittedFormat('|');
    private RelationalContainer incomingData = new RelationalContainer("");
    private RelationalContainer outgoingData = new RelationalContainer("");
    private int sortedCount = 0;
    
    public DataSorter(EtlTaskComplexType ct, SortDataComplexType xml, ETLJobWrapper w)
        throws ETLException
    {
        super(ct, w);
        setData(dataSet);

        base = xml;
        source = base.getSource();
        target = base.getTarget();
        if ( ( source == null || source.trim().length() == 0 ) &&
             ( target == null || target.trim().length() == 0 ) )
        {
            throw new ETLException("Source and target cannot both be undefined for DataSorter");
        }

        setSortColumns(base.getSortColumns());
    }
    
    private void setSortColumns(SortDataComplexType.SortColumns cols)
    {
        if ( cols != null )
        {
            for ( SortDataComplexType.SortColumns.Column col: cols.getColumn() )
            {
                sortColumns.add(new SortColumn(col.getValue(), col.getType()));
            }
        }
    }

    @Override
    public ClassType getClassType()
    {
        return ClassType.DataSorter;
    }

    public Object getBaseObject()
    {
        return base;
    }

    public String getSource()
    {
        return source;
    }

    public String getTarget()
    {
        return target;
    }

    @Override
    public RelationalContainer getData()
        throws ETLException
    {
        setData(super.getData());
        return data;
    }

    @Override
    public void setData(DataSet d)
    {
        super.setData(d);
        if ( d.getType() != DataSet.Type.relational )
        {
            throw new UnsupportedOperationException("DataSet assigned to " + this.getClass().getSimpleName() + " must be relational");
        }
        data = (RelationalContainer)d;
    }

    public RelationalContainer getIncomingData()
    {
        return incomingData;
    }

    public void setIncomingData(RelationalContainer incomingData)
    {
        this.incomingData = incomingData;
    }

    public RelationalContainer getOutgoingData()
    {
        return outgoingData;
    }

    public int getSortedDataCount()
    {
        return sortedCount;
    }

    public void setOutgoingData(RelationalContainer outgoingData)
    {
        this.outgoingData = outgoingData;
    }
    
    public List<SortColumn> getSortColumns()
    {
        return sortColumns;
    }

    public void sortData()
        throws ETLException
    {
        RelationalContainer src = null;
        RelationalContainer dest = null;
        boolean taskDataAsSource = false;

        System.out.println(getTaskName() + ": Sorting data on columns: " + sortColumns);
        
        logger.info("Loading SourceData: " + getSource());
        DataSet ds = wrapper.retrieveNamedData(getSource());
        if ( ds == null )
        {
            ds = dataSet;
            taskDataAsSource = true;
            logger.info("No source data referenced, using assigned task data");
            if ( ds.rowCount() == 0 )
            {
                logger.warn("Assigned task data has no rows, performing no data mapping");
            }
        }

        if ( ds.getType() != DataSet.Type.relational )
        {
            throw new ETLException("Reference to a hierarchical data set. DataCorrelations only support relational data sets.");
        }

        src = (RelationalContainer)ds;
        incomingData = src;
        logger.info("SourceData loaded, contains " + src.rowCount() + " rows of data");
        logger.debug("SourceData: " + src);

        logger.info("Loading TargetData: " + getTarget());
        ds = wrapper.retrieveNamedData(getTarget());
        if ( ds == null )
        {
            logger.warn("Undefined target data: " + getTarget());
            logger.info("Target data undefined, assign data to mapping task");
            if ( !taskDataAsSource )
            {
                ds = dataSet;
            }
            else
            {
                throw new ETLException("Task data has previously been assigned as source, cannot also be used as destination.");
            }
        }

        dest = (RelationalContainer)ds;
        outgoingData = dest;
        logger.info("TargetData loaded");

        dest.setColumnDefinitions(src.getColumnDefinitions());
        
        try
        {
            // Only sort if sorting is declared
            if ( !sortColumns.isEmpty() )
            {
                logger.info("Sorting data by: " + sortColumns);

                ArrayList<String> columns = new ArrayList();
                for ( SortColumn col: sortColumns )
                {
                    DataColumn dc = src.getColumnDefinition(col.getColumnName());
                    if ( dc == null )
                    {
                        throw new SortException("Unable to sort due to undefined column reference in data: " + col.getColumnName());
                    }

                    // Create a map of columns to the actual DataColumn to get data types
                    columnMap.put(col.getColumnName(), dc);
                    columns.add(col.getColumnName());

                    // Create a map of columns to data index
                    // DataIndexes are created with increasing number of columns to indicate improved
                    // uniqueness precision
                    DataIndex di = new DataIndex(DataIndex.Type.nonunique, columns);
                    src.addDataIndex(di);
                    indexMap.put(col.getColumnName(), di);

                    // Define the delimited format so the proper values can be extracted from the key for sorting
                    format.addField(col.getColumnName(), -1, true, false, String.valueOf(DataTypes.STRING));
                }

                // Index the data now so we can access the rows for a given key
                src.indexAllData();
                logger.debug("ColumnMap: " + columnMap);
                logger.trace("IndexMap: " + indexMap);

                // Generate a sorted list of indexes in the order declared by the sort columns
                List<Integer> sortedIndexes = generateSortedIndexes();
                logger.info("SortedIndexes: " + sortedIndexes);

                // If for some reason the sorted index list doesn't contain the exact number of rows
                // in the sorting data set, throw an exception
                if ( sortedIndexes.size() != src.rowCount() )
                {
                    throw new SortException("Unable to sort as sorted indexes don't contain all data in data set");
                }

                // Add all the sorted data in the order of the indexes calculated
                for ( Integer index: sortedIndexes )
                {
                    dest.addDataRow(src.getMappedData().get(index));
                }
            }
        }
        catch (SortException e)
        {
            String msg = "Unable to sort data on columns: " + sortColumns;
            System.out.println(getTaskName() + ": " + msg);
            logger.warn(msg, e);
            dest = src;
            sortedCount = 0;
        }
        catch (IndexException e)
        {
            String msg = "Unable to sort data on columns: " + sortColumns;
            System.out.println(getTaskName() + ": " + msg);
            logger.warn(msg, e);
            dest = src;
            sortedCount = 0;
        }
        catch (ETLException e)
        {
            dest = src;
            sortedCount = 0;
            throw e;
        }
        finally
        {
            sortedCount = dest.rowCount();
            for ( DataIndex di: indexMap.values() )
            {
                src.removeDataIndex(di);
            }
        }
    }
    
    private List<Integer> generateSortedIndexes()
        throws ETLException
    {
        return generateSortedIndexes(new ArrayList(), 0);
    }
    
    private List<Integer> generateSortedIndexes(List<String> keyvals, int index)
        throws ETLException
    {
        ArrayList<Integer> indexes = new ArrayList();
        if ( index < sortColumns.size() )
        {
            SortColumn scol = sortColumns.get(index);
            DataColumn dc = columnMap.get(scol.getColumnName());
            DataIndex di = indexMap.get(scol.getColumnName());
            logger.debug("Sorting on column: " + scol + " with keys: " + keyvals);
            logger.debug("Column type: " + dc);
            logger.trace("DataIndex: " + di);
            
            // If the incoming key list is empty we only want to extract keys from the data index
            // that start with these incoming keys
            List<String> keylist = new ArrayList();
            if ( !keyvals.isEmpty() )
            {
                String fkey = StringUtils.join(keyvals.toArray(new String[0]), '|') + "|";
                for ( String ckey: di.getIndexedValueMap().keySet() )
                {
                    if ( ckey.startsWith(fkey) )
                    {
                        keylist.add(ckey);
                    }
                }
            }
            // Otherwise we know we need to generate sorted keys on all keys in the data index
            else
            {
                keylist.addAll(di.getIndexedValueMap().keySet());
            }
            
            // For each indexed row in the data index, extract the keys and create a map
            // from it to lookup the values by column name
            Map<String,Map<String,String>> parseMap = new HashMap();
            try
            {
                for ( String k: keylist )
                {
                    parseMap.put(k, format.processLine(k));
                }
            }
            catch (TextFormatException e)
            {
                throw new SortException("Unable to sort due to parse exception", e);
            }
            
            // Generate a sorted list of keys ordered by the values of the current sort column
            List<String> keys = generateSortedKeys(dc, keylist, parseMap, scol);
            logger.debug("Sorted keys on column: " + dc + "=" + keys);
            
            // Iterate over the sorted keys and extract the indexes from the data index
            for ( String k: keys )
            {
                // Add the new key to the incoming key list (for index lookup)
                ArrayList klist = new ArrayList(keyvals);
                klist.add(k);
                
                // If the number of rows returned by the data index is 1, add that to the sorted index list
                List<Integer> rows = di.getIndexedRows(klist);
                logger.debug("Indexed rows: " + rows);
                if ( rows.size() == 1 )
                {
                    logger.debug("Adding rows: " + rows);
                    indexes.addAll(rows);
                }
                // If the number of rows is greater than 1 we need to further refine the sorting
                // by using the next sort column in the ordered list
                else if ( rows.size() > 1 )
                {
                    try
                    {
                        logger.debug("Found multiple rows in index: " + klist + "=" + rows);
                        indexes.addAll(generateSortedIndexes(klist, index+1));
                    }
                    catch (SortException e)
                    {
                        logger.warn("No more columns to sort to find unique values, adding last level as unsorted", e);
                        indexes.addAll(rows);
                    }
                }
                // Otherwise something screwed up since we should always get at least one row back
                // from the data index
                else
                {
                    throw new SortException("Failed to sort, could not find index for keys: " + klist);
                }
            }
        }
        else
        {
            throw new SortException("Unable to sort as there are no more sort columns: " + index);
        }
        
        return indexes;
    }
    
    private List<String> generateSortedKeys(DataColumn dc, Collection<String> keylist, Map<String,Map<String,String>> parseMap, SortColumn col)
        throws VariableTypeMismatchException
    {
        ArrayList<String> keys = new ArrayList();
        ArrayList objects = new ArrayList();
        
        logger.debug("Generating sorted key list from: " + keylist);
        logger.debug("SortColumn: " + col);
        logger.debug("ParseMap: " + parseMap);
        
        // Iterate over each key in the incoming list and extract the value from the parsed lookup map
        for ( String key: keylist )
        {
            String val = parseMap.get(key).get(col.getColumnName());
            logger.trace("testing: " + val);
            logger.trace("val=null: " + val == null);
            logger.trace("String(val)=null: " + String.valueOf(val).toLowerCase().equals("null"));
            val = (val == null || String.valueOf(val).toLowerCase().equals("null")?" ":val);
            DataCell cell = new DataCell(dc, val);
            logger.trace("DataCell: " + cell);
            objects.add(cell.getValue());
        }
        
        logger.trace("Sort list: " + objects);
        
        // Sort the extracted values by their natural sorting algorithm, reversing order if descending
        Collections.sort(objects);
        if ( col.getSortType() == SortColumn.SortType.descending )
        {
            Collections.reverse(objects);
        }
        
        // Convert the values to a string since the data index uses tokenized strings as keys
        for ( Object o: objects )
        {
            keys.add(String.valueOf(o));
        }
        
        return keys;
    }

    public static class SortColumn 
    {
        public static enum SortType { ascending, descending };
        
        private String columnName;
        private SortType sortType = SortType.ascending;
        
        public SortColumn() {}
        
        public SortColumn(String column, SortType type)
        {
            columnName = column;
            sortType = type;
        }
        
        public SortColumn(String column, String type)
        {
            columnName = column;
            setSortType(type);
        }

        public String getColumnName()
        {
            return columnName;
        }

        public void setColumnName(String columnName)
        {
            this.columnName = columnName;
        }
        
        public SortType getSortType()
        {
            return sortType;
        }

        public void setSortType(SortType sortType)
        {
            this.sortType = sortType;
        }

        public void setSortType(String type)
            throws IllegalArgumentException
        {
            try
            {
                setSortType(SortType.valueOf(type));
            }
            catch (Exception e)
            {
                throw new IllegalArgumentException("Invalid sort type '" + type + "', must be one of: " + JavaUtils.enumToList(SortType.class));
            }
        }
        
        public String toString()
        {
            return columnName + "(" + sortType + ")";
        }
    }
}
