/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.tasks;

import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.jaxb.EtlTaskComplexType;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import org.apache.log4j.Logger;

/**
 *
 * @author crusnak
 */
public class NullTask extends AbstractTask
{
    public NullTask(String n, ETLJobWrapper w)
    {
        wrapper = w;
        name = n;
        logger = Logger.getLogger(this.getClass());
        setTaskType(String.valueOf(TaskType.task));
        setData(new RelationalContainer(name));
    }
    
    public NullTask(EtlTaskComplexType etl, ETLJobWrapper wrapper)
    {
        super(etl, wrapper);
    }

    @Override
    public ClassType getClassType()
    {
        return ClassType.Null;
    }

    @Override
    public Object getBaseObject()
    {
        return null;
    }
}
