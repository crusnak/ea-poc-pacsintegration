/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.tasks;

import com.blackboard.services.etl.data.Argument;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.exception.RowLevelException;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.EtlTaskComplexType;
import com.blackboard.services.etl.jaxb.ExtractControlComplexType;
import com.blackboard.services.etl.process.Processor;
import static com.blackboard.services.etl.tasks.FileAdapter.unixAbsoluteDir;
import static com.blackboard.services.etl.tasks.FileAdapter.windowsAbsoluteDir;
import static com.blackboard.services.etl.tasks.FileAdapter.windowsAbsoluteFile;
import static com.blackboard.services.etl.tasks.FileAdapter.windowsAbsoluteSlashDir;
import static com.blackboard.services.etl.tasks.FileAdapter.windowsAbsoluteSlashFile;
import static com.blackboard.services.etl.tasks.FileAdapter.windowsRelativeFile;
import com.blackboard.services.etl.wrapper.DataArchiver;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.blackboard.services.utils.BBTSUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import org.apache.commons.lang.time.DateUtils;

/**
 *
 * @author crusnak
 */
public class ExtractControl extends AbstractTask
{
    private static enum ControlProperty { startVariable, endVariable };
    private static enum ControlType { file, database };
    private static enum Action { initialize, set, finalize };

    public static final String CONTROL_ROOT =               "resources";
    public static final String FILENAME =                   "-control";
    public static final String PROP_THIS_FROM =             "date.this.from";
    public static final String PROP_THIS_TO =               "date.this.to";
    public static final String PROP_LAST_FROM =             "date.last.from";
    public static final String PROP_LAST_TO =               "date.last.to";
    
    private ExtractControlComplexType base = null;
    private RelationalContainer data = null;
    private File origRoot = null;
    private File fileRoot = null;
    private String controlName = null;
    private ControlType type = ControlType.file;
    private Action action = null;
    private Map<ControlProperty,String> variables = new HashMap();
    private boolean useBBTSDates = false;
    private String includeTime = "true";
    
    public ExtractControl(EtlTaskComplexType ct, ExtractControlComplexType xml, File root, ETLJobWrapper w)
        throws ETLException
    {
        super(ct, w);
        setData(dataSet);
        
        base = xml;
        origRoot = root;
        setName(xml.getName());
        setAction(xml.getAction());
        setControlType(xml.getType());
        logger.debug("xml useBBTSDates=" +xml);
        useBBTSDates = xml.isUseBBTSDates();
        includeTime = xml.getIncludeTimeForStart();
        
        setControlProperties(xml);
    }

    @Override
    public Object getBaseObject() 
    {
        return base;
    }

    @Override
    public ClassType getClassType() 
    {
        return ClassType.ExtractControl;
    }
    
    @Override
    public DataSet getData() 
        throws ETLException 
    {
        setData(super.getData());
        return data;
    }

    @Override
    public void setData(DataSet d) 
    {
        super.setData(d);
        if ( d.getType() != DataSet.Type.relational )
        {
            throw new UnsupportedOperationException("DataSet assigned to " + this.getClass().getSimpleName() + " must be relational");
        }
        data = (RelationalContainer)d;
    }

    public String getName() 
    {
        return controlName;
    }

    public void setName(String name) 
    {
        this.controlName = name;
    }

    public Action getAction() 
    {
        return action;
    }

    public void setAction(Action action) 
    {
        this.action = action;
    }
    
    public void setAction(String a)
        throws ETLException
    {
        setAction(Action.valueOf(a));
    }

    public ControlType getControlType() 
    {
        return type;
    }

    public void setControlType(ControlType type) 
    {
        this.type = type;
    }
        
    public void setControlType(String a)
        throws ETLException
    {
        setControlType(ControlType.valueOf(a));
    }   
    
    private void setControlProperties(ExtractControlComplexType in)
        throws ETLException
    {
        variables.clear();
        if ( in != null )
        {
            logger.debug("useBBTSDates=" + useBBTSDates);
            
            // Sanitize configuration for start variable
            if ( in.getStartVariable() == null || in.getStartVariable().getValue() == null )
            {
                throw new ETLException("startVariable element must be defined and non-null");
            }
            else if ( wrapper.getGlobalVariable(in.getStartVariable().getValue()) == null )
            {
                throw new ETLException("Global variable for extract control " + controlName + " startVariable " + in.getStartVariable().getValue() + " must be declared");
            }
            else if ( !useBBTSDates && wrapper.getGlobalVariable(in.getStartVariable().getValue()).getColumn().getDataType() != DataTypes.DATE )
            {
                throw new ETLException("Global variable for extract control " + controlName + " startVariable " + in.getStartVariable().getValue() + " must be datatype DATE");
            }
            else if ( useBBTSDates && wrapper.getGlobalVariable(in.getStartVariable().getValue()).getColumn().getDataType() != DataTypes.DOUBLE )
            {
                throw new ETLException("Global variable for extract control " + controlName + " startVariable " + in.getStartVariable().getValue() + " must be datatype DOUBLE");
            }
            
            // Sanitize configuration for end variable
            if ( in.getEndVariable() == null || in.getEndVariable().getValue() == null )
            {
                throw new ETLException("endVariable element must be defined and non-null");
            }
            else if ( wrapper.getGlobalVariable(in.getEndVariable().getValue()) == null )
            {
                throw new ETLException("Global variable for extract control " + controlName + " endVariable " + in.getEndVariable().getValue() + " must be declared");
            }
            else if ( !useBBTSDates && wrapper.getGlobalVariable(in.getEndVariable().getValue()).getColumn().getDataType() != DataTypes.DATE )
            {
                throw new ETLException("Global variable for extract control " + controlName + " endVariable " + in.getEndVariable().getValue() + " must be datatype DATE");
            }     
            else if ( useBBTSDates && wrapper.getGlobalVariable(in.getEndVariable().getValue()).getColumn().getDataType() != DataTypes.DOUBLE )
            {
                throw new ETLException("Global variable for extract control " + controlName + " endVariable " + in.getEndVariable().getValue() + " must be datatype DOUBLE");
            }     
            
            variables.put(ControlProperty.startVariable, in.getStartVariable().getValue());
            variables.put(ControlProperty.endVariable, in.getEndVariable().getValue());
        }
        
        logger.debug("ControlProperties: " + variables);
    }    

    public void controlExtract()
        throws ETLException
    {
        DataArchiver arch = wrapper.getDataArchiver();
        setName(arch.processEmbeddedBeanScript(controlName, this));
        
        System.out.println(getTaskName() + ": Executing extract control: " + controlName + " " + action);
        
        try
        {
            switch (action)
            {
                case initialize:
                    initializeControl();
                    break;
                case finalize:
                    finalizeControl();
                    break;
                case set:
                default:
                    throw new UnsupportedOperationException("Extract control action " + action + " not currently supported");
            }
        }
        catch (IOException e)
        {
            logger.warn(e);
            throw new ETLException("Failed to access file based extract control", e);
        }
    }
    
    private void initializeControl()
        throws ETLException, IOException
    {
        Properties props = new Properties();
        File file = null;
        logger.info("Initializing extract control for " + controlName);
        
        switch (type)
        {
            case file:
                //file = getExtractControlFile(CONTROL_ROOT, controlName + FILENAME + Processor.TXT_EXT);      
                file = getExtractControlFile(CONTROL_ROOT, controlName + Processor.TXT_EXT);      
                props = new Properties();

                if ( file.exists() )
                {
                    logger.debug("Reading extract control file: " + file.getCanonicalPath());
                    props.load(new FileInputStream(file));
                }
                else
                {
                    logger.debug("Extract control file not found, using defaults: " + file.getCanonicalPath());
                }

                break;
            case database:
            default:
                throw new UnsupportedOperationException("ExtractControl type " + type + " not supported");
        }

        // Getting last values of this from control 
        String lastFrom = props.getProperty(PROP_LAST_FROM);
        String lastTo = props.getProperty(PROP_LAST_TO);

        logger.info("ExtractControl " + controlName + " last ran dates: ");
        logger.info("LastFrom: " + lastFrom);
        logger.info("LastTo: " + lastTo);
        
        Date epoch = new Date(0);
        Date start = null;
        Date end = new Date();
        
        try
        {        
            if ( lastTo == null )
            {
                start = epoch;
            }
            else
            {
                start = ETLJobWrapper.extractControlFormat.parse(lastTo);
            }
        }
        catch (ParseException e)
        {
            logger.warn("Unable to parse lastTo as date using format " + ETLJobWrapper.extractControlFormat + ". Utilizing epoch as start date.");
            start = epoch;
        }
        
        DataCell sc = wrapper.getGlobalVariable(variables.get(ControlProperty.startVariable));
        DataCell ec = wrapper.getGlobalVariable(variables.get(ControlProperty.endVariable));
        
        logger.debug("Current value of global variable " + variables.get(ControlProperty.startVariable) + ": " + sc);
        logger.debug("Current value of global variable " + variables.get(ControlProperty.endVariable) + ": " + ec);
        
        boolean incltime = Boolean.valueOf(wrapper.getDataArchiver().processEmbeddedBeanScript(String.valueOf(includeTime)));
        if ( !incltime )
        {
            logger.debug("Removing time from start");
            start = DateUtils.truncate(start, Calendar.DATE);
            logger.debug("New start: " + start);
        }
        
        // Set start as either lastTo or epoch date; end is always current date
        if ( useBBTSDates )
        {
            sc.setValue(BBTSUtils.convertDateToDouble(start));
            ec.setValue(BBTSUtils.convertDateToDouble(end));
        }
        else
        {
            sc.setValue(start);
            ec.setValue(end);
        }

        // Update global variables based on defined variable names in xml
        wrapper.setGlobalVariable(variables.get(ControlProperty.startVariable), sc);
        wrapper.setGlobalVariable(variables.get(ControlProperty.endVariable), ec);
        
        // Convert this from date to string
        String thisFrom = ETLJobWrapper.extractControlFormat.format(start);
        String thisTo = ETLJobWrapper.extractControlFormat.format(end);
        
        // Updating this to current window
        props.setProperty(PROP_THIS_FROM, thisFrom);
        props.setProperty(PROP_THIS_TO, thisTo);
        
        switch (type)
        {
            case file:
                props.store(new FileOutputStream(file), "initializeExtractControl " + controlName);               
                logger.debug("Writing extract control file: " + file.getCanonicalPath());
                break;
            case database:
            default:
                throw new UnsupportedOperationException("ExtractControl type " + type + " not supported");
        }
        
        setData(new RelationalContainer(name, wrapper.getGlobalVariableData()));
    }
    
    private void finalizeControl()
        throws ETLException, IOException
    {
        Properties props = new Properties();
        File file = null;
        logger.info("Finalizing extract control for " + controlName);
        
        // Deterimine how extract control is stored in order to find out how to extract control data
        switch (type)
        {
            case file:
                file = getExtractControlFile(CONTROL_ROOT, controlName + Processor.TXT_EXT);      
                //file = getExtractControlFile(CONTROL_ROOT, controlName + FILENAME + Processor.TXT_EXT);      
                props = new Properties();

                if ( file.exists() )
                {
                    logger.debug("Reading extract control file: " + file.getCanonicalPath());
                    props.load(new FileInputStream(file));
                }
                else
                {
                    logger.debug("Extract control file not found, using defaults: " + file.getCanonicalPath());
                }

                break;
            case database:
            default:
                throw new UnsupportedOperationException("ExtractControl type " + type + " not supported");
        }

        // Getting current values of this from control 
        String thisFrom = props.getProperty(PROP_THIS_FROM);
        String thisTo = props.getProperty(PROP_THIS_TO);

        logger.info("ExtractControl " + controlName + " current run dates: ");
        logger.info("ThisFrom: " + thisFrom);
        logger.info("ThisTo: " + thisTo);
        
        // Updating last from this
        props.setProperty(PROP_LAST_FROM, thisFrom);
        props.setProperty(PROP_LAST_TO, thisTo);
        
        switch (type)
        {
            case file:
                props.store(new FileOutputStream(file), "finalizeExtractControl " + controlName);               
                logger.debug("Writing extract control file: " + file.getCanonicalPath());
                break;
            case database:
            default:
                throw new UnsupportedOperationException("ExtractControl type " + type + " not supported");
        }
        
        setData(new RelationalContainer(name, wrapper.getGlobalVariableData()));
    }
    
    private File getExtractControlFile(String dir, String filename)
        throws IOException, ETLException
    {
        File file = getFileInfo(dir, filename, null, origRoot);
        logger.debug("File for writing: " + file);
        return file;
    }

    private File getFileInfo(String dir, String filename, String f, File defaultRoot)
        throws ETLException
    {
        File file = null;
        logger.debug("Comparing file root: " + dir);
        
        // Change here: if relative directory use etlRoot + directory
        // if absolute directory, replace etlRoot with directory
        
        if ( f != null && f.trim().length() > 0 )
        {
            // File could be declared from a global variable so to ensure the file path is setup properly we have to process the script here
            if ( wrapper.getDataArchiver().containsBeanScript(f) )
            {
                f = wrapper.getDataArchiver().processEmbeddedBeanScript(f);
            }
            
            Matcher m = FileAdapter.windowsAbsoluteSlashFile.matcher(f);
            Matcher m2 = FileAdapter.windowsAbsoluteFile.matcher(f);
            Matcher m3 = FileAdapter.windowsRelativeFile.matcher(f);
            
            logger.debug("Identifying file path: " + f);
            logger.debug("Regex1: " + FileAdapter.windowsAbsoluteSlashFile.pattern());
            logger.debug("Regex2: " + FileAdapter.windowsAbsoluteFile.pattern());
            
            if ( m.matches() )
            {
                logger.info("FileAdapter file uses absolute directory with extra backslash and filename");
                dir = m.group(1);
                filename = m.group(2);
            }
            else if ( m2.matches() )
            {
                logger.info("FileAdapter file uses absolute directory and filename");
                dir = m2.group(1);
                filename = m2.group(2);
            }
            else if ( m3.matches() )
            {
                logger.info("FileAdapter file uses relative directory and filename");
                dir = m3.group(1);
                filename = m3.group(2);
            }
            else
            {
                throw new ETLException("Format for attibute file is incorrect. Format requires both a path (absolute or relative) and a filename.");
            }
        }
                
        if ( dir != null && dir.trim().length() > 0 )
        {
            Matcher m = FileAdapter.windowsAbsoluteSlashDir.matcher(dir);
            if ( m.matches() )
            {
                logger.info("FileAdapter file uses absolute directory with extra backslash");
                fileRoot = new File(m.group(1) + ":" + m.group(2));
            }
            else if ( dir.matches(windowsAbsoluteDir.pattern()) ||
                      dir.matches(unixAbsoluteDir.pattern()) )
            {
                logger.info("FileAdapter file uses absolute directory");
                fileRoot = new File(dir);
            }
            else
            {
                logger.info("FileAdapter file uses relative directory");
                fileRoot = new File(defaultRoot, dir);
            }
        }
        else
        {
            fileRoot = origRoot;
        }
        
        try
        {
            if ( filename != null )
            {
                file = new File(fileRoot.getCanonicalFile(), filename);
            }
        }
        catch (IOException e)
        {
            throw new ETLException("Unable to access ETL root", e);
        }

        logger.debug("Prepared file: " + file);
        return file;
    }    
}
