/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.tasks;

import com.blackboard.services.etl.data.Argument;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.jaxb.ArgumentComplexType;
import com.blackboard.services.etl.jaxb.ArgumentsComplexType;
import com.blackboard.services.etl.jaxb.CustomTaskComplexType;
import com.blackboard.services.etl.jaxb.DataComplexType;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.EtlTaskComplexType;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.blackboard.services.utils.BaseLogger;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 * @author crusnak
 */
public class CustomProcess extends AbstractTask
{
    public static final String CONSTRUCTOR =                "${constructor}";

    private CustomTaskComplexType base = null;
    private RelationalContainer data = null;
    private Class clazz = null;
    private Method method = null;
    private List<Argument> arguments = new ArrayList();
    private List<DataColumn> declaredColumns = new ArrayList();
    private boolean executeIndividual = false;

    public CustomProcess(EtlTaskComplexType ct, CustomTaskComplexType xml, ETLJobWrapper w)
        throws ETLException
    {
        super(ct, w);
        setData(dataSet);

        setCustomProcess(xml);
    }

    public Object getBaseObject()
    {
        return base;
    }

    @Override
    public ClassType getClassType()
    {
        return ClassType.CustomProcess;
    }

    @Override
    public RelationalContainer getData()
        throws ETLException
    {
        setData(super.getData());
        return data;
    }

    @Override
    public void setData(DataSet d)
    {
        super.setData(d);
        if ( d.getType() != DataSet.Type.relational )
        {
            throw new UnsupportedOperationException("DataSet assigned to " + this.getClass().getSimpleName() + " must be relational");
        }
        data = (RelationalContainer)d;
    }

    private void setCustomProcess(CustomTaskComplexType ct)
        throws ETLException
    {
        if ( ct != null )
        {
            base = ct;
            executeIndividual = ct.isExecuteIndividual();
            try
            {
                clazz = Class.forName(ct.getClazz());
                
                // Multiple processing
                if ( !executeIndividual )
                {
                    if ( ct.getArguments() == null || ct.getArguments().getArgument() == null ||
                         ct.getArguments().getArgument().size() == 0 )
                    {
                        Class[] paramtypes = { RelationalContainer.class };
                        method = findMethod(clazz, ct.getMethod(), paramtypes);
                        if ( !RelationalContainer.class.equals(method.getReturnType()) )
                        {
                            throw new ClassCastException("Custom java classes that execute once must return: " + RelationalContainer.class);
                        }
                    }
                    else
                    {
                        setArguments(ct.getArguments());
                        Class[] paramtypes = new Class[arguments.size()+1];
                        paramtypes[0] = RelationalContainer.class;
                        for ( int i=0; i<arguments.size(); i++ )
                        {
                            Argument arg = arguments.get(i);
                            switch ( arg.getArgumentType() )
                            {
                                case column:
                                    throw new ETLException("Column argument types not supported on single execution custom processes");
                                case constant:
                                    paramtypes[i+1] = new DataColumn(arg.getTargetDataType(), null).getColumnClass();
                                    break;
                            }
                        }

                        method = findMethod(clazz, ct.getMethod(), paramtypes);
                        if ( !RelationalContainer.class.equals(method.getReturnType()) )
                        {
                            throw new ClassCastException("Custom java classes that execute once must return: " + RelationalContainer.class);
                        }
                    }
                }
                else
                {
                    setArguments(ct.getArguments());
                }

                setOutputData(ct.getOutputData());
            }
            catch (ClassNotFoundException e) { throw new ETLException(e); }
            catch (NoSuchMethodException e) { throw new ETLException(e); }
            catch (ClassCastException e) { throw new ETLException(e); }
        }
    }

    private void setArguments(ArgumentsComplexType act)
        throws ETLException
    {
        arguments.clear();
        if ( act != null )
        {
            setArguments(act.getArgument());
        }
    }

    private void setArguments(List<ArgumentsComplexType.Argument> args)
        throws ETLException
    {
        arguments.clear();
        for ( ArgumentComplexType arg: args )
        {
            arguments.add(new Argument(arg, this));
        }
    }

    private void setOutputData(CustomTaskComplexType.OutputData ct)
        throws ETLException
    {
        if ( ct != null && ct.getDataElement().size() > 0 )
        {
            declaredColumns.clear();
            for ( DataComplexType data: ct.getDataElement() )
            {
                DataColumn col = new DataColumn(data.getType(), data.getValue());
                declaredColumns.add(col);
            }
        }
    }

    public Class getCustomClass()
    {
        return clazz;
    }

    public Method getCustomMethod()
    {
        return method;
    }

    public RelationalContainer invokeCustomMethod()
        throws ETLException
    {
        RelationalContainer errors = null;
        DataColumn ecol = new DataColumn(DataTypes.STRING, RelationalContainer.COL_ERROR_MSG);

        try
        {
            if ( !executeIndividual )
            {
                if ( method == null )
                {
                    throw new ETLException("Method is null, unable to invoke");
                }

                for ( DataColumn col: declaredColumns )
                {
                    data.addColumnDefinition(col);
                }

                String meth = method.getName();
                meth += "(DataContainer";
                Object[] args = new Object[arguments.size()+1];
                args[0] = data;

                for ( int j=0; j<arguments.size(); j++ )
                {
                    meth += ", ";
                    Object arg = arguments.get(j).getValue();
                    args[j+1] = arg;
                    meth += arg;
                }
                meth += ")";
                logger.info("Invoking custom process: " + meth);
                Object instance = clazz.newInstance();
                errors = (RelationalContainer)method.invoke(instance, args);
            }
            else if ( data.rowCount() > 0 )
            {
                errors = new RelationalContainer(getTaskName() + AbstractTask.ERROR_DATA_NAME);
                errors.setColumnDefinitions(data.getColumnDefinitions());
                errors.addColumnDefinition(ecol);

                Class[] paramtypes = new Class[arguments.size()];
                for ( int i=0; i<arguments.size(); i++ )
                {
                    Argument arg = arguments.get(i);
                    switch ( arg.getArgumentType() )
                    {
                        case column:
                            DataColumn dc = data.getColumnDefinition(String.valueOf(arg.getValue()));
                            paramtypes[i] = dc.getColumnClass();
                            break;
                        case constant:
                            paramtypes[i] = new DataColumn(arg.getTargetDataType(), null).getColumnClass();
                            break;
                    }
                }

                method = findMethod(clazz, base.getMethod(), paramtypes);
                if ( declaredColumns.size() == 0 && !"void".equals(method.getReturnType().getName()) )
                {
                    throw new ClassCastException("Custom java classes that execute over an entire data set with NO output data declared must have void returns.");
                }
                else if ( declaredColumns.size() > 0 && !(Map.class.isAssignableFrom(method.getReturnType())) )
                {
                    throw new ClassCastException("Custom java classes that execute over an entire data set with output data declared must return interface: " + Map.class);
                }

                // Prepare args and call method for each row in the data container
                Iterator<Map<String,DataCell>> it = data.getMappedData().iterator();
                int count = data.rowCount();
                for ( int i=1; it.hasNext(); i++ )
                {
                    data.setCurrentProcessedRow(i);
                    System.out.println(getTaskName() + ": Executing custom method " + method.getName() + " on row #" + i + " out of " + count);
                    Map<String,DataCell> row = it.next();
                    String meth = method.getName();
                    try
                    {
                        Object[] args = new Object[arguments.size()];
                        meth += "(";
                        for ( int j=0; j<arguments.size(); j++ )
                        {
                            Object arg = row.get(arguments.get(j).getValue()).getValue();
                            args[j] = arg;
                            meth += arg;
                            if ( j < arguments.size() -1 )
                            {
                                meth += ", ";
                            }
                        }
                        meth += ")";
                        logger.info("Invoking custom process for row #" + i + ": " + meth);
                        Object instance = clazz.newInstance();
                        Object retval = method.invoke(instance, args);
                        if ( declaredColumns.size() > 0 )
                        {
                            Map map = (Map)retval;
                            for ( DataColumn col: declaredColumns )
                            {
                                Object val = map.get(col.getColumnName());
                                row.put(col.getColumnName(), new DataCell(col, val));
                            }
                        }
                    }
                    catch (InvocationTargetException e)
                    {
                        String msg = "Error processing custom task " + meth + ": " + e.getTargetException().getMessage();
                        logger.warn(msg);
                        it.remove();
                        row.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, msg));
                        errors.addDataRow(row);
                    }
                }
            }
        }
        catch (IllegalAccessException e) { throw new ETLException(e); }
        catch (InvocationTargetException e) { throw new ETLException("Exception thrown by invoked method", e); }
        catch (InstantiationException e) { throw new ETLException("Custom java classes must contain an empty constructor", e); }
        catch (NoSuchMethodException e) { throw new ETLException("Unable to find custom method with the given signature", e); }

        return errors;
    }

    public Object invokeExternalMethod(String classname, String methodname)
        throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException,
               InvocationTargetException, InstantiationException
    {
        return invokeExternalMethod(classname, methodname, null, null, null);
    }

    public Object invokeExternalMethod(String classname, String methodname, Object instance)
        throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException,
               InvocationTargetException, InstantiationException
    {
        return invokeExternalMethod(classname, methodname, null, null, instance);
    }

    public Object invokeExternalMethod(String classname, String methodname, Class[] paramtypes,
                                       Object[] arguments, Object instance)
        throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException,
               InvocationTargetException, InstantiationException
    {
        Object retval = null;
        Class clazz = Class.forName(classname);
        if ( CONSTRUCTOR.equals(methodname) )
        {
            retval = findConstructorAndInvoke(clazz, arguments);
        }
        else
        {
            retval = findMethodAndInvoke(clazz, methodname, instance, arguments);
        }
        return retval;
    }

    private Method findMethod(Class clazz, String methodname, Class[] arguments)
        throws NoSuchMethodException
    {
        Method method = null;
        Method[] methods = clazz.getMethods();
        for ( Method m: methods )
        {
            if ( methodname.equals(m.getName()) )
            {
                if ( paramTypesMatch(arguments, m.getParameterTypes()) )
                {
                    method = m;
                    break;
                }
            }
        }

        if ( method == null )
        {
            String params = "(";
            for ( int i=0; i<arguments.length; i++ )
            {
                params += arguments[i].getName();
                if ( i < arguments.length-1 )
                {
                    params += ", ";
                }
            }
            params += ")";
            throw new NoSuchMethodException(clazz.getName() + "." + methodname + params);
        }

        return method;
    }

    private boolean paramTypesMatch(Class[] args, Class[] paramtypes)
    {
        boolean match = false;
        if ( args == null && paramtypes == null )
        {
            match = true;
        }
        else if ( args != null & paramtypes != null && args.length == paramtypes.length )
        {
            match = true;
            for ( int i=0; i<args.length; i++ )
            {
                match &= args[i].equals(paramtypes[i]);
            }
        }
        return match;
    }

    private Object findMethodAndInvoke(Class clazz, String methodname, Object instance, Object[] arguments)
        throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException,
               InvocationTargetException, InstantiationException
    {
        Object retval = null;
        Method[] methods = clazz.getMethods();
        boolean invoked = false;
        for ( Method m: methods )
        {
            if ( methodname.equals(m.getName()) )
            {
                try
                {
                    retval = m.invoke(instance, arguments);
                    BaseLogger.info("INVOKING: " + m);
                    BaseLogger.info("INSTANCE OBJECT: " + instance);
                    invoked = true;
                    break;
                }
                catch (IllegalArgumentException e)
                {
                    BaseLogger.debug("Method doesn't fit passed arguments: " + m);
                }
            }
        }

        if ( !invoked )
        {
            String msg = clazz.getName() + "." + methodname + "(";
            if ( arguments != null )
            {
                for ( int i=0; i<arguments.length; i++ )
                {
                    if ( arguments[i] != null )
                    {
                        msg += arguments[i].getClass();
                    }
                    else
                    {
                        msg += Object.class;
                    }

                    if ( i < arguments.length-1 )
                    {
                        msg += ", ";
                    }
                }
            }
            msg += ")";
            throw new NoSuchMethodException(msg);
        }
        return retval;
    }

    private Constructor findConstructor(Class clazz, Object[] arguments)
    {
        return null;
    }

    private Object findConstructorAndInvoke(Class clazz, Object[] arguments)
        throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException,
               InvocationTargetException, InstantiationException
    {
        Object retval = null;
        Constructor[] cons = clazz.getConstructors();
        boolean invoked = false;
        for ( Constructor c: cons )
        {
            try
            {
                retval = c.newInstance(arguments);
                BaseLogger.info("CONSTRUCTING: " + c);
                invoked = true;
                break;
            }
            catch (IllegalArgumentException e)
            {
                BaseLogger.debug("Constructor doesn't fit passed arguments: " + c);
            }
        }

        if ( !invoked )
        {
            String msg = clazz.getName() + ".<init>(";
            if ( arguments != null )
            {
                for ( int i=0; i<arguments.length; i++ )
                {
                    if ( arguments[i] != null )
                    {
                        msg += arguments[i].getClass();
                    }
                    else
                    {
                        msg += Object.class;
                    }

                    if ( i < arguments.length-1 )
                    {
                        msg += ", ";
                    }
                }
            }
            msg += ")";
            throw new NoSuchMethodException(msg);
        }
        return retval;
    }

    public String toString()
    {
        StringBuffer out = new StringBuffer();
        out.append("{");
        out.append("TaskName=" + name);
        out.append(", NextTask=" + next);
        out.append(", CustomMethod=" + method);
        out.append("}");
        return out.toString();
    }
}
