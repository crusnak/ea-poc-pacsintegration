/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.tasks;

import com.blackboard.services.etl.data.Argument;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.jaxb.DataComplexType;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.EtlTaskComplexType;
import com.blackboard.services.etl.jaxb.InputTaskComplexType;
import com.blackboard.services.etl.jaxb.OutputTaskComplexType;
import com.blackboard.services.etl.jaxb.TiaConfigComplexType;
import com.blackboard.services.etl.jaxb.TiaInputTaskComplexType;
import com.blackboard.services.etl.jaxb.TiaOutputTaskComplexType;
import com.blackboard.services.etl.jaxb.TiaRequestComplexType;
import com.blackboard.services.etl.wrapper.ConnectionFactory;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.blackboard.services.security.object.TIAConnection;
import com.blackboard.services.tia.TIAConfiguration;
import com.blackboard.services.tia.TIAException;
import com.blackboard.services.tia.TIAMessageException;
import com.blackboard.services.tia.TIAMessageRequest;
import com.blackboard.services.tia.TIAMessageResponse;
import com.blackboard.services.tia.TIAMessenger;
import com.blackboard.services.tia.TIAResponseObject;
import com.blackboard.services.tia.TIATransactionType;
import java.io.IOException;
import java.net.SocketException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.xml.bind.JAXBElement;

/**
 *
 * @author crusnak
 */
public class TIAAdapter extends AbstractAdapter
{
    private static enum ConfigurableProperty { cardNumber, tenderNumber, amount, pin, manualEntry, online };
    private static enum InputProperty { balance, responseCode, message, sequenceNumber };

    public static final String COL_TIA_REQUEST =        "TIA_REQUEST";
    public static final String COL_RESPONSE_CODE =      "RESPONSE_CODE";

    private static final String ARG_CARDNUM =           "cardNumber";
    private static final String ARG_TENDERNUM =         "tenderNumber";
    private static final String ARG_AMOUNT =            "amount";
    private static final String ARG_PIN =               "pin";
    private static final String ARG_MANUAL =            "manualEntry";
    private static final String ARG_ONLINE =            "online";

    private static final String INPUT_BALANCE =         "balance";
    private static final String INPUT_RESP_CODE =       "responseCode";
    private static final String INPUT_MESSAGE =         "message";
    private static final String INPUT_SEQ_NUM =         "sequenceNumber";

    private Object ta = null;
    private RelationalContainer data = null;
    private TiaConfigComplexType config = null;
    private TIAMessenger messenger = null;
    private TIAConnection tiainfo = null;
    private TIATransactionType ttype = TIATransactionType.None;
    private Map<ConfigurableProperty,Argument> arguments = new HashMap();
    private Map<InputProperty,DataColumn> declaredColumns = new HashMap();
    private boolean usesSecman = false;

    public TIAAdapter(EtlTaskComplexType ct, InputTaskComplexType xml, ETLJobWrapper w)
        throws ETLException
    {
        super(ct, xml, w);
        setData(dataSet);

        if ( xml.getTiaInput() == null )
        {
            throw new ETLException("Expecting TIAInput in InputAdapter");
        }

        TiaInputTaskComplexType in = xml.getTiaInput();
        ta = in;
        config = in.getTiaConfig();
        setTIARequestTemplate(in);
    }

    public TIAAdapter(EtlTaskComplexType ct, OutputTaskComplexType xml, ETLJobWrapper w)
        throws ETLException
    {
        super(ct, xml, w);

        if ( xml.getTiaOutput() == null )
        {
            throw new ETLException("Expecting TIAOutput in OutputAdapter");
        }

        TiaOutputTaskComplexType out = xml.getTiaOutput();
        ta = out;
        config = out.getTiaConfig();
        //setConnectionInfo(xml.getConnectionInfo());
        setTIARequestTemplate(out);
    }

    public Object getBaseObject()
    {
        return ta;
    }

    public Adapter.Type getAdapterType()
    {
        return Adapter.Type.TIA;
    }

    @Override
    public RelationalContainer getData()
        throws ETLException
    {
        setData(super.getData());
        return data;
    }

    @Override
    public void setData(DataSet d)
    {
        super.setData(d);
        if ( d.getType() != DataSet.Type.relational )
        {
            throw new UnsupportedOperationException("DataSet assigned to " + this.getClass().getSimpleName() + " must be relational");
        }
        data = (RelationalContainer)d;
    }

    public TIAConnection getConnectionInfo()
    {
        return tiainfo;
    }

    public boolean usesSecurityManager()
    {
        return usesSecman;
    }

    public TIATransactionType getTransactionType()
    {
        return ttype;
    }

    private void setConnectionInfo(TiaConfigComplexType info)
        throws ETLException
    {
        usesSecman = info.isUseSecurityManager();
        tiainfo = ConnectionFactory.getTIAConnection(info, this);
    }

    private void setTIARequestTemplate(TiaInputTaskComplexType ct)
        throws ETLException
    {
        if ( ct != null )
        {
            ttype = TIATransactionType.valueOf(ct.getRequest().getTransactionType());
            setArguments(ct.getRequest());
            setInputColumnDefinitions(ct.getInputData());
        }
    }

    private void setTIARequestTemplate(TiaOutputTaskComplexType ct)
        throws ETLException
    {
        if ( ct != null )
        {
            ttype = TIATransactionType.valueOf(ct.getRequest().getTransactionType());
            setArguments(ct.getRequest());
        }
    }

    private void setArguments(TiaRequestComplexType rct)
        throws ETLException
    {
        arguments.clear();
        if ( rct != null )
        {
            arguments.put(ConfigurableProperty.cardNumber, new Argument(rct.getCardNumber(), this));
            arguments.put(ConfigurableProperty.tenderNumber, new Argument(rct.getTenderNumber(), this));

            // If amount is defined pass it, otherwise don't
            if ( rct.getAmount() != null )
            {
                arguments.put(ConfigurableProperty.amount, new Argument(rct.getAmount(), this));
            }

            // If pin is defined pass it, otherwise don't
            if ( rct.getPin() != null )
            {
                arguments.put(ConfigurableProperty.pin, new Argument(rct.getPin(), this));
            }

            // If manual entry isn't defined set to true
            if ( rct.getManualEntry() == null )
            {
                Argument arg = new Argument(Argument.Type.constant, DataTypes.BOOLEAN, "true");
                arguments.put(ConfigurableProperty.manualEntry, arg);
            }
            else
            {
                arguments.put(ConfigurableProperty.manualEntry, new Argument(rct.getManualEntry(), this));
            }

            // If online isn't defined set to true
            if ( rct.getOnline() == null )
            {
                Argument arg = new Argument(Argument.Type.constant, DataTypes.BOOLEAN, "true");
                arguments.put(ConfigurableProperty.online, arg);
            }
            else
            {
                arguments.put(ConfigurableProperty.online, new Argument(rct.getOnline(), this));
            }
        }
    }

    private void setInputColumnDefinitions(TiaInputTaskComplexType.InputData input)
        throws ETLException
    {
        if ( input != null )
        {
            for ( JAXBElement<DataComplexType> elem: input.getBalanceOrMessageOrResponseCode() )
            {
                DataComplexType ct = elem.getValue();
                DataColumn col = data.addColumnDefinition(String.valueOf(ct.getType()), ct.getValue(), ct.getDefaultValue());
                declaredColumns.put(InputProperty.valueOf(elem.getName().getLocalPart()), col);
            }
        }
    }

    private TIAMessenger initiateTIAConnection()
        throws ETLException
    {
        TIAMessenger msg = null;
        Date start = new Date();

        try
        {
            logger.debug("Initiating tia connection using: " + tiainfo);
            TIAConfiguration config = TIAConfiguration.initialize(null, wrapper.getETLRoot());
            logger.debug("TIAConfig: " + config);
            msg = new TIAMessenger(config, tiainfo);
        }
        catch (TIAException e)
        {
            throw new ETLException("Unable to initialize TIA connection using: " + tiainfo, e);
        }
        catch (IOException e)
        {
            throw new ETLException("Unable to connect to TIA service using: " + tiainfo, e);
        }
        finally
        {
            logger.debug("initiateJDBCConnection() took " + (double)(new Date().getTime() - start.getTime())/1000.0 +
                         " seconds to process.");

        }
        return msg;
    }

    public RelationalContainer initiateRequest()
        throws ETLException
    {
        DataColumn ecol = new DataColumn(DataTypes.STRING, RelationalContainer.COL_ERROR_MSG);
        DataColumn rcol = new DataColumn(DataTypes.STRING, COL_RESPONSE_CODE);
        DataColumn tcol = new DataColumn(DataTypes.STRING, COL_TIA_REQUEST);
        RelationalContainer errors = new RelationalContainer(data.getDataName() + ERROR_DATA_NAME);
        errors.setColumnDefinitions(data.getColumnDefinitions());
        errors.addColumnDefinition(ecol);
        errors.addColumnDefinition(rcol);
        errors.addColumnDefinition(tcol);

        // Set the connection info at runtime (to support dynamic global variable modification)
        setConnectionInfo(config);

        TIAMessageRequest request = null;
        Date now = new Date();

        try
        {
            int count = data.rowCount();
            messenger = initiateTIAConnection();
            logger.debug("TIA request has following arguments: " + arguments);
            Iterator<Map<String,DataCell>> it = data.getMappedData().iterator();
            iterate2: for ( int i=1; it.hasNext(); i++ )
            {
                data.setCurrentProcessedRow(i);
                System.out.println(getTaskName() + ": Executing tia request on row #" + i + " out of " + count);
                Map<String,DataCell> map = it.next();
                request = messenger.initializeNewRequest();
                now = new Date();
                request.setTransactionType(ttype);
                request.setDatetime(now);

                logger.debug("Executing tia request on row #" + i);
                int j = 0;
                for ( Map.Entry<ConfigurableProperty,Argument> entry: arguments.entrySet() )
                {
                    j++;
                    ConfigurableProperty prop = entry.getKey();
                    Argument arg = entry.getValue();
                    Object val = null;
                    String argname = "";
                    if ( arg.getArgumentType() == Argument.Type.column )
                    {
                        argname = String.valueOf(arg.getValue());
                        DataCell cell = map.get(argname);
                        if ( cell != null )
                        {
                            val = cell.getValue();
                        }
                        else if ( data.getColumnDefinition(argname) != null )
                        {
                            val = null;
                        }
                        else
                        {
                            logger.debug("Undefined column " + arg + ", unable to execute request with correct data.");
                            it.remove();
                            map.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, "Undefined column " + arg + ", unable to execute request with correct data."));
                            map.put(COL_TIA_REQUEST, new DataCell(tcol, null));
                            map.put(COL_RESPONSE_CODE, new DataCell(rcol, null));
                            errors.addDataRow(map);
                            continue iterate2;
                        }
                    }
                    else
                    {
                        argname = "Constant";
                        val = arg.getValue();
                    }

                    logger.debug("Property " + prop + " assigned argument #" + j + " '" + argname + "'=" + val);
                    String datatype = "";
                    try
                    {
                        switch (prop)
                        {
                            case cardNumber:
                                request.setCardNumber(String.valueOf(val));
                                break;
                            case tenderNumber:
                                datatype = "integer";
                                int tnum = Integer.parseInt(String.valueOf(val));
                                request.setTenderNumber(tnum);
                                break;
                            case amount:
                                datatype = "double";
                                double amt = Double.parseDouble(String.valueOf(val));
                                request.setTransactionAmount(amt);
                                break;
                            case pin:
                                datatype = "integer";
                                int pin = Integer.parseInt(String.valueOf(val));
                                request.setPin(pin);
                                break;
                            case manualEntry:
                                datatype = "boolean";
                                boolean me = Boolean.valueOf(String.valueOf(val));
                                request.setManualEntry(me);
                                break;
                            case online:
                                datatype = "boolean";
                                boolean on = Boolean.valueOf(String.valueOf(val));
                                request.setOnline(on);
                                break;
                        }
                    }
                    catch (NumberFormatException e)
                    {
                        logger.debug("Unable to assign tia property, expecting " + datatype + ": " + prop, e);
                        it.remove();
                        map.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, "Unable to assign tia property, expecting " + datatype + ": " + prop + ": " + e.getMessage()));
                        map.put(COL_TIA_REQUEST, new DataCell(tcol, null));
                        map.put(COL_RESPONSE_CODE, new DataCell(rcol, null));
                        errors.addDataRow(map);
                        continue iterate2;
                    }
                }
                
                TIAResponseObject robj = null;
                boolean success = false;
                for ( int tnum=1, retries=3; !success && retries>0; tnum++, retries-- )
                {
                    try
                    {
                        logger.debug("TIA Request attempt #" + tnum);
                        TIAMessageResponse response = messenger.sendMessageRequest();
                        success = true;
                        logger.info("#" + j + ": TIA Request '" + request + "' took " + (new Date().getTime()-now.getTime()) + " ms");
                        logger.debug("TIA Response: " + response.getUnencryptedMessage());
                        robj = new TIAResponseObject(response);
                        logger.debug("Parse response: " + robj);
                    }
                    catch (TIAMessageException e)
                    {
                        Throwable cause = e.getCause();
                        if ( cause instanceof SocketException && retries > 0 )
                        {
                            // retry
                            continue;
                        }
                        else
                        {
                            logger.warn("Failed to perform tia transaction", e);
                            String msg = "Failed to perform tia transaction due to connection issue: " + e.getMessage();
                            logger.warn(msg + " | Rowdata: " + map);
                            it.remove();
                            map.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, msg));
                            map.put(COL_TIA_REQUEST, new DataCell(tcol, request.getUnencryptedMessage()));
                            errors.addDataRow(map);
                        }
                    }
                }

                if ( robj != null && robj.isTransactionSuccessful() )
                {
                    // Add a copy of the current row to each returned row
                    for ( Map.Entry<InputProperty,DataColumn> entry: declaredColumns.entrySet() )
                    {
                        InputProperty prop = entry.getKey();
                        DataColumn col = entry.getValue();

                        switch (prop)
                        {
                            case balance:
                                map.put(col.getColumnName(), new DataCell(col, robj.getBalance()));
                                break;
                            case message:
                                map.put(col.getColumnName(), new DataCell(col, robj.getMessage()));
                                break;
                            case responseCode:
                                map.put(col.getColumnName(), new DataCell(col, robj.getResponseCode()));
                                break;
                            case sequenceNumber:
                                map.put(col.getColumnName(), new DataCell(col, robj.getSequenceNumber()));
                                break;
                        }
                    }
                }
                else if ( robj != null )
                {
                    String msg = "Unsuccessful tia transaction because: " + robj.getMessage() + "(" + robj.getResponseCode() + ")";
                    logger.warn(msg + " | Rowdata: " + map);
                    it.remove();
                    map.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, msg));
                    map.put(COL_RESPONSE_CODE, new DataCell(rcol, robj.getResponseCode()));
                    map.put(COL_TIA_REQUEST, new DataCell(tcol, request.getUnencryptedMessage()));
                    errors.addDataRow(map);
                }
                else 
                {
                    String msg = "Unsuccessful tia transaction because: Failed to connect to TIA service";
                    logger.warn(msg + " | Rowdata: " + map);
                    it.remove();
                    map.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, msg));
                    map.put(COL_RESPONSE_CODE, new DataCell(rcol, robj.getResponseCode()));
                    map.put(COL_TIA_REQUEST, new DataCell(tcol, request.getUnencryptedMessage()));
                    errors.addDataRow(map);                
                }
            }
        }
        catch (IOException e)
        {
            throw new ETLException("Unable to create a new tia request", e);
        }
        catch (TIAException e)
        {
            throw new ETLException("Unable to execute tia request: " + request.toString() , e);
        }

        return errors;
    }

    private String escapeForRegex(String input)
    {
        String val = input.replace("$", "\\$");
        val = val.replace("\\", "\\\\");
        return val;
    }

    public String toString()
    {
        StringBuffer out = new StringBuffer();
        out.append("{");
        out.append("TaskName=" + name);
        out.append(", AdapterType=" + getAdapterType());
        out.append(", NextTask=" + next);
        out.append(", Direction=" + direction);
        out.append(", Connection=" + tiainfo);
        out.append(", TransactionType=" + ttype);
        out.append("}");
        return out.toString();
    }

}
