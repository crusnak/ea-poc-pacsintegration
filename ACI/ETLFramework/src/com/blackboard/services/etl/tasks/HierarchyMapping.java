/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.tasks;

import com.blackboard.services.etl.data.Argument;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.etl.data.HierarchyContainer;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.data.TransformTarget;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.exception.IndexException;
import com.blackboard.services.etl.exception.TransformException;
import com.blackboard.services.etl.exception.TransformIgnoreException;
import com.blackboard.services.etl.exception.VariableTypeMismatchException;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.EtlTaskComplexType;
import com.blackboard.services.etl.jaxb.HierarchyMappingTaskComplexType;
import com.blackboard.services.etl.jaxb.HierarchyTransformChoiceComplexType;
import com.blackboard.services.etl.jaxb.TransformComplexType;
import com.blackboard.services.etl.tasks.DataSorter.SortColumn;
import com.blackboard.services.etl.transforms.AbstractTransform;
import com.blackboard.services.etl.transforms.TransformFactory;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.blackboard.services.utils.schema.SchemaDocument;
import com.blackboard.services.utils.exception.CompilationException;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author crusnak
 */
public class HierarchyMapping extends AbstractTask
{
    public static enum MappingType { replace, add }
    public static enum ConvertType { relationalToHierarchical, hierarchicalToRelational };
    public static enum OutputType { json, xml };
    
    private File fileRoot = null;
    private HierarchyMappingTaskComplexType hmap = null;
    private String schemaRef = null;
    private SchemaDocument schema = null;
    private MappingType mtype = MappingType.replace;
    private ConvertType ctype = ConvertType.relationalToHierarchical;
    private String source = null;
    private String target = null;
    private DataSet incomingData = null;
    private DataSet outgoingData = null;
    private HierarchyTransforms oneToOne = null;
    private List<HierarchyTransforms> oneToMany = new ArrayList();
    
    public HierarchyMapping(EtlTaskComplexType ct, HierarchyMappingTaskComplexType map, ETLJobWrapper w, File root)
        throws ETLException
    {
        super(ct, w);
        
        try
        {
            fileRoot = root;
            hmap = map;
            schemaRef = map.getSchemaRef();
            schema = new SchemaDocument(fileRoot, schemaRef);
            
            source = ((map.getSource()!=null)?map.getSource():name);
            target = ((map.getTarget()!=null)?map.getTarget():name);
            setMappingType(map.getMapType());
            setConversionType(map.getConvertType());
            
            oneToOne = new HierarchyTransforms(map.getOneToOne(), this);

            for ( HierarchyTransformChoiceComplexType otm: map.getOneToMany() )
            {
                oneToMany.add(new HierarchyTransforms(otm, this));
            }
        }
        catch ( CompilationException e)
        {
            throw new ETLException("Failed to initialize HierarchyMapping", e);
        }
    }
    
    @Override
    public ClassType getClassType()
    {
        return ClassType.HierarchyMapping;
    }

    @Override
    public HierarchyMappingTaskComplexType getBaseObject()
    {
        return hmap;
    }

    public String getSource()
    {
        return source;
    }

    public String getTarget()
    {
        return target;
    }
    
    public MappingType getMappingType()
    {
        return mtype;
    }

    private void setMappingType(String type)
    {
        mtype = MappingType.valueOf(type);
    }

    public ConvertType getConversionType()
    {
        return ctype;
    }

    private void setConversionType(String type)
    {
        ctype = ConvertType.valueOf(type);
    }

    /*public String toString()
    {
        StringBuilder out = new StringBuilder();
        //out.append("
        
        return out.toString();
    }*/
 

    public RelationalContainer mapData(String jobnum)
        throws ETLException
    {
        logger.debug("HierarchyMapping: " + this);
        DataSet src = null;
        DataSet dest = null;
        DataSet wip = null;
        boolean taskDataAsSource = false;
        
        logger.info("Loading SourceData: " + getSource());
        DataSet ds = wrapper.retrieveNamedData(getSource());
        if ( ds == null )
        {
            ds = dataSet;
            taskDataAsSource = true;
            logger.info("No source data referenced, using assigned task data");
            if ( ds.rowCount() == 0 )
            {
                logger.warn("Assigned task data has no rows, performing no data mapping");
            }
        }
        
        src = ds;
        incomingData = src;
        logger.info("SourceData loaded, contains " + src.rowCount() + " rows of data");
        logger.trace("SourceData: " + src);

        RelationalContainer errors = new RelationalContainer(getTaskName() + AbstractTask.ERROR_DATA_NAME);
        DataColumn ecol = new DataColumn(DataTypes.STRING, RelationalContainer.COL_ERROR_MSG);
        DataColumn scol = new DataColumn(DataTypes.STRING, DataMapping.COL_CELL_SOURCES);
        DataColumn dcol = new DataColumn(DataTypes.STRING, DataMapping.COL_CELL_ERROR);
        errors.setColumnDefinitions(getColumnDefinitionsFromDataSet(src));
        errors.addColumnDefinition(ecol);
        errors.addColumnDefinition(scol);
        errors.addColumnDefinition(dcol);

        logger.info("Loading TargetData: " + getTarget());
        ds = wrapper.retrieveNamedData(getTarget());
        if ( ds == null )
        {
            logger.warn("Undefined target data: " + getTarget());
            logger.info("Target data undefined, assign data to mapping task");
            if ( !taskDataAsSource )
            {
                ds = dataSet;
            }
            else
            {
                throw new ETLException("Task data has previously been assigned as source, cannot also be used as destination.");
            }
        }

        dest = ds;
        outgoingData = dest;
        logger.info("TargetData loaded, contains " + dest.rowCount() + " rows of data");
        
        // Validate the source data with the conversion type
        if ( ctype == ConvertType.relationalToHierarchical && src.getType() == DataSet.Type.hierarchical )
        {
            throw new ETLException("Cannot convert relational data to hierarchical if the source data is hierarchical");
        }
        else if ( ctype == ConvertType.hierarchicalToRelational && src.getType() == DataSet.Type.relational )
        {
            throw new ETLException("Cannot convert hierarchical data to relational if the source data is relational");
        }
        
        // Prepare the target data set based on the conversion type
        if ( ctype == ConvertType.relationalToHierarchical )
        {
            logger.info("SourceData is relational, converting to hierarchical");
            if ( dest.getType() != DataSet.Type.hierarchical )
            {
                if ( dest.rowCount() > 0 )
                {
                    throw new ETLException("Cannot convert relational data to hierarchical when target data contains existing relational data");
                    
                }
                else
                {
                    AbstractTask task = wrapper.getJobTaskByName(getTarget());
                    dest = new HierarchyContainer(getTarget(), schema, oneToOne.getRootXpath());
                    outgoingData = dest;
                    task.setData(dest);
                }
            }

            HierarchyContainer target = (HierarchyContainer)outgoingData;
            wip = new HierarchyContainer(target.getDataName(), target.getSchema(), target.getRootXpath());
            wip.setColumnDefinitions(target.getColumnDefinitions());
        }
        else if ( ctype == ConvertType.hierarchicalToRelational )
        {
            logger.info("SourceData is hierarchical, converting to relational");
            if ( dest.getType() != DataSet.Type.relational )
            {
                if ( dest.rowCount() > 0 )
                {
                    throw new ETLException("Cannot convert hierarchical data to relational when target data contains existing hierarchical data");
                }
                else
                {
                    AbstractTask task = wrapper.getJobTaskByName(getTarget());
                    dest = new RelationalContainer(getTarget());
                    outgoingData = dest;
                    task.setData(dest);
                }
            }

            RelationalContainer target = (RelationalContainer)outgoingData;
            wip = new RelationalContainer(target.getDataName());
            wip.setColumnDefinitions(target.getColumnDefinitions());
        }

        // If the mapping type is replace, clear out all the destination data first
        logger.info("DataMapping type: " + mtype);
        if ( mtype == MappingType.replace )
        {
            dest.clearData();
            logger.info("Replacing all destination data with transformed source data.");
        }

        int rowcount = src.rowCount();
        int destRowCount = dest.rowCount();
        switch (ctype)
        {
            case relationalToHierarchical:
                RelationalContainer source = (RelationalContainer)incomingData;
                if ( !oneToOne.getTransforms().isEmpty() )
                {
                    HashMap<String,Object> last = new HashMap();
                    for ( int i=1; i<=src.rowCount(); i++ )
                    {
                        Map<String,DataCell> rowdata = source.getMappedData().get(i-1);
                        wip.setCurrentProcessedRow(i);
                        System.out.println(getTaskName() + ": Converting row #" + i + " out of " + rowcount);
                        /*try
                        {
                            int count = 0;
                            logger.debug("----------------PROCESSING ROW #" + i + "----------------");

                            for ( SortColumn col: oneToOne.getPrimaryKeys() )
                            {
                                Object val = null;
                                if ( rowdata.containsKey(col.getColumnName()) )
                                {
                                    val = rowdata.get(col.getColumnName());
                                }
                                
                                // TODO: check last map to see if values of primary keys changed indicating
                                // a new one-to-one record
                            }

                            
                            // Perform each transform defined in the data mapping for this row
                            for ( AbstractTransform transform: oneToOne.getTransforms() )
                            {
                                try
                                {
                                    logger.debug("Performing " + transform.getTransformType() + " transform: " + transform);

                                    List<DataCell> sources = extractSourceData(wip, transform, i-1);
                                    logger.debug("Source cell(s): " + sources);

                                    List<DataColumn> targets = extractTargetColumns(outgoingData, transform);
                                    logger.debug("Target column(s): " + targets);

                                    List<DataCell> tcells = transform.transform(targets, sources);
                                    for ( int j=0; j<tcells.size(); j++ )
                                    {
                                        /*DataCell tcell = tcells.get(j);
                                        TransformTarget target = transform.getTargetColumns().get(j);
                                        if ( targets.get(j) == null || target.isTargetDataTypeOverridden() )
                                        {
                                            logger.info("No definition for column '" + target.getColumnName() + "' in " +
                                                        "target data '" + getTarget() + "'. Adding column to data.");
                                            dest.addColumnDefinition(tcell.getColumn().getDataType(),
                                                                     target.getColumnName(), target.getDefaultValue());
                                        }
                                        logger.debug("<------------- OUTPUT: " + tcell + " -------------->");
                                        map.put(target.getColumnName(), tcell);
                                        wip.getMappedData().get(i-1).put(target.getColumnName(), tcell);
                                    }

                                    count++;
                                }
                                catch (TransformIgnoreException e)
                                {
                                    logger.info("Transform error defined as ignorable: " + e.getMessage());
                                }
                            }
                        }
                        catch (VariableTypeMismatchException e)
                        {
                            logger.error("Unable to assign value to incoming source", e);
                            HashMap<String,DataCell> map = new HashMap(source.getMappedData().get(i-1));
                            map.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, e.getMessage()));
                            errors.addDataRow(map);
                            continue;
                        }
                        catch (IndexException e)
                        {
                            logger.error("Failed to index column data", e);
                            HashMap<String,DataCell> map = new HashMap(source.getMappedData().get(i-1));
                            map.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, e.getMessage()));
                            errors.addDataRow(map);
                            continue;
                        }
                        catch (TransformException e)
                        {
                            logger.error("Failed to transform column data", e);
                            HashMap<String,DataCell> map = new HashMap(source.getMappedData().get(i-1));
                            DataCell cell = e.getDataCell();
                            map.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, e.getMessage()));
                            map.put(DataMapping.COL_CELL_ERROR, new DataCell(dcol, cell));

                            if ( cell != null )
                            {
                                map.put(DataMapping.COL_CELL_SOURCES, new DataCell(scol, cell.getColumn().getColumnName()));
                            }                    
                            else
                            {
                                map.put(DataMapping.COL_CELL_SOURCES, new DataCell(scol, null));
                            }

                            errors.addDataRow(map);
                            continue;
                        }*/
                    }
                }
                
                break;
            case hierarchicalToRelational:
                throw new UnsupportedOperationException("Hierarchical to relational data mapping not supported yet");
                //break;
        }

        return errors;
    }
    
    private Map<String,DataColumn> getColumnDefinitionsFromDataSet(DataSet src)
        throws ETLException
    {
        Map<String,DataColumn> cols = new HashMap();
        
        if ( src != null && src.getType() == DataSet.Type.relational )
        {
            cols = ((RelationalContainer)src).getColumnDefinitions();
        }
        else
        {
            throw new UnsupportedOperationException("Converting hierarchical columns to relational not supported yet");
        }
        
        return cols;
    }

    public static class HierarchyTransforms
    {
        private HierarchyMapping parent = null;
        private HierarchyTransformChoiceComplexType ht = null;
        private List<DataSorter.SortColumn> primaryKeys = new ArrayList();
        private List<AbstractTransform> transforms = new ArrayList();
        private String rootXpath = null;
        private boolean ignoreNulls = true;
        
        public HierarchyTransforms(HierarchyTransformChoiceComplexType ct, HierarchyMapping hm)
            throws ETLException
        {
            ht = ct;
            parent = hm;
            rootXpath = ct.getRootXpath();
            ignoreNulls = ct.isIgnoreNulls();
            setPrimaryKeys(ct.getKeys());
            setTransforms(ct.getTransformGroup());
        }
        
        public HierarchyTransformChoiceComplexType getBaseObject()
        {
            return ht;
        }

        public String getRootXpath()
        {
            return rootXpath;
        }

        public boolean isIgnoreNulls()
        {
            return ignoreNulls;
        }

        public List<SortColumn> getPrimaryKeys()
        {
            return primaryKeys;
        }

        private void setPrimaryKeys(HierarchyTransformChoiceComplexType.Keys keys)
        {
            for ( String column: keys.getColumn() )
            {
                primaryKeys.add(new DataSorter.SortColumn(column, DataSorter.SortColumn.SortType.ascending));
            }
        }

        public List<AbstractTransform> getTransforms()
        {
            return transforms;
        }
        
        private void setTransforms(List<TransformComplexType> tforms)
            throws ETLException
        {
            try
            {
                for ( TransformComplexType tform: tforms )
                {
                    AbstractTransform tf = TransformFactory.createTransform(tform, parent);
                    transforms.add(tf);

                    // Validate xpaths against schema
                    for ( TransformTarget target: tf.getTargetColumns() )
                    {
                        parent.schema.validateXpath(rootXpath + target.getColumnName());
                    }
                }
            }
            catch ( CompilationException e)
            {
                throw new ETLException("Failed to setTransforms in HierarchyMapping", e);
            }
        }
    }
}
