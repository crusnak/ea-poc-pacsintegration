/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.tasks.web;

import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.XpathColumn;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.jaxb.WebAPITaskComplexType;
import com.blackboard.services.etl.jaxb.WebConfigComplexType;
import com.blackboard.services.security.object.WebConnection;
import com.blackboard.services.utils.ComparableNameValuePair;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import org.apache.http.Header;
import org.apache.http.NameValuePair;

/**
 *
 * @author crusnak
 */
public interface WebAPIDriver 
{
    public static enum DriverType { HTTP, REST };
    public static enum RequestMethod { GET, PUT, POST, DELETE, OPTIONS, PATCH };
    public static enum ContentType { application_xml, application_json, application_html, text_json, text_xml, application_x_www_form_urlencoded };
    public static enum JoinType { none, inner, outer };
    public static enum NodeType { ELEMENT_NODE , ATTRIBUTE_NODE, TEXT_NODE, CDATA_SECTION_NODE, ENTITY_REFERENCE_NODE, ENTITY_NODE, PROCESSING_INSTRUCTION_NODE, COMMENT_NODE, DOCUMENT_NODE, DOCUMENT_TYPE_NODE, DOCUMENT_FRAGMENT_NODE, NOTATION_NODE }   
    public static enum AfterExecute { none, invalidateToken };
        
    public Object getBaseObject();
    public DriverType getDriverType();
    public RequestMethod getRequestMethod();
    public void setRequestMethod(RequestMethod rm);
    public ContentType getContentType();
    public void setContentType(ContentType ct, String jsonRoot);
    public JoinType getJoinType();
    public void setJoinType(JoinType t);
    public AfterExecute getAfterExecution();
    public void setAfterExcecution(AfterExecute ae);
    public String getBaseURL();
    public URL getURL(Map<String,DataCell> rowdata) throws ETLException;
    public void setURL(String url) throws ETLException;
    public void setAPIURI(String uri) throws ETLException;
    public WebConnection getConnectionInfo();
    public void clearHeaders();
    public List<Header> getHeaders();
    public void addHeader(Header h);
    public void setHeaders(List<Header> headers);
    public void clearParameters();
    public List<NameValuePair> getParameters();
    public List<ComparableNameValuePair> getComparableParameters();
    public void addParameter(NameValuePair param);
    public void setParameters(List<NameValuePair> params);
    public void clearResponseColumns();
    public List<DataColumn> getResponseColumns();
    public void setResponseColumns(List<XpathColumn> cols) throws ETLException;
    public void addResponseColumn(XpathColumn col) throws ETLException;
    public void setConnectionInfo(WebConfigComplexType info) throws ETLException;
    public void setWebConnection(WebConnection con);
    public void setRequest(WebAPITaskComplexType.Http.Request request) throws ETLException, MalformedURLException;
    public void setResponse(WebAPITaskComplexType.Http.Response response) throws ETLException;
    public List<Map<String,DataCell>> execute(Map<String,DataCell> data, boolean handleAuth) throws ETLException;
    public List<Map<String,DataCell>> execute(Map<String,DataCell> data, boolean handleAuth, boolean addResponse) throws ETLException;
    public void closeConnection() throws IOException;
}
