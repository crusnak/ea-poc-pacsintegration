/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.tasks;

/**
 *
 * @author crusnak
 */
public interface Adapter extends ETLTask
{
    public static enum Type { Database, File, Defined, Email, TIA, Binary, Interval };

    public DataDirection getDataDirection();
    public Type getAdapterType();
}
