/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.tasks;

import com.blackboard.services.etl.jaxb.EtlTaskComplexType;
import com.blackboard.services.etl.jaxb.InputTaskComplexType;
import com.blackboard.services.etl.jaxb.OutputTaskComplexType;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;

/**
 *
 * @author crusnak
 */
public abstract class AbstractAdapter extends AbstractTask implements Adapter
{
    protected DataDirection direction = null;

    public AbstractAdapter()
    {
    }

    public AbstractAdapter(EtlTaskComplexType ct, InputTaskComplexType in, ETLJobWrapper wrapper)
    {
        super(ct, wrapper);
        direction = DataDirection.Incoming;
    }

    public AbstractAdapter(EtlTaskComplexType ct, OutputTaskComplexType out, ETLJobWrapper wrapper)
    {
        super(ct, wrapper);
        direction = DataDirection.Outgoing;
    }

    @Override
    public DataDirection getDataDirection()
    {
        return direction;
    }

    @Override
    public ClassType getClassType()
    {
        return ClassType.Adapter;
    }
}
