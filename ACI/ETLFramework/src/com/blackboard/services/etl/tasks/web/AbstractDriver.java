/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.tasks.web;

import com.blackboard.services.etl.data.Argument;
import com.blackboard.services.etl.data.BbJSONContainer;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.etl.data.JacksonJsonContainer;
import com.blackboard.services.etl.data.WebArgument;
import com.blackboard.services.etl.data.XpathColumn;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.jaxb.ArgumentComplexType;
import com.blackboard.services.etl.jaxb.ArgumentsComplexType;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.HierarchyTransformComplexType;
import com.blackboard.services.etl.jaxb.ObjectFactory;
import com.blackboard.services.etl.jaxb.TransformComplexType;
import com.blackboard.services.etl.jaxb.WebAPITaskComplexType;
import com.blackboard.services.etl.jaxb.WebConfigComplexType;
import com.blackboard.services.etl.jaxb.XpathComplexType;
import com.blackboard.services.etl.tasks.AbstractTask;
import com.blackboard.services.etl.tasks.NullTask;
import com.blackboard.services.etl.tasks.WebAPIDispatcher;
import com.blackboard.services.etl.transforms.HierarchyTransform;
import com.blackboard.services.etl.transforms.TransformFactory;
import com.blackboard.services.etl.wrapper.ConnectionFactory;
import com.blackboard.services.etl.wrapper.DataArchiver;
import com.blackboard.services.security.OAuthManager;
import com.blackboard.services.security.object.WebConnection;
import com.blackboard.services.security.object.oauth.OAuthParameters;
import com.blackboard.services.utils.BbJSONObject;
import com.blackboard.services.utils.ComparableHeader;
import com.blackboard.services.utils.ComparableNameValuePair;
import com.blackboard.services.utils.JavaUtils;
import com.blackboard.services.utils.XMLUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.google.common.collect.HashBiMap;
//import com.sun.webkit.dom.NodeListImpl;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicHeaderValueParser;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.message.HeaderValueParser;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author crusnak
 */
public abstract class AbstractDriver implements WebAPIDriver
{
    public static final String HEADER_AUTH_NAME =               "Authorization";
    public static final String HEADER_AUTH_BASIC_VALUE =        "Basic";
    public static final String HEADER_CONTENT_TYPE_NAME =       "Content-Type";
    public static final String HEADER_CT_CHARSET_NAME =         "charset";
    
    private static final int OAUTH_MAX_RETRY_COUNT =            1;
    
    protected WebAPITaskComplexType xml = null;
    protected WebAPIDispatcher task = null;
    protected Logger logger = null;

    protected DriverType type = null;
    protected RequestMethod requestMethod = null;
    protected ContentType contentType = null;
    protected JoinType joinType = JoinType.none;
    protected AfterExecute afterExecute = AfterExecute.none;
    protected WebConnection webInfo = null;
    protected List<Argument> headers = new ArrayList();
    protected List<Header> httpHeaders = new ArrayList();
    protected List<WebArgument> params = new ArrayList();
    protected List<NameValuePair> httpParameters = new ArrayList();
    protected Map<String,XpathColumn> declaredColumns = new LinkedHashMap();
    protected String jsonRootElement = null;
    protected WebArgument url = null;
    protected int retryCount = 0;
    protected String invalidPathHandler = String.valueOf(HierarchyTransform.InvalidPath.NULL);
        
    protected static HashBiMap<String,ContentType> contentMap = HashBiMap.create();
    protected static HashMap<Integer,NodeType> nodeTypeMap = new HashMap();
    
    static
    {
        contentMap.put("application/xml", ContentType.application_xml);
        contentMap.put("application/json", ContentType.application_json);
        contentMap.put("application/html", ContentType.application_html);
        contentMap.put("text/json", ContentType.text_json);
        contentMap.put("text/xml", ContentType.text_xml);
        contentMap.put("application/x-www-form-urlencoded", ContentType.application_x_www_form_urlencoded);
        
        nodeTypeMap.put(1, NodeType.ELEMENT_NODE);
        nodeTypeMap.put(2, NodeType.ATTRIBUTE_NODE);
        nodeTypeMap.put(3, NodeType.TEXT_NODE);
        nodeTypeMap.put(4, NodeType.CDATA_SECTION_NODE);
        nodeTypeMap.put(5, NodeType.ENTITY_REFERENCE_NODE);
        nodeTypeMap.put(6, NodeType.ENTITY_NODE);
        nodeTypeMap.put(7, NodeType.PROCESSING_INSTRUCTION_NODE);
        nodeTypeMap.put(8, NodeType.COMMENT_NODE);
        nodeTypeMap.put(9, NodeType.DOCUMENT_NODE);
        nodeTypeMap.put(10, NodeType.DOCUMENT_TYPE_NODE);
        nodeTypeMap.put(11, NodeType.DOCUMENT_FRAGMENT_NODE);
        nodeTypeMap.put(12, NodeType.NOTATION_NODE);
    }
    
    public static WebAPIDriver getDriver(WebAPITaskComplexType ct, WebAPIDispatcher task)
        throws ETLException
    {
        WebAPIDriver driver = null;
        
        switch (ct.getHttp().getRequestType())
        {
            case "REST":
                driver = new RESTDriver(ct, task);
                break;
            case "HTTP":
                driver = new HTTPDriver(ct, task);
                break;
            default:
                throw new ETLException("Unsupported request type for web-api task: " + ct.getHttp().getRequestType() + ". Supported request types: " + JavaUtils.enumToList(DriverType.class));
        }
        
        return driver;
    }
    
    /**
     * This is a specific factory creator method to return a base HTTP Driver class. It is the responsibility of the caller to call the appropriate methods below to prepare the driver for usage.
     * 
     * @param type DriverType of the driver to create. Either REST or HTTP.
     * @param task WebAPIDispatcher task or null if not attached to an ETL task.
     * @return Returns a base HTTPDriver instance.
     * @see #setRequestMethod(com.blackboard.services.etl.tasks.web.WebAPIDriver.RequestMethod) 
     * @see #setJoinType(com.blackboard.services.etl.tasks.web.WebAPIDriver.JoinType) 
     * @see #setContentType(com.blackboard.services.etl.tasks.web.WebAPIDriver.ContentType, java.lang.String) 
     * @see #setURL(java.lang.String) 
     * @see #setWebConnection(com.blackboard.services.security.object.WebConnection) 
     * @see #setHeaders(com.blackboard.services.etl.jaxb.WebAPITaskComplexType.Http.Request.Headers) 
     * @see #setParameters(java.util.List) 
     * @see #setResponseColumns(java.util.List) 
     */
    public static WebAPIDriver getBaseDriver(DriverType type, WebAPIDispatcher task)
    {
        WebAPIDriver driver = null;
        
        switch (type)
        {
            case HTTP:
                driver = new HTTPDriver(task);
                break;
            case REST:
                driver = new RESTDriver(task);
                break;
        }
        
        return driver;
    }

    protected AbstractDriver(WebAPIDispatcher at)
    {
        task = at;
        logger = Logger.getLogger(this.getClass());
    }
    
    protected AbstractDriver(WebAPITaskComplexType ct, WebAPIDispatcher at)
    {
        this(at);
        xml = ct;
    }
    
    @Override
    public Object getBaseObject()
    {
        return xml;
    }
    
    @Override
    public DriverType getDriverType()
    {
        return type;
    }
    
    @Override
    public RequestMethod getRequestMethod()
    {
        return requestMethod;
    }
    
    @Override
    public void setRequestMethod(RequestMethod rm)
    {
        requestMethod = rm;
    }
    
    protected void setRequestMethod(String method)
        throws IllegalArgumentException
    {
        requestMethod = RequestMethod.valueOf(method);
    }
    
    
    @Override
    public JoinType getJoinType()
    {
        return joinType;
    }
    
    @Override
    public void setJoinType(JoinType jt)
    {
        joinType = jt;
    }

    protected void setJoinType(String join)
    {
        joinType = JoinType.valueOf(join);
    }
    
    @Override
    public AfterExecute getAfterExecution() 
    {
        return afterExecute;
    }

    @Override
    public void setAfterExcecution(AfterExecute ae)
    {
        afterExecute = ae;
    }
    
    protected void setAfterExecution(String ae)
    {
        afterExecute = AfterExecute.valueOf(ae);
    }
    
    @Override
    public String getBaseURL()
    {
        return String.valueOf(url.getValue());
    }

    @Override
    public URL getURL(Map<String,DataCell> rowdata)
        throws ETLException
    {
        URL u = null;
        String enc = System.getProperty("file.encoding");
        
        try
        {
            if ( getRequestMethod() == RequestMethod.GET )
            {
                if ( httpParameters.size() > 0 )
                {
                    // Parameterize URL with httpParameters
                    StringBuilder params = new StringBuilder("?");
                    for ( int i=0; i<httpParameters.size(); i++ )
                    {
                        NameValuePair nvp = httpParameters.get(i);
                        params.append(URLEncoder.encode(nvp.getName(), enc)).append("=").append(URLEncoder.encode(nvp.getValue(), enc));
                        if ( i < httpParameters.size() - 1 )
                        {
                            params.append("&");
                        }
                    }

                    // Add params back to URL
                    WebArgument warg = new WebArgument(task);
                    warg.setArgument(Argument.Type.constant, DataTypes.STRING, String.valueOf(url.getValue()) + params.toString());
                    u = new URL(String.valueOf(replaceHierarchicalVariable(warg, rowdata)));
                }
                else
                {
                    u = new URL(String.valueOf(replaceHierarchicalVariable(url, rowdata)));
                }
            }
            else
            {
                u = new URL(String.valueOf(replaceHierarchicalVariable(url, rowdata)));
            }
        }
        catch (UnsupportedEncodingException e)
        {
            throw new ETLException("Unabled to URL encode using charset encoding: " + enc, e);
        }
        catch (MalformedURLException e)
        {
            throw new ETLException("Unable to create a valid URL from provided row data", e, rowdata);
        }
        
        return u;
    }
    
    @Override
    public void setURL(String u)
        throws ETLException
    {
        logger.trace("setURL(" + u + ")");
        url = new WebArgument(task);
        url.setArgument(Argument.Type.constant, DataTypes.STRING, u);
        url.setArgumentName("URL");
    }

    @Override
    public void setAPIURI(String uri) 
        throws ETLException
    {
        if ( webInfo.getBaseURI() != null )
        {
            setURL(webInfo.getBaseURI() + uri);
        }
        else
        {
            throw new ETLException("Unable to set API URI as base URI is not defined in the WebConnection object");
        }
    }
    
    

    @Override
    public void clearHeaders() 
    {
        httpHeaders.clear();
    }
    
    @Override
    public List<Header> getHeaders()
    {
        return new ArrayList(httpHeaders);
    }

    @Override
    public void addHeader(Header h) 
    {
        // Need to see if a header with the same header name already exists, if so replace it instead of adding
        if ( httpHeaders.contains(h) )
        {
            httpHeaders.set(httpHeaders.indexOf(h), h);
        }
        else
        {
            httpHeaders.add(h);
        }
    }
    
    @Override
    public void setHeaders(List<Header> h)
    {
        httpHeaders.clear();
        for ( Header header: h )
        {
            httpHeaders.add(header);
        }
    }

    @Override
    public void clearParameters() 
    {
        httpParameters.clear();
    }
    
    @Override
    public List<NameValuePair> getParameters()
    {
        return httpParameters;
    }
    
    @Override
    public List<ComparableNameValuePair> getComparableParameters()
    {
       List<ComparableNameValuePair> params = new ArrayList();
       
       for ( NameValuePair p: httpParameters )
       {
           params.add(new ComparableNameValuePair(p.getName(), p.getValue()));
       }
       
       return params;
    }

    @Override
    public void addParameter(NameValuePair param) 
    {
        httpParameters.add(param);
    }
    
    @Override
    public void setParameters(List<NameValuePair> params)
    {
        httpParameters = params;
    }

    @Override
    public void clearResponseColumns() 
    {
        declaredColumns.clear();
    }
    
    @Override
    public List<DataColumn> getResponseColumns()
    {
        return new ArrayList(declaredColumns.values());
    }

    @Override
    public void addResponseColumn(XpathColumn col)
        throws ETLException
    {
        declaredColumns.put(col.getColumnName(), col);

        if ( task != null && col.getDataType() != DataTypes.NODE )
        {
            task.getData().addColumnDefinition(col);
        }
    }
    
    @Override
    public void setResponseColumns(List<XpathColumn> cols)
        throws ETLException
    {
        for ( XpathColumn col: cols)
        {
            addResponseColumn(col);
        }
    }
    
    protected void setContentType(String type, String root)
    {
        ContentType ct = contentMap.get(type);
        if ( ct == null )
        {
            throw new IllegalArgumentException("Unsupported content type for http response: " + type + ". Supported content types: " + contentMap.keySet());
        }
        
        setContentType(ct, root);
    }
    
    @Override
    public void setContentType(ContentType ct, String root)
    {
        contentType = ct;
        
        switch (contentType)
        {
            case application_json:
            case text_json:
                jsonRootElement = root;
                if ( root == null || root.trim().length() == 0 )
                {
                    throw new IllegalArgumentException("JSON content types for http responses require the jsonRootElement attribute to be non-null and not empty");
                }
                break;
        }
    }
    
    @Override
    public ContentType getContentType()
    {
        return contentType;
    }

    public String getContentTypeString()
    {
        String ct = null;
        if ( contentType != null )
        {
            ct = contentMap.inverse().get(contentType);
        }
        return ct;
    }

    @Override
    public WebConnection getConnectionInfo()
    {
        return webInfo;
    }

    @Override
    public void setConnectionInfo(WebConfigComplexType info)
        throws ETLException
    {
        webInfo = ConnectionFactory.getWebConnection(info, task);
    }
    
    @Override
    public void setWebConnection(WebConnection conn)
    {
        webInfo = conn;
    }
    
    protected String processEmbeddedBeanScript(String input)
        throws ETLException
    {
        if ( task != null )
        {
            return task.getETLJobWrapper().getDataArchiver().processEmbeddedBeanScript(input);
        }
        else
        {
            return input;
        }
    }
    
    protected String processEmbeddedBeanScript(String input, Map<String,DataCell> map, char c)
        throws ETLException
    {
        if ( task != null )
        {
            return task.getETLJobWrapper().getDataArchiver().processEmbeddedBeanScript(input, map, c);
        }
        else
        {
            return input;
        }
    }

    protected void setHeaders(WebAPITaskComplexType.Http.Request.Headers h)
        throws ETLException
    {
        headers.clear();
        if ( h != null && h.getHeader() != null )
        {
            for ( WebAPITaskComplexType.Http.Request.Headers.Header header: h.getHeader() )
            {
                addHeader(new ComparableHeader(header.getName(), processEmbeddedBeanScript(header.getValue())));
            }
            logger.info("Setting HTTP Request headers: " + httpHeaders);
        }
    }

    protected void setParams(WebAPITaskComplexType.Http.Request.Params p)
        throws ETLException
    {
        params.clear();
        if ( p != null && p.getParamOrXmlParam() != null )
        {
            for ( Object arg: p.getParamOrXmlParam() )
            {
                params.add(new WebArgument(arg, task));
            }
            logger.info("Setting HTTP Request parameters: " + params);
        }
    }
    
    protected int getRetryCount()
    {
        return retryCount;
    }
    
    protected void setRetryCount(int count)
    {
        retryCount = count;
    }

    public void authenticate(Map<String,DataCell> rowdata) 
        throws ETLException
    {
        if ( webInfo != null )
        {
            logger.debug("Handling authentication for " + webInfo.getAuthType());
            switch (webInfo.getAuthType())
            {
                case basic:
                    String userid = webInfo.getUsername();
                    String password = webInfo.getPassword();

                    if ( userid == null || userid.trim().length() == 0 )
                    {
                        throw new ETLException("Basic authorization requires username to not be null");
                    }

                    if ( password == null || password.trim().length() == 0 )
                    {
                        throw new ETLException("Basic authorization requires password to not be null");
                    }

                    // Basic auth requires the credentials to be added as an authorization header to the normal http request
                    String credentials = userid.trim() + ":" + password.trim();
                    this.addHeader(new ComparableHeader(HEADER_AUTH_NAME, HEADER_AUTH_BASIC_VALUE + " " + Base64.encodeBase64String(credentials.getBytes())));
                    break;
                    
                case digest:
                    throw new UnsupportedOperationException("Digest authentication not currently supported");
                  
                case s2:
                    // If we have null token key in the web connection object then we need to request the authorization tokens
                    if ( webInfo.getTokenKey() == null || webInfo.getTokenKey().trim().length() == 0 )
                    {
                        logger.debug("S2 SESSIONID is null, executing login command");
                        
                        // First, we need to create an http request for login
                        RESTDriver s2Driver = (RESTDriver)getBaseDriver(DriverType.REST, task); 
                        s2Driver.setWebConnection(webInfo);
                        s2Driver.setURL(String.valueOf(webInfo.getBaseURI()) + "/goforms/nbapi");
                        s2Driver.setRequestMethod(RequestMethod.POST);
                        s2Driver.setContentType(ContentType.text_xml, null);

                        // Prep the s2 api xml structure for login
                        StringBuilder login = new StringBuilder();
                        login.append("<NETBOX-API><COMMAND name=\"Login\" num=\"1\"><PARAMS><USERNAME>");
                        login.append(webInfo.getUsername());
                        login.append("</USERNAME><PASSWORD>");
                        login.append(webInfo.getPassword());
                        login.append("</PASSWORD></PARAMS></COMMAND></NETBOX-API>");
                        NameValuePair nvp = new BasicNameValuePair("content", login.toString());
                        
                        // Add response column for s2 session id
                        s2Driver.addResponseColumn(new XpathColumn(DataTypes.STRING, "SESSIONID", "/NETBOX/@sessionid", null));
                        s2Driver.addParameter(nvp);
                        logger.trace("Login XML: " + login);
                        List<Map<String,DataCell>> responseData = s2Driver.execute(new HashMap(), false, false);

                        if ( responseData.isEmpty() )
                        {
                            throw new ETLException("Response from s2 login request is empty, cannot proceed");
                        }

                        logger.trace("S2 login response: " + responseData);
                        DataCell sess = responseData.get(0).get("SESSIONID");
                        if ( sess != null )
                        {
                            webInfo.setTokenKey(String.valueOf(sess.getValue()));
                            rowdata.put(sess.getColumn().getColumnName(), sess);
                        }
                    }
                    break;
                case oauth1:
                    // If we have null token key or token secret values in the web connection object then we need to request the authorization tokens
                    if ( webInfo.getTokenKey() == null || webInfo.getTokenSecret() == null )
                    {

                        // First, we need to create an http request for temp credentials
                        HTTPDriver authDriver = (HTTPDriver)getBaseDriver(DriverType.HTTP, task); 
                        authDriver.setWebConnection(webInfo);
                        authDriver.setURL(String.valueOf(webInfo.getTempTokenAuthorizationURI()));
                        authDriver.setRequestMethod(RequestMethod.POST);
                        authDriver.setContentType(ContentType.application_x_www_form_urlencoded, null);

                        // Add response column for oauth_token and oauth_token_secret
                        authDriver.addResponseColumn(new XpathColumn(DataTypes.STRING, OAuthParameters.OAUTH_TOKEN));
                        authDriver.addResponseColumn(new XpathColumn(DataTypes.STRING, OAuthParameters.OAUTH_TOKEN_SECRET));

                        authDriver.addHeader(OAuthManager.getOAuthTempAuthorizationHeader(OAuthManager.RequestMethod.POST, OAuthManager.Version.oauth1, webInfo, authDriver.getContentTypeString(), new ArrayList()));
                        authDriver.addHeader(new ComparableHeader(HEADER_CONTENT_TYPE_NAME, authDriver.getContentTypeString()));
                        logger.trace("Temp oauth1 token request headers: " + authDriver.getHeaders());
                        List<Map<String,DataCell>> responseData = authDriver.execute(new HashMap(), false, false);

                        if ( responseData.isEmpty() )
                        {
                            throw new ETLException("Response from oauth temp credential request is empty, cannot proceed");
                        }

                        logger.trace("Temp token response: " + responseData);
                        DataCell t = responseData.get(0).get(OAuthParameters.OAUTH_TOKEN);
                        DataCell ts = responseData.get(0).get(OAuthParameters.OAUTH_TOKEN_SECRET);

                        if ( t != null & ts != null )
                        {
                            webInfo.setTokenKey(String.valueOf(t.getValue()));
                            webInfo.setTokenSecret(String.valueOf(ts.getValue()));
                        }
                        else
                        {
                            throw new ETLException("Missing expected response parameters oauth_token and/or oauth_token_secret from oauth1 temp authorization response");
                        }

                        // Second, we need to use the temp credentials to get the real credentials
                        authDriver = (HTTPDriver)getBaseDriver(DriverType.HTTP, task); 
                        authDriver.setWebConnection(webInfo);
                        authDriver.setURL(String.valueOf(webInfo.getRealTokenAuthorizationURI()));
                        authDriver.setRequestMethod(RequestMethod.POST);
                        authDriver.setContentType(ContentType.application_x_www_form_urlencoded, null);

                        // Add response column for oauth_token and oauth_token_secret
                        authDriver.addResponseColumn(new XpathColumn(DataTypes.STRING, OAuthParameters.OAUTH_TOKEN));
                        authDriver.addResponseColumn(new XpathColumn(DataTypes.STRING, OAuthParameters.OAUTH_TOKEN_SECRET));

                        authDriver.addHeader(OAuthManager.getOAuthRealAuthorizationHeader(OAuthManager.RequestMethod.POST, OAuthManager.Version.oauth1, webInfo, authDriver.getContentTypeString(), new ArrayList()));
                        authDriver.addHeader(new ComparableHeader(HEADER_CONTENT_TYPE_NAME, authDriver.getContentTypeString()));
                        logger.trace("Real oauth1 token request headers: " + authDriver.getHeaders());
                        responseData = authDriver.execute(new HashMap(), false, false);

                        if ( responseData.isEmpty() )
                        {
                            throw new ETLException("Response from oauth real credential request is empty, cannot proceed");
                        }

                        logger.trace("Real token response: " + responseData);
                        t = responseData.get(0).get(OAuthParameters.OAUTH_TOKEN);
                        ts = responseData.get(0).get(OAuthParameters.OAUTH_TOKEN_SECRET);

                        if ( t != null && ts != null )
                        {
                            webInfo.setTokenKey(String.valueOf(t.getValue()));
                            webInfo.setTokenSecret(String.valueOf(ts.getValue()));
                        }
                        else
                        {
                            throw new ETLException("Missing expected response parameters oauth_token and/or oauth_token_secret from oauth1 real authorization response");
                        }
                    }

                    // This is added here as opposed to the above as assurance
                    try
                    {
                        URI realURI = null;
                        try
                        {
                            realURI = new URI(String.valueOf(getURL(rowdata)));
                        }
                        catch (Exception e)
                        {
                            realURI = new URI(getBaseURL());
                        }
                        
                        // Finally, the real credentials can be included in the base request as an authorization header
                        addHeader(OAuthManager.getOAuthAPIAuthorizationHeader(OAuthManager.RequestMethod.valueOf(String.valueOf(getRequestMethod())), OAuthManager.Version.oauth1, 
                                  realURI, webInfo, getContentTypeString(), getComparableParameters()));
                    }
                    catch (URISyntaxException e)
                    {
                        // Shouldn't happen
                        throw new ETLException("Unable to convert base URL to URI", e);
                    }
                    break;
            }
        }
    }

    @Override
    public List<Map<String, DataCell>> execute(Map<String, DataCell> data, boolean handleAuth) 
        throws ETLException 
    {
        return execute(data, handleAuth, true);
    }

    @Override
    public List<Map<String, DataCell>> execute(Map<String, DataCell> data, boolean handleAuth, boolean addResponse) 
        throws ETLException 
    {
        return execute(data, handleAuth, true, addResponse);
    }

    protected List<Map<String, DataCell>> execute(Map<String, DataCell> data, boolean handleAuth, boolean resetRetries, boolean addResponse) 
        throws ETLException 
    {
        List<Map<String,DataCell>> responseData = new ArrayList();
        
        if ( requestMethod == null )
        {
            throw new ETLException("Request method cannot be null");
        }
        
        // Reset the retry counter if declared to otherwise increment
        if ( resetRetries )
        {
            retryCount = 0;
        }
        else
        {
            retryCount++;
        }
        
        // If handle auth is true prepare authentication
        if ( handleAuth )
        {
            authenticate(data);
        }
        
        /*try
        {
            logger.info("Executing " + getRequestMethod() + " on " + getBaseURL());
        }
        catch (Exception e)
        {
            logger.info("Executing " + getRequestMethod() + " on " + getBaseURL());
        }*/
        
        switch (requestMethod)
        {
            case GET:
                responseData.addAll(get(data, addResponse));
                break;
            case POST:
                responseData.addAll(post(data, addResponse));
                break;
            case DELETE:
                responseData.addAll(delete(data, addResponse));
                break;
            case OPTIONS:
                responseData.addAll(options(data, addResponse));
                break;
            case PATCH:
                responseData.addAll(patch(data, addResponse));
                break;
            case PUT:
                responseData.addAll(put(data, addResponse));
                break;
            default:
                throw new UnsupportedOperationException("Invalid request method for rest request: " + requestMethod);
        }
        
        // Run any execute after completion tasks 
        executeAfterCompletion();
        
        logger.debug("Response data: " + responseData);
        return responseData;
    }

    protected abstract List<Map<String,DataCell>> get(Map<String,DataCell> rowdata, boolean addResponse) throws ETLException;
            
    protected abstract List<Map<String,DataCell>> post(Map<String,DataCell> rowdata, boolean addResponse) throws ETLException;

    protected abstract List<Map<String,DataCell>> delete(Map<String,DataCell> rowdata, boolean addResponse) throws ETLException;

    protected abstract List<Map<String,DataCell>> options(Map<String,DataCell> rowdata, boolean addResponse) throws ETLException;

    protected abstract List<Map<String,DataCell>> patch(Map<String,DataCell> rowdata, boolean addResponse) throws ETLException;

    protected abstract List<Map<String,DataCell>> put(Map<String,DataCell> rowdata, boolean addResponse) throws ETLException;

    public void setResponse(WebAPITaskComplexType.Http.Response response)
        throws ETLException
    {
        if ( response != null )
        {
            setContentType(response.getContentType(), response.getJsonRootElement());
            invalidPathHandler = response.getInvalidPathAs();
            declaredColumns.clear();
                    
            for ( XpathComplexType ct: response.getDataElement() )
            {
                XpathColumn col = new XpathColumn(String.valueOf(ct.getType()), ct.getValue(), ct.isPrimaryKey(), ct.getDefaultValue());
                col.setReferenceString(ct.getRef());
                col.setXpath(ct.getPath());
                addResponseColumn(col);
            }
            logger.info("ExpectedContentType: " + getContentTypeString());
            logger.info("Setting HTTP Response elements: " + declaredColumns);
        }
        else
        {
            throw new ETLException("Response element in web-api declaration is required");
        }
    }
    
    protected Object replaceHierarchicalVariable(WebArgument arg, Map<String,DataCell> rowdata)
        throws ETLException
    {
        Object val = null;
        
        if ( arg != null )
        {
            val = processEmbeddedBeanScript(String.valueOf(arg.getValue()), rowdata, '@');
        }
        
        return val;
    }
    
    protected List<Map<String,DataCell>> checkForExpiredToken(HttpResponse response, Map<String,DataCell> rowdata)
        throws IOException, ETLException
    {
        List<Map<String,DataCell>> newResponse = new ArrayList();
        
        
        return newResponse;
    }
    
    protected void executeAfterCompletion()
    {
        switch ( afterExecute )
        {
            case invalidateToken:
                switch ( webInfo.getAuthType() )
                {
                    case oauth1:
                        break;
                    case s2:
                        // Remove token
                        logger.debug("AfterExecution: Invalidating token for S2");
                        webInfo.setTokenKey(null);
                        break;
                }
                break;
            default:
        }
    }
    
    protected void parseStatusLine(HttpResponse response)
        throws ETLException
    {
        StatusLine line = null;
        try
        {
            if ( response != null )
            {
                line = response.getStatusLine();
                switch(line.getStatusCode())
                {
                    case 200:
                        break;
                    case 401:
                        // Only throw status exception if the response does not contain "expired" and web connection auth type is not oauth1
                        // If not, this will be handled in parseResponse to retry the request with a new oauth token
                        String content = EntityUtils.toString(response.getEntity());
                        if ( retryCount == OAUTH_MAX_RETRY_COUNT || webInfo == null || webInfo.getAuthType() != WebConnection.AuthType.oauth1 || content == null || !content.toLowerCase().contains("expired") )
                        {
                            throw new ETLException(line + ": Requested URL is protected by an authorization scheme: " + EntityUtils.toString(response.getEntity()));
                        }
                        break;
                    default:
                        throw new ETLException(String.valueOf(line) + ": Content: " + EntityUtils.toString(response.getEntity()));

                }
            }
            else
            {
                throw new ETLException("Null reponse returned from server");
            }
        }
        catch (IOException e)
        {
            throw new ETLException(String.valueOf(line));
        }
    }

    protected List<Map<String,DataCell>> parseResponse(HttpResponse httpResponse, Map<String,DataCell> rowdata, boolean addResponse)
        throws ETLException
    {
        List<Map<String,DataCell>> response = new ArrayList();
        HttpEntity entity = null;
        String charset = OAuthManager.ENCODING_UTF8;
        
        try
        {
            entity = httpResponse.getEntity();
            String content = EntityUtils.toString(entity);

            // Check to see if an expired oauth token was encountered and no token retries have been attempted
            if ( retryCount == 0 && httpResponse.getStatusLine().getStatusCode() == 401 && webInfo != null && 
                 webInfo.getAuthType() == WebConnection.AuthType.oauth1 && content != null && content.toLowerCase().contains("expired")  )
            {
                // Setting token values in the web connection to null will force new oauth tokens to be requested
                webInfo.setTokenKey(null);
                webInfo.setTokenSecret(null);

                // Re-execute request ensuring authentication handler is run
                response.addAll(execute(rowdata, true, false));
            }
            else
            {
                String ct = entity.getContentType().getValue();
                Header h = httpResponse.getFirstHeader(HEADER_CONTENT_TYPE_NAME);
                if ( ct == null )
                {
                    if ( h != null )
                    {
                        ct = h.getValue();
                    }
                }

                // If the content type is still null then we cannot do anything and the resource owner probably needs to be contacted
                if ( ct == null )
                {
                    throw new ETLException("Unable to extract content type from response", entity);
                }

                // Extract charset for any decoding necessary downstream
                h = new BasicHeader(HEADER_CONTENT_TYPE_NAME, ct.replaceAll(";", ","));
                for ( HeaderElement he: h.getElements() )
                {
                    // Once the charset header name is found extract the charset value 
                    if ( HEADER_CT_CHARSET_NAME.equals(he.getName()) )
                    {
                        charset = he.getValue();
                        break;
                    }
                }

                logger.trace("Content-Type: " + ct);
                logger.trace("Charset: " + charset);
                logger.trace("Response entity: " + entity);
                logger.trace("Content: " + content);
                logger.trace("ContentType: " + getContentTypeString());

                // Ensure expected content type matches response content type
                if ( !ct.toLowerCase().contains(getContentTypeString()) )
                {
                    throw new ETLException("Expected content type " + getContentTypeString() + " doesn't match declared content type from http response: " + ct, entity);
                }

                // Parse the response content based on the declared content type
                List<Map<String,DataCell>> resp = parseContent(contentType, content, charset, addResponse);
                for ( Map<String,DataCell> parsed: resp )
                {
                    Map<String,DataCell> rowcopy = new HashMap(rowdata);
                    rowcopy.putAll(parsed);
                    response.add(rowcopy);
                }
            }
        }
        catch (SAXException e)
        {
            throw new ETLException("Error attempting to parse xml response", e, entity);
        }
        catch (ParserConfigurationException e)
        {
            throw new ETLException("Error attempting to parse json response", e, entity);
        }
        catch (UnsupportedEncodingException e)
        {
            throw new ETLException("Error decoding URL encoded content since charset is unsupported: " + charset, entity);
        }
        catch (IOException e)
        {
            throw new ETLException("Error attempting to convert http response into a readable String", e, entity);
        }
        
        return response;
    }
    
    protected List<Map<String,DataCell>> parseContent(ContentType type, String content, String charset, boolean addResponse)
        throws ETLException, ParserConfigurationException, IOException, SAXException, UnsupportedEncodingException
    {
        List<Map<String,DataCell>> response = new ArrayList();
        
        switch (type)
        {
            case application_json:
            case text_json:
                //response.addAll(parseDOMContent(XMLUtils.parseJSON(content, jsonRootElement), addResponse));
                response.addAll(parseJSONContent(content, jsonRootElement, addResponse));
                break;
            case application_xml:
            case text_xml:
                response.addAll(parseDOMContent(XMLUtils.parseXML(content), addResponse));
                break;
            case application_x_www_form_urlencoded:
                response.addAll(parseURLContent(content, charset));
                break;
            default:
                throw new ETLException("Unsupported contentType in response", contentType);
        }
        
        return response;
    }
    
    protected List<Map<String,DataCell>> parseURLContent(String content, String charset)
        throws ETLException, UnsupportedEncodingException
    {
        List<Map<String,DataCell>> response = new ArrayList();
        Map<String,DataCell> map = new HashMap();
        Map<String,String> paramMap = new HashMap();
     
        // URL decode the content
        content = URLDecoder.decode(content, charset);
        logger.debug("Unparsed response: " + content);
        DataColumn responseCol = new DataColumn(DataTypes.STRING, WebAPIDispatcher.COL_HTTP_RESPONSE);
        map.put(WebAPIDispatcher.COL_HTTP_RESPONSE, new DataCell(responseCol, content));
        response.add(map);
        
        // Once content is decoded run this through the Apache Header parser to extract name/value pairs
        HeaderElement[] params = BasicHeaderValueParser.parseElements(content.replaceAll("&", ","), null);
        for ( HeaderElement he: params )
        {
            paramMap.put(he.getName(), he.getValue());
        }
        
        logger.trace("ParamMap: " + paramMap);
        
        // Iterate over each declared response column and assign any matched parameters to the rowdata
        for ( Map.Entry<String,XpathColumn> entry: declaredColumns.entrySet() )
        {
            String colname = entry.getKey();
            XpathColumn col = entry.getValue();
            map.put(col.getColumnName(), new DataCell(col, paramMap.get(colname)));
        }
        
        return response;
    }
    
    protected List<Map<String,DataCell>> parseJSONContent(String jsonResponse, String rootElement, boolean addResponse)
        throws ETLException, IOException
    {
        List<Map<String,DataCell>> response = new ArrayList();
        Map<String,DataCell> map = new HashMap();
        Map<String,List<XpathColumn>> nodeMap = new HashMap();
        
        // The converted json response data is added to the initial row of data for reference
        logger.debug("Unparsed response: " + jsonResponse);
        //BbJSONContainer json = new BbJSONContainer(jsonResponse, rootElement);
        JacksonJsonContainer json = new JacksonJsonContainer(jsonResponse, rootElement);
        
        // Todo: add support for inline field getters for JSON responses
        //JsonNode node = json.getFieldValue(JacksonJsonContainer.PATH_SEPARATOR + rootElement);
        
        JsonNode rnode = json.getFieldValue(JacksonJsonContainer.PATH_SEPARATOR + rootElement);
        logger.info("Root element instanceof " + rnode.getClass().getName());
        logger.trace("Response node: " + rnode);
        
        if ( rnode.getNodeType() == JsonNodeType.ARRAY )
        {
            Iterator<JsonNode> it = rnode.elements();
            while ( it.hasNext() )
            {
                JsonNode node = it.next();
                logger.trace("List elements instanceof " + node.getClass().getName());
                if ( node.getNodeType() ==  JsonNodeType.OBJECT )
                {
                    logger.trace("List element: " + node);
                    if ( addResponse )
                    {
                        map = new HashMap();
                        response.add(map);
                        DataColumn responseCol = new DataColumn(DataTypes.JSON, WebAPIDispatcher.COL_HTTP_RESPONSE);
                        map.put(WebAPIDispatcher.COL_HTTP_RESPONSE, new DataCell(responseCol, node));
                        logger.trace("Adding HTTP RESPONSE: " + node);
                    }
                }
                else
                {
                    throw new UnsupportedOperationException("Embedded JSON objects of " + node.getNodeType() + " not currently supported");
                }
            }
        }
        else if ( rnode.getNodeType() == JsonNodeType.OBJECT )
        {
            response.add(map);
            if ( addResponse )
            {
                DataColumn responseCol = new DataColumn(DataTypes.JSON, WebAPIDispatcher.COL_HTTP_RESPONSE);
                map.put(WebAPIDispatcher.COL_HTTP_RESPONSE, new DataCell(responseCol, json));
                logger.trace("Adding HTTP RESPONSE: " + json);
            }
        }
        else
        {
            throw new UnsupportedOperationException("JSON response objects of " + rnode.getNodeType() + " not currently supported");
        }
        
        logger.trace("Reponse object before parsing reponse columns: " + response);
        
        // Process field extraction here - execute getField HierarchyTransform on http_response column
        ObjectFactory of = new ObjectFactory();
        HierarchyTransformComplexType tcf = of.createHierarchyTransformComplexType();
        tcf.setType("json");
        tcf.setAction("getField");
        tcf.setInvalidPathAs(invalidPathHandler);
        
        // Add source json from http response
        TransformComplexType.SourceColumn sc = of.createTransformComplexTypeSourceColumn();
        sc.setValue(WebAPIDispatcher.COL_HTTP_RESPONSE);
        tcf.getSourceColumn().add(sc);    

        // Iterate over each declared response column and assign any pathed target columns to transform
        TransformComplexType.TargetColumn tc = null;
        for ( Map.Entry<String,XpathColumn> entry: declaredColumns.entrySet() )
        {
            String colname = entry.getKey();
            XpathColumn col = entry.getValue();
            if ( col.getDataType() == DataTypes.NODE )
            {
                
            }
            else
            {
                tc = of.createTransformComplexTypeTargetColumn();
                tc.setPath(col.getXpath());
                tc.setRef(col.getReferenceString());
                tc.setDefaultValue(col.getDefaultValue());
                tc.setOverrideDataType(col.getDataType());
                tc.setValue(colname);
                tcf.getTargetColumn().add(tc);
            }
        }
        
        // Prep the target columns for the transform
        List<DataColumn> targetCols = new ArrayList();
        List<DataColumn> targetColCopy = new ArrayList();
        for ( TransformComplexType.TargetColumn target: tcf.getTargetColumn() )
        {
            DataColumn col = declaredColumns.get(target.getValue());
            targetCols.add(col);
            targetColCopy.add(new DataColumn(col.getDataType(), col.getColumnName(), col.getDefaultValue()));
        }
        
        logger.debug("TargetCols: " + targetCols);
        HierarchyTransform ht = (HierarchyTransform)TransformFactory.createTransform(tcf, task);
        for ( int i=1; i<=response.size(); i++ )
        {
            System.out.println("Processing hierarchyTransform on json response data on row #" + i);
            logger.debug("Processing hierarchyTransform on json response data on row #" + i + ": " + ht);
            
            Map<String,DataCell> rowdata = response.get(i-1);
            List<DataCell> tcells = ht.transform(targetColCopy, new ArrayList<DataCell>(rowdata.values()));
            for ( int j=0; j<tcells.size(); j++ )
            {
                DataCell tcell = tcells.get(j);
                DataColumn tcol = targetCols.get(j);
                tcell.setColumn(tcol);  // Ensure declared column data type is used regardless of what transform did
                logger.debug("<------------- OUTPUT: " + tcol + "=" + tcell + " -------------->");
                rowdata.put(tcol.getColumnName(), tcell);
            }
        }

        logger.trace("Parsed response: " + response);
        
        return response;     
    }
    
    protected List<Map<String,DataCell>> parseDOMContent(Document doc, boolean addResponse)
        throws ETLException, ParserConfigurationException
    {
        List<Map<String,DataCell>> response = new ArrayList();
        Map<String,DataCell> map = new HashMap();
        Map<String,List<XpathColumn>> nodeMap = new HashMap();

        // The converted xml response data is added to the initial row of data for reference
        logger.debug("Unparsed response: " + XMLUtils.toString(doc, false));
        response.add(map);
        if ( addResponse )
        {
            DataColumn responseCol = new DataColumn(DataTypes.XML, WebAPIDispatcher.COL_HTTP_RESPONSE);
            map.put(WebAPIDispatcher.COL_HTTP_RESPONSE, new DataCell(responseCol, XMLUtils.toString(doc, false)));
        }

        // To handle cartesian product results need to do the following:
        // 1. Sort response columns such that all non-node and non-referenced columns are processed first
        // 2. For each node column, need to process that node's referenced columns (separate method)
        // 3. Each row in the current result set must be combined with the results from the node column processing
        
        for ( Map.Entry<String,XpathColumn> entry: declaredColumns.entrySet() )
        {
            XpathColumn col = entry.getValue();
            
            // If the response column is not a NODE or uses a reference string the extracted data can be added to the initial response row
            if ( col.getDataType() != DataTypes.NODE && ( col.getReferenceString() == null || col.getReferenceString().trim().length() == 0 ) )
            {
                map.put(col.getColumnName(), extractXpathValue(col, doc));
            }
            else
            {
                // If the response column is a NODE then column needs to be inserted into the first element of it's node set
                List<XpathColumn> columns = null;
                if ( col.getDataType() == DataTypes.NODE )
                {
                    columns = nodeMap.get(col.getColumnName());
                    if ( columns == null )
                    {
                        columns = new ArrayList();
                        nodeMap.put(col.getColumnName(), columns);
                    }
                    columns.add(0, col);
                }
                // Referenced columns can be added after the first element of the node set
                else
                {
                    columns = nodeMap.get(col.getReferenceString());
                    if ( columns == null )
                    {
                        columns = new ArrayList();
                        nodeMap.put(col.getReferenceString(), columns);
                    }
                    columns.add(col);
                }
            }
        }
        
        logger.trace("NodeMap=" + nodeMap);
        
        // Iterate over each set of node and node reference columns
        for ( List<XpathColumn> list: nodeMap.values() )
        {
            logger.trace("Node set=" + list);
            NodeList nlist = null;
            List<Map<String,DataCell>> workList = null;
            
            // Iterate over each response column to add their data to the response
            for ( XpathColumn col: list )
            {
                
                logger.trace("xcol=" + col);
                if ( col.getDataType() == DataTypes.NODE )
                {
                    // The first element should always be the NODE declaration unless it's missing in the response columns or not found
                    nlist = extractXpathNodeValue(col, doc);
                    if ( nlist.getLength() > 1 )
                    {
                        workList = new ArrayList(nlist.getLength());
                    }
                    // If the xpath expression returns an empty node list then we need to populate the working list with one element in order
                    // ensure that the existing response rows are retained
                    else
                    {
                        workList = new ArrayList();
                        workList.add(new HashMap());
                    }
                }
                else
                {
                    // Iterate over each node returned from the xpath expression
                    for ( int i=0; i<nlist.getLength(); i++ )
                    {
                        Node n = nlist.item(i);
                        Document doc2 = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
                        Node root = doc2.importNode(n, true);
                        doc2.appendChild(root);
                        logger.trace("Referenced node=" + XMLUtils.toString(doc2, false));

                        DataCell val = extractXpathValue(col, doc2);
                        logger.trace(col + " value: " + val + (val!=null?"(" + val.getClass().getName() + ")":""));

                        // For each node returned, a new response row will be needed
                        // For multiple response columns with the same node ref, the data map needs to be reused for each row
                        Map<String,DataCell> workData = null;
                        try
                        {
                            workData = workList.get(i);
                            workData.put(col.getColumnName(), val);
                        }
                        catch (IndexOutOfBoundsException e)
                        {
                            workData = new HashMap();
                            workData.put(col.getColumnName(), val);
                            workList.add(workData);
                        }
                    }
                }
            }
            
            // For each existing response row assign each nodelist row data to the values of the response row (Cartesian Product of results)
            List<Map<String,DataCell>> newResponse = new ArrayList();
            logger.trace("Data for nodelist: " + list);
            for ( Map<String,DataCell> rowdata: response )
            {
                for ( Map<String,DataCell> data: workList )
                {
                    logger.trace("Adding " + data + " to: " + rowdata);
                    Map<String,DataCell> newdata = new HashMap(data);
                    newdata.putAll(new HashMap(rowdata));
                    newResponse.add(newdata);
                }
            }
            
            response = newResponse;
        }

        return response;
    }
    
    private DataCell extractXpathValue(XpathColumn col, Document doc)
        throws ETLException
    {
        XPath expr = XPathFactory.newInstance().newXPath();
        logger.trace("Evaluating xpath: " + col.getXpath());
        DataCell cell = null;

        try
        {
            Node n = (Node)expr.evaluate(col.getXpath(), doc.getDocumentElement(), XPathConstants.NODE);
            if ( n != null )
            {
                NodeType ntype = nodeTypeMap.get((int)n.getNodeType());
                switch(ntype)
                {
                    case ELEMENT_NODE:
                        throw new ETLException("Xpath NODE type must be declared as a NODE DataType: " + col.getXpath(), col);
                    case ATTRIBUTE_NODE:
                    case TEXT_NODE:
                        cell = new DataCell(col, n.getTextContent());
                        break;
                    default:
                        throw new ETLException("Unsupported xpath node type returned: " + ntype, n);
                }
            }
            else
            {
                cell = new DataCell(col, null);
            }
        }
        catch (XPathExpressionException e)
        {
            throw new ETLException("Invalid xpath expression: " + col.getXpath(), col);
        }
        
        return cell;
    }

    private NodeList extractXpathNodeValue(XpathColumn col, Document doc)
        throws ETLException
    {
        NodeList nl = null;
        XPath expr = XPathFactory.newInstance().newXPath();
        logger.trace("Evaluating xpath=" + col.getXpath());

        if ( col.getDataType() == DataTypes.NODE )
        {
            try
            {
                nl = (NodeList)expr.evaluate(col.getXpath(), doc.getDocumentElement(), XPathConstants.NODESET);
                if ( nl != null )
                {
                    Node n = null;
                    if ( nl.getLength() > 0 && nl.item(0) != null )
                    {
                        n = nl.item(0);
                        logger.trace(col.getXpath() + "=" + n);
                        NodeType ntype = nodeTypeMap.get((int)n.getNodeType());
                        if ( ntype != NodeType.ELEMENT_NODE )
                        {
                            throw new ETLException("Unsupported xpath node type returned, expecting NODE: " + ntype, n);
                        }
                    }
                }
            }
            catch (XPathExpressionException e)
            {
                throw new ETLException("Invalid xpath expression: " + col.getXpath(), col);
            }
        }
        
        return nl;
    }
}

