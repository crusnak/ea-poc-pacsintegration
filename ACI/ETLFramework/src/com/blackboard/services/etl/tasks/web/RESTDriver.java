/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.tasks.web;

import com.blackboard.services.etl.data.Argument;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.data.WebArgument;
import com.blackboard.services.etl.data.XpathColumn;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.WebAPITaskComplexType;
import com.blackboard.services.etl.tasks.WebAPIDispatcher;
import com.blackboard.services.security.OAuthManager;
import com.blackboard.services.security.object.WebConnection;
import com.blackboard.services.security.object.oauth.OAuthParameters.SignatureMethod;
import com.blackboard.services.utils.ComparableHeader;
import com.blackboard.services.utils.XMLUtils;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicHeaderValueParser;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author crusnak
 */
public class RESTDriver extends AbstractDriver
{
    private CloseableHttpClient connection = null;

    protected RESTDriver(WebAPIDispatcher task)
    {
        super(task);
    }

    protected RESTDriver(WebAPITaskComplexType ct, WebAPIDispatcher task)
        throws ETLException
    {
        super(ct, task);

        try
        {
            type = DriverType.REST;
            setJoinType(ct.getHttp().getJoinType());
            setAfterExecution(ct.getHttp().getAfterExecute());
            setConnectionInfo(ct.getConnectionInfo());
            setRequest(ct.getHttp().getRequest());
            setResponse(ct.getHttp().getResponse());
        }
        catch (MalformedURLException e)
        {
            throw new ETLException("Bad URL", e);
        }
    }
    
    @Override
    public void setRequest(WebAPITaskComplexType.Http.Request request)
        throws ETLException, MalformedURLException
    {
        if ( request != null )
        {
            setAPIURI(request.getApiURI());
            setRequestMethod(request.getMethod());
            setHeaders(request.getHeaders());
            setParams(request.getParams());
        }
        else
        {
            throw new ETLException("Request element in web-api declaration is required");
        }
    }

    @Override
    public void setResponse(WebAPITaskComplexType.Http.Response response)
        throws ETLException
    {
        super.setResponse(response);
    }

    protected List<Map<String,DataCell>> get(Map<String,DataCell> rowdata, boolean addResponse)
        throws ETLException
    {
        CloseableHttpResponse response = null;
        List<Map<String,DataCell>> newdata = new ArrayList();
        
        try
        {
            logger.debug("Utilizing rowdata=" + rowdata);
            
            // Check if strict SSL checking used
            if ( webInfo.isStrictSSLCheckUsed() )
            {
                connection = HttpClients.createDefault();
            }
            else
            {
                connection = HttpClients.custom().setSSLContext(new SSLContextBuilder().loadTrustMaterial(null, TrustSelfSignedStrategy.INSTANCE).build())
                                                 .setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE).build();
            }
            
            // Iterate through all http request headers
            for ( int j=1; j<=headers.size(); j++ )
            {
                Argument arg = headers.get(j-1);
                String argname = "";
                Object val = null;
                if ( arg.getArgumentType() == Argument.Type.column )
                {
                    argname = String.valueOf(arg.getValue());
                    DataCell cell = rowdata.get(argname);
                    if ( cell != null )
                    {
                        val = cell.getValue();
                    }
                    else if ( task.getData().getColumnDefinition(argname) != null )
                    {
                        val = null;
                    }
                    else
                    {
                        throw new ETLException("Undefined column " + arg + ", unable to set http request header with correct data.", arg);
                    }
                }
                else
                {
                    argname = arg.getColumn().getColumnName();
                    val = arg.getValue();
                }

                logger.debug("Header #" + j + " '" + argname + "'=" + val);
                httpHeaders.add(new ComparableHeader(argname, String.valueOf(val)));
            }

            logger.info("HTTP headers: " + httpHeaders);

            convertParamsToHttpParameters(rowdata);
            
            logger.info("HTTP parameters: " + httpParameters);
            
            // Execute http request
            HttpGet get = new HttpGet(getURL(rowdata).toURI()); 
            get.setHeaders(httpHeaders.toArray(new Header[0]));
            
            logger.info("Executing " + get);
            response = connection.execute(get);
            logger.debug("HTTP response: " + response.getStatusLine());
            parseStatusLine(response);
            
            newdata.addAll(parseResponse(response, rowdata, addResponse));

            EntityUtils.consume(response.getEntity());
        }
        catch (URISyntaxException e)
        {
            throw new ETLException("Unable to parse URL to URI statement: " + getURL(rowdata), e);
        }
        catch (UnsupportedEncodingException e)
        {
            throw new ETLException("Unable to encode http entity since encoding not supported", e);
        }
        catch (IOException e)
        {
            throw new ETLException("Error communicating with http target: " + getURL(rowdata), e);
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new ETLException("Unable to disable strict hostname checking: " + e);
        }
        catch (KeyManagementException e)
        {
            throw new ETLException("Unable to access keystore: " + e);
        }
        catch (KeyStoreException e)
        {
            throw new ETLException("Unable to access keystore: " + e);
        }
        finally
        {
            try
            {
                if ( response != null )
                {
                    response.close();
                }
            }
            catch (IOException e)
            {
                logger.warn("Unable to close http response", e);
            }
        }
        
        return newdata;
    }
    
    protected List<Map<String,DataCell>> post(Map<String,DataCell> rowdata, boolean addResponse)
        throws ETLException
    {
        CloseableHttpResponse response = null;
        List<Map<String,DataCell>> newdata = new ArrayList();

        try
        {
            logger.debug("Utilizing rowdata=" + rowdata);
            
            // Check if strict SSL checking used
            if ( webInfo.isStrictSSLCheckUsed() )
            {
                connection = HttpClients.createDefault();
            }
            else
            {
                connection = HttpClients.custom().setSSLContext(new SSLContextBuilder().loadTrustMaterial(null, TrustSelfSignedStrategy.INSTANCE).build())
                                                 .setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE).build();
            }
            
            // Iterate through all http request headers
            for ( int j=1; j<=headers.size(); j++ )
            {
                Argument arg = headers.get(j-1);
                String argname = "";
                Object val = null;
                if ( arg.getArgumentType() == Argument.Type.column )
                {
                    argname = String.valueOf(arg.getValue());
                    DataCell cell = rowdata.get(argname);
                    if ( cell != null )
                    {
                        val = cell.getValue();
                    }
                    else if ( task.getData().getColumnDefinition(argname) != null )
                    {
                        val = null;
                    }
                    else
                    {
                        throw new ETLException("Undefined column " + arg + ", unable to set http request header with correct data.", arg);
                    }
                }
                else
                {
                    argname = arg.getColumn().getColumnName();
                    val = arg.getValue();
                }

                logger.debug("Header #" + j + " '" + argname + "'=" + val);
                httpHeaders.add(new ComparableHeader(argname, String.valueOf(val)));
            }

            logger.info("HTTP headers: " + httpHeaders);
            convertParamsToHttpParameters(rowdata);
            
            // Execute http request
            HttpPost post = new HttpPost(getURL(rowdata).toURI());
            post.setHeaders(httpHeaders.toArray(new Header[0]));
            if ( httpParameters.size() == 0 )
            {
                throw new ETLException("POST calls must include one parameter for the data entity object.");
            }
            else if ( httpParameters.size() > 1 )
            {
                logger.warn("More than one parameter set for POST call. All parameters after first will be ignored.");
            }
            else
            {
                HttpEntity entity = null;
                NameValuePair nvp = httpParameters.get(0);
                switch ( contentType )
                {
                    case application_json:
                    case text_json:
                        StringEntity se = new StringEntity(nvp.getValue());
                        //se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, String.valueOf(contentType)));
                        entity = se;
                        logger.debug("String Entity: " + nvp.getValue());
                        break;
                    case application_xml:
                    case text_xml:
                        se = new StringEntity(nvp.getValue());
                        entity = se;
                        logger.debug("String Entity: " + nvp.getValue());
                        break;
                    default:
                        throw new UnsupportedOperationException("Unsupported contentType for POST call " + contentType);
                }
                
                logger.info("HTTP Entity: " + entity);
                post.setEntity(entity);
            }
            
            logger.trace("POST headers=" + Arrays.asList((post.getAllHeaders())));
            logger.trace("POST body=" + post.getEntity());
            
            logger.info("Executing " + post);
            response = connection.execute(post);
            logger.debug("HTTP response: " + response.getStatusLine());
            parseStatusLine(response);
            
            newdata.addAll(parseResponse(response, rowdata, addResponse));

            EntityUtils.consume(response.getEntity());
        }
        catch (URISyntaxException e)
        {
            throw new ETLException("Unable to parse URL to URI statement: " + getURL(rowdata), e);
        }
        catch (UnsupportedEncodingException e)
        {
            throw new ETLException("Unable to encode http entity since encoding not supported", e);
        }
        catch (IOException e)
        {
            throw new ETLException("Error communicating with http target: " + getURL(rowdata), e);
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new ETLException("Unable to disable strict hostname checking: " + e);
        }
        catch (KeyManagementException e)
        {
            throw new ETLException("Unable to access keystore: " + e);
        }
        catch (KeyStoreException e)
        {
            throw new ETLException("Unable to access keystore: " + e);
        }
        finally
        {
            try
            {
                if ( response != null )
                {
                    response.close();
                }
            }
            catch (IOException e)
            {
                logger.warn("Unable to close http response", e);
            }
        }
        
        return newdata;
    }

    protected List<Map<String,DataCell>> delete(Map<String,DataCell> rowdata, boolean addResponse)
        throws ETLException
    {
        CloseableHttpResponse response = null;
        List<Map<String,DataCell>> newdata = new ArrayList();
        
        try
        {
            logger.debug("Utilizing rowdata=" + rowdata);
            
            // Check if strict SSL checking used
            if ( webInfo.isStrictSSLCheckUsed() )
            {
                connection = HttpClients.createDefault();
            }
            else
            {
                connection = HttpClients.custom().setSSLContext(new SSLContextBuilder().loadTrustMaterial(null, TrustSelfSignedStrategy.INSTANCE).build())
                                                 .setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE).build();
            }
            
            // Iterate through all http request headers
            for ( int j=1; j<=headers.size(); j++ )
            {
                Argument arg = headers.get(j-1);
                String argname = "";
                Object val = null;
                if ( arg.getArgumentType() == Argument.Type.column )
                {
                    argname = String.valueOf(arg.getValue());
                    DataCell cell = rowdata.get(argname);
                    if ( cell != null )
                    {
                        val = cell.getValue();
                    }
                    else if ( task.getData().getColumnDefinition(argname) != null )
                    {
                        val = null;
                    }
                    else
                    {
                        throw new ETLException("Undefined column " + arg + ", unable to set http request header with correct data.", arg);
                    }
                }
                else
                {
                    argname = arg.getColumn().getColumnName();
                    val = arg.getValue();
                }

                logger.debug("Header #" + j + " '" + argname + "'=" + val);
                httpHeaders.add(new ComparableHeader(argname, String.valueOf(val)));
            }

            logger.info("HTTP headers: " + httpHeaders);
            convertParamsToHttpParameters(rowdata);
            
            // Execute http request
            HttpDelete del = new HttpDelete(getURL(rowdata).toURI());
            del.setHeaders(httpHeaders.toArray(new Header[0]));
            
            logger.trace("DEL headers=" + Arrays.asList((del.getAllHeaders())));
            
            logger.info("Executing " + del);
            response = connection.execute(del);
            logger.debug("HTTP response: " + response.getStatusLine());
            parseStatusLine(response);
            
            newdata.addAll(parseResponse(response, rowdata, addResponse));

            EntityUtils.consume(response.getEntity());
        }
        catch (URISyntaxException e)
        {
            throw new ETLException("Unable to parse URL to URI statement: " + getURL(rowdata), e);
        }
        catch (UnsupportedEncodingException e)
        {
            throw new ETLException("Unable to encode http entity since encoding not supported", e);
        }
        catch (IOException e)
        {
            throw new ETLException("Error communicating with http target: " + getURL(rowdata), e);
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new ETLException("Unable to disable strict hostname checking: " + e);
        }
        catch (KeyManagementException e)
        {
            throw new ETLException("Unable to access keystore: " + e);
        }
        catch (KeyStoreException e)
        {
            throw new ETLException("Unable to access keystore: " + e);
        }
        finally
        {
            try
            {
                if ( response != null )
                {
                    response.close();
                }
            }
            catch (IOException e)
            {
                logger.warn("Unable to close http response", e);
            }
        }
        
        return newdata;
    }
    
    protected List<Map<String,DataCell>> options(Map<String,DataCell> rowdata, boolean addResponse)
        throws ETLException
    {
        throw new UnsupportedOperationException("REST method OPTIONS not currently supported");
    }

    protected List<Map<String,DataCell>> patch(Map<String,DataCell> rowdata, boolean addResponse)
        throws ETLException
    {
        throw new UnsupportedOperationException("REST method PATCH not currently supported");
    }

    protected List<Map<String,DataCell>> put(Map<String,DataCell> rowdata, boolean addResponse)
        throws ETLException
    {
        CloseableHttpResponse response = null;
        List<Map<String,DataCell>> newdata = new ArrayList();
        
        try
        {
            logger.debug("Utilizing rowdata=" + rowdata);
            
            // Check if strict SSL checking used
            if ( webInfo.isStrictSSLCheckUsed() )
            {
                connection = HttpClients.createDefault();
            }
            else
            {
                connection = HttpClients.custom().setSSLContext(new SSLContextBuilder().loadTrustMaterial(null, TrustSelfSignedStrategy.INSTANCE).build())
                                                 .setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE).build();
            }
            
            // Iterate through all http request headers
            for ( int j=1; j<=headers.size(); j++ )
            {
                Argument arg = headers.get(j-1);
                String argname = "";
                Object val = null;
                if ( arg.getArgumentType() == Argument.Type.column )
                {
                    argname = String.valueOf(arg.getValue());
                    DataCell cell = rowdata.get(argname);
                    if ( cell != null )
                    {
                        val = cell.getValue();
                    }
                    else if ( task.getData().getColumnDefinition(argname) != null )
                    {
                        val = null;
                    }
                    else
                    {
                        throw new ETLException("Undefined column " + arg + ", unable to set http request header with correct data.", arg);
                    }
                }
                else
                {
                    argname = arg.getColumn().getColumnName();
                    val = arg.getValue();
                }

                logger.debug("Header #" + j + " '" + argname + "'=" + val);
                httpHeaders.add(new ComparableHeader(argname, String.valueOf(val)));
            }

            logger.info("HTTP headers: " + httpHeaders);
            convertParamsToHttpParameters(rowdata);
            
            // Execute http request
            HttpPut put = new HttpPut(getURL(rowdata).toURI());
            put.setHeaders(httpHeaders.toArray(new Header[0]));
            if ( httpParameters.size() == 0 )
            {
                throw new ETLException("PUT calls must include one parameter for the data entity object.");
            }
            else if ( httpParameters.size() > 1 )
            {
                logger.warn("More than one parameter set for PUT call. All parameters after first will be ignored.");
            }
            else
            {
                HttpEntity entity = null;
                NameValuePair nvp = httpParameters.get(0);
                switch ( contentType )
                {
                    case application_json:
                    case text_json:
                        StringEntity se = new StringEntity(nvp.getValue());
                        //se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, String.valueOf(contentType)));
                        entity = se;
                        logger.debug("String Entity: " + nvp.getValue());
                        break;
                    case application_xml:
                    case text_xml:
                        throw new UnsupportedOperationException("XML contentTypes are currently not supported for PUT calls");
                    default:
                        throw new UnsupportedOperationException("Unsupported contentType for PUT call " + contentType);
                }
                
                logger.info("HTTP Entity: " + entity);
                put.setEntity(entity);
            }
            
            logger.trace("PUT headers=" + Arrays.asList((put.getAllHeaders())));
            logger.trace("PUT body=" + put.getEntity());
            
            logger.info("Executing " + put);
            response = connection.execute(put);
            logger.debug("HTTP response: " + response.getStatusLine());
            parseStatusLine(response);
            
            newdata.addAll(parseResponse(response, rowdata, addResponse));

            EntityUtils.consume(response.getEntity());
        }
        catch (URISyntaxException e)
        {
            throw new ETLException("Unable to parse URL to URI statement: " + getURL(rowdata), e);
        }
        catch (UnsupportedEncodingException e)
        {
            throw new ETLException("Unable to encode http entity since encoding not supported", e);
        }
        catch (IOException e)
        {
            throw new ETLException("Error communicating with http target: " + getURL(rowdata), e);
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new ETLException("Unable to disable strict hostname checking: " + e);
        }
        catch (KeyManagementException e)
        {
            throw new ETLException("Unable to access keystore: " + e);
        }
        catch (KeyStoreException e)
        {
            throw new ETLException("Unable to access keystore: " + e);
        }
        finally
        {
            try
            {
                if ( response != null )
                {
                    response.close();
                }
            }
            catch (IOException e)
            {
                logger.warn("Unable to close http response", e);
            }
        }
        
        return newdata;
    }

    private void convertParamsToHttpParameters(Map<String,DataCell> rowdata)
        throws ETLException
    {
        // Iterate through all http request parameters
        for ( int j=1; j<=params.size(); j++ )
        {
            WebArgument arg = params.get(j-1);
            String argname = "";
            Object val = null;
            if ( arg.getArgumentType() == Argument.Type.column )
            {
                argname = String.valueOf(arg.getValue());
                DataCell cell = rowdata.get(argname);
                if ( cell != null )
                {
                    val = cell.getValue();
                }
                else if ( task.getData().getColumnDefinition(argname) != null )
                {
                    val = null;
                }
                else
                {
                    throw new ETLException("Undefined column " + argname + ", unable to set http request header with correct data.", arg);
                }

                argname = arg.getArgumentName();
            }
            /*else if ( arg.getTargetDataType() == DataTypes.JSON ||
                      arg.getTargetDataType() == DataTypes.XML )
            {   
                logger.debug("Handling variable replacement for xml/json datatypes: " + arg);
                argname = arg.getArgumentName();
                val = replaceHierarchicalVariable(arg, rowdata);

                // Ensure outgoing JSON string is valid
                if ( arg.getTargetDataType() == DataTypes.JSON )
                {
                    XMLUtils.parseJSON(String.valueOf(val));
                }
                // Do I need to check for valid xml here as well? 
            }*/
            else
            {
                argname = arg.getArgumentName();
                val = replaceHierarchicalVariable(arg, rowdata);
            }

            logger.debug("Param #" + j + " '" + argname + "'=" + val);
            httpParameters.add(new BasicNameValuePair(argname, String.valueOf(val)));
        }
    }

    public void closeConnection()
        throws IOException
    {
        if ( connection != null )
        {
            connection.close();
        }
    }

    public static void main(String[] args)
    {
        try
        {
            org.apache.log4j.Appender a = new org.apache.log4j.ConsoleAppender(new org.apache.log4j.PatternLayout("%d{MM-dd HH:mm:ss} | %-5p | [%13F:%-3L] - %m%n"), "System.err");
            org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(RESTDriver.class);
            logger.addAppender(a);
            logger.setLevel(Level.TRACE);

            logger = org.apache.log4j.Logger.getLogger(HTTPDriver.class);
            logger.addAppender(a);
            logger.setLevel(Level.TRACE);
            
            WebConnection webinfo = new WebConnection();
            webinfo.setAuthType(WebConnection.AuthType.basic);
            webinfo.setUsername("Admin");
            webinfo.setPassword("Changeme#1");
            webinfo.setBaseURI("https://127.0.0.1:8888");
            webinfo.setStrictSSLCheckUsed(false);
            
            WebAPIDriver rest = AbstractDriver.getBaseDriver(DriverType.REST, null);
            rest.setRequestMethod(RequestMethod.GET);
            rest.setWebConnection(webinfo);
            rest.setAPIURI("/v1.0/Cardholders");
            rest.addHeader(new ComparableHeader("PublicKey", "B0456A0D0121A2AFC2E632285F2B314F"));
            rest.addHeader(new ComparableHeader("Accept", "application/json"));
            rest.addParameter(new BasicNameValuePair("filter", "UserText1 eq 12345678"));
            rest.addParameter(new BasicNameValuePair("includeAllData", "false"));
            rest.setContentType(ContentType.application_json, "cardholder");
            
            //rest.addResponseColumn(new XpathColumn(DataTypes.NODE, "CARDHOLDERS", "json/cardholder", null));
            //rest.addResponseColumn(new XpathColumn(DataTypes.STRING, "CARDHOLDERID", "/cardholder/CardholderId/text()", "CARDHOLDERS"));
            rest.addResponseColumn(new XpathColumn(DataTypes.STRING, "CARDHOLDERID", "/json/cardholder/CardholderId/text()", null));
            
            System.out.println("Executing " + rest.getRequestMethod() + " on " + rest.getBaseURL());
            List<Map<String,DataCell>> response = rest.execute(new HashMap(), true);

            System.exit(1);
            
            /**WebConnection webinfo = new WebConnection();
            webinfo.setAuthType(WebConnection.AuthType.oauth1);
            webinfo.setSignatureMethod(SignatureMethod.HMAC_SHA1);
            webinfo.setTempTokenAuthorizationURI("https://bbtsservices.transactsp.net/bbts/api/initiate");
            webinfo.setRealTokenAuthorizationURI("https://bbtsservices.transactsp.net/bbts/api/token");
            webinfo.setConsumerKey("E92C38AD-CDED-4411-B562-CA13C77E7EE5");
            webinfo.setConsumerSecret("dNS/RST5HTz9Pt2BlGQv");
            
            WebAPIDriver rest = AbstractDriver.getBaseDriver(DriverType.REST, null);
            rest.setRequestMethod(RequestMethod.GET);
            rest.setWebConnection(webinfo);
            rest.setURL("https://bbtsservices.transactsp.net/bbts/api/management/v1/customers/0000000000000000400004");
            rest.addHeader(new ComparableHeader("TransactSP-Institution-Route", "InstitutionRouteId " + "A4CED449-86E0-453C-8649-0BE73A411EBB"));
            rest.setContentType(ContentType.application_json, "response");
            
            rest.addResponseColumn(new XpathColumn(DataTypes.STRING, "CUSTOMER_NUMBER", "/response/customer/customerNumber/text()", null));
            rest.addResponseColumn(new XpathColumn(DataTypes.STRING, "FIRST_NAME", "/response/customer/firstName/text()", null));
            rest.addResponseColumn(new XpathColumn(DataTypes.STRING, "LAST_NAME", "/response/customer/lastName/text()", null));
            rest.addResponseColumn(new XpathColumn(DataTypes.NODE, "CARDS", "/response/customer/cardNumbers", null));
            rest.addResponseColumn(new XpathColumn(DataTypes.STRING, "CARD_NUMBER", "/cardNumbers/cardNumber/text()", "CARDS"));
            rest.addResponseColumn(new XpathColumn(DataTypes.NODE, "ADDRESSES", "/response/customer/addresses", null));
            rest.addResponseColumn(new XpathColumn(DataTypes.STRING, "ADDRESS_TYPE", "/addresses/addressType/text()", "ADDRESSES"));
            rest.addResponseColumn(new XpathColumn(DataTypes.STRING, "STREET1", "/addresses/street1/text()", "ADDRESSES"));
            rest.addResponseColumn(new XpathColumn(DataTypes.STRING, "STREET2", "/addresses/street2/text()", "ADDRESSES"));
            rest.addResponseColumn(new XpathColumn(DataTypes.NODE, "FIELDS", "/response/customer/definedFields", null));
            rest.addResponseColumn(new XpathColumn(DataTypes.STRING, "VALUE", "/definedFields/value/text()", "FIELDS"));
            rest.addResponseColumn(new XpathColumn(DataTypes.STRING, "FIELD", "/definedFields/caption/text()", "FIELDS"));
            
            System.out.println(rest.getHeaders());
            rest.addHeader(new ComparableHeader("Temp", "boo"));
            rest.addHeader(new ComparableHeader("TransactSP-Institution-Route", "InstitutionRouteId " + "E92C38AD-CDED-4411-B562-CA13C77E7EE5"));
            System.out.println(rest.getHeaders());
            
            System.exit(1);
            
            System.out.println("Executing " + rest.getRequestMethod() + " on " + rest.getBaseURL());
            List<Map<String,DataCell>> response = rest.execute(new HashMap(), true);
            System.out.println(response);
            
            int i = 1;
            for ( Map<String,DataCell> rowdata: response )
            {
                System.out.println("Outputting row #" + i++);
                for ( com.blackboard.services.etl.data.DataColumn col: rest.getResponseColumns() )
                {
                    if ( col.getDataType() != DataTypes.NODE )
                    {
                        System.out.println(col.getColumnName() + "=" + rowdata.get(col.getColumnName()));
                    }
                }
            }*/
            

            /*WebAPIDriver rest = AbstractDriver.getBaseDriver(DriverType.REST, null);
            rest.setRequestMethod(RequestMethod.POST);
            //rest.setURL("https://bbtsservices.transactsp.net/bbts/api/management/v1/customers/1");
            rest.setURL("https://bbtsservices.transactsp.net/bbts/api/token");
            rest.addHeader(new BasicHeader("TransactSP-Institution-Route", "InstitutionRouteId " + "A4CED449-86E0-453C-8649-0BE73A411EBB"));
            rest.addHeader(new BasicHeader(OAuthManager.OAUTH_HEADER_NAME, "OAuth oauth_consumer_key=\"E92C38AD-CDED-4411-B562-CA13C77E7EE5\",oauth_token=\"74fcd104-8d46-4597-a5e3-1453ceff7aa3\", oauth_signature_method=\"HMAC-SHA1\",oauth_timestamp=\"1450199696\",oauth_nonce=\"1130066695\",oauth_version=\"1.0\",oauth_signature=\"q4nwo5o4gHZ0vWU3A30aUjlPdL4%3D\""));
            rest.addHeader(new BasicHeader(HEADER_CONTENT_TYPE_NAME, "application/json"));
            //rest.setContentType(ContentType.application_json, "customer");
            rest.setContentType(ContentType.application_x_www_form_urlencoded, null);
            rest.addResponseColumn(new XpathColumn(DataTypes.STRING, "oauth_token"));
            rest.addResponseColumn(new XpathColumn(DataTypes.STRING, "oauth_token_secret"));
            
            System.out.println("Executing " + rest.getRequestMethod() + " on " + rest.getBaseURL());
            List<Map<String,DataCell>> response = rest.execute(new HashMap(), true);
            System.out.println(response);*/
            
            /*String ct = "application/x-www-form-urlencoded; charset=utf-8";
            ct = ct.replaceAll(";", ",");
            BasicHeader h = new BasicHeader(HEADER_CONTENT_TYPE_NAME, ct);
            System.out.println("Header=" + h);
            for ( HeaderElement he: h.getElements() )
            {
                System.out.println("Element=" + he);
                System.out.println("Name=" + he.getName());
                System.out.println("Value=" + he.getValue());
            }
            
            String query = "oauth_token=3c469604-8961-4877-84e2-0c581957516d&oauth_token_secret=MhPFQLLhWfVRclKkvxUt&oauth_callback_confirmed=true";
            BasicHeaderValueParser parser = BasicHeaderValueParser.INSTANCE;
            HeaderElement[] params = parser.parseElements(query.replaceAll("&", ","), null);
            for ( HeaderElement he: params )
            {
                System.out.println("Element=" + he);
                System.out.println("Name=" + he.getName());
                System.out.println("Value=" + he.getValue());
            }*/
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
