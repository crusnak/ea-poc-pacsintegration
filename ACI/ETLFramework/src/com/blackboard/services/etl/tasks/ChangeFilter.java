/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.tasks;

import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.data.DataIndex;
import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.jaxb.ChangeFilterComplexType;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.EtlTaskComplexType;
import com.blackboard.services.etl.jaxb.SimpleTransformComplexType;
import com.blackboard.services.etl.jaxb.TransformComplexType;
import com.blackboard.services.etl.process.Processor;
import com.blackboard.services.etl.transforms.AbstractTransform;
import com.blackboard.services.etl.transforms.TransformFactory;
import com.blackboard.services.etl.wrapper.DataArchiver;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author crusnak
 */
public class ChangeFilter extends AbstractTask
{
    private static final String COL_CHANGE_STATUS =     "CHANGE_STATUS";
    private static final String COL_JOB_NUMBER =        "JOB_NUMBER";
    
    private static DataColumn statusColumn =            new DataColumn(DataTypes.STRING, COL_CHANGE_STATUS);
    private static DataColumn jobNumberColumn =         new DataColumn(DataTypes.STRING, COL_JOB_NUMBER);

    private ChangeFilterComplexType base = null;
    private RelationalContainer data = null;
    private String inputTaskName = null;
    private List<String> outputTaskNames = new ArrayList();
    private Map<String,String> keyMap = new LinkedHashMap();
    private Map<String,AbstractTransform> dataMap = new LinkedHashMap();
    private RelationalContainer compareData = null;
    private int maxRecords = -1;

    public ChangeFilter(EtlTaskComplexType ct, ChangeFilterComplexType xml, ETLJobWrapper wrapper)
        throws ETLException
    {
        super(ct, wrapper);
        setData(dataSet);
        setMaxRecords(xml.getMaxRecords());

        base = xml;
        setChangeFilter(xml);
    }

    public Object getBaseObject()
    {
        return base;
    }

    @Override
    public ClassType getClassType()
    {
        return ClassType.ChangeFilter;
    }

    public String getInputTaskName()
    {
        return inputTaskName;
    }

    public List<String> getOutputTaskNames()
    {
        return outputTaskNames;
    }
    
    @Override
    public RelationalContainer getData()
        throws ETLException
    {
        setData(super.getData());
        return data;
    }
    
    private void setMaxRecords(String max)
        throws ETLException
    {
        max = wrapper.getDataArchiver().processEmbeddedBeanScript(max);
        
        try
        {
            maxRecords = Integer.parseInt(max);
        }
        catch (NumberFormatException e)
        {
            throw new ETLException("Unable to convert max records to a number: " + max);
        }
    }

    @Override
    public void setData(DataSet d)
    {
        super.setData(d);
        if ( d.getType() != DataSet.Type.relational )
        {
            throw new UnsupportedOperationException("DataSet assigned to " + this.getClass().getSimpleName() + " must be relational");
        }
        data = (RelationalContainer)d;
    }

    public void setComparisonData(RelationalContainer dc)
    {
        compareData = dc;
    }

    private void setChangeFilter(ChangeFilterComplexType filter)
        throws ETLException
    {
        if ( filter != null )
        {
            inputTaskName = filter.getInputTask();
            outputTaskNames = filter.getOutputTask();
            setKeyMap(filter.getPrimaryKeys().getKey());
            setTransformMap(filter.getCompareData().getAssignOrDateOrLookup());
        }
    }

    public Map<String,String> getKeyMap()
    {
        return keyMap;
    }
    
    private void setKeyMap(List<SimpleTransformComplexType> tforms)
    {
        if ( tforms != null )
        {
            for ( SimpleTransformComplexType ct: tforms )
            {
                keyMap.put(ct.getTargetColumn().getValue(), ct.getSourceColumn().getValue());
            }
        }
    }

    private void setTransformMap(List<TransformComplexType> tforms)
        throws ETLException
    {
        if ( tforms != null )
        {
            for ( TransformComplexType ct: tforms )
            {
                String tcol = ct.getTargetColumn().get(0).getValue();
                AbstractTransform tform = TransformFactory.createTransform(ct, this);
                dataMap.put(tcol, tform);
            }
        }
    }

    public void filterChanges(String jobnum)
        throws ETLException
    {
        // Remove any previous index and create unique data index of the primary keys on the cumulated output data
        DataIndex index = new DataIndex(DataIndex.Type.unique, new ArrayList(keyMap.keySet()));
        compareData = getPreviousComparisonData(jobnum);
        compareData.clearIndices();
        compareData.addDataIndex(index);
        compareData.indexAllData();

        logger.debug("PrimaryKeyMap: " + keyMap);
        logger.debug("DataMap: " + dataMap);

        // TODO: this has to be removed because output of data indices is extremely process intensive
        //logger.debug("DataIndices: " + compareData.getDataIndices());

        DataSet ds = wrapper.retrieveNamedData(inputTaskName);
        if ( ds == null )
        {
            throw new ETLException("Reference to undefined task in input-task for change-filter: " + inputTaskName);
        }
        
        if ( ds.getType() != DataSet.Type.relational )
        {
            throw new ETLException("Reference to a hierarchical data set. ChangeFilters only support relational data sets.");
        }
        
        RelationalContainer input = (RelationalContainer)ds;
        data.addColumnDefinitions(input.getColumnDefinitions());
        data.clearData();
        wrapper.getDataStatistics().assignTaskInputRowCount(this, input.rowCount());

        // Iterate over each row in the input data
        int count = input.rowCount();
        for ( int i=1; i<=input.rowCount(); i++ )
        {
            data.setCurrentProcessedRow(i);
            System.out.println(getTaskName() + ": Comparing row #" + i + " out of " + count);
            logger.debug("Comparing input data row #" + i);
            Map<String,DataCell> rowdata = input.getMappedData().get(i-1);
            ArrayList vals = new ArrayList();
            for ( String tcol: index.getIndexedColumnNames() )
            {
                String scol = keyMap.get(tcol);
                if ( scol == null )
                {
                    logger.warn("Can't find source column in primary key: " + tcol);
                }

                Object val = null;
                if ( rowdata.containsKey(scol) )
                {
                    val = rowdata.get(scol).getValue();
                }
                vals.add(val);
            }

            // Get the row in the comparison data that corresponds with the primary key of the input data row
            int row = index.getIndexedRow(vals);

            // If it does exist, each column's value of the input data row defined in the data map will be compared against
            // the row in the output data row.
            if ( row >= 0 )
            {
                // If any column's value differs, the row is added to the task data
                logger.debug("Found existing record in comparison data at row #" + row + " for primary key: " + vals);
                for ( String tcol: dataMap.keySet() )
                {
                    AbstractTransform tform = dataMap.get(tcol);
                    if ( tform == null )
                    {
                        logger.warn("Can't find source column in primary key: " + tcol);
                        data.addDataRow(rowdata);
                        break;
                    }
                    else
                    {
                        List<DataCell> sources = DataMapping.extractSourceData(input, tform, i-1);
                        List<DataColumn> targets = DataMapping.extractTargetColumns(compareData, tform);

                        // Only allow single target transforms
                        DataCell cell = tform.transform(targets, sources).get(0);
                        logger.debug("Transformed cell: " + cell);

                        // If the current column's values differ, then this row data has been modified since the
                        // last job run and will be included for processing
                        if ( !columnValuesEqual(cell.getValue(), compareData.getMappedData().get(row), tcol) )
                        {
                            // If a max record amount is set, check to see if new and changed records 
                            // are less than the amount, otherwise ignore
                            if ( maxRecords == -1 || data.rowCount() < maxRecords )
                            {
                                logger.debug("Row has been modified, row will be processed");
                                data.addDataRow(rowdata);
                            }
                            else
                            {
                                logger.trace("UPDATED row to process but max filter records reached so ignoring this run");
                            }
                            break;
                        }
                    }
                }
            }
            // If it doesn't exist, it is a new record and added to the task data
            else
            {
                // If a max record amount is set, check to see if new and changed records 
                // are less than the amount, otherwise ignore
                if ( maxRecords == -1 || data.rowCount() < maxRecords )
                {
                    data.addDataRow(rowdata);
                    logger.debug("NEW row to process. No record exists for primary key in comparison data: " + vals);
                }
                else
                {
                    logger.trace("NEW row to process but max filter records reached so ignoring this run");
                }
            }
        }

        // Comparison data must be archived here to ensure that the next job picks it up
        archiveComparisonData(jobnum);
    }

    public void updateFromOutput(RelationalContainer output, String jobnum)
        throws ETLException
    {
        logger.info("----------------------------Updating comparison data: " + getTaskName() + "---------------------------");

        // Get the current comparison data (archived) and add the index
        DataIndex index = new DataIndex(DataIndex.Type.unique, new ArrayList(keyMap.keySet()));
        compareData = getCurrentComparisonData(jobnum);
        logger.debug("Current columns: " + compareData.getColumnDefinitions());
        logger.debug("New data: " + data);
        compareData.clearIndices();
        compareData.addDataIndex(index);
        compareData.indexAllData();

        LinkedHashMap<String,DataColumn> cols = new LinkedHashMap();
        cols.put(COL_CHANGE_STATUS, statusColumn);
        cols.put(COL_JOB_NUMBER, jobNumberColumn);
        if ( output.getColumnDefinitions().size() == 0 )
        {
            cols.putAll(compareData.getColumnDefinitions());
        }
        else
        {
            cols.putAll(output.getColumnDefinitions());
        }
        
        compareData.addColumnDefinitions(cols);
        //compareData.setColumnDefinitions(output.getColumnDefinitions());

        logger.debug("Output columns: " + output.getColumnDefinitions());
        
        // TODO: this has to be removed because output of data indices is extremely process intensive
        //logger.debug("DataIndices: " + compareData.getDataIndices());

        // Iterate over each successful output row and add the row in the comparison data,
        // or overwrite the existing row.
        for ( int i=1; i<=output.rowCount(); i++ )
        {
            logger.debug("Comparing output data row #" + i);
            Map<String,DataCell> rowdata = output.getMappedData().get(i-1);
            ArrayList vals = new ArrayList();
            for ( String tcol: index.getIndexedColumnNames() )
            {
                Object val = null;
                if ( rowdata.containsKey(tcol) )
                {
                    val = rowdata.get(tcol).getValue();
                }
                vals.add(val);
            }

            // Get the row in the comparison data that corresponds with the primary key of the input data row
            int row = index.getIndexedRow(vals);
            if ( row >= 0 )
            {
                rowdata.put(COL_CHANGE_STATUS, new DataCell(statusColumn, "UPDATE"));
                rowdata.put(COL_JOB_NUMBER, new DataCell(jobNumberColumn, jobnum));
                compareData.setDataRow(rowdata, row);
                logger.debug("Overwriting existing record in comparison data at row #" + (row+1) + " for primary key: " + vals);
            }
            else
            {
                rowdata.put(COL_CHANGE_STATUS, new DataCell(statusColumn, "NEW"));
                rowdata.put(COL_JOB_NUMBER, new DataCell(jobNumberColumn, jobnum));
                compareData.addDataRow(rowdata);
                logger.debug("NEW row found. Adding to comparison data: " + vals);
            }
        }

        // Comparison data must be archived here to ensure that the next job picks it up
        archiveComparisonData(jobnum);
    }

    private boolean columnValuesEqual(Object sval, Map<String,DataCell> tdata, String tcol)
    {
        Object tval = null;
        boolean equals = false;
        if ( tdata.containsKey(tcol) )
        {
            tval = tdata.get(tcol).getValue();
        }
        
        equals = (sval == null && tval == null) || (sval != null && sval.equals(tval));
        logger.debug("Comparing: " + sval + " == " + tval + ": " + equals);
        return equals;
    }
    
    private DataArchiver getArchiverForComparison()
    {
        DataArchiver archiver = null;
        if ( wrapper.getParent() == null )
        {
            archiver = wrapper.getDataArchiver();
            logger.info("Getting data archiver for comparison from current job wrapper: " + wrapper);
        }
        else
        {
            archiver = wrapper.getFirstAncestor().getDataArchiver();
            //archiver = wrapper.getDataArchiver();
            logger.info("Getting data archiver for comparison from ancestral job wrapper: " + wrapper.getFirstAncestor());
        }
        
        return archiver;
    }

    private RelationalContainer getPreviousComparisonData(String jobnum)
        throws ETLException
    {
        RelationalContainer dc = new RelationalContainer(name + "-compare");
        DataArchiver archiver = getArchiverForComparison();
        logger.info("Retrieving comparison data from previous job run");

        File prevDir = archiver.getRunningArchiveDirectory(jobnum);
        if ( prevDir != null )
        {
            File file = new File(prevDir, Processor.CDC_DIR + "/" + getComparisonFileName(Processor.DAT_EXT));
            try
            {
                logger.debug("Loading data from: " + file.getCanonicalPath());
                dc = (RelationalContainer)archiver.readDataFromBinaryFile(file);
                logger.debug("Archived columns: " + dc.getColumnDefinitions());
                for ( Map<String,DataCell> rowdata: dc.getMappedData() )
                {
                    rowdata.put(COL_CHANGE_STATUS, new DataCell(statusColumn, "OLD"));
                }

            }
            catch (FileNotFoundException e) { logger.warn("No previous comparison data found, no data will be filtered out"); }
            catch (IOException e) { logger.warn("Unable to load previous comparison data found, no data will be filtered out"); }
            catch (ClassNotFoundException e) {throw new ETLException(e);}
            
        }

        logger.debug("Previous data=" + dc);
        
        return dc;
    }
    
    private RelationalContainer getCurrentComparisonData(String jobnum)
        throws ETLException
    {
        RelationalContainer dc = new RelationalContainer(name + "-compare");
        DataArchiver archiver = getArchiverForComparison();
        logger.info("Retrieving comparison data from current job run");

        File dir = archiver.getRunningArchiveDirectory(jobnum);
        if ( dir != null )
        {
            File file = new File(dir, Processor.CDC_DIR + "/" + getComparisonFileName(Processor.DAT_EXT));
            try
            {
                logger.debug("Loading data from: " + file.getCanonicalPath());
                dc = (RelationalContainer)archiver.readDataFromBinaryFile(file);
            }
            catch (IOException e) {throw new ETLException(e);}
            catch (ClassNotFoundException e) {throw new ETLException(e);}
        }
        
        logger.debug("Current data=" + dc);

        return dc;
    }

    public void archiveComparisonData(String jobnum)
        throws ETLException
    {
        DataArchiver archiver = getArchiverForComparison();
        File archiveDir = archiver.getRunningArchiveDirectory(jobnum);
        File cdcDir = new File(archiveDir, Processor.CDC_DIR);
        File file = new File(cdcDir, getComparisonFileName(Processor.DAT_EXT));
        cdcDir.mkdirs();

        // Archive the data to text file if task data archiving is enabled
        try
        {
            if ( archiver.isTaskDataToBeArchived() )
            {
                File textFile = new File(archiveDir, Processor.CDC_DIR + "/" + getComparisonFileName(Processor.TXT_EXT));
                archiver.writeDataToTextFile(compareData, textFile);
                logger.debug("compareData for text output: " + compareData);
            }
        }
        catch (IOException e)
        {
            throw new ETLException("Unable to archive textual comparison data for: " + getTaskName(), e);
        }

        try
        {
            archiver.writeDataToBinaryFile(compareData, file);
            logger.info("Comparison data archived to file: " + file.getCanonicalPath());
        }
        catch (IOException e)
        {
            throw new ETLException("Unable to archive comparison data for later retrieval", e);
        }
        
        logger.debug("Archived data=" + compareData);

        compareData.clearData();
    }
    
    private String getComparisonFileName(String ext)
    {
        String jobname = "";
        if ( wrapper.getParent() != null )
        {
            jobname = wrapper.getJobName() + ".";
        }
        
        return jobname + name + ext;
    }
}
