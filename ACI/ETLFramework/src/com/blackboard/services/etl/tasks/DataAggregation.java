/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.tasks;

import com.blackboard.services.etl.data.Argument;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.jaxb.AggregateDataComplexType;
import com.blackboard.services.etl.jaxb.ArgumentComplexType;
import com.blackboard.services.etl.jaxb.EtlTaskComplexType;
import com.blackboard.services.etl.transforms.AggregateTransform;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author crusnak
 */
public class DataAggregation extends AbstractTask
{
    private AggregateDataComplexType base = null;
    private RelationalContainer data = null;
    private String source = null;
    private List<String> groupColumns = new ArrayList();
    private List<String> includeColumns = new ArrayList();
    private List<DataColumn> targetColumns = new ArrayList();
    private List<AggregateTransform> aggregates = new ArrayList();
    private boolean includeRemaining = false;
    private boolean depletion = false;
    private int roundingPrecision = 0;
    
    public DataAggregation(List<String> cols)
        throws ETLException
    {
        data = new RelationalContainer("on-the-fly");
        groupColumns = cols;
    }

    public DataAggregation(EtlTaskComplexType ct, AggregateDataComplexType xml, ETLJobWrapper w)
        throws ETLException
    {
        super(ct, w);
        setData(dataSet);

        base = xml;
        source = xml.getSource();
        roundingPrecision = xml.getRoundingPrecision();
        
        setAggregates(xml.getAggregateOrTotalOrDepletion());
        setGroupBy(xml.getGroupBy());
        setInclude(xml.getInclude());
    }
    
    @Override
    public ClassType getClassType()
    {
        return ClassType.DataAggregation;
    }

    public Object getBaseObject()
    {
        return base;
    }

    public String getSource()
    {
        return source;
    }

    @Override
    public RelationalContainer getData()
        throws ETLException
    {
        setData(super.getData());
        return data;
    }

    @Override
    public void setData(DataSet d)
    {
        super.setData(d);
        if ( d.getType() != DataSet.Type.relational )
        {
            throw new UnsupportedOperationException("DataSet assigned to " + this.getClass().getSimpleName() + " must be relational");
        }
        data = (RelationalContainer)d;
    }

    public List<String> getGroupByColumns()
    {
        return groupColumns;
    }

    private void setAggregates(List ct)
        throws ETLException
    {
        if ( ct != null )
        {
            boolean nonDepletion = false;
            for (Object o: ct )
            {
                if ( o instanceof AggregateDataComplexType.Aggregate )
                {
                    AggregateTransform at = new AggregateTransform((AggregateDataComplexType.Aggregate)o, this);
                    aggregates.add(at);
                    targetColumns.add(at.getTargetColumn());
                    nonDepletion = true;
                }
                else if ( o instanceof AggregateDataComplexType.Total )
                {
                    AggregateTransform at = new AggregateTransform((AggregateDataComplexType.Total)o, this);
                    aggregates.add(at);
                    targetColumns.add(at.getTargetColumn());
                    nonDepletion = true;
                }
                else if ( o instanceof AggregateDataComplexType.Depletion )
                {
                    AggregateTransform at = new AggregateTransform((AggregateDataComplexType.Depletion)o, this);
                    aggregates.add(at);
                    targetColumns.add(at.getTargetColumn());
                    depletion = true;
                }
                else
                {
                    throw new IllegalArgumentException("Unexpected object type: " + o.getClass());
                }
                
                // If both depletion and group/total aggregates are specified, throw an error
                if ( depletion && nonDepletion )
                {
                    throw new IllegalArgumentException("Depletion aggregates cannot coexist with group or total aggregates. Use separate aggregate tasks in order to handle this case.");
                }
            }
        }
    }

    private void setGroupBy(AggregateDataComplexType.GroupBy ct)
    {
        if ( ct != null )
        {
            for ( String col: ct.getColumn() )
            {
                groupColumns.add(col);
            }
        }
    }

    private void setInclude(AggregateDataComplexType.Include ct)
    {
        if ( ct != null )
        {
            includeRemaining = ct.isIncludeAll();
            for ( String col: ct.getColumn() )
            {
                includeColumns.add(col);
            }
        }
    }

    public RelationalContainer aggregate(RelationalContainer incoming)
        throws ETLException
    {
        RelationalContainer errors = new RelationalContainer(name + ERROR_DATA_NAME);
        Map<String,Map<String,DataCell>> groupMap = new LinkedHashMap();
        Map<String,DataCell> totalMap = new LinkedHashMap();
        Map<String,DataCell> depletionMap = new LinkedHashMap();
        Map<String,Map<String,Set>> distinctMap = new LinkedHashMap();
        setData(new RelationalContainer(name));
        
        if ( incoming != null && incoming.rowCount() > 0 )
        {
            // If adding all columns defined add here since the contents of the data set isn't
            // known until runtime
            if ( includeRemaining )
            {
                for ( String colname: incoming.getColumnDefinitions().keySet() )
                {
                    if ( !groupColumns.contains(colname) && !includeColumns.contains(colname) &&
                         !targetColumns.contains(colname) )
                    {
                        includeColumns.add(colname);
                    }
                }
            }

            // Add column definitions
            for ( String colname: groupColumns )
            {
                DataColumn col = incoming.getColumnDefinition(colname);
                if ( col == null )
                {
                    throw new ETLException("Grouping column not defined in source data: " + colname);
                }

                data.addColumnDefinition(new DataColumn(col.getDataType(), col.getColumnName(), col.getDefaultValue()));
            }

            // Now add all targeted columns
            logger.debug("Adding target columns: " + targetColumns);
            data.addColumnDefinitions(targetColumns);
            
            logger.debug("Adding include columns: " + includeColumns);
            for ( String colname: includeColumns )
            {
                DataColumn col = incoming.getColumnDefinition(colname);
                if ( col == null )
                {
                    throw new ETLException("Column not defined in source data: " + colname);
                }

                data.addColumnDefinition(new DataColumn(col.getDataType(), col.getColumnName(), col.getDefaultValue()));
            }
            

            // Set up the totals map
            /*for ( AggregateTransform tform: aggregates )
            {
                if ( tform.getAggregateType() == AggregateTransform.Type.total )
                {
                    // first ensure that each group filter column is in the groupColumn list
                    DataColumn target = tform.getTargetColumn();
                    String key = generateTotalKey(rowdata, tform.getGroupFilterColumnNames(), target.getColumnName());
                    totalMap.put(key, new DataCell(target, 0));
                }
            }*/

            int count = incoming.rowCount();
            Iterator<Map<String,DataCell>> it = incoming.getMappedData().iterator();
            for ( int i=1; it.hasNext(); i++ )
            {
                data.setCurrentProcessedRow(i);
                System.out.println(getTaskName() + ": Aggregating row #" + i + " out of " + count);
                logger.debug("----------------AGGREGATING ROW #" + i + "----------------");
                Map<String,DataCell> rowdata = it.next();
                Map<String,DataCell> groupdata = new HashMap();
                Map<String,Set> distinctSets = new HashMap();
                logger.debug("Aggregating data from row #" + i + ": " + rowdata);

                String key = generateKey(rowdata);
                if ( groupMap.containsKey(key) )
                {
                    groupdata = groupMap.get(key);
                }
                else
                {
                    groupMap.put(key, groupdata);
                }

                // Do the same for distinctCounts
                if ( distinctMap.containsKey(key) )
                {
                    distinctSets = distinctMap.get(key);
                }
                else
                {
                    distinctMap.put(key, distinctSets);
                }

                // Add all the group columns to the data set
                for ( String colname: groupColumns )
                {
                    DataColumn col = data.getColumnDefinition(colname);
                    DataCell cell = rowdata.get(colname);
                    Object val = null;
                    if ( cell != null )
                    {
                        val = cell.getValue();
                    }
                    DataCell newcell = new DataCell(col, val);
                    groupdata.put(colname, newcell);
                    logger.debug("Assigning " + colname + "=" + newcell);
                }

                // Add all the included columns (not part of the aggregate but included in the data set).
                // We are always ignoring null values.
                for ( String colname: includeColumns )
                {
                    DataColumn col = data.getColumnDefinition(colname);
                    DataCell cell = rowdata.get(colname);
                    if ( cell != null && cell.getValue() != null )
                    {
                        // TODO: Add code that throws error if more than one row has differing (non-null) data
                        DataCell newcell = new DataCell(col, cell.getValue());
                        groupdata.put(colname, newcell);
                        logger.debug("Assigning " + colname + "=" + newcell);
                    }
                    else if ( !groupdata.containsKey(colname) )
                    {
                        DataCell nullcell = new DataCell(col, null);
                        groupdata.put(colname, nullcell);
                        logger.debug("Assigning " + colname + "=NULL");
                    }
                }

                // Perform all data aggregation
                for ( AggregateTransform tform: aggregates )
                {
                    // If this is a depletion aggregate we need to add any value columns to the included column list
                    /*if ( depletion )
                    {
                        ArrayList<String> clist = new ArrayList();
                        clist.add(tform.getGroupValueColumnNames().get(0));
                        clist.add(tform.getGroupValueColumnNames().get(1));
                        
                        // Add the depletion columns to the data set
                        for ( String colname: clist )
                        {
                            DataColumn col = incoming.getColumnDefinition(colname);
                            if ( col == null )
                            {
                                throw new ETLException("Column not defined in source data: " + colname);
                            }

                            data.addColumnDefinition(new DataColumn(col.getDataType(), col.getColumnName(), col.getDefaultValue())); 

                            // Add the original values for each depletion and source name and value columns                 
                            groupdata.put(colname, rowdata.get(colname));
                        }                   
                    }*/
                    
                    DataCell scell = null;
                    Argument src = tform.getSourceArgument();
                    if ( src != null )
                    {
                        switch ( src.getArgumentType())
                        {
                            case column:
                                scell = rowdata.get(src.getValue());
                                if ( scell == null )
                                {
                                    logger.warn("Unable to transform since source data is missing column: " + src);
                                    scell = new DataCell(src.getColumn(), null);
                                }
                                break;
                            case constant:
                                scell = new DataCell(src.getColumn(), src.getValue());
                                break;
                        }

                        // If a source is declared for a count this means that if the value for that source
                        // is null, the count is not incremented
                        if ( tform.getFunction() == AggregateTransform.Function.count )
                        {
                            if ( scell == null || scell.getValue() == null )
                            {
                                scell = new DataCell(src.getColumn(), 0);
                            }
                            else
                            {
                                scell = new DataCell(src.getColumn(), 1);
                            }
                        }
                        // If a source is declared for a count that is either null or was counted before,
                        // the count is not incremented
                        else if ( tform.getFunction() == AggregateTransform.Function.distinctCount )
                        {
                            Object val = null;
                            Set distinctSet = new HashSet();
                            String colname = tform.getTargetColumn().getColumnName();
                            if ( distinctSets.containsKey(colname) )
                            {
                                distinctSet = distinctSets.get(colname);
                            }
                            else
                            {
                                distinctSets.put(colname, distinctSet);
                            }
                            
                            logger.debug("Distinct set: " + distinctSet);
                            
                            if ( scell == null || scell.getValue() == null ||
                                 distinctSet.contains(scell.getValue()) )
                            {
                                scell = new DataCell(src.getColumn(), 0);
                            }
                            else
                            {
                                val = scell.getValue();
                                scell = new DataCell(src.getColumn(), 1);
                                distinctSet.add(val);
                            }
                        }
                    }

                    DataColumn tcol = tform.getTargetColumn();
                    switch (tform.getAggregateType())
                    {
                        case group:
                            DataCell tcell = groupdata.get(tcol.getColumnName());
                            logger.debug("Performing aggregate transform: " + tform);
                            logger.debug("Source cell: " + scell);
                            logger.debug("Previous cell: " + tcell);
                             
                            DataCell agg = tform.transform(scell, tcell);
                            if ( roundingPrecision != 0 )
                            {
                                DecimalFormat df = new DecimalFormat(getRoundingPattern(roundingPrecision));
                                logger.debug("round format=" + df.toPattern());
                                logger.debug("before: " + agg);
                                agg.setValue(df.format(agg.getValue()));
                                logger.debug("after: " + agg);
                            }
                            
                            groupdata.put(tcol.getColumnName(), agg);                            
                            logger.debug("Aggregate cell: " + agg);
                            break;
                        case total:
                            key = generateTotalKey(rowdata, tform.getGroupFilterColumnNames(), tcol.getColumnName());
                            tcell = totalMap.get(key);
                            logger.debug("Performing total transform: " + tform);
                            logger.debug("Source cell: " + scell);
                            logger.debug("Previous cell: " + tcell);

                            logger.debug("Totals: " + totalMap);
                            agg = tform.transform(scell, tcell);
                            groupdata.put(tcol.getColumnName(), agg);
                            totalMap.put(key, agg);

                            logger.debug("Total cell: " + agg);
                            break;
                        case depletion:
                            logger.debug("Performing depletion transform: " + tform);
                            logger.debug("groupdata: " + groupdata);
                            List<String> keys = generateDepletionKeys(rowdata, tform.getGroupFilterColumnNames(), tform.getGroupValueColumnNames());
                            logger.debug("Depletion keys: " + keys);
                            String dkey = keys.get(0);
                            String skey = keys.get(1);
                            
                            // Prime depletion key value with total if null
                            DataCell dcell = depletionMap.get(dkey);
                            if ( dcell == null )
                            {
                                dcell = rowdata.get(tform.getGroupValueColumnNames().get(0));
                                depletionMap.put(dkey, dcell);
                            }
                            
                            // Prime source key value with total if null
                            scell = depletionMap.get(skey);
                            if ( scell == null )
                            {
                                scell = rowdata.get(tform.getGroupValueColumnNames().get(1));
                                depletionMap.put(skey, scell);
                            }
                            
                            logger.debug("Depletion values: " + depletionMap);
                            
                            // Only attempt to deplete a value column if both value columns are non-zero
                            if ( (scell != null && scell.getValue() != null && Double.parseDouble(String.valueOf(scell.getValue())) > 0) && 
                                 (dcell != null && dcell.getValue() != null && Double.parseDouble(String.valueOf(dcell.getValue())) > 0) )
                            {
                                // Identifying if depletion value is depleted
                                DataCell agg1 = tform.transform(scell, dcell);
                                logger.debug("dcell - scell = " + agg1);
                                depletionMap.put(dkey, agg1);
                                
                                // Identifying if source value is depleted
                                DataCell agg2 = tform.transform(dcell, scell);
                                logger.debug("scell - dcell = " + agg2);
                                depletionMap.put(skey, agg2);

                                // If the source was depleted, the value depleted was the full value of the source at the time of this row process
                                DataCell val = null;
                                if ( agg2 != null && agg2.getValue() != null && Double.parseDouble(String.valueOf(agg2.getValue())) <= 0 )
                                {
                                    val = new DataCell(scell.getColumn(), scell.getValue());
                                }
                                // Otherwise, the depletion was depleted before the source, so the value depleted the full value of depletion at the time of this row process
                                else
                                {
                                    val = new DataCell(dcell.getColumn(), dcell.getValue());
                                }

                                //ArrayList clist = new ArrayList(groupColumns);
                                //clist.addAll(tform.getGroupFilterColumnNames());
                                //String gkey = generateKey(rowdata, clist);
                                DecimalFormat df = new DecimalFormat(getRoundingPattern(roundingPrecision));
                                logger.debug("round format=" + df.toPattern());
                                logger.debug("before: " + val);
                                val.setValue(df.format(val.getValue()));
                                logger.debug("after: " + val);
                                groupdata.put(tcol.getColumnName(), val);
                                logger.debug(groupdata);
                                //groupMap.put(gkey, groupdata);
                            }
                            else
                            {
                                logger.debug("Either source or depletion value is null or 0, removing from group data.");
                                groupMap.remove(key);
                            }

                            
                            break;
                    }
                }
            }
            
            logger.debug(groupMap);
            data.addAllRows(groupMap.values());
        }

        logger.debug("Column defs: " + data.getColumnDefinitions());

        return errors;
    }

    private String generateTotalKey(Map<String,DataCell> data, List<String> columns, String target)
    {
        String key = generateKey(data, columns);
        key += "->" + target;
        return key;
    }

    private List<String> generateDepletionKeys(Map<String,DataCell> data, List<String> names, List<String> values)
    {
        ArrayList<String> keys = new ArrayList();
        ArrayList columns = new ArrayList();
        
        // Ensure only depletion column is in key, no source
        columns.addAll(groupColumns);
        columns.remove(names.get(1));
        String key = generateKey(data, columns);
        key += "->" + values.get(0);
        keys.add(key);
        
        // Ensure only source column is in key, no depletion
        columns.clear();
        columns.addAll(groupColumns);
        columns.remove(names.get(0));
        key = generateKey(data, columns);
        key += "->" + values.get(1);
        keys.add(key);
        
        return keys;
    }

    private String generateKey(Map<String,DataCell> data, List<String> columns)
    {
        String key = "";
        if ( data != null )
        {
            for ( int i=0; i<columns.size(); i++ )
            {
                String col = columns.get(i);
                DataCell cell = data.get(col);
                if ( cell == null )
                {
                    logger.warn("Column definition not found in incoming data, this may affect groupings: " + col);
                }
                else
                {
                    key += String.valueOf(cell.getValue());
                }

                if ( i<columns.size()-1 )
                {
                    key += "|";
                }
            }
        }

        return key;
    }

    private String generateKey(Map<String,DataCell> data)
    {
        return generateKey(data, groupColumns);
    }

    private String getRoundingPattern(int precision)
    {
        String pattern = "0";
        if ( precision > 0 )
        {
            pattern = "0.";
            for ( int i=0; i<precision; i++ )
            {
                pattern += "0";
            }
        }
        return pattern;
    }
    
    public String toString()
    {
        StringBuffer out = new StringBuffer();

        return out.toString();
    }
}