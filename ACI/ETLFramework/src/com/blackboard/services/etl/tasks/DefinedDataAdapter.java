/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.tasks;

import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.jaxb.DataComplexType;
import com.blackboard.services.etl.jaxb.DefineInputTaskComplexType;
import com.blackboard.services.etl.jaxb.EtlTaskComplexType;
import com.blackboard.services.etl.jaxb.InputTaskComplexType;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.blackboard.services.utils.BeanScript;
import com.blackboard.services.utils.JavaUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author crusnak
 */
public class DefinedDataAdapter extends AbstractAdapter implements Adapter
{
    public static enum DefineType { assign, join };

    private DefineInputTaskComplexType da = null;
    private RelationalContainer data = null;
    private DefineType defineType = null;
    private String iterateDataName = "";
    private RelationalContainer iterateData = null;
    private List<DataColumn> declaredColumns = new ArrayList();

    public DefinedDataAdapter(EtlTaskComplexType ct, InputTaskComplexType xml, ETLJobWrapper w)
        throws ETLException
    {
        super(ct, xml, w);
        setData(dataSet);

        if ( xml.getDefineInput() == null )
        {
            throw new ETLException("Expecting DefineInput in InputAdapter");
        }

        DefineInputTaskComplexType in = xml.getDefineInput();
        da = in;
        setIterateData(da.getForEach());
        setDeclaredColumns(da.getInputData());
        setDefinedData(da.getDefine());
    }

    public DefineInputTaskComplexType getBaseObject()
    {
        return da;
    }

    public Type getAdapterType()
    {
        return Adapter.Type.Defined;
    }

    @Override
    public RelationalContainer getData()
        throws ETLException
    {
        setData(super.getData());
        return data;
    }

    @Override
    public void setData(DataSet d)
    {
        super.setData(d);
        if ( d.getType() != DataSet.Type.relational )
        {
            throw new UnsupportedOperationException("DataSet assigned to " + this.getClass().getSimpleName() + " must be relational");
        }
        data = (RelationalContainer)d;
    }

    public RelationalContainer getIteratorData()
    {
        return iterateData;
    }

    public DefineType getDefineType()
    {
        return defineType;
    }

    private void setDefineType(String o)
        throws ETLException
    {
        if ( o == null )
        {
            throw new ETLException("DefineType cannot be null for DefineInput.Define, use one of: " + JavaUtils.enumToList(DefineType.class));
        }

        try
        {
            defineType = DefineType.valueOf(o);
        }
        catch (IllegalArgumentException e)
        {
            throw new ETLException("Unsupported select type '" + o + "', expected: " + JavaUtils.enumToList(DefineType.class), e);
        }
    }

    private void setDeclaredColumns(DefineInputTaskComplexType.InputData input)
        throws ETLException
    {
        if ( input != null )
        {
            for ( DataComplexType ct: input.getDataElement() )
            {
                DataColumn col = data.addColumnDefinition(String.valueOf(ct.getType()), ct.getValue(), ct.getDefaultValue());
                declaredColumns.add(col);
                logger.debug("Column defined: " + col);
            }
        }
        else
        {
            throw new ETLException("DefinedData must declare input columns");
        }
    }

    private void setIterateData(String name)
        throws ETLException
    {
        if ( name != null && name.trim().length() > 0 )
        {
            iterateDataName = name;
        }
    }

    private void setDefinedData(DefineInputTaskComplexType.Define define)
        throws ETLException
    {
        if ( define != null )
        {
            setDefineType(define.getType());
        }
    }

    public void defineData()
        throws ETLException
    {
        if ( da != null && da.getDefine() != null )
        {
            // If joining data, incoming count is the iterate data count
            if ( getDefineType() == DefinedDataAdapter.DefineType.join )
            {
                DataSet ds = wrapper.retrieveNamedData(iterateDataName);
                if ( ds == null )
                {
                    throw new ETLException("Undefined iterate/source data: " + ds);
                }
                if ( ds.getType() != DataSet.Type.relational )
                {
                    throw new ETLException("Reference to a hierarchical data set. DefinedDataAdapters only support relational data sets.");
                }
                
                iterateData = (RelationalContainer)ds;
                wrapper.getDataStatistics().assignTaskInputRowCount(this, iterateData.rowCount());
            }

            DefineInputTaskComplexType.Define define = da.getDefine();
            switch (defineType)
            {
                case assign:
                    for ( int i=1; i<=define.getRow().size(); i++ )
                    {
                        System.out.println("Adding data to row #" + i);
                        data.setCurrentProcessedRow(i);
                        logger.debug("Adding data to row #" + i);
                        HashMap<String,DataCell> rowdata = new HashMap();
                        addDefinedData(define, rowdata, i-1);
                        data.addDataRow(rowdata);
                        logger.debug("Defined data row #" + i + ": " + rowdata);
                    }
                    break;
                case join:
                    if ( iterateData == null )
                    {
                        throw new ETLException("Iterate data is null. Join types require for-each data to be declared.");
                    }

                    // Add iterate data column definitions
                    for ( DataColumn col: iterateData.getColumnDefinitions().values() )
                    {
                        data.addColumnDefinition(new DataColumn(col.getDataType(), col.getColumnName(), col.getDefaultValue()));
                    }

                    int total = iterateData.rowCount();
                    for ( int i=1; i<=iterateData.rowCount(); i++ )
                    {
                        System.out.println("Joining data to row #" + i + " of " + total);
                        data.setCurrentProcessedRow(i);
                        for ( int j=1; j<=define.getRow().size(); j++ )
                        {
                            logger.debug("Joining data to row #(" + i + "," + j + ")");
                            Map<String,DataCell> rowdata = new HashMap(iterateData.getMappedData().get(i-1));
                            addDefinedData(define, rowdata, j-1);
                            data.addDataRow(rowdata);
                            logger.debug("Joined data row #(" + i + "," + j + "): " + rowdata);
                        }
                    }
                    break;
            }
        }
        else
        {
            throw new ETLException("DefinedData must declare at least one row of data");
        }
    }

    private void addDefinedData(DefineInputTaskComplexType.Define define,
                                Map<String,DataCell> rowdata, int index)
        throws ETLException
    {
        for ( int j=0; j<define.getRow().get(index).getColumn().size(); j++ )
        {
            DefineInputTaskComplexType.Define.Row.Column col = define.getRow().get(index).getColumn().get(j);
            String name = col.getName();
            DataColumn column = null;
            if ( name != null )
            {
                column = data.getColumnDefinition(name);
                if ( column == null )
                {
                    throw new ETLException("Reference to undefined column in row data for DefinedData: " + name);
                }
            }
            else
            {
                column = declaredColumns.get(j);
            }


            BeanScript bs = new BeanScript(col.getValue().trim());
            Object value = null;
            if ( bs.getAccessorText() != null )
            {
                DataCell gvar = wrapper.retrieveGlobalVariable(bs.getAccessorText());
                if ( gvar == null )
                {
                    throw new ETLException("Argument reference to undefined global variable: " + bs.getAccessorText());
                }

                value = gvar.getValue();
            }
            else
            {
                value = col.getValue();
            }

            DataCell cell = new DataCell(column, value);
            rowdata.put(column.getColumnName(), cell);
            logger.debug("Adding cell to column '" + column.getColumnName() + "': " + cell);
        }
    }
}
