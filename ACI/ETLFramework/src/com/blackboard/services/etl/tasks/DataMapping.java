/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.tasks;

import com.blackboard.services.etl.data.Argument;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.data.DataIndex;
import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.etl.data.TransformSource;
import com.blackboard.services.etl.data.TransformTarget;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.exception.IndexException;
import com.blackboard.services.etl.exception.SortException;
import com.blackboard.services.etl.exception.TransformException;
import com.blackboard.services.etl.exception.TransformIgnoreException;
import com.blackboard.services.etl.exception.VariableTypeMismatchException;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.EtlTaskComplexType;
import com.blackboard.services.etl.jaxb.MappingTaskComplexType;
import com.blackboard.services.etl.jaxb.TransformComplexType;
import com.blackboard.services.etl.transforms.AbstractTransform;
import com.blackboard.services.etl.transforms.TransformFactory;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.blackboard.services.utils.DelimittedFormat;
import com.blackboard.services.utils.JavaUtils;
import com.blackboard.services.utils.TextFormatException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;

/**
 *
 * @author crusnak
 */
public class DataMapping extends AbstractTask
{
    public static enum MappingType { replace, add, union, intersect, assign }
    public static enum HiddenDataType { value, rownum, rowcount, colcount };

    public static final String COL_HIDDEN_ROWNUM =      "[ROWNUM]";
    public static final String COL_HIDDEN_ROWCOUNT =    "[ROWCOUNT]";
    public static final String COL_HIDDEN_COLCOUNT =    "[COLCOUNT]";

    public static final String COL_CELL_SOURCES =   "ERROR_DATA_SOURCES";
    public static final String COL_CELL_ERROR =     "ERROR_DATA_CELL";

    private MappingTaskComplexType map = null;
    private RelationalContainer data = null;
    private String source = null;
    private String target = null;
    private MappingType mtype = MappingType.replace;
    private HashSet<String> sourceColumns = new HashSet();
    private HashSet<String> targetColumns = new HashSet();
    private ArrayList<AbstractTransform> transforms = new ArrayList();
    private RelationalContainer incomingData = new RelationalContainer("");
    private RelationalContainer outgoingData = new RelationalContainer("");
    private boolean copyData = false;
    private int outgoingCount = 0;

    public DataMapping(EtlTaskComplexType ct, MappingTaskComplexType xml, ETLJobWrapper w)
        throws ETLException
    {
        super(ct, w);
        setData(dataSet);

        map = xml;
        source = ((map.getSource()!=null)?map.getSource():name);
        target = ((map.getTarget()!=null)?map.getTarget():name);
        if ( ( source == null || source.trim().length() == 0 ) &&
             ( target == null || target.trim().length() == 0 ) )
        {
            throw new ETLException("Source and target cannot both be undefined for DataMapping");
        }

        copyData = map.isCopyData();
        setMappingType(map.getType());
        setTransforms(map.getAssignOrDateOrLookup());
    }

    @Override
    public ClassType getClassType()
    {
        return ClassType.DataMapping;
    }

    public Object getBaseObject()
    {
        return map;
    }

    public String getSource()
    {
        return source;
    }

    public String getTarget()
    {
        return target;
    }

    public MappingType getMappingType()
    {
        return mtype;
    }

    @Override
    public RelationalContainer getData()
        throws ETLException
    {
        setData(super.getData());
        return data;
    }

    @Override
    public void setData(DataSet d)
    {
        super.setData(d);
        if ( d.getType() != DataSet.Type.relational )
        {
            throw new UnsupportedOperationException("DataSet assigned to " + this.getClass().getSimpleName() + " must be relational");
        }
        data = (RelationalContainer)d;
    }

    public static HiddenDataType getHiddenDataType(Object val)
    {
        HiddenDataType type = HiddenDataType.value;
        if ( COL_HIDDEN_ROWNUM.equals(String.valueOf(val)) )
        {
            type = HiddenDataType.rownum;
        }
        else if ( COL_HIDDEN_ROWCOUNT.equals(String.valueOf(val)) )
        {
            type = HiddenDataType.rowcount;
        }
        if ( COL_HIDDEN_COLCOUNT.equals(String.valueOf(val)) )
        {
            type = HiddenDataType.colcount;
        }
        return type;
    }

    private void setMappingType(String type)
    {
        mtype = MappingType.valueOf(type);
    }

    public List<AbstractTransform> getTransforms()
    {
        return transforms;
    }

    public RelationalContainer getIncomingData()
    {
        return incomingData;
    }

    public void setIncomingData(RelationalContainer incomingData)
    {
        this.incomingData = incomingData;
    }

    public RelationalContainer getOutgoingData()
    {
        return outgoingData;
    }

    public int getOutgoingDataCount()
    {
        return outgoingCount;
    }

    public void setOutgoingData(RelationalContainer outgoingData)
    {
        this.outgoingData = outgoingData;
    }

    public boolean isTargetDataCopiedFromSource()
    {
        return copyData;
    }

    private void setTransforms(List<TransformComplexType> tforms)
        throws ETLException
    {
        for ( TransformComplexType tform: tforms )
        {
            AbstractTransform tf = TransformFactory.createTransform(tform, this);
            transforms.add(tf);

            // Need to add all unique source and target column names
            targetColumns.addAll(tf.getTargetColumnNames());
            for ( Argument arg: tf.getSourceColumns() )
            {
                if ( arg.getArgumentType() == Argument.Type.column )
                {
                    sourceColumns.add(String.valueOf(arg.getValue()));
                }
            }
        }
    }
    
    public RelationalContainer mapData(String jobnum)
        throws ETLException
    {
        logger.debug("DataMapping: " + this);
        // Get source data
        RelationalContainer src = null;
        RelationalContainer dest = null;
        boolean taskDataAsSource = false;

        logger.info("Loading SourceData: " + getSource());
        DataSet ds = wrapper.retrieveNamedData(getSource());
        if ( ds == null )
        {
            ds = dataSet;
            taskDataAsSource = true;
            logger.info("No source data referenced, using assigned task data");
            if ( ds.rowCount() == 0 )
            {
                logger.warn("Assigned task data has no rows, performing no data mapping");
            }
        }

        if ( ds.getType() != DataSet.Type.relational )
        {
            throw new ETLException("Reference to a hierarchical data set. DataMappings only support relational data sets.");
        }
        
        src = (RelationalContainer)ds;
        incomingData = src;
        logger.info("SourceData loaded, contains " + src.rowCount() + " rows of data");
        logger.trace("SourceData: " + src);

        RelationalContainer errors = new RelationalContainer(getTaskName() + AbstractTask.ERROR_DATA_NAME);
        DataColumn ecol = new DataColumn(DataTypes.STRING, RelationalContainer.COL_ERROR_MSG);
        DataColumn scol = new DataColumn(DataTypes.STRING, DataMapping.COL_CELL_SOURCES);
        DataColumn dcol = new DataColumn(DataTypes.STRING, DataMapping.COL_CELL_ERROR);
        errors.setColumnDefinitions(src.getColumnDefinitions());
        errors.addColumnDefinition(ecol);
        errors.addColumnDefinition(scol);
        errors.addColumnDefinition(dcol);

        logger.info("Loading TargetData: " + getTarget());
        ds = wrapper.retrieveNamedData(getTarget());
        if ( ds == null )
        {
            if ( getTarget() != null && getTarget().trim().length() > 0 )
            {
                throw new ETLException("Target reference to an undefined task: " + getTarget());
            }
            
            logger.warn("Undefined target data: " + getTarget());
            logger.info("Target data undefined, assign data to mapping task");
            if ( !taskDataAsSource )
            {
                ds = dataSet;
            }
            else
            {
                throw new ETLException("Task data has previously been assigned as source, cannot also be used as destination.");
            }
        }

        if ( ds.getType() != DataSet.Type.relational )
        {
            throw new ETLException("Reference to a hierarchical data set. DataMappings only support relational data sets.");
        }
        
        dest = (RelationalContainer)ds;
        outgoingData = dest;
        logger.info("TargetData loaded, contains " + dest.rowCount() + " rows of data");

        //ArrayList<Integer> successes = new ArrayList();

        logger.info("DataMapping type: " + mtype);
        // If the mapping type is replace, clear out all the destination data first
        if ( mtype == DataMapping.MappingType.replace )
        {
            dest.clearData();
            logger.info("Replacing all destination data with transformed source data.");
        }

        // Add column definitions for remaining columns in the source that aren't being used
        // in any transform.
        if ( copyData )
        {
            ArrayList<String> added = new ArrayList();

            // Only add columns that aren't being used as a source in a transform
            for ( DataColumn col: src.getColumnDefinitions().values() )
            {
                // NOT SURE IF THIS MAKES SENSE AT ALL HERE AS COLUMNS USED IN A SOURCE MAY NOT
                // BE COPIED OVER AS PART OF A COPY DATA ACTION
                //if ( !sourceColumns.contains(col.getColumnName()) )
                //{
                    logger.debug("Adding column: " + col);
                    dest.addColumnDefinition(col);
                    added.add(col.getColumnName());
                //}
            }

            logger.info("Added unused source columns since copyData is set: " + added);
        }

        RelationalContainer wip = new RelationalContainer(incomingData.getDataName());
        wip.setColumnDefinitions(incomingData.getColumnDefinitions());
        wip.addAllRows(incomingData.getMappedData());

        int rowcount = src.rowCount();
        int destRowCount = dest.rowCount();
        List<AbstractTransform> transforms = getTransforms();
        List<Map<DataCell,Integer>> inserts = new ArrayList(); 
        if ( transforms.size() > 0 )
        {
            for ( int i=1; i<=src.rowCount(); i++ )
            {
                data.setCurrentProcessedRow(i);
                System.out.println(getTaskName() + ": Transforming row #" + i + " out of " + rowcount);
                try
                {
                    int count = 0;
                    logger.debug("----------------PROCESSING ROW #" + i + "----------------");
                    HashMap<String,DataCell> map = new HashMap();
                    if ( mtype == DataMapping.MappingType.assign &&
                         i <= dest.rowCount() )
                    {
                        map = new HashMap(dest.getMappedData().get(i-1));
                        logger.debug("Assigning data to existing row #" + i + ": " + map);
                    }
                    else if ( copyData )
                    {
                        // Add all the data to the current row
                        for ( Map.Entry<String,DataCell> entry: src.getMappedData().get(i-1).entrySet() )
                        {
                            DataCell cell = new DataCell(entry.getValue().getColumn(), entry.getValue().getValue());
                            map.put(entry.getKey(), cell);
                        }
                    }

                    // Perform each transform defined in the data mapping for this row
                    for ( AbstractTransform transform: transforms )
                    {
                        try
                        {
                            logger.debug("Performing " + transform.getTransformType() + " transform: " + transform);

                            List<DataCell> sources = extractSourceData(wip, transform, i-1);
                            logger.debug("Source cell(s): " + sources);

                            List<DataColumn> targets = extractTargetColumns(outgoingData, transform);
                            logger.debug("Target column(s): " + targets);

                            List<DataCell> tcells = transform.transform(targets, sources);
                            for ( int j=0; j<tcells.size(); j++ )
                            {
                                DataCell tcell = tcells.get(j);
                                
                                /*// If the result is a list then this indicates multiple rows that need to be added to the data
                                if ( tcell.getValue() instanceof List )
                                {
                                    logger.warn("Found list result. Need to convert to rows: " + tcell);
                                    
                                    Map<DataCell,Integer> imap = new HashMap();
                                    List<String> list = (List)tcell.getValue();
                                    for ( int x=0; x<list.size(); x++ )
                                    {
                                        
                                        if ( x == 0 )
                                        {
                                            logger.debug(sources.get(j));
                                            tcell = new DataCell(sources.get(j).getColumn(), list.get(x));
                                        }
                                        else
                                        {
                                            imap.put(new DataCell(new DataColumn(sources.get(j).getColumn().getDataType(), transform.getTargetColumns().get(j).getColumnName()), list.get(x)), i-1);
                                            inserts.add(imap);
                                        }
                                    }
                                }*/
                                
                                TransformTarget target = transform.getTargetColumns().get(j);
                                if ( !transform.isTemporary() && (targets.get(j) == null || target.isTargetDataTypeOverridden()) )
                                {
                                    logger.debug("No definition for column '" + target.getColumnName() + "' in " +
                                                "target data '" + getTarget() + "'. Adding column to data.");
                                    if ( target.isTargetDataTypeOverridden() )
                                    {
                                        logger.debug("DataType overriden to " + target.getOverriddenDataType());
                                        DataColumn c = dest.addColumnDefinition(target.getOverriddenDataType(), target.getColumnName(), target.getDefaultValue());
                                        tcell.setColumn(c);
                                    }
                                    else
                                    {
                                        dest.addColumnDefinition(tcell.getColumn().getDataType(),
                                                                 target.getColumnName(), target.getDefaultValue());
                                    }
                                }
                                else if ( transform.isTemporary() )
                                {
                                    logger.debug("Transform is temporary, not assigning target column definition(s) to destination");
                                }
                                
                                logger.debug("<------------- OUTPUT: " + target.getColumnName() + "=" + tcell + " -------------->");
                                map.put(target.getColumnName(), tcell);
                                wip.getMappedData().get(i-1).put(target.getColumnName(), tcell);
                            }

                            count++;
                        }
                        catch (TransformIgnoreException e)
                        {
                            logger.info("Transform error defined as ignorable: " + e.getMessage());
                        }
                    }

                    // If the map type is assign, the current values in the destination
                    // data must be retained, otherwise add the new row
                    if ( mtype == DataMapping.MappingType.assign )
                    {
                        dest.setDataRow(map, i-1);
                    }
                    else
                    {
                        dest.addDataRow(map);
                    }

                    //successes.add(count);
                }
                catch (VariableTypeMismatchException e)
                {
                    logger.error("Unable to assign value to incoming source", e);
                    HashMap<String,DataCell> map = new HashMap(src.getMappedData().get(i-1));
                    map.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, e.getMessage()));
                    errors.addDataRow(map);
                    continue;
                }
                catch (IndexException e)
                {
                    logger.error("Failed to index column data", e);
                    HashMap<String,DataCell> map = new HashMap(src.getMappedData().get(i-1));
                    map.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, e.getMessage()));
                    errors.addDataRow(map);
                    continue;
                }
                catch (TransformException e)
                {
                    logger.error("Failed to transform column data", e);
                    HashMap<String,DataCell> map = new HashMap(src.getMappedData().get(i-1));
                    DataCell cell = e.getDataCell();
                    map.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, e.getMessage()));
                    map.put(DataMapping.COL_CELL_ERROR, new DataCell(dcol, cell));
                    
                    if ( cell != null )
                    {
                        map.put(DataMapping.COL_CELL_SOURCES, new DataCell(scol, cell.getColumn().getColumnName()));
                    }                    
                    else
                    {
                        map.put(DataMapping.COL_CELL_SOURCES, new DataCell(scol, null));
                    }

                    errors.addDataRow(map);
                    continue;
                }
            }

            
            /*logger.info("Need to insert data: " + inserts);
            for ( int i=0; i<inserts.size(); i++ )
            {
                Map.Entry<DataCell,Integer> entry = inserts.get(i).entrySet().iterator().next();
                DataCell cell = entry.getKey();
                int row = entry.getValue();
                
                logger.debug("Inserting after row #" + row + " to column " + cell.getColumn().getColumnName() + ": " + cell);
                Map<String,DataCell> rowdata = new HashMap();
                rowdata.putAll(dest.getMappedData().get(row+i));
                rowdata.put(cell.getColumn().getColumnName(), cell);
                dest.insertDataRow(rowdata, row+1+i);
            }*/
                    
            for ( int i=1; i<=dest.rowCount()-destRowCount; i++ )
            {
                logger.debug("Transformed row #" + i + ": " + dest.getMappedData().get(i-1));
                //logger.debug("Row #" + i + " transformed " + successes.get(i-1) + " columns successfully");
            }
        }
        else
        {
            logger.info("Copied " + src.rowCount() + " rows from " + src.getDataName() + " to " + dest.getDataName());
            System.out.println("Copied " + src.rowCount() + " rows from " + src.getDataName() + " to " + dest.getDataName());
            if ( mtype == DataMapping.MappingType.add )
            {
                dest.addColumnDefinitions(src.getColumnDefinitions());
                dest.addAllRows(src.getMappedData());
            }
            else
            {
                dest.copyDataSet(src, true, true);
            }
        }
        
        // Need to record the outgoing count here since the data is archived before being returned
        // to the processor causing the row count to be zeroed and incorrect for statistics reporting.
        outgoingCount = dest.rowCount();
        
        // If the destination task data is defined we have to set the data on the task to
        // ensure that the proper data is archived for later retrieval - Except GLOBAL data
        if ( !ETLJobWrapper.GLOBAL_VAR_DATA_NAME.equals(getTarget()) &&
             getTarget() != null && getTarget().trim().length() > 0 && !wrapper.isArchivingSuppressed())
        {
            AbstractTask dtask = wrapper.retrieveNamedDataTask(getTarget());
            dtask.setData(dest);
            dtask.archiveTransientData(jobnum);
        }

        return errors;
    }

    public static List<DataColumn> extractTargetColumns(RelationalContainer dc, AbstractTransform transform)
    {
        Logger logger = Logger.getLogger(DataMapping.class);
        logger.debug("Outgoing cols: " + dc.getColumnDefinitions());
        ArrayList<DataColumn> targets = new ArrayList();
        if ( transform.supportsMultipleTargets() )
        {
            for ( TransformTarget target: transform.getTargetColumns() )
            {
                DataColumn col = dc.getColumnDefinition(target.getColumnName());
                if ( col == null )
                {
                    col = null;
                }
                else if ( target.isTargetDataTypeOverridden() )
                {
                    col = new DataColumn(target.getOverriddenDataType(), target.getColumnName(), target.getDefaultValue()); 
                }

                targets.add(col);
            }
        }
        else
        {
            TransformTarget target = transform.getTargetColumns().get(0);
            DataColumn col = dc.getColumnDefinition(target.getColumnName());
            if ( col == null )
            {
                col = null;
            }
            else 
            {
                if ( target.isDefaultValueDefined() )
                {
                    col.setDefaultValue(target.getDefaultValue());
                }
                
                if ( target.isTargetDataTypeOverridden() )
                {
                    col.setDataType(target.getOverriddenDataType());
                }
            }
            
            targets.add(col);
        }

        return targets;
    }

    public static List<DataCell> extractSourceData(RelationalContainer src, AbstractTransform transform, int index)
        throws VariableTypeMismatchException
    {
        Logger logger = Logger.getLogger(DataMapping.class);
        ArrayList<DataCell> sources = new ArrayList();
        logger.debug("Transform sources: " + transform.getSourceColumns());
        if ( transform.supportsMultipleSources() )
        {
            for ( TransformSource sc: transform.getSourceColumns() )
            {
                DataCell scell = null;
                if ( sc.getArgumentType() == Argument.Type.column )
                {
                    scell = src.getMappedData().get(index).get(sc.getValue());
                    if ( sc.isSourceColumnForwarded() && scell != null )
                    {
                        logger.debug("Source column forwarding to column: " + scell.getValue());
                        scell = src.getMappedData().get(index).get(scell.getValue());
                    }

                    if ( scell == null )
                    {
                        logger.debug("Unable to transform since source data is missing column: " + sc);
                        String colname = sc.getColumn().getColumnName();
                        DataColumn col = null;
                        if ( src.getColumnDefinition(colname) != null )
                        {
                            col = src.getColumnDefinition(colname);
                        }
                        else
                        {
                            DataTypes type = DataTypes.STRING;
                            if ( sc.getTargetDataType() != null )
                            {
                                type = sc.getTargetDataType();
                            }
                            col = new DataColumn(type, sc.getColumn().getColumnName());
                            logger.debug("Undefined column referenced, utilizing default type: " + col);
                        }

                        scell = new DataCell(col, null);
                    }
                }
                else
                {
                    logger.debug("Assigning constant with data type: " + sc.getTargetDataType());
                    scell = new DataCell(new DataColumn(sc.getTargetDataType(), null), getSourceValue(src, sc.getValue(), index+1));
                }
                
                sources.add(scell);
            }
        }
        else
        {
            DataCell scell = null;
            TransformSource sc = transform.getSourceColumns().get(0);
            if ( sc.getArgumentType() == Argument.Type.column )
            {
                scell = src.getMappedData().get(index).get(sc.getValue());
                if ( sc.isSourceColumnForwarded() && scell != null )
                {
                    logger.debug("Source column forwarding to column: " + scell.getValue());
                    scell = src.getMappedData().get(index).get(scell.getValue());
                }

                if ( scell == null )
                {
                    logger.debug("Unable to transform since source data is missing column: " + sc);
                    String colname = sc.getColumn().getColumnName();
                    DataColumn col = null;
                    if ( src.getColumnDefinition(colname) != null )
                    {
                        col = src.getColumnDefinition(colname);
                    }
                    else
                    {
                        DataTypes type = DataTypes.STRING;
                        if ( sc.getTargetDataType() != null )
                        {
                            type = sc.getTargetDataType();
                        }
                        col = new DataColumn(type, String.valueOf(sc.getValue()));
                        logger.debug("Undefined column referenced, utilizing default type: " + col);
                    }

                    scell = new DataCell(col, null);
                }
            }
            else
            {
                logger.debug("Assigning constant with data type: " + sc.getTargetDataType());
                scell = new DataCell(new DataColumn(sc.getTargetDataType(), null), getSourceValue(src, sc.getValue(), index+1));
            }

            sources.add(scell);
        }

        return sources;
    }
    
    private static Object getSourceValue(RelationalContainer data, Object val, int rownum)
        throws VariableTypeMismatchException
    {
        Logger logger = Logger.getLogger(DataMapping.class);
        Object value = val;
        switch (getHiddenDataType(val))
        {
            case rownum:
                logger.debug("Source refers to hidden column: rownum");
                value = rownum;
                break;
            case rowcount:
                logger.debug("Source refers to hidden column: rowcount");
                value = data.rowCount();
                break;
            case colcount:
                logger.debug("Source refers to hidden column: colcount");
                value = data.getColumnDefinitions().size();
                break;
        }

        return value;
    }

    public String toString()
    {
        StringBuffer out = new StringBuffer();
        out.append("{");
        out.append("TaskName=" + name);
        out.append(", NextTask=" + next);
        out.append(", Source=" + source);
        out.append(", Target=" + target);
        out.append(", Transforms=" + transforms);
        out.append("}");
        return out.toString();
    }
}
