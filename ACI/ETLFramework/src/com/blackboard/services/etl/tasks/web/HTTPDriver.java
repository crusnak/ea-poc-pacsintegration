/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.tasks.web;

import com.blackboard.services.etl.data.Argument;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.data.WebArgument;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.WebAPITaskComplexType;
import com.blackboard.services.etl.jaxb.XpathComplexType;
import com.blackboard.services.etl.tasks.WebAPIDispatcher;
import com.blackboard.services.utils.ComparableHeader;
import com.blackboard.services.utils.JavaUtils;
import com.blackboard.services.utils.XMLUtils;
import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;

/**
 *
 * @author crusnak
 */
public class HTTPDriver extends AbstractDriver
{
    private CloseableHttpClient connection = null;
    
    protected HTTPDriver(WebAPIDispatcher task)
    {
        super(task);
    }
    
    protected HTTPDriver(WebAPITaskComplexType ct, WebAPIDispatcher task)
        throws ETLException
    {
        super(ct, task);
        
        try
        {
            type = DriverType.HTTP;
            setJoinType(ct.getHttp().getJoinType());
            setAfterExecution(ct.getHttp().getAfterExecute());
            setConnectionInfo(ct.getConnectionInfo());
            setRequest(ct.getHttp().getRequest());
            setResponse(ct.getHttp().getResponse());
        }
        catch (MalformedURLException e)
        {
            throw new ETLException("Bad URL", e);
        }
    }
    
    
    @Override
    public void setRequest(WebAPITaskComplexType.Http.Request request)
        throws ETLException, MalformedURLException
    {
        if ( request != null )
        {
            setAPIURI(request.getApiURI());
            setRequestMethod(request.getMethod());
            setHeaders(request.getHeaders());
            setParams(request.getParams());
        }
        else
        {
            throw new ETLException("Request element in web-api declaration is required");
        }
    }

    @Override
    public void setResponse(WebAPITaskComplexType.Http.Response response)
        throws ETLException
    {
        super.setResponse(response);
    }
    
    @Override
    protected void setRequestMethod(String method)
        throws IllegalArgumentException
    {
        super.setRequestMethod(method);
        switch (requestMethod)
        {
            case DELETE:
            case OPTIONS:
            case PATCH:
            case PUT:
                EnumSet<RequestMethod> list = EnumSet.of(RequestMethod.GET, RequestMethod.POST);
                throw new IllegalArgumentException("HTTP requests do not support" + method + ". Supported methods are: " + JavaUtils.enumToList(list));
        }
    }
    
    protected List<Map<String,DataCell>> get(Map<String,DataCell> rowdata, boolean addResponse)
        throws ETLException
    {
        CloseableHttpResponse response = null;
        List<Map<String,DataCell>> newdata = new ArrayList();
        
        try
        {
            logger.debug("Utilizing rowdata=" + rowdata);
            connection = HttpClients.createDefault();
            HttpGet get = new HttpGet(getURL(rowdata).toURI()); 

            // Iterate through all http request headers
            for ( int j=1; j<=headers.size(); j++ )
            {
                Argument arg = headers.get(j-1);
                String argname = "";
                Object val = null;
                if ( arg.getArgumentType() == Argument.Type.column )
                {
                    argname = String.valueOf(arg.getValue());
                    DataCell cell = rowdata.get(argname);
                    if ( cell != null )
                    {
                        val = cell.getValue();
                    }
                    else if ( task.getData().getColumnDefinition(argname) != null )
                    {
                        val = null;
                    }
                    else
                    {
                        throw new ETLException("Undefined column " + arg + ", unable to set http request header with correct data.", arg);
                    }
                }
                else
                {
                    argname = arg.getColumn().getColumnName();
                    val = arg.getValue();
                }

                logger.debug("Header #" + j + " '" + argname + "'=" + val);
                httpHeaders.add(new ComparableHeader(argname, String.valueOf(val)));
            }

            logger.info("HTTP headers: " + httpHeaders);

            // Execute http request
            // TODO: convert httpParameters to URL encoded query string
            get.setHeaders(httpHeaders.toArray(new Header[0]));
            response = connection.execute(get);
            logger.debug("HTTP response: " + response.getStatusLine());
            parseStatusLine(response);
            
            newdata.addAll(parseResponse(response, rowdata, addResponse));

            EntityUtils.consume(response.getEntity());
        }
        catch (URISyntaxException e)
        {
            throw new ETLException("Unable to parse URL to URI statement: " + getURL(rowdata), e);
        }
        catch (UnsupportedEncodingException e)
        {
            throw new ETLException("Unable to encode http entity since encoding not supported", e);
        }
        catch (IOException e)
        {
            throw new ETLException("Error communicating with http target: " + getURL(rowdata), e);
        }
        finally
        {
            try
            {
                if ( response != null )
                {
                    response.close();
                }
            }
            catch (IOException e)
            {
                logger.warn("Unable to close http response", e);
            }
        }
        
        return newdata;
    }

    protected List<Map<String,DataCell>> post(Map<String,DataCell> rowdata, boolean addResponse)
        throws ETLException
    {
        CloseableHttpResponse response = null;
        List<Map<String,DataCell>> newdata = new ArrayList();
        
        try
        {
            logger.debug("Utilizing rowdata=" + rowdata);
            // Check if strict SSL checking used
            if ( webInfo.isStrictSSLCheckUsed() )
            {
                connection = HttpClients.createDefault();
            }
            else
            {
                connection = HttpClients.custom().setSSLContext(new SSLContextBuilder().loadTrustMaterial(null, TrustSelfSignedStrategy.INSTANCE).build())
                                                 .setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE).build();
            }
            
            HttpPost post = new HttpPost(getURL(rowdata).toURI()); 

            // Iterate through all http request headers
            for ( int j=1; j<=headers.size(); j++ )
            {
                Argument arg = headers.get(j-1);
                String argname = "";
                Object val = null;
                if ( arg.getArgumentType() == Argument.Type.column )
                {
                    argname = String.valueOf(arg.getValue());
                    DataCell cell = rowdata.get(argname);
                    if ( cell != null )
                    {
                        val = cell.getValue();
                    }
                    else if ( task.getData().getColumnDefinition(argname) != null )
                    {
                        val = null;
                    }
                    else
                    {
                        throw new ETLException("Undefined column " + arg + ", unable to set http request header with correct data.", arg);
                    }
                }
                else
                {
                    argname = arg.getColumn().getColumnName();
                    val = arg.getValue();
                }

                logger.debug("Header #" + j + " '" + argname + "'=" + val);
                httpHeaders.add(new ComparableHeader(argname, String.valueOf(val)));
            }

            logger.info("HTTP headers: " + httpHeaders);

            // Iterate through all http request parameters
            for ( int j=1; j<=params.size(); j++ )
            {
                WebArgument arg = params.get(j-1);
                String argname = "";
                Object val = null;
                if ( arg.getArgumentType() == Argument.Type.column )
                {
                    argname = String.valueOf(arg.getValue());
                    DataCell cell = rowdata.get(argname);
                    if ( cell != null )
                    {
                        val = cell.getValue();
                    }
                    else if ( task.getData().getColumnDefinition(argname) != null )
                    {
                        val = null;
                    }
                    else
                    {
                        throw new ETLException("Undefined column " + argname + ", unable to set http request header with correct data.", arg);
                    }

                    argname = arg.getArgumentName();
                }
                else if ( arg.getTargetDataType() == DataTypes.JSON ||
                          arg.getTargetDataType() == DataTypes.XML )
                {   
                    logger.debug("Handling variable replacement for xml/json datatypes: " + arg);
                    argname = arg.getArgumentName();
                    val = replaceHierarchicalVariable(arg, rowdata);
                    if ( arg.getTargetDataType() == DataTypes.JSON )
                    {
                        XMLUtils.parseJSON(String.valueOf(val));
                    }
                }
                else
                {
                    argname = arg.getArgumentName();
                    val = arg.getValue();
                }

                logger.debug("Param #" + j + " '" + argname + "'=" + val);
                httpParameters.add(new BasicNameValuePair(argname, String.valueOf(val)));
            }

            logger.debug("HTTP parameters: " + httpParameters);

            // Execute http request
            post.setHeaders(httpHeaders.toArray(new Header[0]));
            post.setEntity(new UrlEncodedFormEntity(httpParameters));
            response = connection.execute(post);
            logger.debug("HTTP response: " + response.getStatusLine());
            parseStatusLine(response);
            newdata.addAll(parseResponse(response, rowdata, addResponse));

            EntityUtils.consume(response.getEntity());
        }
        catch (URISyntaxException e)
        {
            throw new ETLException("Unable to parse URL to URI statement: " + getURL(rowdata), e);
        }
        catch (UnsupportedEncodingException e)
        {
            throw new ETLException("Unable to encode http entity since encoding not supported", e);
        }
        catch (ParserConfigurationException e)
        {
            throw new ETLException("Failed to parse json or xml response into DOM object", e);
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new ETLException("Unable to disable strict hostname checking: " + e);
        }
        catch (KeyManagementException e)
        {
            throw new ETLException("Unable to access keystore: " + e);
        }
        catch (KeyStoreException e)
        {
            throw new ETLException("Unable to access keystore: " + e);
        }
        catch (IOException e)
        {
            throw new ETLException("Error communicating with http target: " + getURL(rowdata), e);
        }
        finally
        {
            try
            {
                if ( response != null )
                {
                    response.close();
                }
            }
            catch (IOException e)
            {
                logger.warn("Unable to close http response", e);
            }
        }
        
        return newdata;
    }
    
    protected List<Map<String,DataCell>> delete(Map<String,DataCell> data, boolean addResponse)
        throws ETLException
    {
        throw new UnsupportedOperationException("HTTP does not support method DELETE");
    }
    
    protected List<Map<String,DataCell>> options(Map<String,DataCell> data, boolean addResponse)
        throws ETLException
    {
        throw new UnsupportedOperationException("HTTP does not support method OPTIONS");
    }

    protected List<Map<String,DataCell>> patch(Map<String,DataCell> data, boolean addResponse)
        throws ETLException
    {
        throw new UnsupportedOperationException("HTTP does not support method PATCH");
    }

    protected List<Map<String,DataCell>> put(Map<String,DataCell> data, boolean addResponse)
        throws ETLException
    {
        throw new UnsupportedOperationException("HTTP does not support method PUT");
    }

    @Override
    public void closeConnection()
        throws IOException
    {
        if ( connection != null )
        {
            connection.close();
        }
    }
    
    public static void main(String[] args)
    {
        try
        {
            WebAPIDriver http = AbstractDriver.getBaseDriver(DriverType.HTTP, null);
            http.setRequestMethod(RequestMethod.GET);
            //http.setURL("https://bbtsservices.transactsp.net/bbts/api/management/v1/customers/1");
            http.setURL("http://localhost:8080/tibet/CustomerRequest");
            //http.addHeader(new ComparableHeader("TransactSP-Institution-Route", "InstitutionRouteId " + "C5ED4E45-D374-4A85-AC88-323143F85FA3"));
            http.execute(new HashMap(), false);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
