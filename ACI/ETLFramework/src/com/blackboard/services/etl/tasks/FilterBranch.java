/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.tasks;

import com.blackboard.services.etl.data.Argument;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.exception.FilterException;
import com.blackboard.services.etl.jaxb.BooleanComplexType;
import com.blackboard.services.etl.jaxb.CorrelateDataComplexType;
import com.blackboard.services.etl.jaxb.FilterTaskComplexType;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.blackboard.services.utils.BeanScript;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import javax.xml.bind.JAXBElement;
import org.apache.log4j.Logger;

/**
 *
 * @author crusnak
 */
public class FilterBranch
{
    public static final String TYPE_TEST =      "test";
    public static final String TYPE_DEFAULT =   "default";
    
    private AbstractTask task = null;
    private ArrayList<BooleanTest> tests = new ArrayList();
    private boolean defaultTest = false;
    private String nextTaskName = null;
    private Logger logger = null;
    private Integer priority = null;
    private boolean strict = true;

    public FilterBranch(FilterTaskComplexType.Filter filter, AbstractTask t, boolean useStrict)
        throws ETLException
    {
        logger = Logger.getLogger(this.getClass());
        task = t;
        strict = useStrict;
        if ( filter != null )
        {
            setDefaultTest(TYPE_DEFAULT.equals(filter.getType()));
            setPriority(filter.getPriority());

            // Set next task name if exists
            if ( filter.getNextTask() != null )
            {
                setNextTaskName(filter.getNextTask().getName());
            }

            // Create test list
            for ( JAXBElement elem: filter.getIfAndAndOrOr() )
            {
                if ( elem.getDeclaredType() == BooleanComplexType.class )
                {
                    BooleanComplexType ct = (BooleanComplexType)elem.getValue();
                    tests.add(new BooleanTest(ct, t));
                }
                else
                {
                    tests.add(new BooleanTest(elem.getName().getLocalPart()));
                }
            }
        }
    }
    
    public FilterBranch(CorrelateDataComplexType.Select filter, AbstractTask t)
        throws ETLException
    {
        logger = Logger.getLogger(this.getClass());
        task = t;
        if ( filter != null )
        {
            setDefaultTest(false);

            // Create test list
            for ( JAXBElement elem: filter.getWhereAndAndOrOr() )
            {
                if ( elem.getDeclaredType() == BooleanComplexType.class )
                {
                    BooleanComplexType ct = (BooleanComplexType)elem.getValue();
                    tests.add(new BooleanTest(ct, t));
                }
                else
                {
                    tests.add(new BooleanTest(elem.getName().getLocalPart()));
                }
            }
        }
    }
    
    public FilterBranch(CorrelateDataComplexType.Select.AssignColumnData assign, AbstractTask t)
        throws ETLException
    {
        logger = Logger.getLogger(this.getClass());
        task = t;
        if ( assign != null )
        {
            setDefaultTest(false);

            // Create test list
            for ( JAXBElement elem: assign.getWhereAndAndOrOr() )
            {
                if ( elem.getDeclaredType() == BooleanComplexType.class )
                {
                    BooleanComplexType ct = (BooleanComplexType)elem.getValue();
                    tests.add(new BooleanTest(ct, t));
                }
                else
                {
                    tests.add(new BooleanTest(elem.getName().getLocalPart()));
                }
            }
        }
    }

    public boolean isDefaultTest()
    {
        return defaultTest;
    }

    private void setDefaultTest(boolean def)
    {
        defaultTest = def;
    }
    
    public Integer getPriority()
    {
        return priority;
    }
    
    private void setPriority(Integer p)
    {
        priority = p;
    }

    public String getNextTaskName()
    {
        return nextTaskName;
    }

    private void setNextTaskName(String name)
    {
        nextTaskName = name;
    }

    public boolean isValidBranchDataStored()
    {
        return ( nextTaskName != null && nextTaskName.trim().length() > 0 );
    }

    public List<BooleanTest> getTests()
    {
        return tests;
    }

    public boolean testData(Map<String,DataCell> data, AbstractTask task)
        throws ETLException
    {
        boolean match = false;
        BooleanTest.TestType tt = BooleanTest.TestType.OR;
        if ( !isDefaultTest() )
        {
            for ( BooleanTest test: tests )
            {
                switch (test.getTestType())
                {
                    case BOOLEAN:
                        boolean result = false;
                        Argument op1 = test.getOperand1();
                        Argument op2 = test.getOperand2();
                        Object o1 = null;
                        Object o2 = null;

                        if ( op1.getArgumentType() == Argument.Type.column )
                        {
                            if ( data.containsKey(String.valueOf(op1.getValue())) )
                            {
                                o1 = data.get(String.valueOf(op1.getValue())).getValue();
                            }
                            else if ( !strict )
                            {
                                logger.debug("Column not found in data set: " + op1.getValue() + ". Value is assigned NULL.");
                                o1 = null;
                            }
                            else
                            {
                                throw new FilterException("Column not found in data set: " + op1.getValue());
                            }
                        }
                        else
                        {
                            o1 = op1.getValue();
                        }

                        if ( op2.getArgumentType() == Argument.Type.column )
                        {
                            if ( data.containsKey(String.valueOf(op2.getValue())) )
                            {
                                o2 = data.get(String.valueOf(op2.getValue())).getValue();
                            }
                            else
                            {
                                throw new FilterException("Column not found in data set: " + op2.getValue());
                            }
                        }
                        else
                        {
                            o2 = op2.getValue();
                        }

                        logger.debug("Testing: " + o1 + (o1 != null?"(" + o1.getClass().getName() + ")":"") + " " +
                                     test.getBooleanType() + " " + o2 + (o2 != null?"(" + o2.getClass().getName() + ")":""));

                        result = testOperands(test.getBooleanType(), o1, o2, test.getDelimiter1(), test.getDelimiter2());
                        logger.debug("TestResult: " + result);

                        String msg = "Current branch result: " + match + " " + tt + " " + result + "=";
                        switch (tt)
                        {
                            case AND:
                                match &= result;
                                break;
                            case OR:
                                match |= result;
                                break;
                        }
                        logger.debug(msg + match);
                        break;
                    default:
                        tt = test.getTestType();
                        break;
                }
            }
        }
        else
        {
            match = true;
        }
        
        return match;
    }

    private boolean testOperands(BooleanTest.BooleanType bt, Object o1, Object o2, String delimiter1, String delimiter2)
    {
        boolean result = false;
        switch (bt)
        {
            case eq:
                o1 = (o1 instanceof String && ((String)o1).trim().length() == 0 ? null:o1);
                o2 = (o2 instanceof String && ((String)o2).trim().length() == 0 ? null:o2);
                if ( o1 != null && o2 != null )
                {
                    result = o1.equals(o2);
                }
                else if ( o1 == null && o2 == null )
                {
                    result = true;
                }
                break;
            case ne:
                o1 = (o1 instanceof String && ((String)o1).trim().length() == 0 ? null:o1);
                o2 = (o2 instanceof String && ((String)o2).trim().length() == 0 ? null:o2);
                if ( o1 != null && o2 != null )
                {
                    result = !o1.equals(o2);
                }
                else if ( (o1 == null && o2 != null) || (o1 != null && o2 == null) )
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
                break;
            case gt:
                try
                {
                    if ( o1 instanceof Date )
                    {
                        o1 = ((Date)o1).getTime();
                    }
                    if ( o2 instanceof Date )
                    {
                        o2 = ((Date)o2).getTime();
                    }
                    double d1 = Double.parseDouble(String.valueOf(o1));
                    double d2 = Double.parseDouble(String.valueOf(o2));
                    result = d1 > d2;
                }
                catch (NumberFormatException e)
                {
                    logger.debug("At least one operand for gt branch types is not a number, branch FALSE");
                }
                break;
            case gte:
                try
                {
                    if ( o1 instanceof Date )
                    {
                        o1 = ((Date)o1).getTime();
                    }
                    if ( o2 instanceof Date )
                    {
                        o2 = ((Date)o2).getTime();
                    }
                    double d1 = Double.parseDouble(String.valueOf(o1));
                    double d2 = Double.parseDouble(String.valueOf(o2));
                    result = d1 >= d2;
                }
                catch (NumberFormatException e)
                {
                    logger.debug("At least one operand for gte branch types is not a number, branch FALSE");
                }
                break;
            case lt:
                try
                {
                    if ( o1 instanceof Date )
                    {
                        o1 = ((Date)o1).getTime();
                    }
                    if ( o2 instanceof Date )
                    {
                        o2 = ((Date)o2).getTime();
                    }
                    double d1 = Double.parseDouble(String.valueOf(o1));
                    double d2 = Double.parseDouble(String.valueOf(o2));
                    result = d1 < d2;
                }
                catch (NumberFormatException e)
                {
                    logger.debug("At least one operand for lt branch types is not a number, branch FALSE");
                }
                break;
            case lte:
                try
                {
                    if ( o1 instanceof Date )
                    {
                        o1 = ((Date)o1).getTime();
                    }
                    if ( o2 instanceof Date )
                    {
                        o2 = ((Date)o2).getTime();
                    }
                    double d1 = Double.parseDouble(String.valueOf(o1));
                    double d2 = Double.parseDouble(String.valueOf(o2));
                    result = d1 <= d2;
                }
                catch (NumberFormatException e)
                {
                    logger.debug("At least one operand for lte branch types is not a number, branch FALSE");
                }
                break;
            case contains:
                if ( o1 != null )
                {
                    result = String.valueOf(o1).contains(String.valueOf(o2));
                }
                break;
            case startswith:
                if ( o1 != null )
                {
                    result = String.valueOf(o1).startsWith(String.valueOf(o2));
                }
                break;
            case endswith:
                result = String.valueOf(o1).endsWith(String.valueOf(o2));
                break;
            case regex:
                if ( o2 != null )
                {
                    try
                    {
                        Pattern p = Pattern.compile(String.valueOf(o2));
                        logger.debug("Regex pattern=" + p);
                        result = p.matcher(String.valueOf(o1)).matches();
                    }
                    catch (PatternSyntaxException e)
                    {
                        logger.debug("Invalid regex pattern '" + o2 + "', branch FALSE: ", e);
                    }
                    
                }
                break;
            case inlist:
                if ( delimiter1 == null || delimiter1.trim().length() == 0 )
                {
                    throw new IllegalArgumentException("Cannot perform inlist test with undefined operand1Delimiter attribute");
                }
                
                if ( delimiter2 == null || delimiter2.trim().length() == 0 )
                {
                    throw new IllegalArgumentException("Cannot perform inlist test with undefined operand2Delimiter attribute");
                }

                if ( o2 != null )
                {
                    BeanScript bs = new BeanScript(String.valueOf(o2), '!');
                    boolean not = bs.isValidScript();
                    if ( not )
                    {
                        o2 = bs.getAccessorText();
                    }
                    
                    logger.debug("delimiter1=" + delimiter1);
                    ArrayList<String> chklist = new ArrayList(Arrays.asList(String.valueOf((o1 != null?o1:"")).split("\\" + delimiter1)));
                    logger.debug("INLIST source=" + chklist);
                    
                    logger.debug("delimiter2=" + delimiter2);
                    ArrayList<String> inlist = new ArrayList(Arrays.asList(String.valueOf(o2).split("\\" + delimiter2)));
                    logger.debug("INLIST compare=" + inlist);
                    for ( String s: inlist )
                    {
                        result |= chklist.contains(s);
                    }
                    
                    if ( not )
                    {
                        result = !result;
                    }
                }
                else if ( o1 == null && o2 == null )
                {
                    result = true;
                }
                break;
            case notinlist:
                if ( delimiter1 == null || delimiter1.trim().length() == 0 )
                {
                    throw new IllegalArgumentException("Cannot perform notinlist test with undefined operand2Delimiter attribute");
                }
                
                if ( delimiter2 == null || delimiter2.trim().length() == 0 )
                {
                    throw new IllegalArgumentException("Cannot perform notinlist test with undefined operand2Delimiter attribute");
                }

                if ( o2 != null )
                {
                    BeanScript bs = new BeanScript(String.valueOf(o2), '!');
                    boolean not = bs.isValidScript();
                    if ( not )
                    {
                        o2 = bs.getAccessorText();
                    }
                    
                    ArrayList<String> chklist = new ArrayList(Arrays.asList(String.valueOf((o1 != null?o1:"")).split("\\" + delimiter1)));
                    logger.debug("INLIST source=" + chklist);
                    
                    ArrayList<String> inlist = new ArrayList(Arrays.asList(String.valueOf(o2).split("\\" + delimiter2)));
                    logger.debug("INLIST compare=" + inlist);
                    for ( String s: inlist )
                    {
                        result &= !chklist.contains(s);
                    }
                    
                    if ( !not )
                    {
                        result = !result;
                    }
                }
                else if ( o1 == null && o2 == null )
                {
                    result = true;
                }
                break;
        }

        return result;
    }

    public String toString()
    {
        StringBuffer out = new StringBuffer();
        out.append("{");
        out.append("Tests=" + tests);
        out.append(", DefaultTest=" + defaultTest);
        out.append(", NextTask=" + nextTaskName);
        out.append(", Priority=" + priority);
        out.append("}");
        return out.toString();
    }
}
