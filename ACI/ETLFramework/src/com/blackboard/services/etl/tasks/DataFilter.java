/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.tasks;

import com.blackboard.services.etl.JobLoader;
import com.blackboard.services.etl.data.Argument;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.jaxb.BooleanComplexType;
import com.blackboard.services.etl.jaxb.BooleanOperators;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.EtlTaskComplexType;
import com.blackboard.services.etl.jaxb.FilterTaskComplexType;
import com.blackboard.services.etl.jaxb.ObjectFactory;
import com.blackboard.services.etl.jaxb.SwitchTaskComplexType;
import com.blackboard.services.etl.process.AbstractETLProcessor;
import com.blackboard.services.etl.process.CommandLineProcessor;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBElement;

/**
 *
 * @author crusnak
 */
public class DataFilter extends AbstractTask
{
    public static enum ProcessType { batch, iterative };
    public static DataColumn TARGET_COL = new DataColumn(DataTypes.STRING, "TARGET");

    private FilterTaskComplexType base = null;
    private RelationalContainer data = null;
    private ArrayList<FilterTaskComplexType.Filter> filters = new ArrayList();
    private HashMap<Integer,String> priorityMap = new HashMap();
    private ProcessType ptype = ProcessType.batch;
    private boolean strict = true;

    public DataFilter(EtlTaskComplexType et, FilterTaskComplexType xml, ETLJobWrapper w)
        throws ETLException
    {
        super(et, w);
        setData(dataSet);

        base = xml;
        setFilter(xml);
        setProcessType(xml.getProcessType());
        setStrict(xml.getStrict());
    }
    
    public DataFilter(EtlTaskComplexType et, SwitchTaskComplexType xml, ETLJobWrapper w)
        throws ETLException
    {
        super(et, w);
        setData(dataSet);

        if ( xml != null )
        {
            ObjectFactory factory = new ObjectFactory();
            FilterTaskComplexType ct = factory.createFilterTaskComplexType();
            ct.setProcessType(xml.getProcessType());
            
            String columnName = xml.getColumn();
            List<SwitchTaskComplexType.SwitchCase> switches = xml.getSwitchCase();
            for ( SwitchTaskComplexType.SwitchCase sw: switches )
            {
                FilterTaskComplexType.Filter filter = factory.createFilterTaskComplexTypeFilter();
                FilterTaskComplexType.Filter.NextTask nextTask = factory.createFilterTaskComplexTypeFilterNextTask();
                BooleanComplexType bool = factory.createBooleanComplexType();
                BooleanComplexType.Operand1 op1 = factory.createBooleanComplexTypeOperand1();
                BooleanComplexType.Operand2 op2 = factory.createBooleanComplexTypeOperand2();

                op1.setType(String.valueOf(Argument.Type.column));
                op1.setValue(columnName);
                op2.setDataType(sw.getValue().getDataType());
                op2.setType(sw.getValue().getType());
                op2.setValue(sw.getValue().getValue());
                
                bool.setType(BooleanOperators.EQ);
                bool.setOperand1(op1);
                bool.setOperand2(op2);

                nextTask.setName(sw.getNextTask().getName());
                
                JAXBElement elem = factory.createFilterTaskComplexTypeFilterIf(bool);
                filter.setType(FilterBranch.TYPE_TEST);
                filter.getIfAndAndOrOr().add(elem);
                filter.setNextTask(nextTask);
                ct.getFilter().add(filter);
            }
            
            // Add a default filter if defined
            if ( xml.getDefault() != null )
            {
                FilterTaskComplexType.Filter filter = factory.createFilterTaskComplexTypeFilter();
                FilterTaskComplexType.Filter.NextTask nextTask = factory.createFilterTaskComplexTypeFilterNextTask();
                nextTask.setName(xml.getDefault().getNextTask().getName());
                
                filter.setType(FilterBranch.TYPE_DEFAULT);
                filter.setNextTask(nextTask);
                ct.getFilter().add(filter);
            }

            logger.debug("Converting SwitchTaskComplexType to FilterTaskComplexType: " + JobLoader.convertXMLToString(ct));
            
            base = ct;
            setFilter(ct);
            setProcessType(ct.getProcessType());
        }
    }

    public FilterTaskComplexType getBaseObject()
    {
        return base;
    }

    public ClassType getClassType()
    {
        return ClassType.DataFilter;
    }
    
    public ProcessType getProcessType()
    {
        return ptype;
    }

    private void setProcessType(String pt)
    {
        ptype = ProcessType.valueOf(pt);
    }
    
    public boolean isStrict()
    {
        return strict;
    }
    
    private void setStrict(String s)
    {
        strict = Boolean.valueOf(s);
    }
    
    @Override
    public RelationalContainer getData()
        throws ETLException
    {
        setData(super.getData());
        return data;
    }

    @Override
    public void setData(DataSet d)
    {
        super.setData(d);
        if ( d.getType() != DataSet.Type.relational )
        {
            throw new UnsupportedOperationException("DataSet assigned to " + this.getClass().getSimpleName() + " must be relational");
        }
        data = (RelationalContainer)d;
    }

    public boolean isIterative()
    {
        return ProcessType.iterative == ptype;
    }

    public Map<String,RelationalContainer> filterData(AbstractETLProcessor processor)
        throws ETLException
    {
        LinkedHashMap<String,RelationalContainer> filterMap = new LinkedHashMap();
        RelationalContainer errors = new RelationalContainer(data.getDataName() + ERROR_DATA_NAME);
        DataColumn ecol = new DataColumn(DataTypes.STRING, RelationalContainer.COL_ERROR_MSG);
        errors.setColumnDefinitions(data.getColumnDefinitions());
        errors.addColumnDefinition(ecol);
        filterMap.put(errors.getDataName(), errors);

        ArrayList<FilterBranch> branches = new ArrayList();
        for ( FilterTaskComplexType.Filter filter: filters )
        {
            FilterBranch f = new FilterBranch(filter, this, strict);
            branches.add(f);
            if ( f.getPriority() == null )
            {
                priorityMap.put(branches.size(), f.getNextTaskName());
            }
            else if ( priorityMap.containsKey(f.getPriority()) )
            {
                String old = priorityMap.get(f.getPriority());
                priorityMap.put(f.getPriority(), f.getNextTaskName());
                priorityMap.put(branches.size(), old);
            }
            else
            {
                priorityMap.put(f.getPriority(), f.getNextTaskName());
            }
        }
        
        // Prepare mapped list data
        for ( FilterBranch branch: branches )
        {
            if ( branch.isValidBranchDataStored() )
            {
                filterMap.put(branch.getNextTaskName(), new RelationalContainer(branch.getNextTaskName()));
            }
        }

        int count = data.rowCount();
        Iterator<Map<String,DataCell>> it = data.getMappedData().iterator();
        for ( int i=1; it.hasNext(); i++ )
        {
            Map<String,DataCell> row = it.next();
            try
            {
                data.setCurrentProcessedRow(i);
                System.out.println(getTaskName() + ": Comparing row #" + i + " out of " + count);
                logger.debug("--------------------------------------------------------------------");
                logger.debug("Testing data row #" + i + ": " + row);
                for ( FilterBranch branch: branches )
                {
                    logger.debug("FilterBranch: " + branch);
                    boolean match = branch.testData(row, this);
                    logger.debug("BranchResult: " + match);
                    if ( match )
                    {
                        if ( branch.isValidBranchDataStored() )
                        {
                            logger.debug("Row filtered to: " + branch.getNextTaskName());
                            RelationalContainer dc = filterMap.get(branch.getNextTaskName());

                            // If columns are empty and the source data for the filter is not the GLOBAL
                            // data add the column definitions of the filter data.
                            if ( dc.getColumnDefinitions().size() == 0 && !ETLJobWrapper.GLOBAL_VAR_DATA_NAME.equals(xmlTask.getSourceData()) )
                            {
                                logger.debug("Setting undefined column definitions in filtered task to source data column definitions");
                                dc.setColumnDefinitions(data.getColumnDefinitions());
                            }
                            
                            switch (ptype)
                            {
                                // If batch, store all rows in the appropriate container and then allow the 
                                // processor to handle each filtered task separately in batch
                                case batch:
                                    logger.debug("Filtered Data task is BATCH");
                                    dc.addDataRow(row);
                                    row.put(TARGET_COL.getColumnName(), new DataCell(TARGET_COL, branch.getNextTaskName()));
                                    logger.debug(dc.getDataName() + " column defs: " + dc.getColumnDefinitions());
                                    break;
                                    
                                // If iterative, immediately process the target task for the given row, allowing
                                // the processor to build up the container data as it processes each row 
                                // individually
                                case iterative:
                                    logger.debug("Filtered Data task is ITERATIVE");
                                    logger.debug("Process row: " + row);
                                    dc.clearData();
                                    dc.addDataRow(row);
                                    logger.debug("Local container: " + dc);
                                    row.put(TARGET_COL.getColumnName(), new DataCell(TARGET_COL, branch.getNextTaskName()));
                                    logger.debug(dc.getDataName() + " column defs: " + dc.getColumnDefinitions());
                                    
                                    AbstractTask nextTask = wrapper.retrieveNamedDataTask(branch.getNextTaskName());
                                    nextTask.loadDataAndUnlink();
                                    nextTask.setData(new RelationalContainer(nextTask.getData().getDataName(), dc));
                                    logger.debug("Assigned single row data to target: " + nextTask.getData());
                                    wrapper.getDataArchiver().archiveTaskData(processor.getJobNumber(), nextTask); 
                                    
                                    logger.debug("Current task data: " + wrapper.retrieveNamedData(branch.getNextTaskName()));
                                    processor.processIterativeFilteredDataTask(branch.getNextTaskName());
                                    break;
                            }
                        }
                        break;
                    }
                }
            }
            catch (ETLException e)
            {
                logger.warn("Error filtering row #" + i + ": " + e.getMessage(), e);
                it.remove();
                row.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, "Error filtering row #" + i + ": " + e.getMessage()));
                errors.addDataRow(row);
            }
        }

        return filterMap;
    }

    public Map<Integer,String> getPriorityMap()
    {
        return priorityMap;
    }
    
    public Integer lookupPriority(String task)
    {
        Integer p = null;
        for ( Map.Entry<Integer,String> entry: priorityMap.entrySet() )
        {
            Integer i = entry.getKey();
            String nt = entry.getValue();
            if ( nt != null && nt.equals(task) )
            {
                p = i;
            }
        }
        
        return p;
    }
    
    private void setFilter(FilterTaskComplexType ct)
        throws ETLException
    {
        if ( ct != null )
        {
            for ( FilterTaskComplexType.Filter filter: ct.getFilter() )
            {
                filters.add(filter);
            }
        }
    }
}
