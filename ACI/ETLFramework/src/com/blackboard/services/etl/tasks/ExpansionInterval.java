/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.tasks;

import com.blackboard.services.etl.data.Argument;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.exception.RowLevelException;
import com.blackboard.services.etl.jaxb.DataComplexType;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.DateFields;
import com.blackboard.services.etl.jaxb.EtlTaskComplexType;
import com.blackboard.services.etl.jaxb.InputTaskComplexType;
import com.blackboard.services.etl.jaxb.IntervalExpansionInputTaskComplexType;
import com.blackboard.services.etl.wrapper.DataArchiver;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.blackboard.services.utils.JavaUtils;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author crusnak
 */
public class ExpansionInterval extends AbstractAdapter
{
    private static enum AdapterProperty { startInterval, endInterval };
    private static enum ExpansionType { numeric, date };

    private IntervalExpansionInputTaskComplexType ie = null; 
    private RelationalContainer data = null;
    private DateFields interval = null;
    private DataColumn startColumn = null;
    private DataColumn endColumn = null;
    private ExpansionType type = ExpansionType.numeric;
    private String maxCount;
    Map<AdapterProperty,Argument> arguments = new HashMap();

    public ExpansionInterval(EtlTaskComplexType ct, InputTaskComplexType xml, ETLJobWrapper w)
        throws ETLException
    {
        super(ct, xml, w);
        if ( xml.getExpandInterval() == null )
        {
            throw new ETLException("Expecting ExpandInterval in InputAdapter");
        }
        
        IntervalExpansionInputTaskComplexType in = xml.getExpandInterval();
        ie = in;
        setExpansionType(ie.getType());
        
        if ( type == ExpansionType.numeric )
        {
            maxCount = ie.getMaxCount();
        }
        else if ( type == ExpansionType.date )
        {
            interval = ie.getDateInterval();
        }
        
        setStartColumn(in.getInputData().getStartRange());
        setEndColumn(in.getInputData().getEndRange());
        setAdapterProperties(in);
    }

    @Override
    public IntervalExpansionInputTaskComplexType getBaseObject()
    {
        return ie;
    }

    @Override
    public Type getAdapterType()
    {
        return Type.Interval;
    }

    @Override
    public RelationalContainer getData()
        throws ETLException
    {
        setData(super.getData());
        return data;
    }

    @Override
    public void setData(DataSet d)
    {
        super.setData(d);
        if ( d.getType() != DataSet.Type.relational )
        {
            throw new UnsupportedOperationException("DataSet assigned to " + this.getClass().getSimpleName() + " must be relational");
        }
        data = (RelationalContainer)d;
    }
    
    public void setExpansionType(String s)
        throws ETLException
    {
        setExpansionType(ExpansionType.valueOf(s));
    }
    
    public void setExpansionType(ExpansionType et)
    {
        type = et;
    }
    
    public ExpansionType getExpansionType()
    {
        return type;
    }

    private void setStartColumn(DataComplexType ct)
        throws ETLException
    {
        if ( ct != null )
        {
            DataTypes dtype = DataTypes.LONG;
            if ( type == ExpansionType.date )
            {
                dtype = DataTypes.DATE;
            }

            startColumn = new DataColumn(dtype, ct.getValue());
        }
    }
    
    private void setEndColumn(DataComplexType ct)
        throws ETLException
    {
        if ( ct != null )
        {
            DataTypes dtype = DataTypes.INT;
            if ( type == ExpansionType.date )
            {
                dtype = DataTypes.DATE;
            }
            
            endColumn = new DataColumn(dtype, ct.getValue());
        }
    }

    private void setAdapterProperties(IntervalExpansionInputTaskComplexType in)
        throws ETLException
    {
        arguments.clear();
        if ( in != null )
        {
            arguments.put(AdapterProperty.startInterval, new Argument(in.getStartInterval(), this));
            arguments.put(AdapterProperty.endInterval, new Argument(in.getEndInterval(), this));
        }
    }
    
    private <E extends Object> E extractPropertyValue(AdapterProperty prop, Map<String,DataCell> map, Class<E> clazz, boolean allowNull)
        throws ETLException
    {
        E val = null;
        Object o = null;
        String argname = "";
        Argument arg = arguments.get(prop);
        if ( arg != null )
        {
            if ( map == null && arg.getArgumentType() == Argument.Type.column )
            {
                throw new ETLException("No source data defined so must use constant argument types");
            }
            else if ( arg.getArgumentType() == Argument.Type.column )
            {
                argname = String.valueOf(arg.getValue());
                DataCell cell = map.get(argname);
                if ( cell != null )
                {
                    o = cell.getValue();
                }
                else if ( data.getColumnDefinition(argname) != null )
                {
                    o = null;
                }
                else
                {
                    throw new ETLException("Undefined column " + argname + ", unable to extract required value from source data.");
                }

                if ( !allowNull && o == null )
                {
                    throw new RowLevelException("Column " + argname + " value is null and null values are not allowed for " + prop + " arguments.");
                }
                else if ( val != o && !(clazz.isAssignableFrom(o.getClass())) )
                {
                    throw new RowLevelException("Column data referenced by " + argname + " is not an instanceof BinaryObject. Unable to output binary data to a file.");
                }
                else
                {
                    val = (E)o;
                }
            }
            else
            {
                val = (E)arg.getValue();
            }
        }
        else
        {
            throw new ETLException("No arguments defined with property name: " + prop);
        }
        
        return val;
    }

    public void expandOnInterval()
        throws ETLException
    {
        logger.info("expandOnInterval called");
        
        RelationalContainer iterate = new RelationalContainer(null, data);
        int count = iterate.rowCount();
        if ( count == 0 )
        {
            HashMap<String,DataCell> rowdata = new HashMap();
            Date start = extractPropertyValue(AdapterProperty.startInterval, null, Date.class, false);
            Date end = extractPropertyValue(AdapterProperty.endInterval, null, Date.class, false);
            DataColumn scol = new DataColumn(DataTypes.DATE, "StartDate");
            DataColumn ecol = new DataColumn(DataTypes.DATE, "EndDate");
            
            iterate.addColumnDefinition(scol);
            iterate.addColumnDefinition(ecol);
            rowdata.put(scol.getColumnName(), new DataCell(scol, start));
            rowdata.put(ecol.getColumnName(), new DataCell(ecol, end));
            iterate.addDataRow(rowdata);
        }
        
        data.clearColumns();
        data.clearData();
        data.addColumnDefinition(startColumn);
        data.addColumnDefinition(endColumn);
        
        count = iterate.rowCount();
        Iterator<Map<String,DataCell>> it = iterate.getMappedData().iterator();
        for ( int i=1; it.hasNext(); i++ )
        {
            if ( type == ExpansionType.date )
            {
                System.out.println(getTaskName() + ": Expanding dates by " + interval + " on row #" + i + " out of " + count);
                Map<String,DataCell> rowdata = it.next();
                logger.debug("Expanding dates by " + interval + " on row #" + i + ": " + rowdata);

                Date start = extractPropertyValue(AdapterProperty.startInterval, rowdata, Date.class, false);
                Date end = extractPropertyValue(AdapterProperty.endInterval, rowdata, Date.class, false);
                if ( start.getTime() >= end.getTime() )
                {
                    logger.info("Start date is not less than end date, swapping dates.");
                    Date temp = end;
                    end = start;
                    start = temp;
                }

                logger.debug("Date range: " + start + " to " + end);
                ArrayList<Date> dates = new ArrayList();
                Calendar c = Calendar.getInstance();
                c.setTime(start);
                c.add(Calendar.MILLISECOND, -1);
                Date current = start;
                while ( current.getTime() < end.getTime() )
                {
                    logger.debug("Adding interval date: " + current);
                    dates.add(current);
                    c.setTime(current);
                    switch (interval)
                    {
                        case YEAR:
                            c.add(Calendar.MILLISECOND, 1);
                            c.set(Calendar.MONTH, 0);
                            c.set(Calendar.DAY_OF_MONTH, 1);
                            c.set(Calendar.HOUR_OF_DAY, 0);
                            c.set(Calendar.MINUTE, 0);
                            c.set(Calendar.SECOND, 0);
                            c.set(Calendar.MILLISECOND, 0);
                            c.add(Calendar.YEAR, 1);
                            c.add(Calendar.MILLISECOND, -1);
                            current = c.getTime();
                            break;
                        case MONTH:
                            c.add(Calendar.MILLISECOND, 1);
                            c.set(Calendar.DAY_OF_MONTH, 1);
                            c.set(Calendar.HOUR_OF_DAY, 0);
                            c.set(Calendar.MINUTE, 0);
                            c.set(Calendar.SECOND, 0);
                            c.set(Calendar.MILLISECOND, 0);
                            c.add(Calendar.MONTH, 1);
                            c.add(Calendar.MILLISECOND, -1);
                            current = c.getTime();
                            break;
                        case DAY:
                            c.add(Calendar.MILLISECOND, 1);
                            c.set(Calendar.HOUR_OF_DAY, 0);
                            c.set(Calendar.MINUTE, 0);
                            c.set(Calendar.SECOND, 0);
                            c.set(Calendar.MILLISECOND, 0);
                            c.add(Calendar.DAY_OF_MONTH, 1);
                            c.add(Calendar.MILLISECOND, -1);
                            current = c.getTime();
                            break;
                        case HOUR_24:
                            c.add(Calendar.MILLISECOND, 1);
                            c.set(Calendar.MINUTE, 0);
                            c.set(Calendar.SECOND, 0);
                            c.set(Calendar.MILLISECOND, 0);
                            c.add(Calendar.HOUR_OF_DAY, 1);
                            c.add(Calendar.MILLISECOND, -1);
                            current = c.getTime();
                            break;
                        default:
                            EnumSet eset = EnumSet.of(DateFields.YEAR, DateFields.MONTH, DateFields.DAY, DateFields.HOUR_24, DateFields.WEEKOFYEAR);
                            throw new ETLException("Unsupported interval " + interval + ". Supported intervals for date expansion: " + JavaUtils.enumToList(eset));
                    }
                }

                dates.add(end);
                for ( int j=0; j<dates.size()-1; j++ )
                {
                    HashMap<String,DataCell> newdata = new HashMap();
                    Date sdate = dates.get(j);
                    Date edate = dates.get(j+1);
                    c.setTime(sdate);
                    c.add(Calendar.MILLISECOND, 1);
                    sdate = c.getTime();

                    newdata.put(startColumn.getColumnName(), new DataCell(startColumn, sdate));
                    newdata.put(endColumn.getColumnName(), new DataCell(endColumn, edate));
                    data.addDataRow(newdata);
                }
            }
            else if ( type == ExpansionType.numeric )
            {
                int max = 0;
                try
                {
                    DataArchiver arch = wrapper.getDataArchiver();
                    max = Integer.parseInt(arch.processEmbeddedBeanScript(maxCount));
                }
                catch (NumberFormatException e)
                {
                    throw new ETLException("Cannot process numeric interval expansion since maxCount value is non-numeric: " + maxCount);
                }

                System.out.println(getTaskName() + ": Expanding numeric by " + max + " on row #" + i + " out of " + count);
                Map<String,DataCell> rowdata = it.next();
                logger.debug("Expanding numeric by " + max + " on row #" + i + ": " + rowdata);

                int start = extractPropertyValue(AdapterProperty.startInterval, rowdata, Integer.class, false);
                int end = extractPropertyValue(AdapterProperty.endInterval, rowdata, Integer.class, false);
                if ( start >= end )
                {
                    logger.info("Start is not less than end, swapping positions.");
                    int temp = end;
                    end = start;
                    start = temp;
                }

                logger.debug("Numeric range: " + start + " to " + end);
                ArrayList<Integer> numbers = new ArrayList();
                int current = start;
                while ( current < end )
                {
                    logger.debug("Adding interval number: " + current);
                    numbers.add(current);
                    current += max;
                }
                
                logger.debug("Intervals: " + numbers);
                numbers.add(end+1);
                for ( int j=0; j<numbers.size()-1; j++ )
                {
                    HashMap<String,DataCell> newdata = new HashMap();
                    int snum = numbers.get(j);
                    int dnum = numbers.get(j+1);
                    dnum -= 1;

                    newdata.put(startColumn.getColumnName(), new DataCell(startColumn, snum));
                    newdata.put(endColumn.getColumnName(), new DataCell(endColumn, dnum));
                    data.addDataRow(newdata);
                }
            }
            else
            {
                throw new ETLException("Unsupported expansionInterval type: " + type);
            }
        }
    }
}
