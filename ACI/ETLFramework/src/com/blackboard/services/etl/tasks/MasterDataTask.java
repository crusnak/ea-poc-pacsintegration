/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.tasks;

import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Date;

/**
 *
 * @author crusnak
 */
public class MasterDataTask extends AbstractTask
{
    private RelationalContainer data = null;
    
    public MasterDataTask(String taskName, ETLJobWrapper wrapper)
    {
        super(wrapper);
        setData(dataSet);
        
        name = taskName;
    }

    public Object getBaseObject()
    {
        return null;
    }

    public ClassType getClassType()
    {
        return null;
    }

    @Override
    public void setData(DataSet d)
    {
        super.setData(d);
        if ( d != null && d.getType() != DataSet.Type.relational )
        {
            throw new UnsupportedOperationException("DataSet assigned to " + this.getClass().getSimpleName() + " must be relational");
        }
        data = (RelationalContainer)d;
    }

    /**
     * Overridden to ensure that modified master data is stored properly, since this data is
     * archived after each task.
     * @return
     * @throws ETLException
     */
    @Override
    public RelationalContainer getData()
        throws ETLException
    {
        setData(super.getData());
        Date start = new Date();
        if ( archived )
        {
            BufferedInputStream bis = null;
            ObjectInputStream ois = null;

            try
            {
                bis = new BufferedInputStream(new FileInputStream(archiveFile));
                ois = new ObjectInputStream(bis);
                data = (RelationalContainer)ois.readObject();
                ois.close();
                logger.info(data.rowCount() + " rows of data loaded from archive " + archiveFile.getCanonicalPath() + " took " +
                             (double)(new Date().getTime() - start.getTime())/1000.0 + " seconds");
                archived = false;
            }
            catch (IOException e) {throw new ETLException(e);}
            catch (ClassNotFoundException e) {throw new ETLException(e);}
        }

        return data;
    }

}
