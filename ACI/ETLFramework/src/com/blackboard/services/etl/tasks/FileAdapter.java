/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.tasks;

import com.blackboard.services.etl.data.Argument;
import com.blackboard.services.etl.data.BinaryObject;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.exception.IndexException;
import com.blackboard.services.etl.exception.RowLevelException;
import com.blackboard.services.etl.jaxb.DataComplexType;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.EtlTaskComplexType;
import com.blackboard.services.etl.jaxb.FileInputTaskComplexType;
import com.blackboard.services.etl.jaxb.FileOutputTaskComplexType;
import com.blackboard.services.etl.jaxb.MultipleFileInputTaskComplexType;
import com.blackboard.services.etl.jaxb.MultipleFileOutputTaskComplexType;
import com.blackboard.services.etl.jaxb.InputTaskComplexType;
import com.blackboard.services.etl.jaxb.ObjectFactory;
import com.blackboard.services.etl.jaxb.OutputTaskComplexType;
import com.blackboard.services.etl.process.Processor;
import com.blackboard.services.etl.wrapper.ConnectionFactory;
import com.blackboard.services.etl.wrapper.DataArchiver;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.blackboard.services.security.BbKeyGenerator;
import com.blackboard.services.security.ConnectionManager;
import com.blackboard.services.security.PGPFileUtils;
import com.blackboard.services.security.SecuredObjectManager;
import com.blackboard.services.security.SecurityManager;
import com.blackboard.services.security.exception.BbTSSecurityException;
import com.blackboard.services.security.object.EncryptionObject;
import com.blackboard.services.security.object.NamedKey;
import com.blackboard.services.security.object.RegisterableApplication;
import com.blackboard.services.security.object.SSHConnection;
import com.blackboard.services.ssh.SFTPReadFile;
import com.blackboard.services.ssh.SSHFileTransfer;
import com.blackboard.services.utils.DelimittedFormat;
import com.blackboard.services.utils.FixedWidthFormat;
import com.blackboard.services.utils.RadixConversion;
import com.blackboard.services.utils.RegexFormat;
import com.blackboard.services.utils.TextFormat;
import com.blackboard.services.utils.TextFormatException;
import com.blackboard.services.utils.WildcardFilter;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.CharBuffer;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.imageio.IIOException;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
//import org.apache.commons.vfs.FileSystemException;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKey;

/**
 *
 * @author crusnak
 */
public class FileAdapter extends AbstractAdapter implements Adapter
{
    private static enum AdapterProperty { localFile, targetFilename, filename, directory };
    
    public static enum TextFormatType { fixed, delimitted, regex, excel };
    public static enum FixedWidthIndexing { zero_based, one_based };
    public static enum OutputType { replace, append };
    public static enum LinefeedFormat { CRLF, CR, LF };
    public static enum FileSort { filename, timestamp };

    public static final String COL_INPUT_LINE = "INPUT_LINE";
    public static final long BYTE_INTERVAL =    131072;

    public static final Pattern windowsAbsoluteDir = Pattern.compile("^[A-Za-z]\\:.*$");
    public static final Pattern windowsAbsoluteSlashDir = Pattern.compile("^([A-Za-z])\\\\:(.*)$");
    public static final Pattern windowsAbsoluteFile = Pattern.compile("^([A-Za-z]\\:.*[\\\\\\/])(.*)$");
    public static final Pattern windowsAbsoluteSlashFile = Pattern.compile("^([A-Za-z]\\\\:.*[\\\\\\/])(.*)$");
    public static final Pattern windowsRelativeFile = Pattern.compile("^(.*[\\\\\\/])(.*)$");
    public static final Pattern windowsNoDirFile = Pattern.compile("^([A-Za-z0-9\\-\\_\\.]+)$");
    public static final Pattern unixAbsoluteDir = Pattern.compile("^\\/.*$");
    
    private static final String DEFAULT_DELIM = "\\t";

    private Object fa = null;
    private RelationalContainer data = null;
    private File file = null;
    private String processFilename = null;
    private File copyFile = null;
    private File fileRoot = null;
    private File copyRoot = null;
    private File origRoot = null;
    private String remotePath = null;
    private String remoteFilename = null;
    private String binaryFileRoot = null;
    private String binaryZipFile = null;
    private String filename = null;
    private String directory = null;
    private String copyFilename = null;
    private String copyDirectory = null;
    private SSHConnection sftpinfo = null;
    private EncryptionObject encryptinfo = null;
    private boolean useHeader = false;
    private int ignoreRows = 0;
    private String ignoreStartsWith = null;
    private boolean archiveInput = false;
    private boolean removeRemote = false;
    private boolean outputAll = false;
    private boolean overwrite = false;
    private boolean appendJobnum = false;
    private boolean outputBinary = false;
    private boolean createEmpty = false;
    private boolean zipBinary = false;
    private boolean processMultiple = false;
    private Argument binaryFilename = null;
    private Argument binaryData = null;
    private Argument outputFilesDirectory = null;
    private Argument outputFilesFilename = null;
    private TextFormatType tftype = null;
    private FixedWidthIndexing idxtype = null;
    private OutputType otype = null;
    private TextFormat format = null;
    private LinefeedFormat linefeed = null;
    private FileSort fileSort = FileSort.filename;
    private boolean lastLinefeed = false;
    private boolean ignoreCase = true;
    private Integer limitCount = null;
    private String jobnumSeparator = ".";
    private boolean retainFilename = false;
    private boolean copyFiles = false;
    private String worksheet = "";
    private ExcelFileHelper fhelp = null;
    
    private Map<File,String> serverFileMap = new HashMap();
    Map<AdapterProperty,Argument> arguments = new HashMap();
    private DataColumn localFilenameColumn = null;
    private DataColumn localDirectoryColumn = null;
    private DataColumn localFileColumn = null;
    private DataColumn serverFileColumn = null;
    private RelationalContainer sourceFiles = null;
    
    public FileAdapter(FileOutputTaskComplexType xml, File root, boolean write, boolean empty)
        throws ETLException
    {
        fa = xml;
        direction = DataDirection.Outgoing;
        fileRoot = root;
        origRoot = root;
        overwrite = write;
        createEmpty = empty;
        otype = OutputType.replace;
        setOutput(xml);
    }

    public FileAdapter(EtlTaskComplexType ct, InputTaskComplexType xml, File root, ETLJobWrapper w)
        throws ETLException
    {
        super(ct, xml, w);
        setData(dataSet);
        
        if ( xml.getFileInput() != null )
        {
            logger.info("Constructing FileInput");
            FileInputTaskComplexType in = xml.getFileInput();
            fa = in;
            fileRoot = root;
            origRoot = root;
            processMultiple = false;
            archiveInput = in.isArchiveFile();
            removeRemote = in.isRemoveRemoteFile();
            setFileInfo(in.getFile());
            setFileCopy(in.getCopyFile());
            setInputData(in.getInputData());
        }
        else if ( xml.getFilesInput() != null )
        {
            logger.info("Constructing FilesInput");
            MultipleFileInputTaskComplexType in = xml.getFilesInput();
            fa = in;
            fileRoot = root;
            origRoot = root;
            processMultiple = true;
            archiveInput = in.isArchiveFiles();
            removeRemote = in.isRemoveRemoteFiles();
            setFileInfo(in.getFile());
            setFileCopy(in.getCopyFile());
            setInputData(in.getInputData());
        }
        else
        {
            throw new ETLException("Expecting FileInput or FilesInput in InputAdapter");
        }

    }

    public FileAdapter(EtlTaskComplexType ct, OutputTaskComplexType xml, File root, ETLJobWrapper w)
        throws ETLException
    {
        super(ct, xml, w);
        setData(dataSet);

        if ( xml.getFileOutput() != null )
        {
            FileOutputTaskComplexType out = xml.getFileOutput();
            fa = out;
            fileRoot = root;
            origRoot = root;
            processMultiple = false;
            setFileInfo(out.getFile(), out.getOutputBinary() != null);
            setFileCopy(out.getCopyFile());
            setOutput(out);
        }
        else if ( xml.getFilesOutput() != null )
        {
            MultipleFileOutputTaskComplexType out = xml.getFilesOutput();
            fa = out;
            fileRoot = root;
            origRoot = root;
            processMultiple = true;
            archiveInput = out.isArchiveOriginalFiles();
            setFileInfo(out.getFile());
            setFileCopy(out.getCopyFiles());
            setOutputFiles(out.getOutputFiles());
            setOutputData(out.getOutputData());
        }
        else
        {
            throw new ETLException("Expecting FileOutput or FilesOutput in OutputAdapter");
        }

    }

    public FileAdapter(RelationalContainer dc, File outputFile)
        throws ETLException
    {
        direction = DataDirection.Outgoing;
        file = outputFile;
        data = dc;
        otype = OutputType.replace;
        ObjectFactory factory = new ObjectFactory();

        FileOutputTaskComplexType.OutputData output = factory.createFileOutputTaskComplexTypeOutputData();
        output.setType(String.valueOf(TextFormatType.delimitted));
        output.setDelimiter(DEFAULT_DELIM);
        output.setWriteHeader(true);
        output.setOutputAll(true);
        setOutputData(output);
    }
    
    public FileAdapter(String file, String dir, File root)
        throws ETLException
    {
        direction = DataDirection.Incoming;
        setFileInfo(file, dir, null, root);
        
    }

    public Object getBaseObject()
    {
        return fa;
    }

    public Adapter.Type getAdapterType()
    {
        return Adapter.Type.File;
    }

    @Override
    public RelationalContainer getData()
        throws ETLException
    {
        setData(super.getData());
        return data;
    }

    @Override
    public void setData(DataSet d)
    {
        super.setData(d);
        if ( d != null && d.getType() != DataSet.Type.relational )
        {
            throw new UnsupportedOperationException("DataSet assigned to " + this.getClass().getSimpleName() + " must be relational");
        }
        data = (RelationalContainer)d;
    }

    public TextFormatType getTextFormatType()
    {
        return tftype;
    }

    private void setTextFormatType(String tf)
    {
        tftype = TextFormatType.valueOf(tf);
    }

    public FixedWidthIndexing getFixedWidthIndexingType()
    {
        return idxtype;
    }

    private void setFixedWidthIndexingType(String tf)
    {
        idxtype = FixedWidthIndexing.valueOf(tf);
    }

    public OutputType getOutputType()
    {
        return otype;
    }

    private void setOutputType(String ot)
    {
        otype = OutputType.valueOf(ot);
    }

    public LinefeedFormat getLinefeedFormat()
    {
        return linefeed;
    }

    private void setLinefeedFormat(String lf)
    {
        linefeed = LinefeedFormat.valueOf(lf);
    }
    
    private FileSort getFileSort()
    {
        return fileSort;
    }
    
    private void setFileSort(String fs)
    {
        fileSort = FileSort.valueOf(fs);
    }

    public File getTextFile()
    {
        return file;
    }

    public String getProcessedFilename()
    {
        return processFilename;
    }

    public void setTextFile(File f)
    {
        file = f;
    }

    public boolean isReadFileToBeArchived()
    {
        return archiveInput;
    }

    public RelationalContainer readFile(String jobnum)
        throws ETLException
    {
        logger.debug("Arguments: " + arguments);
        
        // Reassign columns and format at runtime in case this is a target filtered task (can't use current
        // task data on file inputs)
        if ( fa instanceof FileInputTaskComplexType )
        {
            setInputData(((FileInputTaskComplexType)fa).getInputData());
        }
        else if ( fa instanceof MultipleFileInputTaskComplexType )
        {
            setInputData(((MultipleFileInputTaskComplexType)fa).getInputData());
        }

        // Cannot join current task data to file inputs
        if ( arguments.containsKey(AdapterProperty.filename) )
        {
            sourceFiles = new RelationalContainer(name, data);
            data.clearData();
        }
        else
        {
            data.clearData();
        }
        
        DataColumn ecol = new DataColumn(DataTypes.STRING, RelationalContainer.COL_ERROR_MSG);
        DataColumn lcol = new DataColumn(DataTypes.STRING, COL_INPUT_LINE);
        RelationalContainer errors = new RelationalContainer(data.getDataName() + ERROR_DATA_NAME);
        errors.addColumnDefinition(lcol);
        errors.addColumnDefinition(ecol);

        try
        {
            // Set the connection info at runtime (to support dynamic global variable modification)
            if ( fa instanceof FileInputTaskComplexType )
            {
                sftpinfo = ConnectionFactory.getSSHConnection(((FileInputTaskComplexType)fa).getFile(), this);
                encryptinfo = ConnectionFactory.getEncryptionObject(((FileInputTaskComplexType)fa).getFile(), this);
            }
            else if ( fa instanceof MultipleFileInputTaskComplexType )
            {
                sftpinfo = ConnectionFactory.getSSHConnection(((MultipleFileInputTaskComplexType)fa).getFile(), this);
                encryptinfo = ConnectionFactory.getEncryptionObject(((MultipleFileInputTaskComplexType)fa).getFile(), this);
            }
            logger.debug("SFTP info: " + sftpinfo);
            logger.debug("Encryption info: " + encryptinfo);
            
            // If any bean script exists in the filename, assign global variable values
            updateFileWithGlobalVariableAssignment();
            updateCopyFileWithGlobalVariableAssignment();

            List<File> files = getFilesForReading(jobnum);
            if ( !files.isEmpty() )
            {
                int fileCount = 0;
                for ( File origFile: files )
                {
                    fileCount++;
                    File actualFile = origFile;
                    processFilename = actualFile.getCanonicalPath();
                    logger.debug("Checking file existance: " + processFilename);
                    if ( actualFile.exists() )
                    {
                        logger.info("File found, processing inputs...");
                        BufferedReader reader = null;
                        if ( encryptinfo != null )
                        {
                            logger.info("Decrypting file with algorithm: " + encryptinfo.getEncryptionType());
                            Cipher c = getCipher(Cipher.DECRYPT_MODE, encryptinfo);
                            switch (encryptinfo.getEncryptionType())
                            {
                                case PGP:
                                    BufferedInputStream bis = new BufferedInputStream(new FileInputStream(actualFile));
                                    BufferedInputStream kis = new BufferedInputStream(new FileInputStream(encryptinfo.getSecretKeyRingFile()));
                                    File temp = new File(wrapper.getDataArchiver().getRunningArchiveDirectory(jobnum), actualFile.getName() + ".txt");
                                    temp.deleteOnExit();

                                    PGPFileUtils.decryptFile(bis, kis, encryptinfo.getPassphrase().toCharArray(), temp);
                                    actualFile = temp;
                                    FileInputStream fis = new FileInputStream(temp);
                                    
                                    if ( tftype == TextFormatType.excel )
                                    {
                                        fhelp = new ExcelFileHelper(fis);
                                    }
                                    else
                                    {
                                        reader = new BufferedReader(new InputStreamReader(fis));
                                    }
                                    break;
                                default:
                                    fis = new FileInputStream(actualFile);
                                    CipherInputStream cis = new CipherInputStream(fis, c);
                                    
                                    if ( tftype == TextFormatType.excel )
                                    {
                                        fhelp = new ExcelFileHelper(cis);
                                    }
                                    else
                                    {
                                        reader = new BufferedReader(new InputStreamReader(cis));
                                    }                                    
                            }
                        }
                        else
                        {
                            FileInputStream fis = new FileInputStream(actualFile);
                            
                            if ( tftype == TextFormatType.excel )
                            {
                                fhelp = new ExcelFileHelper(fis);
                            }
                            else
                            {                            
                                reader = new BufferedReader(new InputStreamReader(fis));
                            }
                        }

                        if ( !processMultiple )
                        {
                            // If excel type, have the excel file helper do the majority of the work
                            if ( tftype == TextFormatType.excel )
                            {
                                // Set the current worksheet by name defined in the xml, if not it is always first worksheet in file
                                fhelp.setCurrentWorksheet(worksheet);
                                logger.debug("Ignoring rows " + ignoreRows);
                                
                                Map<Integer,String> headers = null;
                                if ( useHeader )
                                {
                                    headers = fhelp.getHeaderValues(ignoreRows);
                                    logger.debug("Excel headers read at rownum " + ignoreRows + ": " + headers);
                                    for ( String header: headers.values() )
                                    {
                                        data.addColumnDefinition(DataTypes.STRING, header, null);
                                    }
                                    
                                    fhelp.setCurrentRow(ignoreRows + 1);
                                }
                                else
                                {
                                    headers = new LinkedHashMap();
                                    for ( int j=0; j<data.getColumnNames().size(); j++ )
                                    {
                                        headers.put(j, data.getColumnNames().get(j));
                                    }
                                    fhelp.setCurrentRow(ignoreRows);
                                }
                                
                                int lastRownum = fhelp.getCurrentWorksheet().getLastRowNum();
                                for ( fhelp.getCurrentRow(); fhelp.getCurrentRow().getRowNum() < lastRownum; fhelp.getNextRow() )
                                {
                                    int i = fhelp.getCurrentRow().getRowNum() + 1;
                                    System.out.println(getTaskName() + ": Reading row #" + i + " from input file #" + fileCount);
                                    HashMap<String,String> row = new HashMap();
                                    //List<String> columns = data.getColumnNames();
                                    List<String> cells = fhelp.getRowData();
                                    
                                    try
                                    {
                                        logger.debug("Cell data for row " + i + ": " + cells);
                                        for ( Map.Entry<Integer,String> entry: headers.entrySet() )
                                        {
                                            int x = entry.getKey();
                                            String colname = entry.getValue();
                                            String val = null;

                                        /*for ( int x=0; x<columns.size(); x++ )
                                        {
                                            String colname = null;
                                            String val = null;*/ 

                                            try
                                            {
                                                //colname = columns.get(x);
                                                val = cells.get(x);
                                            }
                                            catch (IndexOutOfBoundsException e)
                                            {
                                                logger.debug("Reference to undefined cell at column: " + x);
                                                val = "";
                                            }

                                            row.put(colname, val);
                                        }

                                        data.addFileDataRow(row);
                                    }
                                    catch (ETLException e)
                                    {
                                        // If an index exception is thrown and index exceptions are not set as fatal
                                        // throw a row level error.
                                        if ( e instanceof IndexException && !wrapper.indexExceptionsAreFatal() )
                                        {
                                            // Add to errors
                                            String error = "Failed to add data from read row: " + (i+ignoreRows) + ": " + e.getMessage();
                                            logger.warn(error);
                                            Map<String,DataCell> rowe = new HashMap();
                                            rowe.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, error));
                                            rowe.put(COL_INPUT_LINE, new DataCell(lcol, cells));
                                            errors.addDataRow(rowe);
                                        }
                                        else
                                        {
                                            throw new ETLException("Failed to add data from read row: " + (i+ignoreRows), e);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                for ( int i=0; isReaderReady(reader) && i<ignoreRows; i++ )
                                {
                                    String line = readLineFromReader(reader);
                                    logger.debug("Ignoring row #" + i + ": " + line);
                                }

                                if ( tftype != null )
                                {
                                    logger.debug("TextFormat: " + format);

                                    if ( isReaderReady(reader) && useHeader ) 
                                    {
                                        String head = readLineFromReader(reader);
                                        logger.debug("Processing header to define format fields: " + head);
                                        // Read the first row and build up the column definitions
                                        String[] header = format.readHeader(head);
                                        for ( int i=0; i<header.length; i++ )
                                        {
                                            switch (tftype)
                                            {
                                                case delimitted:
                                                    DelimittedFormat ft = (DelimittedFormat)format;
                                                    ft.addField(header[i].trim(), -1, true, false, String.valueOf(DataTypes.STRING));
                                                    data.addColumnDefinition(DataTypes.STRING, header[i].trim(), null);
                                                    break;
                                                case fixed:
                                                    // TODO: add header fields to format for fixed width
                                                    break;
                                            }
                                        }
                                        logger.debug("Header read, format updated: " + format);
                                    }

                                    // Keep reading while there are lines to read
                                    for ( int i=1; isReaderReady(reader); i++ )
                                    {
                                        data.setCurrentProcessedRow(i);
                                        System.out.println(getTaskName() + ": Reading row #" + i + " from input file #" + fileCount);
                                        String line = readLineFromReader(reader);
                                        try
                                        {
                                            // If the line is empty or it starts with the specified ignore string, don't add a row
                                            if ( line.trim().length() > 0 && !(ignoreStartsWith != null && line.startsWith(ignoreStartsWith)) )
                                            {
                                                Map<String,String> row = format.processLine(line);
                                                logger.trace("Rowdata #" + i + ": " + row);
                                                data.addFileDataRow(row);
                                            }
                                        }
                                        catch (TextFormatException e)
                                        {
                                            // Add to errors
                                            String error = "Failed to process input line: " + e.getMessage();
                                            logger.warn(error);
                                            Map<String,DataCell> row = new HashMap();
                                            row.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, error));
                                            row.put(COL_INPUT_LINE, new DataCell(lcol, line));
                                            errors.addDataRow(row);
                                        }
                                        catch (ETLException e)
                                        {
                                            // If an index exception is thrown and index exceptions are not set as fatal
                                            // throw a row level error.
                                            if ( e instanceof IndexException && !wrapper.indexExceptionsAreFatal() )
                                            {
                                                // Add to errors
                                                String error = "Failed to add data from read row: " + (i+ignoreRows) + ": " + e.getMessage();
                                                logger.warn(error);
                                                Map<String,DataCell> row = new HashMap();
                                                row.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, error));
                                                row.put(COL_INPUT_LINE, new DataCell(lcol, line));
                                                errors.addDataRow(row);
                                            }
                                            else
                                            {
                                                throw new ETLException("Failed to add data from read row: " + (i+ignoreRows), e);
                                            }
                                        }
                                    }
                                }
                            }                           
                        }
                        else
                        {
                            System.out.println(getTaskName() + ": Found file : " + actualFile.getName());
                            Map<String,DataCell> rowdata = new HashMap();
                            rowdata.put(localFilenameColumn.getColumnName(), new DataCell(localFilenameColumn, actualFile.getName()));
                            rowdata.put(localFileColumn.getColumnName(), new DataCell(localFileColumn, actualFile.getCanonicalPath()));
                            rowdata.put(localDirectoryColumn.getColumnName(), new DataCell(localDirectoryColumn, actualFile.getParentFile().getCanonicalPath()));
                            data.addDataRow(rowdata);
                        }

                        logger.debug("Determining if copying file: " + copyFile + " or retainFilename=" + retainFilename);
                        
                        // If retaining filename on sftp read copy requests, handle here
                        if ( retainFilename )
                        {
                            setTargetFileInfo(copyDirectory, origFile.getName(), origRoot);
                            logger.debug("Copying file from " + actualFile.getCanonicalPath() + " to: " + copyFile.getCanonicalPath());
                            FileUtils.copyFile(actualFile, copyFile);
                        }
                        // Copy the file to the requested target
                        else if ( copyFile != null )
                        {
                            logger.debug("Copying file from " + actualFile.getCanonicalPath() + " to: " + copyFile.getCanonicalPath());
                            FileUtils.copyFile(actualFile, copyFile);
                        }

                        // If excel, let the file helper finalize any open handles, otherwise close the reader
                        if ( tftype == TextFormatType.excel )
                        {
                            try
                            {
                                fhelp.finalize();
                            }
                            catch (Throwable t)
                            {
                                logger.warn("Failed to finalize excel file: " + actualFile, t);
                            }
                        }
                        else
                        {
                            reader.close();
                        }

                        // Move file to input directory if specified
                        archiveFileInput(jobnum, origFile);
                    }
                    else
                    {
                        System.out.println("Input file(s) not found, no rows imported: " + processFilename);
                        logger.info("File doesn't exist, no subsequent processing needed: " + processFilename);
                    }
                }
            }
            else
            {
                System.out.println("No input files found, no processing performed.");
                logger.info("No input files found, no subsequent processing needed.");
            }

        }
        catch (GeneralSecurityException e)
        {
            throw new ETLException("Unable to decrypt incoming file: " + file + ", of cipher type: " + encryptinfo.getEncryptionTypeString(), e);
        }
        catch (IOException e)
        {
            throw new ETLException("Failure reading file: " + file, e);
        }

        // Debug indices if data contains unique columns
        if ( data.containsUniqueColumns() )
        {
            logger.trace("PrimaryKeyIndex: " + data.getDataIndices());
        }
        
        // Add the errors to the wrapper
        if ( errors.rowCount() > 0 )
        {
            wrapper.setErrorData(name, errors);
        }

        return data;
    }

    public RelationalContainer writeFile(String jobnum)
        throws ETLException
    {
        DataColumn ecol = new DataColumn(DataTypes.STRING, RelationalContainer.COL_ERROR_MSG);
        RelationalContainer errors = new RelationalContainer(data.getDataName() + ERROR_DATA_NAME);
        errors.setColumnDefinitions(data.getColumnDefinitions());
        errors.addColumnDefinition(ecol);
        
        // Set the connection info at runtime (to support dynamic global variable modification)
        if ( fa instanceof FileOutputTaskComplexType )
        {
            sftpinfo = ConnectionFactory.getSSHConnection(((FileOutputTaskComplexType)fa).getFile(), this);
            encryptinfo = ConnectionFactory.getEncryptionObject(((FileOutputTaskComplexType)fa).getFile(), this);
        }
        else if ( fa instanceof MultipleFileOutputTaskComplexType )
        {
            sftpinfo = ConnectionFactory.getSSHConnection(((MultipleFileOutputTaskComplexType)fa).getFile(), this);
            encryptinfo = ConnectionFactory.getEncryptionObject(((MultipleFileOutputTaskComplexType)fa).getFile(), this);
        }

        // Output type has no bearing when outputing binary files
        if ( outputBinary )
        {
            // If any bean script exists in the filename, assign global variable values
            updateBinaryFileRootWithGlobalVariableAssignment();
            
            DataColumn fcol = data.getColumnDefinition(String.valueOf(binaryFilename.getValue()));
            if ( fcol == null )
            {
                throw new ETLException("Undefined filename column in data source: " + binaryFilename);
            }
            else if ( fcol.getDataType() != DataTypes.STRING )
            {
                throw new ETLException("Binary outputs require filename arguments to be a string data type");
            }
            
            DataColumn bcol = data.getColumnDefinition(String.valueOf(binaryData.getValue()));
            if ( bcol == null )
            {
                throw new ETLException("Undefined binary data column in data source: " + binaryData);
            }
            else if ( bcol.getDataType() != DataTypes.BINARY )
            {
                throw new ETLException("Binary outputs require data arguments to be a binary data type");
            }
            
            SSHFileTransfer sftp = null;
            
            try
            {
                if ( sftpinfo != null )
                {
                    sftp = new SSHFileTransfer(sftpinfo);
                }
            
                ArrayList<File> files = new ArrayList();
                if ( data.rowCount() > 0 )
                {
                    Iterator<Map<String,DataCell>> it = data.getMappedData().iterator();
                    for ( int i=0; it.hasNext(); i++ )
                    {
                        data.setCurrentProcessedRow(i+1);
                        System.out.println(getTaskName() + ": Writing binary file #" + (i+1) + " out of " + data.rowCount());
                        Map<String,DataCell> row = it.next();
                        DataCell filename = row.get(binaryFilename.getValue());
                        DataCell data = row.get(binaryData.getValue());
                        File file = null;
                        byte[] b = new byte[0];

                        try
                        {
                            if ( filename == null )
                            {
                                throw new IOException("Failed to create binary data file with null filename");
                            }
                            else
                            {
                                file = new File(fileRoot, String.valueOf(filename.getValue()));

                                // If we are zipping the binary files, delete the zipped files on exit
                                if ( zipBinary || sftp != null )
                                {
                                    file.deleteOnExit();
                                    files.add(file);
                                }
                            }

                            BinaryObject bo = null;
                            if ( data != null )
                            {
                                bo = (BinaryObject)data.getValue();
                                b = bo.getData();
                            }

                            // Check sftp connection and upload file if needed
                            if ( sftpinfo != null )
                            {
                                Date now = new Date();
                                String remoteFile = remotePath + "/" + filename.getValue();
                                logger.info("Need to write remote file to " + sftpinfo.getHost() + ": " + remoteFile);

                                SSHFileTransfer.WriteMode putType;
                                switch (otype)
                                {
                                    case append:
                                        putType = SSHFileTransfer.WriteMode.append;
                                        break;
                                    default:
                                        putType = SSHFileTransfer.WriteMode.overwrite;
                                }

                                long read = sftp.put(bo.getTempFile(), remoteFile, putType, BYTE_INTERVAL);
                                logger.info("Wrote " + read +  " bytes from file '" + remoteFile + "' on remote server '" + sftpinfo.getHost() + "' took " + (new Date().getTime()-now.getTime()) + " ms");
                            }
                            else
                            {
                                logger.debug("Writing binary file for row #" + (i+1) + ": " + file);
                                BinaryObject output = new BinaryObject(file, 0, 0);
                                output.setData(b);
                                b = new byte[0];
                            }
                        }
                        catch (ClassCastException e)
                        {
                            // Add to errors
                            String error = "Data is not a BinaryObject: " + e.getMessage();
                            logger.warn(error + " | Rowdata: " + row, e);
                            it.remove();
                            row.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, error));
                            errors.addDataRow(row);
                        }
                        catch (IOException e) 
                        {
                            // Add to errors
                            String error = "Failed to create binary output file: " + e.getMessage();
                            logger.warn(error + " | Rowdata: " + row, e);
                            it.remove();
                            row.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, error));
                            errors.addDataRow(row);
                        }
                    }

                    // If we are to zip the binary files up do so here
                    if ( zipBinary )
                    {
                        try
                        {
                            System.out.println(getTaskName() + ": Adding " + files.size() + " binary files to zip archive: " + file.getCanonicalPath());
                            logger.info("Zipping binary files to zip file: " + file.getCanonicalPath());

                            int BUFFER = 2048;
                            byte[] data = new byte[BUFFER];
                            FileOutputStream fos = new FileOutputStream(file);
                            ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(fos));
                            for ( File f: files )
                            {
                                FileInputStream fis = new FileInputStream(f);
                                BufferedInputStream bis = new BufferedInputStream(fis, BUFFER);
                                ZipEntry entry = new ZipEntry(f.getName());
                                zos.putNextEntry(entry);

                                int count;
                                while ( (count = bis.read(data, 0, BUFFER)) != -1 )
                                {
                                    zos.write(data, 0, count);
                                }

                                fis.close();
                            }

                            zos.close();

                            logger.info(files.size() + " files added to zip archive: " + file.getCanonicalPath());
                        }
                        catch (IOException e)
                        {
                            throw new ETLException("Failed to zip binary files to: " + file, e);
                        }
                    }
                }
                else
                {
                    System.out.println("No binary files to output");
                }
            }
            catch (JSchException e)
            {
                String error = "Failure writing remote file: " + directory + "/" + filename;
                logger.warn(error, e);
                for ( Map<String,DataCell> rowdata: data.getMappedData() )
                {
                    rowdata.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, error + ": " + e.getMessage()));
                    errors.addDataRow(rowdata);
                }

                data.clearData();
            }
            finally
            {
                if ( sftp != null )
                {
                    sftp.disconnect();
                }
            }
        }
        else
        {
            // If any bean script exists in the filename, assign global variable values
            updateFileWithGlobalVariableAssignment();
            updateCopyFileWithGlobalVariableAssignment();

            // If job number is to be appended to the filename, update the file here
            if ( appendJobnum )
            {
                updateFileWithJobNumber(jobnum);
            }

            SSHFileTransfer sftp = null;
            
            try
            {
                if ( sftpinfo != null )
                {
                    sftp = new SSHFileTransfer(sftpinfo);
                }
                
                File pgpTempFile = null;
                File actualFile = getFileForWriting(jobnum);
                processFilename = actualFile.getCanonicalPath();
                switch (otype)
                {
                    case replace:
                        break;
                    case append:
                        break;
                }
                
                if ( actualFile.exists() && !overwrite && otype == OutputType.replace && copyFile == null )
                {
                    String error = "File already exists. Overwrite attribute is false and output type is replace, cannot write file.";
                    logger.warn(error);
                    for ( Map<String,DataCell> rowdata: data.getMappedData() )
                    {
                        rowdata.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, error));
                        errors.addDataRow(rowdata);
                    }

                    data.clearData();
                }
                else if ( data.rowCount() > 0 || createEmpty || copyFile != null )
                {
                    if ( !processMultiple )
                    {
                        boolean fileExists = actualFile.exists();
                        if ( fileExists && otype == OutputType.replace )
                        {
                            logger.warn("File found but overwrite attribute is true, overwriting existing file.");
                            actualFile.delete();
                        }

                        // If output all is set, output all the columns defined by the data container
                        if ( outputAll )
                        {
                            switch (tftype)
                            {
                                case excel:
                                case delimitted:
                                    ((DelimittedFormat)format).resetFields();
                                    for ( DataColumn col: data.getColumnDefinitions().values() )
                                    {
                                        ((DelimittedFormat)format).addField(new DelimittedFormat.Field(col.getColumnName(), -1, true, false, String.valueOf(DataTypes.STRING)));
                                        if ( data.getColumnDefinition(col.getColumnName().trim()) == null )
                                        {
                                            DataColumn dc = data.addColumnDefinition(DataTypes.STRING, col.getColumnName().trim(), null);
                                            logger.debug("Adding undefined column definition for output all file output: " + dc);
                                        }
                                    }
                                    break;
                                case fixed:

                                    // TODO: add support for fixed width file formats
                                    /*((FixedWidthFormat)format).resetFields();
                                    for ( DataColumn col: data.getColumnDefinitions().values() )
                                    {
                                        ((FixedWidthFormat)format).addField(new FixedWidthFormat.Field(name, idx);
                                    }*/
                                    break;
                                case regex:
                                    throw new ETLException("Cannot use regex file type as a file output");
                            }
                        }

                        BufferedWriter writer = null;
                        OutputStream fhos = null;
                        if ( encryptinfo != null && otype == OutputType.replace )
                        {
                            if ( tftype == TextFormatType.excel )
                            {
                                throw new ETLException("Cannot encrypt an excel output file. Use password protection instead.");
                            }
                            
                            logger.info("Encrypting file with algorithm: " + encryptinfo.getEncryptionType());
                            Cipher c = getCipher(Cipher.ENCRYPT_MODE, encryptinfo);
                            switch (encryptinfo.getEncryptionType())
                            {
                                case PGP:
                                    pgpTempFile = File.createTempFile("pgp", "txt");
                                    pgpTempFile.deleteOnExit();
                                    writer = new BufferedWriter(new FileWriter(pgpTempFile));
                                    break;
                                default:
                                    FileOutputStream fos = new FileOutputStream(actualFile);
                                    CipherOutputStream cos = new CipherOutputStream(fos, c);
                                    writer = new BufferedWriter(new OutputStreamWriter(cos));
                            }
                        }
                        else if ( encryptinfo != null && otype == OutputType.append )
                        {
                            String error = "Cannot append to existing encrypted file. Use output type replace.";
                            logger.warn(error);
                            for ( Map<String,DataCell> rowdata: data.getMappedData() )
                            {
                                rowdata.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, error));
                                errors.addDataRow(rowdata);
                            }

                            data.clearData();
                        }
                        else
                        {
                            if ( tftype != TextFormatType.excel )
                            {
                                writer = new BufferedWriter(new FileWriter(processFilename, otype == OutputType.append));
                            }
                        }

                        // Regular file format
                        if ( tftype != TextFormatType.excel )
                        {   
                            // If the output type is replace and useHeader is set, output the header.
                            // Or if output type is append and the file doesn't exist, output the header
                            if ( (useHeader && otype == OutputType.replace) || (useHeader && !fileExists) )
                            {
                                // Write the header to the file
                                ArrayList<String> values = new ArrayList();
                                for ( TextFormat.Field field: format.getSimpleFields() )
                                {
                                    values.add(field.getName());
                                }

                                try
                                {
                                    String line = format.formatLine(values);
                                    writeLineToWriter(writer, line, false);
                                }
                                catch (TextFormatException e)
                                {
                                    throw new ETLException("Unable to format header data to output: " + values, e);
                                }
                            }

                            logger.info("Formatting output using text format: " + format);
                            int count = data.rowCount();
                            Iterator<Map<String,DataCell>> it = data.getMappedData().iterator();
                            for ( int i=0; it.hasNext(); i++ )
                            {
                                Map<String,DataCell> row = it.next();
                                if ( getTaskName() != null )
                                {
                                    data.setCurrentProcessedRow(i);
                                    System.out.println(getTaskName() + ": Writing row #" + (i+1) + " out of " + count);
                                }

                                try
                                {
                                    // This relies on the text formatter to extract the necessary values
                                    /*HashMap<String,String> map = new HashMap();
                                    for ( Map.Entry<String,DataCell> entry: row.entrySet() )
                                    {
                                        DataCell cell = entry.getValue();
                                        String val = null;
                                        if ( cell != null && cell.getValue() != null )
                                        {
                                            val = String.valueOf(cell.getValue());
                                        }
                                        map.put(entry.getKey(), val);
                                    }*/

                                    // This extracts the necessary values within the adapter and is able
                                    // to handle constants appropriately.
                                    ArrayList<String> values = new ArrayList();
                                    for ( TextFormat.Field field: format.getSimpleFields() )
                                    {
                                        // If field is constant add the name of the field as the value, performing
                                        // any embedded bean script global variable lookups, and converting to the defined datatype.
                                        if ( field.isConstant() )
                                        {
                                            DataColumn dc = new DataColumn(DataTypes.valueOf(field.getDataType()), null);
                                            DataCell cell = new DataCell(dc, wrapper.getDataArchiver().processEmbeddedBeanScript(field.getName()));
                                            String value = "";
                                            if ( cell.getValue() != null && cell.getValue() instanceof BinaryObject )
                                            {
                                                value = String.valueOf(RadixConversion.bytesToChars(((BinaryObject)cell.getValue()).getData()));
                                            }
                                            else if ( cell.getValue() != null )
                                            {
                                                value = String.valueOf(cell.getValue());
                                            }

                                            logger.trace("Assigning constant value to " + field + ": " + cell + " -> '" + value + "'");
                                            values.add(value);
                                        }
                                        // Otherwise, get the value of the data container with the name of the field
                                        else
                                        {
                                            String val = null;
                                            DataCell cell = row.get(field.getName());
                                            if ( cell != null && cell.getValue() != null )
                                            {
                                                if ( cell.getValue() instanceof Date )
                                                {
                                                    val = wrapper.dateOutputFormat.format(cell.getValue());
                                                }
                                                else
                                                {
                                                    val = String.valueOf(cell.getValue());
                                                }
                                            }

                                            values.add(val);
                                        }
                                    }

                                    //String line = format.formatLine(map);
                                    String line = format.formatLine(values);
                                    writeLineToWriter(writer, line, !it.hasNext());
                                }
                                catch (TextFormatException e)
                                {
                                    // Add to errors
                                    String error = "Failed to format data to output: " + e.getMessage();
                                    logger.warn(error + " | Rowdata: " + row, e);
                                    it.remove();
                                    row.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, error));
                                    errors.addDataRow(row);
                                }
                            }

                            writer.flush();
                            writer.close();
                        }
                        // Excel file output
                        else
                        {
                            logger.info("Outputing to excel file: " + processFilename);
                            fhelp = new ExcelFileHelper(actualFile);
                            fhelp.setCurrentWorksheet(worksheet);
                            fhos = new FileOutputStream(fhelp.getReferencedFile());
                            
                            if ( fileExists && otype == OutputType.append && tftype == TextFormatType.excel )
                            {
                                logger.info("Excel file found with append true, setting row pointer to last row");
                                fhelp.getLastRow();
                            } 
                            
                            Row erow = fhelp.getCurrentRow();
                            Map<String,Integer> headers = new HashMap();
                            // If the output type is replace and useHeader is set, output the header.
                            // Or if output type is append and the file doesn't exist, output the header
                            if ( (useHeader && otype == OutputType.replace) || (useHeader && !fileExists) )
                            {
                                // Write the header to the file
                                erow = fhelp.getFirstRow();
                                for ( int j=0; j<format.getSimpleFields().size(); j++ )
                                {
                                    Cell ecell = erow.createCell(j);
                                    ecell.setCellValue(format.getSimpleFields().get(j).getName());
                                    headers.put(format.getSimpleFields().get(j).getName(), j);
                                    logger.debug("Setting header " + ecell.getColumnIndex() + "=" + format.getSimpleFields().get(j));
                                }
                            }
                            
                            if ( headers.isEmpty() )
                            {
                                throw new ETLException("Headers are required for Excel output in order to assign the appropriate DataCells to the correct columns on a worksheet");
                            }
                            
                            logger.info("Outputing using excel format");
                            int count = data.rowCount();
                            Iterator<Map<String,DataCell>> it = data.getMappedData().iterator();
                            for ( int i=0; it.hasNext(); i++ )
                            {
                                erow = fhelp.getNextRow();
                                Map<String,DataCell> rowdata = it.next();
                                if ( getTaskName() != null )
                                {
                                    data.setCurrentProcessedRow(i);
                                    System.out.println(getTaskName() + ": Writing row #" + (i+1) + " out of " + count);

                                    for ( int j=0; j<format.getSimpleFields().size(); j++ )
                                    {
                                        String fname = format.getSimpleFields().get(j).getName();
                                        int cindex = headers.get(fname);
                                        Cell ecell = erow.createCell(cindex);
                                        
                                        logger.trace("rowdata value=" + rowdata.get(fname));
                                        if ( rowdata.containsKey(fname) && rowdata.get(fname).getValue() != null )
                                        {
                                            ecell.setCellValue(String.valueOf(rowdata.get(fname).getValue()));
                                        }
                                        else
                                        {
                                            ecell.setBlank();
                                        }
                                    }

                                }
                            }
                        
                            // Write file
                            fhelp.writeWorkbook(fhos);
                            fhos.close();
                            
                        }
                        

                        // If PGP encryption, we need to encrypt here
                        if ( encryptinfo != null && encryptinfo.getEncryptionType() == EncryptionObject.Type.PGP )
                        {
                            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(actualFile));
                            PGPPublicKey publicKey = PGPFileUtils.readPublicKey(encryptinfo.getPublicKeyRingFile());
                            PGPFileUtils.encryptFile(bos, pgpTempFile, publicKey, true, true);
                        }

                        logger.debug("FileAdapter wrote file to: " + processFilename);

                        // Check sftp connection and upload file if needed
                        if ( sftpinfo != null )
                        {
                            Date now = new Date();
                            String remoteFile = directory + "/" + filename;
                            logger.info("Need to write remote file to " + sftpinfo.getHost() + ": " + remoteFile);

                            SSHFileTransfer.WriteMode putType;
                            switch (otype)
                            {
                                case append:
                                    putType = SSHFileTransfer.WriteMode.append;
                                    break;
                                default:
                                    putType = SSHFileTransfer.WriteMode.overwrite;
                            }

                            long read = sftp.put(actualFile, remoteFile, putType, BYTE_INTERVAL);
                            logger.info("Wrote " + read +  " bytes from file '" + remoteFile + "' on remote server '" + sftpinfo.getHost() + "' took " + (new Date().getTime()-now.getTime()) + " ms");
                        }
                    }
                    else
                    {
                        if ( copyFiles )
                        {
                            int count = data.rowCount();
                            Iterator<Map<String,DataCell>> it = data.getMappedData().iterator();
                            for ( int i=1; it.hasNext(); i++ )
                            {
                                data.setCurrentProcessedRow(i);
                                System.out.println(getTaskName() + ": copying file #" + i + " out of " + count);
                                Map<String,DataCell> rowdata = it.next();
                                logger.debug("Reading file #" + i + ": " + rowdata);

                                String localfile = extractPropertyValue(AdapterProperty.localFile, rowdata, String.class, false);
                                String targetname = extractPropertyValue(AdapterProperty.targetFilename, rowdata, String.class, true);
                                actualFile = new File(localfile);
                                if ( targetname == null )
                                {
                                    targetname = actualFile.getName();
                                }

                                // If PGP encryption, we need to encrypt here
                                if ( encryptinfo != null && encryptinfo.getEncryptionType() == EncryptionObject.Type.PGP )
                                {
                                    pgpTempFile = File.createTempFile("pgp", "txt");
                                    pgpTempFile.deleteOnExit();
                                    BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(actualFile));
                                    PGPPublicKey publicKey = PGPFileUtils.readPublicKey(encryptinfo.getPublicKeyRingFile());
                                    PGPFileUtils.encryptFile(bos, pgpTempFile, publicKey, true, true);
                                    actualFile = pgpTempFile;
                                    logger.debug("FileAdapter encrypted file to: " + actualFile.getCanonicalPath());
                                }

                                // Check sftp connection and upload file if needed
                                if ( sftpinfo != null )
                                {
                                    Date now = new Date();
                                    String remoteFile = directory + "/" + targetname;
                                    filename = targetname;
                                    logger.info("Need to write remote file to " + sftpinfo.getHost() + ": " + remoteFile);

                                    SSHFileTransfer.WriteMode putType;
                                    switch (otype)
                                    {
                                        case append:
                                            putType = SSHFileTransfer.WriteMode.append;
                                            break;
                                        default:
                                            putType = SSHFileTransfer.WriteMode.overwrite;
                                    }

                                    long read = sftp.put(actualFile, remoteFile, putType, BYTE_INTERVAL);
                                    logger.info("Wrote " + read +  " bytes from file '" + actualFile.getCanonicalPath() + "' on remote server '" + sftpinfo.getHost() + "' took " + (new Date().getTime()-now.getTime()) + " ms");
                                }
                                else
                                {
                                    setTargetFileInfo(directory, targetname, origRoot);
                                    if ( actualFile.renameTo(copyFile) )
                                    {
                                        logger.info("Input file " + actualFile.getCanonicalPath() + " moved to: " + copyFile.getCanonicalPath());
                                    }
                                    else
                                    {
                                        logger.warn("Unable to move input file " + actualFile.getCanonicalPath() + " to: " + copyFile.getCanonicalPath());
                                    }
                                }

                                // Move file to input directory if specified
                                archiveFileInput(jobnum, actualFile);
                            }
                        }
                        else
                        {
                            logger.info("Outputing data to multiple files");
                            
                            // If output all is set, output all the columns defined by the data container
                            if ( outputAll )
                            {
                                switch (tftype)
                                {
                                    case delimitted:
                                        ((DelimittedFormat)format).resetFields();
                                        for ( DataColumn col: data.getColumnDefinitions().values() )
                                        {
                                            ((DelimittedFormat)format).addField(new DelimittedFormat.Field(col.getColumnName(), -1, true, false, String.valueOf(DataTypes.STRING)));
                                            data.addColumnDefinition(DataTypes.STRING, col.getColumnName().trim(), null);
                                        }
                                        break;
                                    case fixed:

                                        // TODO: add support for fixed width file formats
                                        /*((FixedWidthFormat)format).resetFields();
                                        for ( DataColumn col: data.getColumnDefinitions().values() )
                                        {
                                            ((FixedWidthFormat)format).addField(new FixedWidthFormat.Field(name, idx);
                                        }*/
                                        break;
                                    case regex:
                                        throw new ETLException("Cannot use regex file type as a file output");
                                }
                            }
                            
                            // Read data and put into buckets of data for each file
                            HashMap<File,List<Map<String,DataCell>>> files = new LinkedHashMap();
                            if ( data.rowCount() > 0 )
                            {
                                Iterator<Map<String,DataCell>> it = data.getMappedData().iterator();
                                for ( int i=0; it.hasNext(); i++ )
                                {
                                    data.setCurrentProcessedRow(i+1);
                                    //System.out.println(getTaskName() + ": Writing file #" + (i+1) + " out of " + data.rowCount());
                                    Map<String,DataCell> rowdata = it.next();
                                    logger.debug("Rowdata: " + rowdata);
                                    String dir = extractPropertyValue(AdapterProperty.directory, rowdata, String.class, true);
                                    String fn = extractPropertyValue(AdapterProperty.filename, rowdata, String.class, false);
                                    getFileForWriting(dir, fn, jobnum);

                                    // If job number is to be appended to the filename, update the file here
                                    if ( appendJobnum )
                                    {
                                        updateFileWithJobNumber(jobnum);
                                    }
                                    
                                    List<Map<String,DataCell>> bucket = new ArrayList();
                                    if ( files.containsKey(file) )
                                    {
                                        bucket = files.get(file);
                                    }
                                    else
                                    {
                                        files.put(file, bucket);
                                    }
                                    bucket.add(rowdata);
                                }
                            }
                            
                            int fnum = 0;
                            for ( Map.Entry<File,List<Map<String,DataCell>>> entry: files.entrySet() )
                            {
                                fnum++;
                                File f = entry.getKey();
                                List<Map<String,DataCell>> data = entry.getValue();
                                BufferedWriter writer = new BufferedWriter(new FileWriter(f.getAbsolutePath(), otype == OutputType.append));
                                
                                // If the output type is replace and useHeader is set, output the header.
                                // Or if output type is append and the file doesn't exist, output the header
                                if ( (useHeader && otype == OutputType.replace) || (useHeader && !f.exists()) )
                                {
                                    // Write the header to the file
                                    ArrayList<String> values = new ArrayList();
                                    for ( TextFormat.Field field: format.getSimpleFields() )
                                    {
                                        values.add(field.getName());
                                    }

                                    try
                                    {
                                        String line = format.formatLine(values);
                                        writeLineToWriter(writer, line, false);
                                    }
                                    catch (TextFormatException e)
                                    {
                                        throw new ETLException("Unable to format header data to output: " + values, e);
                                    }
                                }
                                
                                logger.info("Writing to file: " + f.getCanonicalPath());
                                logger.info("Formatting output using text format: " + format);
                                int count = data.size();
                                Iterator<Map<String,DataCell>> it = data.iterator();
                                for ( int i=0; it.hasNext(); i++ )
                                {
                                    Map<String,DataCell> row = it.next();
                                    if ( getTaskName() != null )
                                    {
                                        System.out.println(getTaskName() + ": Writing row #" + (i+1) + " out of " + count + " to file #" + fnum);
                                    }

                                    try
                                    {
                                        // This extracts the necessary values within the adapter and is able
                                        // to handle constants appropriately.
                                        ArrayList<String> values = new ArrayList();
                                        for ( TextFormat.Field field: format.getSimpleFields() )
                                        {
                                            // If field is constant add the name of the field as the value, performing
                                            // any embedded bean script global variable lookups, and converting to the defined datatype.
                                            if ( field.isConstant() )
                                            {
                                                DataColumn dc = new DataColumn(DataTypes.valueOf(field.getDataType()), null);
                                                DataCell cell = new DataCell(dc, wrapper.getDataArchiver().processEmbeddedBeanScript(field.getName()));
                                                String value = "";
                                                if ( cell.getValue() != null && cell.getValue() instanceof BinaryObject )
                                                {
                                                    value = String.valueOf(RadixConversion.bytesToChars(((BinaryObject)cell.getValue()).getData()));
                                                }
                                                else if ( cell.getValue() != null )
                                                {
                                                    value = String.valueOf(cell.getValue());
                                                }

                                                logger.trace("Assigning constant value to " + field + ": " + cell + " -> '" + value + "'");
                                                values.add(value);
                                            }
                                            // Otherwise, get the value of the data container with the name of the field
                                            else
                                            {
                                                String val = null;
                                                DataCell cell = row.get(field.getName());
                                                if ( cell != null && cell.getValue() != null )
                                                {
                                                    if ( cell.getValue() instanceof Date )
                                                    {
                                                        val = wrapper.dateOutputFormat.format(cell.getValue());
                                                    }
                                                    else
                                                    {
                                                        val = String.valueOf(cell.getValue());
                                                    }
                                                }

                                                values.add(val);
                                            }
                                        }

                                        //String line = format.formatLine(map);
                                        String line = format.formatLine(values);
                                        writeLineToWriter(writer, line, !it.hasNext());
                                    }
                                    catch (TextFormatException e)
                                    {
                                        // Add to errors
                                        String error = "Failed to format data to output: " + e.getMessage();
                                        logger.warn(error + " | Rowdata: " + row, e);
                                        it.remove();
                                        row.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, error));
                                        errors.addDataRow(row);
                                    }
                                }

                                writer.flush();
                                writer.close();
                            }
                        }
                    }
                }
                else
                {
                    // If there is nothing to write ensure column definitions are still updated so any change filters using this output task will have a consistent
                    // column definition.
                    // If output all is set, output all the columns defined by the data container
                    if ( outputAll )
                    {
                        switch (tftype)
                        {
                            case delimitted:
                                ((DelimittedFormat)format).resetFields();
                                for ( DataColumn col: data.getColumnDefinitions().values() )
                                {
                                    ((DelimittedFormat)format).addField(new DelimittedFormat.Field(col.getColumnName(), -1, true, false, String.valueOf(DataTypes.STRING)));
                                    data.addColumnDefinition(DataTypes.STRING, col.getColumnName().trim(), null);
                                }
                                break;
                            case fixed:

                                // TODO: add support for fixed width file formats
                                /*((FixedWidthFormat)format).resetFields();
                                for ( DataColumn col: data.getColumnDefinitions().values() )
                                {
                                    ((FixedWidthFormat)format).addField(new FixedWidthFormat.Field(name, idx);
                                }*/
                                break;
                            case regex:
                                throw new ETLException("Cannot use regex file type as a file output");
                        }
                    }
                }
            }
            catch (PGPException e)
            {
                throw new ETLException("Failed to encrypt outgoing file using PGP: " + processFilename, e.getUnderlyingException());
            }
            catch (GeneralSecurityException e)
            {
                throw new ETLException("Unable to encrypt outgoing file: " + processFilename + ", of cipher type: " + encryptinfo.getEncryptionTypeString(), e);
            }
            catch (JSchException e)
            {
                String error = "Failure writing remote file: " + directory + "/" + filename;
                logger.warn(error, e);
                for ( Map<String,DataCell> rowdata: data.getMappedData() )
                {
                    rowdata.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, error + ": " + e.getMessage()));
                    errors.addDataRow(rowdata);
                }

                data.clearData();
            }
            catch (IOException e)
            {
                String error = "Failure writing file: " + processFilename;
                logger.warn(error, e);
                for ( Map<String,DataCell> rowdata: data.getMappedData() )
                {
                    rowdata.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, error + ": " + e.getMessage()));
                    errors.addDataRow(rowdata);
                }

                data.clearData();
            }
            finally
            {
                if ( sftp != null )
                {
                    sftp.disconnect();
                }
            }
        }

        return errors;
    }

    private List<File> getFilesForReading(String jobnum)
        throws IOException, ETLException
    {
        ArrayList<File> files = new ArrayList();
        logger.debug("Arguments: " + arguments);
        
        if ( processMultiple )
        {
            if ( sftpinfo == null )
            {
                DataArchiver arch = wrapper.getDataArchiver();
                String filename = arch.processEmbeddedBeanScript(remoteFilename);
                WildcardFilter filter = new WildcardFilter(filename, ignoreCase);

                File[] found = fileRoot.listFiles(filter);
                logger.info("Looking for files matching " + filename + " at: " + fileRoot);
                if ( found == null || found.length == 0 )
                {
                    logger.info("No files found matching wildcard: " + filename);
                }
                else
                {
                    logger.info("Found " + found.length + " files matching wildcard: " + filename);
                    
                    // Sort files by sort type
                    if ( fileSort != null )
                    {
                        switch (fileSort)
                        {
                            case filename:
                                logger.info("Sorting file list by filename");
                                java.util.Arrays.sort(found);
                                break;
                            case timestamp:
                                logger.info("Sorting file list by timestamp");
                                Map<Long,File> filemap = new HashMap();
                                long[] timestamps = new long[found.length];
                                for ( int i=0; i<found.length; i++ )
                                {
                                    timestamps[i] = found[i].lastModified();
                                    filemap.put(timestamps[i], found[i]);
                                }

                                logger.debug(filemap);
                                java.util.Arrays.sort(timestamps);
                                for ( int i=0; i<found.length; i++ )
                                {
                                    found[i] = filemap.get(timestamps[i]);
                                }
                                break;
                        }
                    }

                    // Add files to file list
                    for ( File f: found )
                    {
                        logger.info("File " + f.getCanonicalPath() + " found");
                        files.add(f);

                        // If limitCount defined only add files until the limitCount is reached
                        if ( limitCount != null && files.size() >= limitCount && found.length > limitCount )
                        {
                            logger.info("File list is larger than limitCount " + limitCount + ", ignoring remaining");
                            break;
                        }
                    }
                }
            }
            else
            {
                SSHFileTransfer sftp = null;

                try
                {
                    File f = null;
                    Date now = new Date();
                    DataArchiver arch = wrapper.getDataArchiver();
                    String path = arch.processEmbeddedBeanScript(remotePath);
                    String filename = arch.processEmbeddedBeanScript(remoteFilename);
                    File input = new File(arch.getRunningArchiveDirectory(jobnum), Processor.INPUT_DIR + "/");
                    input.mkdirs();

                    if (ignoreCase == true)
                    {
                        logger.warn("CANNOT ignore case on SFTP wildcard searches");
                    }
                    
                    f = new File(input, filename);
                    String src = path + "/" + filename;
                    logger.info("Need to extract remote files from " + sftpinfo.getHost() + ": " + src);
                    sftp = new SSHFileTransfer(sftpinfo);
                    List<ChannelSftp.LsEntry> filenames = sftp.getFiles(path + "/" + filename);
                    if ( filenames.isEmpty() )
                    {
                        logger.info("No files found matching wildcard: " + filename);
                    }
                    else
                    {
                        logger.info("Found " + filenames.size() + " files matching wildcard: " + filename);
                        for ( ChannelSftp.LsEntry entry: filenames )
                        {
                            String fn = entry.getFilename();
                            f = new File(input, fn);
                            src = path + "/" + fn;
                            SFTPReadFile rf = sftp.get(src, f.getCanonicalPath(), BYTE_INTERVAL);
                            serverFileMap.put(f, src);
                            logger.info("Read " + rf.getBytesRead() +  " bytes from file '" + path + "' on remote server '" + sftpinfo.getHost() + "' took " + (new Date().getTime()-now.getTime()) + " ms");
                            files.add(f);
                            
                            // If limitCount defined only add files until the limitCount is reached
                            if ( limitCount != null && files.size() >= limitCount && filenames.size() > limitCount )
                            {
                                logger.info("File list is larger than limitCount " + limitCount + ", ignoring remaining");
                                break;
                            }
                            
                        }
                    }
                }
                catch (JSchException e)
                {
                    throw new IOException(e);
                }
                finally
                {
                    if ( sftp != null )
                    {
                        sftp.disconnect();
                    }
                }
            }
        }
        else
        {
            if ( sftpinfo == null )
            {
                if ( arguments.containsKey(AdapterProperty.filename) )
                {
                    logger.info("Extracting files to read from source data: " + sourceFiles);
                    for ( int i=0; i<sourceFiles.rowCount(); i++ )
                    {
                        Map<String,DataCell> rowdata = sourceFiles.getMappedData().get(i);
                        logger.debug("Looking up files from row #" + (i+1) + rowdata);
                        String filename = extractPropertyValue(AdapterProperty.filename, rowdata, String.class, false);
                        String directory = null;
                        
                        // Cannot use setFileInfo since that handles file roots automatically. You must be
                        // able to declare the absolute file path using filename plus directory.
                        if ( arguments.containsKey(AdapterProperty.directory) )
                        {
                            directory = extractPropertyValue(AdapterProperty.directory, rowdata, String.class, true);
                            file = new File(directory, filename);
                        }
                        else
                        {
                            file = new File(filename);
                        }

                        files.add(file);
                    }
                }
                else if ( file != null )
                {
                    
                    files.add(file);
                }
            }
            else
            {
                SSHFileTransfer sftp = null;

                try
                {
                    File f = null;
                    Date now = new Date();
                    DataArchiver arch = wrapper.getDataArchiver();
                    String path = arch.processEmbeddedBeanScript(remotePath);
                    String filename = arch.processEmbeddedBeanScript(remoteFilename);
                    File input = new File(arch.getRunningArchiveDirectory(jobnum), Processor.INPUT_DIR + "/");
                    input.mkdirs();

                    f = new File(input, filename);
                    String src = path + "/" + filename;
                    logger.info("Need to extract remote file from " + sftpinfo.getHost() + ": " + src);
                    sftp = new SSHFileTransfer(sftpinfo);
                    SFTPReadFile rf = sftp.get(src, f.getCanonicalPath(), BYTE_INTERVAL);
                    serverFileMap.put(f, src);
                    logger.info("Read " + rf.getBytesRead() +  " bytes from file '" + path + "' on remote server '" + sftpinfo.getHost() + "' took " + (new Date().getTime()-now.getTime()) + " ms");
                    files.add(f);
                }
                catch (JSchException e)
                {
                    throw new IOException(e);
                }
                finally
                {
                    if ( sftp != null )
                    {
                        sftp.disconnect();
                    }
                }
            }
        }
        
        logger.debug("ServerFileMap: " + serverFileMap);
        logger.debug("Files to read: " + files);
        
        return files;
    }
    
    private File getFileForWriting(String jobnum)
        throws IOException, ETLException
    {
        File f = null;
        if ( sftpinfo == null )
        {
            f = file;
        }
        else
        {
            f = File.createTempFile(getTaskName() + "-" + jobnum, Processor.TXT_EXT);
            f.deleteOnExit();
        }
        
        logger.debug("File for writing: " + f);
        return f;
    }

    private File getFileForWriting(String dir, String filename, String jobnum)
        throws IOException, ETLException
    {
        setFileInfo(dir, filename, null, origRoot);
        logger.debug("File for writing: " + file);
        return file;
    }

    private void setFileInfo(FileInputTaskComplexType.File info)
        throws ETLException
    {
        if ( info != null )
        {
            if ( info.getFilenameColumn() == null ) //|| info.getDirectoryColumn() == null )
            {
                directory = info.getDirectory();
                filename = info.getFilename();
                String f = info.getFile();
                setFileInfo(directory, filename, f, origRoot);
                logger.info("FileAdapter input file: " + file);
            }
            else if ( info.getFile() == null )
            {
                setFileColumns(info);
                setFileInfo(null, null, null, origRoot);
                logger.info("FileAdapter uses input files declared in source data: " + arguments);
            }
        }
    }

    private void setFileInfo(MultipleFileInputTaskComplexType.File info)
        throws ETLException
    {
        if ( info != null )
        {
            logger.debug("directory attribute=" + info.getDirectory());
            directory = info.getDirectory();
            filename = info.getWildcardFilename();
            ignoreCase = info.isIgnoreCase();
            limitCount = info.getLimitCount();
            setFileSort(info.getSort());
            setFileInfo(directory, filename, null, origRoot);
            logger.info("FileAdapter input file: " + file);
        }
    }

    private void setFileInfo(FileOutputTaskComplexType.File info, boolean binary)
        throws ETLException
    {
        if ( info != null )
        {
            if ( binary )
            {
                binaryFileRoot = info.getDirectory();
                setOutputType(info.getOutputType());
                overwrite = info.isOverwrite();
                zipBinary = info.isZipBinaryFiles();
                logger.info("FileAdapter output root: " + binaryFileRoot);
                if ( zipBinary )
                {
                    binaryZipFile = info.getFilename();
                }
            }
            else
            {
                directory = info.getDirectory();
                filename = info.getFilename();
                String file = info.getFile();
                setFileInfo(directory, filename, file, origRoot);
                setOutputType(info.getOutputType());
                overwrite = info.isOverwrite();
                appendJobnum = info.isAppendJobNumber();
                createEmpty = info.isCreateEmptyFile();
                jobnumSeparator = info.getJobNumberSeparator();
                logger.info("FileAdapter output file: " + file + (appendJobnum?jobnumSeparator + "(jobnumber)":""));
            }
        }
    }

    private void setFileInfo(MultipleFileOutputTaskComplexType.File info)
        throws ETLException
    {
        if ( info != null )
        {
            directory = info.getDirectory();
            setFileInfo(directory, "null", null, origRoot);
            setOutputType(info.getOutputType());
            overwrite = info.isOverwrite();
            appendJobnum = info.isAppendJobNumber();
            createEmpty = info.isCreateEmptyFile();
            jobnumSeparator = info.getJobNumberSeparator();
            logger.info("FileAdapter output file: " + file + (appendJobnum?jobnumSeparator + "(jobnumber)":""));
        }
    }

    private void setFileInfo(String dir, String filename, String f, File defaultRoot)
        throws ETLException
    {
        logger.debug("Comparing file root: " + dir);
        
        // Change here: if relative directory use etlRoot + directory
        // if absolute directory, replace etlRoot with directory
        
        if ( f != null && f.trim().length() > 0 )
        {
            // File could be declared from a global variable so to ensure the file path is setup properly we have to process the script here
            if ( wrapper.getDataArchiver().containsBeanScript(f) )
            {
                f = wrapper.getDataArchiver().processEmbeddedBeanScript(f);
            }
            
            Matcher m = windowsAbsoluteSlashFile.matcher(f);
            Matcher m2 = windowsAbsoluteFile.matcher(f);
            Matcher m3 = windowsRelativeFile.matcher(f);
            Matcher m4 = windowsNoDirFile.matcher(f);
            
            logger.debug("Identifying file path: " + f);
            logger.debug("Regex1: " + windowsAbsoluteSlashFile.pattern());
            logger.debug("Regex2: " + windowsAbsoluteFile.pattern());
            
            if ( m.matches() )
            {
                logger.info("FileAdapter file uses absolute directory with extra backslash and filename");
                dir = m.group(1);
                filename = m.group(2);
            }
            else if ( m2.matches() )
            {
                logger.info("FileAdapter file uses absolute directory and filename");
                dir = m2.group(1);
                filename = m2.group(2);
            }
            else if ( m3.matches() )
            {
                logger.info("FileAdapter file uses relative directory and filename");
                dir = m3.group(1);
                filename = m3.group(2);
            }
            else if ( m4.matches() )
            {
                logger.info("FileAdapter file does not contain path so only using filename");
                filename = m4.group(1);
            }
            else
            {
                throw new ETLException("Format for attribute file is incorrect. Format requires both a path (absolute or relative) and a filename.");
            }

            if ( !wrapper.getDataArchiver().containsBeanScript(fileRoot.getAbsolutePath()) &&
                 sftpinfo == null && !fileRoot.exists() )
            {
                logger.info("Local file root doesn't exist, making directories: " + fileRoot.getAbsolutePath());
                fileRoot.mkdirs();
            }
        }
                
        if ( dir != null && dir.trim().length() > 0 )
        {
            Matcher m = windowsAbsoluteSlashDir.matcher(dir);
            if ( m.matches() )
            {
                logger.info("FileAdapter file uses absolute directory with extra backslash");
                fileRoot = new File(m.group(1) + ":" + m.group(2));
            }
            else if ( dir.matches(windowsAbsoluteDir.pattern()) ||
                      dir.matches(unixAbsoluteDir.pattern()) )
            {
                logger.info("FileAdapter file uses absolute directory");
                fileRoot = new File(dir);
            }
            else
            {
                logger.info("FileAdapter file uses relative directory");
                fileRoot = new File(defaultRoot, dir);
            }

            if ( !wrapper.getDataArchiver().containsBeanScript(fileRoot.getAbsolutePath()) &&
                 sftpinfo == null && !fileRoot.exists() )
            {
                logger.info("Local file root doesn't exist, making directories: " + fileRoot.getAbsolutePath());
                fileRoot.mkdirs();
            }
        }
        else
        {
            fileRoot = origRoot;
        }

        remotePath = dir;// + "/" + filename;
        remoteFilename = filename;
        
        try
        {
            if ( filename != null )
            {
                file = new File(fileRoot.getCanonicalFile(), filename);
            }
        }
        catch (IOException e)
        {
            throw new ETLException("Unable to access ETL root", e);
        }

        logger.debug("Prepared file: " + file);
    }

    private void setTargetFileInfo(String dir, String filename, File defaultRoot)
        throws ETLException
    {
        logger.debug("Comparing target file root: " + dir);
        
        // Change here: if relative directory use etlRoot + directory
        // if absolute directory, replace etlRoot with directory
        
        if ( dir != null && dir.trim().length() > 0 )
        {
            Matcher m = windowsAbsoluteSlashDir.matcher(dir);
            if ( m.matches() )
            {
                logger.info("FileAdapter target file uses absolute directory with extra backslash");
                copyRoot = new File(m.group(1) + ":" + m.group(2));
            }
            else if ( dir.matches(windowsAbsoluteDir.pattern()) ||
                      dir.matches(unixAbsoluteDir.pattern()) )
            {
                logger.info("FileAdapter target file uses absolute directory");
                copyRoot = new File(dir);
            }
            else
            {
                logger.info("FileAdapter target file uses relative directory");
                copyRoot = new File(defaultRoot, dir);
            }

            if ( !wrapper.getDataArchiver().containsBeanScript(copyRoot.getAbsolutePath()) &&
                 sftpinfo == null && !copyRoot.exists() )
            {
                logger.info("Local target file root doesn't exist, making directories: " + copyRoot.getAbsolutePath());
                copyRoot.mkdirs();
            }
        }
        else
        {
            copyRoot = origRoot;
        }

        try
        {
            if ( filename != null )
            {
                copyFile = new File(copyRoot.getCanonicalFile(), filename);
            }
        }
        catch (IOException e)
        {
            throw new ETLException("Unable to access ETL root", e);
        }
    }

    private void updateFileWithGlobalVariableAssignment()
        throws ETLException
    {
        if ( wrapper != null )
        {
            DataArchiver arch = wrapper.getDataArchiver();
            directory = arch.processEmbeddedBeanScript(directory);
            filename = arch.processEmbeddedBeanScript(filename);
            logger.info("File directory with updated global variable: " + directory);
            logger.info("File name updated with global variable: " + filename);
            setFileInfo(directory, filename, null, origRoot);
        }
    }
    
    private void updateCopyFileWithGlobalVariableAssignment()
        throws ETLException
    {
        if ( wrapper != null )
        {
            DataArchiver arch = wrapper.getDataArchiver();
            copyDirectory = arch.processEmbeddedBeanScript(copyDirectory);
            copyFilename = arch.processEmbeddedBeanScript(copyFilename);
            logger.info("Target directory with updated global variable: " + copyDirectory);
            logger.info("Target file name updated with global variable: " + copyFilename);
            setTargetFileInfo(copyDirectory, copyFilename, origRoot);
        }
    }

    private void updateBinaryFileRootWithGlobalVariableAssignment()
        throws ETLException
    {
        if ( wrapper != null )
        {
            DataArchiver arch = wrapper.getDataArchiver();
            setFileInfo(arch.processEmbeddedBeanScript(binaryFileRoot), arch.processEmbeddedBeanScript(binaryZipFile), null, origRoot);
        }
    }
    
    private void updateFileWithJobNumber(String jobnum)
    {
        if ( file != null )
        {
            File dir = file.getParentFile();
            int idx = file.getName().lastIndexOf(".");
            String prefix = file.getName().substring(0, idx);
            String ext = file.getName().substring(idx);

            file = new File(dir, prefix + jobnumSeparator + jobnum + ext);
        }
    }
    
    private void setFileCopy(FileInputTaskComplexType.CopyFile copy)
        throws ETLException
    {
        if ( copy != null )
        {
            copyFilename = copy.getTargetFilename();
            copyDirectory = copy.getTargetDirectory();
            retainFilename = copy.getTargetFilename() == null && copy.isRetainFilename();
        }
    }

    private void setFileCopy(MultipleFileInputTaskComplexType.CopyFile copy)
        throws ETLException
    {
        if ( copy != null )
        {
            copyFilename = copy.getTargetFilename();
            copyDirectory = copy.getTargetDirectory();
            retainFilename = copy.getTargetFilename() == null && copy.isRetainFilename();
        }
    }

    private void setFileCopy(FileOutputTaskComplexType.CopyFile copy)
        throws ETLException
    {
        if ( copy != null )
        {
            copyFiles = true;
            copyFilename = copy.getSourceFilename();
            copyDirectory = copy.getSourceDirectory();
        }
    }

    private void setFileCopy(MultipleFileOutputTaskComplexType.CopyFiles copy)
        throws ETLException
    {
        if ( copy != null )
        {
            copyFiles = true;
            arguments.clear();
            arguments.put(AdapterProperty.localFile, new Argument(copy.getLocalFile(), this));
            arguments.put(AdapterProperty.targetFilename, new Argument(copy.getTargetFilename(), this));
        }
    }
    
    private void setFileColumns(FileInputTaskComplexType.File file)
        throws ETLException
    {
        arguments.clear();
        if ( file != null )
        {
            if ( file.getFilenameColumn() != null )
            {
                arguments.put(AdapterProperty.filename, new Argument(file.getFilenameColumn(), this));
            }
            if ( file.getDirectoryColumn() != null )
            {
                arguments.put(AdapterProperty.directory, new Argument(file.getDirectoryColumn(), this));
            }
        }
    }

    private void setInputData(FileInputTaskComplexType.InputData input)
        throws ETLException
    {
        if ( input != null )
        {
            setTextFormatType(input.getType());
            setFixedWidthIndexingType(input.getIndexing());
            useHeader = input.isUseHeaderNames();
            ignoreStartsWith = input.getIgnoreRowsStartingWith();
            ignoreRows = input.getIgnoreRows();
            worksheet = input.getWorksheet();
            if ( ignoreRows == 0 && input.isIgnoreHeaders() )
            {
                ignoreRows = 1;
            }

            if ( useHeader )
            {
                switch (tftype)
                {
                    case delimitted:
                        char delim = getDelimiterChar(input.getDelimiter());
                        DelimittedFormat ft = new DelimittedFormat(delim);
                        ft.setRetainQuoted(input.isRetainQuoted());
                        format = ft;
                        break;
                    case fixed:
                        // TODO: support for setup of fixed width format from headers for file adapter
                        throw new ETLException("Fixed width file formats do not support headers currently");
                        //break;
                    case regex:
                        throw new ETLException("Regex file formats do not support headers currently");
                }
            }
            else
            {
                switch (tftype)
                {
                    case delimitted:
                        char delim = getDelimiterChar(input.getDelimiter());
                        DelimittedFormat dt = new DelimittedFormat(delim);
                        for ( FileInputTaskComplexType.InputData.DataElement dct: input.getDataElement() )
                        {
                            String n = dct.getValue();
                            String type = String.valueOf(DataTypes.STRING);
                            if ( dct.getType() != null )
                            {
                                type = String.valueOf(dct.getType());
                            }
                            
                            String def = dct.getDefaultValue();
                            boolean unique = dct.isPrimaryKey();
                            int len = dct.getMaxLength();
                            DelimittedFormat.Field f = new DelimittedFormat.Field(n, len, true, false, type);
                            dt.addField(f);
                            data.addColumnDefinition(type, n, unique, def);
                        }

                        format = dt;
                        break;
                    case fixed:
                        FixedWidthFormat ft = new FixedWidthFormat();
                        int mod = 0;
                        if ( idxtype == FixedWidthIndexing.one_based )
                        {
                            mod = -1;
                        }

                        FileInputTaskComplexType.InputData.DataElement last = null;
                        for ( FileInputTaskComplexType.InputData.DataElement dct: input.getDataElement() )
                        {
                            String n = dct.getValue();
                            String type = String.valueOf(DataTypes.STRING);
                            if ( dct.getType() != null )
                            {
                                type = String.valueOf(dct.getType());
                            }
                            
                            String def = dct.getDefaultValue();
                            boolean unique = dct.isPrimaryKey();
                            int idx = dct.getStartIndex() + mod;

                            // Cannot start with an index less than 0
                            if ( idx < 0 )
                            {
                                int start = 0;
                                if ( idxtype == FixedWidthIndexing.one_based )
                                {
                                    start = 1;
                                }
                                throw new ETLException("Fixed width index cannot be less than " + start + " for indexing type: " + idxtype);
                            }

                            // Ensure that if lengths are declared that any subsequent field's index is correct 
                            // compared to the previous field declaration
                            if ( last != null && last.getMaxLength() >= 0 && idx != (last.getStartIndex() + mod + last.getMaxLength()) )
                            {
                                throw new ETLException("Start index " + (idx-mod) + " does not match expected index declared by previous field's index and length: " + last.getStartIndex() + ", " + last.getMaxLength());
                            }
                            
                            FixedWidthFormat.Field f = new FixedWidthFormat.Field(n, idx, String.valueOf(Argument.Type.constant).equals(type), String.valueOf(DataTypes.STRING));
                            ft.addField(f);
                            last = dct;
                            data.addColumnDefinition(type, n, unique, def);
                        }

                        format = ft;
                        break;
                    case regex:
                        RegexFormat rt = new RegexFormat(input.getPattern());
                        for ( FileInputTaskComplexType.InputData.DataElement dct: input.getDataElement() )
                        {
                            String n = dct.getValue();
                            String type = String.valueOf(dct.getType());
                            String def = dct.getDefaultValue();
                            boolean unique = dct.isPrimaryKey();
                            rt.addField(n, String.valueOf(DataTypes.STRING));
                            data.addColumnDefinition(type, n, unique, def);
                        }

                        format = rt;
                        break;
                    case excel:
                        for ( FileInputTaskComplexType.InputData.DataElement dct: input.getDataElement() )
                        {
                            String n = dct.getValue();
                            String type = String.valueOf(DataTypes.STRING);
                            if ( dct.getType() != null )
                            {
                                type = String.valueOf(dct.getType());
                            }
                            
                            String def = dct.getDefaultValue();
                            boolean unique = dct.isPrimaryKey();
                            data.addColumnDefinition(type, n, unique, def);
                        }
                        break;
                }
            }

            if ( format != null )
            {
                logger.debug("TextFormat: " + format.toString());
            }
        }
    }

    private void setInputData(MultipleFileInputTaskComplexType.InputData input)
       throws ETLException
    {
        if ( input != null )
        {
            DataComplexType lfn = input.getLocalFilename();
            DataComplexType lf = input.getLocalFile();
            DataComplexType lfd = input.getLocalDirectory();
            DataComplexType sf = input.getServerFile();
            localFilenameColumn = new DataColumn(lfn.getType(), lfn.getValue(), lfn.getDefaultValue());
            localFileColumn = new DataColumn(lf.getType(), lf.getValue(), lf.getDefaultValue());
            data.addColumnDefinition(localFilenameColumn);
            data.addColumnDefinition(localFileColumn);
            
            if ( lfd != null )
            {
                localDirectoryColumn = new DataColumn(lfd.getType(), lfd.getValue(), lfd.getDefaultValue());
                data.addColumnDefinition(localDirectoryColumn);
                logger.info("Added localDirectoryColumn");
            }
            
            if ( sf != null )
            {
                serverFileColumn = new DataColumn(sf.getType(), sf.getValue(), sf.getDefaultValue());
                data.addColumnDefinition(serverFileColumn);
            }
            
        }
    }
    
    private void setOutput(FileOutputTaskComplexType ct)
        throws ETLException
    {
        if ( ct != null )
        {
            if ( ct.getOutputData() != null )
            {
                setOutputData(ct.getOutputData());
            }
            else if ( ct.getOutputBinary() != null )
            {
                setOutputBinary(ct.getOutputBinary());
            }
        }
    }

    private void setOutputBinary(FileOutputTaskComplexType.OutputBinary binary)
        throws ETLException
    {
        if ( binary != null )
        {
            outputBinary = true;
            binaryFilename = new Argument(binary.getFilename(), this);
            binaryData = new Argument(binary.getBinaryData(), this);
            
            if ( binaryFilename.getArgumentType() == Argument.Type.constant )
            {
                throw new ETLException("Binary outputs do not support constant filename declarations");
            }
            
            if ( binaryData.getArgumentType() == Argument.Type.constant )
            {
                throw new ETLException("Binary outputs do not support constant binary data declarations");
            }
        }
    }
    
    private void archiveFileInput(String jobnum, File file)
    {
        SSHFileTransfer sftp = null;
        
        try
        {
            if ( (archiveInput || removeRemote) && file.exists() )
            {
                DataArchiver arch = wrapper.getDataArchiver();
                if ( archiveInput )
                {
                    File inputDir = new File(arch.getRunningArchiveDirectory(jobnum), Processor.INPUT_DIR + "/");
                    inputDir.mkdirs();
                    if ( file.renameTo(new File(inputDir.getCanonicalFile(), file.getName())) )
                    {
                        logger.info("Input file " + file.getCanonicalPath() + " moved to: " + inputDir.getCanonicalPath());
                    }
                    else
                    {
                        logger.warn("Unable to move input file " + file.getCanonicalPath() + " to: " + inputDir.getCanonicalPath());
                    }
                }
                
                // If the file was retrieved from an external server and archived, remove the original
                if ( sftpinfo != null && removeRemote )
                {
                    sftp = new SSHFileTransfer(sftpinfo);
                    String src = serverFileMap.get(file);
                    logger.info("Attempting to remove remote file: " + src);
                    logger.debug("ServerFileMap: " + serverFileMap);
                    if ( sftp.rm(src) )
                    {
                        logger.warn("Successfully removed the orignal file from host '" + sftpinfo.getHost() + "' : " + src);
                    }
                    else
                    {
                        logger.warn("FAILED to remove the orignal file from host '" + sftpinfo.getHost() + "' : " + src);
                    }
                }
            }
            // Not sure why this was here. Basically if sftp is the destination, and archiveOriginalFiles was false, the local files would get deleted and no archived
            /*else if ( !archiveInput && sftpinfo != null )
            {
                file.deleteOnExit();
            }*/
            
        }
        catch (Exception e)
        {
            logger.warn("Unable to archive file input", e);
        }
        finally
        {
            if ( sftp != null )
            {
                sftp.disconnect();
            }
        }
    }
    
    private void setOutputData(FileOutputTaskComplexType.OutputData output)
        throws ETLException
    {
        if ( output != null )
        {
            setTextFormatType(output.getType());
            setFixedWidthIndexingType(output.getIndexing());
            setLinefeedFormat(output.getLinefeedFormat());
            useHeader = output.isWriteHeader();
            outputAll = output.isOutputAll();
            lastLinefeed = output.isLastRecordLinefeed();
            worksheet = output.getWorksheet();

            switch (tftype)
            {
                case delimitted:
                    char delim = getDelimiterChar(output.getDelimiter());
                    DelimittedFormat dt = new DelimittedFormat(delim);

                    if ( !output.isOutputAll() )
                    {
                        for ( FileOutputTaskComplexType.OutputData.DataElement dct: output.getDataElement() )
                        {
                            String n = dct.getValue();
                            int len = dct.getMaxLength();
                            String type = dct.getType();
                            DelimittedFormat.Field f = new DelimittedFormat.Field(n, len, true, false, String.valueOf(Argument.Type.constant).equals(type), String.valueOf(dct.getDataType()));
                            dt.addField(f);
                            data.addColumnDefinition(DataTypes.STRING, n, null);
                        }
                    }

                    format = dt;
                    break;
                case fixed:
                    FixedWidthFormat ft = new FixedWidthFormat();
                    if ( !output.isOutputAll() )
                    {
                        int mod = 0;
                        if ( idxtype == FixedWidthIndexing.one_based )
                        {
                            mod = -1;
                        }
                        
                        FileOutputTaskComplexType.OutputData.DataElement last = null;
                        for ( FileOutputTaskComplexType.OutputData.DataElement dct: output.getDataElement() )
                        {
                            String n = dct.getValue();
                            int idx = dct.getStartIndex() + mod;
                            String type = dct.getType();
                            String justify = dct.getJustify();
                            
                            // Cannot start with an index less than 0
                            if ( idx < 0 )
                            {
                                int start = 0;
                                if ( idxtype == FixedWidthIndexing.one_based )
                                {
                                    start = 1;
                                }
                                throw new ETLException("Fixed width index cannot be less than " + start + " for indexing type: " + idxtype);
                            }
                            
                            // Ensure that if lengths are declared that any subsequent field's index is correct 
                            // compared to the previous field declaration
                            if ( last != null && last.getMaxLength() >= 0 && idx != (last.getStartIndex() + mod + last.getMaxLength()) )
                            {
                                throw new ETLException("Start index " + (idx-mod) + " does not match expected index declared by previous field's index and length: " + last.getStartIndex() + ", " + last.getMaxLength());
                            }
                            
                            FixedWidthFormat.Field f = new FixedWidthFormat.Field(n, idx, String.valueOf(Argument.Type.constant).equals(type), String.valueOf(dct.getDataType()));
                            f.setJustification(justify);
                            ft.addField(f);
                            last = dct;
                            data.addColumnDefinition(DataTypes.STRING, n, null);
                        }
                        
                        // If a length is declared in the last field declaration an empty field must be
                        // added to ensure that the length of the fixed width format is correct
                        if ( last.getMaxLength() >= 0 )
                        {
                            ft.addField(new FixedWidthFormat.Field("", last.getStartIndex() + mod + last.getMaxLength(), true, String.valueOf(DataTypes.STRING)));
                        }
                    }

                    format = ft;
                    break;
                case regex:
                    throw new ETLException("Cannot use regex file type as a file output");
                case excel:
                    
                    dt = new DelimittedFormat('\t');

                    if ( !output.isOutputAll() )
                    {
                        for ( FileOutputTaskComplexType.OutputData.DataElement dct: output.getDataElement() )
                        {
                            String n = dct.getValue();
                            int len = dct.getMaxLength();
                            String type = String.valueOf(DataTypes.STRING);
                            if ( dct.getDataType()!= null )
                            {
                                type = String.valueOf(dct.getDataType());
                            }
                            
                            DelimittedFormat.Field f = new DelimittedFormat.Field(n, len, true, false, String.valueOf(Argument.Type.constant).equals(type), String.valueOf(dct.getDataType()));
                            dt.addField(f);
                            data.addColumnDefinition(type, n, null);
                        }
                    }
                    
                    format = dt;
                    break;
            }
        }
    }

    private void setOutputFiles(MultipleFileOutputTaskComplexType.OutputFiles files)
        throws ETLException
    {
        if ( files != null )
        {
            arguments.clear();
            arguments.put(AdapterProperty.directory, new Argument(files.getDirectory(), this));
            arguments.put(AdapterProperty.filename, new Argument(files.getFilename(), this));
        }
    }
    
    private void setOutputData(MultipleFileOutputTaskComplexType.OutputData output)
        throws ETLException
    {
        if ( output != null )
        {
            setTextFormatType(output.getType());
            setFixedWidthIndexingType(output.getIndexing());
            setLinefeedFormat(output.getLinefeedFormat());
            useHeader = output.isWriteHeader();
            outputAll = output.isOutputAll();
            lastLinefeed = output.isLastRecordLinefeed();
            
            switch (tftype)
            {
                case delimitted:
                    char delim = getDelimiterChar(output.getDelimiter());
                    DelimittedFormat dt = new DelimittedFormat(delim);

                    if ( !output.isOutputAll() )
                    {
                        for ( MultipleFileOutputTaskComplexType.OutputData.DataElement dct: output.getDataElement() )
                        {
                            String n = dct.getValue();
                            int len = dct.getMaxLength();
                            String type = dct.getType();
                            DelimittedFormat.Field f = new DelimittedFormat.Field(n, len, true, false, String.valueOf(Argument.Type.constant).equals(type), String.valueOf(dct.getDataType()));
                            dt.addField(f);
                        }
                    }

                    format = dt;
                    break;
                case fixed:
                    FixedWidthFormat ft = new FixedWidthFormat();
                    if ( !output.isOutputAll() )
                    {
                        int mod = 0;
                        if ( idxtype == FixedWidthIndexing.one_based )
                        {
                            mod = -1;
                        }
                        
                        MultipleFileOutputTaskComplexType.OutputData.DataElement last = null;
                        for ( MultipleFileOutputTaskComplexType.OutputData.DataElement dct: output.getDataElement() )
                        {
                            String n = dct.getValue();
                            int idx = dct.getStartIndex() + mod;
                            String type = dct.getType();
                            
                            // Cannot start with an index less than 0
                            if ( idx < 0 )
                            {
                                int start = 0;
                                if ( idxtype == FixedWidthIndexing.one_based )
                                {
                                    start = 1;
                                }
                                throw new ETLException("Fixed width index cannot be less than " + start + " for indexing type: " + idxtype);
                            }
                            
                            // Ensure that if lengths are declared that any subsequent field's index is correct 
                            // compared to the previous field declaration
                            if ( last != null && last.getMaxLength() >= 0 && idx != (last.getStartIndex() + mod + last.getMaxLength()) )
                            {
                                throw new ETLException("Start index " + (idx-mod) + " does not match expected index declared by previous field's index and length: " + last.getStartIndex() + ", " + last.getMaxLength());
                            }
                            
                            FixedWidthFormat.Field f = new FixedWidthFormat.Field(n, idx, String.valueOf(Argument.Type.constant).equals(type), String.valueOf(dct.getDataType()));
                            ft.addField(f);
                            last = dct;
                        }
                        
                        // If a length is declared in the last field declaration an empty field must be
                        // added to ensure that the length of the fixed width format is correct
                        if ( last.getMaxLength() >= 0 )
                        {
                            ft.addField(new FixedWidthFormat.Field("", last.getStartIndex() + mod + last.getMaxLength(), true, String.valueOf(DataTypes.STRING)));
                        }
                    }

                    format = ft;
                    break;
                case regex:
                    throw new ETLException("Cannot use regex file type as a file output");
            }
        }
    }

    private char getDelimiterChar(String d)
        throws ETLException
    {
        char delim = 0x40;
        if ( d == null || d.trim().length() == 0 || d.trim().length() > 2 )
        {
            throw new ETLException("Delimiter character must be a single charcter");
        }

        if ( d.trim().length() == 1 )
        {
            delim = d.charAt(0);
        }
        else
        {
            if ( d.trim().equals("\\t") )
            {
                delim = '\t';
            }
            else
            {
                throw new ETLException("Only escape characters supported as delimiters are: \\t");
            }
        }

        return delim;
    }

    private Cipher getCipher(int mode, EncryptionObject info)
        throws GeneralSecurityException
    {
        Cipher cipher = null;
        if ( info != null )
        {
            switch (info.getEncryptionType())
            {
                case AES:
                    cipher = BbKeyGenerator.getAESCipher(mode, info.getEncryptionHexKey());
                    break;
                case DES3:
                    cipher = BbKeyGenerator.getDES3Cipher(mode, info.getEncryptionHexKey());
                    break;
                case Blowfish:
                    cipher = BbKeyGenerator.getBlowfishCipher(mode, info.getEncryptionHexKey());
                    break;
                case PGP:
                    break;
                default:
                    throw new UnsupportedOperationException("No support for cipher type currently exists: " + info.getEncryptionTypeString());
            }
        }
        else
        {
            throw new GeneralSecurityException("Cannot encrypt/decrypt with null encryption object");
        }
        
        return cipher;
    }
    
    /*private void setColumnDefinitions(List<DataComplexType> elements)
        throws ETLException
    {
        for ( DataComplexType ct: elements )
        {
            data.addColumnDefinition(String.valueOf(ct.getType()), ct.getValue(), ct.getSchema());
        }
    }*/
    
    public static boolean isReaderReady(Reader reader)
        throws IOException
    {
        boolean ready = false;
        
        // Mark the current position in case we actually read data and not the end of file
        reader.mark(1);
        int i = reader.read();
        ready = i >= 0;
        
        // If the reader still contains data reset to the previously marked position so it can
        // be read properly in subsequent reads
        if ( ready )
        {
            reader.reset();
        }
        
        return ready;
    }
    
    public static String readLineFromReader(Reader reader)
        throws IOException
    {
        String line = "";

        // If the reader has data to read keep reading until a LF is found
        if ( isReaderReady(reader) )
        {
            int i = reader.read();
            while ( i >= 0 )
            {
                byte b = (byte)i;
                char c = RadixConversion.byteToChar(b);

                // If the read character is a LF check to see if the next character is another
                // LF since CR+LF characters are often written to denote EOL
                if ( c == '\n' || c == '\r' )
                {
                    reader.mark(1);
                    i = reader.read();
                    b = (byte)i;
                    c = RadixConversion.byteToChar(b);
                    
                    // If the next character is not a LF then reset to the previously marked position
                    // since the read ahead data is needed
                    if ( c != '\n' )
                    {
                        reader.reset();
                    }

                    break;
                }
                else
                {
                    line += c;
                }

                i = reader.read();
            }
        }
        // Otherwise, the line is null to indicate no more data to read.
        else
        {
            line = null;
        }

        return line;
    }

    private void writeLineToWriter(Writer writer, String line, boolean isLastRecord)
        throws IOException
    {
        writer.write(line);
        if ( !isLastRecord || lastLinefeed )
        {
            switch (linefeed)
            {
                case CRLF:
                    writer.write('\r');
                    writer.write('\n');
                    break;
                case CR:
                    writer.write('\r');
                    break;
                case LF:
                    writer.write('\n');
                    break;
            }
        }
    }
    
    private <E extends Object> E extractPropertyValue(AdapterProperty prop, Map<String,DataCell> map, Class<E> clazz, boolean allowNull)
        throws ETLException
    {
        E val = null;
        Object o = null;
        String argname = "";
        Argument arg = arguments.get(prop);
        if ( arg != null )
        {
            if ( arg.getArgumentType() == Argument.Type.column )
            {
                argname = String.valueOf(arg.getValue());
                DataCell cell = map.get(argname);
                if ( cell != null )
                {
                    o = cell.getValue();
                }
                else if ( data.getColumnDefinition(argname) != null )
                {
                    o = null;
                }
                else
                {
                    throw new ETLException("Undefined column " + argname + ", unable to extract required value from source data.");
                }

                if ( !allowNull && o == null )
                {
                    throw new RowLevelException("Column " + argname + " value is null and null values are not allowed for " + prop + " arguments.");
                }
                else if ( val != o && !(clazz.isAssignableFrom(o.getClass())) )
                {
                    throw new RowLevelException("Column data referenced by " + argname + " is not an instanceof BinaryObject. Unable to output binary data to a file.");
                }
                else
                {
                    val = (E)o;
                }
            }
            else if ( arg.getArgumentType() == Argument.Type.constant )
            {
                val = (E)arg.getValue();
            }
            else
            {
                throw new ETLException(argname + " must be a valid column reference in source data. List and output argument types are not supported.");
            }
        }
        else
        {
            throw new ETLException("No arguments defined with property name: " + prop);
        }
        
        return val;
    }

    public String toString()
    {
        StringBuffer out = new StringBuffer();
        out.append("{");
        out.append("TaskName=" + name);
        out.append(", NextTask=" + next);
        out.append(", AdapterType=" + getAdapterType());
        out.append(", Direction=" + direction);
        out.append(", File=" + file);
        out.append("}");
        return out.toString();
    }
}
