/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.tasks;

import com.blackboard.services.etl.data.Argument;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.data.DataIndex;
import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.etl.data.TransformSource;
import com.blackboard.services.etl.data.TransformTarget;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.exception.FilterException;
import com.blackboard.services.etl.exception.TransformException;
import com.blackboard.services.etl.exception.TransformIgnoreException;
import com.blackboard.services.etl.exception.VariableTypeMismatchException;
import com.blackboard.services.etl.jaxb.CorrelateDataComplexType;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.EtlTaskComplexType;
import com.blackboard.services.etl.jaxb.TransformComplexType;
import com.blackboard.services.etl.transforms.AbstractTransform;
import com.blackboard.services.etl.transforms.TransformFactory;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.blackboard.services.utils.JavaUtils;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;

/**
 *
 * @author crusnak
 */
public class DataCorrelation extends AbstractTask
{
    public static enum SelectType { all, distinct };
    public static enum JoinType { none, outer, inner };
    public static enum ExpandType { delimiter, regex };

    public static final String COL_CELL_ERROR =     "ERROR_RESULT_SET";
    public static final String COL_QUERY =          "ERROR_QUERY";
    
    private Logger logger = null;

    private CorrelateDataComplexType base = null;
    private String iterateDataName = "";
    private String queryDataName = "";
    private RelationalContainer data = null;
    private RelationalContainer iterateData = null;
    private RelationalContainer queryData = null;
    private Map<FilterBranch,List<AbstractTransform>> transformMap = new HashMap();
    private List<FilterBranch> filters = new ArrayList();
    private List<DataColumn> declaredColumns = new ArrayList();
    private Map<DataColumn,DataColumn> renameColumns = new HashMap();
    private SelectType selectType = null;
    private JoinType joinType = null;
    private HashMap<String,DataIndex> indexMap = new HashMap();
    private boolean includeAll = false;
    private boolean reindex = false;
    private boolean assign = false;
    private boolean expand = false;
    private boolean ignoreMissingColumns = false;
    private String expandColumn = null;
    private String expandExpr = null;
    private ExpandType expandType = null;

    public DataCorrelation(EtlTaskComplexType ct, CorrelateDataComplexType xml, ETLJobWrapper w)
        throws ETLException
    {
        super(ct, w);
        setData(dataSet);

        logger = Logger.getLogger(this.getClass());
        setDataSelect(xml.getSelect());
        if ( xml.getSelect() != null )
        {
            setColumnDefinitions(xml.getInputData());
        }
        else 
        {
            //setColumnDefinitions(xml.getInputData());
        }
    }

    public Object getBaseObject()
    {
        return base;
    }

    @Override
    public ClassType getClassType()
    {
        return ClassType.DataCorrelation;
    }

    @Override
    public RelationalContainer getData()
        throws ETLException
    {
        setData(super.getData());
        return data;
    }

    @Override
    public void setData(DataSet d)
    {
        super.setData(d);
        if ( d.getType() != DataSet.Type.relational )
        {
            throw new UnsupportedOperationException("DataSet assigned to " + this.getClass().getSimpleName() + " must be relational");
        }
        data = (RelationalContainer)d;
    }

    public String getIteratorDataName()
    {
        return iterateDataName;
    }

    public void setIteratorData(RelationalContainer dc)
    {
        iterateData = dc;
    }

    public RelationalContainer getIteratorData()
    {
        return iterateData;
    }

    public void setQueryData(RelationalContainer dc)
    {
        iterateData = dc;
    }

    public String getQueryDataName()
    {
        return queryDataName;
    }

    public RelationalContainer getQueryData()
    {
        return queryData;
    }

    public SelectType getSelectType()
    {
        return selectType;
    }

    private void setSelectType(String o)
        throws ETLException
    {
        if ( o == null )
        {
            throw new ETLException("SelectType cannot be null for DataCorrelation.Select, use one of: " + JavaUtils.enumToList(SelectType.class));
        }

        try
        {
            selectType = SelectType.valueOf(o);
        }
        catch (IllegalArgumentException e)
        {
            throw new ETLException("Unsupported select type '" + o + "', expected: " + JavaUtils.enumToList(SelectType.class), e);
        }
    }

    public JoinType getJoinType()
    {
        return joinType;
    }

    private void setJoinType(String o)
        throws ETLException
    {
        if ( o == null )
        {
            throw new ETLException("JoinType cannot be null for DataCorrelation.Select, use one of: " + JavaUtils.enumToList(JoinType.class));
        }

        try
        {
            joinType = JoinType.valueOf(o);
        }
        catch (IllegalArgumentException e)
        {
            throw new ETLException("Unsupported join type '" + o + "', expected: " + JavaUtils.enumToList(JoinType.class), e);
        }
    }

    public ExpandType getExpandType()
    {
        return expandType;
    }

    private void setExpandType(String o)
        throws ETLException
    {
        if ( o == null )
        {
            throw new ETLException("ExpandType cannot be null for DataCorrelation.Select.SplitOnColumn, use one of: " + JavaUtils.enumToList(ExpandType.class));
        }

        try
        {
            expandType = ExpandType.valueOf(o);
        }
        catch (IllegalArgumentException e)
        {
            throw new ETLException("Unsupported expand type '" + o + "', expected: " + JavaUtils.enumToList(ExpandType.class), e);
        }
    }

    private void setDataSelect(CorrelateDataComplexType.Select ct)
        throws ETLException
    {
        if ( ct != null )
        {
            setSelectType(ct.getType());
            setJoinType(ct.getJoin());
            reindex = ct.isReindex();

            if ( ct.getForEach() != null )
            {
                iterateDataName = ct.getForEach().getValue();
                DataSet ds = wrapper.retrieveNamedData(iterateDataName);
                if ( ds == null )
                {
                    throw new ETLException("Undefined iterate/source data: " + iterateDataName);
                }
                
                if ( ds.getType() != DataSet.Type.relational )
                {
                    throw new ETLException("Reference to a hierarchical data set. DataCorrelations only support relational data sets.");
                }

                iterateData = (RelationalContainer)ds;
                data.addColumnDefinitions(iterateData.getColumnDefinitions());
            }

            queryDataName = ct.getFrom().getValue();
            ignoreMissingColumns = ct.getFrom().isIgnoreMissingColumns();
            DataSet ds = wrapper.retrieveNamedData(queryDataName);
            if ( ds == null )
            {
                //logger.warn("Undefined query/target data: " + queryDataName + ". Will attempt to retrieve data again when processing task.");
                throw new ETLException("Undefined query/target data: " + queryDataName + ". Will attempt to retrieve data again when processing task.");
            }
            if ( ds.getType() != DataSet.Type.relational )
            {
                throw new ETLException("Reference to a hierarchical data set. DataCorrelations only support relational data sets.");
            }

            queryData = (RelationalContainer)ds;

            assign = !ct.getAssignColumnData().isEmpty();
            expand = ct.getSplitOnColumn() != null;
            if ( assign )
            {
                for ( CorrelateDataComplexType.Select.AssignColumnData assign: ct.getAssignColumnData() )
                {
                    // Add each where clause to the filters
                    FilterBranch filter = new FilterBranch(assign, this);
                    filters.add(filter);

                    // Add all transforms to the transform list
                    ArrayList<AbstractTransform> tforms = new ArrayList();
                    for ( TransformComplexType tf: assign.getTransform().getAssignOrDateOrLookup() )
                    {
                        tforms.add(TransformFactory.createTransform(tf, this));
                    }
                    
                    transformMap.put(filter, tforms);
                }
            }
            else if ( expand )
            {
                expandColumn = ct.getSplitOnColumn().getSourceColumn().getValue();
                expandExpr = ct.getSplitOnColumn().getExpression();
                setExpandType(ct.getSplitOnColumn().getType());
            }
            else if ( !ct.getWhereAndAndOrOr().isEmpty() )
            {
                filters.add(new FilterBranch(ct, this));
            }
            
            //initializeIndexMap();
            //logger.debug("DataIndex/column map: " + indexMap);
        }
    }

    private void initializeIndexMap()
        throws ETLException
    {
        Set<List<String>> colnames = getColumnNamesNeededForIndexedFilter(filters);
        logger.debug("NeededColumns: " + colnames);

        for ( List<String> cols: colnames )
        {
            if ( !indexMap.containsKey(String.valueOf(cols)) )
            {
                //DataIndex idx = new DataIndex(DataIndex.Type.nonunique, cols);//, queryData.getColumnDefinitions());
                DataIndex idx = new DataIndex(DataIndex.Type.nonunique, filters.get(0));
                queryData.addDataIndex(idx);
                Object old = indexMap.put(String.valueOf(cols), idx);
                if ( old != null )
                {
                    logger.debug("No index defined for " + cols + ", adding non-unique index: " + idx);
                }
                else
                {
                    logger.debug("Overwriting index defined for " + cols + ", adding non-unique index: " + idx);
                }
            }
            
            Collection<DataIndex> indices = queryData.getDataIndices();
            Set<DataIndex> set = new HashSet(indices.size());
            set.addAll(indices);
            for ( String colname: cols )
            {
                Set<DataIndex> matches = queryData.getDataIndicesForColumn(colname);
                set.retainAll(matches);
                logger.trace("DataIndex matches for '" + colname + "': " + matches);
            }

            Iterator<DataIndex> it = set.iterator();
            while ( it.hasNext() )
            {
                DataIndex idx = it.next();
                boolean exact = true;
                for ( String colname: cols )
                {
                    exact &= idx.containsColumn(colname);
                }
                exact &= cols.size() == idx.getIndexedColumnNames().size();

                if ( exact )
                {
                    indexMap.put(String.valueOf(cols), idx);
                    //logger.trace("Existing DataIndex found for " + cols + ": " + idx);
                    break;
                }
            }
        }

        //logger.trace("DataIndices: " + queryData.getDataIndices());
        //logger.debug("DataIndex/column map: " + indexMap);
    }

    private void setColumnDefinitions(CorrelateDataComplexType.InputData input)
        throws ETLException
    {
        if ( input != null )
        {
            renameColumns.clear();
            includeAll = input.isIncludeAllColumns();
            for ( CorrelateDataComplexType.InputData.DataElement ct: input.getDataElement() )
            {
                //DataColumn col = data.addColumnDefinition(String.valueOf(ct.getType()), ct.getValue(), ct.getSchema(), ct.getDefaultValue());
                DataColumn col = new DataColumn(ct.getType(), ct.getValue(), ct.getDefaultValue());
                DataColumn rename = col;
                if ( ct.getAS() != null && ct.getAS().trim().length() > 0 )
                {
                    rename = new DataColumn(ct.getType(), ct.getAS(), ct.getDefaultValue());
                    data.addColumnDefinition(rename);
                }
                else
                {
                    data.addColumnDefinition(col);
                }
                renameColumns.put(col, rename);
                declaredColumns.add(col);
            }
        }
    }

    public RelationalContainer correlate()
        throws ETLException
    {
        RelationalContainer errors = new RelationalContainer(getTaskName() + ERROR_DATA_NAME);
        DataColumn ecol = new DataColumn(DataTypes.STRING, RelationalContainer.COL_ERROR_MSG);
        DataColumn qcol = new DataColumn(DataTypes.STRING, DataCorrelation.COL_QUERY);
        DataColumn scol = new DataColumn(DataTypes.STRING, DataCorrelation.COL_CELL_ERROR);
        DataColumn tcol = new DataColumn(DataTypes.STRING, DataMapping.COL_CELL_ERROR);

        DataSet ds = wrapper.retrieveNamedData(queryDataName);;
        if ( queryData == null )
        {
            throw new ETLException("Undefined query/target data: " + queryDataName);
        }

        if ( ds.getType() != DataSet.Type.relational )
        {
            throw new ETLException("Reference to a hierarchical data set. DataCorrelation only support relational data sets.");
        }
        queryData = (RelationalContainer)ds;
        
        // Reload the index map to ensure that the proper index references are retained
        initializeIndexMap();

        if ( iterateDataName == null || iterateDataName.trim().length() == 0 )
        {
            //logger.debug("QueryData: " + queryData);

            // Add all columns from query data if requested
            if ( includeAll )
            {
                data.addColumnDefinitions(queryData.getColumnDefinitions());
            }
            // Otherwise include all declared columns
            else
            {
                for ( DataColumn col: declaredColumns )
                {
                    data.addColumnDefinition(renameColumns.get(col));
                }
            }

            logger.debug("Update column definitions: " + data.getColumnDefinitions());

            // If a reindex is desired do so first
            if ( reindex )
            {
                System.out.println("Reindexing " + queryData.rowCount() + " rows of data");
                queryData.indexAllData();
                //logger.trace("Reindexed index map: " + indexMap);
            }

            if ( expand )
            {
                if ( queryData.getColumnDefinition(expandColumn) == null )
                {
                    throw new ETLException("Reference to undefined expansion column in query data: " + expandColumn);
                }
                
                logger.info("Splitting rows on column " + expandColumn + " using " + expandType + " expression: " + expandExpr);
                int count = queryData.rowCount();
                Iterator<Map<String,DataCell>> it = queryData.getMappedData().iterator();
                for ( int i=1; it.hasNext(); i++ )
                {
                    System.out.println(getTaskName() + ": Expanding row #" + i + " out of " + count);
                    Map<String,DataCell> qMap = it.next();
                    logger.debug("Expanding Current row #" + i + ": " + qMap);
                    
                    List<String> elist = new ArrayList();
                    if ( expandType == ExpandType.delimiter )
                    {
                        elist = Arrays.asList(String.valueOf(qMap.get(expandColumn).getValue()).split("\\" + expandExpr));
                    }
                    else if ( expandType == ExpandType.regex )
                    {
                        throw new UnsupportedOperationException("SplitOnColumn does not currently support regex expansion");
                    }
                    
                    // Add each split value as it's on row in the same column
                    for ( String s: elist )
                    {
                       Map<String,DataCell> rowdata = new HashMap();
                       rowdata.putAll(qMap);
                       rowdata.put(expandColumn, new DataCell(queryData.getColumnDefinitions().get(expandColumn), s));
                       data.addDataRow(rowdata);
                    }
                }
            }
            else
            {
                // Only support where, no assigns without iterate data
                List<Map<String,DataCell>> rs = selectData(filters.get(0));
                if ( selectType == SelectType.distinct )
                {
                    rs = convertDistinctData(rs, declaredColumns);
                }
                //logger.debug("ResultSet: " + rs);

                // If no rows returned, add a nulled data map with all the declared columns
                // to indicate the joined data is null
                if ( rs.size() == 0 )
                {
                    logger.debug("No match found assigning null to all declared columns");
                    HashMap<String,DataCell> empty = new HashMap();
                    if ( includeAll )
                    {
                        for ( DataColumn col: queryData.getColumnDefinitions().values() )
                        {
                            empty.put(col.getColumnName(), new DataCell(col, null));
                        }
                    }
                    else
                    {
                        for ( DataColumn col: declaredColumns )
                        {
                            empty.put(renameColumns.get(col).getColumnName(), new DataCell(renameColumns.get(col), null));
                        }
                    }
                }

                // Add all the result data
                for ( Map<String,DataCell> result: rs )
                {
                    // Add all declared columns first
                    Map<String,DataCell> rowdata = new HashMap();
                    for ( DataColumn col: declaredColumns )
                    {
                        if ( data.getColumnDefinition(renameColumns.get(col).getColumnName()) == null )
                        {
                            data.addColumnDefinition(renameColumns.get(col));
                        }

                        DataCell cell = result.get(col.getColumnName());
                        if ( cell == null )
                        {
                            // Handle data error?
                            logger.debug("Undefined column in query data declared as input data element, assigning NULL: " + col);
                            rowdata.put(col.getColumnName(), new DataCell(col, null));
                        }
                        else
                        {
                            rowdata.put(col.getColumnName(), new DataCell(col, cell.getValue()));
                        }
                    }

                    // Add all data and columns from result
                    if ( includeAll )
                    {
                        for ( Map.Entry<String,DataCell> entry: result.entrySet() )
                        {
                            String colname = entry.getKey();
                            DataColumn col = data.getColumnDefinition(colname);
                            if ( col != null )
                            {
                                DataCell cell = entry.getValue();
                                rowdata.put(colname, new DataCell(cell.getColumn(), cell.getValue()));
                            }
                        }
                    }

                    data.addDataRow(rowdata);
                    //logger.debug("Row #" + data.rowCount() + ": " + rowdata);
                }
            }
        }
        else
        {
            ds = wrapper.retrieveNamedData(iterateDataName);
            if ( ds.getType() != DataSet.Type.relational )
            {
                throw new ETLException("Reference to a hierarchical data set. DataCorrelations only support relational data sets.");
            }

            iterateData = (RelationalContainer)ds;
            errors.setColumnDefinitions(iterateData.getColumnDefinitions());
            errors.addColumnDefinition(ecol);
            errors.addColumnDefinition(qcol);
            errors.addColumnDefinition(scol);
            errors.addColumnDefinition(tcol);

            logger.debug("QueryData: " + queryData);
            logger.debug("IterateData: " + iterateData);

            // Assign all columns from the iterate data to the current data set
            data.setColumnDefinitions(iterateData.getColumnDefinitions());

            // Add all columns from query data if requested
            if ( includeAll )
            {
                logger.debug("Adding all column definitions from query data: " + queryData.getColumnDefinitions());
                data.addColumnDefinitions(queryData.getColumnDefinitions());
            }
            // Otherwise include all declared columns
            else
            {
                for ( DataColumn col: declaredColumns )
                {
                    data.addColumnDefinition(renameColumns.get(col));
                }
            }

            logger.debug("Update column definitions: " + data.getColumnDefinitions());

            // If a reindex is desired do so first
            if ( reindex )
            {
                System.out.println("Reindexing " + queryData.rowCount() + " rows of data");
                queryData.indexAllData();
                //logger.trace("Reindexed index map: " + indexMap);
            }

            int count = iterateData.rowCount();
            Iterator<Map<String,DataCell>> it = iterateData.getMappedData().iterator();
            rowbranch: for ( int i=1; it.hasNext(); i++ )
            {
                data.setCurrentProcessedRow(i);
                System.out.println(getTaskName() + ": Correlating row #" + i + " out of " + count);
                Map<String,DataCell> iterateMap = it.next();
                logger.debug("Processing Current row #" + i + ": " + iterateMap);

                if ( filters.isEmpty() )
                {
                    logger.info("No filters declared, joining all rows with declared columns");
                    List<Map<String,DataCell>> rs = queryData.getMappedData();

                    // Add all the result data
                    for ( Map<String,DataCell> result: rs )
                    {
                        // Add all of the iterate data to the current row
                        Map<String,DataCell> rowdata = new HashMap(iterateMap);
                        for ( Map.Entry<String,DataCell> entry: iterateMap.entrySet() )
                        {
                            String colname = entry.getKey();
                            logger.debug("Adding column: " + colname);
                            if ( data.getColumnDefinition(colname) == null )
                            {
                                data.addColumnDefinition(iterateData.getColumnDefinition(colname));
                            }

                            DataCell cell = entry.getValue();
                            rowdata.put(colname, new DataCell(cell.getColumn(), cell.getValue()));
                        }

                        // Add all declared columns first
                        for ( DataColumn col: declaredColumns )
                        {
                            if ( data.getColumnDefinition(renameColumns.get(col).getColumnName()) == null )
                            {
                                data.addColumnDefinition(renameColumns.get(col));
                            }

                            DataCell cell = result.get(col.getColumnName());
                            if ( cell == null )
                            {
                                // Handle data error?
                                logger.debug("Undefined column in query data declared as input data element, assigning NULL: " + col);
                                rowdata.put(renameColumns.get(col).getColumnName(), new DataCell(renameColumns.get(col), null));
                            }
                            else
                            {
                                rowdata.put(renameColumns.get(col).getColumnName(), new DataCell(renameColumns.get(col), cell.getValue()));
                            }
                        }

                        // Add all data and columns from result
                        if ( includeAll )
                        {
                            for ( Map.Entry<String,DataCell> entry: result.entrySet() )
                            {
                                String colname = entry.getKey();
                                DataColumn col = data.getColumnDefinition(colname);
                                if ( col != null )
                                {
                                    DataCell cell = entry.getValue();
                                    rowdata.put(colname, new DataCell(cell.getColumn(), cell.getValue()));
                                }
                            }
                        }

                        data.addDataRow(rowdata);
                        logger.debug("Row #" + data.rowCount() + ": " + rowdata);
                    }
                }
                else
                {
                    for ( FilterBranch filter: filters )
                    {
                        List<AbstractTransform> transforms = transformMap.get(filter);
                        logger.debug("Filter: " + filter);
                        logger.debug("Transforms: " + transforms);

                        List<Map<String,DataCell>> rs = selectData(iterateMap, filter);
                        if ( selectType == SelectType.distinct )
                        {
                            rs = convertDistinctData(rs, declaredColumns);
                        }
                        //logger.debug("ResultSet: " + rs);

                        switch (joinType)
                        {
                            case inner:
                                if ( rs.size() == 0 )
                                {
                                    logger.debug("No rows returned, inner-join: removing referenced row data");
                                    //it.remove();
                                    continue;

                                    // Inner joins should not error if no rows are returned
                                    /*map.put(DataContainer.COL_ERROR_MSG, new DataCell(ecol, "No rows returned, row removed due to inner-join"));
                                    map.put(COL_STATEMENT_MSG, new DataCell(scol, statement));
                                    errors.addDataRow(map);

                                    logger.debug("No rows returned matching criteria, removing row from data.");
                                    it.remove();*/
                                    // Add to errors
                                }
                                break;
                            case outer:

                                // If no rows returned, add a nulled data map with all the declared columns
                                // to indicate the joined data is null
                                if ( rs.size() == 0 )
                                {
                                    logger.debug("No match found assigning null to all declared columns");
                                    HashMap<String,DataCell> empty = new HashMap();
                                    if ( includeAll )
                                    {
                                        for ( DataColumn col: queryData.getColumnDefinitions().values() )
                                        {
                                            empty.put(col.getColumnName(), new DataCell(col, null));
                                        }
                                    }
                                    else
                                    {
                                        for ( DataColumn col: declaredColumns )
                                        {
                                            empty.put(renameColumns.get(col).getColumnName(), new DataCell(renameColumns.get(col), null));
                                        }
                                    }

                                    // Remove null values that use the same name as the iterate data columns in the
                                    // where clauses. If this isn't done then the null value will overwrite the
                                    // column in the iterate data when no match is found.
                                    for ( BooleanTest test: filter.getTests() )
                                    {
                                        if ( test.getOperand1() != null && test.getOperand2() != null &&
                                             test.getOperand1().getValue().equals(test.getOperand2().getValue()) )
                                        {
                                            DataColumn col = queryData.getColumnDefinition(String.valueOf(test.getOperand1().getValue()));
                                            if ( col != null )
                                            {
                                                logger.debug("Removing nulled column with same name as in for-each data: " + col.getColumnName());
                                                empty.remove(col.getColumnName());
                                            }
                                        }
                                    }

                                    // Add all of the iterate data to the current row
                                    empty.putAll(iterateMap);
                                    if ( joinType != JoinType.none )
                                    {
                                        for ( Map.Entry<String,DataCell> entry: iterateMap.entrySet() )
                                        {
                                            String colname = entry.getKey();
                                            if ( data.getColumnDefinition(colname) == null )
                                            {
                                                data.addColumnDefinition(iterateData.getColumnDefinition(colname));
                                            }

                                            DataCell cell = entry.getValue();
                                            empty.put(colname, new DataCell(cell.getColumn(), cell.getValue()));
                                        }
                                    }
                                    
                                    rs.add(empty);
                                }
                                else if ( assign && rs.size() > 1 )
                                {
                                    logger.error("More than one row returned, please refine select query to ensure 0 or 1 row is returned for column assignment.");
                                    HashMap<String,DataCell> map = new HashMap(iterateMap);
                                    map.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, "More than one row returned, please refine select query to ensure 0 or 1 row is returned for column assignment"));
                                    map.put(DataCorrelation.COL_QUERY, new DataCell(qcol, String.valueOf(extractColumnQueries(iterateMap, filter.getTests()))));
                                    map.put(DataCorrelation.COL_CELL_ERROR, new DataCell(scol, String.valueOf(rs)));
                                    errors.addDataRow(map);
                                    //it.remove();
                                    continue;
                                }
                                break;
                        }

                        if ( !assign )
                        {
                            // Add all the result data
                            for ( Map<String,DataCell> result: rs )
                            {
                                // Add all of the iterate data to the current row
                                Map<String,DataCell> rowdata = new HashMap(iterateMap);
                                if ( joinType != JoinType.none )
                                {
                                    for ( Map.Entry<String,DataCell> entry: iterateMap.entrySet() )
                                    {
                                        String colname = entry.getKey();
                                        if ( data.getColumnDefinition(colname) == null )
                                        {
                                            data.addColumnDefinition(iterateData.getColumnDefinition(colname));
                                        }

                                        DataCell cell = entry.getValue();
                                        rowdata.put(colname, new DataCell(cell.getColumn(), cell.getValue()));
                                    }
                                }

                                // Add all declared columns first
                                for ( DataColumn col: declaredColumns )
                                {
                                    if ( data.getColumnDefinition(renameColumns.get(col).getColumnName()) == null )
                                    {
                                        logger.debug("Adding column: " + renameColumns.get(col));
                                        data.addColumnDefinition(renameColumns.get(col));
                                    }

                                    DataCell cell = result.get(col.getColumnName());
                                    if ( cell == null )
                                    {
                                        // Handle data error?
                                        logger.debug("Undefined column in query data declared as input data element, assigning NULL: " + col);
                                        rowdata.put(renameColumns.get(col).getColumnName(), new DataCell(renameColumns.get(col), null));
                                    }
                                    else
                                    {
                                        rowdata.put(renameColumns.get(col).getColumnName(), new DataCell(renameColumns.get(col), cell.getValue()));
                                    }
                                }

                                // Add all data and columns from result
                                if ( includeAll )
                                {
                                    for ( Map.Entry<String,DataCell> entry: result.entrySet() )
                                    {
                                        String colname = entry.getKey();
                                        DataColumn col = data.getColumnDefinition(colname);
                                        if ( col != null )
                                        {
                                            DataCell cell = entry.getValue();
                                            rowdata.put(colname, new DataCell(cell.getColumn(), cell.getValue()));
                                        }
                                    }
                                }

                                data.addDataRow(rowdata);
                                logger.debug("Row #" + data.rowCount() + ": " + rowdata);
                            }
                        }
                        else
                        {
                            Map<String,DataCell> rowdata = rs.get(0);
                            RelationalContainer temp = new RelationalContainer(queryData.getDataName());
                            temp.addColumnDefinitions(queryData.getColumnDefinitions());
                            temp.addDataRow(rowdata);
                            logger.debug("Temp container: " + temp);

                            for ( AbstractTransform transform: transforms )
                            {
                                try
                                {
                                    logger.debug("Performing " + transform.getTransformType() + " transform: " + transform);

                                    List<DataCell> sources = DataMapping.extractSourceData(temp, transform, 0);
                                    logger.debug("Source cell(s): " + sources);

                                    ArrayList<DataColumn> targets = new ArrayList();
                                    if ( transform.supportsMultipleTargets() )
                                    {
                                        for ( String target: transform.getTargetColumnNames() )
                                        {
                                            targets.add(data.getColumnDefinition(target));
                                        }
                                    }
                                    else
                                    {
                                        String target = transform.getTargetColumnNames().get(0);
                                        targets.add(data.getColumnDefinition(target));
                                    }
                                    logger.debug("Target column(s): " + targets);

                                    List<DataCell> tcells = transform.transform(targets, sources);
                                    for ( int j=0; j<tcells.size(); j++ )
                                    {
                                        DataCell tcell = tcells.get(j);
                                        TransformTarget target = transform.getTargetColumns().get(j);
                                        if ( targets.get(j) == null || target.isTargetDataTypeOverridden() )
                                        {
                                            logger.info("No definition for column '" + target.getColumnName() + "'. Adding column to data.");
                                            data.addColumnDefinition(tcell.getColumn().getDataType(),
                                                                     target.getColumnName(), target.getDefaultValue());
                                        }
                                        logger.debug("Target cell: " + tcell);
                                        iterateMap.put(target.getColumnName(), tcell);
                                    }
                                }
                                catch (TransformIgnoreException e)
                                {
                                    logger.info("Transform error defined as ignorable: " + e.getMessage());
                                }
                                catch (TransformException e)
                                {
                                    logger.error("Failed to transform column data", e);
                                    HashMap<String,DataCell> map = new HashMap(iterateMap);
                                    map.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, e.getMessage()));
                                    map.put(DataCorrelation.COL_QUERY, new DataCell(qcol, String.valueOf(extractColumnQueries(iterateMap, filter.getTests()))));
                                    map.put(DataCorrelation.COL_CELL_ERROR, new DataCell(scol, String.valueOf(rs)));
                                    map.put(DataMapping.COL_CELL_ERROR, new DataCell(tcol, e.getDataCell()));
                                    errors.addDataRow(map);
                                    continue rowbranch;
                                }
                            }
                        }
                    }

                }
                // Only add the iterate map with assigned transformed columns when not doing simple correlations
                if ( assign )
                {
                    data.addDataRow(iterateMap);
                }
            }
        }

        return errors;
    }

    // TODO: finish this
    public List<Map<String,DataCell>> convertDistinctData(List<Map<String,DataCell>> rs, List<DataColumn> groupColumns)
        throws ETLException
    {
        ArrayList<Map<String,DataCell>> groupedRS = new ArrayList();
        List<String> cols = new ArrayList();
        for ( DataColumn col: groupColumns )
        {
            cols.add(col.getColumnName());
        }

        RelationalContainer dc = new RelationalContainer("source");
        DataAggregation da = new DataAggregation(cols);
        dc.setData(rs);
        da.aggregate(dc);
        return ((RelationalContainer)da.getData()).getMappedData();
    }

    public List<Map<String,DataCell>> selectData(Map<String,DataCell> argdata, FilterBranch filter)
        throws ETLException
    {
        ArrayList<Map<String,DataCell>> rs = new ArrayList();
        List<Map<String,Object>> argcols = extractColumnQueries(argdata, filter.getTests()); 

        // For each indexed search, find the row numbers associated with the given values
        for (int i=0; i<argcols.size(); i++ )
        {
            Map<String,Object> argmap = argcols.get(i);
            logger.trace("Argcols " + (i+1) + ": " + argmap);
            List vals = new ArrayList();
            //logger.trace("DataIndexMap: " + indexMap);
            DataIndex di = indexMap.get(String.valueOf(argmap.keySet()));
            //logger.trace("DataIndex for query: " + di);

            String statement = "SELECT * FROM " + queryDataName + " WHERE ";
            for ( int j=0; j<di.getIndexedColumnNames().size(); j++ )
            {
                String colname = di.getIndexedColumnNames().get(j);
                Object val = argmap.get(colname);
                vals.add(val);
                statement += colname + "=" + val;
                if ( j<di.getIndexedColumnNames().size()-1 )
                {
                    statement += " AND ";
                }
            }

            List<Integer> rownums = di.getIndexedRows(vals);
            logger.debug("Performing query: " + statement);
            logger.debug("Rows retrieved: " + rownums);
            for ( int rownum: rownums )
            {
                rs.add(queryData.getMappedData().get(rownum));
            }
        }

        return rs;
    }

    public List<Map<String,DataCell>> selectData(FilterBranch filter)
        throws ETLException
    {
        ArrayList<Map<String,DataCell>> rs = new ArrayList();
        List<Map<String,Object>> argcols = extractColumnQueries(null, filter.getTests());

        // For each indexed search, find the row numbers associated with the given values
        for (int i=0; i<argcols.size(); i++ )
        {
            Map<String,Object> argmap = argcols.get(i);
            List vals = new ArrayList();
            DataIndex di = indexMap.get(String.valueOf(argmap.keySet()));
            //logger.trace("DataIndex for query: " + di);

            String statement = "SELECT * FROM " + queryDataName + " WHERE ";
            for ( int j=0; j<di.getIndexedColumnNames().size(); j++ )
            {
                String colname = di.getIndexedColumnNames().get(j);
                Object val = argmap.get(colname);
                vals.add(val);
                statement += colname + "=" + val;
                if ( j<di.getIndexedColumnNames().size()-1 )
                {
                    statement += " AND ";
                }
            }

            logger.debug("Values: " + vals);

            List<Integer> rownums = di.getIndexedRows(vals);
            logger.info("Performing query: " + statement);
            logger.debug("Rows retrieved: " + rownums);
            for ( int rownum: rownums )
            {
                rs.add(queryData.getMappedData().get(rownum));
            }
        }

        return rs;
    }
    
    private List<Map<String,Object>> extractColumnQueries(Map<String,DataCell> argdata, List<BooleanTest> tests)
        throws ETLException
    {
        ArrayList<Map<String,Object>> argcols = new ArrayList();
        Map<String,Object> current = new LinkedHashMap();

        // Create an indexMap of all data indexes needed by the query
        //initializeIndexMap(queryData.getDataIndices());

        // Based on the where clause, build up a list of queries against the appropriate data index.
        for ( BooleanTest test: tests )
        {
            if ( test.getTestType() == BooleanTest.TestType.BOOLEAN )
            {
                Argument op1 = test.getOperand1();
                Argument op2 = test.getOperand2();
                if ( test.getBooleanType() == BooleanTest.BooleanType.eq || 
                     test.getBooleanType() == BooleanTest.BooleanType.startswith || 
                     test.getBooleanType() == BooleanTest.BooleanType.endswith || 
                     test.getBooleanType() == BooleanTest.BooleanType.contains )
                {
                    String colname = "";
                    if ( op1.getArgumentType() == Argument.Type.column )
                    {
                        colname = String.valueOf(op1.getValue());
                        if ( !ignoreMissingColumns && queryData.getColumnDefinition(colname) == null )
                        {
                            throw new ETLException("Reference to undefined column name '" + colname +
                                                    "' in query/target data for where clause: " + test);
                        }
                    }
                    else
                    {
                        throw new ETLException("DataProcess select queries only support column argument types for operand1 of a where clause");
                    }

                    Object val = null;
                    switch ( op2.getArgumentType() )
                    {
                        case column:
                            if ( argdata == null )
                            {
                                throw new IllegalArgumentException("Operand2 must be a constant type when no for-each data set is defined");
                            }
                            else
                            {
                                if ( argdata.containsKey(String.valueOf(op2.getValue())) )
                                {
                                    val = argdata.get(String.valueOf(op2.getValue())).getValue();
                                }
                                else
                                {
                                    throw new FilterException("Column not found in data set: " + op2.getValue());
                                }
                            }
                            break;
                        case constant:
                            val = op2.getValue();
                            break;
                    }

                    current.put(colname, val);
                }
                else
                {
                    throw new ETLException("DataProcess select queries only support where clause types of 'eq|startswith|endswith'");
                }
            }
            // Or breaks the index up and requires a new index
            else if ( test.getTestType() == BooleanTest.TestType.OR )
            {
                // define index
                argcols.add(current);
                current = new LinkedHashMap();
            }
        }

        argcols.add(current);
        
        return argcols;
    }

    private Set<List<String>> getColumnNamesNeededForIndexedFilter(List<FilterBranch> filters)
        throws ETLException
    {
        Set<List<String>> needed = new HashSet();
        for ( FilterBranch filter: filters )
        {
            List<String> current = new ArrayList();
            for ( BooleanTest test: filter.getTests() )
            {
                if ( test.getTestType() == BooleanTest.TestType.BOOLEAN )
                {
                    Argument arg = test.getOperand1();
                    switch ( test.getBooleanType() )
                    {
                        case eq:
                        case startswith:
                        case endswith:
                        case contains:
                        case regex:
                            if ( arg.getArgumentType() != Argument.Type.column )
                            {
                                throw new ETLException("Operand1 of a where clause must be a column");
                            }

                            String colname = String.valueOf(arg.getValue());
                            if ( !ignoreMissingColumns && queryData.getColumnDefinition(colname) == null )
                            {
                                logger.warn("Reference to undefined column name '" + colname +
                                            "' in query/target data for where clause: " + test);
                            }
                            current.add(colname);
                            break;
                        default:
                            EnumSet<BooleanTest.BooleanType> set = EnumSet.of(BooleanTest.BooleanType.eq, BooleanTest.BooleanType.startswith, BooleanTest.BooleanType.endswith, BooleanTest.BooleanType.contains, BooleanTest.BooleanType.regex);
                            throw new ETLException("DataProcess select queries only support where clause types of " + JavaUtils.enumToList(set) + ": " + test.getBooleanType());
                    }
                }
                // Or breaks the index up and requires a new index
                else if ( test.getTestType() == BooleanTest.TestType.OR )
                {
                    // define index
                    needed.add(current);
                    current = new ArrayList();
                }
            }
            needed.add(current);
        }

        return needed;
    }

    @Override
    public String toString()
    {
        StringBuffer out = new StringBuffer();
        out.append("{");
        out.append("TaskName=" + name);
        out.append(", NextTask=" + next);
        out.append(", For-each=" + iterateDataName);
        out.append(", Query=" + queryDataName);
        out.append(", Filters=" + filters);
        out.append(", TransformMap=" + transformMap);
        out.append("}");
        return out.toString();
    }
}
