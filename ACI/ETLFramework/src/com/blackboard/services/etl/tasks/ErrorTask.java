/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.tasks;

import com.blackboard.services.etl.data.Argument;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.jaxb.ArgumentComplexType;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.ErrorTaskComplexType;
import com.blackboard.services.etl.jaxb.EtlTaskComplexType;
import com.blackboard.services.etl.jaxb.ObjectFactory;
import com.blackboard.services.etl.jaxb.StringTransformComplexType;
import com.blackboard.services.etl.jaxb.TransformComplexType.SourceColumn;
import com.blackboard.services.etl.jaxb.TransformComplexType.TargetColumn;
import com.blackboard.services.etl.transforms.AbstractTransform;
import com.blackboard.services.etl.transforms.StringTransform;
import com.blackboard.services.etl.transforms.TransformFactory;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 * @author crusnak
 */
public class ErrorTask extends AbstractTask
{
    public static final String COL_CELL_ERROR =     "ERROR_DATA_CELL";
    private static final String COL_TCOL_NAME =     "TARGET";

    private ErrorTaskComplexType base = null;
    private RelationalContainer data = null;
    private AbstractTransform transform = null;
    private Argument affected = null;
    private boolean fatal = false;
    
    public ErrorTask(EtlTaskComplexType etl, ErrorTaskComplexType xml, ETLJobWrapper wrapper)
        throws ETLException
    {
        super(etl, wrapper);
        base = xml;
        fatal = "fatal".equals(xml.getType());
        setErrorText(xml);
    }

    @Override
    public ClassType getClassType()
    {
        return ClassType.ErrorTask;
    }

    @Override
    public Object getBaseObject()
    {
        return base;
    }
    
    @Override
    public RelationalContainer getData()
        throws ETLException
    {
        setData(super.getData());
        return data;
    }

    @Override
    public void setData(DataSet d)
    {
        super.setData(d);
        if ( d.getType() != DataSet.Type.relational )
        {
            throw new UnsupportedOperationException("DataSet assigned to " + this.getClass().getSimpleName() + " must be relational");
        }
        data = (RelationalContainer)d;
    }

    private void setErrorText(ErrorTaskComplexType ct)
        throws ETLException
    {
        if ( ct != null )
        {
            ObjectFactory factory = new ObjectFactory();
            StringTransformComplexType stct = new ObjectFactory().createStringTransformComplexType();
            stct.setAction(String.valueOf(StringTransform.Action.concatenate));
            for ( ErrorTaskComplexType.ErrorText error : ct.getErrorText() )
            {
                SourceColumn scol = factory.createTransformComplexTypeSourceColumn();
                scol.setDataType(DataTypes.STRING);
                scol.setType(error.getType());
                scol.setValue(error.getValue());
                stct.getSourceColumn().add(scol);
            }
            
            TargetColumn tcol = factory.createTransformComplexTypeTargetColumn();
            tcol.setValue(COL_TCOL_NAME);
            stct.getTargetColumn().add(tcol);
            
            transform = TransformFactory.createTransform(stct, this);
            if ( ct.getAffectedColumn() != null )
            {
                affected = new Argument(ct.getAffectedColumn(), this);
            }
        }
    }
    
    public RelationalContainer throwErrors()
        throws ETLException
    {
        DataColumn ecol = new DataColumn(DataTypes.STRING, RelationalContainer.COL_ERROR_MSG);
        DataColumn scol = new DataColumn(DataTypes.STRING, COL_CELL_ERROR);
        RelationalContainer errors = new RelationalContainer(data.getDataName() + ERROR_DATA_NAME);
        errors.setColumnDefinitions(data.getColumnDefinitions());
        errors.addColumnDefinition(ecol);
        
        if ( affected != null )
        {
            errors.addColumnDefinition(scol);
        }

        logger.info("Throwing errors for " + data.rowCount() + " rows.");
        System.err.println(getTaskName() + ": Throwing errors for " + data.rowCount() + " rows");

        Iterator<Map<String,DataCell>> it = data.getMappedData().iterator();
        for ( int i=1; it.hasNext(); i++ )
        {
            Map<String,DataCell> map = it.next();
            logger.debug("Processing row #" + i + ": " + map);

            List<DataCell> sources = DataMapping.extractSourceData(data, transform, i-1);
            List<DataColumn> targets = DataMapping.extractTargetColumns(data, transform);
            List<DataCell> cells = transform.transform(targets, sources);
            String msg = String.valueOf(cells.get(0).getValue());
            map.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, msg));

            if ( affected != null )
            {
                if ( affected.getArgumentType() == Argument.Type.column )
                {
                    map.put(COL_CELL_ERROR, new DataCell(scol, map.get(affected.getValue())));
                }
                else
                {
                    map.put(COL_CELL_ERROR, new DataCell(scol, String.valueOf(affected.getValue())));
                }
            }

            if ( fatal )
            {
                throw new ETLException("Fatal error thrown by task: " + msg);
            }
            else
            {
                errors.addDataRow(map);
            }
        }

        // Since all rows are thrown as errors remove them after completion
        data.clearData();
        
        return errors;
    }
}
