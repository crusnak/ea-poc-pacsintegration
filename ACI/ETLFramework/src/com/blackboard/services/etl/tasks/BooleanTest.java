/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.tasks;

import com.blackboard.services.etl.data.Argument;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.jaxb.BooleanComplexType;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;

/**
 *
 * @author crusnak
 */
public class BooleanTest
{
    public static enum TestType { BOOLEAN, AND, OR };
    public static enum BooleanType { eq, ne, gt, gte, lt, lte, contains, startswith, endswith, regex, inlist, notinlist };

    private AbstractTask task = null;
    private Argument operand1 = null;
    private Argument operand2 = null;
    private TestType ttype = null;
    private BooleanType btype = null;
    private String delimiter1 = null;
    private String delimiter2 = null;

    public BooleanTest(TestType tt)
    {
        setTestType(tt);
    }

    public BooleanTest(String tt)
    {
        setTestType(tt);
    }

    public BooleanTest(BooleanComplexType ct, AbstractTask t)
        throws ETLException
    {
        task = t;
        setTestType(TestType.BOOLEAN);
        if ( ct != null )
        {
            if ( ct.getType() == null || ct.getType().value() == null )
            {
                throw new ETLException("No boolean operator defined for BooleanTest");
            }

            operand1 = new Argument(ct.getOperand1(), t);
            operand2 = new Argument(ct.getOperand2(), t);
            setBooleanType(ct.getType().value());
            delimiter1 = ct.getOperand1Delimiter();
            delimiter2 = ct.getOperand2Delimiter();
        }
    }

    public TestType getTestType()
    {
        return ttype;
    }

    private void setTestType(TestType tt)
    {
        ttype = tt;
    }

    private void setTestType(String tt)
    {
        if ( tt != null )
        {
            setTestType(TestType.valueOf(tt.toUpperCase()));
        }
    }

    public BooleanType getBooleanType()
    {
        return btype;
    }

    private void setBooleanType(BooleanType bt)
    {
        btype = bt;
    }

    private void setBooleanType(String bt)
    {
        if ( bt != null )
        {
            setBooleanType(BooleanType.valueOf(bt));
        }
    }

    public Argument getOperand1()
    {
        return operand1;
    }

    public Argument getOperand2()
    {
        return operand2;
    }
    
    public String getDelimiter1()
    {
        return delimiter1;
    }

    public String getDelimiter2()
    {
        return delimiter2;
    }

    public String toString()
    {
        StringBuffer out = new StringBuffer();
        switch (ttype)
        {
            case BOOLEAN:
                out.append(operand1);
                out.append(" " + btype + " ");
                out.append(operand2);
                break;
            case AND:
            case OR:
                out.append(String.valueOf(ttype));
                break;
        }
        return out.toString();
    }
}
