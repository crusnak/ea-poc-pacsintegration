/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.tasks;

import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.jaxb.ControlTaskComplexType;
import com.blackboard.services.etl.jaxb.EtlTaskComplexType;
import com.blackboard.services.etl.process.AbstractETLProcessor;
import com.blackboard.services.etl.process.CommandLineProcessor;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author crusnak
 */
public class ControlTask extends AbstractTask
{
    private ControlTaskComplexType base = null;
    private Map<String,String> taskMap = new LinkedHashMap();
    
    public ControlTask(EtlTaskComplexType ct, ControlTaskComplexType xml, ETLJobWrapper w)
        throws ETLException
    {
        super(ct,w);
        base = xml;
        setTaskList(xml.getTask());
    }

    @Override
    public ControlTaskComplexType getBaseObject()
    {
        return base;
    }
    
    @Override
    public ClassType getClassType()
    {
        return ETLTask.ClassType.Control;
    }
    
    public int getProcessTaskCount()
    {
        return taskMap.size();
    }
    
    private void setTaskList(List<ControlTaskComplexType.Task> tasks)
    {
        for ( ControlTaskComplexType.Task task: tasks )
        {
            taskMap.put(task.getValue(), task.getAssignedData());
        }
    }
    
    public void processControl(AbstractETLProcessor processor)
        throws ETLException
    {
        for (Map.Entry<String,String> entry: taskMap.entrySet() )
        {
            String taskName = entry.getKey();
            String sourceData = entry.getValue();
            AbstractTask at = wrapper.getJobTaskByName(taskName);
            if ( at == null )
            {
                throw new ETLException("No task defined with name: " + taskName);
            }

            System.out.println(getTaskName() + ": Passing control to task: " + taskName);
            logger.info("Processing control request to task: " + taskName);
            at.setSourceDataTaskName(sourceData);
            processor.processBatchFilteredDataTask(taskName);
        }
    }
}
