/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.tasks;

import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.jaxb.EtlTaskComplexType;
import com.blackboard.services.etl.process.Processor;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.blackboard.services.utils.BeanScript;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import org.apache.log4j.Logger;

/**
 *
 * @author crusnak
 */
public abstract class AbstractTask implements ETLTask
{
    public static final String ERROR_DATA_NAME =            "-errors";

    public static final String CONSTANT_DATA_COUNT =        "DATA.count";
    public static final String CONSTANT_DATA_ROW =          "DATA.row";
    
    protected EtlTaskComplexType xmlTask = null;
    protected Logger logger = null;
    protected ETLJobWrapper wrapper = null;
    protected String name = null;
    protected String next = null;
    protected String srcData = null;
    protected String handler = null;
    protected boolean handled = false;
    protected TaskType type = TaskType.task;
    protected DataSet dataSet = null;

    protected File archiveFile = null;
    protected boolean archived = false;

    protected AbstractTask()
    {
        logger = Logger.getLogger(this.getClass());
    }

    public void destroy()
    {
        Logger.getLogger(this.getClass()).debug("Cleaning up task " + name);
        
        if ( dataSet != null )
        {
            dataSet.clear();
        }
        
        logger = null;
        wrapper = null;
    }
    
    protected AbstractTask(ETLJobWrapper w)
    {
        this();
        wrapper = w;
    }

    protected AbstractTask(EtlTaskComplexType xml, ETLJobWrapper w)
    {
        this(w);
        xmlTask = xml;
        name = xml.getName();
        setTaskType(xml.getType());
        setSourceDataTaskName(xml.getSourceData());
        setErrorHandlerTaskName(xml.getHandler());
        if ( xml.getNextTask() != null )
        {
            setNextTaskName(xml.getNextTask().getName());
        }
        dataSet = new RelationalContainer(name);
    }

    public abstract ClassType getClassType();

    public TaskType getTaskType()
    {
        return type;
    }

    public ETLJobWrapper getETLJobWrapper()
    {
        return wrapper;
    }
    
    protected void setTaskType(String tt)
    {
        try
        {
            if ( tt == null )
            {
                tt = String.valueOf(TaskType.task);
            }
            type = TaskType.valueOf(tt);
        }
        catch (IllegalArgumentException e) {};
    }

    public String getTaskName()
    {
        return name;
    }

    public String getNextTaskName()
    {
        return next;
    }

    protected void setNextTaskName(String n)
    {
        next = n;
    }

    public String getSourceDataTaskName()
    {
        return srcData;
    }

    protected void setSourceDataTaskName(String n)
    {
        srcData = n;
    }

    public boolean isCurrentlyArchived()
    {
        return archived;
    }
    
    public String getErrorHandlerTaskName()
    {
        return handler;
    }

    protected void setErrorHandlerTaskName(String n)
    {
        // Error tasks and null tasks cannot have handlers
        if ( getClassType() != ClassType.ErrorTask &&
             getClassType() != ClassType.Null )
        {
            handler = n;
        }
    }
    
    public boolean isErrorsHandled()
    {
        return handled;
    }
    
    public void setErrorsHandled(boolean h)
    {
        handled = h;
    }

    public void loadDataAndUnlink()
        throws ETLException
    {
        setData(getData());
        archived = false;
    }

    public DataSet getData()
        throws ETLException
    {
        Date start = new Date();
        DataSet ds = null;
        if ( archived )
        {
            try
            {
                ds = wrapper.getDataArchiver().readDataFromBinaryFile(archiveFile);
                logger.info(ds.rowCount() + " rows loaded from archive " + archiveFile.getCanonicalPath() + " took " +
                             (double)(new Date().getTime() - start.getTime())/1000.0 + " seconds");
            }
            catch (IOException e) {throw new ETLException(e);}
            catch (ClassNotFoundException e) {throw new ETLException(e);}
        }
        else
        {
            ds = dataSet;
        }

        return ds;
    }

    public void setData(DataSet d)
    {
        dataSet = d;
        archiveFile = null;
        archived = false;
    }

    public Object getGlobalVariableValue(String text)
        throws ETLException
    {
        Object obj = null;
        BeanScript bs = new BeanScript(text);
        if ( bs.getAccessorText() == null )
        {
            obj = text;
        }
        else
        {
            if ( CONSTANT_DATA_COUNT.equals(bs.getAccessorText()) )
            {
                obj = new Integer(dataSet.rowCount());
            }
            else if ( bs.getAccessorText() != null && bs.getAccessorText().contains(CONSTANT_DATA_COUNT) )
            {
                logger.debug("Looking up count by named data set: " + bs.getAccessorText());
                String dsname = bs.getAccessorText().substring(0, bs.getAccessorText().indexOf("$"));
                AbstractTask at = wrapper.getJobTaskByName(dsname);
                if ( at == null )
                {
                    throw new ETLException("Cannot get record count for undefined task data set: " + dsname);
                }
                else
                {
                    obj = new Integer(at.getData().rowCount());
                }
            }
            else if ( CONSTANT_DATA_ROW.equals(bs.getAccessorText()) )
            {
                obj = new Integer(dataSet.getCurrentProcessedRow());
            }
            else
            {
                DataCell gvar = wrapper.retrieveGlobalVariable(bs.getAccessorText());
                if ( gvar == null )
                {
                    throw new ETLException("Argument reference to undefined global variable: " + bs.getAccessorText());
                }
                obj = gvar.getValue();
            }
        }
        
        return obj;
    }

    public void archiveTransientData(String jobnum)
        throws ETLException
    {
        File dir = wrapper.getDataArchiver().getRunningArchiveDirectory(jobnum);
        archiveFile = new File(dir, name + Processor.DAT_EXT);

        try
        {
            wrapper.getDataArchiver().writeDataToBinaryFile(dataSet, archiveFile);
            logger.info("Transient data archived to file: " + archiveFile.getCanonicalPath());
        }
        catch (IOException e)
        {
            throw new ETLException("Unable to archive transient data for later retrieval", e);
        }

        dataSet.clearData();
        archiveFile.deleteOnExit();
        archived = true;
    }

    /*public void removeArchivedData()
        throws ETLException
    {
        if ( archived && archiveFile != null )
        {
            archiveFile.deleteOnExit();
        }
    }*/

    @Override
    public String toString()
    {
        return super.toString();
    }
}
