/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.tasks;

import com.blackboard.services.etl.data.Argument;
import com.blackboard.services.etl.data.BinaryObject;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.exception.RowLevelException;
import com.blackboard.services.etl.jaxb.ArgumentComplexType;
import com.blackboard.services.etl.jaxb.EtlTaskComplexType;
import com.blackboard.services.etl.jaxb.BinaryInputTaskComplexType;
import com.blackboard.services.etl.jaxb.BinaryOutputTaskComplexType;
import com.blackboard.services.etl.jaxb.DataComplexType;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.InputTaskComplexType;
import com.blackboard.services.etl.jaxb.OutputTaskComplexType;
import com.blackboard.services.etl.process.Processor;
import com.blackboard.services.etl.wrapper.ConnectionFactory;
import com.blackboard.services.etl.wrapper.DataArchiver;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.blackboard.services.security.BbKeyGenerator;
import com.blackboard.services.security.ConnectionManager;
import com.blackboard.services.security.PGPFileUtils;
import com.blackboard.services.security.SecuredObjectManager;
import com.blackboard.services.security.SecurityManager;
import com.blackboard.services.security.exception.BbTSSecurityException;
import com.blackboard.services.security.object.EncryptionObject;
import com.blackboard.services.security.object.NamedKey;
import com.blackboard.services.security.object.RegisterableApplication;
import com.blackboard.services.security.object.SSHConnection;
import com.blackboard.services.ssh.SFTPReadFile;
import com.blackboard.services.ssh.SSHFileTransfer;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 *
 * @author crusnak
 */
public class BinaryAdapter extends AbstractAdapter implements Adapter
{
    private static enum AdapterProperty { binaryFile, binaryData, fileLocation };
    
    private Object ba = null; 
    private SSHConnection sftpinfo = null;
    //private Argument binaryFile = null;
    //private Argument binaryData = null;
    private RelationalContainer data = null;
    private RelationalContainer iterateData = null;
    private String iterateDataName = null;
    private String directory = null;
    private String zipFilename = null;
    private boolean suppressMissing = false;
    private boolean zipBinaries = false;
    private boolean archiveOriginal = false;
    private File origRoot = null;
    private File fileRoot = null;
    private File file = null;
    private String remotePath = null;
    private String remoteFilename = null;
    Map<AdapterProperty,Argument> arguments = new HashMap();
    private DataColumn binaryColumn = null;
            
    public BinaryAdapter(EtlTaskComplexType ct, InputTaskComplexType xml, File root, ETLJobWrapper w)
        throws ETLException
    {
        super(ct, xml, w);
        setData(dataSet);

        if ( xml.getBinaryInput() == null )
        {
            throw new ETLException("Expecting BinaryInput in InputAdapter");
        }

        BinaryInputTaskComplexType in = xml.getBinaryInput();
        ba = in;
        fileRoot = root;
        origRoot = root;
        suppressMissing = in.isSuppressMissingFiles();
        setBinaryColumn(in.getForEach().getBinaryColumn());
        setAdapterProperties(in.getForEach());
    }

    public BinaryAdapter(EtlTaskComplexType ct, OutputTaskComplexType xml, File root, ETLJobWrapper w)
        throws ETLException
    {
        super(ct, xml, w);
        setData(dataSet);

        if ( xml.getBinaryOutput() == null )
        {
            throw new ETLException("Expecting BinaryOutput in OutputAdapter");
        }

        BinaryOutputTaskComplexType out = xml.getBinaryOutput();
        ba = out;
        fileRoot = root;
        origRoot = root;
        directory = out.getDirectory();
        zipBinaries = out.isZipFiles();
        zipFilename = out.getZipFilename();
        setAdapterProperties(out.getForEach());
    }

    public Object getBaseObject()
    {
        return ba;
    }

    public Adapter.Type getAdapterType()
    {
        return Adapter.Type.Binary;
    }

    @Override
    public RelationalContainer getData()
        throws ETLException
    {
        setData(super.getData());
        return data;
    }

    @Override
    public void setData(DataSet d)
    {
        super.setData(d);
        if ( d.getType() != DataSet.Type.relational )
        {
            throw new UnsupportedOperationException("DataSet assigned to " + this.getClass().getSimpleName() + " must be relational");
        }
        data = (RelationalContainer)d;
    }
    
    private void setIterateData(String name)
        throws ETLException
    {
        if ( name != null )
        {
            iterateDataName = name;
        }
        else
        {
            iterateDataName = getSourceDataTaskName();
        }
        
        if ( iterateDataName == null )
        {
            iterateData = new RelationalContainer(null, data);
        }
        else
        {
            iterateData = new RelationalContainer(iterateDataName, wrapper.retrieveNamedData(iterateDataName));
        }
    }
    
    public RelationalContainer getIteratorData()
    {
        return iterateData;
    }

    public void setQueryData(RelationalContainer dc)
    {
        iterateData = dc;
    }

    private void setAdapterProperties(BinaryInputTaskComplexType.ForEach in)
        throws ETLException
    {
        arguments.clear();
        if ( in != null )
        {
            arguments.put(AdapterProperty.binaryFile, new Argument(in.getBinaryFile(), this));
        }
    }
    
    private void setAdapterProperties(BinaryOutputTaskComplexType.ForEach out)
        throws ETLException
    {
        arguments.clear();
        if ( out != null )
        {
            arguments.put(AdapterProperty.binaryFile, new Argument(out.getFilename(), this));
            if ( out.getBinaryData() != null )
            {
                arguments.put(AdapterProperty.binaryData, new Argument(out.getBinaryData(), this));
            }
            else if ( out.getFileLocation() != null )
            {
                arguments.put(AdapterProperty.fileLocation, new Argument(out.getFileLocation(), this));
                archiveOriginal = out.isArchiveOriginalFiles();
            }
            else
            {
                throw new ETLException("BinaryAdaptor requires either binaryData or fileLocation under a for-each block"); 
            }
        }
    }
    
    private void setBinaryColumn(DataComplexType ct)
    {
        if ( ct != null )
        {
            binaryColumn = new DataColumn(DataTypes.BINARY, ct.getValue());
        }
    }
    
    private void setFileInfo(String dir, String filename, File defaultRoot)
        throws ETLException
    {
        logger.debug("Comparing file root: " + dir);
        
        // Change here: if relative directory use etlRoot + directory
        // if absolute directory, replace etlRoot with directory
        
        if ( dir != null && dir.trim().length() > 0 )
        {
            Matcher m = FileAdapter.windowsAbsoluteSlashDir.matcher(dir);
            if ( m.matches() )
            {
                logger.info("BinaryAdapter file uses absolute directory with extra backslash");
                fileRoot = new File(m.group(1) + ":" + m.group(2));
            }
            else if ( dir.matches(FileAdapter.windowsAbsoluteDir.pattern()) ||
                      dir.matches(FileAdapter.unixAbsoluteDir.pattern()) )
            {
                logger.info("BinaryAdapter file uses absolute directory");
                fileRoot = new File(dir);
            }
            else
            {
                logger.info("BinaryAdapter file uses relative directory");
                fileRoot = new File(defaultRoot, dir);
            }

            if ( !fileRoot.exists() && !wrapper.getDataArchiver().containsBeanScript(fileRoot.getAbsolutePath()) )
            {
                logger.info("File root doesn't exist, making directories: " + fileRoot.getAbsolutePath());
                fileRoot.mkdirs();
            }
        }

        remotePath = dir + "/" + filename;
        remoteFilename = filename;
        
        try
        {
            if ( filename != null )
            {
                file = new File(fileRoot.getCanonicalFile(), filename);
            }
        }
        catch (IOException e)
        {
            throw new ETLException("Unable to access ETL root", e);
        }
    }
    
    private File getFileForWriting(String jobnum, String dir, String filename, File root)
        throws IOException, ETLException
    {
        File f = null;
        if ( zipBinaries == true || sftpinfo != null )
        {
            root = wrapper.getDataArchiver().getRunningArchiveDirectory(jobnum);
            f = new File(root, filename);
            f.deleteOnExit();
        }
        else 
        {
            setFileInfo(dir, filename, root);
            f = file;
        }
        
        logger.debug("File for writing: " + f.getCanonicalPath());
        return f;
    }
    
    private File getZipFileForWriting(String jobnum, String dir, String filename, File root)
        throws IOException, ETLException
    {
        File f = null;
        if ( zipBinaries == true && sftpinfo != null )
        {
            root = wrapper.getDataArchiver().getRunningArchiveDirectory(jobnum);
            f = new File(root, filename);
            f.deleteOnExit();
        }
        else 
        {
            setFileInfo(dir, filename, root);
            f = file;
        }
        
        logger.debug("File for writing: " + f.getCanonicalPath());
        return f;
    }

    private void updateBinaryFileWithGlobalVariableAssignment()
        throws ETLException
    {
        if ( wrapper != null )
        {
            DataArchiver arch = wrapper.getDataArchiver();
            directory = arch.processEmbeddedBeanScript(directory);
            zipFilename = arch.processEmbeddedBeanScript(zipFilename);
        }
    }

    public RelationalContainer readBinaryFiles(String jobnum)
        throws ETLException
    {
        DataColumn ecol = new DataColumn(DataTypes.STRING, RelationalContainer.COL_ERROR_MSG);
        RelationalContainer errors = new RelationalContainer(data.getDataName() + ERROR_DATA_NAME);

        // Set the connection info at runtime (to support dynamic global variable modification)
        sftpinfo = ConnectionFactory.getSSHConnection(((BinaryInputTaskComplexType)ba).getConnection(), this);

        setIterateData(((BinaryInputTaskComplexType)ba).getForEach().getSourceData());
        /*iterateData = wrapper.retrieveNamedData(iterateDataName);
        if ( iterateData == null )
        {
            throw new ETLException("Undefined iterate data: " + iterateDataName);
        }*/
        logger.debug("Iterate data: " + iterateData);
        
        data.addColumnDefinitions(iterateData.getColumnDefinitions());
        data.addColumnDefinition(binaryColumn);
        errors.setColumnDefinitions(data.getColumnDefinitions());
        errors.addColumnDefinition(ecol);
        
        // If any bean script exists in the filename, assign global variable values
        updateBinaryFileWithGlobalVariableAssignment();
        
        SSHFileTransfer sftp = null;
        try
        {
            if ( sftpinfo != null )
            {
                sftp = new SSHFileTransfer(sftpinfo);
            }
        
            int count = iterateData.rowCount();
            Iterator<Map<String,DataCell>> it = iterateData.getMappedData().iterator();
            for ( int i=1; it.hasNext(); i++ )
            {
                data.setCurrentProcessedRow(i);
                System.out.println(getTaskName() + ": Reading binary file #" + i + " out of " + count);
                Map<String,DataCell> rowdata = it.next();
                Map<String,DataCell> joindata = new HashMap(rowdata);
                logger.debug("Reading binary file #" + i + ": " + joindata);

                String path = extractPropertyValue(AdapterProperty.binaryFile, rowdata, String.class, true);

                // Read the binary file and add to the data set
                FileInputStream fis = null;
                try
                {
                    // Check if the file doesn't exist and suppress missing files is not set
                    if ( path != null )
                    {
                        //File bf = new File(path);
                        File bf = getFileForReading(jobnum, path);
                        if ( !bf.exists() && !suppressMissing )
                        {
                            logger.warn("File not found and suppress missing files not enabled: " + path);
                            it.remove();
                            joindata.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, "File not found and suppress missing files not enabled: " + path));
                            errors.addDataRow(joindata);
                            continue;
                        }
                        else if ( !bf.exists() )
                        {
                            joindata.put(binaryColumn.getColumnName(), new DataCell(binaryColumn, null));
                        }
                        else if ( bf.exists() )
                        {
                            joindata.put(binaryColumn.getColumnName(), new DataCell(binaryColumn, new BinaryObject(bf)));
                        }
                    }
                    else if ( !suppressMissing )
                    {
                        logger.warn("Filename not declared and suppress missing files not enabled: " + path);
                        it.remove();
                        joindata.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, "Filename not declared and suppress missing files not enabled: " + path));
                        errors.addDataRow(joindata);
                        continue;
                    }
                    else
                    {
                        joindata.put(binaryColumn.getColumnName(), new DataCell(binaryColumn, null));
                    }
                }
                catch (IOException e)
                {
                    logger.warn("Unable to read binary file: " + path + ": " + e.getMessage(), e);
                    it.remove();
                    joindata.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, "Unable to read binary file: " + path + ": " + e.getMessage()));
                    errors.addDataRow(joindata);
                    continue;
                }
                finally
                {
                    if ( fis != null )
                    {
                        try
                        {
                            fis.close();
                        }
                        catch (IOException e)
                        {
                            logger.warn("Failed to close file", e);
                        }
                    }
                }

                data.addDataRow(joindata);
            }
        }
        catch (JSchException e)
        {
            String error = "Failed to communicate to remote server: " + sftpinfo.getHost();
            logger.warn(error, e);
            for ( Map<String,DataCell> rowdata: data.getMappedData() )
            {
                rowdata.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, error + ": " + e.getMessage()));
                errors.addDataRow(rowdata);
            }

            data.clearData();
        }
        finally
        {
            if ( sftp != null )
            {
                sftp.disconnect();
            }
        }
        
        if ( logger.isDebugEnabled() )
        {
            for ( int i=0; i<data.rowCount(); i++ )
            {
                logger.debug("Row #" + (i+1) + ": " + data.getMappedData().get(i));
            }
        }
        
        return errors;
    }

    public RelationalContainer writeBinaryFiles(String jobnum)
        throws ETLException
    {
        DataColumn ecol = new DataColumn(DataTypes.STRING, RelationalContainer.COL_ERROR_MSG);
        RelationalContainer errors = new RelationalContainer(data.getDataName() + ERROR_DATA_NAME);

        // Set the connection info at runtime (to support dynamic global variable modification)
        sftpinfo = ConnectionFactory.getSSHConnection(((BinaryOutputTaskComplexType)ba).getConnection(), this);

        setIterateData(((BinaryOutputTaskComplexType)ba).getForEach().getSourceData());
        /*iterateData = wrapper.retrieveNamedData(iterateDataName);
        if ( iterateData == null )
        {
            throw new ETLException("Undefined iterate data: " + iterateDataName);
        }*/
        logger.debug("Iterate data: " + iterateData);
        
        data.addColumnDefinitions(iterateData.getColumnDefinitions());
        errors.setColumnDefinitions(data.getColumnDefinitions());
        errors.addColumnDefinition(ecol);
        
        // If any bean script exists in the filename, assign global variable values
        updateBinaryFileWithGlobalVariableAssignment();

        FileInputStream fis = null;
        ZipOutputStream zos = null;
        SSHFileTransfer sftp = null;

        try
        {
            if ( sftpinfo != null )
            {
                sftp = new SSHFileTransfer(sftpinfo);
            }

            int count = iterateData.rowCount();
            ArrayList<File> files = new ArrayList();
            Iterator<Map<String,DataCell>> it = iterateData.getMappedData().iterator();
            for ( int i=1; it.hasNext(); i++ )
            {
                data.setCurrentProcessedRow(i);
                System.out.println(getTaskName() + ": Writing binary file #" + i + " out of " + count);
                Map<String,DataCell> rowdata = it.next();
                Map<String,DataCell> joindata = new HashMap(rowdata);
                logger.debug("Writing binary file #" + i + ": " + joindata);

                BinaryObject bo = null;
                String localfile = null;
                String origfile = null;
                byte[] b = new byte[0];
                try
                {
                    localfile = extractPropertyValue(AdapterProperty.binaryFile, rowdata, String.class, false);
                    bo = extractPropertyValue(AdapterProperty.binaryData, rowdata, BinaryObject.class, true);
                    origfile = extractPropertyValue(AdapterProperty.fileLocation, rowdata, String.class, true);
                    
                    if ( bo != null )
                    {
                        // Output the binary data to the specified local file
                        File f = getFileForWriting(jobnum, directory, localfile, origRoot);
                        b = bo.getData();
                        BinaryObject output = new BinaryObject(f, 0, 0);
                        output.setData(b);
                        b = new byte[0];

                        // If sending binary files remotely and were not archiving the binaries, do so here
                        if ( sftpinfo != null && !zipBinaries )
                        {
                            Date now = new Date();
                            String remoteFile = directory + "/" + localfile;
                            logger.info("Need to write remote file to " + sftpinfo.getHost() + ": " + remoteFile);

                            long read = sftp.put(f, remoteFile, SSHFileTransfer.WriteMode.overwrite, 0);
                            logger.info("Wrote " + read +  " bytes from file '" + localfile + "' on remote server '" + sftpinfo.getHost() + "' took " + (new Date().getTime()-now.getTime()) + " ms");
                        }
                        // If we are zipping the binary files, add the file pointers to a list for later reference
                        else if ( zipBinaries )
                        {
                            files.add(f);
                        }
                    }
                    else if ( origfile != null )
                    {
                        File f = new File(origfile);
                        files.add(f);
                    }
                    
                    // Add row to data so any change filters can process it
                    data.addDataRow(rowdata);
                }
                /*catch (FileSystemException e)
                {
                    String msg = "Failure writing remote file: " + directory + "/" + zipFilename;
                    logger.warn(msg, e);
                    it.remove();
                    joindata.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, msg));
                    errors.addDataRow(joindata);
                    continue;
                }*/
                catch (IOException e)
                {
                    String msg = "Unable to write file: " + localfile + " due to row level exception: " + e.getMessage();
                    logger.warn(msg, e);
                    it.remove();
                    joindata.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, msg));
                    errors.addDataRow(joindata);
                    continue;
                }
                catch (RowLevelException e)
                {
                    String msg = "Unable to write binary file: " + localfile + " due to row level exception: " + e.getMessage();
                    logger.warn(msg, e);
                    it.remove();
                    joindata.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, msg));
                    errors.addDataRow(joindata);
                    continue;
                }
                catch (ETLException e)
                {
                    throw e;
                }
            }

            // If we are to zip the binary files up do so here
            if ( zipBinaries && !files.isEmpty() )
            {
                File file = getZipFileForWriting(jobnum, directory, zipFilename, origRoot);
                System.out.println(getTaskName() + ": Adding " + files.size() + " binary files to zip archive: " + file.getCanonicalPath());
                logger.info("Zipping binary files to zip file: " + file.getCanonicalPath());

                int BUFFER = 2048;
                byte[] data = new byte[BUFFER];
                FileOutputStream fos = null;
                try
                {
                    fos = new FileOutputStream(file);
                    zos = new ZipOutputStream(new BufferedOutputStream(fos));
                    for ( File f: files )
                    {
                        BufferedInputStream bis = null;
                        try
                        {
                            fis = new FileInputStream(f);
                            bis = new BufferedInputStream(fis, BUFFER);
                            ZipEntry entry = new ZipEntry(f.getName());
                            zos.putNextEntry(entry);

                            while ( (count = bis.read(data, 0, BUFFER)) != -1 )
                            {
                                zos.write(data, 0, count);
                            }
                        }
                        finally
                        {
                            if ( bis != null )
                            {
                                bis.close();
                            }
                            if ( fis != null )
                            {
                                fis.close();
                            }
                        }
                    }
                }
                finally
                {
                    if ( zos != null )
                    {
                        zos.close();
                    }
                    if ( fos != null )
                    {
                        fos.close();
                    }
                }


                logger.info(files.size() + " files added to zip archive: " + file.getCanonicalPath());

                // Check sftp connection and upload file if needed
                if ( sftpinfo != null )
                {
                    Date now = new Date();
                    String remoteFile = directory + "/" + zipFilename;
                    logger.info("Need to write remote file to " + sftpinfo.getHost() + ": " + remoteFile);

                    long read = sftp.put(file, remoteFile, SSHFileTransfer.WriteMode.overwrite, FileAdapter.BYTE_INTERVAL);
                    logger.info("Wrote " + read +  " bytes from file '" + remoteFile + "' on remote server '" + sftpinfo.getHost() + "' took " + (new Date().getTime()-now.getTime()) + " ms");
                }

                if ( archiveOriginal )
                {
                    it = iterateData.getMappedData().iterator();
                    for ( int i=1; it.hasNext(); i++ )
                    {
                        Map<String,DataCell> rowdata = it.next();
                        String origfile = extractPropertyValue(AdapterProperty.fileLocation, rowdata, String.class, false);
                        File f = new File(origfile);
                        archiveFileInput(jobnum, f);
                    }
                }
            }
        }
        catch (JSchException e)
        {
            String error = "Failure writing remote file: " + directory + "/" + zipFilename;
            logger.warn(error, e);
            for ( Map<String,DataCell> rowdata: data.getMappedData() )
            {
                rowdata.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, error + ": " + e.getMessage()));
                errors.addDataRow(rowdata);
            }

            data.clearData();
        }
        catch (IOException e)
        {
            throw new ETLException("Failed to zip binary files to: " + file, e);
        }
        finally
        {
            if ( fis != null )
            {
                try
                {
                    fis.close();
                }
                catch (IOException e)
                {
                    logger.warn("Failed to close file", e);
                }
            }

            if ( zos != null )
            {
                try
                {
                    zos.close();
                }
                catch (IOException e)
                {
                    logger.warn("Failed to close file", e);
                }
            }

            if ( sftp != null )
            {
                sftp.disconnect();
            }
        }
        
        return errors;
    }

    private void archiveFileInput(String jobnum, File file)
    {
        try
        {
            if ( archiveOriginal && file.exists() )
            {
                DataArchiver arch = wrapper.getDataArchiver();
                File inputDir = new File(arch.getRunningArchiveDirectory(jobnum), Processor.INPUT_DIR + "/");
                inputDir.mkdirs();
                if ( file.renameTo(new File(inputDir.getCanonicalFile(), file.getName())) )
                {
                    logger.info("Input file " + file.getCanonicalPath() + " moved to: " + inputDir.getCanonicalPath());
                }
                else
                {
                    logger.warn("Unable to move input file " + file.getCanonicalPath() + " to: " + inputDir.getCanonicalPath());
                }
            }
        }
        catch (IOException e)
        {
            logger.warn("Unable to archive file input", e);
        }
    }
    
    private File getFileForReading(String jobnum, String path)
        throws IOException, ETLException
    {
        File readfile = null;
        if ( sftpinfo == null )
        {
            readfile = new File(path);
        }
        else
        {
            SSHFileTransfer sftp = null;

            try
            {
                File f = null;
                Date now = new Date();
                DataArchiver arch = wrapper.getDataArchiver();
                File input = new File(arch.getRunningArchiveDirectory(jobnum), Processor.INPUT_DIR + "/");
                input.mkdirs();

                f = new File(input, new File(path).getName());
                logger.info("Need to extract remote file from " + sftpinfo.getHost() + ": " + path);
                sftp = new SSHFileTransfer(sftpinfo);
                SFTPReadFile rf = sftp.get(path, f.getCanonicalPath(), FileAdapter.BYTE_INTERVAL);
                logger.info("Read " + rf.getBytesRead() +  " bytes from file '" + path + "' on remote server '" + sftpinfo.getHost() + "' took " + (new Date().getTime()-now.getTime()) + " ms");
                readfile = f;
            }
            catch (JSchException e)
            {
                throw new IOException(e);
            }
            finally
            {
                if ( sftp != null )
                {
                    sftp.disconnect();
                }
            }
        }
        
        return readfile;
    }
    
    private <E extends Object> E extractPropertyValue(AdapterProperty prop, Map<String,DataCell> map, Class<E> clazz, boolean allowNull)
        throws ETLException
    {
        E val = null;
        Object o = null;
        String argname = "";
        Argument arg = arguments.get(prop);
        if ( arg != null )
        {
            if ( arg.getArgumentType() == Argument.Type.column )
            {
                argname = String.valueOf(arg.getValue());
                DataCell cell = map.get(argname);
                if ( cell != null )
                {
                    o = cell.getValue();
                }
                else if ( data.getColumnDefinition(argname) != null )
                {
                    o = null;
                }
                else
                {
                    throw new ETLException("Undefined column " + argname + ", unable to extract required value from source data.");
                }

                if ( !allowNull && o == null )
                {
                    throw new RowLevelException("Column " + argname + " value is null and null values are not allowed for " + prop + " arguments.");
                }
                else if ( val != o && !(clazz.isAssignableFrom(o.getClass())) )
                {
                    throw new RowLevelException("Column data referenced by " + argname + " is not an instanceof BinaryObject. Unable to output binary data to a file.");
                }
                else
                {
                    val = (E)o;
                }
            }
            else
            {
                throw new ETLException(argname + " must be a valid column reference in source data. Constants are not supported.");
            }
        }
        else if ( !allowNull )
        {
            throw new ETLException("No arguments defined with property name: " + prop);
        }
        
        return val;
    }
}
