/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.tasks;

import com.blackboard.services.etl.data.Argument;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.jaxb.ArgumentComplexType;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.EmailOutputTaskComplexType;
import com.blackboard.services.etl.jaxb.EtlJob.MasterData.Data;
import com.blackboard.services.etl.jaxb.EtlTaskComplexType;
import com.blackboard.services.etl.jaxb.OutputTaskComplexType;
import com.blackboard.services.etl.process.Processor;
import com.blackboard.services.etl.wrapper.ConnectionFactory;
import com.blackboard.services.etl.wrapper.DataArchiver;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.blackboard.services.security.ConnectionManager;
import com.blackboard.services.security.exception.BbTSSecurityException;
import com.blackboard.services.security.object.EmailConnection;
import com.blackboard.services.security.object.NamedKey;
import com.blackboard.services.security.object.RegisterableApplication;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Provider;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.SimpleEmail;
import org.apache.log4j.Logger;

/**
 *
 * @author crusnak
 */
public class EmailAdapter extends AbstractAdapter
{
    public static final String COL_TO_MSG =         "RECIPIENTS";

    private static final String PROP_SMTP_HOST =    "mail.smtp.host";
    private static final String PROP_SMTP_PORT =    "mail.smtp.port";
    private static final String PROP_SMTP_AUTH =    "mail.smtp.auth";
    private static final String PROP_SMTP_USER =    "mail.smtp.user";
    private static final String PROP_SMTP_TLS =     "mail.smtp.starttls.enable";
    private static final String PROP_SMTP_PART =    "mail.smtp.sendpartial";
    private static final String PROP_SMTP_SSL =     "mail.smtp.ssl.enable";

    private static enum Protocol { smtp, imap, pop3 };

    private Object ea = null;
    private RelationalContainer data = null;
    private Protocol protocol = Protocol.smtp;
    private EmailConnection emailInfo = null;
    private InternetAddress from;
    private ArrayList<InternetAddress> to = new ArrayList();
    private String subject = "";
    private Argument subjectArg = null;
    private String contentType = "text/plain";
    private String body = "";
    private Argument templateDirectory = null;
    private Argument templateFilename = null;
    private Argument templateContentType = null;
    private Argument sender = null;
    private ArrayList<Argument> recipients = new ArrayList();
    private ArrayList<Argument> files = new ArrayList();
    private ArrayList<File> attachments = new ArrayList();
    private boolean iterate = false;
    private boolean attachData = false;
    private boolean authEnabled = false;
    private boolean sendEmpty = true;
    private boolean usesSecman = false;
    private File origRoot;

    public EmailAdapter(EmailConnection conn, String adapterName)
        throws ETLException
    {
        if ( conn != null )
        {
            logger = Logger.getLogger(this.getClass());
            protocol = Protocol.smtp;
            emailInfo = conn;
            direction = DataDirection.Outgoing;
            name = adapterName;
            data = new RelationalContainer(name);
            authEnabled = emailInfo.isAuthenticationRequired();
        }
        else
        {
            throw new ETLException("Unable to create EmailAdapter, EmailConnection is null");
        }
    }
    
    public EmailAdapter(EmailOutputTaskComplexType xml, ETLJobWrapper w)
        throws ETLException
    {
        ea = xml;
        direction = DataDirection.Outgoing;
        wrapper = w;
        setServerInfo(xml);
        setEmailSettings(xml);
    }

    public EmailAdapter(EtlTaskComplexType ct, OutputTaskComplexType xml, File root, ETLJobWrapper w)
        throws ETLException
    {
        super(ct, xml, w);
        origRoot = root;
        setData(dataSet);

        if ( xml.getEmailOutput() == null )
        {
            throw new ETLException("Expecting EmailOutput in OutputAdapter");
        }

        EmailOutputTaskComplexType out = xml.getEmailOutput();
        ea = out;
        setServerInfo(out);
        setEmailSettings(out);
    }

    private void setServerInfo(EmailOutputTaskComplexType out)
        throws ETLException
    {
        usesSecman = out.getServer().isUseSecurityManager();
        emailInfo = ConnectionFactory.getEmailConnection(out, this);
        
        /*if ( out != null && out.getServer() != null )
        {
            EmailOutputTaskComplexType.Server server = out.getServer();
            setProtocol(server.getProtocol());
            if ( server.getEmailConnection() != null )
            {
                try
                {
                    String appname = server.getEmailConnection().getApplication();
                    String key = server.getEmailConnection().getKey();

                    RegisterableApplication app = new RegisterableApplication(appname);
                    NamedKey nkey = new NamedKey(appname, key);
                    System.out.println("conman key: " + nkey);
                    ConnectionManager conman = ConnectionManager.getInstance(app);
                    emailInfo = conman.getEmailConnection(nkey);
                    if ( emailInfo == null )
                    {
                        throw new ETLException("Email connection not registered with security manager: " + nkey);
                    }
                    
                    authEnabled = emailInfo.isAuthenticationRequired();
                }
                catch (BbTSSecurityException e)
                {
                    throw new ETLException("Unable to access SecurityManger to get email connection", e);
                }
            }
            else
            {
                emailInfo = new EmailConnection("", "");
                emailInfo.setHost(server.getHost());
                emailInfo.setPort(server.getPort());
                if ( server.getUsername() != null && server.getUsername().trim().length() > 0 &&
                     server.getPassword() != null && server.getPassword().trim().length() > 0 )
                {
                    authEnabled = true;
                    emailInfo.setUsername(server.getUsername());
                    emailInfo.setPassword(server.getPassword());
                }

                // Initiate email settings
                if ( server.getSettings() != null )
                {
                    emailInfo.setStartTLS(server.getSettings().isStartTLS());
                    emailInfo.setSSLEnabled(server.getSettings().isEnableSSL());
                    emailInfo.setSendPartial(server.getSettings().isSendPartial());
                }
            }
        }*/
    }

    private void setEmailSettings(EmailOutputTaskComplexType out)
        throws ETLException
    {
        if ( out != null )
        {
            iterate = out.isIterate();
            attachData = out.isAttachData();
            sendEmpty = out.isSendEmpty();
            subjectArg = new Argument(out.getSubject(), this); 
            setEmailBody(out.getBody());
            
            // Check if an email template is defined and if so, use that
            EmailOutputTaskComplexType.BodyTemplate b = out.getBodyTemplate();
            if ( b != null )
            {
                if ( b.getDirectory() != null )
                {
                    templateDirectory = new Argument(b.getDirectory(), this);
                }
                else
                {
                    throw new ETLException("<etl:directory> element under <etl:bodyTemplate> must exist");
                }
                
                if ( b.getTemplate() != null )
                {
                    templateFilename = new Argument(b.getTemplate(), this);
                }
                else
                {
                    throw new ETLException("<etl:template> element under <etl:bodyTemplate> must exist");
                }
                
                if ( b.getContentType() != null )
                {
                    templateContentType = new Argument(b.getContentType(), this);
                }
            }
            else
            {
                body = out.getBody().getValue();
                setEmailContentType(out.getBody().getContentType());
            }

            if ( out.getSender() != null )
            {
                sender = new Argument(out.getSender(), this);
            }

            for ( ArgumentComplexType ct: out.getRecipients().getRecipient() )
            {
                recipients.add(new Argument(ct, this));
            }
            
            if ( out.getAttachments() != null )
            {
                for ( ArgumentComplexType ct: out.getAttachments().getAttachment() )
                {
                    files.add(new Argument(ct, this));
                }
            }
        }
    }

    @Override
    public Adapter.Type getAdapterType()
    {
        return Type.Email;
    }

    @Override
    public Object getBaseObject()
    {
        return ea;
    }

    @Override
    public RelationalContainer getData()
        throws ETLException
    {
        setData(super.getData());
        return data;
    }

    @Override
    public void setData(DataSet d)
    {
        super.setData(d);
        if ( d.getType() != DataSet.Type.relational )
        {
            throw new UnsupportedOperationException("DataSet assigned to " + this.getClass().getSimpleName() + " must be relational");
        }
        data = (RelationalContainer)d;
    }

    public Protocol getProtocol()
    {
        return protocol;
    }

    private void setProtocol(String p)
    {
        protocol = Protocol.valueOf(p);
    }

    public boolean isSendEmpty()
    {
        return sendEmpty;
    }
    
    public void setSendEmpty(boolean empty)
    {
        sendEmpty = empty;
    }
    
    public void setFileAttachments(File[] files)
    {
        removeAllFileAttachments();
        for ( File f: files )
        {
            addFileAttachment(f);
        }
    }

    public void setFileAttachments(List<File> files)
    {
        setFileAttachments(files.toArray(new File[0]));
    }

    public void addFileAttachment(File file)
    {
        attachments.add(file);
    }

    public boolean removeFileAttachment(File file)
    {
        return attachments.remove(file);
    }

    public void removeAllFileAttachments()
    {
        attachments.clear();
    }

    public InternetAddress getSender()
    {
        return from;
    }

    public void setSender(InternetAddress addr)
    {
        from = addr;
    }

    public List<InternetAddress> getRecipients()
    {
        return to;
    }

    public void setRecipients(InternetAddress[] addresses)
    {
        removeAllRecipients();
        for (InternetAddress addr: addresses )
        {
            addRecipient(addr);
        }
    }

    public void setRecipients(List<InternetAddress> addresses)
    {
        setRecipients(addresses.toArray(new InternetAddress[0]));
    }

    public void addRecipient(InternetAddress addr)
    {
        to.add(addr);
    }

    public boolean removeRecipient(InternetAddress addr)
    {
        return to.remove(addr);
    }

    public void removeAllRecipients()
    {
        to.clear();
    }

    public void removeAllFiles()
    {
        files.clear();
    }

    public String getSMTPHost()
    {
        return emailInfo.getHost();
    }

    public void setSMPTHost(String h)
    {
        emailInfo.setHost(h);
    }

    public String getEmailSubject()
    {
        return subject;
    }

    public void setEmailSubject(String sub)
    {
        subject = sub;
    }

    public String getEmailContentType()
    {
        return contentType;
    }
    
    public void setEmailContentType(String ct)
    {
        contentType = ct;
    }
    
    public String getEmailBody()
    {
        return body;
    }

    public void setEmailBody(String b)
    {
        body = b;
    }

    public void setEmailBody(EmailOutputTaskComplexType.Body b)
    {
        body = "";
        
        if ( b != null && b.getValue() != null )
        {
            body = b.getValue();
        }
    }

    public RelationalContainer email(String jobnum)
        throws ETLException
    {
        RelationalContainer errors = null;
        DataColumn ecol = new DataColumn(DataTypes.STRING, RelationalContainer.COL_ERROR_MSG);
        DataColumn scol = new DataColumn(DataTypes.STRING, COL_TO_MSG);

        if ( data != null )
        {
            errors = new RelationalContainer(data.getDataName() + ERROR_DATA_NAME);
            errors.setColumnDefinitions(data.getColumnDefinitions());
            errors.addColumnDefinition(ecol);
            errors.addColumnDefinition(scol);
        }
        
        try
        {
            Properties props = prepareSessionProperties();
            logger.info("Initializing mail session properties: " + props);

            // Prepare the transport
            Session session = Session.getInstance(props);
            Transport transport = session.getTransport(String.valueOf(protocol));
            
            // Only send emails if the task data contains at least one row or the sendEmpty flag is true
            if ( data != null && data.rowCount() > 0 || sendEmpty )
            {
                if ( iterate )
                {
                    int count = data.rowCount();
                    Iterator<Map<String,DataCell>> it = data.getMappedData().iterator();
                    for ( int i=0; it.hasNext(); i++ )
                    {
                        Map<String,DataCell> map = it.next();
                        if ( getTaskName() != null )
                        {
                            data.setCurrentProcessedRow(i);
                            System.out.println(getTaskName() + ": Sending email #" + (i+1) + " out of " + count);
                            logger.info("Sending email #" + (i+1) + " out of " + count);
                        }

                        try
                        {    
                            Message message = prepareEmailMessage(session, jobnum, map);
                            logger.info("Email message successfully prepared....sending");
                            logger.debug("Sender: " + from);
                            logger.debug("Recipients: " + to);
                            logger.debug("Subject: " + message.getSubject());
                            try
                            {
                                MimeMultipart part = (MimeMultipart)message.getContent();
                                String body = "";
                                for ( int e=0; e<part.getCount(); e++ )
                                {
                                    BodyPart bp = part.getBodyPart(e);
                                    if ( bp.getContentType() == "text/plain" )
                                    {
                                        body = String.valueOf(bp.getContent());
                                        break;
                                    }
                                }
                                logger.debug("Body: " + body.replace('\n', ' ').replace('\r', ' '));
                            }
                            catch (IOException e )
                            {
                                logger.warn("Unable to extract email body for debugging", e);
                            }
                            logger.debug("Files attached: " + attachments);

                            // Send the generated message
                            try
                            {
                                if ( authEnabled || emailInfo.isAuthenticationRequired() )
                                {
                                    transport.connect(emailInfo.getHost(), emailInfo.getUsername(), emailInfo.getPassword());
                                }
                                else
                                {
                                    transport.connect(emailInfo.getHost(), null, null);
                                }
                                transport.sendMessage(message, to.toArray(new InternetAddress[0]));
                            }
                            finally
                            {
                                transport.close();
                            }
                        }
                        catch (ETLException e)
                        {
                            String list = "";
                            for ( int j=0; j<to.size(); j++ )
                            {
                                list += to.get(j).getAddress();
                                if ( j < to.size() -1 )
                                {
                                    list += ", ";
                                }
                            }

                            String msg = "Unable to send email because: " + e.getMessage();
                            logger.warn(msg + " | Rowdata: " + map, e);
                            if ( errors != null )
                            {
                                it.remove();
                                map.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, msg));
                                map.put(COL_TO_MSG, new DataCell(scol, list));
                                errors.addDataRow(map);
                            }
                            else
                            {
                                throw e;
                            }
                        }
                        catch (SendFailedException e)
                        {
                            String list = "";
                            for ( int j=0; j<to.size(); j++ )
                            {
                                list += to.get(j).getAddress();
                                if ( j < to.size() -1 )
                                {
                                    list += ", ";
                                }
                            }
                            String msg = "Failed to send email to the following recipients: " + list + " because: " + e.getMessage();
                            logger.warn(msg + " | Rowdata: " + map, e);
                            if ( errors != null )
                            {
                                it.remove();
                                map.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, msg));
                                map.put(COL_TO_MSG, new DataCell(scol, list));
                                errors.addDataRow(map);
                            }
                            else
                            {
                                throw e;
                            }
                        }
                        catch (MessagingException e)
                        {
                            String list = "";
                            for ( int j=0; j<to.size(); j++ )
                            {
                                list += to.get(j).getAddress();
                                if ( j < to.size() -1 )
                                {
                                    list += ", ";
                                }
                            }

                            String msg = "Unable to send email because: " + e.getMessage();
                            logger.warn(msg + " | Rowdata: " + map, e);
                            if ( errors != null )
                            {
                                it.remove();
                                map.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, msg));
                                map.put(COL_TO_MSG, new DataCell(scol, list));
                                errors.addDataRow(map);
                            }
                            else
                            {
                                throw e;
                            }
                        }
                    }
                }
                else
                {
                    Message message = prepareEmailMessage(session, jobnum, null);

                    logger.info("Email message successfully prepared....sending");
                    logger.debug("Sender: " + from);
                    logger.debug("Recipients: " + to);
                    logger.debug("Subject: " + subject);
                    logger.debug("Body: " + body.replace('\n', ' ').replace('\r', ' '));
                    logger.debug("Files attached: " + attachments);

                    // Send the generated message
                    try
                    {
                        if ( authEnabled )
                        {
                            transport.connect(emailInfo.getHost(), emailInfo.getUsername(), emailInfo.getPassword());
                        }
                        else
                        {
                            transport.connect(emailInfo.getHost(), null, null);
                        }
                        transport.sendMessage(message, to.toArray(new InternetAddress[0]));
                    }
                    finally
                    {
                        transport.close();
                    }
                }
            }
        }
        catch (ETLException e)
        {
            // TODO: Need to eventually replace this fatal error by removing all data
            // from task and assigning to error data.
            if ( errors == null )
            {
                throw new ETLException("Unable to send email", e);
            }
            else
            {
                throw new ETLException("Unable to send email", e);
            }
        }
        catch (SendFailedException e)
        {
            String list = "";
            for ( int i=0; i<to.size(); i++ )
            {
                list += to.get(i).getAddress();
                if ( i < to.size() -1 )
                {
                    list += ", ";
                }
            }
            String msg = "Failed to send email to the following recipients: " + list;

            // TODO: Need to eventually replace this fatal error by removing all data
            // from task and assigning to error data.
            if ( errors == null )
            {
                throw new ETLException(msg, e);
            }
            else
            {
                throw new ETLException(msg, e);
            }
        }
        catch (MessagingException e)
        {
            // TODO: Need to eventually replace this fatal error by removing all data
            // from task and assigning to error data.
            if ( errors == null )
            {
                throw new ETLException("Unable to send email", e);
            }
            else
            {
                throw new ETLException("Unable to send email", e);
            }
        }

        return errors;
    }

    private Properties prepareSessionProperties()
    {
        Properties props = new Properties();

        switch (protocol)
        {
            case smtp:
                props.setProperty(PROP_SMTP_HOST, emailInfo.getHost());
                if ( emailInfo.getPort() > 0 )
                {
                    props.setProperty(PROP_SMTP_PORT, String.valueOf(emailInfo.getPort()));
                }

                props.setProperty(PROP_SMTP_AUTH, String.valueOf(emailInfo.isAuthenticationRequired() || authEnabled));
                if ( authEnabled && emailInfo.getUsername() != null && emailInfo.getUsername().trim().length() > 0 )
                {
                    props.setProperty(PROP_SMTP_USER, emailInfo.getUsername());
                }

                props.setProperty(PROP_SMTP_TLS, String.valueOf(emailInfo.isStartTLS()));
                props.setProperty(PROP_SMTP_PART, String.valueOf(emailInfo.isSendPartial()));

                // TODO: need to look into how to handle SSL connection to smtp servers
                //props.setProperty(PROP_SMTP_SSL, String.valueOf(enableSSL));

                break;
            case imap:
                throw new UnsupportedOperationException("Support for imap email protocol not available");
                //break;
            case pop3:
                throw new UnsupportedOperationException("Support for pop3 email protocol not available");
                //break;
        }

        return props;
    }

    private Email prepareEmailMessage(String jobnum)
    {
        Email email = new SimpleEmail();
        email.setHostName("smtp.gmail.com");
        email.setSmtpPort(587);

        return email;
    }

    private Message prepareEmailMessage(Session session, String jobnum, Map<String,DataCell> row)
        throws ETLException, MessagingException
    {
        Message message = new MimeMessage(session);
        Multipart multipart = new MimeMultipart();
        
        String emailSubject = "";
        String emailBody = "";

        // If a sender is defined in the xml task, use that
        if ( sender != null )
        {
            if ( sender.getArgumentType() == Argument.Type.constant )
            {
                setSender(new InternetAddress(String.valueOf(sender.getValue())));
                logger.debug("Sender defined in task as constant, assigning to message: " + from);
            }
            else if ( iterate )
            {
                Object s = sender.getValue();
                if ( s == null )
                {
                    throw new ETLException("Unable to assign sender. Sender argument is not defined.");
                }

                DataCell cell = row.get(String.valueOf(s));
                if ( cell == null )
                {
                    throw new ETLException("Unable to assign sender. Sender argument references non-existing column.");
                }
                else if ( cell.getValue() == null )
                {
                    throw new ETLException("Unable to assign sender. Sender value in data is null and sender cannot be null.");
                }

                setSender(new InternetAddress(String.valueOf(cell.getValue())));
                logger.debug("Sender defined in task as column, assigning to message: " + from);
            }
            else
            {
                    throw new ETLException("Unable to assign sender. Cannot assign a column value when iterate=false");
            }
        }

        // If recipients were defined in the xml task, use that list
        if ( !recipients.isEmpty() )
        {
            logger.info("Recipients defined in task, assigning to message");
            removeAllRecipients();
            for ( Argument arg: recipients )
            {
                if ( arg.getArgumentType() == Argument.Type.constant )
                {
                    // If constant contains multiple addresses parse and add
                    if ( String.valueOf(arg.getValue()).contains(",") || String.valueOf(arg.getValue()).contains(";") )
                    {
                        for ( String addr: String.valueOf(arg.getValue()).split(",|;") )
                        {
                            try
                            {
                                addRecipient(new InternetAddress(addr));
                                logger.debug("Assigning recipient as constant to message: " + addr);
                            }
                            catch (AddressException e)
                            {
                                logger.warn("Unable to parse email address recipient, not adding to recipient list: " + addr, e);
                            }
                        }
                    }
                    else
                    {
                        InternetAddress recip = new InternetAddress(String.valueOf(arg.getValue()));
                        addRecipient(recip);
                        logger.debug("Assigning recipient as constant to message: " + recip);
                    }
                }
                else if ( iterate )
                {
                    Object r = arg.getValue();
                    if ( r == null )
                    {
                        throw new ETLException("Unable to assign recipient. Recipient argument is not defined.");
                    }

                    DataCell cell = row.get(String.valueOf(r));
                    if ( cell == null )
                    {
                        throw new ETLException("Unable to assign recipient. Recipient argument references non-existing column.");
                    }
                    else if ( cell.getValue() == null )
                    {
                        throw new ETLException("Unable to assign recipient. Recipient value in data is null and recipient cannot be null.");
                    }

                    // If column value contains multiple addresses parse and add
                    if ( String.valueOf(cell.getValue()).contains(",") || String.valueOf(cell.getValue()).contains(";") )
                    {
                        for ( String addr: String.valueOf(cell.getValue()).split(",|;") )
                        {
                            try
                            {
                                addRecipient(new InternetAddress(addr));
                                logger.debug("Assigning recipient as column to message: " + addr);
                            }
                            catch (AddressException e)
                            {
                                logger.warn("Unable to parse email address recipient, not adding to recipient list: " + addr, e);
                            }
                        }
                    }
                    else
                    {
                        InternetAddress recip = new InternetAddress(String.valueOf(cell.getValue()));
                        addRecipient(recip);
                        logger.debug("Assigning recipient as column to message: " + recip);
                    }
                }
                else
                {
                    throw new ETLException("Unable to assign recipient. Cannot assign a column value when iterate=false");
                }
            }
        }
        
        // Prepare subject line
        if ( subjectArg != null )
        {
            if ( subjectArg.getArgumentType() == Argument.Type.constant )
            {
                if ( subjectArg.getValue() != null )
                {
                    subject = String.valueOf(subjectArg);
                    logger.debug("Assigning subject as constant to message: " + subject);
                }
            }
            else if ( iterate )
            {
                Object r = subjectArg.getValue();
                if ( r != null )
                {
                    DataCell cell = row.get(String.valueOf(r));
                    if ( cell != null && cell.getValue() != null )
                    {
                        subject = String.valueOf(cell.getValue());
                        logger.debug("Assigning subject as column to message: " + subject);
                    }
                }
            }
            else
            {
                throw new ETLException("Unable to assign subject. Cannot assign a column value when iterate=false");
            }
        }
        
        // If an email body template was defined we need to prepare it here
        if ( templateDirectory != null && templateFilename != null )
        {
            String dir = null;
            String filename = null;
            File fileRoot = null;
            File file = null;
            
            if ( origRoot == null )
            {
                origRoot = new File("");
            }

            // Process template content type
            if ( templateContentType != null && templateContentType.getArgumentType() == Argument.Type.constant )
            {
                if ( templateContentType.getValue() != null )
                {
                    setEmailContentType(String.valueOf(templateContentType.getValue()));
                    logger.debug("Assigning template content type as constant to message: " + contentType);
                }
            }
            else if ( iterate && templateContentType != null )
            {
                Object r = templateContentType.getValue();
                if ( r != null )
                {
                    DataCell cell = row.get(String.valueOf(r));
                    if ( cell != null && cell.getValue() != null )
                    {
                        setEmailContentType(String.valueOf(cell.getValue()));
                        logger.debug("Assigning template content type as column to message: " + contentType);
                    }
                }
            }
            else if ( templateContentType != null )
            {
                throw new ETLException("Unable to reference email template content type. Cannot reference a column value when iterate=false");
            }
            
            // Process template directory first
            if ( templateDirectory.getArgumentType() == Argument.Type.constant )
            {
                if ( templateDirectory.getValue() != null )
                {
                    dir = String.valueOf(templateDirectory.getValue());
                    logger.debug("Assigning template directory as constant to message: " + dir);
                }
            }
            else if ( iterate )
            {
                Object r = templateDirectory.getValue();
                if ( r != null )
                {
                    DataCell cell = row.get(String.valueOf(r));
                    if ( cell != null && cell.getValue() != null )
                    {
                        dir = String.valueOf(cell.getValue());
                        logger.debug("Assigning template directory as column to message: " + dir);
                    }
                }
            }
            else
            {
                throw new ETLException("Unable to reference email template directory. Cannot reference a column value when iterate=false");
            }
            
            // Process template filename
            if ( templateFilename.getArgumentType() == Argument.Type.constant )
            {
                if ( templateFilename.getValue() != null )
                {
                    filename = String.valueOf(templateFilename.getValue());
                    logger.debug("Assigning template filename as constant to message: " + filename);
                }
            }
            else if ( iterate )
            {
                Object r = templateFilename.getValue();
                if ( r != null )
                {
                    DataCell cell = row.get(String.valueOf(r));
                    if ( cell != null && cell.getValue() != null )
                    {
                        filename = String.valueOf(cell.getValue());
                        logger.debug("Assigning template filename as column to message: " + filename);
                    }
                }
            }
            else
            {
                throw new ETLException("Unable to reference email template filename. Cannot reference a column value when iterate=false");
            }

            // Prepare the template directory depending on if a relative or absolute directory was used
            if ( dir != null && dir.trim().length() > 0 )
            {
                Matcher m = FileAdapter.windowsAbsoluteSlashDir.matcher(dir);
                if ( m.matches() )
                {
                    logger.info("Template file uses absolute directory with extra backslash");
                    fileRoot = new File(m.group(1) + ":" + m.group(2));
                }
                else if ( dir.matches(FileAdapter.windowsAbsoluteDir.pattern()) ||
                          dir.matches(FileAdapter.unixAbsoluteDir.pattern()) )
                {
                    logger.info("Template file uses absolute directory");
                    fileRoot = new File(dir);
                }
                else
                {
                    logger.info("Template file uses relative directory");
                    fileRoot = new File(origRoot, dir);
                }
            }
            else
            {
                fileRoot = origRoot;
            }
            
            file = new File(fileRoot, filename);
            setEmailBody(readEmailTemplate(file));
        }

        if ( iterate )
        {
            DataArchiver arch = wrapper.getDataArchiver();
            emailSubject = arch.processEmbeddedBeanScript(subject, row, '$');
            emailBody = arch.processEmbeddedBeanScript(body, row, '$'); 
        }
        else if ( wrapper != null )
        {
            DataArchiver arch = wrapper.getDataArchiver();
            emailSubject = arch.processEmbeddedBeanScript(subject);
            emailBody = arch.processEmbeddedBeanScript(body);
        }
        else
        {
            emailSubject = subject;
            emailBody = body;
        }
        
        if ( !files.isEmpty() )
        {
            removeAllFileAttachments();
            logger.info("Files to attach defined in task, assigning to message");
            for ( Argument arg: files )
            {
                if ( arg.getArgumentType() == Argument.Type.constant )
                {
                    File f = new File(String.valueOf(arg.getValue()));
                    if ( !f.exists() )
                    {
                        logger.warn("File does not exist, not attaching to email message: " + f);
                    }
                    addFileAttachment(f);
                    logger.debug("Assigning file as constant to message: " + f);
                }
                else if ( iterate )
                {
                    Object r = arg.getValue();
                    if ( r == null )
                    {
                        throw new ETLException("Unable to assign recipient. Recipient argument is not defined.");
                    }

                    DataCell cell = row.get(String.valueOf(r));
                    if ( cell == null )
                    {
                        throw new ETLException("Unable to assign recipient. Recipient argument references non-existing column.");
                    }
                    else if ( cell.getValue() == null )
                    {
                        throw new ETLException("Unable to assign recipient. Recipient value in data is null and recipient cannot be null.");
                    }

                    File f = new File(String.valueOf(cell.getValue()));
                    if ( !f.exists() )
                    {
                        logger.warn("File does not exist, not attaching to email message: " + f);
                    }
                    addFileAttachment(f);
                    logger.debug("Assigning file as column to message: " + f);
                }
                else
                {
                    throw new ETLException("Unable to assign file attachment. Cannot assign a column value when iterate=false");
                }
            }
        }

        // If attaching the data of this task then create the task data in a temporary file and assign
        // to the internal variable
        if ( attachData )
        {
            logger.info("Attaching data from task");

            try
            {
                addFileAttachment(writeTemporaryTaskData(jobnum));
            }
            catch (ETLException e)
            {
                logger.warn("Unable to write task data to file to attach to email message", e);
            }
        }

        // Assign sender and recipients to the message
        message.setFrom(from);
        for ( InternetAddress addr: to )
        {
            message.addRecipient(Message.RecipientType.TO, addr);
        }

        // Assign the content of the message
        message.setSubject(emailSubject);
        BodyPart bp = new MimeBodyPart();
        bp.setContent(emailBody, contentType);
        multipart.addBodyPart(bp);

        // Assign the file attachments to the message
        for ( File f: attachments )
        {
            try
            {
                MimeBodyPart mbp = new MimeBodyPart();
                mbp.attachFile(f);
                multipart.addBodyPart(mbp);
            }
            catch (IOException e)
            {
                logger.warn("Unable to attach file: " + f, e);
            }
        }
        
        message.setContent(multipart);

        return message;
    }
    
    private String readEmailTemplate(File f)
        throws ETLException
    {
        String body = "";
        
        try
        {
            // If the email template file exists read it
            if ( f.exists() )
            {
                body = "";
                try
                {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(f))); 
                    // Keep reading while there are lines to read
                    for ( int i=1; FileAdapter.isReaderReady(reader); i++ )
                    {
                        body += FileAdapter.readLineFromReader(reader) + "\n";
                    }
                }
                catch (IOException e)
                {
                    throw new ETLException("Failed reading template file defining email body: ", e);
                }
            }
            else
            {
                throw new ETLException("Unable to find template file defining email body: " + f.getCanonicalPath());
            }        

            logger.info("Successfully read email template from file: " + f.getCanonicalPath());
        }
        catch (IOException e)
        {
            throw new ETLException(e);
        }
        
        return body;
    }

    private File writeTemporaryTaskData(String jobnum)
        throws ETLException
    {
        DataArchiver archiver = wrapper.getDataArchiver();
        File dir = archiver.getRunningArchiveDirectory(jobnum);
        File temp = new File(dir, name + Processor.TXT_EXT);
        try
        {
            archiver.writeDataToTextFile(data, temp);
        }
        catch (IOException e)
        {
            throw new ETLException("Unable to write task data to file: " + temp, e);
        }
        temp.deleteOnExit();

        return temp;
    }

}
