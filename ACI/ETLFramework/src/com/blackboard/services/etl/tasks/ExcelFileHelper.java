/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.tasks;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author crusnak
 */
public class ExcelFileHelper 
{
    private Workbook workbook = null;
    private Sheet currentWorksheet = null;
    private Row currentRow = null;
    private Cell currentCell = null;
    private File file = null;
    
    public ExcelFileHelper(File f)
        throws IOException
    {
            if ( f.exists() )
            {
                FileInputStream fis = new FileInputStream(f);
                Workbook wb = new XSSFWorkbook(fis);
                workbook = wb;               
                file = f;
            }
            else
            {
                workbook = new XSSFWorkbook();
                file = f;
            }
    }
    
    public ExcelFileHelper(InputStream is)
        throws IOException
    {
        if ( is != null && is.available() > 0 )
        {
            Workbook wb = new XSSFWorkbook(is);
            workbook = wb;
        }
        else
        {
            workbook = new XSSFWorkbook();
        }
    }

    @Override
    protected void finalize() throws Throwable 
    {
        super.finalize(); 
        if ( workbook != null )
        {
            workbook.close();
        }
    }   
    
    public File getReferencedFile()
    {
        return file;
    }
    
    public Workbook getWorkbook()
    {
        return workbook;
    }
    
    public Sheet getCurrentWorksheet()
    {
        if ( currentWorksheet == null )
        {
            try
            {
                currentWorksheet = workbook.getSheetAt(0);
            }
            catch (IllegalArgumentException e)
            {
                currentWorksheet = workbook.createSheet("Worksheet1");
            }
        }
        
        return currentWorksheet;
    }
    
    public Sheet setCurrentWorksheet(String name)
    {
        if ( name == null || name.trim().length() == 0 )
        {
            currentWorksheet = getCurrentWorksheet();
        }
        else 
        {
            System.out.println(workbook.getSheet(name));
            if ( ( currentWorksheet = workbook.getSheet(name) ) == null )
            {
                System.out.println("Creating worksheet with name as none exists: " + name);
                currentWorksheet = workbook.createSheet(name);
            }
        }
        
        return currentWorksheet;
    }
    
    public Row getCurrentRow()
    {
        if ( currentRow == null )
        {
            currentRow = getCurrentWorksheet().getRow(currentWorksheet.getTopRow());
            if ( currentRow == null )
            {
                currentRow = currentWorksheet.createRow(0);
            }
        }
        
        return currentRow;
    }
    
    public Row getLastRow()
    {
        currentRow = getCurrentWorksheet().getRow(currentWorksheet.getLastRowNum());
        return currentRow;
    }
    
    public int getLastRownum()
    {
        currentRow = getLastRow();
        return currentRow.getRowNum();
    }
    
    public Row getFirstRow()
    {
        currentRow = getCurrentWorksheet().getRow((currentWorksheet.getFirstRowNum()));
        return currentRow;
    }

    public int getFirstRownum()
    {
        currentRow = getFirstRow();
        return currentRow.getRowNum();
    }
    
    
    public Row setCurrentRow(int rownum)
    {
        currentRow = getCurrentWorksheet().getRow(rownum);
        if ( currentRow == null )
        {
            currentRow = currentWorksheet.createRow(rownum);
        }
        
        return currentRow;
    }
    
    public Row getNextRow()
    {
        currentRow = setCurrentRow(currentRow.getRowNum() + 1);
        return currentRow;
    }
    
    public Map<Integer,String> getHeaderValues()
    {
        return getHeaderValues(0);
    }
    
    public Map<Integer,String> getHeaderValues(int offset)
    {
        Map<Integer,String> headers = new LinkedHashMap();
        Row r = currentWorksheet.getRow(currentWorksheet.getTopRow() + offset);
        if ( r != null )
        {
            int start = r.getFirstCellNum();
            int end = r.getLastCellNum();

            for ( int i=start; i<end; i++ )
            {
                Cell c = r.getCell(i);
                if ( c != null )
                {
                    headers.put(i, r.getCell(i).getStringCellValue());
                }
            }
        }
        
        return headers;
    }
    
    public List<String> getRowData()
    {
        ArrayList<String> data = new ArrayList();
        Row row = getCurrentRow();
        int start = row.getFirstCellNum();
        int end = row.getLastCellNum();
        
        for ( int i=start; i<end; i++ )
        {
            Cell c = row.getCell(i);
            if ( c != null )
            {
                data.add(c.getStringCellValue());
            }
            else
            {
                data.add("");
            }
        }
        
        return data;
    }
    
    public List<String> getRowData(int rownum)
    {
        setCurrentRow(rownum);
        return getRowData();
    }
    
    public void writeWorkbook(OutputStream os)
        throws IOException
    {
        if ( os != null )
        {
            try
            {
                workbook.write(os);
            }
            finally
            {
                os.close();
                workbook.close();
            }
        }
        else
        {
            throw new IOException("Output stream is not defined, unable to write workbook to stream.");
        }
    }
    
    public void writeWorkbook(File file)
        throws IOException
    {
        writeWorkbook(new FileOutputStream(file));
    }
    
    public String getColumnName(int colnum)
    {
        String s = "";
        int c = colnum + 1;
        if ( c / 27 > 0 )
        {
            s += getColumnName(c / 27 - 1);
        }
        
        s += String.valueOf((char)((colnum % 26) + 65));
        
        return s;
    }
    
    public void clearCurrentSheet()
    {
        Sheet s = getCurrentWorksheet();
        Iterator<Row> it = s.rowIterator();
        while ( it.hasNext() )
        {
            it.next();
            it.remove();
        }
        
        currentRow = null;
    }
    
    public static void main(String[] args)
    {
        try
        {
            File f = new File("test.xlsx");
            ExcelFileHelper fh = new ExcelFileHelper(f);
            System.out.println(fh.getColumnName(676));
            //System.exit(0);
            
            Workbook wb = fh.getWorkbook();
            Sheet s = fh.setCurrentWorksheet("Departments2");
            Row r = null;
            Cell c = null;

            System.out.println("First rownum: " + s.getFirstRowNum());
            System.out.println("Last rownum: " + s.getLastRowNum());
            System.out.println("Headers: " + fh.getHeaderValues());
            
            // Set header
            r = s.createRow(s.getTopRow());
            for ( int j=0; j<10; j++ )
            {
                c = r.createCell(j);
                c.setCellValue("Header " + fh.getColumnName(j) + 1);
            }
            
            System.out.println("Headers: " + fh.getHeaderValues());
            int start = s.getLastRowNum() + 1;
            int end = start + 30;
            
            int lastRownum = fh.getCurrentWorksheet().getLastRowNum();
            //for ( fh.getCurrentRow(); fh.getCurrentRow().getRowNum() < lastRownum; fh.getNextRow() )
            //{
            for ( int i=start; i<end; i++ )
            {
                r = s.createRow(i);
                for ( int j=0; j<10; j++ )
                {
                    c = r.createCell(j);
                    //System.out.println(fh.getColumnName(j));
                    c.setCellValue("" + fh.getColumnName(j) + (i+1));
                }
                System.out.println("Rowdata: " + fh.getRowData());
            }
            
            fh.writeWorkbook(f);
            
            System.out.println(f.getCanonicalPath());
            if ( f.length() == 0 )
            {
                f.deleteOnExit();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace(System.err);
        }
    }
}
