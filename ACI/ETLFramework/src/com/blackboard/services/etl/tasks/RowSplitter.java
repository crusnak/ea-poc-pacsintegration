/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.tasks;

import com.blackboard.services.etl.data.Argument;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.exception.RowLevelException;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.EtlTaskComplexType;
import com.blackboard.services.etl.jaxb.ExtractControlComplexType;
import com.blackboard.services.etl.jaxb.SplitRowsComplexType;
import com.blackboard.services.etl.process.Processor;
import static com.blackboard.services.etl.tasks.AbstractTask.ERROR_DATA_NAME;
import static com.blackboard.services.etl.tasks.FileAdapter.unixAbsoluteDir;
import static com.blackboard.services.etl.tasks.FileAdapter.windowsAbsoluteDir;
import static com.blackboard.services.etl.tasks.FileAdapter.windowsAbsoluteFile;
import static com.blackboard.services.etl.tasks.FileAdapter.windowsAbsoluteSlashDir;
import static com.blackboard.services.etl.tasks.FileAdapter.windowsAbsoluteSlashFile;
import static com.blackboard.services.etl.tasks.FileAdapter.windowsRelativeFile;
import com.blackboard.services.etl.wrapper.DataArchiver;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;

/**
 *
 * @author crusnak
 */
public class RowSplitter extends AbstractTask
{
    private SplitRowsComplexType base = null;
    private RelationalContainer data = null;
    private char delimiter = '\t';
    private String column = null;
    private String identityColumn = null;
    boolean addSource = false;
    
    public RowSplitter(EtlTaskComplexType ct, SplitRowsComplexType xml, File root, ETLJobWrapper w)
        throws ETLException
    {
        super(ct, w);
        setData(dataSet);
        
        base = xml;
        column = xml.getColumn();
        identityColumn = xml.getSourceIdentityColumn();
        delimiter = getDelimiterChar(xml.getDelimiter());
        addSource = xml.isAddSourceRow();
    }

    @Override
    public Object getBaseObject() 
    {
        return base;
    }

    @Override
    public ETLTask.ClassType getClassType() 
    {
        return ETLTask.ClassType.RowSplitter;
    }
    
    @Override
    public DataSet getData() 
        throws ETLException 
    {
        setData(super.getData());
        return data;
    }

    @Override
    public void setData(DataSet d) 
    {
        super.setData(d);
        if ( d.getType() != DataSet.Type.relational )
        {
            throw new UnsupportedOperationException("DataSet assigned to " + this.getClass().getSimpleName() + " must be relational");
        }
        data = (RelationalContainer)d;
    }

    public String getColumnName() 
    {
        return column;
    }

    public void setColumnName(String name) 
    {
        this.column = name;
    }
    
    public String getSourceIdentityColumnName() 
    {
        return column;
    }

    public void setSourceIdentityColumnName(String name) 
    {
        this.column = name;
    }

    public boolean isSourceRowAdded()
    {
        return addSource;
    }

    public void setSourceRowAdded(boolean b)
    {
        this.addSource = b;
    }
    
    public char getDelimiter() 
    {
        return delimiter;
    }

    public void setDelimiterChar(char c) 
    {
        this.delimiter = c;
    }
        
    public void setDelimiterChar(String c)
        throws ETLException
    {
        this.delimiter = getDelimiterChar(c);
    }      

    public RelationalContainer splitRows()
        throws ETLException
    {
        DataColumn ecol = new DataColumn(DataTypes.STRING, RelationalContainer.COL_ERROR_MSG);
        RelationalContainer errors = new RelationalContainer(data.getDataName() + ERROR_DATA_NAME);
        errors.setColumnDefinitions(data.getColumnDefinitions());
        errors.addColumnDefinition(ecol);
        
        DataColumn icol = null;
        if ( identityColumn != null && identityColumn.trim().length() > 0 )
        {
            logger.info("sourceIdentityColumn defined as " + identityColumn + ". Adding column to task data set.");
            icol = new DataColumn(DataTypes.BOOLEAN, identityColumn);
            data.addColumnDefinition(icol);
        }

        RelationalContainer iterate = new RelationalContainer(null, data);        
        data.clearData();
        
        try
        {
            if ( iterate.rowCount() > 0 )
            {
                Iterator<Map<String,DataCell>> it = iterate.getMappedData().iterator();
                for ( int i=0; it.hasNext(); i++ )
                {
                    iterate.setCurrentProcessedRow(i+1);
                    System.out.println(getTaskName() + ": Splitting rows on delimiter '" + delimiter + "' #" + (i+1) + " out of " + iterate.rowCount());
                    Map<String,DataCell> row = it.next();
                    DataColumn col = iterate.getColumnDefinition(column);
                    
                    // Validate that the column is defined in the source data
                    if ( col == null )
                    {
                        throw new RowLevelException("Column " + column + " is not defined in the source data");
                    }
                    
                    // If addSource is true, then add the orignal row data as a separate row back.
                    if ( addSource )
                    {
                        HashMap<String,DataCell> newdata = new HashMap();
                        newdata.putAll(row);
                        
                        // If identity column declared add the boolean value
                        if ( icol != null )
                        {
                            newdata.put(identityColumn, new DataCell(icol, true));
                        }
                        
                        logger.trace("Adding source data row: " + newdata);
                        data.addDataRow(newdata);
                    }
                    
                    DataCell cell = row.get(column);
                    if ( cell != null && cell.getValue() != null )
                    {
                        String str = cell.getValue().toString();
                        String[] dvals = str.split(String.valueOf(delimiter));
                        for ( int x=0; x<dvals.length; x++ )
                        {
                            logger.debug("Creating row on split value: " + dvals[x]);

                            HashMap<String,DataCell> newdata = new HashMap();
                            newdata.putAll(row);
                            newdata.put(column, new DataCell(col, dvals[x]));
                            
                            // If identity column declared add the boolean value
                            if ( icol != null )
                            {
                                newdata.put(identityColumn, new DataCell(icol, false));
                            }   
                            
                            logger.trace("Adding split data row: " + newdata);
                            data.addDataRow(newdata);
                        }
                    }
                }
            }
            else
            {
                System.out.println("No rows to split on");
            }
        }
        catch (RowLevelException e)
        {
            String error = "Row level error occurred on row " + iterate.getCurrentProcessedRow();
            logger.warn(error, e);
            for ( Map<String,DataCell> rowdata: data.getMappedData() )
            {
                rowdata.put(RelationalContainer.COL_ERROR_MSG, new DataCell(ecol, error + ": " + e.getMessage()));
                errors.addDataRow(rowdata);
            }

            data.clearData();
        }
        
        return errors;
    }
    
    private char getDelimiterChar(String d)
        throws ETLException
    {
        char delim = 0x40;
        if ( d == null || d.trim().length() == 0 || d.trim().length() > 2 )
        {
            throw new ETLException("Delimiter character must be a single charcter");
        }

        if ( d.trim().length() == 1 )
        {
            delim = d.charAt(0);
        }
        else
        {
            if ( d.trim().equals("\\t") )
            {
                delim = '\t';
            }
            else
            {
                throw new ETLException("Only escape characters supported as delimiters are: \\t");
            }
        }

        return delim;
    }
    
}
