/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.tasks;

import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.etl.exception.ETLException;

/**
 *
 * @author crusnak
 */
public interface ETLTask
{
    public static enum ClassType { Adapter, DataMapping, CustomProcess, ChangeFilter, DataFilter, SubJob, DataCorrelation, DataAggregation, Null, ErrorTask, DataSorter, Control, HierarchyMapping, WebAPIDispatcher, ExtractControl, RowSplitter };
    public static enum TaskType { start, end, task };

    public Object getBaseObject();
    public abstract ClassType getClassType();
    public TaskType getTaskType();
    public String getTaskName();
    public String getNextTaskName();
    public String getSourceDataTaskName();
    public String getErrorHandlerTaskName();
    public boolean isErrorsHandled();
    public void setErrorsHandled(boolean h);
    public DataSet getData() throws ETLException;
    public void setData(DataSet d);
}
