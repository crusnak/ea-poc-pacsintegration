/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.tasks;

import com.blackboard.services.etl.JobLoader;
import com.blackboard.services.etl.data.Argument;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.jaxb.EtlTaskComplexType;
import com.blackboard.services.etl.jaxb.SubJobComplexType;
import com.blackboard.services.etl.process.CommandLineProcessor;
import com.blackboard.services.etl.wrapper.DataArchiver;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.blackboard.services.utils.BeanScript;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author crusnak
 */
public class SubJob extends AbstractTask
{
    public static final String JOB_FILE_EXT =   ".xml";

    private File etlRoot = null;
    private SubJobComplexType base = null;
    private String jobname = null;
    private String iterateDataName = null;
    private ArrayList<Parameter> params = new ArrayList();
    JobLoader subjobLoader = null;

    public SubJob(EtlTaskComplexType ct, SubJobComplexType xml, ETLJobWrapper w, File root)
        throws ETLException
    {
        super(ct, w);
        etlRoot = root;
        setSubJob(xml);
    }

    public Object getBaseObject()
    {
        return base;
    }

    @Override
    public ClassType getClassType()
    {
        return ClassType.SubJob;
    }
    
    private void setSubJob(SubJobComplexType ct)
        throws ETLException
    {
        if ( ct != null )
        {
            jobname = ct.getJobName();
            iterateDataName = ct.getIterateOn();
            for ( SubJobComplexType.Parameter p: ct.getParameter() )
            {
                Parameter param = new Parameter(p, this);
                if ( param.getArgumentType() == Argument.Type.column &&
                     (iterateDataName == null || iterateDataName.trim().length() == 0) )
                {
                    throw new ETLException("Column arguments cannot be assigned to parameter values if iterate-on is null: " + param);
                }
                params.add(new Parameter(p, this));
            }

            // Preload job in order to identify any change filters
            subjobLoader = new JobLoader(etlRoot, jobname + JOB_FILE_EXT);
        }
    }
    
    public ETLJobWrapper getSubJobWrapper(String jobnum, Map<String,String> pmap, ETLJobWrapper parent)
        throws ETLException
    {
        ETLJobWrapper subjob = null;
        
        logger.info("Retrieving sub-job " + jobname);
        subjob = subjobLoader.getETLJobWrapper(pmap, jobnum);
        subjob.setParent(parent);
        
        return subjob;
    }

    public Map<String,String> getParameterMap()
        throws ETLException
    {
        HashMap<String,String> paramMap = new HashMap();
        for ( Parameter p: params )
        {
            paramMap.put(p.getParameterName(), p.getParameterValue());
        }
        return paramMap;
    }

    public Map<String,String> getParameterMap(Map<String,DataCell> rowdata)
        throws ETLException
    {
        HashMap<String,String> paramMap = new HashMap();
        for ( Parameter p: params )
        {
            switch (p.getArgumentType())
            {
                case column:
                    Object val = null;
                    DataCell cell = rowdata.get(p.getParameterValue());
                    if ( cell != null )
                    {
                        val = cell.getValue();
                    }
                    paramMap.put(p.getParameterName(), String.valueOf(val));
                    break;
                case constant:
                    paramMap.put(p.getParameterName(), p.getParameterValue());
                    break;
            }
        }
        return paramMap;
    }
    
    public String getTargetJobName()
    {
        return jobname;
    }

    public void executeSubJob(String jobnum, ETLJobWrapper parent)
        throws ETLException
    {
        ETLJobWrapper subjob = null;
        CommandLineProcessor processor = null;
        if ( iterateDataName == null || iterateDataName.trim().length() == 0 )
        {
            Date startDate = new Date();
            try
            {
                Map<String,String> paramMap = getParameterMap();
                System.out.println("\nStarting sub-job " + jobname);
                System.out.println("Parameters: " + paramMap + "\n");
                logger.info("------------------------Starting sub-job " + jobname + "--------------------------------");
                logger.info("Sub-job parameters: " + paramMap);
                ///JobLoader loader = new JobLoader(etlRoot, jobname + JOB_FILE_EXT);
                //subjob = loader.getETLJobWrapper(paramMap, jobnum);
                subjob = getSubJobWrapper(jobnum, paramMap, parent);
                //subjob.setParent(parent);
                processor = new CommandLineProcessor(subjob, jobnum);
                processor.process();
                logger.info("------------------------Completed sub-job " + jobname + "-------------------------------");
                System.out.println("\nCompleted sub-job " + jobname +"\n");
            }
            finally
            {
                finishSubjob(subjob, jobnum, startDate);
                if ( processor != null )
                {
                    processor.destroy();
                }
            }
        }
        else
        {
            DataSet iterateData = wrapper.retrieveNamedData(iterateDataName);
            int i = 0;
            for ( i=1; i<=iterateData.rowCount(); i++ )
            {
                dataSet.setCurrentProcessedRow(i);
                Date startDate = new Date();
                try
                {
                    Map<String,DataCell> rowdata = ((RelationalContainer)iterateData).getMappedData().get(i-1);
                    Map<String,String> paramMap = getParameterMap(rowdata);
                    System.out.println("\nStarting sub-job " + jobname + " on row #" + i);
                    System.out.println("Parameters: " + paramMap + "\n");
                    logger.info("------------------------Starting sub-job " + jobname + " on row #" + i + "--------------------------------");
                    logger.info("Sub-job parameters: " + paramMap);
                    JobLoader loader = new JobLoader(etlRoot, jobname + JOB_FILE_EXT);
                    subjob = loader.getETLJobWrapper(paramMap, jobnum);
                    subjob.setParent(parent);
                    processor = new CommandLineProcessor(subjob, jobnum);
                    processor.process();
                    logger.info("------------------------Completed sub-job " + jobname + "-------------------------------");
                    System.out.println("\nCompleted sub-job " + jobname +"\n");
                }
                finally
                {
                    finishSubjob(subjob, jobnum, startDate);
                    if ( processor != null )
                    {
                        processor.destroy();
                    }
                }
            }
        }
    }
    
    private void finishSubjob(ETLJobWrapper subjob, String jobnum, Date startDate)
        throws ETLException
    {
        // Unlock job
        if ( subjob != null )
        {
            subjob.getDataArchiver().removeOldArchives();
            subjob.getDataArchiver().unlockJob(jobnum);
            NullTask task = new NullTask(subjob.getJobName(), subjob);
            subjob.getDataStatistics().assignTaskExecutionTime(task, new Date().getTime() - startDate.getTime());
            File stats = subjob.getDataArchiver().outputDataStatistics(jobnum);
            subjob.getDataArchiver().emailCompletedJobInfo(jobnum, null, stats);
        }
    }

    @Override
    public String toString()
    {
        StringBuffer out = new StringBuffer();
        out.append("{");
        out.append("TaskName=" + name);
        out.append(", NextTask=" + next);
        out.append(", JobName=" + jobname);
        out.append("}");
        return out.toString();
    }

    public class Parameter extends Argument
    {
        private String name = null;

        public Parameter(SubJobComplexType.Parameter param, AbstractTask task)
            throws ETLException
        {
            super(param, task);
            name = param.getName();
        }

        public String getParameterName()
        {
            return name;
        }

        public String getParameterValue()
        {
            String out = null;
            Object val = super.getValue();
            if ( val != null )
            {
                out = String.valueOf(val);
            }
            return out;
        }

        public String toString()
        {
            return name + "=" + super.toString();
        }
    }
}
