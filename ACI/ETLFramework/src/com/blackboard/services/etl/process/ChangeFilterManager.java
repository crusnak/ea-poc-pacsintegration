/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.process;

import com.blackboard.services.etl.JobLoader;
import com.blackboard.services.etl.data.Argument;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.utils.CommandLineArgumentManager;
import com.blackboard.services.utils.ResourceLoader;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.jaxb.BooleanComplexType;
import com.blackboard.services.etl.jaxb.BooleanOperators;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.FilterTaskComplexType;
import com.blackboard.services.etl.jaxb.ObjectFactory;
import com.blackboard.services.etl.tasks.AbstractTask;
import com.blackboard.services.etl.tasks.ChangeFilter;
import com.blackboard.services.etl.tasks.FilterBranch;
import com.blackboard.services.etl.wrapper.DataArchiver;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.xml.bind.JAXBElement;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author crusnak
 */
public class ChangeFilterManager
{
    public static enum Action { MANAGE };
    public static final String SYS_PROP_LOG_FILE =  "log.job.file";

    private File logFile = null;
    private File appRoot = null;
    private File jobFile = null;
    private String jobNumber = null;
    private Logger logger = null;
    private String lastJobnum = null;
    private File lastFilterFile = null;
    private File lastTextFile = null;
    private ETLJobWrapper wrapper = null;
    private RelationalContainer filterData = null;
    private ChangeFilter filterTask = null;
    private Map<Integer,Map<String,DataCell>> removed = new LinkedHashMap();
    private Map<Integer,Map<Map<String,DataCell>,Map<String,DataCell>>> modified = new LinkedHashMap();

    public ChangeFilterManager(Map<String,String> params, String logfilename)
        throws ETLException
    {
        String root = params.get(String.valueOf(Processor.ManageParams.etl_root));
        String job = params.get(String.valueOf(Processor.ManageParams.job_name));
        String path = params.get(String.valueOf(Processor.ManageParams.job_path));
        appRoot = new File(".");
        if ( root != null )
        {
            appRoot = new File(root);
        }
        
        System.out.println("Job path=" + path);

        File logdir = new File(appRoot, Processor.LOG_DIR);
        logFile = new File(logdir, logfilename + Processor.LOG_EXT);
        System.setProperty(SYS_PROP_LOG_FILE, logfilename);
        logger = Logger.getLogger(this.getClass()); 

        try
        {
            Map<String,String> args = CommandLineArgumentManager.removeActionArgumentsFromMap(params, Processor.ManageParams.class);
            JobLoader loader = new JobLoader(appRoot, new File(path));
            jobNumber = ETLJobWrapper.jobNumberFormat.format(new Date());
            wrapper = loader.getETLJobWrapper(args, jobNumber);
            //throw new UnsupportedOperationException("The ChangeFilterManager is disabled until further notice.");
        }
        catch (ETLException e)
        {
            logger.error("Failed to initialize etl job", e);
            throw new ETLException("Failed to initialize etl job", e);
        }
        /*catch (IOException e)
        {
            throw new ETLException("Cannot find etl job: " + job, e);
        }*/
    }

    public List<ChangeFilter> getDefinedChangeFilters()
    {
        return wrapper.getTasksByType(ChangeFilter.class);
    }
    
    public RelationalContainer loadFilterData(String filter)
        throws ETLException
    {
        removed.clear();
        modified.clear();
        filterData = (RelationalContainer)getPreviousComparisonData(filter);
        filterTask = (ChangeFilter)wrapper.getJobTaskByName(filter);
        return filterData;
    }
    
    public ChangeFilter getChangeFilterTask()
    {
        return filterTask;
    }
    
    public Map<Integer,Map<String,DataCell>> searchDataWithFilter(List<Filter> filters)
        throws ETLException
    {
        Map<Integer,Map<String,DataCell>> filtered = new LinkedHashMap();
        FilterBranch branch = createFilterBranch(filters);
        for ( int i=0; i<filterData.rowCount(); i++ )
        {
            Map<String,DataCell> rowdata = filterData.getMappedData().get(i);
            if ( filters == null || filters.isEmpty() || branch.testData(rowdata, filterTask) )
            {
                filtered.put(i, rowdata);
            }
        }
        
        return filtered;
    }
    
    public Map<Integer,Map<String,DataCell>> getRemovedEntries()
    {
        return removed;
    }
    
    public Map<Integer,Map<Map<String,DataCell>,Map<String,DataCell>>> getModifiedEntries()
    {
        return modified;
    }
    
    public void findModifications()
    {
        if ( filterData != null )
        {
            for ( Map.Entry<Integer,Map<String,DataCell>> entry: removed.entrySet() )
            {
                System.out.println("Removed index #" + entry.getKey() + ": " + entry.getValue());
            }

            for ( Map.Entry<Integer,Map<Map<String,DataCell>,Map<String,DataCell>>> entry: modified.entrySet() )
            {
                System.out.println("Modified index #" + entry.getKey() + ":");
                System.out.println("\tOLD=" + entry.getValue().keySet() + "\tNEW=" + entry.getValue().values());
            }
        }
    }
    
    public void saveModifications()
        throws ETLException
    {
        File copiedFilterFile = new File(lastFilterFile.getParentFile(), lastFilterFile.getName() + ".old");
        File copiedTextFile = new File(lastTextFile.getParentFile(), lastTextFile.getName() + ".old");

        try
        {
            // Comparison data must be archived here to ensure that the next job picks it up
            FileUtils.copyFile(lastFilterFile, copiedFilterFile);
            FileUtils.copyFile(lastTextFile, copiedTextFile);
            filterTask.setComparisonData(filterData);
            filterTask.archiveComparisonData(lastJobnum);
        }
        catch (IOException e)
        {
            throw new ETLException("Failed to copy old filter data", e);
        }
     }
    
    public List<RowData> extractPrimaryKeyValues(Map<Integer,Map<String,DataCell>> data)
    {
        List<RowData> extract = new ArrayList();
        if ( data != null )
        {
            for ( Map.Entry<Integer,Map<String,DataCell>> entry: data.entrySet() )
            {
                extract.add(new RowData(entry.getKey(), filterTask.getKeyMap().keySet(), filterData.getColumnNames(), entry.getValue()));
            }
        }
        
        return extract;
    }
    
    public Map<String,DataCell> removeDataRow(int index)
    {
        Map<String,DataCell> row = filterData.removeRow(index);
        removed.put(index + removed.size(), row);
        return row;
    }
    
    public boolean updateDataRow(int index, String column, DataCell newcell)
    {
        boolean success = false;
        try
        {
            Map<String,DataCell> rowdata = filterData.getMappedData().get(index);
            DataCell oldcell = rowdata.put(column, newcell);
            success = true;
            Map<Map<String,DataCell>,Map<String,DataCell>> updates = modified.get(index);
            Map<String,DataCell> oldmap = null;
            Map<String,DataCell> newmap = null;
            if ( updates == null )
            {
                updates = new LinkedHashMap();
                oldmap = new LinkedHashMap();
                newmap = new LinkedHashMap();
                updates.put(oldmap, newmap);
                modified.put(index, updates);
            }
            else
            {
                oldmap = new ArrayList<Map<String,DataCell>>(updates.keySet()).get(0);
                newmap = new ArrayList<Map<String,DataCell>>(updates.values()).get(0);
            }
            
            oldmap.put(column, oldcell);
            newmap.put(column, newcell);
        }
        catch (ArrayIndexOutOfBoundsException e) {}
        
        return success;
    }
    
    private FilterBranch createFilterBranch(List<Filter> filters)
        throws ETLException
    {
        FilterBranch branch = null;
        ObjectFactory factory = new ObjectFactory();
        FilterTaskComplexType.Filter filter = factory.createFilterTaskComplexTypeFilter();
        filter.setType(FilterBranch.TYPE_TEST);

        for ( int i=0; i<filters.size(); i++ )
        {
            Filter f = filters.get(i);
            BooleanComplexType bool = factory.createBooleanComplexType();
            BooleanComplexType.Operand1 op1 = factory.createBooleanComplexTypeOperand1();
            BooleanComplexType.Operand2 op2 = factory.createBooleanComplexTypeOperand2();

            DataColumn col = filterData.getColumnDefinition(f.getColumn());
            if ( col == null )
            {
                throw new ETLException("Column " + f.getColumn() + " is not defined in change filter data set");
            }
            
            op1.setType(String.valueOf(Argument.Type.column));
            op1.setValue(f.getColumn());
            op2.setDataType(col.getDataType());
            op2.setType(String.valueOf(Argument.Type.constant));
            op2.setValue(f.getValue());

            bool.setType(f.getOperator());
            bool.setOperand1(op1);
            bool.setOperand2(op2);

            JAXBElement elem = factory.createFilterTaskComplexTypeFilterIf(bool);
            filter.getIfAndAndOrOr().add(elem);
            if ( i < filters.size()-1 )
            {
                JAXBElement and = factory.createFilterTaskComplexTypeFilterAnd(null);
                filter.getIfAndAndOrOr().add(and);
            }
        }

        //logger.debug("Creating FilterBranch: " + JobLoader.convertXMLToString(filter));
        branch = new FilterBranch(filter, filterTask, true);
        return branch;
    }
    
    private DataSet getPreviousComparisonData(String filter)
        throws ETLException
    {
        DataSet dc = new RelationalContainer(filter + "-compare");
        DataArchiver archiver = wrapper.getDataArchiver();
        logger.info("Retrieving comparison data for filter '" + filter + "' from previous job run");

        File prevDir = archiver.getMostRecentArchiveDirectory();
        if ( prevDir != null )
        {
            lastJobnum = prevDir.getName();
            lastFilterFile = new File(prevDir, Processor.CDC_DIR + "/" + filter + Processor.DAT_EXT);
            lastTextFile = new File(prevDir, Processor.CDC_DIR + "/" + filter + Processor.TXT_EXT);
            try
            {
                logger.debug("Loading data from: " + lastFilterFile.getCanonicalPath());
                System.out.println("Loading data from: " + lastFilterFile.getCanonicalPath());
                dc = archiver.readDataFromBinaryFile(lastFilterFile);
            }
            catch (IOException e) 
            { 
                throw new ETLException("Unable to find previous comparison data at: " + lastFilterFile, e); 
            }
            catch (ClassNotFoundException e) {throw new ETLException(e);}
            
        }
        else
        {
            throw new ETLException("Unable to find previous comparison data for: " + filter);
        }

        logger.debug("Previous data=" + dc);
        
        return dc;
    }
    
    public static class Filter
    {
        private String column;
        private BooleanOperators operator;
        private String value;
        
        public Filter(String col, BooleanOperators op, String val)
        {
            column = col;
            operator = op;
            value = val;
        }
        
        public Filter(String col, String op, String val)
        {
            column = col;
            operator = BooleanOperators.valueOf(op);
            value = val;
        }

        public String getColumn()
        {
            return column;
        }

        public void setColumn(String column)
        {
            this.column = column;
        }

        public BooleanOperators getOperator()
        {
            return operator;
        }

        public void setOperator(BooleanOperators operator)
        {
            this.operator = operator;
        }

        public String getValue()
        {
            return value;
        }

        public void setValue(String value)
        {
            this.value = value;
        }
        
        public String toString()
        {
            return column + " " + operator + " '" + value + "'";
        }
    }
            
    public static class RowData
    {
        private int index = -1;
        private Map<String,Object> keyValues = new LinkedHashMap();
        private Map<String,Object> allValues = new LinkedHashMap();
        
        public RowData(int idx, Collection<String> keyColumns, Collection<String> allColumns, Map<String,DataCell> rowdata)
        {
            index = idx;
            for ( String key: keyColumns )
            {
                Object val = null;
                DataCell cell = rowdata.get(key);
                if ( cell != null )
                {
                    val = cell.getValue();
                }
                
                keyValues.put(key, val);
            }
            
            for ( String col: allColumns )
            {
                Object val = null;
                DataCell cell = rowdata.get(col);
                if ( cell != null )
                {
                    val = cell.getValue();
                }
                
                allValues.put(col, val);
            }
        }
        
        public int getRowIndex()
        {
            return index;
        }
        
        public Map<String,Object> getKeyValues()
        {
            return keyValues;
        }
        
        public Map<String,Object> getAllValues()
        {
            return allValues;
        }
        
        public String toString()
        {
            StringBuilder out = new StringBuilder();
            out.append(index + "=");
            out.append("<");
            out.append("KeyValues=" + getKeyValues());
            out.append(", AllValues=" + getAllValues());
            out.append(">");
            return out.toString();
        }
    }
    
    public static void main(String[] args)
    {
        ChangeFilterManager manager = null;
        try
        {
            /*Class clazz = Class.forName("com.blackboard.services.utils.CommandLineArgumentManager");
            java.lang.reflect.Method[] methods = clazz.getDeclaredMethods();
            try
            {
                ArrayList<Class> types = new ArrayList();
                types.add(Class.class);
                types.add(Class.class);
                types.add(HashMap.class);
                types.add(ArrayList.class);
                java.lang.reflect.Method method = findMethod(clazz, "extractCommandLineArguments", types);
                System.out.println(method);
            }
            catch ( NoSuchMethodException e)
            {
                e.printStackTrace();
            }
            Object[] margs = { "MANAGE" };*/
            //Object retval = method.invoke(null, margs);
            //System.out.println(retval + (retval!=null?"(" + retval.getClass() + ")":""));
            /*for ( java.lang.reflect.Method method: methods )
            {
                System.out.println(method);
            }*/
            //args = new String[5];
            //args[0] = String.valueOf(Action.MANAGE);
            //args
            HashMap<Action,Class<? extends Enum>> actionMap = new HashMap();
            actionMap.put(Action.MANAGE, Processor.StartParams.class);
            Map<String,String> paramMap = CommandLineArgumentManager.extractCommandLineArguments(ChangeFilterManager.class, Action.class, actionMap, args);
            String jobname = paramMap.get(String.valueOf(Processor.StartParams.job_name));
            Map<String,String> argmap = CommandLineArgumentManager.removeActionArgumentsFromMap(paramMap, Processor.StartParams.class);
            Collection argvals = argmap.values();
            String argstring = DataArchiver.escapeListForFilename(argvals);
            if ( jobname.endsWith(".xml") )
            {
                jobname = jobname.substring(0, jobname.indexOf(".")) + "_" + argstring;
            }

            /*manager = new ChangeFilterManager(paramMap, jobname + "_manager_" + argstring);
            System.out.println("ChangeFilters=" + manager.getDefinedChangeFilters());
            System.out.println("FilterData=" + manager.loadFilterData("filter-data-changes"));
            ArrayList<Filter> filters = new ArrayList();
            filters.add(new Filter("ID1", BooleanOperators.CONTAINS, "901100"));
            //filters.add(new Filter("Gender", BooleanOperators.EQ, "\"F\""));
            Map<Integer,Map<String,DataCell>> filtered = manager.searchDataWithFilter(filters);
            System.out.println("Filtered " + filtered.size() + " record(s): " + filtered);
            List<RowData> extract = manager.extractPrimaryKeyValues(filtered);
            System.out.println("Rowdata: " + extract);
            manager.removeDataRow(32);
            DataColumn c = manager.filterData.getColumnDefinition("MiddleInitial");
            DataCell initial = new DataCell(c, "\"Z\"");
            manager.updateDataRow(22, c.getColumnName(), initial);
            initial = new DataCell(c, "\"X\"");
            manager.updateDataRow(21, c.getColumnName(), initial);
            manager.findModifications();
            System.out.println(manager.lastJobnum);
            manager.saveModifications();*/
        }
        catch (Exception e)
        {
            e.printStackTrace();
            String linenum = CommandLineProcessor.extractLineNumberFromXMLException(e);
            e.printStackTrace();
            System.err.println(linenum);
            
            if ( manager != null )
            {
                if ( manager.logger != null )
                {
                    manager.logger.error("FATAL ERROR: " + linenum, e);
                }
            }
        }
    }
}
