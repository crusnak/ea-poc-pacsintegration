/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.process;

import com.blackboard.services.etl.JobLoader;
import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.exception.ProcessingException;
import com.blackboard.services.etl.tasks.AbstractTask;
import com.blackboard.services.etl.tasks.Adapter;
import com.blackboard.services.etl.tasks.BinaryAdapter;
import com.blackboard.services.etl.tasks.ChangeFilter;
import com.blackboard.services.etl.tasks.ControlTask;
import com.blackboard.services.etl.tasks.CustomProcess;
import com.blackboard.services.etl.tasks.DataAggregation;
import com.blackboard.services.etl.tasks.DataCorrelation;
import com.blackboard.services.etl.tasks.DataDirection;
import com.blackboard.services.etl.tasks.DataFilter;
import com.blackboard.services.etl.tasks.DataMapping;
import com.blackboard.services.etl.tasks.DataSorter;
import com.blackboard.services.etl.tasks.DatabaseAdapter;
import com.blackboard.services.etl.tasks.DefinedDataAdapter;
import com.blackboard.services.etl.tasks.EmailAdapter;
import com.blackboard.services.etl.tasks.ErrorTask;
import com.blackboard.services.etl.tasks.ExpansionInterval;
import com.blackboard.services.etl.tasks.ExtractControl;
import com.blackboard.services.etl.tasks.FileAdapter;
import com.blackboard.services.etl.tasks.RowSplitter;
import com.blackboard.services.etl.tasks.SubJob;
import com.blackboard.services.etl.tasks.TIAAdapter;
import com.blackboard.services.etl.tasks.WebAPIDispatcher;
import com.blackboard.services.etl.wrapper.DataStatistics;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.blackboard.services.utils.ResourceLoader;
import com.blackboard.services.utils.exception.CompilationException;
import com.blackboard.services.utils.schema.SchemaDocument;
import com.blackboard.services.utils.schema.SchemaNode;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import org.xml.sax.SAXParseException;

/**
 *
 * @author crusnak
 */
public abstract class AbstractETLProcessor implements Processor
{
    protected File appRoot = null;
    protected File jobFile = null;
    protected Logger logger = null;
    protected Date startDate = null;
    protected ETLJobWrapper wrapper = null;
    protected String jobNumber = null;
    protected ArrayList<String> taskPath = new ArrayList();
    
    protected AbstractETLProcessor() {}
    
    protected static URL findJobDefinition(File root, String job)
        throws ETLException
    {
        URL jobURL = null;
        
        try
        {
            try
            {
                jobURL = ResourceLoader.findResourceAsURL(null, root, job);
            }
            catch (IOException e)
            {
                jobURL = ResourceLoader.findResourceAsURL(null, root, job + ".xml");
            }
        }
        catch (IOException e)
        {
            throw new ETLException("Cannot find etl job: " + job, e);
        }
        
        return jobURL;
    }
    
    public void destroy()
    {
        logger.info("Cleaning up processor for job " + wrapper.getJobName());
        wrapper.destroy();
        wrapper = null;
        System.gc();
    }
    
    public static ETLJobWrapper initializeETLWrapper(File root, String jobname)
        throws ETLException
    {
        JobLoader loader = new JobLoader(root, findJobDefinition(root, jobname).getFile());
        return loader.getETLJobWrapper(new HashMap(), "1");
    }

    public static List<CommandLineProcessor.JobMetadata> listAvailableJobs(Map<String,String> params)
        throws IOException
    {
        String root = params.get(String.valueOf(StartParams.etl_root));
        File appRoot = new File(".");
        if ( root != null )
        {
            appRoot = new File(root);
        }
        
        System.out.println("\nAPPLICATION ROOT=" + appRoot);
        
        HashSet<CommandLineProcessor.JobMetadata> jobs = new HashSet();
        List<File> xmlFiles = ResourceLoader.findResourcesAsFiles(".xml");
        for ( File xml: xmlFiles )
        {
            try
            {
                // Attempt to load the xml as an ETLJob to see if this xml file is a valid job.
                String jobname = xml.getName().substring(0, xml.getName().indexOf("."));
                new JobLoader(appRoot, xml);
                jobs.add(new CommandLineProcessor.JobMetadata(jobname, xml));
            }
            catch (ETLException e)
            {
                // Ignore if can't load xml as etl job
                //System.out.println("Can't load " + xml + ": " + e.getMessage());
            }
        }
        
        List<CommandLineProcessor.JobMetadata> list = Collections.list(Collections.enumeration(jobs));
        Collections.sort(list);
        return list;
    }
    
    public static void listValidXPaths(Map<String,String> params)
        throws ETLException
    {
        String root = params.get(String.valueOf(XPathParams.etl_root));
        File appRoot = new File(".");
        if ( root != null )
        {
            appRoot = new File(root);
        }
        
        String schemaName = params.get(String.valueOf(XPathParams.xsd));
        SchemaDocument schema = null;
        
        try
        {
            if ( schemaName.endsWith(".xsd") )
            {
                schema = new SchemaDocument(appRoot, schemaName);
            }
            else
            {
                schema = new SchemaDocument(appRoot, schemaName + ".xsd");
            }

            System.out.println("\nAPPLICATION ROOT=" + appRoot);
            System.out.println("Listing all valid xpaths for: " + schema.getSchemaFile());
            System.out.println("-----------------------------------------");

            Set<String> xpaths = schema.getValidXpathExpressions(schema.getRootElement());
            for ( String path: xpaths )
            {
                String line = "\t" + path + ": (";
                SchemaNode node = schema.retrieveXPathElement(path);
                if ( node.getMinOccurs() != node.getMaxOccurs() )
                {
                    line += node.getMinOccurs() + ".." + (node.getMaxOccurs()==-1?"*":node.getMaxOccurs()) + ")";
                }
                else
                {
                    line += node.getMinOccurs() + ")";
                }
                System.out.println(line);
            }
        }
        catch ( CompilationException e)
        {
            throw new ETLException("Failed to process schema document", e);
        }
    }

    public static String extractLineNumberFromXMLException(Exception e)
    {
        String linenum = "";
        Throwable t = e;
        
        if ( t != null )
        {
            while ( ( t = t.getCause() ) != null )
            {
                if ( t instanceof SAXParseException )
                {
                    linenum = "XML error occurred on line number; " + ((SAXParseException)t).getLineNumber();
                }
            }
        }
        
        return linenum;
    }
    
    
    public String getJobNumber()
    {
        return jobNumber;
    }
    
    public ETLJobWrapper getJobWrapper()
    {
        return wrapper;
    }
    
    public void restart(String jobnum, String restartTask)
        throws ETLException
    {
        throw new UnsupportedOperationException("Restarting a process is currently not supported");        
    }

    protected void processAdapter(Adapter a)
        throws ETLException
    {
        if ( a != null )
        {
            switch (a.getDataDirection())
            {
                case Incoming:
                    processIncomingAdapter(a);
                    break;
                case Outgoing:
                    processOutgoingAdapter(a);
                    break;
            }
        }
    }

    protected void processIncomingAdapter(Adapter a)
        throws ETLException
    {
        DataStatistics stats = wrapper.getDataStatistics();
        if ( a != null )
        {
            logger.debug("Incoming Adapter: " + a.getAdapterType());
            switch (a.getAdapterType())
            {
                case Database:
                    processIncomingDatabaseAdapter((DatabaseAdapter)a);
                    break;
                case File:
                    processIncomingFileAdapter((FileAdapter)a);
                    break;
                case Defined:
                    processIncomingDefinedDataAdapter((DefinedDataAdapter)a);
                    break;
                case TIA:
                    processIncomingTIAAdapter((TIAAdapter)a);
                    break;
                case Binary:
                    processIncomingBinaryAdapter((BinaryAdapter)a);
                    if ( ((BinaryAdapter)a).getIteratorData() != null )
                    {
                        stats.assignTaskInputRowCount(a, ((BinaryAdapter)a).getIteratorData().rowCount());
                    }
                    break;
                case Interval:
                    processIncomingExpansionIntervalAdapter((ExpansionInterval)a);
                    break;
                default:
                    throw new ProcessingException("Unsupported adapter type: " + a);
            }
        }
    }

    protected void processOutgoingAdapter(Adapter a)
        throws ETLException
    {
        DataStatistics stats = wrapper.getDataStatistics();
        if ( a != null )
        {
            logger.debug("Outgoing Adapter: " + a.getTaskName() + "=" + a);
            switch (a.getAdapterType())
            {
                case Database:
                    processOutgoingDatabaseAdapter((DatabaseAdapter)a);
                    break;
                case File:
                    processOutgoingFileAdapter((FileAdapter)a);
                    break;
                case TIA:
                    processOutgoingTIAAdapter((TIAAdapter)a);
                    break;
                case Binary:
                    processOutgoingBinaryAdapter((BinaryAdapter)a);
                    if ( ((BinaryAdapter)a).getIteratorData() != null )
                    {
                        stats.assignTaskInputRowCount(a, ((BinaryAdapter)a).getIteratorData().rowCount());
                    }
                    break;
                case Email:
                    processOutgoingEmailAdapter((EmailAdapter)a);
                    break;
                default:
                    throw new ProcessingException("Unsupported adapter type: " + a);
            }

            // Get a list of change filter tasks associated with this output task
            // so the comparison data can be updated
            Date now = new Date();
            for ( String tname: wrapper.getChangeFilterTasksByOutput(a.getTaskName()) )
            {
                ChangeFilter filter = (ChangeFilter)wrapper.getJobTaskByName(tname);
                filter.updateFromOutput((RelationalContainer)a.getData(), jobNumber);
            }

            if ( wrapper.getChangeFilterTasksByOutput(a.getTaskName()).size() > 0 )
            {
                logger.info("Update of change filters for task '" + a.getTaskName() + "' took " +
                            (double)(new Date().getTime() - now.getTime())/1000.0 +
                            " seconds to process.");
            }
        }
    }

    protected void processIncomingDatabaseAdapter(DatabaseAdapter da)
        throws ETLException
    {
        if ( da != null )
        {
            if ( da.getDataDirection() != DataDirection.Incoming )
            {
                throw new ProcessingException("Incorrect data direction for DataAdapter: expected incoming but found " + da.getDataDirection());
            }

            RelationalContainer data = da.getData();
            RelationalContainer errors = da.executeQuery();
            for ( int i=1; i<=data.rowCount(); i++ )
            {
                logger.debug("Query Results row#" + i + ": " + data.getMappedData().get(i-1));
            }

            if ( errors != null && errors.rowCount() > 0 )
            {
                wrapper.setErrorData(da.getTaskName(), errors);
            }
            logger.info(da.getData().rowCount() + " rows loaded from query: " + da.getExtractableQuery().getTrimmedStatement());
        }
    }

    protected void processIncomingFileAdapter(FileAdapter fa)
        throws ETLException
    {
        if ( fa != null )
        {
            if ( fa.getDataDirection() != DataDirection.Incoming )
            {
                throw new ProcessingException("Incorrect data direction for FileAdapter: expected incoming but found " + fa.getDataDirection());
            }

            RelationalContainer dc = fa.readFile(jobNumber);
            for ( int i=1; i<=dc.rowCount(); i++ )
            {
                logger.debug("Input file row#" + i + ": " + dc.getMappedData().get(i-1));
            }
            logger.info(dc.rowCount() + " rows read from file: " + fa.getTextFile());
        }
    }

    protected void processIncomingDefinedDataAdapter(DefinedDataAdapter da)
        throws ETLException
    {
        if ( da != null )
        {
            if ( da.getDataDirection() != DataDirection.Incoming )
            {
                throw new ProcessingException("Incorrect data direction for DefinedDataAdapter: expected incoming but found " + da.getDataDirection());
            }

            // Call to define the data since join types require runtime data
            da.defineData();
            logger.info(da.getData().rowCount() + " rows loaded by declaration");
        }
    }

    protected void processIncomingExpansionIntervalAdapter(ExpansionInterval da)
        throws ETLException
    {
        if ( da != null )
        {
            if ( da.getDataDirection() != DataDirection.Incoming )
            {
                throw new ProcessingException("Incorrect data direction for DateInterval: expected incoming but found " + da.getDataDirection());
            }

            // Call to define the data since join types require runtime data
            da.expandOnInterval();
            logger.info(da.getData().rowCount() + " rows expanded by " + da.getExpansionType() + " range");
        }
    }

    protected void processIncomingTIAAdapter(TIAAdapter ta)
        throws ETLException
    {
        if ( ta != null )
        {
            if ( ta.getDataDirection() != DataDirection.Incoming )
            {
                throw new ProcessingException("Incorrect data direction for TIAAdapter: expected incoming but found " + ta.getDataDirection());
            }

            RelationalContainer data = ta.getData();
            RelationalContainer errors = ta.initiateRequest();
            for ( int i=1; i<=data.rowCount(); i++ )
            {
                logger.debug("Request Results row#" + i + ": " + data.getMappedData().get(i-1));
            }

            if ( errors != null && errors.rowCount() > 0 )
            {
                wrapper.setErrorData(ta.getTaskName(), errors);
            }
            logger.info(ta.getData().rowCount() + " rows loaded from request: " + ta.getTransactionType());
        }
    }

    protected void processIncomingBinaryAdapter(BinaryAdapter ba)
        throws ETLException
    {
        if ( ba != null )
        {
            if ( ba.getDataDirection() != DataDirection.Incoming )
            {
                throw new ProcessingException("Incorrect data direction for BinaryAdapter: expected incoming but found " + ba.getDataDirection());
            }

            RelationalContainer errors = ba.readBinaryFiles(jobNumber);
            int executed = ba.getData().rowCount();
            int ecount = 0;
            if ( errors != null && errors.rowCount() > 0 )
            {
                ecount = errors.rowCount();
                wrapper.setErrorData(ba.getTaskName(), errors);
            }
            logger.info(executed + " binary files successfully read out of " + (executed+ecount));
        }
    }

    protected void processOutgoingDatabaseAdapter(DatabaseAdapter da)
        throws ETLException
    {
        if ( da != null )
        {
            if ( da.getDataDirection() != DataDirection.Outgoing )
            {
                throw new ProcessingException("Incorrect data direction for DataAdapter: expected outgoing but found " + da.getDataDirection());
            }

            RelationalContainer errors = da.executeUpdate();
            int executed = da.getData().rowCount();
            int ecount = 0;
            if ( errors != null && errors.rowCount() > 0 )
            {
                ecount = errors.rowCount();
                wrapper.setErrorData(da.getTaskName(), errors);
            }
            logger.info(executed + " rows successfully executed out of " + (executed+ecount));
        }
    }

    protected void processOutgoingFileAdapter(FileAdapter fa)
        throws ETLException
    {
        if ( fa != null )
        {
            if ( fa.getDataDirection() != DataDirection.Outgoing )
            {
                throw new ProcessingException("Incorrect data direction for FileAdapter: expected outgoing but found " + fa.getDataDirection());
            }

            RelationalContainer errors = fa.writeFile(jobNumber);
            int executed = fa.getData().rowCount();
            int ecount = 0;
            if ( errors != null && errors.rowCount() > 0 )
            {
                ecount = errors.rowCount();
                wrapper.setErrorData(fa.getTaskName(), errors);
            }
            logger.info(executed + " rows successfully written out of " + (executed+ecount));
            logger.info(executed + " rows written to file: " + fa.getTextFile());
        }
    }

    protected void processOutgoingEmailAdapter(EmailAdapter ea)
        throws ETLException
    {
        if ( ea != null )
        {
            if ( ea.getDataDirection() != DataDirection.Outgoing )
            {
                throw new ProcessingException("Incorrect data direction for EmailAdapter: expected outgoing but found " + ea.getDataDirection());
            }

            RelationalContainer errors = ea.email(jobNumber);
            int executed = ea.getData().rowCount();
            int ecount = 0;
            if ( errors != null && errors.rowCount() > 0 )
            {
                ecount = errors.rowCount();
                wrapper.setErrorData(ea.getTaskName(), errors);
            }
            logger.info(executed + " rows successfully emailed out of " + (executed+ecount));
        }
    }

    protected void processOutgoingTIAAdapter(TIAAdapter ta)
        throws ETLException
    {
        if ( ta != null )
        {
            if ( ta.getDataDirection() != DataDirection.Outgoing )
            {
                throw new ProcessingException("Incorrect data direction for TIAAdapter: expected outgoing but found " + ta.getDataDirection());
            }

            RelationalContainer errors = ta.initiateRequest();
            int executed = ta.getData().rowCount();
            int ecount = 0;
            if ( errors != null && errors.rowCount() > 0 )
            {
                ecount = errors.rowCount();
                wrapper.setErrorData(ta.getTaskName(), errors);
            }
            logger.info(executed + " rows successfully executed out of " + (executed+ecount));
        }
    }

    protected void processOutgoingBinaryAdapter(BinaryAdapter ba)
        throws ETLException
    {
        if ( ba != null )
        {
            if ( ba.getDataDirection() != DataDirection.Outgoing )
            {
                throw new ProcessingException("Incorrect data direction for BinaryAdapter: expected outgoing but found " + ba.getDataDirection());
            }

            RelationalContainer errors = ba.writeBinaryFiles(jobNumber);
            int executed = ba.getData().rowCount();
            int ecount = 0;
            if ( errors != null && errors.rowCount() > 0 )
            {
                ecount = errors.rowCount();
                wrapper.setErrorData(ba.getTaskName(), errors);
            }
            logger.info(executed + " binary files successfully written out of " + (executed+ecount));
        }
    }

    protected void processDataMapping(DataMapping mapping)
        throws ETLException
    {
        if ( mapping != null )
        {
            RelationalContainer errors = mapping.mapData(jobNumber);
            RelationalContainer dest = mapping.getOutgoingData();
            logger.debug(mapping.getSource() + " transformed to " + mapping.getTarget() + ": " + dest);
            int executed = dest.rowCount();
            int ecount = 0;
            if ( errors != null && errors.rowCount() > 0 )
            {
                ecount = errors.rowCount();
                wrapper.setErrorData(mapping.getTaskName(), errors);
            }
            logger.info(executed + " rows successfully transformed out of " + (executed+ecount));
        }
    }

    protected void processDataSorter(DataSorter sorter)
        throws ETLException
    {
        if ( sorter != null )
        {
            sorter.sortData();
            RelationalContainer src = sorter.getIncomingData();
            RelationalContainer dest = sorter.getOutgoingData();
            logger.debug(sorter.getSource() + " sorted on " + sorter.getSortColumns() + ": " + dest);
            int executed = sorter.getSortedDataCount();
            logger.info(executed + " rows successfully sorted out of " + src.rowCount());
        }
    }

    protected void processCustomProcess(CustomProcess process)
        throws ETLException
    {
        if ( process != null )
        {
            RelationalContainer errors = process.invokeCustomMethod();
            int processed = process.getData().rowCount();
            int ecount = 0;
            if ( errors != null && errors.rowCount() > 0 )
            {
                ecount = errors.rowCount();
                wrapper.setErrorData(process.getTaskName(), errors);
                logger.warn("Error row data: " + errors);
            }
            logger.info(processed + " rows successfully processed out of " + (processed+ecount));
        }
    }

    protected List<String> processDataFilter(DataFilter filter)
        throws ETLException
    {
        ArrayList<String> filteredTasks = new ArrayList();
        ArrayList<Integer> keys = new ArrayList();
        if ( filter != null )
        {
            filter.getData().addColumnDefinition(DataFilter.TARGET_COL);
            Map<String,RelationalContainer> filterData = filter.filterData(this);
            
            // Only handle assigning task data if the data filter processes in batch
            if ( !filter.isIterative() )
            {
                logger.debug("FilteredData: " + filterData);
                Iterator<Map.Entry<String,RelationalContainer>> it = filterData.entrySet().iterator();
                while ( it.hasNext() )
                {
                    Map.Entry<String,RelationalContainer> entry = it.next();
                    String task = entry.getKey();
                    RelationalContainer rowdata = entry.getValue();
                    if ( !(filter.getTaskName() + AbstractTask.ERROR_DATA_NAME).equals(task) )
                    {
                        DataSet ds = wrapper.retrieveNamedData(task);
                        
                        // Only run a filtered task if the filtered data is not empty
                        if ( ds != null && rowdata.rowCount() > 0 )
                        {
                            ds.copyDataSet(rowdata, true, true);
                            keys.add(filter.lookupPriority(task));
                            //Integer priority = filter.lookupPriority(task);
                            //filteredTasks.set(priority, task);
                            logger.debug("Filtered data assigned: " + ds);
                        }
                        else if ( ds == null )
                        {
                            throw new ETLException("Cannot assign filtered data to undeclared task: " + task);
                        }
                    }
                    else if ( rowdata.rowCount() > 0 )
                    {
                        logger.warn("Errors returned from data filter: " + rowdata);
                        wrapper.setErrorData(filter.getTaskName(), rowdata);
                    }
                }
            }
        }
        
        // Order the processing by the order declared in the xml document
        Map<Integer,String> priorityMap = filter.getPriorityMap();
        logger.debug("prioritymap=" + priorityMap);
        Collections.sort(keys);
        for ( Integer i: keys )
        {
            filteredTasks.add(priorityMap.get(i));
        }
        
        return filteredTasks;
    }
    
    protected void processSubJob(SubJob job)
        throws ETLException
    {
        if ( job != null )
        {
            job.executeSubJob(jobNumber, wrapper);
        }
    }

    protected void processDataCorrelation(DataCorrelation dp)
        throws ETLException
    {
        if ( dp != null )
        {
            RelationalContainer errors = dp.correlate();
            int processed = dp.getData().rowCount();
            int ecount = 0;
            if ( errors != null && errors.rowCount() > 0 )
            {
                ecount = errors.rowCount();
                wrapper.setErrorData(dp.getTaskName(), errors);
                logger.warn("Error row data: " + errors);
            }
            logger.info(processed + " rows successfully processed out of " + (processed+ecount));
        }
    }

    protected void processChangeFilter(ChangeFilter cf)
        throws ETLException
    {
        if ( cf != null )
        {
            cf.filterChanges(jobNumber);

            /*DataContainer errors = dp.correlate();
            int processed = dp.getData().rowCount();
            int ecount = 0;
            if ( errors != null && errors.rowCount() > 0 )
            {
                ecount = errors.rowCount();
                wrapper.setErrorData(dp.getTaskName(), errors);
                logger.warn("Error row data: " + errors);
            }
            logger.info(processed + " rows successfully processed out of " + (processed+ecount));*/
        }
    }

    protected void processDataAggregation(DataAggregation ad)
        throws ETLException
    {
        if ( ad != null )
        {
            logger.debug("DataAggregation: " + ad);
            // Get source data
            DataSet ds = null;

            logger.info("Loading SourceData: " + ad.getSource());
            ds = wrapper.retrieveNamedData(ad.getSource());
            if ( ds == null )
            {
                throw new ETLException("Undefined data source referenced: " + ad.getSource());
            }

            if ( ds.getType() != DataSet.Type.relational )
            {
                throw new ETLException("Cannot use hierarchical data sets for DataAggregations");
            }
            
            int inc = ds.rowCount();
            RelationalContainer src = (RelationalContainer)ds;
            wrapper.getDataStatistics().assignTaskInputRowCount(ad, src.rowCount());
            logger.info("SourceData loaded: " + src);

            RelationalContainer errors = ad.aggregate(src);
            int processed = ad.getData().rowCount();
            int ecount = 0;
            if ( errors != null && errors.rowCount() > 0 )
            {
                ecount = errors.rowCount();
                wrapper.setErrorData(ad.getTaskName(), errors);
                logger.warn("Error row data: " + errors);
            }
            logger.info(processed + " rows successfully aggregated from " + inc + (ecount>0?"; removed " + ecount + " due to errors":""));
        }
    }

    protected void processErrorTask(ErrorTask et)
        throws ETLException
    {
        if ( et != null )
        {
            RelationalContainer errors = et.throwErrors();
            int ecount = 0;
            if ( errors != null && errors.rowCount() > 0 )
            {
                ecount = errors.rowCount();
                wrapper.setErrorData(et.getTaskName(), errors);
            }
            logger.info(ecount + " rows converted to errors");
        }
    }

    protected void processControlTask(ControlTask ct)
        throws ETLException
    {
        if ( ct != null )
        {
            ct.processControl(this);
            logger.info(ct.getProcessTaskCount() + " subtasks processed.");
        }
    }
    
    protected void processExtractControl(ExtractControl ct)
        throws ETLException
    {
        if ( ct != null )
        {
            ct.controlExtract();
        }
    }

    protected void processRowSplitter(RowSplitter rs)
        throws ETLException
    {
        if ( rs != null )
        {
            RelationalContainer errors = rs.splitRows();
            int processed = rs.getData().rowCount();
            int ecount = 0;
            if ( errors != null && errors.rowCount() > 0 )
            {
                ecount = errors.rowCount();
                wrapper.setErrorData(rs.getTaskName(), errors);
                logger.warn("Error row data: " + errors);
            }
            logger.info(processed + " rows successfully split out of " + (processed+ecount));
        }
    }

    protected void processWebApi(WebAPIDispatcher web)
        throws ETLException
    {
        if ( web != null )
        {
            RelationalContainer data = web.getData();
            RelationalContainer errors = web.execute();
            for ( int i=1; i<=data.rowCount(); i++ )
            {
                logger.debug("Web api results row#" + i + ": " + data.getMappedData().get(i-1));
            }

            if ( errors != null && errors.rowCount() > 0 )
            {
                wrapper.setErrorData(web.getTaskName(), errors);
            }
            logger.info(web.getData().rowCount() + " rows executed from api: " + web.getDriver().getBaseURL());
        }
    }
    
    public abstract void process() throws ETLException;
    public abstract void processIterativeFilteredDataTask(String task) throws ETLException;
    public abstract void processBatchFilteredDataTask(String task) throws ETLException;
    protected abstract AbstractTask processTask(AbstractTask task, boolean iterative) throws ETLException;
    
    public static class JobMetadata implements Comparable<Object>
    {
        private String name = "";
        private File path = null;
        
        public JobMetadata(String n, String f)
        {
            name = n;
            path = new File(f);
        }
        
        public JobMetadata(String n, File f)
        {
            name = n;
            path = f;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public File getPath() {
            return path;
        }

        public void setPath(File path) {
            this.path = path;
        }

        @Override
        public int compareTo(Object arg0) 
        {
            int c = 0;
            if ( arg0 instanceof JobMetadata )
            {
                if ( name != null )
                {
                    c = name.compareTo(((JobMetadata)arg0).getName());
                }
            }
            
            return c;
        }

        @Override
        public boolean equals(Object obj) 
        {
            boolean equal = false;
            if ( obj instanceof JobMetadata )
            {
                equal = name != null && name.equals(((JobMetadata)obj).getName());
            }
            
            return equal;
        }

        @Override
        public int hashCode() 
        {
            return (name != null ? name.hashCode(): 0);
        }
        
        public String toString()
        {
            String s = name;
            try
            {
                s += " (" + path.getCanonicalPath() + ")";
            }
            catch (IOException e) {}
            return s;
        }
    }
    
}
