/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.process;

import com.blackboard.services.etl.JobLoader;
import com.blackboard.services.etl.data.BinaryObject;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.tasks.Adapter;
import com.blackboard.services.etl.tasks.CustomProcess;
import com.blackboard.services.etl.tasks.DataDirection;
import com.blackboard.services.etl.tasks.DataMapping;
import com.blackboard.services.etl.tasks.DatabaseAdapter;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.exception.ProcessingException;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.EtlJob;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.blackboard.services.etl.tasks.AbstractTask;
import com.blackboard.services.etl.tasks.BinaryAdapter;
import com.blackboard.services.etl.tasks.ChangeFilter;
import com.blackboard.services.etl.tasks.ControlTask;
import com.blackboard.services.etl.tasks.DataAggregation;
import com.blackboard.services.etl.tasks.DataFilter;
import com.blackboard.services.etl.tasks.DataCorrelation;
import com.blackboard.services.etl.tasks.DataSorter;
import com.blackboard.services.etl.tasks.ExpansionInterval;
import com.blackboard.services.etl.tasks.DefinedDataAdapter;
import com.blackboard.services.etl.tasks.ETLTask.ClassType;
import com.blackboard.services.etl.tasks.EmailAdapter;
import com.blackboard.services.etl.tasks.ErrorTask;
import com.blackboard.services.etl.tasks.ExtractControl;
import com.blackboard.services.etl.tasks.FileAdapter;
import com.blackboard.services.etl.tasks.NullTask;
import com.blackboard.services.etl.tasks.RowSplitter;
import com.blackboard.services.etl.tasks.SubJob;
import com.blackboard.services.etl.tasks.TIAAdapter;
import com.blackboard.services.etl.tasks.WebAPIDispatcher;
import com.blackboard.services.etl.wrapper.DataArchiver;
import com.blackboard.services.etl.wrapper.DataStatistics;
import com.blackboard.services.installer.InstallationMetadataUtils;
import com.blackboard.services.utils.CommandLineArgumentManager;
import com.blackboard.services.utils.ResourceLoader;
import com.blackboard.services.utils.schema.SchemaDocument;
import com.blackboard.services.utils.schema.SchemaNode;
import com.blackboard.services.utils.exception.CompilationException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import org.apache.log4j.Logger;
import org.w3c.dom.DOMException;
import org.xml.sax.SAXParseException;

/**
 *
 * @author crusnak
 */
public class CommandLineProcessor extends AbstractETLProcessor
{
    public static final String SYS_PROP_LOG_FILE =  "log.job.file";
    public static final String COL_ERROR_TASK =     "ERROR_TASK";

    private File logFile = null;

    public CommandLineProcessor(Map<String,String> params, String logfilename)
        throws ETLException
    {
        String root = params.get(String.valueOf(StartParams.etl_root));
        String job = params.get(String.valueOf(StartParams.job_name));
        appRoot = new File(".");
        if ( root != null )
        {
            appRoot = new File(root);
        }

        File logdir = new File(appRoot, LOG_DIR);
        logFile = new File(logdir, logfilename + LOG_EXT);
        System.setProperty(SYS_PROP_LOG_FILE, logfilename);
        logger = Logger.getLogger(this.getClass());

        try
        {
            URL jobURL = null;
            try
            {
                jobURL = ResourceLoader.findResourceAsURL(null, appRoot, job);
            }
            catch (IOException e)
            {
                jobURL = ResourceLoader.findResourceAsURL(null, appRoot, job + ".xml");
            }

            Map<String,String> args = CommandLineArgumentManager.removeActionArgumentsFromMap(params, StartParams.class);
            jobFile = new File(jobURL.getFile());
            JobLoader loader = new JobLoader(appRoot, jobFile.getName());
            jobNumber = ETLJobWrapper.jobNumberFormat.format(new Date());
            wrapper = loader.getETLJobWrapper(args, jobNumber);
            wrapper.setSuppressArchiving(false);
            
            // Assign reserved variable values for running job
            wrapper.setGlobalVariable(Processor.GVAR_ARCHIVE_DIR, new DataCell(new DataColumn(DataTypes.STRING, Processor.GVAR_ARCHIVE_DIR), wrapper.getDataArchiver().getRunningArchiveDirectory(jobNumber).getCanonicalPath()));                
        }
        catch (ETLException e)
        {
            logger.error("Failed to initialize etl job", e);
            throw new ETLException("Failed to initialize etl job", e);
        }
        catch (IOException e)
        {
            throw new ETLException("Cannot find etl job: " + job, e);
        }

        //VariableWrapper vw = new VariableWrapper(var, wrapper);
        //vw.setValue(appRoot);
        //wrapper.getVariableMap().put(APP_ROOT, vw);
    }
    
    public CommandLineProcessor(ETLJobWrapper job, String jobnum)
        throws ETLException
    {
        logger = Logger.getLogger(this.getClass());
        wrapper = job;
        jobNumber = jobnum;
        jobFile = wrapper.getJobFile();
        appRoot = wrapper.getETLRoot();
    }

    public void destroy()
    {
        logger.info("Cleaning up processor for job " + wrapper.getJobName());
        wrapper.destroy();
        wrapper = null;
        System.gc();
    }

    public static List<JobMetadata> listAvailableJobs(Map<String,String> params)
        throws IOException
    {
        String root = params.get(String.valueOf(StartParams.etl_root));
        File appRoot = new File(".");
        if ( root != null )
        {
            appRoot = new File(root);
        }
        
        System.out.println("\nAPPLICATION ROOT=" + appRoot);
        
        HashSet<JobMetadata> jobs = new HashSet();
        List<File> xmlFiles = ResourceLoader.findResourcesAsFiles(".xml");
        for ( File xml: xmlFiles )
        {
            try
            {
                // Attempt to load the xml as an ETLJob to see if this xml file is a valid job.
                String jobname = xml.getName().substring(0, xml.getName().indexOf("."));
                new JobLoader(appRoot, xml);
                jobs.add(new JobMetadata(jobname, xml));
            }
            catch (ETLException e)
            {
                // Ignore if can't load xml as etl job
                //System.out.println("Can't load " + xml + ": " + e.getMessage());
            }
        }
        
        List<JobMetadata> list = Collections.list(Collections.enumeration(jobs));
        Collections.sort(list);
        return list;
    }
    
    public static void listValidXPaths(Map<String,String> params)
        throws ETLException
    {
        String root = params.get(String.valueOf(XPathParams.etl_root));
        File appRoot = new File(".");
        if ( root != null )
        {
            appRoot = new File(root);
        }
        
        String schemaName = params.get(String.valueOf(XPathParams.xsd));
        SchemaDocument schema = null;
        
        try
        {
            if ( schemaName.endsWith(".xsd") )
            {
                schema = new SchemaDocument(appRoot, schemaName);
            }
            else
            {
                schema = new SchemaDocument(appRoot, schemaName + ".xsd");
            }

            System.out.println("\nAPPLICATION ROOT=" + appRoot);
            System.out.println("Listing all valid xpaths for: " + schema.getSchemaFile());
            System.out.println("-----------------------------------------");

            Set<String> xpaths = schema.getValidXpathExpressions(schema.getRootElement());
            for ( String path: xpaths )
            {
                String line = "\t" + path + ": (";
                SchemaNode node = schema.retrieveXPathElement(path);
                if ( node.getMinOccurs() != node.getMaxOccurs() )
                {
                    line += node.getMinOccurs() + ".." + (node.getMaxOccurs()==-1?"*":node.getMaxOccurs()) + ")";
                }
                else
                {
                    line += node.getMinOccurs() + ")";
                }
                System.out.println(line);
            }
        }
        catch ( CompilationException e)
        {
            throw new ETLException("Failed to process schema document", e);
        }
    }

    public String getJobNumber()
    {
        return jobNumber;
    }
    
    public ETLJobWrapper getJobWrapper()
    {
        return wrapper;
    }
    
    public void process()
        throws ETLException
    {
        startDate = new Date();
        System.out.println("Logfile located at: " + logFile);
        logger.info("JAVA_HOME=" + System.getProperty("java.home"));
        logger.info("------------------------Job #" + jobNumber + " '" + wrapper.getJobName() + "' started----------------------" );
        logger.info("Default TimeZone: " + TimeZone.getDefault());
        logger.debug("Job definition: " + wrapper);

        // Copy any previous run comparison data to the current archive directory
        wrapper.getDataArchiver().getRunningArchiveDirectory(jobNumber).mkdirs();
        wrapper.getDataArchiver().copyPreviousComparisonData(jobNumber);

        // Lock job if not currently running, otherwise throw exception
        if ( wrapper.getDataArchiver().isJobLocked() )
        {
            String lockJobnum = lockJobnum = wrapper.getDataArchiver().getLockedJobnum();
            
            throw new ETLException("Unable to process job '" + wrapper.getJobName() +
                                   "' because the job with the same arguments is currently locked by another process: " + lockJobnum);
        }
        else 
        {
            wrapper.getDataArchiver().lockJob(jobNumber);
        }

        AbstractTask currentTask = wrapper.getStartJobTask();
        if ( currentTask == null )
        {
            throw new ETLException("No start task defined in ETL job definition.");
        }
        
        AbstractTask lastTask = null;
        while ( currentTask != null )
        {
            lastTask = currentTask;
            currentTask = processTask(currentTask, false);
        }

        int processed = lastTask.getData().rowCount();
        int errors = 0;
        for ( RelationalContainer dc: wrapper.getErrorDataMap().values() )
        {
            errors += dc.getMappedData().size();
        }
        //logger.debug("Final data: " + wrapper.getDataContainerMap());
        logger.debug("Error data: " + wrapper.getErrorDataMap());
        logger.info("Completed ETL processing of " + processed + " rows of data out of " + (processed+errors));
        
        DecimalFormat fmt = new DecimalFormat("0.00");
        double elapsed = (double)(new Date().getTime() - startDate.getTime())/1000.0;
        logger.info("Job #" + jobNumber + " '" + wrapper.getJobName() + "' took " +
            elapsed + " seconds to process.");
        System.out.println("\nJob #" + jobNumber + " '" + wrapper.getJobName() + "' took " +
            fmt.format((double)(elapsed/60.0)) + " minutes to process.");

        logger.info("------------------------Job #" + jobNumber + " '" + wrapper.getJobName() + "' completed----------------------" );
        wrapper.getDataArchiver().archiveMasterData(jobNumber);
        wrapper.getDataArchiver().archiveErrorData(jobNumber);
    }

    /*public void process(String jobnum, String restart)
        throws ETLException
    {
        throw new UnsupportedOperationException("Restarting a process is currently not supported");
        logger.info("--------------------------Restarting Job #" + jobnum + " '" + wrapper.getJobName() + "' at task '" + restart + "'-------------------------");
        logger.info("--------------------------New Job #" + jobNumber + " '" + wrapper.getJobName() + "' started----------------------" );
        logger.debug("Job definition: " + wrapper);

        try
        {
            File archDatFile = new File(wrapper.getDataArchiver().getArchiveDirectory(), jobnum + "/" + DATA_DIR + "/" + restart + DAT_EXT);
            logger.info("Loading archived data from: " + archDatFile.getCanonicalPath());
            wrapper.getDataArchiver().readSnapshotDataFromFile(archDatFile);
        }
        catch (GeneralSecurityException e)
        {
            throw new ETLException("Error intializing cipher for decrypting archive data for job #" + jobnum + " and task " + restart, e);
        }
        catch (BbTSSecurityException e)
        {
            throw new ETLException("Error decrypting snapshot data from archive for job #" + jobnum + " and task " + restart, e);
        }
        catch (IOException e)
        {
            throw new ETLException("Unable to load archive data for job #" + jobnum + " and task " + restart, e);
        }

        // Restarting a task loads data that was output after that task completed and needs to be processed
        // by the next task.
        AbstractTask prevTask = wrapper.getJobTaskByName(restart);
        AbstractTask currentTask = wrapper.getJobTaskByName(prevTask.getNextTaskName());
        AbstractTask lastTask = null;
        while ( currentTask != null )
        {
            lastTask = currentTask;
            currentTask = processTask(currentTask);
        }

        int processed = lastTask.getData().rowCount();
        int errors = 0;
        for ( DataContainer dc: wrapper.getErrorDataMap().values() )
        {
            errors += dc.getMappedData().size();
        }
        //logger.debug("Final data: " + wrapper.getDataContainerMap());
        logger.debug("Error data: " + wrapper.getErrorDataMap());
        logger.info("Completed ETL processing of " + processed + " rows of data out of " + (processed+errors));
        logger.info("------------------------New Job #" + jobNumber + " '" + wrapper.getJobName() + "' completed----------------------" );

        archiveErrorData();
        wrapper.getDataArchiver().removeOldArchives();
         
    }*/

    @Override
    public void processIterativeFilteredDataTask(String task)
        throws ETLException
    {
        logger.info("------------------------Filtered (Iterative) Data task '" + task + "' started----------------------" );
        System.out.println();
        System.out.println("Filtered (Iterative) Data task '" + task + "' started" );
        System.out.println();

        AbstractTask currentTask = wrapper.getJobTaskByName(task);
        AbstractTask lastTask = null;
        HashSet<AbstractTask> tasks = new HashSet();
        while ( currentTask != null )
        {
            tasks.add(currentTask);
            lastTask = currentTask;
            currentTask = processTask(currentTask, true);
        }

        // Need to archive all tasks in this filtered list once complete since they aren't archived
        // after task completion in iterative format.
        for ( AbstractTask at: tasks )
        {
            // Must clear correlation data as it will get used as an input for subsequent iterative calls
            if ( at.getClassType() == ClassType.DataCorrelation )
            {
                at.getData().clearData();;
            }
            
            at.archiveTransientData(jobNumber);
            wrapper.archiveTransientMasterData(jobNumber);
            
        }
        
        System.gc();
        
        int processed = lastTask.getData().rowCount();
        int errors = 0;
        for ( RelationalContainer dc: wrapper.getErrorDataMap().values() )
        {
            errors += dc.getMappedData().size();
        }
        //logger.debug("Final data: " + wrapper.getDataContainerMap());
        logger.debug("Error data: " + wrapper.getErrorDataMap());
        logger.info("Completed ETL processing of " + processed + " rows of data out of " + (processed+errors));
        logger.info("------------------------Filtered (Iterative) Data task '" + task + "' completed----------------------" );
        System.out.println();
        System.out.println("Filtered (Iterative) Data task '" + task + "' completed" );
        System.out.println();
    }
    
    @Override
    public void processBatchFilteredDataTask(String task)
        throws ETLException
    {
        logger.info("------------------------Filtered (Batch) Data task '" + task + "' started----------------------" );
        System.out.println();
        System.out.println("Filtered (Batch) Data task '" + task + "' started" );
        System.out.println();

        AbstractTask currentTask = wrapper.getJobTaskByName(task);
        AbstractTask lastTask = null;
        while ( currentTask != null )
        {
            lastTask = currentTask;
            currentTask = processTask(currentTask, false);
        }

        int processed = lastTask.getData().rowCount();
        int errors = 0;
        for ( RelationalContainer dc: wrapper.getErrorDataMap().values() )
        {
            errors += dc.getMappedData().size();
        }
        //logger.debug("Final data: " + wrapper.getDataContainerMap());
        logger.debug("Error data: " + wrapper.getErrorDataMap());
        logger.info("Completed ETL processing of " + processed + " rows of data out of " + (processed+errors));
        logger.info("------------------------Filtered (Batch) Data task '" + task + "' completed----------------------" );
        System.out.println();
        System.out.println("Filtered (Batch) Data task '" + task + "' completed" );
        System.out.println();
    }

    @Override
    protected AbstractTask processTask(AbstractTask task, boolean iterative)
        throws ETLException
    {
        AbstractTask next = null;
        DataStatistics stats = wrapper.getDataStatistics();
        logger.info("------------------------CurrentTask: " + ( task != null ? task.getTaskName() : "no task") + "----------------------" );
        System.out.println();
        System.out.println("CurrentTask: " + ( task != null ? task.getTaskName() : "no task"));
        System.out.println();

        if ( task != null )
        {
            // If the current task's data is currently archived, load from file and store in memory
            if ( task.isCurrentlyArchived() )
            {
                task.loadDataAndUnlink();
            }

            Date start = new Date();
            taskPath.add(task.getTaskName());

            // Assign source data from another task
            if ( task.getSourceDataTaskName() != null && task.getSourceDataTaskName().trim().length() > 0 )
            {
                logger.info("Assigning task data from task: " + task.getSourceDataTaskName());
                DataSet dc = wrapper.retrieveNamedData(task.getSourceDataTaskName());
                if ( dc == null )
                {
                    logger.warn("Source data is undefined: " + task.getSourceDataTaskName());
                }
                else
                {
                    task.getData().copyDataSet(dc, false, true);
                    task.setData(task.getData());
                    //task.getData().setDataContainer(dc, false, true);
                    logger.debug("TaskData assigned: " + task.getData());
                }
            }

            DataSet incoming = task.getData();
            int incomingRows = incoming.rowCount();
            int outgoingRows = 0;
            stats.assignTaskInputRowCount(task, incomingRows);
            logger.info("Incoming data contains " + incomingRows + " rows of data.");
            logger.debug("Incoming: " + incoming);

            switch (task.getClassType())
            {
                case Adapter:
                    processAdapter((Adapter)task);
                    outgoingRows = task.getData().rowCount();
                    break;
                case DataMapping:
                    processDataMapping((DataMapping)task);
                    stats.assignTaskInputRowCount(task, ((DataMapping)task).getIncomingData().rowCount());
                    outgoingRows = ((DataMapping)task).getOutgoingDataCount();
                    ((DataMapping)task).setIncomingData(null);
                    ((DataMapping)task).setOutgoingData(null);
                    break;
                case CustomProcess:
                    processCustomProcess((CustomProcess)task);
                    outgoingRows = task.getData().rowCount();
                    break;
                case DataFilter:
                    List<String> tasks = processDataFilter((DataFilter)task);
                    
                    // If the data filter is batch, process all filtered tasks
                    // No need to process filter if iterative since the filter handled
                    // processing individually.
                    if ( !((DataFilter)task).isIterative() )
                    {
                        for ( String t: tasks )
                        {
                            processBatchFilteredDataTask(t);
                        }
                    }
                    outgoingRows = task.getData().rowCount();
                    break;
                case SubJob:
                    processSubJob((SubJob)task);
                    outgoingRows = task.getData().rowCount();
                    break;
                case DataCorrelation:
                    processDataCorrelation((DataCorrelation)task);
                    if ( ((DataCorrelation)task).getIteratorData() != null )
                    {
                        stats.assignTaskInputRowCount(task, ((DataCorrelation)task).getIteratorData().rowCount());
                    }
                    outgoingRows = task.getData().rowCount();
                    ((DataCorrelation)task).setIteratorData(null);
                    ((DataCorrelation)task).setQueryData(null);
                    break;
                case DataAggregation:
                    processDataAggregation((DataAggregation)task);
                    outgoingRows = task.getData().rowCount();
                    break;
                case ChangeFilter:
                    processChangeFilter((ChangeFilter)task);
                    outgoingRows = task.getData().rowCount();
                    break;
                case ErrorTask:
                    processErrorTask((ErrorTask)task);
                    outgoingRows = 0;
                    break;
                case DataSorter:
                    processDataSorter((DataSorter)task);
                    stats.assignTaskInputRowCount(task, ((DataSorter)task).getIncomingData().rowCount());
                    outgoingRows = ((DataSorter)task).getSortedDataCount();
                    ((DataSorter)task).setIncomingData(null);
                    ((DataSorter)task).setOutgoingData(null);
                    break;
                case Control:
                    processControlTask((ControlTask)task);
                    break;
                case ExtractControl:
                    processExtractControl((ExtractControl)task);
                    break;
                case RowSplitter:
                    processRowSplitter((RowSplitter)task);
                    outgoingRows = task.getData().rowCount();
                    break;
                case WebAPIDispatcher:
                    processWebApi((WebAPIDispatcher)task);
                    outgoingRows = task.getData().rowCount();
                    break;
            }

            /*Map<String,DataContainer> dmap = wrapper.getDataContainerMap();
            for ( DataContainer dc: dmap.values() )
            {
                logger.debug("DataContainer: " + dc);
            }*/

            // Archive task data if desired prior to testing for task issues to ensure that data
            // is always archived if run successfully
            wrapper.getDataArchiver().archiveTaskData(jobNumber, task);

            // Only look for next tasks if the task type is not end
            if ( task.getTaskType() != AbstractTask.TaskType.end )
            {
                next = wrapper.getJobTaskByName(task.getNextTaskName());
                if ( task != null && next == null && task.getNextTaskName() != null &&
                     task.getNextTaskName().trim().length() > 0 )
                {
                    throw new ETLException("Missing task for next-task: " + task.getNextTaskName());
                }
                else if ( task != null && next == null && task.getTaskType() != AbstractTask.TaskType.end &&
                          task.getClassType() != AbstractTask.ClassType.DataFilter )
                {
                    throw new ETLException("No next-task defined for non-ending task: " + task.getTaskName());
                }
            }

            stats.assignTaskOutputRowCount(task, outgoingRows);
            if ( incomingRows == 0 && outgoingRows > 0 )
            {
                logger.info("Successfully read " + outgoingRows + " rows of data.");
            }
            else if ( incomingRows > 0 )
            {
                logger.info("Successfully processed " + outgoingRows + " rows out of " + incomingRows + " rows of data.");
            }

            int errorRows = 0;
            RelationalContainer errors = wrapper.retrieveErrorData(task.getTaskName());
            if ( errors != null )
            {
                errorRows = errors.rowCount();
            }
            stats.assignTaskErrorRowCount(task, errorRows);

            Date end = new Date();
            logger.info("Task '" + task.getTaskName() + "' took " + 
                        (double)(end.getTime() - start.getTime())/1000.0 +
                        " seconds to process.");


            stats.assignTaskExecutionTime(task, end.getTime() - start.getTime());

            // This section off-loads the task data used to file so it isn't resident
            // in memory anymore.  Necessary when approaching processing 100k rows.
            if ( !iterative )
            {
                task.archiveTransientData(jobNumber);
                wrapper.archiveTransientMasterData(jobNumber);
                System.gc();
            }
            
            // Handle errors if handler declared and handlers are not disabled
            if ( !wrapper.areHandlersDisabled() && errors != null && task.getErrorHandlerTaskName() != null )
            {
                String handler = task.getErrorHandlerTaskName();
                logger.info("Error handler found for task " + task.getTaskName() + ": " + handler);
                
                AbstractTask htask = wrapper.getJobTaskByName(handler);
                if ( htask == null )
                {
                    logger.warn("Undefined handler declared for task " + task.getTaskName() + ", unable to handle errors.");
                }
                else
                {
                    // Remove the error data since it's being handled
                    wrapper.removeErrorData(task.getTaskName());

                    task.setErrorsHandled(true);
                    DataSet dc = htask.getData();
                    if ( dc != null )
                    {
                        if ( dc.getType() != DataSet.Type.relational )
                        {
                            throw new UnsupportedOperationException("Task handlers do not support hierarchical data sets yet");
                        }
                        
                        dc.copyDataSet(errors, true, true);
                        DataColumn tcol = new DataColumn(DataTypes.STRING, COL_ERROR_TASK);
                        ((RelationalContainer)dc).addColumnDefinition(tcol);
                        
                        // Add the error task name to every row in the target handler so the handler data
                        // knows which task it failed from.
                        for ( Map<String,DataCell> rowdata: ((RelationalContainer)dc).getMappedData() )
                        {
                            rowdata.put(COL_ERROR_TASK, new DataCell(tcol, task.getTaskName()));
                        }
                        
                        AbstractTask currentTask = htask;
                        while ( currentTask != null )
                        {
                            currentTask = processTask(currentTask, false);
                        }
                    }
                }
            }
        }

        return next;
    }
    
    public static void main(String[] args)
    {
        CommandLineProcessor processor = null;
        Action action = null;
        try
        {
            HashMap<Action,Class<? extends Enum>> actionMap = new HashMap();
            actionMap.put(Action.START, StartParams.class);
            actionMap.put(Action.RESTART, RestartParams.class);
            actionMap.put(Action.USAGE, UsageParams.class);
            actionMap.put(Action.LIST, ListParams.class);
            actionMap.put(action.XPATH, XPathParams.class);
            
            action = Action.valueOf(args[0]);
            Map<String,String> paramMap = CommandLineArgumentManager.extractCommandLineArguments(CommandLineProcessor.class, Action.class, actionMap, args);
            String jobname = paramMap.get(String.valueOf(StartParams.job_name));
            Map<String,String> argmap = CommandLineArgumentManager.removeActionArgumentsFromMap(paramMap, Action.class);
            Collection argvals = argmap.values();

            switch (action)
            {
                case START:
                    String argstring = DataArchiver.escapeListForFilename(argvals);

                    processor = new CommandLineProcessor(paramMap, jobname + "_" + argstring);
                    processor.process();
                    break;
                case USAGE:
                    argvals = argmap.values();
                    argstring = DataArchiver.escapeListForFilename(argvals);
                    if ( jobname.endsWith(".xml") )
                    {
                        jobname = jobname.substring(0, jobname.indexOf(".")) + "_" + argstring;
                    }

                    processor = new CommandLineProcessor(paramMap, jobname + "_" + argstring);
                    Map<String,EtlJob.GlobalVariables.Variable> usages = processor.wrapper.getArgumentUsage();
                    System.out.println("Argument usage for job: " + jobname);
                    System.out.println("------------------------------------------------");
                    if ( usages.isEmpty() )
                    {
                        System.out.println("\tJob does not contain any job arguments.");
                    }
                    else
                    {
                        for ( Map.Entry<String,EtlJob.GlobalVariables.Variable> entry: usages.entrySet() )
                        {
                            String arg = entry.getKey();
                            EtlJob.GlobalVariables.Variable var = entry.getValue();
                            StringBuilder out = new StringBuilder();
                            String usage = "";
                            if ( var.getUsage() != null )
                            {
                                usage = var.getUsage().trim();
                                usage.replace('\r', '\n');
                            }
                            
                            out.append(arg + ": {\n");
                            out.append(InstallationMetadataUtils.enforceColSize("DataType=" + var.getDataType(), 3, 80) + "\n");
                            out.append((var.getValue() != null)?InstallationMetadataUtils.enforceColSize("Default=" + var.getValue(), 3, 80) + "\n" :"");
                            out.append((var.getUsage() != null)?InstallationMetadataUtils.enforceColSize("Usage=" + usage, 3, 80):"");
                            out.append("\n\t}");
                            System.out.println("\t" + out);
                        }
                    }
                    break;
                case LIST:
                    System.out.println("Listing all available jobs on the classpath:");
                    System.out.println("-----------------------------------------");
        
                    List<JobMetadata> jobs = listAvailableJobs(argmap);
                    for ( JobMetadata job: jobs )
                    {
                        System.out.println("\t" + job);
                    }                    
                    break;
                case XPATH:
                    listValidXPaths(argmap);
                    break;
                case RESTART:
                    throw new ETLException("RESTART not currently supported");
            }
        }
        catch (Exception e)
        {
            String linenum = extractLineNumberFromXMLException(e);
            e.printStackTrace();
            System.err.println(linenum);
            
            if ( processor != null )
            {
                if ( processor.logger != null )
                {
                    processor.logger.error("FATAL ERROR: " + linenum, e);
                }

                // Attempt to archive the log file and email the fatal error to the error recipients
                if ( processor.wrapper != null )
                {
                    try
                    {
                        processor.wrapper.getDataArchiver().emailFatalError(processor.jobNumber, processor.logFile, e);
                    }
                    catch (Exception ex)
                    {
                        processor.logger.error("Unable to email error information", ex);
                    }
                }
            }
        }
        finally
        {
            // Unlock job
            if ( processor != null )
            {
                processor.wrapper.getDataArchiver().removeOldArchives();
                File log = processor.wrapper.getDataArchiver().archiveLogFile(processor.jobNumber, processor.logFile);
                processor.wrapper.getDataArchiver().unlockJob(processor.jobNumber);
                
                try
                {
                    ETLJobWrapper w = processor.wrapper;
                    NullTask task = new NullTask(w.getJobName(), w);
                    if ( action == Action.START )
                    {
                        w.getDataStatistics().assignTaskExecutionTime(task, new Date().getTime() - processor.startDate.getTime());
                        File stats = w.getDataArchiver().outputDataStatistics(processor.jobNumber);
                        processor.wrapper.getDataArchiver().emailCompletedJobInfo(processor.jobNumber, log, stats);
                    }
                    processor.wrapper.destroy();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
}
