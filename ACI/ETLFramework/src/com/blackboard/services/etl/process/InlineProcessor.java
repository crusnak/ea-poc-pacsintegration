/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.process;

import com.blackboard.services.etl.JobLoader;
import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.DataSet;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.jaxb.DataTypes;
import static com.blackboard.services.etl.process.CommandLineProcessor.COL_ERROR_TASK;
import com.blackboard.services.etl.tasks.AbstractTask;
import com.blackboard.services.etl.tasks.Adapter;
import com.blackboard.services.etl.tasks.ChangeFilter;
import com.blackboard.services.etl.tasks.ControlTask;
import com.blackboard.services.etl.tasks.CustomProcess;
import com.blackboard.services.etl.tasks.DataAggregation;
import com.blackboard.services.etl.tasks.DataCorrelation;
import com.blackboard.services.etl.tasks.DataFilter;
import com.blackboard.services.etl.tasks.DataMapping;
import com.blackboard.services.etl.tasks.DataSorter;
import com.blackboard.services.etl.tasks.ETLTask;
import com.blackboard.services.etl.tasks.ErrorTask;
import com.blackboard.services.etl.tasks.ExtractControl;
import com.blackboard.services.etl.tasks.RowSplitter;
import com.blackboard.services.etl.tasks.SubJob;
import com.blackboard.services.etl.tasks.WebAPIDispatcher;
import com.blackboard.services.etl.wrapper.DataStatistics;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import org.apache.log4j.Logger;

/**
 *
 * @author crusnak
 */
public class InlineProcessor extends AbstractETLProcessor
{
    public static final String SYS_PROP_LOG_FILE =  "log.file";

    private File logFile = null;
    
    public InlineProcessor(String root, String jobname, Map<String,String> params, String logfilename, String jobnum)
        throws ETLException
    {
        appRoot = new File(".");
        if ( root != null )
        {
            appRoot = new File(root);
        }
        
        System.setProperty(SYS_PROP_LOG_FILE, logfilename);
        logger = Logger.getLogger(this.getClass());
        logger.info("##############################################################################################");
        logger.info("Executing etl job " + jobname + " for job number: " + jobnum);
        
        try
        {
            jobFile = new File(findJobDefinition(appRoot, jobname).getFile());
            JobLoader loader = new JobLoader(appRoot, jobFile.getName());
            jobNumber = jobnum;
            wrapper = loader.getETLJobWrapper(params, jobNumber);
            wrapper.setSuppressArchiving(true);
            
            // Assign reserved variable values for running job
            wrapper.setGlobalVariable(Processor.GVAR_ARCHIVE_DIR, new DataCell(new DataColumn(DataTypes.STRING, Processor.GVAR_ARCHIVE_DIR), wrapper.getDataArchiver().getRunningArchiveDirectory(jobNumber).getCanonicalPath()));                
        }
        catch (ETLException e)
        {
            logger.error("Failed to initialize etl job", e);
            throw new ETLException("Failed to initialize etl job", e);
        }
        catch (IOException e)
        {
            throw new ETLException("I/O error", e);
        }
    }

    public InlineProcessor(ETLJobWrapper job, Map<String,String> args)
        throws ETLException
    {
        logger = Logger.getLogger(this.getClass());
        wrapper = job;
        jobNumber = ETLJobWrapper.jobNumberFormat.format(new Date());
        jobFile = wrapper.getJobFile();
        appRoot = wrapper.getETLRoot();
        wrapper.assignJobArguments(args);                
        wrapper.setSuppressArchiving(true);
    }
    
    @Override
    public void process()
        throws ETLException
    {
        startDate = new Date();
        System.out.println("Logfile located at: " + logFile);
        logger.info("JAVA_HOME=" + System.getProperty("java.home"));
        logger.info("------------------------Job #" + jobNumber + " '" + wrapper.getJobName() + "' started----------------------" );
        logger.info("Default TimeZone: " + TimeZone.getDefault());
        logger.debug("Job definition: " + wrapper);

        // InlineProcessors don't support job locking
        
        AbstractTask currentTask = wrapper.getStartJobTask();
        if ( currentTask == null )
        {
            throw new ETLException("No start task defined in ETL job definition.");
        }
        
        AbstractTask lastTask = null;
        while ( currentTask != null )
        {
            lastTask = currentTask;
            currentTask = processTask(currentTask, false);
        }

        int processed = lastTask.getData().rowCount();
        int errors = 0;
        for ( RelationalContainer dc: wrapper.getErrorDataMap().values() )
        {
            errors += dc.getMappedData().size();
        }
        //logger.debug("Final data: " + wrapper.getDataContainerMap());
        logger.debug("Error data: " + wrapper.getErrorDataMap());
        logger.info("Completed ETL processing of " + processed + " rows of data out of " + (processed+errors));
        
        DecimalFormat fmt = new DecimalFormat("0.00");
        double elapsed = (double)(new Date().getTime() - startDate.getTime())/1000.0;
        logger.info("Job #" + jobNumber + " '" + wrapper.getJobName() + "' took " +
            elapsed + " seconds to process.");
        System.out.println("\nJob #" + jobNumber + " '" + wrapper.getJobName() + "' took " +
            fmt.format((double)(elapsed/60.0)) + " minutes to process.");

        logger.info("------------------------Job #" + jobNumber + " '" + wrapper.getJobName() + "' completed----------------------" );
        destroy();
    }

    @Override
    protected AbstractTask processTask(AbstractTask task, boolean iterative) 
        throws ETLException 
    {
        AbstractTask next = null;
        logger.info("------------------------CurrentTask: " + ( task != null ? task.getTaskName() : "no task") + "----------------------" );
        System.out.println();
        System.out.println("CurrentTask: " + ( task != null ? task.getTaskName() : "no task"));
        System.out.println();

        if ( task != null )
        {
            Date start = new Date();
            taskPath.add(task.getTaskName());

            // Assign source data from another task
            if ( task.getSourceDataTaskName() != null && task.getSourceDataTaskName().trim().length() > 0 )
            {
                logger.info("Assigning task data from task: " + task.getSourceDataTaskName());
                DataSet dc = wrapper.retrieveNamedData(task.getSourceDataTaskName());
                if ( dc == null )
                {
                    logger.warn("Source data is undefined: " + task.getSourceDataTaskName());
                }
                else
                {
                    task.getData().copyDataSet(dc, false, true);
                    task.setData(task.getData());
                    //task.getData().setDataContainer(dc, false, true);
                    logger.debug("TaskData assigned: " + task.getData());
                }
            }

            DataSet incoming = task.getData();
            int incomingRows = incoming.rowCount();
            int outgoingRows = 0;
            logger.info("Incoming data contains " + incomingRows + " rows of data.");
            logger.debug("Incoming: " + incoming);

            switch (task.getClassType())
            {
                case Adapter:
                    processAdapter((Adapter)task);
                    outgoingRows = task.getData().rowCount();
                    break;
                case DataMapping:
                    processDataMapping((DataMapping)task);
                    outgoingRows = ((DataMapping)task).getOutgoingDataCount();
                    ((DataMapping)task).setIncomingData(null);
                    ((DataMapping)task).setOutgoingData(null);
                    break;
                case CustomProcess:
                    processCustomProcess((CustomProcess)task);
                    outgoingRows = task.getData().rowCount();
                    break;
                case DataFilter:
                    List<String> tasks = processDataFilter((DataFilter)task);
                    
                    // If the data filter is batch, process all filtered tasks
                    // No need to process filter if iterative since the filter handled
                    // processing individually.
                    if ( !((DataFilter)task).isIterative() )
                    {
                        for ( String t: tasks )
                        {
                            processBatchFilteredDataTask(t);
                        }
                    }
                    outgoingRows = task.getData().rowCount();
                    break;
                case SubJob:
                    processSubJob((SubJob)task);
                    outgoingRows = task.getData().rowCount();
                    break;
                case DataCorrelation:
                    processDataCorrelation((DataCorrelation)task);
                    outgoingRows = task.getData().rowCount();
                    ((DataCorrelation)task).setIteratorData(null);
                    ((DataCorrelation)task).setQueryData(null);
                    break;
                case DataAggregation:
                    processDataAggregation((DataAggregation)task);
                    outgoingRows = task.getData().rowCount();
                    break;
                case ChangeFilter:
                    processChangeFilter((ChangeFilter)task);
                    outgoingRows = task.getData().rowCount();
                    break;
                case ErrorTask:
                    processErrorTask((ErrorTask)task);
                    outgoingRows = 0;
                    break;
                case DataSorter:
                    processDataSorter((DataSorter)task);
                    outgoingRows = ((DataSorter)task).getSortedDataCount();
                    ((DataSorter)task).setIncomingData(null);
                    ((DataSorter)task).setOutgoingData(null);
                    break;
                case Control:
                    processControlTask((ControlTask)task);
                    break;
                case ExtractControl:
                    processExtractControl((ExtractControl)task);
                    break;
                case RowSplitter:
                    processRowSplitter((RowSplitter)task);
                    outgoingRows = task.getData().rowCount();
                    break;
                case WebAPIDispatcher:
                    processWebApi((WebAPIDispatcher)task);
                    outgoingRows = task.getData().rowCount();
                    break;
            }

            // Only look for next tasks if the task type is not end
            if ( task.getTaskType() != AbstractTask.TaskType.end )
            {
                next = wrapper.getJobTaskByName(task.getNextTaskName());
                if ( task != null && next == null && task.getNextTaskName() != null &&
                     task.getNextTaskName().trim().length() > 0 )
                {
                    throw new ETLException("Missing task for next-task: " + task.getNextTaskName());
                }
                else if ( task != null && next == null && task.getTaskType() != AbstractTask.TaskType.end &&
                          task.getClassType() != AbstractTask.ClassType.DataFilter )
                {
                    throw new ETLException("No next-task defined for non-ending task: " + task.getTaskName());
                }
            }

            if ( incomingRows == 0 && outgoingRows > 0 )
            {
                logger.info("Successfully read " + outgoingRows + " rows of data.");
            }
            else if ( incomingRows > 0 )
            {
                logger.info("Successfully processed " + outgoingRows + " rows out of " + incomingRows + " rows of data.");
            }

            int errorRows = 0;
            RelationalContainer errors = wrapper.retrieveErrorData(task.getTaskName());
            if ( errors != null )
            {
                errorRows = errors.rowCount();
            }

            Date end = new Date();
            logger.info("Task '" + task.getTaskName() + "' took " + 
                        (double)(end.getTime() - start.getTime())/1000.0 +
                        " seconds to process.");

            // Handle errors if handler declared and handlers are not disabled
            if ( !wrapper.areHandlersDisabled() && errors != null && task.getErrorHandlerTaskName() != null )
            {
                String handler = task.getErrorHandlerTaskName();
                logger.info("Error handler found for task " + task.getTaskName() + ": " + handler);
                
                AbstractTask htask = wrapper.getJobTaskByName(handler);
                if ( htask == null )
                {
                    logger.warn("Undefined handler declared for task " + task.getTaskName() + ", unable to handle errors.");
                }
                else
                {
                    // Remove the error data since it's being handled
                    wrapper.removeErrorData(task.getTaskName());

                    task.setErrorsHandled(true);
                    DataSet dc = htask.getData();
                    if ( dc != null )
                    {
                        if ( dc.getType() != DataSet.Type.relational )
                        {
                            throw new UnsupportedOperationException("Task handlers do not support hierarchical data sets yet");
                        }
                        
                        dc.copyDataSet(errors, true, true);
                        DataColumn tcol = new DataColumn(DataTypes.STRING, COL_ERROR_TASK);
                        ((RelationalContainer)dc).addColumnDefinition(tcol);
                        
                        // Add the error task name to every row in the target handler so the handler data
                        // knows which task it failed from.
                        for ( Map<String,DataCell> rowdata: ((RelationalContainer)dc).getMappedData() )
                        {
                            rowdata.put(COL_ERROR_TASK, new DataCell(tcol, task.getTaskName()));
                        }
                        
                        AbstractTask currentTask = htask;
                        while ( currentTask != null )
                        {
                            currentTask = processTask(currentTask, false);
                        }
                    }
                }
            }
        }

        return next;
    }
    
    @Override
    public void processIterativeFilteredDataTask(String task)
        throws ETLException
    {
        throw new ETLException("Processing iterative ETL tasks in an InlineProcessor is not currently supported");
        
        /*logger.info("------------------------Filtered (Iterative) Data task '" + task + "' started----------------------" );
        System.out.println();
        System.out.println("Filtered (Iterative) Data task '" + task + "' started" );
        System.out.println();

        AbstractTask currentTask = wrapper.getJobTaskByName(task);
        AbstractTask lastTask = null;
        HashSet<AbstractTask> tasks = new HashSet();
        while ( currentTask != null )
        {
            tasks.add(currentTask);
            lastTask = currentTask;
            currentTask = processTask(currentTask, true);
        }

        // Need to archive all tasks in this filtered list once complete since they aren't archived
        // after task completion in iterative format.
        for ( AbstractTask at: tasks )
        {
            // Must clear correlation data as it will get used as an input for subsequent iterative calls
            if ( at.getClassType() == ETLTask.ClassType.DataCorrelation )
            {
                at.getData().clearData();;
            }
            
            at.archiveTransientData(jobNumber);
            wrapper.archiveTransientMasterData(jobNumber);
            
        }
        
        System.gc();
        
        int processed = lastTask.getData().rowCount();
        int errors = 0;
        for ( RelationalContainer dc: wrapper.getErrorDataMap().values() )
        {
            errors += dc.getMappedData().size();
        }
        //logger.debug("Final data: " + wrapper.getDataContainerMap());
        logger.debug("Error data: " + wrapper.getErrorDataMap());
        logger.info("Completed ETL processing of " + processed + " rows of data out of " + (processed+errors));
        logger.info("------------------------Filtered (Iterative) Data task '" + task + "' completed----------------------" );
        System.out.println();
        System.out.println("Filtered (Iterative) Data task '" + task + "' completed" );
        System.out.println();*/
    }
    
    @Override
    public void processBatchFilteredDataTask(String task)
        throws ETLException
    {
        logger.info("------------------------Filtered (Batch) Data task '" + task + "' started----------------------" );
        System.out.println();
        System.out.println("Filtered (Batch) Data task '" + task + "' started" );
        System.out.println();

        AbstractTask currentTask = wrapper.getJobTaskByName(task);
        AbstractTask lastTask = null;
        while ( currentTask != null )
        {
            lastTask = currentTask;
            currentTask = processTask(currentTask, false);
        }

        int processed = lastTask.getData().rowCount();
        int errors = 0;
        for ( RelationalContainer dc: wrapper.getErrorDataMap().values() )
        {
            errors += dc.getMappedData().size();
        }
        //logger.debug("Final data: " + wrapper.getDataContainerMap());
        logger.debug("Error data: " + wrapper.getErrorDataMap());
        logger.info("Completed ETL processing of " + processed + " rows of data out of " + (processed+errors));
        logger.info("------------------------Filtered (Batch) Data task '" + task + "' completed----------------------" );
        System.out.println();
        System.out.println("Filtered (Batch) Data task '" + task + "' completed" );
        System.out.println();
    }

    @Override
    protected void processChangeFilter(ChangeFilter cf)
        throws ETLException
    {
        throw new ETLException("ChangeFilter tasks are not supported in InlineProcessors as it requires archiving");
    }
}
