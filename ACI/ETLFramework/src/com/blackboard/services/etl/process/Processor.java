/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl.process;

/**
 *
 * @author crusnak
 */
public interface Processor
{
    public static enum Action { START, RESTART, USAGE, LIST, XPATH };
    public static enum StartParams { etl_root, job_name  };
    public static enum RestartParams { etl_root, job_name, job_number, restart_task  };
    public static enum UsageParams { etl_root, job_name };
    public static enum ListParams { etl_root };
    public static enum XPathParams { etl_root, xsd }
    public static enum ManageParams { etl_root, job_name, job_path }

    public static final String DATA_DIR =          "data";
    public static final String ERROR_DIR =         "errors";
    public static final String INPUT_DIR =         "inputs";
    public static final String LOG_DIR =           "logs";
    public static final String CDC_DIR =           "cdc";
    public static final String DAT_EXT =           ".dat";
    public static final String TXT_EXT =           ".txt";
    public static final String LOG_EXT =           ".log";
    
    public static final String GVAR_JOB_NAME =     "JobName";
    public static final String GVAR_JOB_NUM =      "JobNumber";
    public static final String GVAR_ARCHIVE_DIR =  "ArchiveDirectory";
    public static final String GVAR_EXCEPTION =    "Exception";
    public static final String GVAR_JOB_ARGS =     "JobArguments";
    public static final String GVAR_ERROR_TASKS =  "TaskErrors";
}
