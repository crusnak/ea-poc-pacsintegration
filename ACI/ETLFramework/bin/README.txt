If an input or output is using a mapped network drive, in order for a scheduled task to have access to that it needs to be available upon login to that user.
When using local service, no external resources are available upon login so it needs to be added to a bat file prior to kicking off the etl job. See below:

net use b: \\server\path to your bat file 