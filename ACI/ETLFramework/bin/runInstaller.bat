echo off
SET ETL_ROOT=..

SET CLASSPATH=%ETL_ROOT%
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/classes/
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/BbInteractiveInstaller.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/TIAInterface.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/ojdbc8.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/mysql-5.1.13.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/sqljdbc42.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/jtds-1.3.1.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/postgresql-9.1.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/ifxjdbc.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/unijdbc.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/asjava.zip
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/servlet-api-2.2.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/mail.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/commons-lang.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/commons-email.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/commons-io-1.4.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/CustomQueryExtract.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/jsch-0.1.55.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/commons-vfs.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/commons-io-1.4.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/bcpg-jdk16-146.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/bcprov-jdk16-146.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/guava-13.0.1.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/httpclient-4.5.1.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/httpcore-4.4.3.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/commons-codec-1.9.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/slf4j-api-1.7.30.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/slf4j-jdk14-1.7.30.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/sshd-cli-2.5.1.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/sshd-common-2.5.1.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/sshd-core-2.5.1.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/sshd-scp-2.5.1.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/sshd-sftp-2.5.1.jar
set CLASSPATH=%CLASSPATH%;%ETL_ROOT%/resources/
set CLASSPATH=%CLASSPATH%;%ETL_ROOT%/resources/install

set JAVA_EXE="%JAVA_HOME%\bin\java"

set JVM_ARGS=-Xms128m -Xmx512m
set MAIN_CLASS=com.blackboard.services.installer.view.CommandLineInstaller
set COMMAND=%JAVA_EXE% 

set JDK=%JAVA_HOME:.=_%
echo %JDK%
for /f "tokens=* delims=\" %%a in ("%JDK%") do set d=%%~na
set JDK=%d%

if "%JDK:~0,5%"=="jdk-9" goto JDK9
IF "%JDK:~0,6%"=="jdk1_8" goto JDK8
IF "%JDK:~0,6%"=="jdk1_7" goto JDK7
IF "%JDK:~0,6%"=="jdk1_6" goto JDK6
IF "%JDK:~0,6%"=="jdk1_5" goto JDK5
goto javaunknown

:JDK9
set JAVA_VERSION=%JDK:~4,1%
set COMMAND=%COMMAND% --add-modules java.xml.bind
goto execute

:JDK8
set JAVA_VERSION=%JDK:~5,1%
goto execute

:JDK7
set JAVA_VERSION=%JDK:~5,1%
goto notsupported

:JDK6
set JAVA_VERSION=%JDK:~5,1%
goto notsupported

:JDK5
set JAVA_VERSION=%JDK:~5,1%
goto notsupported

:javaunknown
echo Unable to determine java version from %JDK%. It is likely either the JAVA_HOME environment variable is unset, java is installed in a non-standard location, or the java version is very old or higher than 9.
goto end

:notsupported
echo JAVA version %JAVA_VERSION% is not supported. Upgrade to at least Java 8.
goto end

:execute
echo JAVA version: %JAVA_VERSION%
set COMMAND=%COMMAND% %JVM_ARGS% -classpath %CLASSPATH% %MAIN_CLASS% %1 %ETL_ROOT%

echo on
%COMMAND%
goto end

:end
echo off
del %ETL_ROOT%\logs\temp.log*
