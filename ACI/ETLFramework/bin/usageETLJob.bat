echo off
SET ETL_ROOT=..

SET CLASSPATH=%ETL_ROOT%
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/classes/
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/TIAInterface.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/ojdbc8.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/mysql-5.1.13.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/sqljdbc4.2.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/jtds-1.3.1.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/postgresql-9.1.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/ifxjdbc.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/unijdbc.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/asjava.zip
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/jsch.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/mail.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/commons-lang.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/commons-email.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/commons-io-1.4.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/CustomQueryExtract.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/jsch-0.1.44.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/commons-vfs.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/bcpg-jdk16-146.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/bcprov-jdk16-146.jar
set CLASSPATH=%CLASSPATH%;%ETL_ROOT%/resources/
set CLASSPATH=%CLASSPATH%;%ETL_ROOT%/resources/jobs

set JVM_ARGS=-Xms128m -Xmx3072m
set MAIN_CLASS=com.blackboard.services.etl.process.CommandLineProcessor
set COMMAND="%JAVA_HOME%\bin\java" %JVM_ARGS% --add-modules java.xml.bind -classpath %CLASSPATH% %MAIN_CLASS% USAGE %ETL_ROOT%

:loop
IF [%1]==[] GOTO continue
    set COMMAND=%COMMAND% %1
SHIFT
GOTO loop
:continue

echo on
%COMMAND%
