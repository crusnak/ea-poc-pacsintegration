echo off
SET ETL_ROOT=..

SET CLASSPATH=%ETL_ROOT%
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/classes/
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/SecurityManager.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/TIAInterface.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/ojdbc6.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/mysql-5.1.13.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/sqljdbc4.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/mail.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/CustomQueryExtract.jar
set CLASSPATH=%CLASSPATH%;%ETL_ROOT%/resources/
set CLASSPATH=%CLASSPATH%;%ETL_ROOT%/resources/jobs

set JVM_ARGS=-Xms128m -Xmx1536m
set MAIN_CLASS=com.blackboard.services.etl.process.CommandLineProcessor
set COMMAND=%JAVA_HOME_ETL%\bin\java %JVM_ARGS% -classpath %CLASSPATH% %MAIN_CLASS% RESTART %ETL_ROOT%

:loop
IF [%1]==[] GOTO continue
    set COMMAND=%COMMAND% %1
SHIFT
GOTO loop
:continue

echo on
echo ETL RESTARTS ARE NOT SUPPORTED CURRENTLY
