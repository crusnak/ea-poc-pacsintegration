#!/bin/sh 

##########
#
# Usage: salto_etl.sh 
#    ex: salto_export.sh 
#
# Synopsis: calls etl java program
#          
#
#
#
# 2011.03.17 Created TLN
#
##########

# Set Environment Variables
JAVA_HOME=/opt/java6/bin/java 
ETL_ROOT=..
DATA="$ETL_ROOT/input"

CLASSPATH="$ETL_ROOT"
CLASSPATH="$CLASSPATH:$ETL_ROOT/classes"
CLASSPATH="$CLASSPATH:$ETL_ROOT/TIAInterface.jar"
CLASSPATH="$CLASSPATH:$ETL_ROOT/lib/ojdbc6.jar"
CLASSPATH="$CLASSPATH:$ETL_ROOT/lib/mysql-5.1.13.jar"
CLASSPATH="$CLASSPATH:$ETL_ROOT/lib/sqljdbc4.jar"
CLASSPATH="$CLASSPATH:$ETL_ROOT/lib/postgresql-9.1.jar"
CLASSPATH="$CLASSPATH:$ETL_ROOT/lib/ifxjdbc.jar"
CLASSPATH="$CLASSPATH:$ETL_ROOT/lib/CustomQueryExtract.jar"
CLASSPATH="$CLASSPATH:$ETL_ROOT/lib/jtds-1.2.5.jar"
CLASSPATH="$CLASSPATH:$ETL_ROOT/lib/jsch.jar"
CLASSPATH="$CLASSPATH:$ETL_ROOT/lib/mail.jar"
CLASSPATH="$CLASSPATH:$ETL_ROOT/lib/commons-lang.jar"
CLASSPATH="$CLASSPATH:$ETL_ROOT/lib/commons-email.jar"
CLASSPATH="$CLASSPATH:$ETL_ROOT/lib/commons-io-1.4.jar"
CLASSPATH="$CLASSPATH:$ETL_ROOT/lib/jsch-0.1.44.jar"
CLASSPATH="$CLASSPATH:$ETL_ROOT/lib/commons-vfs.jar"
CLASSPATH="$CLASSPATH:$ETL_ROOT/lib/bcpg-jdk16-146.jar"
CLASSPATH="$CLASSPATH:$ETL_ROOT/lib/bcprov-jdk16-146.jar"
CLASSPATH="$CLASSPATH:$ETL_ROOT/resources"
CLASSPATH="$CLASSPATH:$ETL_ROOT/resources/jobs"
CLASSPATH="$CLASSPATH:$ETL_ROOT/com/blackboard/services/etl/process"

JVM_ARGS="-Xms128m -Xmx3072m"
MAIN_CLASS="com.blackboard.services.etl.process.CommandLineProcessor"
COMMAND="$JAVA_HOME $JVM_ARGS -classpath $CLASSPATH $MAIN_CLASS START $ETL_ROOT"


# read command line args (multiple command line arguments are not supported)

#while [ $1 -ne 0 ]
  #do 
#eval $1
#echo $#
	COMMAND="$COMMAND $1"
	#shift
   #done 

# execute etl
exec $COMMAND


