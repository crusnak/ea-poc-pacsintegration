/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl;

import com.blackboard.services.etl.transforms.ArithmeticTransformTest;
import com.blackboard.services.etl.transforms.StringTransformTest;
import com.blackboard.services.logging.AppenderFactory;
import com.blackboard.services.logging.Appenders;
import com.blackboard.services.logging.LayoutFactory;
import com.blackboard.services.logging.Layouts;
import com.blackboard.services.logging.LogFactory;
import java.util.Enumeration;
import java.util.HashMap;
import junit.framework.TestSuite;
import org.apache.log4j.Appender;
import org.apache.log4j.Layout;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author crusnak
 */
@RunWith( Suite.class )
@Suite.SuiteClasses( 
{
    ArithmeticTransformTest.class,
    StringTransformTest.class
} )
public class ETLTestSuite extends TestSuite
{
    @BeforeClass
    public static void setUpClass()
        throws Exception
    {
        Object[] lprops = { "%-5p | [%13F:%-3L] - %m%n" };
        Layout layout = LayoutFactory.createLayout(Layouts.PatternLayout, lprops);
        
        Object[] aprops = { layout };
        Appender appender = AppenderFactory.createAppender(Appenders.ConsoleAppender, aprops);
        
        Appender[] appenders = { appender };
        LogFactory.registerLogger("com.blackboard.services.etl", appenders);
        Logger logger = Logger.getLogger("com.blackboard.services.etl");
        logger.setLevel(Level.DEBUG);
    }

    @AfterClass
    public static void tearDownClass()
            throws Exception
    {
    }

    @Before
    public void setUp()
            throws Exception
    {
    }

    @After
    public void tearDown()
            throws Exception
    {
    }
}

