/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl;

import com.blackboard.services.etl.data.DataCell;
import com.blackboard.services.etl.data.DataColumn;
import com.blackboard.services.etl.data.RelationalContainer;
import com.blackboard.services.etl.data.TransformSource;
import com.blackboard.services.etl.data.TransformTarget;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.exception.ETLTestException;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.EtlTest;
import com.blackboard.services.etl.jaxb.EtlTestChoiceComplexType;
import com.blackboard.services.etl.tasks.DataMapping;
import com.blackboard.services.etl.transforms.AbstractTransform;
import com.blackboard.services.utils.ResourceLoader;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.apache.log4j.Logger;
import static org.junit.Assert.*;

/**
 *
 * @author crusnak
 */
public class ETLTestFactory
{
    private static Logger logger = null;
    
    public static Collection<Object[]> loadTestDefinition(String filename)
        throws ETLException
    {
        Collection<Object[]> data = new ArrayList();
        File appRoot = new File(".");
        logger = Logger.getLogger(ETLTestFactory.class);

        try
        {
            logger.debug(appRoot.getCanonicalPath());
            URL testURL = ResourceLoader.findResourceAsURL(null, appRoot, filename);

            File testFile = new File(testURL.getFile());
            TestLoader loader = new TestLoader(appRoot, testFile.getName());
            EtlTest test = loader.getETLTestCase();
            data = ETLTestFactory.prepareTestCase(test.getValue());
        }
        catch (ETLException e)
        {
            throw new ETLTestException("Failed to initialize etl test case", e);
        }
        catch (IOException e)
        {
            throw new ETLTestException("Cannot find etl test case: " + filename, e);
        }
        
        return data;
    }
    
    public static Collection<Object[]> prepareTestCase(EtlTestChoiceComplexType tc)
        throws ETLException
    {
        ArrayList<Object[]> params = new ArrayList();
        logger.info("Processing " + tc.getTransformOrTask().size() + " tests");
        for (Object o: tc.getTransformOrTask() )
        {
            if ( o instanceof EtlTestChoiceComplexType.Task )
            {
                params.add(prepareTestTask((EtlTestChoiceComplexType.Task)o));
            }
            else if ( o instanceof EtlTestChoiceComplexType.Transform )
            {
                params.add(prepareTestTransform((EtlTestChoiceComplexType.Transform)o));
            }
            else
            {
                throw new ETLTestException("Cannot test Object of type: " + o.getClass());
            }
        }
        
        for (Object[] param: params )
        {
            for ( int i=0; i<param.length; i++ )
            {
                logger.trace("Param #" + (i+1) + "=" + param[i]);
            }
        }

        return params;
    }
    
    private static Object[] prepareTestTask(EtlTestChoiceComplexType.Task ct)
        throws ETLException
    {
        Object[] params = new Object[0];
        
        return params;
    }
    
    private static Object[] prepareTestTransform(EtlTestChoiceComplexType.Transform ct)
        throws ETLException
    {
        Object[] params = new Object[0];
        if ( ct.getAssign() != null )
        {
        }
        else if ( ct.getBinary() != null )
        {
        }
        else if ( ct.getBoolean() != null )
        {
            params = new Object[3];
            params[0] = ct.getName();
            params[1] = ct.getExpectedDataType();
            params[2] = ct.getBoolean();
        }
        else if ( ct.getDate() != null )
        {
            params = new Object[4];
            params[0] = ct.getName();
            params[1] = ct.getExpectedDataType();
            params[2] = ct.getDate();
            params[3] = ct.getModifier();
        }
        else if ( ct.getLookup() != null )
        {
        }
        else if ( ct.getMath() != null )
        {
            params = new Object[3];
            params[0] = ct.getName();
            params[1] = ct.getExpectedDataType();
            params[2] = ct.getMath();
        }
        else if ( ct.getString() != null )
        {
            params = new Object[3];
            params[0] = ct.getName();
            params[1] = ct.getExpectedDataType();
            params[2] = ct.getString();
        }
        else
        {
            throw new ETLTestException("No supported transforms found for: " + ct.getName());
        }
        
        return params;
    }

    public static void testTransform(AbstractTransform tform, DataTypes dataType)
       throws ETLException
    {
        List<TransformSource> sources = tform.getSourceColumns();
        List<TransformTarget> targets = tform.getTargetColumns();
        List<DataCell> inputs = new ArrayList();
        List<DataCell> outputs = new ArrayList();
        List<DataColumn> tcols = new ArrayList();
        RelationalContainer empty = new RelationalContainer(null);
        
        for ( int i=0; i<sources.size(); i++ )
        {
            inputs = DataMapping.extractSourceData(empty, tform, i);
        }
        
        for ( int i=0; i<targets.size(); i++ )
        {
            TransformTarget tgt = targets.get(i);
            DataColumn col = new DataColumn(dataType, "Output" + (i+1));
            tcols.add(col);
            outputs.add(new DataCell(col, tgt.getColumnName()));
        }
        
        Logger.getLogger(ETLTestFactory.class).debug("Inputs: " + inputs);
        Logger.getLogger(ETLTestFactory.class).debug("Outputs: " + outputs);
        
        List<DataCell> results = tform.transform(tcols, inputs);
        
        Logger.getLogger(ETLTestFactory.class).debug("Results: " + results);

        assertEquals("Comparing expected target count to actual target count: ", outputs.size(), results.size());
        
        for ( int i=0; i<outputs.size(); i++ )
        {
            DataCell output = outputs.get(i);
            DataCell result = results.get(i);
            assertEquals("Comparing expected cell value index #" + i + " to actual cell value index #" + i + ":", output, result);
            logger.info("<--------------" + outputs.get(i) + "=" + results.get(i) + "-------------->");
        }
        
    }

    public static void testDateTransform(AbstractTransform tform, DataTypes dataType, String modifier)
       throws Exception
    {
        List<TransformSource> sources = tform.getSourceColumns();
        List<TransformTarget> targets = tform.getTargetColumns();
        List<DataCell> inputs = new ArrayList();
        List<DataCell> outputs = new ArrayList();
        List<DataColumn> tcols = new ArrayList();
        RelationalContainer empty = new RelationalContainer(null);
        
        for ( int i=0; i<sources.size(); i++ )
        {
            inputs = DataMapping.extractSourceData(empty, tform, i);
        }
        
        for ( int i=0; i<targets.size(); i++ )
        {
            TransformTarget tgt = targets.get(i);
            DataColumn col = new DataColumn(dataType, "Output" + (i+1));
            tcols.add(col);
            if ( dataType == DataTypes.DATE && modifier != null )
            {
                SimpleDateFormat format = new SimpleDateFormat(modifier);
                outputs.add(new DataCell(col, format.parse(tgt.getColumnName())));
            }
            else
            {
                outputs.add(new DataCell(col, tgt.getColumnName()));
            }
        }
        
        Logger.getLogger(ETLTestFactory.class).debug("Inputs: " + inputs);
        Logger.getLogger(ETLTestFactory.class).debug("Outputs: " + outputs);
        
        List<DataCell> results = tform.transform(tcols, inputs);
        
        Logger.getLogger(ETLTestFactory.class).debug("Results: " + results);

        assertEquals("Comparing expected target count to actual target count: ", outputs.size(), results.size());
        
        for ( int i=0; i<outputs.size(); i++ )
        {
            DataCell output = outputs.get(i);
            DataCell result = results.get(i);
            assertEquals("Comparing expected cell value index #" + i + " to actual cell value index #" + i + ":", output, result);
            logger.info("<--------------" + outputs.get(i) + "=" + results.get(i) + "-------------->");
        }
        
    }
    
}
