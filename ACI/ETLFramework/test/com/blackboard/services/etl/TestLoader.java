/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.etl;

import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.exception.ETLTestException;
import com.blackboard.services.etl.jaxb.EtlTest;
import com.blackboard.services.etl.jaxb.ObjectFactory;
import com.blackboard.services.utils.ResourceLoader;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;

/**
 *
 * @author crusnak
 */
public class TestLoader
{
    File xml = null;
    EtlTest test = null;
    File etlRoot = null;

    public TestLoader(File root, String f)
        throws ETLException
    {
        try
        {
            etlRoot = root;
            xml = ResourceLoader.findResourceAsFile(null, root, f);
            JAXBContext jc = JAXBContext.newInstance("com.blackboard.services.etl.jaxb");
            Unmarshaller um = jc.createUnmarshaller();
            test = (EtlTest)um.unmarshal(xml);
        }
        catch (IOException e)
        {
            throw new ETLException("Fatal Error", e);
        }
        catch (JAXBException e)
        {
            throw new ETLException("Fatal Error", e);
        }
    }

    public EtlTest getETLTestCase()
        throws ETLException
    {
        return test;
    }
    

    public File getLoadedFile()
    {
        return xml;
    }
    
    public static String convertXMLToString(Object xml)
        throws ETLException
    {
        String output = null;
        try
        {
            javax.xml.bind.JAXBContext context = javax.xml.bind.JAXBContext.newInstance("com.blackboard.services.etl.jaxb");
            Marshaller m = context.createMarshaller();
            StringWriter writer = new StringWriter();
            m.marshal(new ObjectFactory().createXml(xml), writer);
            output = writer.toString();
        }
        catch (javax.xml.bind.JAXBException e)
        {
            throw new ETLException("Unable to convert XML to string", e);
        }

        return output;
    }

    public static String convertComplexTypeToString(Object ct, Class clazz)
        throws ETLException
    {
        String output = null;
        try
        {
            javax.xml.bind.JAXBContext context = javax.xml.bind.JAXBContext.newInstance("com.blackboard.services.etl.jaxb");
            Marshaller m = context.createMarshaller();
            StringWriter writer = new StringWriter();
            m.marshal(new JAXBElement(new QName(""), clazz, ct), writer);
            output = writer.toString();
        }
        catch (javax.xml.bind.JAXBException e)
        {
            throw new ETLException("Unable to convert complex type to string", e);
        }

        return output;
    }

}
