/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.transforms;

import com.blackboard.services.etl.ETLTestFactory;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.jaxb.DataTypes;
import com.blackboard.services.etl.jaxb.EtlTest;
import com.blackboard.services.etl.jaxb.DateTransformComplexType;

import com.blackboard.services.logging.AppenderFactory;
import com.blackboard.services.logging.Appenders;
import com.blackboard.services.logging.LayoutFactory;
import com.blackboard.services.logging.Layouts;
import com.blackboard.services.logging.LogFactory;
import java.io.File;
import java.util.Collection;
import org.apache.log4j.Appender;
import org.apache.log4j.Layout;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import static org.junit.Assert.*;

/**
 *
 * @author crusnak
 */
@RunWith(Parameterized.class)
public class DateTransformTest 
{
    private static String TRANSFORM_FILE =              "test-date-transforms.xml";

    private AbstractTransform tform = null;
    private String testName = null;
    private DataTypes expectedDataType = null;
    private String modifier = null;
    
    @Parameterized.Parameters(name="{0}")
    public static Collection<Object[]> data()
       throws Exception
    {
       return ETLTestFactory.loadTestDefinition(TRANSFORM_FILE);
    }
    
    public DateTransformTest(String name, DataTypes expected, DateTransformComplexType ct, String mod)
        throws ETLException
    {
        testName = name;
        expectedDataType = expected;
        modifier = mod;
        
        tform = TransformFactory.createTransform(ct, null);
        if ( !(tform instanceof DateTransform) )
        {
            throw new ETLException("Definition file contains tests that are not DateTransforms: " + tform.getClass());
        }
    }

    @BeforeClass
    public static void setUpClass()
        throws Exception
    {
        Object[] lprops = { "%-5p | [%13F:%-3L] - %m%n" };
        Layout layout = LayoutFactory.createLayout(Layouts.PatternLayout, lprops);
        
        Object[] aprops = { layout };
        Appender appender = AppenderFactory.createAppender(Appenders.ConsoleAppender, aprops);
        
        Appender[] appenders = { appender };
        LogFactory.registerLogger("com.blackboard.services.etl", appenders);
        Logger logger = Logger.getLogger("com.blackboard.services.etl");
        logger.setLevel(Level.TRACE);
    }

    @AfterClass
    public static void tearDownClass()
        throws Exception
    {
    }
    
    @Before
    public void setUp()
    {
    }
    
    @After
    public void tearDown()
    {
    }

    /**
     * Test of transform method, of class StringTransform.
     */
    @Test
    public void testDateTransform()
        throws Exception
    {
        Logger.getLogger(this.getClass()).info("----------------------- Testing DateTransform: " + testName + "------------------------");
        Logger.getLogger(this.getClass()).info("DateTransform: " + tform);
        ETLTestFactory.testDateTransform(tform, expectedDataType, modifier);
    }
}
