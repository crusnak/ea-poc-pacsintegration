/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.etl.exception;

/**
 *
 * @author crusnak
 */
public class ETLTestException extends ETLException
{
    public ETLTestException()
    {
        super();
    }

    public ETLTestException(String message)
    {
        super(message);
    }

    public ETLTestException(Throwable cause)
    {
        super(cause);
    }

    public ETLTestException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
