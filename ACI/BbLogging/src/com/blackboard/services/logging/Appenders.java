/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.logging;

/**
 *
 * @author crusnak
 */
public enum Appenders
{
    ConsoleAppender,
    FileAppender,
    RollingFileAppender,
    DailyRollingFileAppender,
    BbRollingFileAppender,
    BbDynamicRollingFileAppender
}
