/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.logging;

import com.blackboard.services.logging.appender.BbRollingFileAppender;
import com.blackboard.services.utils.JavaUtils;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.DailyRollingFileAppender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.HTMLLayout;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.RollingFileAppender;
import org.apache.log4j.SimpleLayout;
import org.apache.log4j.xml.XMLLayout;

/**
 *
 * @author crusnak
 */
public class LoggingMetadata
{
    private static HashMap<Enum,Class> classMap = new HashMap();

    static
    {
        classMap.put(Appenders.ConsoleAppender, ConsoleAppender.class);
        classMap.put(Appenders.DailyRollingFileAppender, DailyRollingFileAppender.class);
        classMap.put(Appenders.FileAppender, FileAppender.class);
        classMap.put(Appenders.RollingFileAppender, RollingFileAppender.class);
        classMap.put(Appenders.BbRollingFileAppender, BbRollingFileAppender.class);

        classMap.put(Layouts.HTMLLayout, HTMLLayout.class);
        classMap.put(Layouts.PatternLayout, PatternLayout.class);
        classMap.put(Layouts.SimpleLayout, SimpleLayout.class);
        classMap.put(Layouts.XMLLayout, XMLLayout.class);
    }

    private LinkedHashMap<String,DataType> defs = new LinkedHashMap();
    private LinkedHashMap<String,Object> vals = new LinkedHashMap();
    private Class classDef = null;
    private Enum metadataType = null;

    public LoggingMetadata(Enum type)
        throws IllegalArgumentException
    {
        if ( !classMap.containsKey(type) )
        {
            EnumSet a = EnumSet.allOf(Appenders.class);
            EnumSet l = EnumSet.allOf(Layouts.class);
            List accepted = JavaUtils.enumToList(a);
            accepted.addAll(JavaUtils.enumToList(l));
            throw new IllegalArgumentException("Invalid LoggingMetadata type, only accepts: " + accepted);
        }
        metadataType = type;
        classDef = classMap.get(type);
    }

    public LoggingMetadata(LoggingMetadata copy)
    {
        this(copy.getMetadataType());
        this.defs = copy.defs;
    }

    public DataType getPropertyDefinition(String name)
    {
        return defs.get(name);
    }

    public DataType getPropertyDefinition(Enum name)
    {
        return defs.get(String.valueOf(name));
    }

    DataType setPropertyDefinition(String name, DataType type)
    {
        return defs.put(name, type);
    }

    DataType setPropertyDefinition(Enum name, DataType type)
    {
        return defs.put(String.valueOf(name), type);
    }

    public Object getPropertyValue(String name)
    {
        Object val = vals.get(name);
        DataType def = defs.get(name);
        if ( val == null && def.getDefaultValue() != null )
        {
            val = def.getDefaultValue();
        }
        return val;
    }

    public Object getPropertyValue(Enum name)
    {
        return getPropertyValue(String.valueOf(name));
    }

    void setPropertyValue(String name, Object v)
        throws LoggingInitializationException
    {
        DataType dt = defs.get(name);
        try
        {
            if ( dt != null )
            {
                if ( v instanceof String )
                {
                    if ( dt.getClassType() == Boolean.class )
                    {
                        v = Boolean.valueOf((String)v);
                    }
                    else if ( dt.getClassType() == Integer.class )
                    {
                        v = Integer.valueOf((String)v);
                    }
                    else if ( dt.getClassType() == Long.class )
                    {
                        v = Long.valueOf((String)v);
                    }
                }

                if ( v == null || dt.getClassType().isInstance(v) )
                {
                    vals.put(name, v);
                }
                else
                {
                    throw new IllegalArgumentException("Mismatched type: expecting " + dt.getClassType().getName() +
                                                       " but found " + v.getClass().getName());
                }
            }
            else
            {
                throw new LoggingInitializationException("Unknown property name: " + name, this);
            }
        }
        catch (IllegalArgumentException e)
        {
            throw new LoggingInitializationException("Error setting property '" + name + "'", e, this);
        }
        catch (LoggingInitializationException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new LoggingInitializationException(e, this);
        }
    }

    void setPropertyValue(Enum name, Object val)
        throws LoggingInitializationException
    {
        setPropertyValue(String.valueOf(name), val);
    }


    void setPropertyValues(Object[] values)
        throws LoggingInitializationException
    {
        LinkedHashMap vmap = new LinkedHashMap();
        String[] names = getPropertyNames().toArray(new String[0]);
        for ( int i=0; i<values.length; i++ )
        {
            if ( i < names.length )
            {
                vmap.put(names[i], values[i]);
            }
        }
        setPropertyValues(vmap.entrySet());
    }

    void setPropertyValues(Set<Map.Entry> values)
        throws LoggingInitializationException
    {
        if ( (values == null && getRequiredProperties().size() > 0) ||
              values != null && values.size() < getRequiredProperties().size() )
        {
            throw new LoggingInitializationException("Missing required properties: " + getRequiredProperties(), this);
        }

        for ( Map.Entry entry: values )
        {
            String key = String.valueOf(entry.getKey());
            setPropertyValue(key, entry.getValue());
        }
    }

    public Set<Map.Entry<String,DataType>> getPropertyDefinitions()
    {
        return defs.entrySet();
    }

    public List<String> getPropertyNames()
    {
        return new ArrayList(defs.keySet());
    }

    public Map<String,DataType> getRequiredProperties()
    {
        LinkedHashMap<String,DataType> props = new LinkedHashMap();
        for ( Map.Entry<String,DataType> entry: getPropertyDefinitions() )
        {
            if ( entry.getValue().isRequired() )
            {
                props.put(entry.getKey(), entry.getValue());
            }
        }

        return props;
    }

    public Class getClassDefinition()
    {
        return classDef;
    }

    public Enum getMetadataType()
    {
        return metadataType;
    }

    public String toString()
    {
        StringBuffer out = new StringBuffer();
        out.append(classDef.getName() + "@" + hashCode() + "->");
        out.append("[");
        String[] names = getPropertyNames().toArray(new String[0]);
        for ( int i=0; i<names.length; i++ )
        {
            DataType dt = getPropertyDefinition(names[i]);
            Object value = getPropertyValue(names[i]);
            out.append(names[i] + "=");
            out.append(value);
            if ( value != null && value.equals(dt.getDefaultValue()) )
            {
                out.append("{DEFAULT}");
            }
            out.append(dt);
            if ( i < names.length-1 )
            {
                out.append(", ");
            }
        }
        out.append("]");
        return out.toString();
    }
}
