/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.logging;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.HTMLLayout;
import org.apache.log4j.Layout;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.SimpleLayout;
import org.apache.log4j.xml.XMLLayout;

/**
 *
 * @author crusnak
 */
public class LayoutFactory extends PropertyFactory
{
    private static HashMap<Layouts,LoggingMetadata> layouts = new HashMap();

    static
    {
        // Setup
        LoggingMetadata mdata = new LoggingMetadata(Layouts.SimpleLayout);
        layouts.put(Layouts.SimpleLayout, mdata);
        
        mdata = new LoggingMetadata(Layouts.PatternLayout);
        mdata.setPropertyDefinition(LayoutOptions.ConversionPattern, new DataType(String.class, PatternLayout.DEFAULT_CONVERSION_PATTERN, false));
        layouts.put(Layouts.PatternLayout, mdata);

        mdata = new LoggingMetadata(Layouts.XMLLayout);
        mdata.setPropertyDefinition(LayoutOptions.LocationInfo, new DataType(Boolean.class, false, false));
        mdata.setPropertyDefinition(LayoutOptions.Properties, new DataType(Boolean.class, false, false));
        layouts.put(Layouts.XMLLayout, mdata);

        mdata = new LoggingMetadata(Layouts.HTMLLayout);
        mdata.setPropertyDefinition(LayoutOptions.LocationInfo, new DataType(Boolean.class, false, false));
        mdata.setPropertyDefinition(LayoutOptions.Title, new DataType(String.class, "Log4J Log Messages", false));
        layouts.put(Layouts.HTMLLayout, mdata);
    }

    public static LoggingMetadata getLayoutMetadata(Layouts layout)
    {
        return layouts.get(layout);
    }

    public static Layout createLayout(Layouts type, Object[] props)
        throws LoggingInitializationException
    {
        LoggingMetadata mdata = getLayoutMetadata(type);
        LinkedHashMap values = new LinkedHashMap();
        String[] names = mdata.getPropertyNames().toArray(new String[0]);
        for ( int i=0; i<props.length; i++ )
        {
            if ( i < names.length )
            {
                values.put(names[i], props[i]);
            }
        }
        mdata.setPropertyValues(values.entrySet());
        return createLayout(type, mdata);
    }

    public static Layout createLayout(Layouts type, Map props)
        throws LoggingInitializationException
    {
        LoggingMetadata mdata = getLayoutMetadata(type);
        mdata.setPropertyValues(props.entrySet());
        return createLayout(type, mdata);
    }

    public static Layout createLayout(Layouts type, LoggingMetadata mdata)
        throws LoggingInitializationException
    {
        Layout layout = null;
        Class clazz = mdata.getClassDefinition();
        //System.out.println("Creating layout of " + clazz);
        //System.out.println(mdata.getPropertyDefinitions());

        try
        {
            // Ensure that the underlying class is assignable as the Layout class
            if ( !Layout.class.isAssignableFrom(clazz) )
            {
                throw new LoggingInitializationException("Invalid Layout: found " + clazz, mdata);
            }

            Object obj = clazz.newInstance();
            for ( Map.Entry<String,DataType> entry: mdata.getPropertyDefinitions() )
            {
                String accessor = getAccessor(Accessor.set, entry.getKey());
                DataType dt = entry.getValue();
                Object value = mdata.getPropertyValue(entry.getKey());
                if ( dt.isRequired() || value != null )
                {
                    //System.out.println(accessor + "(" + dt.getValue() + ")");
                    Class[] ptypes = { convertToPrimitiveClass(dt.getClassType()) };
                    Method method = clazz.getMethod(accessor, ptypes);
                    //System.out.println(method);
                    method.invoke(obj, value);
                    //accessor = getAccessor(Accessor.get, entry.getKey());
                    //method = clazz.getMethod(accessor, null);
                    //System.out.println(clazz.getSimpleName() + "." + accessor + "=" + method.invoke(obj, null));
                }
            }

            Method method = clazz.getMethod("activateOptions", (Class[])null);
            method.invoke(obj, (Object[])null);
            layout = (Layout)obj;

        }
        catch (IllegalAccessException e) { throw new LoggingInitializationException(e, mdata); }
        catch (InstantiationException e) { throw new LoggingInitializationException(e, mdata); }
        catch (InvocationTargetException e) { throw new LoggingInitializationException(e, mdata); }
        catch (NoSuchMethodException e) { throw new LoggingInitializationException(e, mdata); }

        return layout;
    }

    public static void main(String[] args)
    {
        try
        {
            Class.forName("com.blackboard.services.logging.LayoutFactory");
            System.out.println(LayoutFactory.layouts);

            LoggingMetadata mdata = LayoutFactory.getLayoutMetadata(Layouts.PatternLayout);
            System.out.println(mdata);
            System.out.println(mdata.getRequiredProperties());

            Object[] props = { "%-5p [%t]: %m%n" };
            mdata.setPropertyValues(props);
            System.out.println(mdata);

            Layout l = LayoutFactory.createLayout(Layouts.PatternLayout, mdata);
            System.out.println(l);

            java.util.HashMap<String,Object> map = new java.util.HashMap();
            map.put("ConversionPattern", "%t");
            map.put("LocationInfo", true);
            l = LayoutFactory.createLayout(Layouts.XMLLayout, map);
            System.out.println(l);
        }
        catch (Exception e) { e.printStackTrace(); }
    }
}
