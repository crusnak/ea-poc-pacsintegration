/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.logging;

import com.blackboard.services.utils.BaseLogger;
import com.blackboard.services.utils.BbBaseProperties;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletContext;
import org.apache.log4j.Appender;
import org.apache.log4j.Layout;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * This class is used to define a common interface to properties files as well as allowing
 * LOG4J property values to be included in the properties file.  This gives greater control
 * over LOG4J properties, especially if you have a project that has more than one properties
 * file.
 * @author crusnak
 */
public class LoggingProperties extends BbBaseProperties
{
    private static final String LOG4J =                     "log4j";
    private static final String LOGGER =                    "logger";
    private static final String ROOT_LOGGER =               "rootLogger";
    private static final String APPENDER =                  "appender";
    private static final String LAYOUT =                    "layout";


    private HashMap<String,LoggingMetadata> appenderData = new HashMap();
    private HashMap<String,LoggingMetadata> layoutData = new HashMap();
    private HashMap<String,Logger> loggers = new HashMap();
    private static LoggingProperties instance = null;

    /**
     * Internal constructor used to define the servlet context, application root, and the
     * resource of the properties file.
     * @param context The ServletContext for finding all resources associated with this class
     * in a web context.
     * @param root The application root directory used as a fallback if files are not found
     * within the web context.
     * @param resource The application resource (properties filename).
     * @throws IOException thrown if the resource is not able to be found.  <tt>Note:</tt> this
     * means that if you are generating properties files on the fly this class will not work
     * unless you create an empty file to work from.  Best practice is to create an empty
     * file at installation or build time to ensure that the underlying properties class can
     * find it's properties file and update it upon saving.
     */
    protected LoggingProperties(ServletContext context, File root, String resource)
        throws IOException
    {
        super(context, root, resource);
    }
    
    /**
     * Initializes the Log4J properties by setting the required contexts in order for the ResourceLoader
     * to find the class resources properly.  This is a convenience method to initialize log4j from a specific
     * properties file programmatically.  This may be necessary if the environment running the VM has
     * restrictions on how it loads log4j.
     * @param ctx The ServletContext for finding all resources associated with this class
     * in a web context.
     * @param root The application root directory used as a fallback if files are not found
     * within the web context.
     * @param resource The application resource (properties filename) and requisite path.
     * @return the singleton class instance
     * @throws IOException thrown if the class resource is not able to be found.  <tt>Note:</tt> this
     * means that if you are generating properties files on the fly this class will not work
     * unless you create an empty file to work from.  Best practice is to create an empty
     * file at installation or build time to ensure that the underlying properties class can
     * find it's properties file and update it upon saving.
     */
    public static LoggingProperties initialize(ServletContext context, File root, String resource)
        throws IOException
    {
        BaseLogger.info("Initializing log4j properties from: " + resource);
        if ( instance == null )
        {
            instance = new LoggingProperties(context, root, resource);
        }
        return instance;
    }

    /**
     * Overridden to also process any LOG4J properties that may be present. This method is always
     * called from the underlying load operation in the super class.
     * @param inStream The input stream to load from.
     * @throws IOException thrown if an error occurs while reading from the stream.
     * @see BbBaseProperties#load()
     */
    @Override
    public synchronized void load(InputStream inStream)
            throws IOException
    {
        super.load(inStream);
        parseLog4JProperties();
    }

    /**
     * Overridden to also process any LOG4J properties that may be present. This
     * method ensures that if a Reader is used it is handled the same as if a stream is used.
     * @param reader The reader to load from.
     * @throws IOException thrown if an error occurs while reading from the reader.
     */
    @Override
    public synchronized void load(Reader reader)
            throws IOException
    {
        super.load(reader);
        parseLog4JProperties();
    }

    /**
     * Accessor to the named Logger defined by the underlying properties file.
     * @param name The name of the logger to retrieve
     * @return The Logger defined by the given name, or null if not defined.
     */
    public Logger getLogger(String name)
    {
        return loggers.get(name);
    }

    /**
     * This is the main method for processing all existing LOG4J properties in the
     * underlying properties file.  It processes all appender, layout, and logger
     * properties and registers them with the LOG4J engine.  The properties are
     * parsed in the order they are read in, meaning that if loggers are defined
     * using layouts or appenders that have yet to be defined, the registration
     * would fail.
     */
    private void parseLog4JProperties()
    {
        appenderData = new HashMap();
        layoutData = new HashMap();
        loggers = new HashMap();

        //Enumeration names = propertyNames();
        BaseLogger.info("Parsing any log4j properties for: " + this.getClass());
        ArrayList names = new ArrayList(keySet());
        Collections.sort(names);

        for ( Object o: names )
        {
            String name = (String)o;
            if ( name.contains(LOG4J) )
            {
                if ( name.contains(APPENDER) )
                {
                    parseAppender(name);
                }
                else if ( name.contains(LOGGER) )
                {
                    parseLogger(name);
                }
                else if ( name.contains(ROOT_LOGGER) )
                {
                    parseLogger(name);
                }
            }
        }
    }

    /**
     * Parses appender properties with the name of the property given.
     * @param name The property name defining a property who's value is to be processed as an appender.
     */
    private void parseAppender(String name)
    {
        try
        {
            if ( name.contains(LAYOUT) )
            {
                parseLayout(name);
            }
            else
            {
                String allowed = getAllowedProperties(AppenderOptions.values());
                //System.out.println(APPENDER + ": " + name);
                Pattern pp = Pattern.compile("^" + LOG4J + "\\." + APPENDER + "\\.(.+)\\.(" + allowed + ")$");
                Pattern pa = Pattern.compile("^" + LOG4J + "\\." + APPENDER + "\\.(.+)$");
                Matcher mp = pp.matcher(name);
                Matcher ma = pa.matcher(name);
                if ( mp.matches() )
                {
                    String aname = mp.group(1);
                    String prop = mp.group(2);
                    String comp = aname + "." + prop;
                    String val = getProperty(name);
                    LoggingMetadata adata = appenderData.get(aname);
                    if ( adata != null )
                    {
                        //System.out.println("Setting " + aname + "." + prop + "=" + val);
                        adata.setPropertyValue(prop, val);
                    }
                    else
                    {
                        System.err.println("Can't set property '" + comp + "' since the appender wasn't able to be created");
                    }
                }
                else if ( ma.matches() )
                {
                    String aname = ma.group(1);
                    String appenderClass = getProperty(name);
                    String aShortClass = appenderClass.substring(appenderClass.lastIndexOf(".")+1);
                    Appenders atype = null;
                    
                    // Ensure the appender class passed is supported
                    try
                    {
                        atype = Appenders.valueOf(aShortClass);
                    }
                    catch (IllegalArgumentException e)
                    {
                        throw new LoggingInitializationException("Unsupported Appender '" + aShortClass + "' for appender name '" + aname + "'", e, null);
                    }

                    appenderData.put(aname, AppenderFactory.getAppenderMetadata(atype));
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace(System.err);
        }
    }

    /**
     * Parses layout properties with the name of the property given.
     * @param name The property name defining a property who's value is to be processed as a layout.
     */
    private void parseLayout(String name)
    {
        try
        {
            String allowed = getAllowedProperties(LayoutOptions.values());
            //System.out.println(LAYOUT + ": " + name);
            Pattern pp = Pattern.compile("^" + LOG4J + "\\." + APPENDER + "\\.(.+)\\." + LAYOUT + "\\.(" + allowed + ")$");
            Pattern pl = Pattern.compile("^" + LOG4J + "\\." + APPENDER + "\\.(.+)\\." + LAYOUT + "$");
            Matcher mp = pp.matcher(name);
            Matcher ml = pl.matcher(name);
            if ( mp.matches() )
            {
                String lname = mp.group(1);
                String prop = mp.group(2);
                String comp = lname + "." + LAYOUT + "." + prop;
                String val = getProperty(name);
                LoggingMetadata ldata = layoutData.get(lname);
                if ( ldata != null )
                {
                    ldata.setPropertyValue(prop, val);
                }
                else
                {
                    System.err.println("Can't set property '" + comp + "' since the appender wasn't able to be created");
                }
            }
            else if ( ml.matches() )
            {
                String lname = ml.group(1);
                //System.out.println(appenderData.get(lname));
                String layoutClass = getProperty(name);
                String lShortClass = layoutClass.substring(layoutClass.lastIndexOf(".")+1);
                Layouts ltype = null;

                // Ensure the layout class passed is supported
                try
                {
                    ltype = Layouts.valueOf(lShortClass);
                }
                catch (IllegalArgumentException e)
                {
                    throw new LoggingInitializationException("Unsupported Layout '" + lShortClass + "' for layout name '" + lname + "'", e, null);
                }

                layoutData.put(lname, LayoutFactory.getLayoutMetadata(ltype));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace(System.err);
        }
    }

    /**
     * Parses logger properties with the name of the property given.
     * @param name The property name defining a property who's value is to be processed as an logger.
     */
    private void parseLogger(String name)
    {
        try
        {
            //System.out.println(LOGGER + ": " + name);
            Pattern p1 = Pattern.compile("^" + LOG4J + "\\." + LOGGER + "\\.(.+)$");
            Pattern p2 = Pattern.compile("^" + LOG4J + "\\." + ROOT_LOGGER + "$");
            Matcher m1 = p1.matcher(name);
            Matcher m2 = p2.matcher(name);

            // Named Logger
            if ( m1.matches() )
            {
                ArrayList<Appender> appenders = new ArrayList();
                Level level = Level.OFF;
                String lname = m1.group(1);
                String pval = getProperty(name);
                //System.out.println(m.group(1) + "=" + pval);
                String[] values = pval.split(",\\s*");
                for ( int i=0; i<values.length; i++ )
                {
                    if ( i == 0 )
                    {
                        level = Level.toLevel(values[i]);
                        BaseLogger.info("Setting log level to " + level + " for logger: " + lname);
                    }
                    else
                    {
                        LoggingMetadata ldata = layoutData.get(values[i]);
                        LoggingMetadata adata = appenderData.get(values[i]);
                        if ( ldata != null && adata != null )
                        {
                            Layout layout = LayoutFactory.createLayout((Layouts)ldata.getMetadataType(), ldata);
                            adata.setPropertyValue(LAYOUT, layout);
                            appenders.add(AppenderFactory.createAppender((Appenders)adata.getMetadataType(), adata));
                        }
                        else
                        {
                            throw new LoggingInitializationException("Logger '" + lname + "' cannot add unknown appender: " + values[i], null);
                        }
                    }
                }

                LogFactory.registerLogger(lname, appenders);
                Logger logger = Logger.getLogger(lname);
                logger.setLevel(level);
                loggers.put(lname, logger);
            }
            // Root Logger
            else if ( m2.matches() )
            {
                ArrayList<Appender> appenders = new ArrayList();
                Level level = Level.OFF;
                String pval = getProperty(name);
                String[] values = pval.split(",\\s*");
                for ( int i=0; i<values.length; i++ )
                {
                    if ( i == 0 )
                    {
                        level = Level.toLevel(values[i]);
                        BaseLogger.info("Setting log level to " + level + " for ROOT_LOGGER");
                    }
                    else
                    {
                        LoggingMetadata ldata = layoutData.get(values[i]);
                        LoggingMetadata adata = appenderData.get(values[i]);
                        if ( ldata != null && adata != null )
                        {
                            Layout layout = LayoutFactory.createLayout((Layouts)ldata.getMetadataType(), ldata);
                            adata.setPropertyValue(LAYOUT, layout);
                            appenders.add(AppenderFactory.createAppender((Appenders)adata.getMetadataType(), adata));
                        }
                        else
                        {
                            throw new LoggingInitializationException("ROOT_LOGGER " + " cannot add unknown appender: " + values[i], null);
                        }
                    }
                }

                LogFactory.registerRootLogger(appenders);
                Logger logger = Logger.getRootLogger();
                logger.setLevel(level);
                loggers.put(ROOT_LOGGER, logger);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace(System.err);
        }
    }

    /**
     * Convenience method used to output which properties are allowed for an appender
     * or a layout in a tokenized string (token = |).  This is used to dynamically generate
     * an OR based regex expression.
     * @param props An array of objects
     * @return A tokenized string of each object using '|' as a token
     * @see AppenderOptions
     * @see LayoutOptions
     */
    private String getAllowedProperties(Object[] props)
    {
        String out = "";
        for ( int i=0; i<props.length; i++ )
        {
            out += String.valueOf(props[i]);
            if ( i < props.length - 1 )
            {
                out += "|";
            }
        }

        return out;
    }

    /**
     * Returns a string representation of this properties class that doesn't include any log4j properties.
     * @return the string
     */
    @Override
    public String toString()
    {
        return toString(false);
    }

    /**
     * Overloaded method to return a string representation of this properties class with the
     * option to include all log4j properties or not.
     * @param includeLog4j <tt>true</tt> to include all log4j properties in the output.
     * @return the string
     */
    public String toString(boolean includeLog4j)
    {
        String out = "";
        if ( includeLog4j )
        {
            out = super.toString();
        }
        else
        {
            out += "{";
            Enumeration names = propertyNames();
            while ( names.hasMoreElements() )
            {
                String name = (String)names.nextElement();
                if ( !name.contains(LOG4J) )
                {
                    out += name + "=" + getProperty(name);
                    if ( names.hasMoreElements() )
                    {
                        out += ", ";
                    }
                }
            }
            out += "}";
        }

        return out;
    }

    public static void main(String[] args)
    {
        try
        {
            LoggingProperties props = new LoggingProperties(null, null,"resources/tibet.properties" );
            //java.io.File root = new java.io.File("");
            //System.out.println(root.getCanonicalPath());
            //props.load(new java.io.FileInputStream("resources/tibet.properties"));
            System.out.println(props);
            System.out.println(props.appenderData);
            System.out.println(props.layoutData);
            //System.out.println(props.loggers);

            Logger logger = Logger.getLogger("com.blackboard.services.installer");
            //logger = Logger.getLogger("com.blackboard.services.tibet");
            logger.debug("test DEBUG");
            logger.info("test INFO");
            logger.warn("test WARN");
            logger.error("test ERROR");
            logger.fatal("test FATAL");

            logger = Logger.getLogger("com.blackboard.services.installer.view.");
            logger.info("Testing rollover2");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
