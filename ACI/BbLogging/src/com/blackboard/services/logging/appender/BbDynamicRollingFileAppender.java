/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.logging.appender;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Layout;
import org.apache.log4j.RollingFileAppender;
import org.apache.log4j.spi.LoggingEvent;

/**
 * Overloaded version of Apache's RollingFileAppender.  Only difference is that the
 * logfile, upon calling activateOptions, will rollover the log files if Append is false, regardless
 * of the file size.
 * @see RollingFileAppender
 * @author crusnak
 */
public class BbDynamicRollingFileAppender extends RollingFileAppender
{
    private Map<String,FileAppender> appenderMap = new HashMap();
    private String property;
    
    public BbDynamicRollingFileAppender()
    {
        super();
    }

    public BbDynamicRollingFileAppender(Layout layout, String filename)
        throws IOException
    {
        super(layout, filename);
        activateOptions();
    }

    public BbDynamicRollingFileAppender(Layout layout, String filename, boolean append)
        throws IOException
    {
        super(layout, filename, append);
        activateOptions();
    }

    public String getSystemProperty()
    {
        return property;
    }
    
    public void setSystemProperty(String p)
    {
        property = p;
    }
    
    /**
     * Overriden to rollover the log files if Append is false.
     */
    @Override
    public void activateOptions()
    {
        if ( fileAppend == false )
        {
            /*try
            {
                FileInputStream in = new FileInputStream(super.fileName);
                System.out.println(in.available());
                if ( in.available() > 0 )
                {*/
                    rollOver();
                /*}
                in.close();
            }
            catch (IOException e) {e.printStackTrace();}*/
        }
        super.activateOptions();
    }

    @Override
    public void append(LoggingEvent event) 
    {
        //System.out.println(property + "=" + System.getProperty(property));
        String logfile = System.getProperty("log.file");
        FileAppender fa = null;
        if ( appenderMap.containsKey(logfile) )
        {
            fa = appenderMap.get(logfile);
        }
        else
        {
            try
            {
                File root = new File(fileName).getParentFile();
                File file = new File(root, logfile);
                System.out.println("Dynamic log file located at: " + file);
                fa = new BbRollingFileAppender(layout, file.getAbsolutePath(), fileAppend);
                appenderMap.put(logfile, fa);
            }
            catch (IOException e)
            {
                System.out.println("Unable to initialize BbRollingFileAppender with filename: " + logfile);
                e.printStackTrace(System.out);
                fa = this;
            }
        }
        
        fa.append(event); 
    }
    
    
}
