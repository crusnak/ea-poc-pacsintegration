/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.logging.appender;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.log4j.Layout;
import org.apache.log4j.RollingFileAppender;
import org.apache.log4j.spi.LoggingEvent;

/**
 * Overloaded version of Apache's RollingFileAppender.  Only difference is that the
 * logfile, upon calling activateOptions, will rollover the log files if Append is false, regardless
 * of the file size.
 * @see RollingFileAppender
 * @author crusnak
 */
public class BbRollingFileAppender extends RollingFileAppender
{

    public BbRollingFileAppender()
    {
        super();
    }

    public BbRollingFileAppender(Layout layout, String filename)
        throws IOException
    {
        super(layout, filename);
        activateOptions();
    }

    public BbRollingFileAppender(Layout layout, String filename, boolean append)
        throws IOException
    {
        super(layout, filename, append);
        activateOptions();
    }

    /**
     * Overriden to rollover the log files if Append is false.
     */
    @Override
    public void activateOptions()
    {
        if ( fileAppend == false )
        {
            /*try
            {
                FileInputStream in = new FileInputStream(super.fileName);
                System.out.println(in.available());
                if ( in.available() > 0 )
                {*/
                    rollOver();
                /*}
                in.close();
            }
            catch (IOException e) {e.printStackTrace();}*/
        }
        super.activateOptions();
    }

}
