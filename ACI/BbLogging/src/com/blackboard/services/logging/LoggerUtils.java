/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.logging;

import java.util.Enumeration;
import org.apache.log4j.Appender;
import org.apache.log4j.Category;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;

/**
 *
 * @author crusnak
 */
public class LoggerUtils
{
    public static void traverseAppenders(Logger logger)
    {
        if ( logger != null )
        {
            String name = logger.getName();
            System.out.println(name);
            Enumeration appenders = logger.getAllAppenders();
            while ( appenders.hasMoreElements() )
            {
                Appender appender = (Appender)appenders.nextElement();
                System.out.println("\t" + appender.getName());
                if ( appender instanceof FileAppender )
                {
                    System.out.println("\tOutput=" + ((FileAppender)appender).getFile());
                }
            }
            traverseAppenders(logger.getParent());
        }
    }

    private static void traverseAppenders(Category logger)
    {
        if ( logger != null )
        {
            String name = logger.getName();
            System.out.println(name);
            Enumeration appenders = logger.getAllAppenders();
            while ( appenders.hasMoreElements() )
            {
                Appender appender = (Appender)appenders.nextElement();
                System.out.println("\t" + appender.getName());
                if ( appender instanceof FileAppender )
                {
                    System.out.println("\tOutput=" + ((FileAppender)appender).getFile());
                }
            }
            traverseAppenders(logger.getParent());
        }
    }
}
