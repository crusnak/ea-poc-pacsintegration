/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.logging;

import java.util.HashMap;

/**
 *
 * @author crusnak
 */
public class PropertyFactory
{
    protected static enum Accessor { get, set };

    protected static String getAccessor(Accessor acc, String accessor)
    {
        String first = accessor.substring(0, 1);
        String remain = accessor.substring(1);
        return acc + first.toUpperCase() + remain;
    }

    protected static Class convertToPrimitiveClass(Class clazz)
    {
        Class pclass = clazz;
        if ( clazz == Integer.class ) pclass = Integer.TYPE;
        else if ( clazz == Boolean.class ) pclass = Boolean.TYPE;
        else if ( clazz == Long.class ) pclass = Long.TYPE;
        return pclass;
    }

}
