/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.logging;

/**
 *
 * @author crusnak
 */
public class DataType
{
    private Class classType;
    private Object defaultValue = null;
    private boolean required = true;
    //private Object value = null;

    public DataType() {}

    public DataType(Class type)
    {
        classType = type;
    }

    public DataType(Class type, Object def, boolean req)
    {
        classType = type;
        setDefaultValue(def);
        required = req;
    }

    public Class getClassType()
    {
        return classType;
    }

    public void setClassType(Class classType)
    {
        this.classType = classType;
    }

    public Object getDefaultValue()
    {
        return defaultValue;
    }

    public void setDefaultValue(Object dv)
    {
        if ( dv == null || dv.getClass().equals(classType) )
        {
            this.defaultValue = dv;
        }
        else
        {
            throw new IllegalArgumentException("Mismatched type: expecting " + classType.getName() +
                                               " but found " + dv.getClass().getName());
        }
    }

    public boolean isRequired()
    {
        return required;
    }

    public void setRequired(boolean required)
    {
        this.required = required;
    }

    /*public Object getValue()
    {
        Object val = value;
        if ( value == null && defaultValue != null )
        {
            val = getDefaultValue();
        }
        return val;
    }

    public void setValue(Object v)
    {
        if ( v instanceof String )
        {
            if ( classType == Boolean.class )
            {
                v = Boolean.valueOf((String)v);
            }
            else if ( classType == Integer.class )
            {
                v = Integer.valueOf((String)v);
            }
            else if ( classType == Long.class )
            {
                v = Long.valueOf((String)v);
            }
        }

        if ( v == null || classType.isInstance(v) )
        {
            this.value = v;
        }
        else
        {
            throw new IllegalArgumentException("Mismatched type: expecting " + classType.getName() +
                                               " but found " + v.getClass().getName());
        }
    }*/

    public String toString()
    {
        return "(" + classType.getName() + (required?"|REQUIRED": "") + ")";
    }

    public static void main(String[] args)
    {
        try
        {
            DataType dt = new DataType();
            dt.setClassType(Integer.class);
            dt.setDefaultValue(1);
            //dt.setValue((int)35);
            System.out.println(dt);
        }
        catch (Exception e) { e.printStackTrace(); }
    }
}
