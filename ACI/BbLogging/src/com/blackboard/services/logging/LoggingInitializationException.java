/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.logging;

/**
 *
 * @author crusnak
 */
public class LoggingInitializationException extends Exception
{
    private LoggingMetadata mdata = null;

    public LoggingInitializationException(LoggingMetadata md)
    {
        super();
        mdata = md;
    }

    public LoggingInitializationException(String msg, LoggingMetadata md)
    {
        super(msg);
        mdata = md;
    }

    public LoggingInitializationException(Throwable t, LoggingMetadata md)
    {
        super(t);
        mdata = md;
    }

    public LoggingInitializationException(String msg, Throwable t, LoggingMetadata md)
    {
        super(msg, t);
        mdata = md;
    }

    public LoggingMetadata getMetadata()
    {
        return mdata;
    }
}
