package com.blackboard.services.logging;

import java.util.List;
import java.util.Map;
import org.apache.log4j.Appender;
import org.apache.log4j.Layout;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggerRepository;

/**
 *
 * @author crusnak
 */
public class LogFactory
{
    private static LoggerRepository repository = Logger.getRootLogger().getLoggerRepository();

    public static void registerRootLogger(List<Appender> appenders)
    {
        registerRootLogger(appenders.toArray(new Appender[0]));
    }

    public static void registerRootLogger(Appender[] appenders)
    {
        Logger root = Logger.getRootLogger();
        root.removeAllAppenders();;

        for ( int i=0; i<appenders.length; i++ )
        {
            appenders[i].setName("root-Appender-" + i);
            root.addAppender(appenders[i]);
        }
    }

    public static Logger registerLogger(String name, List<Appender> appenders)
    {
        return registerLogger(name, appenders.toArray(new Appender[0]));
    }

    public static Logger registerLogger(String name, Appender[] appenders)
    {
        Logger log = repository.getLogger(name);
        log.removeAllAppenders();
        
        for ( int i=0; i<appenders.length; i++ )
        {
            appenders[i].setName(name + "-Appender-" + i);
            log.addAppender(appenders[i]);
        }
        
        return log;
    }

    public static void main(String[] args)
    {
        try
        {
            Layout l = LayoutFactory.createLayout(Layouts.SimpleLayout, (Map)null);
            Object[] props = { l };
            Appender a = AppenderFactory.createAppender(Appenders.ConsoleAppender, props );
            Object[] props2 = { l, "test.log" };
            Appender b = AppenderFactory.createAppender(Appenders.FileAppender, props2);
            //LogFactory lf = LogFactory.getInstance(ApplicationServerType.Tomcat);
            Appender[] apps = { a };
            LogFactory.registerLogger("console", apps);

            System.out.println("---------------------------------------------");
            java.util.Enumeration loggers = LogFactory.repository.getCurrentLoggers();
            while ( loggers.hasMoreElements() )
            {
                Logger log = (Logger)loggers.nextElement();
                System.out.println(log.getName() + "=" + log);
                java.util.Enumeration appenders = log.getAllAppenders();
                while ( appenders.hasMoreElements() )
                {
                    Appender app = (Appender)appenders.nextElement();
                    System.out.println("Appender: " + app.getName() + "=" + app);
                }
            }


            Appender[] apps2 = { AppenderFactory.createAppender(Appenders.ConsoleAppender, props), b };
            LogFactory.registerLogger("console", apps2);

            System.out.println("---------------------------------------------");
            loggers = LogFactory.repository.getCurrentLoggers();
            while ( loggers.hasMoreElements() )
            {
                Logger log = (Logger)loggers.nextElement();
                System.out.println(log.getName() + "=" + log);
                java.util.Enumeration appenders = log.getAllAppenders();
                while ( appenders.hasMoreElements() )
                {
                    Appender app = (Appender)appenders.nextElement();
                    System.out.println("Appender: " + app.getName() + "=" + app);
                }
            }

            LogFactory.repository.getLogger("console").setLevel(Level.INFO);
            LogFactory.repository.getLogger("console").info("Test INFO");

            Logger.getLogger("console").warn("Test - WARN");
        }
        catch (LoggingInitializationException e)
        {
            e.printStackTrace();
            System.out.println(e.getMetadata());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
