/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.logging;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Appender;
import org.apache.log4j.Layout;

/**
 *
 * @author crusnak
 */
public class AppenderFactory extends PropertyFactory
{
    private static HashMap<Appenders,LoggingMetadata> appenders = new HashMap();

    static
    {
        try
        {
            // Setup
            LoggingMetadata mdata = new LoggingMetadata(Appenders.FileAppender);
            mdata.setPropertyDefinition(AppenderOptions.layout, new DataType(Layout.class));
            mdata.setPropertyDefinition(AppenderOptions.File, new DataType(String.class));
            mdata.setPropertyDefinition(AppenderOptions.Append, new DataType(Boolean.class, true, false));
            mdata.setPropertyDefinition(AppenderOptions.BufferedIO, new DataType(Boolean.class, false, false));
            mdata.setPropertyDefinition(AppenderOptions.BufferSize, new DataType(Integer.class, null, false));
            appenders.put(Appenders.FileAppender, mdata);

            mdata = new LoggingMetadata(Appenders.RollingFileAppender);
            mdata.setPropertyDefinition(AppenderOptions.layout, new DataType(Layout.class));
            mdata.setPropertyDefinition(AppenderOptions.File, new DataType(String.class));
            mdata.setPropertyDefinition(AppenderOptions.Append, new DataType(Boolean.class, true, false));
            mdata.setPropertyDefinition(AppenderOptions.BufferedIO, new DataType(Boolean.class, false, false));
            mdata.setPropertyDefinition(AppenderOptions.BufferSize, new DataType(Integer.class, null, false));
            mdata.setPropertyDefinition(AppenderOptions.MaxBackupIndex, new DataType(Integer.class, 1, false));
            mdata.setPropertyDefinition(AppenderOptions.MaxFileSize, new DataType(String.class, "10MB", false));
            appenders.put(Appenders.RollingFileAppender, mdata);

            mdata = new LoggingMetadata(Appenders.DailyRollingFileAppender);
            mdata.setPropertyDefinition(AppenderOptions.layout, new DataType(Layout.class));
            mdata.setPropertyDefinition(AppenderOptions.File, new DataType(String.class));
            mdata.setPropertyDefinition(AppenderOptions.DatePattern, new DataType(String.class));
            mdata.setPropertyDefinition(AppenderOptions.Append, new DataType(Boolean.class, true, false));
            mdata.setPropertyDefinition(AppenderOptions.BufferedIO, new DataType(Boolean.class, false, false));
            mdata.setPropertyDefinition(AppenderOptions.BufferSize, new DataType(Integer.class, null, false));
            appenders.put(Appenders.DailyRollingFileAppender, mdata);

            mdata = new LoggingMetadata(Appenders.ConsoleAppender);
            mdata.setPropertyDefinition(AppenderOptions.layout, new DataType(Layout.class));
            mdata.setPropertyDefinition(AppenderOptions.Target, new DataType(String.class, "System.out", false));
            mdata.setPropertyDefinition(AppenderOptions.Follow, new DataType(Boolean.class, false, false));
            appenders.put(Appenders.ConsoleAppender, mdata);

            mdata = new LoggingMetadata(Appenders.BbRollingFileAppender);
            mdata.setPropertyDefinition(AppenderOptions.layout, new DataType(Layout.class));
            mdata.setPropertyDefinition(AppenderOptions.File, new DataType(String.class));
            mdata.setPropertyDefinition(AppenderOptions.Append, new DataType(Boolean.class, true, false));
            mdata.setPropertyDefinition(AppenderOptions.BufferedIO, new DataType(Boolean.class, false, false));
            mdata.setPropertyDefinition(AppenderOptions.BufferSize, new DataType(Integer.class, null, false));
            mdata.setPropertyDefinition(AppenderOptions.MaxBackupIndex, new DataType(Integer.class, 1, false));
            mdata.setPropertyDefinition(AppenderOptions.MaxFileSize, new DataType(String.class, "10MB", false));

            mdata = new LoggingMetadata(Appenders.BbDynamicRollingFileAppender);
            mdata.setPropertyDefinition(AppenderOptions.layout, new DataType(Layout.class));
            mdata.setPropertyDefinition(AppenderOptions.File, new DataType(String.class));
            mdata.setPropertyDefinition(AppenderOptions.Append, new DataType(Boolean.class, true, false));
            mdata.setPropertyDefinition(AppenderOptions.SystemProperty, new DataType(String.class, true, false));
            mdata.setPropertyDefinition(AppenderOptions.BufferedIO, new DataType(Boolean.class, false, false));
            mdata.setPropertyDefinition(AppenderOptions.BufferSize, new DataType(Integer.class, null, false));
            mdata.setPropertyDefinition(AppenderOptions.MaxBackupIndex, new DataType(Integer.class, 1, false));
            mdata.setPropertyDefinition(AppenderOptions.MaxFileSize, new DataType(String.class, "10MB", false));
            appenders.put(Appenders.BbRollingFileAppender, mdata);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Unable to initialize AppenderFactory", e);
        }
    }

    public static LoggingMetadata getAppenderMetadata(Appenders appender)
    {
        return new LoggingMetadata(appenders.get(appender));
    }

    public static Appender createAppender(Appenders type, Object[] props)
        throws LoggingInitializationException
    {
        LoggingMetadata mdata = getAppenderMetadata(type);
        LinkedHashMap values = new LinkedHashMap();
        String[] names = mdata.getPropertyNames().toArray(new String[0]);
        for ( int i=0; i<props.length; i++ )
        {
            if ( i < names.length )
            {
                values.put(names[i], props[i]);
            }
        }
        mdata.setPropertyValues(values.entrySet());
        return createAppender(type, mdata);
    }

    public static Appender createAppender(Appenders type, Map props)
        throws LoggingInitializationException
    {
        LoggingMetadata mdata = getAppenderMetadata(type);
        mdata.setPropertyValues(props.entrySet());
        return createAppender(type, mdata);
    }

    public static Appender createAppender(Appenders type, LoggingMetadata mdata)
        throws LoggingInitializationException
    {
        Appender appender = null;
        Class clazz = mdata.getClassDefinition();
        System.out.println("Creating appender of " + clazz);
        System.out.println(mdata.getPropertyDefinitions());

        try
        {
            // Ensure that the underlying class is assignable as the Layout class
            if ( !Appender.class.isAssignableFrom(clazz) )
            {
                throw new LoggingInitializationException("Invalid Appender: found " + clazz, mdata);
            }

            Object obj = clazz.newInstance();
            for ( Map.Entry<String,DataType> entry: mdata.getPropertyDefinitions() )
            {
                String accessor = getAccessor(Accessor.set, entry.getKey());
                DataType dt = entry.getValue();
                Object value = mdata.getPropertyValue(entry.getKey());
                if ( dt.isRequired() || value != null )
                {
                    System.out.println(accessor);
                    Class[] ptypes = { convertToPrimitiveClass(dt.getClassType()) };
                    Method method = clazz.getMethod(accessor, ptypes);
                    System.out.println(method);
                    method.invoke(obj, value);
                    //accessor = getAccessor(Accessor.get, entry.getKey());
                    //method = clazz.getMethod(accessor, null);
                    //System.out.println(clazz.getSimpleName() + "." + accessor + "=" + method.invoke(obj, null));
                }
            }

            Method method = clazz.getMethod("activateOptions", (Class[])null);
            method.invoke(obj, (Object[])null);
            appender = (Appender)obj;

        }
        catch (IllegalAccessException e) { throw new LoggingInitializationException(e, mdata); }
        catch (InstantiationException e) { throw new LoggingInitializationException(e, mdata); }
        catch (InvocationTargetException e) { throw new LoggingInitializationException(e, mdata); }
        catch (NoSuchMethodException e) { throw new LoggingInitializationException(e, mdata); }

        return appender;
    }

    public static void main(String[] args)
    {
        try
        {
            Class.forName("com.blackboard.services.logging.AppenderFactory");
            System.out.println(AppenderFactory.appenders);

            LoggingMetadata mdata = AppenderFactory.getAppenderMetadata(Appenders.FileAppender);
            System.out.println(mdata);
            System.out.println(mdata.getRequiredProperties());

            Object[] props = { new org.apache.log4j.SimpleLayout(), "test.log", false };
            mdata.setPropertyValues(props);
            System.out.println(mdata);

            Appender l = AppenderFactory.createAppender(Appenders.RollingFileAppender, mdata);
            System.out.println(l);

            java.util.HashMap map = new java.util.HashMap();
            map.put("layout", new org.apache.log4j.PatternLayout());
            map.put(AppenderOptions.Follow, true);
            l = AppenderFactory.createAppender(Appenders.ConsoleAppender, map);
            System.out.println(l);

        }
        catch (Exception e) { e.printStackTrace(); }
    }
}
