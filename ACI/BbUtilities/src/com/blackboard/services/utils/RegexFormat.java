/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.utils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author crusnak
 */
public class RegexFormat implements TextFormat
{
    ArrayList<Field> fields = new ArrayList();
    Pattern pattern = null;

    public RegexFormat(String p)
    {
        pattern = Pattern.compile(p);
    }

    public void setFields(List<Field> f)
    {
        fields = new ArrayList(f);
    }

    public void addFields(List<Field> f)
    {
        fields.addAll(f);
    }

    public void addField(Field f)
    {
        fields.add(f);
    }

    public void addField(String name, String dt)
    {
        fields.add(new Field(name, dt));
    }

    public Field getFieldByName(String name)
    {
        Field field = null;
        for ( Field f: fields )
        {
            if ( f.getName().equals(name) )
            {
                field = f;
                break;
            }
        }
        return field;
    }

    public void resetFields()
    {
        fields.clear();
    }

    public boolean removeField(Field f)
    {
        return fields.remove(f);
    }

    public List<Field> getFields()
    {
        return fields;
    }

    public List<Field> getSimpleFields()
    {
        return fields;
    }

    public String[] readHeader(String line)
    {
        throw new UnsupportedOperationException("Headers aren't supported in regex file formats");
    }

    public Map<String, String> processLine(String line)
        throws TextFormatException
    {
        LinkedHashMap<String,String> map = new LinkedHashMap();

        try
        {
            if ( line != null && line.trim().length() > 0 )
            {
                Matcher m = pattern.matcher(line);
                if ( m.matches() )
                {
                    for ( int i=0; i<fields.size(); i++ )
                    {
                        Field f = fields.get(i);
                        if ( i < m.groupCount() )
                        {
                            String val = m.group(i+1).trim();
                            map.put(f.getName(), val);
                        }
                        else
                        {
                            map.put(f.getName(), "");
                        }
                    }
                }
                else
                {
                    throw new IllegalArgumentException("Incoming line does not match regex pattern: " + pattern);
                }
            }
        }
        catch (IllegalArgumentException e)
        {
            throw new TextFormatException(this, "Unable to process line because: " + e.getMessage(), e);
        }
        catch (Exception e)
        {
            throw new TextFormatException(this, "Unable to process line: " + line, e);
        }

        return map;
    }

    public String formatLine(List<String> values)
    {
        throw new UnsupportedOperationException("Cannot format output to a regex pattern.");
    }

    public String formatLine(Map<String, String> values)
    {
        throw new UnsupportedOperationException("Cannot format output to a regex pattern.");
    }

    public String formatLine(String[] values)
    {
        throw new UnsupportedOperationException("Cannot format output to a regex pattern.");
    }

    @Override
    public String toString()
    {
        return pattern + " " + String.valueOf(fields);
    }

    public static class Field implements TextFormat.Field
    {
        private String name  = null;
        private String dataType = null;

        public Field(String n, String dt)
        {
            name = n;
            dataType = dt;
        }

        public String getName()
        {
            return name;
        }

        public void setName(String n)
        {
            name = n;
        }
        
        public boolean isConstant()
        {
            return false;
        }
        
        public String getDataType()
        {
            return dataType;
        }
        
        public void setDataType(String dt)
        {
            dataType = dt;
        }

        public String toString()
        {
            return name + "[" + dataType + "]";
        }
    }
    
    public static void main(String[] args)
    {
        System.out.println("Test");
        try
        {
            String input = "CS";
            Pattern p = Pattern.compile("BK|CS|GA|GR|HE|HH|IH|SC|UA|US");
            Matcher m = p.matcher(input);
            System.out.println(input);
            if ( m.matches() )
            {
                //System.out.println("'" + m.group(1) + "'");
                System.out.println("Matches");
            }
            else
            {
                System.out.println("No match");
            }
            
            ArrayList<String> list = new ArrayList(java.util.Arrays.asList("3;77".split(";")));
            System.out.println(list);
            
            input = "Hokoda, Emma 00001186706";
            p = Pattern.compile("^\\s*(\\w+,\\s*\\w+)\\s*([0-9]+)\\s*$");
            m = p.matcher(input);
            
            if ( m.matches() )
            {
                System.out.println("'" + m.group(1) + "'");
                System.out.println("'" + m.group(2) + "'");
            }
            else
            {
                System.out.println("No match");
            }
            
            java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            java.util.Date date = fmt.parse("2018-04-23 06:56:12");
            System.out.println(date);
            
            input = "601067009577941";
            p = Pattern.compile("^[1-9][0-9]{14}$");
            m = p.matcher(input);
            
            if ( m.matches() )
            {
                System.out.println(input + "=Matches");
            }
            else
            {
                System.out.println(input + "=No match");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}