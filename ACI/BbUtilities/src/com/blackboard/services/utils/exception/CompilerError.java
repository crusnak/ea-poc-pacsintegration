/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.utils.exception;

/**
 *
 * @author crusnak
 */
public class CompilerError
{
    String message;

    public CompilerError(String msg)
    {
        message = msg;
    }

    public String getMessage()
    {
        return message;
    }

    @Override
    public String toString()
    {
        return "\n" + message;
    }
}
