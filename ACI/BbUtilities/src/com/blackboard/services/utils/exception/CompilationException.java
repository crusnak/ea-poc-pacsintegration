/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.utils.exception;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author crusnak
 */
public class CompilationException extends Exception
{
    private ArrayList<CompilerError> errors = new ArrayList<CompilerError>();

    public CompilationException()
    {
        super();
    }

    public CompilationException(String message)
    {
        super(message);
    }

    public CompilationException(Throwable cause)
    {
        super(cause);
    }

    public CompilationException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public List<CompilerError> getErrors()
    {
        return errors;
    }

    public void addError(String error)
    {
        errors.add(new CompilerError(error));
    }

    public void addErrors(List<CompilerError> moreErrors)
    {
        errors.addAll(moreErrors);
    }

    @Override
    public String toString()
    {
        String s = getClass().getName();
        String message = getLocalizedMessage();
        List warnings = getErrors();
        return (message != null) ? (s + ": " + message + "\n" + warnings) : s + "\n" + warnings;
    }
}
