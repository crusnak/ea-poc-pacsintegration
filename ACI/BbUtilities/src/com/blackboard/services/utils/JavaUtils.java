/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.utils;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import org.apache.http.NameValuePair;

/**
 *
 * @author crusnak
 */
public class JavaUtils
{
    private static enum Test { a, b, c };

    public static <E extends Enum<E>> List<E> enumToList(Class<E> enumClass)
    {
        ArrayList<E> list = new ArrayList();
        EnumSet<E> set = EnumSet.allOf(enumClass);
        for ( E e: set )
        {
            list.add(e);
        }

        return list;
    }

    public static <E extends Enum<E>> List<E> enumToList(EnumSet<E> eset)
    {
        ArrayList<E> list = new ArrayList();
        for ( E e: eset )
        {
            list.add(e);
        }
        
        return list;
    }

    public static <E extends Enum<E>> List<String> enumToStringList(Class<E> enumClass)
    {
        ArrayList<String> list = new ArrayList();
        EnumSet<E> set = EnumSet.allOf(enumClass);
        for ( E e: set )
        {
            list.add(String.valueOf(e));
        }
        return list;
    }

    public static <E extends Enum<E>> E getEnum(Class<E> enumeration, String enumString)
    {
        E e = null;
        if ( enumString != null )
        {
            e = Enum.valueOf(enumeration, enumString);
        }
        return e;
    }

    public static String classFieldsToHashString(Object o)
    {
        String str = null;
        if ( o != null )
        {
            StringBuffer out = new StringBuffer();
            out.append("{");
            Class c = o.getClass();
            while ( c != null )
            {
                Field[] fields = c.getDeclaredFields();
                for ( int i=0; i<fields.length; i++ )
                {
                    Field field = fields[i];
                    try
                    {
                        field.setAccessible(true);
                        out.append(field.getName() + "=" + field.get(o));
                        if ( i < fields.length-1 )
                        {
                            out.append(", ");
                        }
                    }
                    catch (IllegalAccessException e)
                    {
                        e.printStackTrace();
                    }
                }
                
                c = c.getSuperclass();
            }
            out.append("}");
            str = out.toString();
        }

        return str;
    }
    
    public static String percentEncode(String s, String enc)
        throws UnsupportedEncodingException
    {
        return percentEncode(s, enc, true);
    }
    
    public static String percentEncode(String s, String enc, boolean decode)
        throws UnsupportedEncodingException
    {
        if ( s == null )
        {
            return null;
        }
        
        String decoded = s;
        if ( decode )
        {
            decoded = URLDecoder.decode(s, enc);
        }
        
        String encoded = URLEncoder.encode(decoded, enc);
        
        String output = "";
        for ( char c: encoded.toCharArray() )
        {
            if ( c == '+' )
            {
                output += "%20";
            }
            else
            {
                output += c;
            }
        }
        
        return output;
    }

    public static void main(String[] args)
    {
        try
        {
            String d = "783325.32";
            String i = d.substring(0, d.indexOf("."));
            System.out.println("Double: " + d);
            System.out.println("Int: " + i);
            
            String query = "a2=r%20b";

            System.out.println(percentEncode(query, "UTF-8"));
            
            /*java.io.ByteArrayInputStream bais = new java.io.ByteArrayInputStream(new java.io.FileInputStream("output\\800669330.jpg").readAllBytes());
            java.awt.image.BufferedImage bimg = javax.imageio.ImageIO.read(bais);
            
            int w = bimg.getWidth();
            int h = bimg.getHeight();
            int maxw = 1024;
            int maxh = 1024;
            int maxd = Math.max(w, h);
            
            System.out.println("Width=" + w + ", height=" + h);
            System.out.println("Largest dimension=" + maxd);
            
            double mult = (double)maxh / (double)maxd;
            System.out.println("Multiplier=" + mult);
            int uw = (int)(w * mult);
            int uh = (int)(h * mult);
            System.out.println("New width=" + uw + ", new height=" + uh);
            
            java.awt.image.BufferedImage scaled = new java.awt.image.BufferedImage(uw, uh, java.awt.image.BufferedImage.TYPE_INT_ARGB);
            java.awt.geom.AffineTransform at = java.awt.geom.AffineTransform.getScaleInstance(mult, mult);
            java.awt.image.AffineTransformOp ato = new java.awt.image.AffineTransformOp(at, java.awt.image.AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
            scaled = ato.filter(bimg, scaled);
            System.out.println(ato);
            System.out.println(scaled);
            
            javax.imageio.ImageIO.write(scaled, "PNG", new java.io.FileOutputStream("output\\800669330.png"));
            */
            
            /*DelimittedFormat format = new DelimittedFormat(',', false);
            format.addField("A", 30, false, false);
            format.addField("B", 22, false, true);
            System.out.println(JavaUtils.classFieldsToHashString(format));*/

            /*Test t = getEnum(Test.class, "b");
            System.out.println(t);
            List<Test> list = enumToList(Test.class);
            System.out.println(list);*/
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
}
