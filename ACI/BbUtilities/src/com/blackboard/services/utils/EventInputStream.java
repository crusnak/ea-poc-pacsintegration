/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.utils;

import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author crusnak
 */
public class EventInputStream extends InputStream
{
    public static enum Events { open, read, close, mark, reset, skip };
    
    InputStream input = null;
    StreamEventHandler handler = null;
    
    public EventInputStream(InputStream is)
        throws IllegalArgumentException
    {
        input = is;
        if ( input == null )
        {
            throw new IllegalArgumentException("Cannot create a event input stream with a null underlying stream.");
        }
    }

    public EventInputStream(InputStream is, StreamEventHandler h)
        throws IllegalArgumentException
    {
        this(is);
        handler = h;
        
        if ( handler != null )
        {
            Object[] args = { input };
            handler.processEvent(Events.open, args);
        }
    }

    public StreamEventHandler getEventHandler()
    {
        return handler;
    }
    
    public InputStream getUnderlyingStream()
    {
        return input;
    }
    
    @Override
    public int read()
            throws IOException
    {
        int read = input.read();
        if ( handler != null )
        {
            Object[] args = { read };
            handler.processEvent(Events.read, args);
        }
        
        return read;
    }

    @Override
    public int hashCode()
    {
        return input.hashCode();
    }

    @Override
    public synchronized void mark(int readlimit)
    {
        input.mark(readlimit);
        if ( handler != null )
        {
            Object[] args = { readlimit };
            handler.processEvent(Events.mark, args);
        }
    }

    @Override
    public boolean markSupported()
    {
        return input.markSupported();
    }

    @Override
    public synchronized void reset()
            throws IOException
    {
        input.reset();
        if ( handler != null )
        {
            Object[] args = { };
            handler.processEvent(Events.reset, args);
        }
    }

    @Override
    public long skip(long n)
            throws IOException
    {
        long l = input.skip(n);
        if ( handler != null )
        {
            Object[] args = { n };
            handler.processEvent(Events.skip, args);
        }
        
        return l;
    }

    @Override
    public int available()
            throws IOException
    {
        return input.available();
    }

    @Override
    public boolean equals(Object obj)
    {
        return input.equals(obj);
    }

    @Override
    public void close()
            throws IOException
    {
        input.close();
        if ( handler != null )
        {
            Object[] args = { };
            handler.processEvent(Events.close, args);
        }
    }
}
