/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.utils;

import java.io.Serializable;
import org.apache.http.Header;
import org.apache.http.message.BasicHeader;

/**
 *
 * @author crusnak
 */
public class ComparableHeader extends BasicHeader implements Header, Serializable, Cloneable, Comparable<Object> 
{
    private static final long serialVersionUID = 8904376843268364L;
    
    private boolean compareValues = false;
    
    public ComparableHeader()
    {
        super(null, null);
    }
    
    public ComparableHeader(String name, String value)
    {
        this(name, value, false);
    }

    public ComparableHeader(String name, String value, boolean compare)
    {
        super(name, value);
        this.compareValues = compare;
    }

    public int compareTo(Object o) 
    {
        int compare = 0;
        
        if ( compareValues )
        {
            compare = String.valueOf(o).compareTo(String.valueOf(this));
        }
        else
        {
            if ( o instanceof Header )
            {
                Header h = (Header)o;
                compare = String.valueOf(h.getName()).compareTo(String.valueOf(this.getName()));
            }
        }
        
        return compare;
    }

    @Override
    public boolean equals(Object obj) 
    {
        boolean equals = false;
        
        if ( compareValues )
        {
            equals = String.valueOf(obj).equals(String.valueOf(this));
        }
        else
        {
            if ( obj instanceof Header )
            {
                Header h = (Header)obj;
                equals = String.valueOf(h.getName()).equals(String.valueOf(getName()));
            }
        }

        return equals;
    }
    
    
    
    public static void main(String[] args)
    {
        try
        {
            java.util.HashSet<Header> headers = new java.util.HashSet<Header>();
            headers.add(new ComparableHeader("header1", "value1"));
            headers.add(new ComparableHeader("header2", "value2"));
            
            Header h1 = new ComparableHeader("header2", null);
            System.out.println(headers.contains(h1));
        }
        catch (Exception e )
        {
            e.printStackTrace();
        }
    }
    
}


