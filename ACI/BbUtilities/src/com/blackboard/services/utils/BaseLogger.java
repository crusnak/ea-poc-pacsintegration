/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.utils;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author crusnak
 */
public class BaseLogger
{
    private static Level logLevel = Level.ALL;

    public static void log(Level level, String message)
    {
        log(level, message, null);
    }

    public static void log(Level level, String message, Throwable t)
    {
        Logger logger = Logger.getRootLogger();
        if ( logger.getAllAppenders().hasMoreElements() )
        {
            logger.log(level, message, t);
        }
        else
        {
            if ( level.isGreaterOrEqual(logLevel) )
            {
                if ( level == Level.ERROR || level == Level.FATAL || level == Level.WARN )
                {
                    System.err.println(level + " - " + message);
                    if ( t != null )
                    {
                        t.printStackTrace(System.err);
                    }
                }
                else
                {
                    System.out.println(level + " - " + message);
                    if ( t != null )
                    {
                        t.printStackTrace(System.out);
                    }
                }
            }
        }
    }

    public static Level getLogLevel()
    {
        return logLevel;
    }

    public static void setLogLevel(Level level)
    {
        logLevel = level;
    }

    public static void trace(String msg)
    {
        log(Level.TRACE, msg);
    }

    public static void trace(String msg, Throwable t)
    {
        log(Level.TRACE, msg, t);
    }

    public static void debug(String msg)
    {
        log(Level.DEBUG, msg);
    }

    public static void debug(String msg, Throwable t)
    {
        log(Level.DEBUG, msg, t);
    }

    public static void info(String msg)
    {
        log(Level.INFO, msg);
    }

    public static void info(String msg, Throwable t)
    {
        log(Level.INFO, msg, t);
    }

    public static void warn(String msg)
    {
        log(Level.WARN, msg);
    }

    public static void warn(String msg, Throwable t)
    {
        log(Level.WARN, msg, t);
    }

    public static void error(String msg)
    {
        log(Level.ERROR, msg);
    }

    public static void error(String msg, Throwable t)
    {
        log(Level.ERROR, msg, t);
    }

    public static void fatal(String msg)
    {
        log(Level.FATAL, msg);
    }

    public static void fatal(String msg, Throwable t)
    {
        log(Level.FATAL, msg, t);
    }

    public static void main(String[] args)
    {
        debug("TestD");
        info("TestI", new Exception("Error1"));
        warn("TestW");
        error("TestE", new Exception("Error2"));
    }
}
