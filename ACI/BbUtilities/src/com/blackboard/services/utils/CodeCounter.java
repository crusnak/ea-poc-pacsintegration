/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.utils;

import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipFile;

/**
 *
 * @author crusnak
 */
public class CodeCounter
{
    public static List<File> getFilteredFiles(File root, String f)
        throws IOException
    {
        ArrayList<File> files = new ArrayList();
        CodeFilter filter = new CodeFilter(f);
        if ( root.exists() && root.isDirectory() )
        {
            File[] cfiles = root.listFiles(filter);
            for ( File file: cfiles )
            {
                files.add(file);
            }

            File[] afiles = root.listFiles();
            for ( File file: afiles )
            {
                if ( file.isDirectory() )
                {
                    files.addAll(getFilteredFiles(file, f));
                }
            }
        }

        return files;
    }

    public static List<String> getFilteredFileNames(File root, String f)
        throws IOException
    {
        ArrayList<String> classes = new ArrayList();
        if ( root.exists() && root.isDirectory() )
        {
            List<File> files = getFilteredFiles(root, f);
            for ( File file: files )
            {
                classes.add(getRelativeFileName(root, file));

            }
        }
        else if ( root.exists() && root.isFile() )
        {
            ZipFile zip = new ZipFile(root);
            List<String> files = FileUtils.extractFileNamesFromArchive(zip);
            System.out.println(files);
            for ( String name: files )
            {
                if ( name.contains(f) )
                {
                    classes.add(name);
                }
            }
        }

        return classes;
    }

    public static String getRelativeFileName(File root, File cfile)
    {
        String name = "";
        System.out.println(cfile);

        if ( cfile != null )
        {
            name = cfile.getName();
            File current = cfile.getParentFile();
            while ( !root.equals(current) )
            {
                name = current.getName() + "/" + name;
                current = current.getParentFile();
            }
        }

        return name;
    }

    public static int count(String filter, String[] sourceDirs)
        throws IOException
    {
        int total = 0;
        for (String source: sourceDirs )
        {
            System.out.println("--------------------------------------------------");
            int subtotal = 0;
            File dir = new File(source);
            System.out.println("Processing code with filter <" + filter + ">: " + dir.getCanonicalPath());
            if ( !dir.exists() )
            {
                System.err.println("Source dir doesn't exist...ignoring.");
            }
            else if ( !dir.isDirectory() )
            {
                System.err.println("Source dir is a file, not a directory...ignoring.");
            }
            else
            {
                File[] allFiles = dir.listFiles();
                for ( File file: allFiles )
                {
                    if ( file.isDirectory() )
                    {
                        String[] childDir = { file.getAbsolutePath() };
                        subtotal += count(filter, childDir);
                    }
                }

                CodeFilter codeFilter = new CodeFilter(filter);
                File[] codeFiles = dir.listFiles(codeFilter);
                for ( File file: codeFiles )
                {
                    System.out.println("------------------------------------------");
                    System.out.println("Counting code: " + file.getCanonicalPath());
                    int counted = countCode(file);
                    System.out.println("Total number of lines for " + file.getName() + "=" + counted);
                    subtotal += counted;
                }

            }

            System.out.println("Total lines of code for " + dir.getName() + ": " + subtotal);
            total += subtotal;
        }

        return total;
    }

    private static int countCode(File file)
        throws IOException
    {
        int total = 0;
        LineNumberReader reader = new LineNumberReader(new FileReader(file));
        String line = "";
        while ( (line = reader.readLine()) != null )
        {
            total++;
        }

        return total;
    }


    private static class CodeFilter implements FilenameFilter
    {
        private String[] filters = new String[0];
        public CodeFilter(String f)
        {
            filters = f.split(",");
        }

        public boolean accept(File dir, String name)
        {
            //System.out.println("testing: " + dir + File.separator + name);
            boolean valid = false;
            for ( String filter: filters )
            {
                valid |= name.contains(filter);
            }
            return valid;
        }

    }

    public static void main(String[] args)
    {
        try
        {
            if ( args.length < 2 )
            {
                throw new IllegalArgumentException("Usage: CodeCount <comma-separated file_filters> <source_root_directory1> ... <source_root_directoryN>");
            }

            String[] dirs = new String[args.length-1];
            System.arraycopy(args, 1, dirs, 0, args.length-1);
            int total = CodeCounter.count(args[0], dirs);
            System.out.println("Total lines of code: " + total);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
