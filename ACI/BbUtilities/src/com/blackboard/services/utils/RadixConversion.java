/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.utils;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Locale;

/**
 *
 * @author crusnak
 */
public class RadixConversion
{
    public static byte[] convertHexStringToBytes(String hex)
    {
        // Ensure multiple of 2
        if ( hex.length() % 2 == 1 )
        {
            hex = "0" + hex;
        }

        byte[] bytes = new byte[hex.length() / 2];
        for ( int i=0; i<bytes.length; i++ )
        {
            String h = "0x" + hex.substring(i*2, i*2+2);
            bytes[i] = Integer.decode(h).byteValue();
        }

        return bytes;
    }

    public static byte convertHexCharToByte(char h)
    {
        // A single hex char is 4 bits so add 4 more 0 bits and return the single byte
        byte[] bytes = convertHexStringToBytes("0" + h);
        return bytes[0];
    }

    public static String byteToHexString(byte b)
    {
        String hex = Integer.toHexString(b);

        // Since using Integer.toHexString on a byte, 4 bytes are returned ( 8 hex )
        // Prepend 0 in front ( a byte represented as an integer
        // Negative bytes are represented as 32 bit two's complement ( i.e. 24 most significant
        // bits are F)
        while ( hex.length() < 8 )
        {
            hex = "0" + hex;
        }

        return hex.substring(6);
    }

    public static String bytesToHexString(byte[] bytes)
    {
        return bytesToHexString(bytes, true);
    }

    public static String bytesToHexString(byte[] bytes, boolean space)
    {
        String hex = "";
        if ( bytes != null )
        {
            for ( byte b: bytes )
            {
                hex += byteToHexString(b);
                if ( space ) hex += " ";
            }
        }

        return hex.trim();
    }

    public static final byte[] intToBytes(int value) 
    {
        return new byte[] {
                    (byte)(value >>> 24),
                    (byte)(value >>> 16),
                    (byte)(value >>> 8),
                    (byte)value};
    }

    public static String bytesToIntString(byte[] bytes)
    {
        return bytesToIntString(bytes, true);
    }

    public static String bytesToIntString(byte[] bytes, boolean space)
    {
        String ints = "";
        if ( bytes != null )
        {
            for ( byte b: bytes )
            {
                ints += String.valueOf((int)b);
                if ( space ) ints += " ";
            }
        }

        return ints.trim();
    }    

    public static boolean compareBytes(byte[] b1, byte[] b2)
    {
        boolean match = false;
        if ( b1.length == b2.length )
        {
            match = true;
            for ( int i=0; i<b1.length; i++ )
            {
                match &= b1[i] == b2[i];
            }
        }

        return match;
    }

    public static char[] bytesToChars(byte[] b)
    {
        char[] chars = new char[b.length];
        for ( int i=0; i<b.length; i++ )
        {
            chars[i] = (char)(b[i] & 0xFF);
        }
        return chars;
    }
    
    public static char byteToChar(byte b)
    {
        return (char)(b & 0xFF);
    }

    public static byte[] charToBytes(char c)
    {
        byte[] bytes = new byte[2];
        bytes[1] = (byte)c;
        bytes[0] = (byte)(c >> 8);
        return bytes;
    }

    public static byte[] charsToBytes(char[] chars)
    {
        ArrayList<byte[]> temp = new ArrayList();
        ArrayList<Byte> bytelist = new ArrayList();
        for ( char c: chars )
        {
            temp.add(charToBytes(c));
        }

        for ( byte[] ba: temp )
        {
            if ( ba[0] == 0 )
            {
                bytelist.add(ba[1]);
            }
            else
            {
                bytelist.add(ba[0]);
                bytelist.add(ba[1]);
            }
        }

        byte[] bytes = new byte[bytelist.size()];
        for ( int i=0; i<bytelist.size(); i++ )
        {
            bytes[i] = bytelist.get(i);
        }

        return bytes;
    }
    
    public static String bytesToString(byte[] b)
    {
        return new String(bytesToChars(b));
    }
    
    public static String bytesToString(byte[] b, Charset cs)
    {
        return new String(b, cs);
    }
    
    public static byte[] stringToBytes(String msg)
    {
        byte[] b = new byte[0];
        if ( msg != null )
        {
            b = RadixConversion.charsToBytes(msg.toCharArray());
        }
        return b;
    }

    /*public static int compareBytes(byte[] a, byte[] b)
    {
        int comp = 0;
        if ( a.length != b.length )
        {
            comp = ( a.length > b.length ? 1 : -1 );
        }
        else
        {
            for ( int i=0; i<a.length&&i<b.length&&comp==0; i++ )
            {
                comp = new Byte(a[i]).compareTo(new Byte(b[i]));
            }
        }

        return comp;
    }*/

    public static void main(String[] args)
    {
        char cr = '\n';
        char lf = '\r';
        
        System.out.println(RadixConversion.bytesToHexString(RadixConversion.charsToBytes(System.getProperty("line.separator").toCharArray())));
        
        System.out.println(cr + "=" + RadixConversion.bytesToHexString(RadixConversion.charToBytes(cr)));
        System.out.println(lf + "=" + RadixConversion.bytesToHexString(RadixConversion.charToBytes(lf)));
        
        System.exit(0);
        
        //byte[] empty = { (byte)0x0000 };
        String key = "4c7d415a764b2d7d5c51724408491a11";// + new String(empty);
        System.out.println("KEY=" + key);
        byte[] bytes = key.getBytes();
        byte[] bytes2 = convertHexStringToBytes(key);
        System.out.println("BYTES1=" + bytesToHexString(bytes));
        byte[] bytes3 = convertHexStringToBytes(new String(bytes));
        System.out.println("BYTES2=" + bytesToHexString(bytes2, false));
        System.out.println("BYTES3=" + bytesToHexString(bytes3, false));
        System.out.println(bytesToHexString(bytes, false) + "=" + bytesToHexString(bytes2, false));
        System.out.println("BYTES1=BYTES2: " + compareBytes(bytes, bytes2));
        System.out.println("BYTES2=BYTES3: " + compareBytes(bytes2, bytes3));
        
        int i = 2;
        String hex = bytesToHexString(RadixConversion.intToBytes(i), false);
        System.out.println(i);
        System.out.println(hex);
        
        System.out.println(convertHexCharToByte('2'));
    }
}
