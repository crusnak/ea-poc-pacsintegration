/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.utils;

/**
 *
 * @author crusnak
 */
public class TextFormatException extends Exception
{
    private TextFormat format = null;

    public TextFormatException(TextFormat tf)
    {
        format = tf;
    }

    public TextFormatException(TextFormat tf, String msg)
    {
        super(msg);
        format = tf;
    }

    public TextFormatException(TextFormat tf, Throwable t)
    {
        super(t);
        format = tf;
    }

    public TextFormatException(TextFormat tf, String msg, Throwable t)
    {
        super(msg, t);
        format = tf;
    }

    public TextFormat getTextFormat()
    {
        return format;
    }

    @Override
    public String getMessage()
    {
        return this.toString();
    }

    @Override
    public String toString()
    {
        String s = getClass().getName();
        String message = super.getMessage();
        String error = (format != null) ? "TextFormat error on: " + format : "";
        return (message != null) ? (s + ": " + message + ": " + error) : s + ": " + error;
    }
}
