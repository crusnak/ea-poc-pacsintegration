/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.utils;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

/**
 * This interface is used to define the capabilities of JSON objects.  It includes support
 * for error handling as well as converting the base JSON object into XML (via JAXB).
 * @author crusnak
 */
public interface JSON
{
    /**
     * Converts the JSON object into a String representation: <tt>{var1=val1, var2=val2}</tt>
     * @return The JSON string
     */
    public String toJSONString();
    
    /**
     * Adds a new Array of parameter values to this JSONObject.
     * @param key The name of the parameter
     * @param jsons An array of JSON objects
     * @return The replaced value for this parameter or <tt>null</tt> if not previously assigned.
     */
    public Object put(Object key, JSON[] jsons);
    
    /**
     * Adds a new key/value pair or overwrites the existing value.
     * This is overloaded to check if the value is an enumeration value or not. If so, the enumeration value is stored as a string so
     * external JSON parsers can properly handle those values.
     * @param key The name of the parameter
     * @param val The value of the parameter
     * @return The replaced value for this parameter or <tt>null</tt> if not previously assigned.
     */
    public Object put(Object key, Object val);

    /**
     * Converts the JSON string to a JSONObject with the parameters defined in the
     * string.  If the JSON string is not a JSONObject an exception will be thrown.
     * @param json The JSON string to parse
     * @throws IllegalArgumentException Thrown if the incoming string cannot be parsed into a JSON construct,
     * or the JSON string represents a JSONArray or JSONValue.
     */
    public void setJSONString(String json) throws IllegalArgumentException;
    
}
