/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.utils;

/**
 *
 * @author crusnak
 */
public abstract class StreamEventHandler
{
    public abstract void processEvent(Enum event, Object[] args);
}
