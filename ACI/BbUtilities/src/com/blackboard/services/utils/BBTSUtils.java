/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.utils;

import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author crusnak
 */
public class BBTSUtils
{
    private static final Date adjustedEpoch = new Date(0 - Calendar.getInstance().getTimeZone().getOffset(0));
    private static final long daysToEpoch = 25569L;
    private static final long msInDay = 86400000L;
    private static final long msInHour = 3600000L;
    private static final long msInMinute = 60000L;
    private static final long msInSecond = 1000L;

    public static Date convertDoubleToDate(double d)
    {
        return convertDoubleToDate(d, true);
    }

    public static Date convertDoubleToDate(double d, boolean roundMilliseconds)
    {
        long days = (long)d - daysToEpoch;
        Date date = new Date(adjustedEpoch.getTime() + days * msInDay);

        double fraction = d - (long)d;
        fraction = 24 * fraction;
        int hours = (int)fraction;
        date.setTime(date.getTime() + hours * msInHour);

        fraction = fraction - hours;
        fraction = 60 * fraction;
        int minutes = (int)fraction;
        date.setTime(date.getTime() + minutes * msInMinute);

        fraction = fraction - minutes;
        fraction = 60 * fraction;
        int seconds = (int)fraction;
        date.setTime(date.getTime() + seconds * msInSecond);

        fraction = fraction - seconds;
        fraction = 1000 * fraction;
        int ms = (int)fraction;
        if ( roundMilliseconds )
        {
            double f = (double)ms/1000;
            long second = Math.round(f);
            date.setTime(date.getTime() + second * msInSecond);
        }
        else
        {
            date.setTime(date.getTime() + ms);
        }

        Calendar c = Calendar.getInstance();
        if ( c.getTimeZone().inDaylightTime(date) )
        {
            date.setTime(date.getTime() - c.getTimeZone().getDSTSavings());
        }
        
        return date;
    }

    /**
     * This algorthim is only accurate to the second level, not milliseconds.
     * @param d
     * @param roundMilliseconds
     * @return
     */
    public static Double convertDateToDouble(Date date)
    {
        Double val = null;

        if ( date != null )
        {
            long time = date.getTime();
            Calendar c = Calendar.getInstance();
            if ( c.getTimeZone().inDaylightTime(date) )
            {
                time += c.getTimeZone().getDSTSavings();
            }

            long days = time/msInDay;
            val = (double)(days + daysToEpoch);

            long hours = time-days*msInDay;
            double fraction = (double)(hours - adjustedEpoch.getTime())/msInDay;
            val += fraction;
            
        }

        return val;
    }

    /**
     * This algorthim is only accurate to the second level, not milliseconds.
     * @param d
     * @param roundMilliseconds
     * @return
     */
    public static Double convertDateToDouble(Date date, boolean roundMilliseconds)
    {
        Double val = null;

        if ( date != null )
        {
            long time = date.getTime();
            Calendar c = Calendar.getInstance();
            if ( roundMilliseconds )
            {
                c.setTime(date);
                double millis = (double)((double)c.get(Calendar.MILLISECOND)/msInSecond);
                c.add(Calendar.SECOND, (int)Math.round(millis));
                c.set(Calendar.MILLISECOND, 0);
                time = c.getTimeInMillis();
            }

            // Adjust the epoch if in daylight savings time
            if ( c.getTimeZone().inDaylightTime(date) )
            {
                time += c.getTimeZone().getDSTSavings();
            }
            
            long days = time/msInDay;
            val = (double)(days + daysToEpoch);
            
            long hours = time-days*msInDay;
            double fraction = (double)(hours - adjustedEpoch.getTime())/msInDay;
            val += fraction;
        }

        return val;
    }

    public static long dollarsToPennies(String dollars ) throws NumberFormatException {
        int pos = dollars.indexOf( '.' );
        long pennies = 0;
        
        if( pos == -1 ){ // no pennies....
            pennies = Long.parseLong( dollars ) * 100;
        } else {
            String before = dollars.substring(0, pos );
            long val = Long.parseLong( before ) * 100;
            String after = dollars.substring( pos+1 );
            int len = after.length();
            
            if( len > 2 ){ // truncate to 2 digits
                after = after.substring( 0, 2 );
                len = 2;
            } //END if( len > 2 )
            
            if( len != 0 ){
                long dec = Long.parseLong( after );
                
                if( len == 2 ){
                    val += dec;
                } else if( len == 1 ){
                    val += dec * 10;
                } //END if( len == 2 )
            } //END if( len != 0 )
            
            pennies = val;
        } //END if( pos == -1 )
        
        return pennies;
    } //END public static long dollarsToPennies(String dollars ) throws NumberFormatException
    

    public static void main(String[] args)
    {
        try
        {
            //java.time.LocalDateTime ldt = java.time.LocalDateTime.of(2018, 5, 2, 8, 57, 24, 873856000);
            //java.time.ZonedDateTime zdt = ldt.atZone(java.time.ZoneId.systemDefault());
            //System.out.println(ldt);
            //System.out.println(zdt);
              
            System.out.println(Double.toString(-0.01));
            System.out.println(dollarsToPennies(Double.toString(-0.01)));
            
            String s = "[A,Residence Hall A.R,Residence Hall B]";
            String[] i = s.split("\\.");
            System.out.println(java.util.Arrays.asList(i));
            /*Date date = new Date(1318873391000L);
            System.out.println(date.getTime());
            System.out.println(date);
            double db = convertDateToDouble(date, true);
            System.out.println(db);
            java.text.DecimalFormat doubleDateFormat = new java.text.DecimalFormat("#.##########");
            doubleDateFormat.setRoundingMode(RoundingMode.HALF_UP);
            System.out.println(doubleDateFormat.format(db));
            Date dt2 = convertDoubleToDate(db, false);
            System.out.println(dt2.getTime());
            System.out.println(dt2);*/
            System.exit(1);
            
            java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("EEE MMM d HH:mm:ss.SSS z yyyy");
            Date date = format.parse("Sat Nov 17 14:11:19.000 PDT 2012");
            Date now = new Date();
            System.out.println(date + "=" + date.getTime());
            System.out.println(now + "=" + now.getTime());
            java.text.SimpleDateFormat format2 = new java.text.SimpleDateFormat("EEEEE MMMMM d, yyyy");
            String ds = format2.format(date);
            System.out.println(ds);
            
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            c.add(Calendar.MONTH, 12);
            System.out.println(c.getTime() + "=" + c.getTimeInMillis());
            System.out.println(c.getTimeInMillis() < now.getTime());
            System.exit(1);
            
            //System.out.println(convertDateToDouble(date, false));
            
            //double d = convertDateToDouble(date, false);
            double d = 40852.823009259257;
            date = convertDoubleToDate(d, false);

            System.out.println(d);
            System.out.println(date);
            System.out.println(date.getTime());
            System.exit(1);

            format = new java.text.SimpleDateFormat("yyyyMMddHHmmss");
            d = convertDateToDouble(format.parse("20100101000000"));
            System.out.println(d);
            double d2 = convertDateToDouble(format.parse("20100101001500"));
            System.out.println(d2);
            System.out.println(d2-d);
            System.out.println(convertDoubleToDate(d, true));

            double d3 = (double)900/(double)86400;
            System.out.println(d3);
            
            System.out.println("Today: " + convertDateToDouble(new Date()));
            
            System.out.println("Epoch: " + new Date(0));
            System.out.println("Optim epoch: " + new Date(-61200000));
            
            String test = "A fatal error occurred while running ETL job: process-payroll-transactions (#20110714132716).\\n\\nNext line";
            System.out.println("'" + test.replaceAll("\\\\n", "\n") + "'");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
