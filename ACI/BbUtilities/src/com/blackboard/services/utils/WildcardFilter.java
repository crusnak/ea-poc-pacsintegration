/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.utils;

import java.io.File;
import java.io.FilenameFilter;

/**
 *
 * @author crusnak
 */
public class WildcardFilter implements FilenameFilter
{
    public static char CHAR_WILDCARD =  '*';
    private String[] filters = new String[0];
    private boolean containsWildcard = false;
    private boolean defaultAccept = false;
    private boolean ignoreCase = true;

    public WildcardFilter(String wildcard)
    {
        this(wildcard, true);
    }
    
    public WildcardFilter(String wildcard, boolean ignore)
    {
        if ( wildcard != null )
        {
            ignoreCase = ignore;
            int count = 0;
            for ( char c: wildcard.toCharArray() )
            {
                if ( c == CHAR_WILDCARD )
                {
                    count++;
                }
            }
            
            
            
            filters = wildcard.split("\\" + String.valueOf(CHAR_WILDCARD), count+1);
            containsWildcard = count >  0;
            if ( filters.length == 1 )
            {
                defaultAccept = filters[0].length() > 0;
            }
            else
            {
                defaultAccept = true;
            }
        }
    }
    
    public boolean accept(File dir, String name)
    {
        boolean accept = defaultAccept;
        for ( int i=0; i<filters.length; i++ )
        {
            String filter = filters[i];
            if ( !containsWildcard && filter.length() > 0 )
            {
                if ( ignoreCase )
                {
                    accept &= name.equalsIgnoreCase(filter);
                }
                else
                {
                    accept &= name.equals(filter);
                }
            }
            else if ( containsWildcard )
            {
                if ( i == 0 && filter.length() > 0 )
                {
                    if ( ignoreCase )
                    {
                        accept &= name.toLowerCase().startsWith(filter.toLowerCase());
                    }
                    else
                    {
                        accept &= name.startsWith(filter);
                    }
                }
                else if ( i == filters.length-1 && filter.length() > 0 )
                {
                    if ( ignoreCase )
                    {
                        accept &= name.toLowerCase().endsWith(filter.toLowerCase());
                    }
                    else
                    {
                        accept &= name.endsWith(filter);
                    }
                }
                else
                {
                    if ( ignoreCase )
                    {
                        accept &= name.toLowerCase().contains(filter.toLowerCase());
                    }
                    else
                    {
                        accept &= name.contains(filter);
                    }
                }
            }
        }

        return accept;
    }
    
    public static void main(String[] args)
    {
        try
        {
            String wc = "*.jpg";
            File dir = new File("i:/qw/image/optim/19980821");
            File[] list = dir.listFiles(new WildcardFilter(wc, false));
            java.util.regex.Pattern p = java.util.regex.Pattern.compile("$.*([0-9]+)\\.jpg^");
            for ( File f: list )
            {
                System.out.println("Found: " + f.getAbsolutePath());
                String[] matches = p.split(f.getAbsolutePath());
                for ( String s: matches )
                {
                    System.out.println(s);
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
