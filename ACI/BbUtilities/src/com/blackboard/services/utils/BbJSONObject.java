package com.blackboard.services.utils;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * This class defines a specific implementation of the JSONSimple API to create
 * bean objects that can be converted to or created from a JSON string.  This
 * facilitates communication to AJAX clients who want more speed than XML can
 * give them.
 * @author crusnak
 */
public class BbJSONObject extends JSONObject implements JSON
{
    protected static SimpleDateFormat jsonDateFormat = new SimpleDateFormat("MM-dd-yyyy");
    protected static SimpleDateFormat jsonDateTimeFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
    protected static SimpleDateFormat jsonDateTimeFormatMS = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss.SSS");
 
    /**
     * Default empty constructor.
     */
    public BbJSONObject()
    {
        super();
    }

    /**
     * Constructor that parses the given JSON string and creates an object map
     * representation that can be further manipulated.
     * @param json The JSON string to parse and create an object from.
     * @throws IllegalArgumentException Thrown if the incoming string cannot be parsed into a JSON construct.
     */
    public BbJSONObject(String json)
        throws IllegalArgumentException
    {
        super();
        setJSONString(json);
    }

    /**
     * Constructor that parses the given JSON string and creates an object map
     * representation that can be further manipulated.
     * @param json The JSON string to parse and create an object from.
     * @param root The root mapping the entire object to (necessary for REST responses)
     * @throws IllegalArgumentException Thrown if the incoming string cannot be parsed into a JSON construct.
     */
    public BbJSONObject(String json, String root)
        throws IllegalArgumentException
    {
        super();
        if ( root != null && root.trim().length() > 0 )
        {
            StringBuilder modjson = new StringBuilder();
            modjson.append("{\"" + root + "\": ");
            modjson.append(json);
            modjson.append("}");
            setJSONString(modjson.toString());
        }
        else
        {
            setJSONString(json);
        }
    }

    /**
     * This method parses the given JSON string and returns either a JSONObject, JSONArray, or
     * JSONValue.
     * @param json The JSON string to parse
     * @return Either a JSONObject, JSONArray, or JSONValue.
     * @throws IllegalArgumentException Thrown if the incoming string cannot be parsed into a JSON construct.
     */
    public static Object parseJSONString(String json)
        throws IllegalArgumentException
    {
        Object o = null;
        try
        {
            JSONParser parser = new JSONParser();
            o = parser.parse(json);
        }
        catch (ParseException e)
        {
            throw new IllegalArgumentException("Invalid json string: " + json, e);
        }
        return o;
    }

    /**
     * Adds a new Array of parameter values to this JSONObject.
     * @param key The name of the parameter
     * @param jsons An array of JSON objects
     * @return The replaced value for this parameter or <tt>null</tt> if not previously assigned.
     */
    public Object put(Object key, JSON[] jsons)
    {
        return super.put(key, new BbJSONArray(jsons));
    }
    
    /**
     * Adds a new key/value pair or overwrites the existing value.
     * This is overloaded to check if the value is an enumeration value or not. If so, the enumeration value is stored as a string so
     * external JSON parsers can properly handle those values.
     * @param key The name of the parameter
     * @param val The value of the parameter
     * @return The replaced value for this parameter or <tt>null</tt> if not previously assigned.
     */
    @Override
    public Object put(Object key, Object val)
    {
        if ( val instanceof Enum )
        {
            //System.out.println("Adding enum to json");
            return super.put(key, String.valueOf(val));
        }
        else
        {
            return super.put(key, val);
        }
    }

    /**
     * Converts this Object into a JSON string.
     * @return The JSON string representation of this object.
     */
    @Override
    public String toString()
    {
        return toJSONString();
    }

    @Override
    public String toJSONString()
    {
        return super.toJSONString();
    }

    /**
     * Converts the JSON string to a JSONObject with the parameters defined in the
     * string.  If the JSON string is not a JSONObject an exception will be thrown.
     * @param json The JSON string to parse
     * @throws IllegalArgumentException Thrown if the incoming string cannot be parsed into a JSON construct,
     * or the JSON string represents a JSONArray or JSONValue.
     */
    public void setJSONString(String json)
        throws IllegalArgumentException
    {
        clear();
        Object parsed = parseJSONString(json);
        if ( parsed instanceof JSONObject )
        {
            Iterator it = ((JSONObject)parsed).entrySet().iterator();

            // Iterator over all map values
            while ( it.hasNext() )
            {
                Map.Entry entry = (Map.Entry)it.next();
                Object value = entry.getValue();

                // If the value is a JSONArray, it needs to be stored as a list of values
                if ( value instanceof JSONArray )
                {
                    ArrayList<BbJSONObject> list = new ArrayList();
                    for ( Object o: (JSONArray)value )
                    {
                        //org.apache.log4j.Logger.getLogger(this.getClass()).debug("ArrayElement: '" + o + "'");
                        if ( o != null && String.valueOf(o).trim().length() > 0 )
                        {
                            list.add(new BbJSONObject(String.valueOf(o)));
                        }
                    }
                    put(entry.getKey(), list);
                }
                // If the value is a JSONObject, it needs to be stored as a parsed BbJSONObject
                else if ( value instanceof JSONObject )
                {
                    put(entry.getKey(), new BbJSONObject(String.valueOf(entry.getValue())));
                }
                else
                {
                    put(entry.getKey(), entry.getValue());
                }
            }
        }
        else if ( parsed instanceof JSONArray )
        {
            throw new IllegalArgumentException("Unable to convert json array to JSONObject. Force an object reference around array: " + json);
        }
        else
        {
            throw new IllegalArgumentException("Unable to convert json string to JSONObject: " + json);
        }
    }

    @Override
    public void writeJSONString(Writer writer)
            throws IOException
    {
        //super.writeJSONString(writer);
        throw new UnsupportedOperationException("BbJSONObject.writeJSONString not supported yet");
    }

    /**
     * This class is used to store an Array of JSONObjects.  Convenience class to ensure
     * that the JSONSimple api properly converts an array of BbJSONObjects into a string.
     */
    public static class BbJSONArray
    {
        private JSON[] jsons;

        /**
         * Constructor taking an array of JSON objects.
         * @param objects Array of JSON objects.
         */
        public BbJSONArray(JSON[] objects)
        {
            jsons = objects;
        }
        
        public int getLength()
        {
            return jsons.length;
        }
        
        public JSON get(int index)
        {
            return jsons[index];
        }

        /**
         * This method ensures that an array of BbJSONObjects are properly converted
         * to their string representations.  This also ensures that nested objects
         * with nested objects are converted properly.
         * @return The string representation of the array of JSON objects.
         */
        public String toString()
        {
            String string = "";
            string += "[";
            for ( int i=0; i<jsons.length; i++ )
            {
                string += jsons[i].toJSONString();
                if ( i < jsons.length-1 )
                {
                    string += ",";
                }
            }
            string += "]";

            return string;
        }
    }
    
    public String convertIntercappedToUnderscoredCapped(String in)
    {
        String out = null;
        if ( in != null )
        {
            out = "";
            char[] chars = in.toCharArray();
            boolean upper = true; // indicates if char is upper
            for ( char c: chars )
            {
                // Leading edge so create a new separator
                if ( upper == false && Character.isUpperCase(c) )
                {
                    out += '_';
                }
                
                upper = Character.isUpperCase(c);
                out += Character.toUpperCase(c);
            }
        }
        
        return out;
    }

    protected List<String> getJSONArrayString(Object o)
    {
        ArrayList<String> list = new ArrayList();
        try
        {
            JSONParser parser = new JSONParser();
            if ( o != null )
            {
                Object json = parser.parse(String.valueOf(o));
                if ( json != null && json instanceof JSONArray )
                {
                    JSONArray array = (JSONArray)json;
                    for ( int i=0; i<array.size(); i++ ) 
                    {
                        list.add(String.valueOf(array.get(i)));
                    }
                }
            }
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        
        return list;
    }

    
    public static String formatDate(Date date)
    {
        String d = null;
        if ( date != null )
        {
            d = jsonDateFormat.format(date);
        }
        return d;
    }

    public static String formatDateTime(Date date)
    {
        String dt = null;
        if ( date != null )
        {
            dt = jsonDateTimeFormat.format(date);
        }
        return dt;
    }
    
    public static String formatDateTimeMS(Date date)
    {
        String dt = null;
        if ( date != null )
        {
            dt = jsonDateTimeFormatMS.format(date);
        }
        return dt;
    }

    public static Date parseDateString(String string)
        throws java.text.ParseException
    {
        Date d = null;
        if ( string != null )
        {
            d = jsonDateFormat.parse(string);
        }
        return d;
    }

    public static Date parseDateTimeString(String string)
        throws java.text.ParseException
    {
        Date dt = null;
        if ( string != null )
        {
            dt = jsonDateTimeFormat.parse(string);
        }
        return dt;
    }
    
    public static Date parseDateTimeMSString(String string)
        throws java.text.ParseException
    {
        Date dt = null;
        if ( string != null )
        {
            dt = jsonDateTimeFormatMS.parse(string);
        }
        return dt;
    }
    
    public static void main(String[] args)
    {
        String j = "[{\"id\":\"23390677-4187-4185-8398-7806e4836639\",\"correlationId\":\"23390677-4187-4185-8398-7806e4836639\",\"type\":\"CustomerChanged\",\"time\":\"2021-07-14T17:23:38.454+00:00\",\"source\":\"d18f30df-f17f-44e9-9a35-33dde5652cb9\",\"subject\":\"d18f30df-f17f-44e9-9a35-33dde5652cb9\",\"specversion\":\"v1.0\",\"dataschema\":\"v1.0\",\"subscribers\":\"d18f30df-f17f-44e9-9a35-33dde5652cb9\",\"replayAttempt\":null,\"data\":{\"firstName\":\"Chris\",\"middleName\":\"Salto-Load01\",\"lastName\":\"bslastname100001\",\"isActive\":true,\"startDate\":\"2012-01-01T07:00:00+00:00\",\"endDate\":\"2060-08-25T07:00:00+00:00\",\"customerNumber\":\"0000000000000000100001\"}}]";
        
        System.out.println(j);
        //BbJSONObject json = new BbJSONObject(j);
        Object parsed = parseJSONString(j);
        System.out.println(parsed);
        if ( parsed instanceof JSONArray )
        {
            System.out.println("JSONArray");
            JSONArray array = (JSONArray)parsed;
            for ( Object obj: array )
            {
                BbJSONObject json = new BbJSONObject(String.valueOf(obj));
                System.out.println(json);
            }
        }
        else if ( parsed instanceof JSONObject )
        {
            System.out.println("JSONObject");
        }
        else
        {
            System.out.println("Unknown parsed object: " + parsed.getClass());
        }
    }
}
