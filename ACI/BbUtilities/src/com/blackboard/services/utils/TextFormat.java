/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.utils;

import java.util.List;
import java.util.Map;

/**
 *
 * @author crusnak
 */
public interface TextFormat
{
    public Map<String,String> processLine(String line) throws TextFormatException;
    public String[] readHeader(String line);
    public String formatLine(List<String> values) throws TextFormatException;
    public String formatLine(String[] values) throws TextFormatException;
    public String formatLine(Map<String,String> values) throws TextFormatException;
    public <E extends Field> List<E> getSimpleFields();
    public <E extends Field> E getFieldByName(String name);

    public static interface Field
    {
        public String getName();
        public boolean isConstant();
        public String getDataType();
    }
}
