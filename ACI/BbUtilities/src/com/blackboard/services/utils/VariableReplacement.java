/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.utils;

import java.util.Map;
import java.util.regex.Matcher;
import javax.management.ReflectionException;
import org.apache.log4j.Logger;

/**
 *
 * @author crusnak
 */
public class VariableReplacement
{
    private int startIndex;
    private int endIndex;
    private String beanText;
    private boolean processIndividual = true;
    private Map<String,?> wrapper;
    private char beanChar = '$';

    public VariableReplacement(int s, int e, Map<String,?> w)
    {
        startIndex = s;
        endIndex = e;
        wrapper = w;
    }

    public VariableReplacement(int s, int e, String bt, Map<String,?> w)
    {
        this(s, e, w);
        beanText = bt;
    }
    
    public VariableReplacement(int s, int e, String bt, Map<String,?> w, boolean individual)
    {
        this(s,e,w);
        beanText = bt;
        processIndividual = individual;
    }

    public VariableReplacement(int s, int e, String bt, Map<String,?> w, boolean individual, char c)
    {
        this(s,e,bt,w,individual);
        beanChar = c;
    }

    public int getEndIndex()
    {
        return endIndex;
    }

    public void setEndIndex(int endIndex)
    {
        this.endIndex = endIndex;
    }

    public String getBeanText()
    {
        return beanText;
    }

    public void setBeanText(String bt)
    {
        this.beanText = bt;
    }

    public int getStartIndex()
    {
        return startIndex;
    }

    public void setStartIndex(int startIndex)
    {
        this.startIndex = startIndex;
    }

    public String getReplacementText()
    {
        return getReplacementText(false);
    }
    
    public char getBeanChar()
    {
        return beanChar;
    }
    
    public void setBeanChar(char c)
    {
        beanChar = c;
    }
    
    public String getReplacementText(boolean nullAsEmpty)
    {
        String text = beanText;
        if ( processIndividual )
        {
            BeanScript bs = new BeanScript(beanText, beanChar);
            if ( bs.hasRemainingAccessors() )
            {
                String varname = bs.getNextAccessor();
                Object val = wrapper.get(varname);
                if ( val != null )
                {
                    try
                    {
                        while ( bs.hasRemainingAccessors() )
                        {
                            val = bs.accessNextObject(val);
                        }

                        text = String.valueOf(val);
                    }
                    catch (ReflectionException e)
                    {
                        BaseLogger.warn(e.getMessage(), e);
                        Logger.getLogger(this.getClass()).warn(e);
                        if ( nullAsEmpty )
                        {
                            text = "";
                        }
                        else
                        {
                            text = "{null}";
                        }
                    }
                }
                else if ( nullAsEmpty )
                {
                    text = "";
                }
                else
                {
                    text = "{null}";
                }
            }
        }
        else
        {
            Matcher m = BeanScript.getSinglePattern(beanChar).matcher(beanText);
            if ( m.matches() )
            {
                Object val = wrapper.get(m.group(1));
                if ( val != null )
                {
                    text = String.valueOf(val);
                }
                else if ( nullAsEmpty )
                {
                    text = "";
                }
                else
                {
                    text = "{null}";
                }
            }
            else if ( nullAsEmpty )
            {
                text = "";
            }
            else
            {
                text = "{null}";
            }
        }
        return text;
    }

    public String getReplacementText(String var, Object val)
    {
        String text = beanText;
        BeanScript bs = new BeanScript(beanText);
        if ( bs.hasRemainingAccessors() )
        {
            String ref = bs.getNextAccessor();
            if ( var.equals(ref) && val != null )
            {
                try
                {
                    while ( bs.hasRemainingAccessors() )
                    {
                        val = bs.accessNextObject(val);
                    }

                    text = String.valueOf(val);
                }
                catch (ReflectionException e)
                {
                    BaseLogger.warn(e.getMessage(), e);
                    Logger.getLogger(this.getClass()).warn(e);
                    text = "{null}";
                }
            }
            else
            {
                text = getReplacementText();
            }
        }
        return text;
    }

    public String toString()
    {
        return "(start=" + startIndex + ", end=" + endIndex + ", val=" + getReplacementText() + ", bt=" + beanText + ")";
    }
}
