/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.utils;

import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author crusnak
 */
public class CommandLineArgumentManager
{
    public static <E extends Enum<E>> Map<String,String>
        removeActionArgumentsFromMap(Map<String,String> args, Class<E> params)
    {
        LinkedHashMap<String,String> newmap = new LinkedHashMap();
        for ( Map.Entry<String,String> entry: args.entrySet() )
        {
            String key = entry.getKey();
            String val = entry.getValue();
            try
            {
                Enum.valueOf(params, key);
            }
            catch (IllegalArgumentException e)
            {
                newmap.put(key, val);
            }
        }



        return newmap;
    }

    public static <E extends Enum<E>> Map<String,String>
        extractCommandLineArguments(Class clazz, Class<E> actionClazz, Map<E,Class<? extends Enum>> actionMap, List<String> args)
        throws IllegalArgumentException
    {
        return extractCommandLineArguments(clazz, actionClazz, actionMap, args.toArray((new String[0])));
    }
    
    public static <E extends Enum<E>> Map<String,String>
        extractCommandLineArguments(Class clazz, Class<E> actionClazz, Map<E,Class<? extends Enum>> actionMap, String[] args)
        throws IllegalArgumentException
    {
        LinkedHashMap<String,String> paramMap = new LinkedHashMap();
        String usage = "Usage: " + clazz.getSimpleName() + " " + JavaUtils.enumToList(actionClazz) + " <action specific params>";

        try
        {
            if ( args.length > 0 )
            {
                String[] pargs = new String[args.length-1];
                System.arraycopy(args, 1, pargs, 0, args.length-1);
                String action = null;
                try
                {
                    action = args[0];
                }
                catch (Exception e)
                {
                    throw new IllegalArgumentException(usage, e);
                }

                boolean found = false;
                List<E> actions = JavaUtils.enumToList(actionClazz);
                for ( E act: actions )
                {
                    if ( action.equals(String.valueOf(act)) )
                    {
                        found = true;
                        Class<? extends Enum> paramClazz = actionMap.get(act);
                        if ( paramClazz == null )
                        {
                            throw new IllegalArgumentException("Missing required paramater enum for action: " + act + "\n" + usage);
                        }

                        List<Enum> plist = JavaUtils.enumToList(paramClazz);
                        String params = "";
                        for ( Enum p: plist )
                        {
                            params += "<" + p + "> ";
                        }
                        usage = "Usage: " + clazz.getSimpleName() + " " + action + " " + params + "<optional_arg=optional_val>(OPTIONAL)";

                        // Assign start parameters
                        for ( int i=0; i<plist.size(); i++ )
                        {
                            Enum p = plist.get(i);
                            if ( pargs.length <=i )
                            {
                                throw new IllegalArgumentException("Missing required parameter: " + p + "\n" + usage);
                            }
                            paramMap.put(String.valueOf(p), pargs[i]);
                        }

                        // Assign optional paramters
                        String[] oargs = new String[pargs.length-plist.size()];
                        System.arraycopy(pargs, plist.size(), oargs, 0, oargs.length);
                        for ( int i=0; i<oargs.length; i++ )
                        {
                            String[] argary = oargs[i].split("=");
                            if ( argary.length != 2 )
                            {
                                throw new IllegalArgumentException("Optional arguments must be in the form of optional_arg=optional_val: " + oargs[i] + "\n" + usage);
                            }
                            paramMap.put(argary[0], argary[1]);
                        }
                    }
                }
                
                if ( !found )
                {
                    throw new IllegalArgumentException("Unsupported action '" + action + "'. Valid actions: " + actions);
                }
            }
            else
            {
                throw new IllegalArgumentException(usage);
            }
        }
        catch (IllegalArgumentException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new IllegalArgumentException(usage, e);
        }

        return paramMap;
    }
}
