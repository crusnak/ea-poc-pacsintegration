/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.utils;

import java.io.File;
import java.io.FilenameFilter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import javax.servlet.ServletContext;
import org.apache.log4j.Logger;

/**
 *
 * @author crusnak
 */
public class ResourceLoader
{
    public static URL loadResourceAsURL(ClassLoader cl, String resource)
    {
        URL url = null;
        if ( cl == null )
        {
            cl = ClassLoader.getSystemClassLoader();
        }
        
        url = cl.getResource(resource);
        return url;
    }

    private static URL extractResourceFromArchive(URL url, File appRoot)
    {
        BaseLogger.info("Resource found in archive, extracting to application root");
        int idx = url.getFile().indexOf('!');
        String path = url.getFile().substring(0, idx);
        String apath = url.getFile().substring(idx+1);

        try
        {
            URL aurl = new URL(path);
            ZipFile archive = new ZipFile(aurl.getFile());
            File extracted = FileUtils.extractFileFromArchive(archive, apath, appRoot);
            if ( extracted != null && extracted.exists() )
            {
                url = extracted.toURI().toURL();
                BaseLogger.info("Extracted archived resource to: " + extracted.getCanonicalPath());
            }
        }
        catch (MalformedURLException e) 
        {
            BaseLogger.warn("Unable to extract '" + apath + "' from archive: " + path, e);
        }
        catch (IOException e)
        {
            BaseLogger.warn("Unable to extract '" + apath + "' from archive: " + path, e);
        }

        return url;
    }

    public static URL loadResourceAsURL(ServletContext ctx, String resource)
    {
        URL url = null;
        String webappRoot = "/WEB-INF/";

        if ( ctx != null )
        {
            try
            {
                String path = ctx.getRealPath(webappRoot + resource);
                url = ctx.getResource(webappRoot + resource);
            }
            catch (MalformedURLException e)
            {
                e.printStackTrace(System.err);
            }
        }
        return url;
    }

    public static File loadResourceAsFile(ServletContext ctx, String resource)
    {
        File file = new File(resource);
        String webappRoot = "/WEB-INF/";

        if ( ctx != null )
        {
            String path = ctx.getRealPath(webappRoot + resource);
            file = new File(path);
        }
        return file;
    }

    public static URL loadResourceAsURL(File root, String resource)
    {
        URL url = null;

        if ( root == null )
        {
            root = new File("");
        }

        File f = new File(root, resource);
        if ( f.exists() )
        {
            try
            {
                url = f.toURI().toURL();
            }
            catch (MalformedURLException e)
            {
                e.printStackTrace(System.err);
            }
        }

        return url;
    }

    public static File loadResourceAsFile(File root, String resource)
    {
        if ( root == null )
        {
            root = new File("");
        }

        return new File(root, resource);
    }

    public static File findResourceAsFile(ServletContext context, File fileRoot, String path)
        throws IOException
    {
        File file = new File(path);

        if ( !file.exists() )
        {
            BaseLogger.debug("Can't find '" + path + "' at: " + file.getCanonicalPath() + ". Searching within the classpath");
            URL url = loadResourceAsURL(ClassLoader.getSystemClassLoader(), path);
            if ( url != null && url.getFile().contains("!") )
            {
                url = extractResourceFromArchive(url, fileRoot);
                file = new File(url.getFile());
            }
            else if ( url != null )
            {
                file = new File(url.getFile());
            }
        }

        // Look for resource
        if ( !file.exists() )
        {
            BaseLogger.debug("Can't find '" + path + "' at: " + file.getAbsolutePath() + ". Searching within app server web context.");
            file = loadResourceAsFile(context, path);
        }

        if ( !file.exists() )
        {
            BaseLogger.debug("Can't find '" + path + "' at: " + file.getAbsolutePath() + ". Searching within root directory.");
            file = loadResourceAsFile(fileRoot, path);
        }

        if ( !file.exists() )
        {
            BaseLogger.debug("Can't find '" + path + "' at: " + file.getAbsolutePath() + ". Searching as app server web context URL.");
            URL url = loadResourceAsURL(context, path);
            if ( url != null )
            {
                file = new File(url.getFile());
            }
        }

        if ( !file.exists() )
        {
            BaseLogger.debug("Can't find '" + path + "' at: " + file.getAbsolutePath() + ". searching within root directory.");
            throw new IOException("Unable to find resource. Please ensure that '" + path + "' exists somewhere " +
                                  "on the classpath, application server web context or within the root " +
                                  "application path");
        }
        else
        {
            BaseLogger.debug("Found '" + path + "': " + file.getCanonicalPath());
        }

        return file;
    }

    public static URL findResourceAsURL(ServletContext context, File fileRoot, String path)
        throws IOException
    {
        // Look for the resource on the classpath
        URL url = loadResourceAsURL(ClassLoader.getSystemClassLoader(), path);

        // Look for resource
        if ( url == null )
        {
            BaseLogger.debug("Can't find '" + path + "' on the classpath, searching within app server web context");
            url = loadResourceAsURL(context, path);
        }

        if ( url == null )
        {
            BaseLogger.debug("Can't find '" + path + "' in an app server context, searching within root directory");
            url = loadResourceAsURL(fileRoot, path);
        }

        if ( url == null )
        {
            BaseLogger.debug("Can't find '" + path + "' in the app root directory, searching within root directory");
            throw new IOException("Unable to find resource. Please ensure that '" + path + "' exists somewhere " +
                                  "on the classpath, application server web context or within the root " +
                                  "application path");
        }
        else
        {
            BaseLogger.debug("Found '" + path + "': " + url);
        }

        return url;
    }
    
    public static List<File> findResourcesAsFiles(String filter)
        throws IOException
    {
        ArrayList<File> files = new ArrayList();
        String cp = System.getProperty("java.class.path");
        String[] paths = cp.split(";");
        ResourceFilter resourceFilter = new ResourceFilter(filter);
        for ( String path: paths )
        {
            File f = new File(path);
            //System.out.println("Path=" + f);
            if ( f.isDirectory() )
            {
                File[] found = f.listFiles(resourceFilter);
                for ( File file: found )
                {
                    files.add(file);
                }
                
                File[] afiles = f.listFiles();
                for ( File file: afiles )
                {
                    if ( file.isDirectory() )
                    {
                        files.addAll(findResourcesAsFiles(file, filter));
                    }
                }
            }

        }
        
        return files;
    }
    
    protected static List<File> findResourcesAsFiles(File root, String filter)
        throws IOException
    {
        ArrayList<File> files = new ArrayList();
        ResourceFilter resourceFilter = new ResourceFilter(filter);
        if ( root != null && root.isDirectory() )
        {
            //System.out.println("Path=" + root);
            File[] found = root.listFiles(resourceFilter);
            for ( File file: found )
            {
                files.add(file);
            }

            File[] afiles = root.listFiles();
            for ( File file: afiles )
            {
                if ( file.isDirectory() )
                {
                    files.addAll(findResourcesAsFiles(file, filter));
                }
            }
        }
        
        return files;
    }
    
    public static class ResourceFilter implements FilenameFilter
    {
        String filter = "";
        
        public ResourceFilter(String f)
        {
            filter = f;
        }
        
        public boolean accept(File dir, String name)
        {
            return name.contains(filter);
        }
        
    }
    
    public static void main(String[] args)
    {
        try
        {
            System.out.println("Testing");
            System.out.println(findResourcesAsFiles(".class"));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
