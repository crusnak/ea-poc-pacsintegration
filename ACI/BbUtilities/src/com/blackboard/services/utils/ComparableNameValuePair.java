/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.utils;

import java.io.Serializable;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHeader;

/**
 *
 * @author crusnak
 */
public class ComparableNameValuePair implements NameValuePair, Serializable, Cloneable, Comparable<Object> 
{
    private static final long serialVersionUID = 59600850905435L;
    
    protected String name;
    protected String value;
    
    public ComparableNameValuePair(String n, String v)
    {
        name = n;
        value = v;
    }
    
    /**
     * This constructor expects the name/value pair to be in the form name=value. The first string will be split on the first instance of =.
     * @param nvpair 
     */
    public ComparableNameValuePair(String nvpair)
    {
        if ( nvpair != null )
        {
            String[] pair = nvpair.split("=", 2);
            try
            {
                name = pair[0];
                value = pair[1];
            }
            catch (ArrayIndexOutOfBoundsException e)
            {
                name = pair[0];
                value = "";
            }
        }
    }
    
    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public int compareTo(Object o) 
    {
        return toString().compareTo(String.valueOf(o));
    }

    @Override
    public String toString() 
    {
        return name + "=" + value;
    }
    
    
}
