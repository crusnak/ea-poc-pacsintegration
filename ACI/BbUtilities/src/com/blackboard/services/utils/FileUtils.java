/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 *
 * @author crusnak
 */
public class FileUtils
{
    public static boolean createFileAndDirectories(File file)
        throws IOException
    {
        boolean success = false;
        if ( file != null )
        {
            if ( !file.exists() )
            {
                file.getParentFile().mkdirs();
            }
            success = file.createNewFile();
        }
        return success;
    }

    public static boolean copyFromStream(InputStream in, OutputStream out)
        throws IOException
    {
        boolean success = false;
        if ( in != null && out != null )
        {
            int avail = 0;
            while ( (avail = in.available()) > 0 )
            {
                byte[] bytes = new byte[avail];
                in.read(bytes);
                out.write(bytes);
            }

            out.flush();
            out.close();
        }
        return success;
    }

    public static File extractFileFromArchive(ZipFile archive, String apath, File dest)
        throws IOException
    {
        File file = null;
        if ( archive != null )
        {
            // Ensure that the path doesn't start with a file separator or the file
            // will never be found in the archive
            if ( apath.startsWith("/") || apath.startsWith("\\") )
            {
                apath = apath.substring(1);
            }
            
            ZipEntry entry = archive.getEntry(apath);
            if ( entry == null )
            {
                throw new IOException("Can't find " + apath + " within archive.");
            }
            
            file = new File(dest, apath);
            createFileAndDirectories(file);
            copyFromStream(archive.getInputStream(entry), new FileOutputStream(file));
        }

        return file;
    }

    public static List<String> extractFileNamesFromArchive(ZipFile archive)
    {
        ArrayList<String> names = new ArrayList();
        if ( archive != null )
        {
            Enumeration<? extends ZipEntry> entries = archive.entries();
            while ( entries.hasMoreElements() )
            {
                ZipEntry entry = entries.nextElement();
                names.add(entry.getName());
            }
        }

        return names;
    }

    public static boolean deleteDirectory(File dir)
    {
        if ( dir.exists() && dir.isDirectory() )
        {
            File[] files = dir.listFiles();
            for ( int i=0; i<files.length; i++ )
            {
                if ( files[i].isDirectory() )
                {
                    deleteDirectory(files[i]);
                }
                else
                {
                    files[i].delete();
                }
            }
        }

        return dir.delete();
    }
    
    public static String readFileContents(String resource)
        throws IOException
    {
        String query = "";
        URL url = ClassLoader.getSystemResource(resource);
        if ( url != null )
        {
            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
            String line = "";
            while ( (line= reader.readLine()) != null )
            {
                query += line;
            }
        }
        else
        {
            throw new FileNotFoundException("Unable to locate resource " + resource + ". Ensure it exists within the classpaths.");
        }

        return query;
    }
}
