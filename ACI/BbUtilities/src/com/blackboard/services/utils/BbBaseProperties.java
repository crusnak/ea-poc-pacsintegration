/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import javax.servlet.ServletContext;

/**
 * This class is used to allow common programmatic interface to properties files.  It also
 * handles loading and saving of the properties file using the ResourceLoader.  This ensures
 * that all child classes of this class will be able to find their associated properties
 * files via the same algorithm.
 * @author crusnak
 * @see ResourceLoader
 */
public class BbBaseProperties extends Properties
{
    protected File appRoot = null;
    protected ServletContext context = null;
    protected URL propertyURL = null;

    /**
     * Internal constructor used to define the servlet context, application root, and the
     * resource of the properties file.  This will NOT create a new resource file if this
     * resource cannot be found.
     * @param ctx The ServletContext for finding all resources associated with this class
     * in a web context.
     * @param root The application root directory used as a fallback if files are not found
     * within the web context.
     * @param resource The application resource (properties filename).
     * @throws IOException thrown if the resource is not able to be found.  <tt>Note:</tt> this
     * means that if you are generating properties files on the fly this class will not work
     * unless you create an empty file to work from.  Best practice is to create an empty
     * file at installation or build time to ensure that the underlying properties class can
     * find it's properties file and update it upon saving.
     */
    protected BbBaseProperties(ServletContext ctx, File root, String resource)
        throws IOException
    {
        this(ctx, root, resource, false);
    }

    /**
     * Internal constructor used to define the servlet context, application root, and the
     * resource of the properties file.
     * @param ctx The ServletContext for finding all resources associated with this class
     * in a web context.
     * @param root The application root directory used as a fallback if files are not found
     * within the web context.
     * @param resource The application resource (properties filename).
     * @param create Creates the resource if it doesn't exist using the root File given.
     * @throws IOException thrown if the resource is not able to be found.  <tt>Note:</tt> this
     * means that if you are generating properties files on the fly this class will not work
     * unless you create an empty file to work from.  Best practice is to create an empty
     * file at installation or build time to ensure that the underlying properties class can
     * find it's properties file and update it upon saving.
     */
    protected BbBaseProperties(ServletContext ctx, File root, String resource, boolean create)
        throws IOException
    {
        super();
        context = ctx;
        appRoot = root;
        updatePropertyURL(resource, create);
        
        /*File f = null;
        InputStream is = null;
        
        try
        {
            f = ResourceLoader.findResourceAsFile(context, appRoot, resource);
        }
        catch (IOException e)
        {
            if ( create )
            {
                f = new File(appRoot, resource);
                f.createNewFile();
                BaseLogger.warn("Unable to find resource, creating new resource using app root: " + f);
            }
            else
            {
                throw e;
            }
        }
        
        try
        {
            propertyURL = f.toURI().toURL();
            is = propertyURL.openStream();
            load(is);
        }
        finally
        {
            if ( is != null )
            {
                is.close();
            }
        }*/
    }

    public void updatePropertyURL(String resource, boolean create)
        throws IOException
    {
        File f = null;
        InputStream is = null;

        try
        {
            f = ResourceLoader.findResourceAsFile(context, appRoot, resource);
        }
        catch (IOException e)
        {
            if ( create )
            {
                f = new File(appRoot, resource);
                f.createNewFile();
                BaseLogger.warn("Unable to find resource, creating new resource using app root: " + f);
            }
            else
            {
                throw e;
            }
        }
        
        try
        {
            propertyURL = f.toURI().toURL();
            is = propertyURL.openStream();
            load(is);
        }
        finally
        {
            if ( is != null )
            {
                is.close();
            }
        }
    }
    
    /**
     * Loads the properties file identified by the resource defined during construction.
     * @throws IOException thrown if the properties file cannot be found.
     */
    public void load()
        throws IOException
    {
        if ( propertyURL == null )
        {
            throw new IOException("Cannot load properties for " + this.getClass().getName() +
                                  ". Unable to find properties file.");
        }

        InputStream is = propertyURL.openStream();
        load(propertyURL.openStream());
        is.close();
    }

    /**
     * Saves all of the properties defined in this class in the file identified by the
     * resource defined at construction.
     * @throws IOException thrown if the properties file doesn't exist or the resource URL
     * is not a local file. <tt>Note:</tt> this means that if you are generating properties
     * files on the fly this class will not work unless you create an empty file to work from.
     * Best practice is to create an empty file at installation or build time to ensure that the
     * underlying properties class can find it's properties file and update it upon saving.
     */
    public void save()
        throws IOException
    {
        save(null);
    }

    /**
     * Saves all of the properties defined in this class in the file identified by the
     * resource defined at construction.
     * @param title An optional title to enter as a comment in the properties file.
     * @throws IOException thrown if the properties file doesn't exist or the resource URL
     * is not a local file. <tt>Note:</tt> this means that if you are generating properties
     * files on the fly this class will not work unless you create an empty file to work from.
     * Best practice is to create an empty file at installation or build time to ensure that the
     * underlying properties class can find it's properties file and update it upon saving.
     */
    public void save(String title)
        throws IOException
    {
        if ( propertyURL == null )
        {
            throw new IOException("Cannot save properties for " + this.getClass().getName() +
                                  ". Unable to find properties file.");
        }

        File out = new File(propertyURL.getFile());
        if ( !out.exists() )
        {
            throw new IOException("Cannot save properties for " + this.getClass().getName() +
                                  ". Unable to convert properties URL to file: " + out);
        }

        FileOutputStream os = null;
        try
        {
            os = new FileOutputStream(out);
            store(os, title);
        }
        finally
        {
            if ( os != null )
            {
                os.close();
            }
        }
    }
}
