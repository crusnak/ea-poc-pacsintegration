/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.management.ReflectionException;

/**
 *
 * @author crusnak
 */
public class BeanScript
{
    public static final Pattern embeddedPattern = Pattern.compile("(\\$\\{.+?\\})");
    public static final Pattern scriptPattern = Pattern.compile("^\\s*\\$\\{(.*)\\}\\s*$");
    public static final Pattern beanPattern = Pattern.compile("^(.+?)\\.(.+)$");
    public static final Pattern singlePattern = Pattern.compile("^\\$\\{(.+)\\}$");

    private char beanChar = '$';
    private String originalText = "";
    private String accessorText = null;
    private String currentAccessor = "";
    
    public static Pattern getEmbeddedPattern(char c)
    {
        return Pattern.compile("(\\" + c + "\\{.+?\\})");
    }

    public static Pattern getScriptPattern(char c)
    {
        return Pattern.compile("^\\s*\\" + c + "\\{(.*)\\}\\s*$");
    }

    public static Pattern getSinglePattern(char c)
    {
        return Pattern.compile("^\\" + c + "\\{(.+)\\}$");
    }

    public BeanScript(String text)
    {
        beanChar = '$';
        originalText = text;
        accessorText = extractAccessors();
    }
    
    public BeanScript(String text, char c)
    {
        beanChar = c;
        originalText = text;
        accessorText = extractAccessors();
    }

    public String getScriptText()
    {
        return originalText;
    }

    public String getAccessorText()
    {
        return accessorText;
    }

    public String getCurrentAccessor()
    {
        return currentAccessor;
    }

    public String getNextAccessor()
    {
        currentAccessor = "";
        Matcher m = beanPattern.matcher(accessorText);
        if ( m.matches() )
        {
            currentAccessor = m.group(1);
            accessorText = m.group(2);
        }
        else if ( m != null )
        {
            currentAccessor = accessorText;
            accessorText = null;
        }
        return currentAccessor;
    }

    public boolean isValidScript()
    {
        return extractAccessors() != null;
    }

    public boolean hasRemainingAccessors()
    {
        return accessorText != null;
    }

    private String extractAccessors()
    {
        String accessors = null;
        Matcher m = getScriptPattern(beanChar).matcher(originalText);
        if ( m.matches() )
        {
            accessors = m.group(1);
        }
        return accessors;
    }

    public Object accessNextObject(Object o)
        throws ReflectionException
    {
        Object next = null;
        if ( o != null )
        {
            String accessor = getNextAccessor();
            String first = accessor.substring(0, 1);
            String remain = accessor.substring(1);
            String getaccessor = "get" + first.toUpperCase() + remain;
            String isaccessor = "is" + first.toUpperCase() + remain;
            Method m = null;

            try
            {
                m = o.getClass().getMethod(getaccessor, (Class[])null);
            }
            catch (NoSuchMethodException e)
            {
                try
                {
                    m = o.getClass().getMethod(isaccessor, (Class[])null);
                }
                catch (NoSuchMethodException ex)
                {
                    try
                    {
                        m = o.getClass().getMethod(accessor, (Class[])null);
                    }
                    catch (NoSuchMethodException exc)
                    {
                        String msg = "Unable to access either " + accessor + ", " + getaccessor + ", or " + isaccessor + " on object " + o.getClass();
                        throw new ReflectionException(e, msg);
                    }
                }
            }
            try
            {
                m.setAccessible(true);
                next = m.invoke(o, (Object[])null);
            }
            catch (IllegalAccessException e)
            {
                String msg = "Unable to access " + accessor + " on object " + o.getClass();
                throw new ReflectionException(e, msg);
            }
            catch (InvocationTargetException e)
            {
                throw new ReflectionException(e);
            }
        }
        else
        {
            throw new NullPointerException("Unable to access next object in bean script since object to act on is null");
        }

        return next;
    }

    public String toString()
    {
        return originalText + "|" + accessorText;
    }

    public static void main(String[] args)
    {
        try
        {
            
            Pattern equal = Pattern.compile("\\[^\\\\]=");
            String f = "s2.query.cards=SELECT ca.cust_id AS CUST_ID, c.custnum AS CUSTNUM, ca.cardnumber AS CARDNUMBER, ca.issue_number AS ISSUE_NUMBER, ca.card_idm AS CARD_IDM, ca.card_type AS CARD_TYPE, ca.cardname AS CARDNAME, ca.modifieddatetime AS MODIFIEDDATETIME FROM card ca INNER JOIN customer C ON C.cust_id\\=ca.cust_id WHERE ca.card_status\\=0 AND ca.cust_id IS NOT NULL AND C.is_active\\='T'";
            Matcher fm = equal.matcher(f);
            
            System.out.println(f + " matches? " + equal.pattern());
            System.out.println(fm.matches());
            
            System.exit(0);
    
            String l = "[A,Blah|R,Bleh]";
            System.out.println(l);
            Matcher lm = Pattern.compile("\\[?()\\]?").matcher(l);
            if ( lm.find() )
            {
                System.out.println(lm.group());
                System.out.println(lm.groupCount());
                System.out.println(lm.group(0));
            }
            
            String ss = "c\\:/etl/blob/images";
            System.out.println(ss);
            Pattern sp = Pattern.compile("^[A-Za-z]\\\\:.*$");
            System.out.println(ss + " == " + sp.pattern() + ": " + ss.matches(sp.pattern()));
            
            
            String e = "Error1Error2";
            String[] es = e.split("\\n");
            for ( String s: es )
            {
                System.out.println(s);
            }
            
            String t = "JDBC|BBTS29|com.blackboard.services.tibet.api.AccountType";
            String[] split = t.split("(-|\\|)");
            for ( String s: split )
            {
                System.out.println(s);
            }
            
            Pattern p1 = Pattern.compile("(-|\\|)");
            Matcher a1 = p1.matcher(t);
            if ( a1.find() )
            {
                System.out.println(a1.group());
            }
            
            Pattern p = Pattern.compile("(\\$\\{.+?\\})");
            Matcher a = p.matcher("${institution.institutionId");

            java.util.ArrayList matches = new java.util.ArrayList();
            while ( a.find() )
            {
                matches.add(a.group(1));
            }
            System.out.println(a.matches());
            System.out.println(matches);
            System.out.println(a.toMatchResult());

            p = Pattern.compile("(.*?)\\..*");
            a = p.matcher("Test-1.xml");
            matches = new java.util.ArrayList();
            while ( a.find() )
            {
                matches.add(a.group(1));
            }
            System.out.println(a.matches());
            System.out.println(matches);
            System.out.println(a.toMatchResult());
            
            System.exit(1);
            
            java.util.HashMap m = new java.util.HashMap();
            BeanScript bs = new BeanScript(" ${class.name}");
            System.out.println("valid ? " + bs.isValidScript());
            Object o = m;
            while ( bs.hasRemainingAccessors() )
            {
                o = bs.accessNextObject(o);
                System.out.println(bs.getCurrentAccessor());
            }
            
            java.util.HashMap map = new java.util.HashMap();
            map.put("aramark.client.institution", "institution");
            map.put("aramark.client.server", "server");
            
            a = embeddedPattern.matcher("${aramark.client.institution}-${aramark.client.server}.locations.xml");
            matches = new java.util.ArrayList();
            java.util.ArrayList singles = new java.util.ArrayList();
            while ( a.find() )
            {
                matches.add(a.group(1));
                singles.add(new VariableReplacement(a.start(), a.end(), a.group(1), map, false));
                //Matcher a2 = singlePattern.matcher(a.group(1));
                //if ( a2.find() )
                //{
                //    singles.add(a2.group(1));
                //}
            }
            System.out.println(a.matches());
            System.out.println(matches);
            System.out.println(singles);
            System.out.println(a.toMatchResult());
            
        }
        catch (Exception e) {e.printStackTrace();}
    }
}
