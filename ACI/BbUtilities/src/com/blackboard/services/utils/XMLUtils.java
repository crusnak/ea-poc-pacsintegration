/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.utils;

import com.sun.org.apache.xerces.internal.dom.DocumentImpl;
import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PushbackInputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.json.simple.JSONObject;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author crusnak
 */
public class XMLUtils
{
    public static Document parseXML(String xml)
        throws IOException, SAXException
    {
        DOMParser parser = new DOMParser();
        parser.parse(new InputSource(new StringReader(xml)));
        return parser.getDocument();
    }
    
    public static Document parseXML(File xml)
        throws IOException, SAXException
    {
        DOMParser parser = new DOMParser();
        parser.parse(new InputSource(checkForBOM(new FileInputStream(xml))));
        return parser.getDocument();
    }
    
    public static Document parseXML(Element e)
        throws IOException, SAXException, ParserConfigurationException
    {
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        Node n = doc.importNode(e, true);
        doc.appendChild(n);
        return doc;
    }
    
    public static Document parseJSON(String json)
        throws ParserConfigurationException
    {
        return parseJSON(new BbJSONObject(json));
    }
    
    public static Document parseJSON(String json, String rootName)
        throws ParserConfigurationException
    {
        System.out.println("parseJSON(" + rootName + ")");
        return parseJSON(new BbJSONObject(json, rootName));
    }
    
    public static Document parseJSON(JSONObject json)
        throws ParserConfigurationException
    {
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        Element root = doc.createElement("json");
        doc.appendChild(root);
        
        // Iterate over all root json elements
        Iterator<Map.Entry<String,Object>> it = json.entrySet().iterator();
        while ( it.hasNext() )
        {
            Map.Entry<String,Object> entry = it.next();
            if ( entry.getValue() instanceof ArrayList )
            {
                ArrayList array = (ArrayList)entry.getValue();
                for ( Object o: array )
                {
                    if ( o instanceof BbJSONObject )
                    {
                        Node n = doc.importNode(parseJSON((BbJSONObject)o, entry.getKey()).getDocumentElement(), true);
                        root.appendChild(n);
                    }
                    else
                    {
                        throw new ParserConfigurationException("Invalid array object: " + o.getClass());
                    }
                }
            }
            else if ( entry.getValue() instanceof BbJSONObject )
            {
                BbJSONObject bbjson = (BbJSONObject)entry.getValue();
                Node n = doc.importNode(parseJSON((BbJSONObject)entry.getValue(), entry.getKey()).getDocumentElement(), true);
                root.appendChild(n);
            }
            else
            {
                Element e = doc.createElement(entry.getKey());
                if ( entry.getValue() != null )
                {
                    e.setTextContent(String.valueOf(entry.getValue()));
                }
                root.appendChild(e);
            }
        }
        
        return doc;
    }
    
    public static Document parseJSON(JSONObject json, String rootName)
        throws ParserConfigurationException
    {
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        Element root = doc.createElement(rootName);
        doc.appendChild(root);
        
        // Iterate over all root json elements
        Iterator<Map.Entry<String,Object>> it = json.entrySet().iterator();
        while ( it.hasNext() )
        {
            Map.Entry<String,Object> entry = it.next();
            if ( entry.getValue() instanceof ArrayList )
            {
                ArrayList array = (ArrayList)entry.getValue();
                for ( Object o: array )
                {
                    if ( o instanceof BbJSONObject )
                    {
                        Node n = doc.importNode(parseJSON((BbJSONObject)o, entry.getKey()).getDocumentElement(), true);
                        root.appendChild(n);
                    }
                    else
                    {
                        throw new ParserConfigurationException("Invalid array object: " + o.getClass());
                    }
                }
            }
            else if ( entry.getValue() instanceof BbJSONObject )
            {
                BbJSONObject bbjson = (BbJSONObject)entry.getValue();
                Node n = doc.importNode(parseJSON((BbJSONObject)entry.getValue(), entry.getKey()).getDocumentElement(), true);
                root.appendChild(n);
            }
            else
            {
                Element e = doc.createElement(entry.getKey());
                if ( entry.getValue() != null )
                {
                    e.setTextContent(String.valueOf(entry.getValue()));
                }
                root.appendChild(e);
            }
        }
        
        return doc;
    }
    
    /**
     * Recursively renames the namespace of a node.
     * @param node the starting node.
     * @param namespace the new namespace. Supplying <tt>null</tt> removes the namespace.
     */
    public static void renameNamespaceRecursive(Node node, String namespace) {
        Document document = node.getOwnerDocument();
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            document.renameNode(node, namespace, node.getNodeName());
        }
        NodeList list = node.getChildNodes();
        for (int i = 0; i < list.getLength(); ++i) {
            renameNamespaceRecursive(list.item(i), namespace);
        }
    }

private static InputStream checkForBOM(InputStream inputStream) 
        throws IOException 
    {
        PushbackInputStream pushbackInputStream = new PushbackInputStream(new BufferedInputStream(inputStream), 3);
        byte[] bom = new byte[2];
        if (pushbackInputStream.read(bom) != -1) 
        {
            if (!(bom[0] == (byte) 0xEF && bom[1] == (byte) 0xBB)) 
            {
                byte[] bom2 = new byte[1];
                byte[] bomutf = new byte[3];
                if ( pushbackInputStream.read(bom2) != -1 && bom[0] != (byte) 0xBF )
                {
                    System.arraycopy(bom, 0, bomutf, 0, 2);
                    System.arraycopy(bom2, 0, bomutf, 2, 1);
                    pushbackInputStream.unread(bomutf);
                }
            }
            else if (!(bom[0] == (byte) 0xFE && bom[1] == (byte) 0xFF))
            {
                pushbackInputStream.unread(bom);
            }
        }
        return pushbackInputStream;
    }    

    public static Element getXPathElement(String xpath, Element root)
    {
        Element e = null;
        
        return e;
    }
    
    public static Node evaluateSimpleXpath(String xpath, Element root)
    {
        
        Node node = null;
        Element current = root;
        if ( xpath != null )
        {
            String[] paths = xpath.split("/");
            for ( int i=0; i<paths.length; i++ )
            {
                if ( current != null )
                {
                    if ( i == 0 && paths[i].trim().length() == 0 )
                    {
                        continue;
                    }
                    else if ( i == paths.length-1 && paths[i].trim().length() == 0 )
                    {
                        if ( current.getChildNodes().getLength() == 1 )
                        {
                            node = current.getFirstChild();
                        }
                        else
                        {
                            node = current;
                        }
                    }
                    else
                    {
                        Node n = getChildNode(paths[i], current);
                        if ( n != null )
                        {
                            if ( n.getNodeType() != Node.ELEMENT_NODE )
                            {
                                node = n;
                                break;
                            }
                            else
                            {
                                current = (Element)n;
                                if ( i == paths.length-1 )
                                {
                                    if ( current.getChildNodes().getLength() == 1 )
                                    {
                                        node = current.getFirstChild();
                                    }
                                    else
                                    {
                                        node = current;
                                    }
                                }
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                else
                {
                    break;
                }
            }
        }
        
        return node;
    }
    
    public static Node getChildNode(String child, Element root)
    {
        Node node = null;
        if ( child != null )
        {
            for ( int i=0; i<root.getChildNodes().getLength(); i++ )
            {
                Node n = root.getChildNodes().item(i);
                if ( child.equals(n.getNodeName()) )
                {
                    node = n;
                }
            }
        }
        return node;
    }
    
    public static Attr getAttributeNode(String child, Element root)
    {
        Attr attr = null;
        
        return attr;
    }
    
    public static Node getChildTextNode(String child, Element root)
    {
        Node text = null;
        if ( child != null )
        {
            for ( int i=0; i<root.getChildNodes().getLength(); i++ )
            {
                Node n = root.getChildNodes().item(i);
                if ( child.equals(n.getNodeName()) && n.getNodeType() == Node.TEXT_NODE )
                {
                    text = n;
                }
            }
        }
        return text;
    }
    
    public static Element getFirstChildElement(Node n)
    {
        Element e = null;
        
        if ( n != null )
        {
            for ( int i=0; i<n.getChildNodes().getLength(); i++ )
            {
                Node child = n.getChildNodes().item(i);
                if ( child.getNodeType() == Node.ELEMENT_NODE )
                {
                    e = (Element)child;
                    break;
                }
            }
        }        
        return e;
    }
    
    public static boolean hasChildElements(Node n)
    {
        boolean found = false;
        
        if ( n != null )
        {
            for ( int i=0; i<n.getChildNodes().getLength(); i++ )
            {
                Node child = n.getChildNodes().item(i);
                if ( child.getNodeType() == Node.ELEMENT_NODE )
                {
                    found = true;
                    break;
                }
            }
        }
        return found;
    }
    
    public static List<Element> getChildElements(Node n)
    {
        ArrayList<Element> elements = new ArrayList();
        
        if ( n != null )
        {
            for ( int i=0; i<n.getChildNodes().getLength(); i++ )
            {
                Node child = n.getChildNodes().item(i);
                if ( child.getNodeType() == Node.ELEMENT_NODE )
                {
                    elements.add((Element)child);
                }
            }
        }
        
        return elements;
    }
    
    public static String toString(Node node, boolean indent)
        throws RuntimeException
    {
        StringWriter out = new StringWriter();
        try
        {
            Transformer trans = TransformerFactory.newInstance().newTransformer();
            trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            trans.setOutputProperty(OutputKeys.INDENT, (indent?"yes":"no"));

            trans.transform(new DOMSource(node), new StreamResult(out));
            String s = out.toString();
            if ( !indent )
            {   
                s = s.replaceAll("\\r\\n", "");
                s = s.replaceAll("\\s+", " ");
            }
            return s;
        }
        catch (TransformerException e)
        {
            throw new RuntimeException("Failed to transform document fragment for output", e);
        }
    }
    
    public static String toString(Document doc, boolean indent) {
        try {
            StringWriter sw = new StringWriter();
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.INDENT, (indent?"yes":"no"));
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

            transformer.transform(new DOMSource(doc), new StreamResult(sw));
            String s = sw.toString();
            if ( !indent )
            {   
                s = s.replaceAll("\\r\\n", "");
                s = s.replaceAll("\\s+", " ");
            }
            return s;
        } catch (Exception ex) {
            throw new RuntimeException("Error converting to String", ex);
        }
    }

    public static void main(String[] args)
    {
        try
        {
            /*String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><EngineDocList> <DocVersion DataType=\"String\">1.0</DocVersion> <EngineDoc>  <ContentType DataType=\"String\">OrderFormDoc</ContentType>  <DocumentId DataType=\"String\">4e457e69-0505-3000-003c-0003ba9896f7</DocumentId>  <IPAddress DataType=\"String\">127.0.0.1</IPAddress>  <Instructions>   <Pipeline DataType=\"String\">Payment</Pipeline>  </Instructions>  <MessageList>   <MaxSev DataType=\"S32\">6</MaxSev>   <Message>    <AdvisedAction DataType=\"S32\">16</AdvisedAction>    <Audience DataType=\"String\">Merchant</Audience>    <Component DataType=\"String\">CcxOcc</Component>    <ContextId DataType=\"String\">PaymentDba</ContextId>    <DataState DataType=\"S32\">3</DataState>    <FileLine DataType=\"S32\">545</FileLine>    <FileName DataType=\"String\">CcxOccExecute.cpp</FileName>    <FileTime DataType=\"String\">05:26:46Jul 26 2011</FileTime>    <ResourceId DataType=\"S32\">2</ResourceId>    <Sev DataType=\"S32\">6</Sev>    <Text DataType=\"String\">Did not find a unique, qualifying transaction for Order &apos;18258E92-FDA9-423B-95A8-92D898D8FE4B&apos;.</Text>   </Message>  </MessageList>  <OrderFormDoc>   <Consumer>    <PaymentMech>     <Type DataType=\"String\">CreditCard</Type>    </PaymentMech>   </Consumer>   <DateTime DataType=\"DateTime\">1313180043475</DateTime>   <Id DataType=\"String\">18258E92-FDA9-423B-95A8-92D898D8FE4B</Id>   <Mode DataType=\"String\">Y</Mode>   <Transaction>    <Type DataType=\"String\">Void</Type>   </Transaction>  </OrderFormDoc>  <Overview>   <CcErrCode DataType=\"S32\">1067</CcErrCode>   <CcReturnMsg DataType=\"String\">System error.</CcReturnMsg>   <DateTime DataType=\"DateTime\">1313180043475</DateTime>   <Mode DataType=\"String\">Y</Mode>   <Notice DataType=\"String\">Did not find a unique, qualifying transaction for Order &apos;18258E92-FDA9-423B-95A8-92D898D8FE4B&apos;.</Notice>   <OrderId DataType=\"String\">18258E92-FDA9-423B-95A8-92D898D8FE4B</OrderId>   <TransactionStatus DataType=\"String\">E</TransactionStatus>  </Overview>  <SourceId DataType=\"String\">Online Meal Plan Store - Blackboard Transact Consulting - Void Request</SourceId>  <User>   <Alias DataType=\"String\">BB_SANDBOX_STORE</Alias>   <ClientId DataType=\"S32\">8051</ClientId>   <EffectiveAlias DataType=\"String\">BB_SANDBOX_STORE</EffectiveAlias>   <EffectiveClientId DataType=\"S32\">8051</EffectiveClientId>   <Name DataType=\"String\">testorder</Name>   <Password DataType=\"String\">XXXXXXX</Password>  </User> </EngineDoc> <TimeIn DataType=\"DateTime\">1313180043462</TimeIn> <TimeOut DataType=\"DateTime\">1313180043626</TimeOut></EngineDocList>";
            Document doc = parseXML(xml);
            Node node = evaluateSimpleXpath("/EngineDoc/Overview/", doc.getDocumentElement());
            System.out.println(node);*/
            
            String json = "{\"CUST_ID\":79,\"CUSTNUM\":\"0000000000000000000005\",\"FIRSTNAME\":\"John\",\"MIDDLENAME\":null,\"LASTNAME\":\"Doe5\",\"BIRTHDATE\":null,\"SEX\":\"Undefined\",\"PINNUMBER\":null,\"IS_ACTIVE\":true,\"ACTIVE_START_DATE\":\"12-30-2010\",\"ACTIVE_END_DATE\":\"12-31-2015\",\"OPENDATETIME\":\"05-10-2010 18:50:47\",\"LASTMOD_DATETIME\":\"05-29-2015 10:38:46\",\"LASTMOD_USERNAME\":\"administrator\",\"CARD\":[{\"CUST_ID\":79,\"CARDNUM\":\"0000000000000001624225\",\"ISSUE_NUMBER\":null,\"CARD_STATUS_TEXT\":null,\"CARD_STATUS_DATETIME\":\"05-10-2010 18:50:47\",\"CARD_STATUS\":\"Active\",\"CARD_TYPE\":\"Standard\",\"PRIMARY\":true}, {\"CUST_ID\":79,\"CARDNUM\":\"0000000000000001624223\",\"ISSUE_NUMBER\":null,\"CARD_STATUS_TEXT\":null,\"CARD_STATUS_DATETIME\":\"05-10-2010 18:50:47\",\"CARD_STATUS\":\"Active\",\"CARD_TYPE\":\"Standard\",\"PRIMARY\":false}]}";
            XMLUtils.parseJSON(json);
            
            System.exit(1);
            
            String[] cmd = { "c:/etl/sonoma/bin/runETLJob.bat", "post-transactions" };
            //String[] cmd = { "javac" };
            //Process p = Runtime.getRuntime().exec(cmd);
            //Process p = Runtime.getRuntime().exec("c:/etl/sonoma/bin/runETLJob.bat post-transactions");
            Process p = Runtime.getRuntime().exec("C:\\oracle\\product\\11.2.0\\dbhome_1\\BIN\\launch.exe");
            BufferedReader stderr = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            BufferedReader stdout = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedWriter stdin = new BufferedWriter(new OutputStreamWriter(p.getOutputStream()));
            
            try
            {
                String line = null;
                System.out.println("<STDERR>");
                while ( (line = stderr.readLine()) != null )
                {
                    System.out.println(line);
                }
                System.out.println("<STDERR>");

                line = null;
                System.out.println("<STDOUT>");
                while ( (line = stdout.readLine()) != null )
                {
                    System.out.println(line);
                }
                System.out.println("<STDOUT>");
            }
            catch (IOException e)
            {
                System.err.println("Failed to read executed command output");
                e.printStackTrace();
                System.exit(0);
            }
            
            int val = p.waitFor();
            System.out.println("ExitValue: " + val);
        }
        catch (Throwable e)
        {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
