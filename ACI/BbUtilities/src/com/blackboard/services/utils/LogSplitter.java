/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author crusnak
 */
public class LogSplitter
{
    public static void split(File f, long maxbytes, boolean useDates)
        throws IOException
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
        File out = null;
        long bytes = 0;
        int index = 1;
        String line = "";
        Pattern p1 = Pattern.compile("([0-9]{4})-([0-9]{2})-([0-9]{2})");
        Pattern p2 = Pattern.compile("([0-9]{4})/([0-9]{2})/([0-9]{2})");
        BufferedWriter bw = null;
        while ( (line = br.readLine()) != null )
        {
            if ( bytes == 0 )
            {
                if ( bw != null )
                {
                    bw.close();
                }

                Matcher m1 = p1.matcher(line);
                Matcher m2 = p2.matcher(line);
                if ( useDates && m1.find() )
                {
                    String year = m1.group(1);
                    String month = m1.group(2);
                    String day = m1.group(3);
                    out = new File(f.getPath() + "." + year + month + day);
                    bw = new BufferedWriter(new FileWriter(out));
                }
                else if ( useDates && m2.find() )
                {
                    String year = m2.group(1);
                    String month = m2.group(2);
                    String day = m2.group(3);
                    out = new File(f.getPath() + "." + year + month + day);
                    bw = new BufferedWriter(new FileWriter(out));
                }
                else
                {
                    out = new File(f.getPath() + "." + index++);
                    bw = new BufferedWriter(new FileWriter(out));
                }

                System.out.println("Outputing to new file " + out.getCanonicalPath());
            }

            bytes += line.getBytes().length + 1;
            bw.write(line);
            bw.newLine();

            if ( bytes > maxbytes )
            {
                System.out.println("Wrote " + bytes + " bytes to " + out.getName());
                bytes = 0;
            }
        }
    }

    public static void main(String[] args)
    {
        try
        {
            if ( args.length != 2 && args.length != 3 )
            {
                throw new IllegalArgumentException("Usage: LogSplitter <logfile> <max size in Mb> <use date extensions>?");
            }
            else
            {
                File logfile = new File(args[0]);
                long size = Long.parseLong(args[1]);
                boolean useDates = (args.length == 3 ? Boolean.valueOf(args[2]):false);
                size = size * 1024 * 1024;

                System.out.println(logfile.getCanonicalPath());
                System.out.println("logfile size in bytes=" + size);
                if ( !logfile.exists() )
                {
                    throw new FileNotFoundException(logfile.getCanonicalPath());
                }
                else
                {
                    split(logfile, size, useDates);
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.exit(-1);
        }
    }
}
