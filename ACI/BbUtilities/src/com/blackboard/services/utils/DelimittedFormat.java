/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author crusnak
 */
public class DelimittedFormat implements TextFormat
{
    private ArrayList<Field> fields = new ArrayList();
    private char delimitter = '\u0000';
    private boolean appendDelimitter = false;
    private boolean retainQuoted = false;

    public DelimittedFormat(char d)
    {
        this(d, false);
    }

    public DelimittedFormat(char d, boolean endWithDelimitter)
    {
        delimitter = d;
        appendDelimitter = endWithDelimitter;
    }

    public void setFields(List<Field> f)
    {
        fields = new ArrayList(f);
    }

    public void addFields(List<Field> f)
    {
        fields.addAll(f);
    }

    public void addField(Field f)
    {
        fields.add(f);
    }

    /**
     * Add a new DelimittedFormat.Field.
     * @param name The name of the field.
     * @param len The maximum length of the field, -1 for no limit.
     * @param empty True to allow empty field values.
     * @param pad True to pad with leading zeroes to the max length. No relevance when max length is -1.
     * @see Field
     */
    public void addField(String name, int len, boolean empty, boolean pad, String dataType)
    {
        fields.add(new Field(name, len, empty, pad, dataType));
    }

    /**
     * Add a new DelimittedFormat.Field.
     * @param name The name of the field.
     * @param len The maximum length of the field, -1 for no limit.
     * @param empty True to allow empty field values.
     * @param pad True to pad with leading zeroes to the max length. No relevance when max length is -1.
     * @param constant True to indicate this is a constant field and will always equal the field name as the value.
     * @see Field
     */
    public void addField(String name, int len, boolean empty, boolean pad, boolean constant, String dataType)
    {
        fields.add(new Field(name, len, empty, pad, constant, dataType));
    }

    public Field getFieldByName(String name)
    {
        Field field = null;
        for ( Field f: fields )
        {
            if ( f.getName().equals(name) )
            {
                field = f;
                break;
            }
        }
        return field;
    }

    public void resetFields()
    {
        fields.clear();
    }

    public boolean removeField(Field f)
    {
        return fields.remove(f);
    }

    public List<Field> getFields()
    {
        return fields;
    }

    public List<Field> getSimpleFields()
    {
        return fields;
    }

    public boolean isAppendDelimitter()
    {
        return appendDelimitter;
    }

    public void setAppendDelimitter(boolean appendDelimitter)
    {
        this.appendDelimitter = appendDelimitter;
    }

    public boolean isRetainQuoted()
    {
        return retainQuoted;
    }

    /** 
     * If setting to TRUE, the delimiter value is ignored on process and it's separating between quoted text
     * @param retain
    */
    public void setRetainQuoted(boolean retain)
    {
        this.retainQuoted = retain;
    }

    public char getDelimitter()
    {
        return delimitter;
    }

    public void setDelimitter(char delimitter)
    {
        this.delimitter = delimitter;
    }

    public String[] readHeader(String line)
    {
        return splitLineByDelimitter(line);
    }

    private String[] splitLineByDelimitter(String line)
    {
        String[] split = new String[0];
        if ( line != null && !retainQuoted )
        {
            split = line.split("\\" + String.valueOf(delimitter));
        }
        else if ( line != null )
        {
            ArrayList<String> tokens = new ArrayList();
            String regex = "\\\".*?\\\"";
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(line);
            while ( m.find() )
            {
                tokens.add(m.group());
            }
            
            split = tokens.toArray(new String[0]);
        }
        return split;
    }

    public Map<String,String> processLine(String line)
        throws TextFormatException
    {
        LinkedHashMap<String,String> map = new LinkedHashMap();

        try
        {
            if ( line != null )
            {
                String[] ary = splitLineByDelimitter(line);
                for ( int i=0; i<fields.size(); i++ )
                {
                    Field f = fields.get(i);
                    if ( i < ary.length )
                    {
                        String val = ary[i];
                        if ( f.getMaxLength() != -1 && ary[i].length() > f.getMaxLength() )
                        {
                            val = ary[i].substring(0, f.getMaxLength());
                        }

                        if ( f.isPadWithLeadingZeroes() && f.getMaxLength() != -1 )
                        {
                            while ( val.length() < f.getMaxLength() )
                            {
                                val = '0' + val;
                            }
                            
                            // I have seriously no idea what I was smoking when I wrote this. 
                            // Nothing is happening here
                            /**int j = 0;
                            for ( j=0; j<val.toCharArray().length; j++ )
                            {
                                if ( val.toCharArray()[j] == '0' && j == val.toCharArray().length-1 )
                                {
                                    break;
                                }
                                if ( val.toCharArray()[j] != '0' )
                                {
                                    break;
                                }
                            }

                            val = ary[i].substring(j);*/
                        }

                        map.put(f.getName(), val.trim());
                    }
                    else
                    {
                        map.put(f.getName(), "");
                    }
                }
            }
        }
        catch (Exception e)
        {
            throw new TextFormatException(this, "Unable to process line: " + line, e);
        }

        return map;
    }

    public String formatLine(List<String> values)
        throws TextFormatException
    {
        return formatLine(values.toArray(new String[0]), false);
    }

    public String formatLine(List<String> values, boolean suppress)
        throws TextFormatException
    {
        return formatLine(values.toArray(new String[0]), suppress);
    }

    public String formatLine(String[] values)
        throws TextFormatException
    {
        return formatLine(values, false);
    }

    public String formatLine(Map<String,String> map)
        throws TextFormatException
    {
        ArrayList<String> values = new ArrayList();
        for ( Field f: fields )
        {
            values.add(map.get(f.getName()));
        }

        return formatLine(values);
    }

    public String formatLine(String[] values, boolean suppress)
        throws TextFormatException
    {
        String line = "";

        try
        {
            // Strip out any carriage returns as this will break the file format
            for ( int i=0; i<values.length; i++ )
            {
                if ( values[i] != null )
                {
                    values[i] = values[i].replaceAll("\\n", "");
                    values[i] = values[i].replaceAll("\\r", "");
                }
            }

            for ( int i=0; i<fields.size(); i++ )
            {
                String current = "";
                Field f = fields.get(i);
                if ( i < fields.size()-1 )
                {
                    if ( i < values.length && values[i] != null )
                    {
                        current = values[i];
                        if ( f.getMaxLength() != -1 && current.length() > f.getMaxLength() )
                        {
                            current = current.substring(0, f.getMaxLength());
                        }
                        else if ( f.isPadWithLeadingZeroes() && current.length() < f.getMaxLength() )
                        {
                            for ( int j=0; current.length()<f.getMaxLength(); j++ )
                            {
                                current = "0" + current;
                            }
                        }
                    }
                    else if ( !f.isEmptyAllowed() && !suppress )
                    {
                        throw new IllegalArgumentException("Cannot format data for non empty field: " + f);
                    }

                    current += delimitter;
                }
                else
                {
                    if ( i < values.length && values[i] != null )
                    {
                        current += values[i];
                        if ( f.isPadWithLeadingZeroes() && current.length() < f.getMaxLength() )
                        {
                            for ( int j=0; current.length()<f.getMaxLength(); j++ )
                            {
                                current = "0" + current;
                            }
                        }
                    }
                    else if ( !f.isEmptyAllowed() && !suppress )
                    {
                        throw new IllegalArgumentException("Cannot format data for non empty field: " + f);
                    }

                    if ( appendDelimitter )
                    {
                        current += delimitter;
                    }
                }

                //System.out.println(f + ": " + current);
                line += current;
            }
        }
        catch (Exception e)
        {
            ArrayList<String> vals = new ArrayList();
            for ( String v: values )
            {
                vals.add(v);
            }

            throw new TextFormatException(this, "Unable to format line with: " + vals, e);
        }

        return line;
    }

    @Override
    public String toString()
    {
        StringBuilder out = new StringBuilder();
        out.append("{");
        out.append("Delimiter=" + delimitter);
        out.append(", Fields=" + fields);
        out.append("}");
        return out.toString();
    }

    public static class Field implements TextFormat.Field
    {
        private String name = "";
        private int maxLength = -1;
        private boolean emptyAllowed = false;
        private boolean padWithLeadingZeroes = false;
        private boolean constant = false;
        private String dataType = null;

        public Field() {}

        public Field(String n, int l, boolean e, boolean p, String dt)
        {
            name = n;
            maxLength = l;
            emptyAllowed = e;
            padWithLeadingZeroes = p;
            dataType = dt;
        }

        public Field(String n, int l, boolean e, boolean p, boolean c, String dt)
        {
            this(n, l, e, p, dt);
            constant = c;
        }

        public boolean isEmptyAllowed()
        {
            return emptyAllowed;
        }

        public void setEmptyAllowed(boolean emptyAllowed)
        {
            this.emptyAllowed = emptyAllowed;
        }

        public int getMaxLength()
        {
            return maxLength;
        }

        public void setMaxLength(int maxLength)
        {
            this.maxLength = maxLength;
        }

        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public boolean isPadWithLeadingZeroes()
        {
            return padWithLeadingZeroes;
        }

        public void setPadWithLeadingZeroes(boolean padWithLeadingZeroes)
        {
            this.padWithLeadingZeroes = padWithLeadingZeroes;
        }
        
        public boolean isConstant()
        {
            return constant;
        }
        
        public String getDataType()
        {
            return dataType;
        }
        
        public void setDataType(String dt)
        {
            dataType = dt;
        }

        @Override
        public String toString()
        {
            return name + "([" + dataType + "] " + maxLength + "," + emptyAllowed + "," + padWithLeadingZeroes + ")";
        }
    }

    public static void main(String[] args)
    {
        String line = "\"first, last\",\"hard\",\"a,b,c\",\"aaee33\"";
        /*String regex = "\\\".*?\\\"\\" + String.valueOf(",") + "*";
        System.out.println(line);
        System.out.println(regex);
        
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(line);
        
        while ( m.find() )
        {
            System.out.println(m.group());
        }*/
        
        DelimittedFormat format = new DelimittedFormat(',');
        format.setRetainQuoted(true);
        format.addField("Column1", -1, false, false, "String");
        format.addField("Column2", -1, false, false, "String");
        format.addField("Column3", -1, false, false, "String");
        format.addField("Column4", -1, false, false, "String");

        try
        {
            //String[] fields = { "0088", "1", "2345", "3333", "0060", "2", "1", "000004", "2",
            //                    "20040219144203", "T", "125", "USD", "F", "566010004852620", "0", "0" };
            //String line = format.formatLine(fields);
            System.out.println(line);
            Map processed = format.processLine(line);
            System.out.println(processed);

            String orig = "0088~1~2345~3333~0060~2~1~000004~2~20040219144203~T~125~USD~F~566010004852620~0~0~   ";
            System.out.println(orig);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
