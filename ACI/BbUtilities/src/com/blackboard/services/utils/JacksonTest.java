/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.InvalidPathException;
import java.util.Iterator;

/**
 *
 * @author crusnak
 */
public class JacksonTest 
{
    public static final char PATH_SEPARATOR =          '/';
    private static final ObjectMapper mapper = new ObjectMapper();

    String json;
    JsonNode rootNode;
    
    public JacksonTest(String j)
        throws IOException
    {
        json = j;
        rootNode = mapper.readTree(json);
    }
    
    public JacksonTest(File f)
        throws IOException
    {
        rootNode = mapper.readTree(f);
        json = mapper.writeValueAsString(rootNode);
    }

    public void setFieldValue(String nodepath, Object value)
       throws InvalidPathException
    {
        if ( nodepath == null || nodepath.trim().length() == 0 || nodepath.trim().charAt(0) != PATH_SEPARATOR )
        {
            throw new InvalidPathException(nodepath, "Invalid path. Paths must start with " + PATH_SEPARATOR);
        }

        JsonNode source = getParentFieldValue(nodepath);
        String name = nodepath.substring(nodepath.lastIndexOf(PATH_SEPARATOR) + 1);
        if ( source.getNodeType() == JsonNodeType.OBJECT )
        {
            setField((ObjectNode)source, name, value);
        }
        else
        {
            throw new IllegalArgumentException("Unsupported parent source json object found via nodepath: " + nodepath + ": " + source.getClass().getName());
        }
    }
    
    private void setField(ObjectNode node, String name, Object val)
    {
        if ( val == null )
        {
            node.put(name, (Integer)null);
        }
        else if ( val instanceof Integer )
        {
            node.put(name, (Integer)val);
        }
        else if ( val instanceof Float )
        {
            node.put(name, (Float)val);
        }
        else if ( val instanceof Double )
        {
            node.put(name, (Double)val);
        }
        else if ( val instanceof Long )
        {
            node.put(name, (Long)val);
        }
        else if ( val instanceof Short )
        {
            node.put(name, (Short)val);
        }
        else if ( val instanceof Boolean )
        {
            node.put(name, (Boolean)val);
        }
        else if ( val instanceof BigInteger )
        {
            node.put(name, (BigInteger)val);
        }
        else if ( val instanceof BigDecimal )
        {
            node.put(name, (BigDecimal)val);
        }
        else if ( val instanceof JacksonTest )
        {
            node.set(name, ((JacksonTest)val).rootNode);
        }
        else if ( val instanceof JsonNode )
        {
            node.set(name, (JsonNode)val);
        }
        else
        {
            node.put(name, String.valueOf(val));
        }
    }
    
    public JsonNode getFieldValue(String nodepath)
       throws InvalidPathException
    {
        JsonNode node = rootNode.at(nodepath);
        if ( node.getNodeType() == JsonNodeType.MISSING )
        {
            throw new InvalidPathException(this.rootNode.toString(), "Cannot get json node from source with path: " + nodepath);
        }
        
        return node;
    }
    
    private JsonNode getParentFieldValue(String nodepath)
        throws InvalidPathException
    {
        //System.out.println("Processing: '" + nodepath + "'");
        
        if ( nodepath == null || nodepath.trim().length() == 0 || nodepath.trim().charAt(0) != PATH_SEPARATOR )
        {
            throw new InvalidPathException(nodepath, "Invalid path. Paths must start with " + PATH_SEPARATOR);
        }

        // Remove everything after last path separator
        if ( nodepath.lastIndexOf(PATH_SEPARATOR) > 0 )
        {
            nodepath = nodepath.substring(0, nodepath.lastIndexOf(PATH_SEPARATOR));
        }
        else
        {
            nodepath = "";
        }
        
        if ( String.valueOf(PATH_SEPARATOR).equals(nodepath) )
        {
            nodepath = "";
        }
        
        return getFieldValue(nodepath);
    }
    
    
    public static void main(String[] args)
    {
        try
        {
            JacksonTest jt = new JacksonTest(new File("I:\\Git\\bb-custom-all\\BbETL\\Mainline\\BbUtilities\\json.txt"));
            System.out.println("json: " + jt.json + "(" + jt.json.getClass().getName() + ")");
            System.out.println("node: " + jt.rootNode + "(" + jt.rootNode.getClass().getName() + ")");
            
            String access = "{\"CardholderId\":\"33200bfe-8918-406d-9ab3-1c95d64396e4\",\"LastName\":\"Allen\",\"FirstName\":\"Josh\",\"MiddleInitial\":\"y\",\"CompanyId\":null,\"MemberOfAllSites\":true,\"Notes\":null,\"CardholderStatus\":1,\"CardholderActiveDate\":null,\"CardholderExpireDate\":null,\"LastCardholderModRowVersion\":\"0x00000000000036BD\",\"LastModified\":\"2021-08-17T00:03:30.253\",\"Images\":[],\"CardholderAccessLevels\":[{\"CardholderAccessLevelID\":\"4e43d5cc-f23b-4d51-969c-0a0b90861f86\",\"CardholderID\":\"33200bfe-8918-406d-9ab3-1c95d64396e4\",\"AccessLevelID\":\"df589e20-842e-417b-9600-4aaad9e0de1b\",\"LastModified\":\"2021-08-17T00:03:30.257\",\"ActivateDate\":null,\"DeactivateDate\":null},{\"CardholderAccessLevelID\":\"2d43b4cf-9c48-4b91-bed4-ec48df249250\",\"CardholderID\":\"33200bfe-8918-406d-9ab3-1c95d64396e4\",\"AccessLevelID\":\"58d7ab01-7535-4aab-bcb9-34025f92c7c3\",\"LastModified\":\"2021-08-16T17:34:29.267\",\"ActivateDate\":null,\"DeactivateDate\":null},{\"CardholderID\":\"33200bfe-8918-406d-9ab3-1c95d64396e4\",\"AccessLevelID\":\"0abd8860-38a4-4adc-9835-812b74fb442f\",\"LastModified\":\"2021-07-14T22:44:31.638Z\",\"ActivateDate\":\"2019-02-01T08:00:00Z\",\"DeactivateDate\":\"2019-05-15T17:00:00Z\"}],\"CardholderSites\":[],\"CardholderCards\":[{\"CardId\":\"a7d3672a-bef0-47b2-8eba-b5b4312952a9\",\"CardholderID\":\"33200bfe-8918-406d-9ab3-1c95d64396e4\",\"CardStatus\":1,\"LastModified\":\"2021-08-17T00:03:30.26\",\"ActiveDate\":\"1899-12-30T07:00:00\",\"ExpireDate\":\"2173-10-13T07:00:00\"}],\"UserColumns\":{\"UserText1\":\"0000000000000000800017\",\"UserText2\":\"c5e03480-5d9d-464b-9f28-808311bd83fb\",\"UserText3\":null,\"UserText4\":null,\"UserText5\":null,\"UserText6\":null,\"UserText7\":null,\"UserText8\":null,\"UserText9\":null,\"UserText10\":null,\"UserText11\":null,\"UserText12\":null,\"UserText13\":null,\"UserText14\":null,\"UserText15\":null,\"UserText16\":null,\"UserText17\":null,\"UserText18\":null,\"UserText19\":null,\"UserText20\":null,\"Department\":null,\"UserDate1\":null,\"UserDate2\":null,\"UserDate3\":null,\"UserDate4\":null,\"UserDate5\":null,\"UserNumeric1\":null,\"UserNumeric2\":null,\"UserNumeric3\":null,\"UserNumeric4\":null,\"UserNumeric5\":null}}";
            //JsonNode node = jt.getFieldValue("/CardholderAccessLevels/AccessLevelID");
            //System.out.println(node);
            
            JacksonTest ja = new JacksonTest(access);
            System.out.println(ja.rootNode.toPrettyString());
            
            //System.out.println(ja.getFieldValue("/AccessLevelID"));
            System.exit(0);
            
            JsonNode node = jt.rootNode.at("/0");
            System.out.println(node);
            
            ArrayNode an = (ArrayNode)jt.rootNode.at("/0/CardholderAccessLevels");
            System.out.println(node);
            int size = an.size();
            an.addObject();
            an.set(size, ja.rootNode);
            System.out.println(an.toPrettyString());
            System.out.println(jt.rootNode.toPrettyString());
            
            ObjectNode al = (ObjectNode)mapper.createObjectNode();
            al.put("Test1", "abc");
            al.put("Test2", "def");
            
            //System.out.println(mapper.writeValueAsString(al));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
