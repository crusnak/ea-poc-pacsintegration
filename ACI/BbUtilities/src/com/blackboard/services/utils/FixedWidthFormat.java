/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author crusnak
 */
public class FixedWidthFormat implements TextFormat
{
    ArrayList<Field> fields = new ArrayList();
    boolean sorted = false;

    public FixedWidthFormat() {}

    public void setFields(List<Field> f)
    {
        fields = new ArrayList(f);
        Collections.sort(fields);
    }

    public void addFields(List<Field> f)
    {
        fields.addAll(f);
        Collections.sort(fields);
    }

    public void addField(Field f)
    {
        fields.add(f);
        Collections.sort(fields);
    }

    public void addField(String name, int idx, String dataType)
    {
        fields.add(new Field(name, idx, dataType));
        Collections.sort(fields);
    }

    public void addField(String name, int idx, boolean constant, String dataType)
    {
        fields.add(new Field(name, idx, constant, dataType));
        Collections.sort(fields);
    }
    
    public Field insertField(int idx, String name, int len, String dataType)
        throws ArrayIndexOutOfBoundsException
    {
        Field repl = fields.get(idx);
        fields.set(idx, new Field(name, repl.getStartIndex(), dataType));
        repl.setStartIndex(repl.getStartIndex() + len);
        fields.add(repl);
        Collections.sort(fields);
        return repl;
    }

    public Field insertField(int idx, String name, int len, boolean constant, String dataType)
        throws ArrayIndexOutOfBoundsException
    {
        Field repl = fields.get(idx);
        fields.set(idx, new Field(name, repl.getStartIndex(), constant, dataType));
        repl.setStartIndex(repl.getStartIndex() + len);
        fields.add(repl);
        Collections.sort(fields);
        return repl;
    }

    public Field getFieldByName(String name)
    {
        Field field = null;
        for ( Field f: fields )
        {
            if ( f.getName().equals(name) )
            {
                field = f;
                break;
            }
        }
        return field;
    }
    
    public void resetFields()
    {
        fields.clear();
    }

    public boolean removeField(String name)
    {
        Field field = null;
        for ( Field f: fields )
        {
            if ( f.getName().equals(name) )
            {
                field = f;
                break;
            }
        }
        return removeField(field);
    }
    
    public boolean removeField(Field field)
    {
        boolean removed = false;
        if ( field != null )
        {
            int idx = fields.indexOf(field);
            int[] lens = new int[fields.size()];

            for ( int i=0; i<fields.size(); i++ )
            {
                lens[i] = getFieldLength(fields.get(i));
            }

            if ( idx >= 0 )
            {
                for ( int i=idx+1; i < fields.size(); i++ )
                {
                    if ( i == idx+1 )
                    {
                        fields.get(i).setStartIndex(fields.get(idx).getStartIndex());
                    }
                    else
                    {
                        fields.get(i).setStartIndex(fields.get(i-1).getStartIndex() + lens[i-1]);
                    }

                }
            }

            removed = fields.remove(field);
        }
        
        return removed;
    }
    

    public List<Field> getFields()
    {
        return fields;
    }

    public List<Field> getSimpleFields()
    {
        return fields;
    }

    public String[] readHeader(String line)
    {
        return new String[0];
    }

    public Map<String,String> processLine(String line)
        throws TextFormatException
    {
        LinkedHashMap<String,String> map = new LinkedHashMap();

        try
        {
            for ( int i=0; i<fields.size(); i++ )
            {
                Field f = fields.get(i);
                if ( i < fields.size()-1 )
                {
                    Field n = fields.get(i+1);
                    String extract = line.substring(f.getStartIndex(), n.getStartIndex());
                    map.put(f.getName(), extract.trim());
                }
                else
                {
                    String extract = line.substring(f.getStartIndex());
                    map.put(f.getName(), extract.trim());
                }
            }
        }
        catch (Exception e)
        {
            throw new TextFormatException(this, "Unable to process line: " + line, e);
        }

        return map;
    }

    public Map<Field,Integer> getFieldLengthMap()
    {
        Map<Field,Integer> map = new LinkedHashMap();
        for ( Field f: fields )
        {
            map.put(f, getFieldLength(f));
        }
        
        return map;
    }
    
    public int getFieldLength(Field f)
    {
        int length = 0;
        int[] range = getFieldRange(f);
        if ( range[0] != -1 || range[1] != -1 )
        {
            if ( range[1] == -1 )
            {
                length = -1;
            }
            else
            {
                length = range[1] - range[0];
            }
        }
        
        return length;
    }
    
    public int getFieldLength(String name)
    {
        return getFieldLength(new Field(name, -1, null));
    }
    
    /**
     * This method determines the index range for the given field.  The return value
     * is a 2-dimension int array where the first element indicates the start index 
     * (inclusive) and the second element indicates the end index (exclusive).
     *
     * A -1 in both elements indicates the field was not found.  A -1 in the second element
     * indicates the field is the last field defined and will contain everything the the end
     * of the incoming string.
     * @param f The field to find the range for.
     * @return A 2-dimension int array indicated the start and end index of the field.
     */
    public int[] getFieldRange(Field f)
    {
        int[] range = new int[2];
        
        int idx = fields.indexOf(f);
        if ( idx < 0 )
        {
            for ( int i=0; i<fields.size(); i++ )
            {
                if ( f.getName().equals(fields.get(i).getName()) )
                {
                    idx = i;
                    break;
                }
            }
        }
        
        // If not found indicate -1 in start and end range
        if ( idx < 0 )
        {
            range[0] = -1;
            range[1] = -1;
        }
        else
        {
            f = fields.get(idx);
        
            // If the field found is the last field indicate -1 in the end range
            if ( idx == fields.size()-1 )
            {
                range[0] = f.getStartIndex();
                range[1] = -1;
            }
            else
            {
                Field n = fields.get(idx+1);
                range[0] = f.getStartIndex();
                range[1] = n.getStartIndex();
            }
        }
        
        return range;
    }
    
    public int[] getFieldRange(String name)
    {
        return getFieldRange(new Field(name, -1, null));
    }
    
    public String formatLine(Map<String,String> map)
        throws TextFormatException
    {
        return formatLine(map, true);
    }
    
    public String formatLine(Map<String,String> map, boolean removeLineFeeds)
        throws TextFormatException
    {
        ArrayList<String> values = new ArrayList();
        for ( Field f: fields )
        {
            values.add(map.get(f.getName()));
        }

        return formatLine(values, removeLineFeeds);
    }

    public String formatLine(List<String> values)
        throws TextFormatException
    {
        return formatLine(values, true);
    }
    
    public String formatLine(List<String> values, boolean removeLineFeeds)
        throws TextFormatException
    {
        return formatLine(values.toArray(new String[0]), removeLineFeeds);
    }

    public String formatLine(String[] values)
        throws TextFormatException
    {
        return formatLine(values, true);
    }
    
    public String formatLine(String[] values, boolean removeLineFeeds)
        throws TextFormatException
    {
        String line = "";

        try
        {
            if ( removeLineFeeds )
            {
                for ( int i=0; i<values.length; i++ )
                {
                    if ( values[i] != null )
                    {
                        values[i] = values[i].replaceAll("\\n", "");
                        values[i] = values[i].replaceAll("\\r", "");
                    }
                }
            }

            Collections.sort(fields);
            for ( int i=0; i<fields.size(); i++ )
            {
                Field f = fields.get(i);
                if ( i < fields.size()-1 )
                {
                    Field n = fields.get(i+1);
                    int index = n.getStartIndex()-f.getStartIndex();
                    if ( i < values.length )
                    {
                        String val = values[i];
                        if ( val == null )
                        {
                            val = "";
                        }
                        
                        if ( val.length() > index )
                        {
                            val = val.substring(0, index);
                        }

                        // If left justify, add the value before any padding
                        if ( f.getJustification() == Field.Justify.left )
                        {
                            line += val;
                            for ( int j=0; j<index-val.length(); j++ )
                            {
                                line += " ";
                            }
                        }
                        // Otherwise, add the value after any padding
                        else
                        {
                            for ( int j=0; j<index-val.length(); j++ )
                            {
                                line += " ";
                            }
                            line += val;
                        }
                    }
                    else
                    {
                        for ( int j=0; j<index; j++ )
                        {
                            line += " ";
                        }
                    }

                }
                else
                {
                    if ( i < values.length )
                    {
                        line += values[i];
                    }
                }
            }
        }
        catch (Exception e)
        {
            ArrayList<String> vals = new ArrayList();
            for ( String v: values )
            {
                vals.add(v);
            }

            throw new TextFormatException(this, "Unable to format line with: " + vals, e);
        }

        return line;
    }

    @Override
    public String toString()
    {
        return String.valueOf(getFieldLengthMap());
    }

    public static class Field implements Comparable, TextFormat.Field
    {
        public static enum Justify { right, left };
        
        private int startIndex;
        private String name = "";
        private boolean constant = false;
        private Justify justify = Justify.left;
        private String dataType = null;

        public Field() {}

        public Field(String n, int idx, String dt)
        {
            startIndex = idx;
            name = n;
            dataType = dt;
        }

        public Field(String n, int idx, boolean c, String dt)
        {
            this(n, idx, dt);
            constant = c;
        }

        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public int getStartIndex()
        {
            return startIndex;
        }

        public void setStartIndex(int startIndex)
        {
            this.startIndex = startIndex;
        }

        public boolean isConstant()
        {
            return constant;
        }
        
        public Justify getJustification()
        {
            return justify;
        }
        
        public void setJustification(Justify j)
        {
            justify = j;
        }
        
        public void setJustification(String j)
            throws IllegalArgumentException
        {
            justify = Justify.valueOf(j);
        }
        
        public int compareTo(Object o)
        {
            int comp = 0;
            if ( o instanceof Field )
            {
                int index = ((Field)o).getStartIndex();
                comp = Long.valueOf(startIndex).compareTo(Long.valueOf(index));
            }

            return comp;
        }
        
        public String getDataType()
        {
            return dataType;
        }
        
        public void setDataType(String dt)
        {
            dataType = dt;
        }

        @Override
        public String toString()
        {
            return name + "([" + dataType + "] " + startIndex + "|" + justify + ")";
        }
    }

    public static void main(String[] args)
    {
        String line = "00000000056081238965";
        System.out.println(line.replaceFirst("^0\\d[0]*", "572227"));
        /*try
        {
            FixedWidthFormat format = new FixedWidthFormat();
            format.addField("balance", 61);
            format.addField("campusid", 0);
            format.addField("name", 20);
            format.addField("$", 57);
            format.addField("privilege", 51);
            format.addField("plan", 67);

            String[] fields = { "011144141", "Brooks, Thomas Whitson", "1007", "$343234", "0.01", "001" };
            String l = format.formatLine(fields);
            System.out.println(format);
            System.out.println(l);
            Map m = format.processLine(l);
            System.out.println(m);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }*/
    }

}
