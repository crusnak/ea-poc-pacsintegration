/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.utils.schema;

import com.blackboard.services.utils.XMLUtils;
import com.blackboard.services.utils.exception.CompilationException;
import com.blackboard.services.utils.exception.CompilerError;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.xml.transform.TransformerException;

/**
 *
 * @author crusnak
 */
public class SchemaElement implements SchemaNode
{
    public static enum ChildType { sequence, choice, simple };

    private SchemaElement parent = null;
    private org.w3c.dom.Node node = null;
    private String name = null;
    private String dataType = null;
    private ChildType childType = null;
    private int minOccurs = 1;
    private int maxOccurs = 1;
    private String defaultValue = "";
    private List<String> restrictions = null;
    private List<SchemaElement> children = null;
    private List<SchemaAttribute> attributes = new ArrayList();

    public SchemaElement(org.w3c.dom.Element e, SchemaElement p)
        throws CompilationException
    {
        node = e;
        parent = p;
        name = e.getAttribute("name");
        parseCardinality(e);

        if ( SchemaDocument.SCHEMA_ELEMENT.equals(e.getLocalName()) )
        {
            parseElement(e);
        }
        else if ( SchemaDocument.SCHEMA_COMPLEX_TYPE.equals(e.getLocalName()) )
        {
            parseComplexType(e);
        }
        else if ( SchemaDocument.SCHEMA_SIMPLE_TYPE.equals(e.getLocalName()) )
        {
            parseSimpleType(e);
        }
        else if ( SchemaDocument.SCHEMA_SEQUENCE.equals(e.getLocalName()) )
        {
            // If the parent is also a sequence, sequences of sequences equivalate to a sequence
            if ( parent.childType == ChildType.sequence )
            {
                parent.children = parseSequence(e);
            }
            else
            {
                children = parseSequence(e);
            }
        }
        else if ( SchemaDocument.SCHEMA_CHOICE.equals(e.getLocalName()) )
        {
            children = parseChoice(e);
        }
        else
        {
            throw new CompilationException("Unexpected schema element found: " + e.getNodeName());
        }
    }

    public Type getType()
    {
        return Type.element;
    }

    public SchemaElement getParentElement()
    {
        return parent;
    }

    public void setParentElement(SchemaElement parent)
    {
        this.parent = parent;
    }

    public String getName()
    {
        return name;
    }

    public String getDataType()
    {
        return dataType;
    }

    public ChildType getChildType()
    {
        return childType;
    }

    public int getMinOccurs()
    {
        return minOccurs;
    }

    public int getMaxOccurs()
    {
        return maxOccurs;
    }

    public String getDefaultValue()
    {
        return defaultValue;
    }

    public List<String> getRestrictions()
    {
        return restrictions;
    }

    public List<SchemaElement> getChildren()
    {
        return children;
    }

    public List<SchemaAttribute> getAttributes()
    {
        return attributes;
    }

    public org.w3c.dom.Node getDOMNode()
    {
        return node;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setDataType(String dataType)
    {
        this.dataType = dataType;
    }

    public void setChildType(ChildType childType)
    {
        this.childType = childType;
    }

    public void setMinOccurs(int minOccurs)
    {
        this.minOccurs = minOccurs;
    }

    public void setMaxOccurs(int maxOccurs)
    {
        this.maxOccurs = maxOccurs;
    }

    public void setDefaultValue(String defaultValue)
    {
        this.defaultValue = defaultValue;
    }

    public void setRestrictions(List<String> restrictions)
    {
        this.restrictions = restrictions;
    }

    public void setChildren(List<SchemaElement> children)
    {
        this.children = children;
    }

    public void setAttributes(List<SchemaAttribute> attributes)
    {
        this.attributes = attributes;
    }
        
    private void parseElement(org.w3c.dom.Element e)
        throws CompilationException
    {
        List<CompilerError> errors = new ArrayList();

        defaultValue = e.getAttribute("default");

        // If there are no child elements this is a simple type
        if ( !XMLUtils.hasChildElements(e) )
        {
            childType = ChildType.simple;
            dataType = e.getAttribute("type");
        }
        else if ( SchemaDocument.SCHEMA_COMPLEX_TYPE.equals(XMLUtils.getFirstChildElement(e).getLocalName()) )
        {
            parseComplexType(XMLUtils.getFirstChildElement(e));
        }
        else if ( SchemaDocument.SCHEMA_SIMPLE_TYPE.equals(XMLUtils.getFirstChildElement(e).getLocalName()) )
        {
            parseSimpleType(XMLUtils.getFirstChildElement(e));
        }
        else 
        {
            errors.add(new CompilerError("Unexpected schema element found: " + XMLUtils.getFirstChildElement(e).getNodeName()));
        }

        // If there were any errors during parsing throw them now
        if ( !errors.isEmpty() )
        {
            CompilationException ex = new CompilationException("Failed to parse schema element: " + e.getNodeName() + (e.getAttribute("name")!=null?"(" + e.getAttribute("name") + ")":""));
            ex.addErrors(errors);
            throw ex;
        }
    }

    private void parseComplexType(org.w3c.dom.Element e)
        throws CompilationException 
    {
        List<CompilerError> errors = new ArrayList();

        for ( org.w3c.dom.Element elem: XMLUtils.getChildElements(e) )
        {
            if ( SchemaDocument.SCHEMA_SEQUENCE.equals(elem.getLocalName()) )
            {
                children = parseSequence(elem);
            }
            else if ( SchemaDocument.SCHEMA_CHOICE.equals(elem.getLocalName()) )
            {
                children = parseChoice(elem);
            }
            else if ( SchemaDocument.SCHEMA_SIMPLE_CONTENT.equals(elem.getLocalName()) )
            {
                parseSimpleContent(elem);
            }
            else if ( SchemaDocument.SCHEMA_ATTRIBUTE.equals(elem.getLocalName()) )
            {
                parseAttributes(elem);
            }
            else 
            {
                errors.add(new CompilerError("Unexpected schema element found: " + XMLUtils.getFirstChildElement(e).getNodeName()));
            }
        }

        // If there were any errors during parsing throw them now
        if ( !errors.isEmpty() )
        {
            CompilationException ex = new CompilationException("Failed to parse schema element: " + e.getNodeName() + (e.getAttribute("name")!=null?"(" + e.getAttribute("name") + ")":""));
            ex.addErrors(errors);
            throw ex;
        }
    }

    private void parseSimpleType(org.w3c.dom.Element e)
        throws CompilationException
    {
        childType = ChildType.simple;
        if ( SchemaDocument.SCHEMA_RESTRICTION.equals(XMLUtils.getFirstChildElement(e).getLocalName()) )
        {
            restrictions = parseRestrictions(XMLUtils.getFirstChildElement(e));
        }
    }

    private void parseSimpleContent(org.w3c.dom.Element e)
        throws CompilationException
    {
        ArrayList<CompilerError> errors = new ArrayList();

        childType = ChildType.simple;
        if ( SchemaDocument.SCHEMA_EXTENSION.equals(XMLUtils.getFirstChildElement(e).getLocalName()) )
        {
            parseExtension(XMLUtils.getFirstChildElement(e));
        }
        else
        {
            errors.add(new CompilerError("Unexpected schema element found: " + XMLUtils.getFirstChildElement(e).getNodeName()));
        }

        // If there were any errors during parsing throw them now
        if ( !errors.isEmpty() )
        {
            CompilationException ex = new CompilationException("Failed to parse schema element: " + e.getNodeName() + (e.getAttribute("name")!=null?"(" + e.getAttribute("name") + ")":""));
            ex.addErrors(errors);
            throw ex;
        }
    }

    private void parseExtension(org.w3c.dom.Element e)
        throws CompilationException
    {
        dataType = e.getAttribute("base");
        for ( org.w3c.dom.Element elem: XMLUtils.getChildElements(e) )
        {
            parseAttributes(elem);
        }
    }

    private List<SchemaElement> parseSequence(org.w3c.dom.Element e)
        throws CompilationException
    {
        ArrayList<SchemaElement> sequence = new ArrayList();

        childType = ChildType.sequence;
        for ( org.w3c.dom.Element element: XMLUtils.getChildElements(e) )
        {
            sequence.add(new SchemaElement(element, this));
        }

        return sequence;
    }

    private List<SchemaElement> parseChoice(org.w3c.dom.Element e)
        throws CompilationException
    {
        ArrayList<SchemaElement> choices = new ArrayList();

        childType = ChildType.choice;
        for ( org.w3c.dom.Element element: XMLUtils.getChildElements(e) )
        {
            choices.add(new SchemaElement(element, this));
        }

        return choices;
    }

    private List<String> parseRestrictions(org.w3c.dom.Element e)
        throws CompilationException
    {
        List<CompilerError> errors = new ArrayList();
        ArrayList<String> restrictions = new ArrayList();

        dataType = e.getAttribute("base");
        for ( org.w3c.dom.Element element: XMLUtils.getChildElements(e) )
        {
            if ( SchemaDocument.SCHEMA_ENUMERATION.equals(element.getLocalName()) )
            {
                restrictions.add(element.getAttribute("value"));
            }
            else 
            {
                errors.add(new CompilerError("Unexpected schema element found: " + element.getNodeName()));
            }
        }

        // If there were any errors during parsing throw them now
        if ( !errors.isEmpty() )
        {
            CompilationException ex = new CompilationException("Failed to parse schema element: " + e.getNodeName() + (e.getAttribute("name")!=null?"(" + e.getAttribute("name") + ")":""));
            ex.addErrors(errors);
            throw ex;
        }

        return restrictions;
    }

    private void parseCardinality(org.w3c.dom.Element e)
        throws CompilationException
    {
        List<CompilerError> errors = new ArrayList();
        String min = e.getAttribute("minOccurs");
        String max = e.getAttribute("maxOccurs");
        if ( !"".equals(min) )
        {
            try
            {
                minOccurs = Integer.parseInt(min);
            }
            catch ( NumberFormatException ex)
            {
                errors.add(new CompilerError("Invalid number passed to minOccurs: " + min + ": " + ex.getLocalizedMessage()));
            }
        }

        if ( "unbounded".equals(max) )
        {
            maxOccurs = -1;
        }
        else if ( !"".equals(max) )
        {
            try
            {
                maxOccurs = Integer.parseInt(max);
            }
            catch ( NumberFormatException ex)
            {
                errors.add(new CompilerError("Invalid number passed to maxOccurs: " + max + ": " + ex.getLocalizedMessage()));
            }
        }

        if ( !errors.isEmpty() )
        {
            CompilationException ex = new CompilationException("Failed to parse schema element: " + e.getNodeName() + (e.getAttribute("name")!=null?"(" + e.getAttribute("name") + ")":""));
            ex.addErrors(errors);
            throw ex;
        }
    }

    private void parseAttributes(org.w3c.dom.Element e)
        throws CompilationException
    {
        if ( !XMLUtils.hasChildElements(e) || 
             SchemaDocument.SCHEMA_SIMPLE_TYPE.equals(XMLUtils.getFirstChildElement(e).getLocalName()))
        {
            attributes.add(new SchemaAttribute(e, this));
        }
    }

    public String toXML()
        throws TransformerException
    {
        return XMLUtils.toString(node, false);
    }

    public String toXPathExpression()
    {
        ArrayList<String> paths = new ArrayList();
        String xpath = "";

        SchemaElement parent = this;
        while ( parent != null )
        {
            paths.add(parent.getName());
            parent = parent.getParentElement();
        }

        Collections.reverse(paths);
        for ( String path: paths )
        {
            if ( path.trim().length() > 0 )
            {
                xpath += "/" + path;
            }
        }

        return xpath;
    }

    public String toString()
    {
        StringBuilder out = new StringBuilder();

        out.append("\n{");
        out.append("type=" + childType);
        out.append(", name=" + name);
        out.append((minOccurs!=1?", minOccurs=" + minOccurs:""));
        out.append((maxOccurs!=1?", maxOccurs=" + maxOccurs:""));
        out.append((childType==ChildType.simple?", datatype=" + dataType:""));
        out.append((childType==ChildType.simple?", restrictions=" + restrictions:""));
        out.append((childType==ChildType.simple?", default=" + defaultValue:""));
        out.append((!attributes.isEmpty()?", attr=" + attributes:""));
        out.append((childType!=ChildType.simple?", children=" + children:""));
        out.append("}");

        return out.toString();
    }
}
