/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.utils.schema;

import com.blackboard.services.utils.schema.SchemaElement;

/**
 *
 * @author crusnak
 */
public interface SchemaNode
{
        public static enum Type { element, attribute };
        
        public Type getType();
        public String getName();
        public int getMinOccurs();
        public int getMaxOccurs();
        public String getDefaultValue();
        public String getDataType();
        public SchemaElement getParentElement();
        public org.w3c.dom.Node getDOMNode();
        public String toXPathExpression();
}
