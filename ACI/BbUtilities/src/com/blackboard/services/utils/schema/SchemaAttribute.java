/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.utils.schema;

import com.blackboard.services.utils.XMLUtils;
import com.blackboard.services.utils.exception.CompilationException;
import com.blackboard.services.utils.exception.CompilerError;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author crusnak
 */
public class SchemaAttribute implements SchemaNode
{
    public static enum Use { optional, required, prohibited };

    private org.w3c.dom.Node node = null;
    private SchemaElement parent = null;
    private String name = null;
    private String dataType = null;
    private Use use = Use.optional;
    private String defaultValue = null;
    private List<String> restrictions = null;

    public SchemaAttribute(org.w3c.dom.Element e, SchemaElement p)
        throws CompilationException
    {
        node = e;
        parent = p;

        name = e.getAttribute("name");
        dataType = e.getAttribute("type");
        defaultValue = e.getAttribute("default");

        if ( e.getAttribute("use") != null && e.getAttribute("use").trim().length() > 0 )
        {
            use = Use.valueOf(e.getAttribute("use"));
        }

        if ( XMLUtils.hasChildElements(e) &&
             SchemaDocument.SCHEMA_SIMPLE_TYPE.equals(XMLUtils.getFirstChildElement(e).getLocalName()) )
        {
            org.w3c.dom.Element simpleType = XMLUtils.getFirstChildElement(e);
            if ( SchemaDocument.SCHEMA_RESTRICTION.equals(XMLUtils.getFirstChildElement(simpleType).getLocalName()) )
            {
                restrictions = parseAttributeRestrictions(XMLUtils.getFirstChildElement(simpleType));
            }
            else
            {
                 throw new CompilationException("Unexpected schema element found: " + XMLUtils.getFirstChildElement(simpleType).getNodeName());
            }
        }
    }

    private List<String> parseAttributeRestrictions(org.w3c.dom.Element e)
        throws CompilationException
    {
        List<CompilerError> errors = new ArrayList();
        ArrayList<String> restrictions = new ArrayList();

        dataType = e.getAttribute("base");
        for ( org.w3c.dom.Element element: XMLUtils.getChildElements(e) )
        {
            if ( SchemaDocument.SCHEMA_ENUMERATION.equals(element.getLocalName()) )
            {
                restrictions.add(element.getAttribute("value"));
            }
            else 
            {
                errors.add(new CompilerError("Unexpected schema element found: " + element.getNodeName()));
            }
        }

        // If there were any errors during parsing throw them now
        if ( !errors.isEmpty() )
        {
            CompilationException ex = new CompilationException("Failed to parse schema element: " + e.getNodeName() + (e.getAttribute("name")!=null?"(" + e.getAttribute("name") + ")":""));
            ex.addErrors(errors);
            throw ex;
        }

        return restrictions;
    }

    public SchemaNode.Type getType()
    {
        return SchemaNode.Type.attribute;
    }

    public SchemaElement getParentElement()
    {
        return parent;
    }

    public org.w3c.dom.Node getDOMNode()
    {
        return node;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDataType()
    {
        return dataType;
    }

    public boolean isRequired()
    {
        return use == Use.required;
    }

    public boolean isProhibited()
    {
        return use == Use.prohibited;
    }

    public String getDefaultValue()
    {
        return defaultValue;
    }

    public void setDataType(String dataType)
    {
        this.dataType = dataType;
    }

    public void setUse(Use use)
    {
        this.use = use;
    }

    public void setDefaultValue(String defaultValue)
    {
        this.defaultValue = defaultValue;
    }

    public void setRestrictions(List<String> restrictions)
    {
        this.restrictions = restrictions;
    }

    
    public int getMinOccurs()
    {
        int val = 0;
        switch (use)
        {
            case optional:
            case prohibited:
                val = 0;
                break;
            case required:
                val = 1;
                break;
        }

        return val;
    }

    public int getMaxOccurs()
    {
        int val = 0;
        switch(use)
        {
            case optional:
            case required:
                val = 1;
                break;
            case prohibited:
                val = 0;
                break;
        }


        return val;
    }

    public String toXPathExpression()
    {
        String xpath = parent.toXPathExpression();
        xpath += "[@" + name + "]";
        return xpath;
    }

    public String toString()
    {
        StringBuilder out = new StringBuilder();

        out.append("{");
        out.append("name=" + name);
        out.append(", datatype=" + dataType);
        out.append(", use=" + use);
        out.append((defaultValue!=null?", default=" + defaultValue:""));
        out.append((restrictions!=null?", restrictions=" + restrictions:""));
        out.append("}");

        return out.toString();
    }
}
