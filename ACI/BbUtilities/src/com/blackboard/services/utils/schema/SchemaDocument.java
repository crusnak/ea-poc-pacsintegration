/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.utils.schema;

import com.blackboard.services.utils.ResourceLoader;
import com.blackboard.services.utils.XMLUtils;
import com.blackboard.services.utils.exception.CompilationException;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 *
 * @author crusnak
 */
public class SchemaDocument
{
    public static final String SCHEMA_NAMESPACE = "http://www.w3.org/2001/XMLSchema";
    public static final String SCHEMA_ELEMENT = "element";
    public static final String SCHEMA_COMPLEX_TYPE = "complexType";
    public static final String SCHEMA_SIMPLE_TYPE = "simpleType";
    public static final String SCHEMA_SEQUENCE = "sequence";
    public static final String SCHEMA_CHOICE = "choice";
    public static final String SCHEMA_SIMPLE_CONTENT = "simpleContent";
    public static final String SCHEMA_RESTRICTION = "restriction";
    public static final String SCHEMA_ENUMERATION = "enumeration";
    public static final String SCHEMA_EXTENSION = "extension";
    public static final String SCHEMA_ATTRIBUTE = "attribute";

    public static final String TYPE_64BINARY = "base64Binary";
    public static final String TYPE_BOOLEAN = "boolean";
    public static final String TYPE_BYTE = "byte";
    public static final String TYPE_DATE = "date";
    public static final String TYPE_DATETIME = "datetime";
    public static final String TYPE_DECIMAL = "decimal";
    public static final String TYPE_DOUBLE = "double";
    public static final String TYPE_FLOAT = "float";
    public static final String TYPE_HEX_BINARY = "hexBinary";
    public static final String TYPE_INTEGER = "integer";
    public static final String TYPE_INT = "int";
    public static final String TYPE_LONG = "long";
    public static final String TYPE_SHORT = "short";
    public static final String TYPE_STRING = "string";
    public static final String TYPE_TIME = "time";
    
    private static final String ROOT_TYPE = "<root>";
    
    private Logger logger = null;
    private File schemaFile = null;
    private Document schema = null;
    private BiMap<String,String> namespaceMap = null;
    private Map<String,SchemaNode> xpathMap = new HashMap();
    private Map<String,SchemaElement> typeMap = new HashMap();
    private String targetNamespace = null;
    
    public SchemaDocument(File root, String file)
        throws CompilationException
    {
        try
        {
            logger = Logger.getLogger(this.getClass());
            schemaFile = ResourceLoader.findResourceAsFile(null, root, file);
            schema = XMLUtils.parseXML(schemaFile);
        }
        catch ( IOException e)
        {
            throw new CompilationException("Unable to locate XML schema for HierarchyMapping: " + file, e);
        }
        catch ( SAXException e)
        {
            throw new CompilationException("Unable to parse XML schema document: " + schemaFile, e);
        }
        
        parseSchema();
    }
    

    public Document getSchema()
    {
        return schema;
    }

    public BiMap<String, String> getNamespaceMap()
    {
        return namespaceMap;
    }

    public File getSchemaFile()
    {
        return schemaFile;
    }

    public Map<String, SchemaElement> getTypeMap()
    {
        return typeMap;
    }
    
    public boolean hasRootElement()
    {
        return getRootElement() != null;
    }
    
    public SchemaElement getRootElement()
    {
        return typeMap.get(ROOT_TYPE);
    }

    public String getTargetNamespace()
    {
        return targetNamespace;
    }

    public String getNamespaceURI(String namespace)
    {
        return namespaceMap.get(namespace);
    }
    
    public String getNamespace(String namespaceURI)
    {
        return namespaceMap.inverse().get(namespaceURI);
    }
    
    public Map<String,SchemaNode> getXPathMap()
    {
        return xpathMap;
    }
    
    public SchemaNode retrieveXPathElement(String xpath)
    {
        return xpathMap.get(xpath);
    }

    private void parseSchema()
        throws CompilationException
    {
        org.w3c.dom.Element root = schema.getDocumentElement();
        
        // TODO: need to check for includes and load the schema for this separately 
        // then add all namespaces and typeMaps to this object prior to parsing this document
        // to ensure all references are found.
        
        // Get namespaces
        namespaceMap = HashBiMap.create();
        for ( int i=0; i<root.getAttributes().getLength(); i++ )
        {
            org.w3c.dom.Node node = root.getAttributes().item(i);
            String attrName = node.getNodeName();
            String attrVal = node.getNodeValue();
            if ( attrName.startsWith("xmlns:") )
            {
                namespaceMap.put(attrName.substring(6), attrVal);
            }
            
            if ( attrName.startsWith("targetNamespace") )
            {
                targetNamespace = attrVal;
            }
        }
        
        // Look for a single root element
        for ( int i=0; i<root.getChildNodes().getLength(); i++ )
        {
            org.w3c.dom.Node node = root.getChildNodes().item(i);
            if ( node.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE )
            {
                org.w3c.dom.Element elem = (org.w3c.dom.Element)node;
                if ( SCHEMA_ELEMENT.equals(node.getLocalName()) )
                {
                    typeMap.put(ROOT_TYPE, new SchemaElement(elem, null));
                }
                else if ( SCHEMA_COMPLEX_TYPE.equals(node.getLocalName()) || 
                          SCHEMA_SIMPLE_TYPE.equals(node.getLocalName()) )
                {
                    typeMap.put(elem.getAttribute("name"), new SchemaElement(elem, null));
                }
            }
        }
        
        SchemaElement rootType = getRootElement();
        replaceReferences(rootType);
        loadXPathMap();
        
        logger.debug("Valid namespaces: " + namespaceMap);
        logger.debug("Target namespace: " + targetNamespace);
        logger.debug("Schema Type Map: " + typeMap);
        logger.debug("Valid xpath expressions: " + getValidXpathExpressions(rootType));
        logger.trace("Xpath map: " + xpathMap);
    }
    
    public void validateXpath(String xpath)
        throws CompilationException
    {
        logger.debug("Validating xpath in schema: " + xpath);
        if ( xpath == null )
        {
            throw new CompilationException("Xpath cannot be null");
        }
        
        String[] paths = xpath.split("\\/");
        if ( paths.length == 0 )
        {
            throw new CompilationException("Xpath must start with a root /: " + xpath);
        }
        
        CompilationException e = new CompilationException("Invalid xpath expression: " + xpath);
        String serialPath = "";
        SchemaElement current = typeMap.get(ROOT_TYPE);
        for ( int i=0; i<paths.length; i++ )
        {
            String path = paths[i];
            
            // i=0 will always be empty since the xpath expression must start with a leading /
            if ( i == 1 )
            {
                serialPath += "/" + path;
                if ( !path.equals(current.getName()) )
                {
                    e.addError("Root node in xpath does not match root in schema: " + serialPath + " != " + current.getName());
                    throw e;
                }
            }
            else if ( i > 1 )
            {
                String attr = null;
                
                // Check to see if this path element contains an attribute component
                if ( path.contains("[@") )
                {
                    attr = path.substring(path.indexOf("[@") + 2, path.indexOf("]"));
                    path = path.substring(0, path.indexOf("["));
                }
                        
                serialPath += "/" + path;
                boolean found = false;
                for ( SchemaElement child: current.getChildren() )
                {
                    // Check to see if any child schema element matches the name of the path
                    SchemaElement match = null;
                    if ( (match = matchChild(child, path)) != null )
                    {
                        found = true;
                        current = match;
                        break;
                    }
                }
                
                // If no matching element found an error must be thrown
                if ( !found )
                {
                    e.addError("Unable to find matching path in schema: " + serialPath);
                }
                
                // If there was an attribute component we need to test for the existence of that
                if ( attr != null )
                {
                    if ( current.getAttributes() == null || current.getAttributes().isEmpty() )
                    {
                        e.addError("Schema element does not contain attributes for the given path: " + serialPath);
                    }
                    else
                    {
                        serialPath += "[@" + attr + "]";
                        found = false;
                        for ( SchemaAttribute attribute: current.getAttributes() )
                        {
                            // Check to see if any attribute matches the name of the path
                            if ( attr.equals(attribute.getName()) )
                            {
                                found = true;
                                break;
                            }

                            // If no matching attribute found an error must be thrown
                            if ( !found )
                            {
                                e.addError("Unable to find matching attribute in schema for the given path: " + serialPath);
                            }
                        }
                    }
                }
                
                if ( !e.getErrors().isEmpty() )
                {
                    throw e;
                }
            }
        }
    }
    
    private SchemaElement matchChild(SchemaElement root, String name)
    {
        SchemaElement child = null;

        // If the root name matches the given name we found a match.
        if ( root != null && root.getName() != null && root.getName().equals(name) )
        {
            child = root;
        }
        // If the name doesn't match but the root name is empty and has children, treat the root
        // children at the same level as the root element.
        else if ( root != null && ( root.getName() == null || root.getName().trim().length() == 0 ) &&
                  root.getChildren() != null )
        {
            for ( SchemaElement c: root.getChildren() )
            {
                child = matchChild(c, name);
                if ( child != null )
                {
                    break;
                }
            }
        }
        
        return child;
    }
    
    private void replaceReferences(SchemaElement root)
    {
        if ( typeMap.containsKey(root.getDataType()) )
        {
            logger.debug("Found type reference: " + root.getDataType() + " within " + root.getName());
            SchemaElement ref = typeMap.get(root.getDataType());
            if ( ref.getChildType() == SchemaElement.ChildType.simple )
            {
                root.setChildType(SchemaElement.ChildType.simple);
                root.setDataType(ref.getDataType());
                root.setDefaultValue(ref.getDefaultValue());
                root.setMinOccurs(ref.getMinOccurs());
                root.setMaxOccurs(ref.getMaxOccurs());
                root.setRestrictions(ref.getRestrictions());
                ref.setParentElement(root);
            }
            else
            {
                root.setChildType(ref.getChildType());
                root.setDataType(null);
                root.setChildren(ref.getChildren());
                ref.setParentElement(root);
            }
        }
        else if ( root.getChildren() != null && !root.getChildren().isEmpty() )
        {
            for ( SchemaElement child: root.getChildren() )
            {
                replaceReferences(child);
            }
        }
    }
    
    public Set<String> getValidXpathExpressions(SchemaElement root)
    {
        LinkedHashSet<String> xpaths = new LinkedHashSet();
        
        if ( root != null )
        {
            //System.out.println(root.getName());
            xpaths.add(root.toXPathExpression());

            for ( SchemaAttribute attr: root.getAttributes() )
            {
                //System.out.println(attr);
                xpaths.add(attr.toXPathExpression());
            }

            if ( root.getChildren() != null )
            {
                for ( SchemaElement child: root.getChildren() )
                {
                    //System.out.println(child);
                    xpaths.addAll(getValidXpathExpressions(child));
                }
            }
        }
        
        return xpaths;
    }
    
    private void loadXPathMap()
    {
        Set<String> xpaths = getValidXpathExpressions(getRootElement());
        for ( String path: xpaths )
        {
            xpathMap.put(path, findElement(path));
        }
    }
    
    private SchemaNode findElement(String xpath)
        throws IllegalArgumentException
    {
        SchemaNode current = null;
        
        // Since we know if we get past this check that the expression is valid we don't need
        // to test for appropriate values of the nodes
        Set<String> xpaths = getValidXpathExpressions(getRootElement());
        if ( !xpaths.contains(xpath) )
        {
            throw new IllegalArgumentException("Invalid xpath to schema: " + xpath + " - Valid expressions: " + xpaths);
        }
        
        String[] paths = xpath.split("\\/");
        current = getRootElement();
        for ( int i=0; i<paths.length; i++ )
        {
            String path = paths[i];
            
            // i=0 will always be empty since the xpath expression must start with a leading /
            // i=1 will always be the root element
            if ( i > 1 )
            {
                String attr = null;
                
                // Check to see if this path element contains an attribute component
                if ( path.contains("[@") )
                {
                    attr = path.substring(path.indexOf("[@") + 2, path.indexOf("]"));
                    path = path.substring(0, path.indexOf("["));
                }
                        
                if ( current.getType() == SchemaNode.Type.element )
                {
                    SchemaElement e = (SchemaElement)current;
                    for ( SchemaElement child: e.getChildren() )
                    {
                        // Check to see if any child schema element matches the name of the path
                        SchemaElement match = null;
                        if ( ( match = matchChild(child, path)) != null )
                        {
                            current = match;
                            break;
                        }
                    }

                    // If there was an attribute component we need to test for the existence of that
                    if ( attr != null )
                    {
                        for ( SchemaAttribute attribute: e.getAttributes() )
                        {
                            // Check to see if any attribute matches the name of the path
                            if ( attr.equals(attribute.getName()) )
                            {
                                current = attribute;
                                break;
                            }
                        }
                    }
                }
            }
        }        
        
        return current;
    }
}
