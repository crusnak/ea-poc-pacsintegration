echo off
SET ETL_ROOT=..

SET CLASSPATH=%ETL_ROOT%
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/classes/
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/TIAInterface.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/ojdbc8.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/mysql-5.1.13.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/sqljdbc42.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/jtds-1.3.1.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/postgresql-9.1.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/ifxjdbc.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/unijdbc.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/asjava.zip
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/mail.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/commons-lang.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/commons-email.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/commons-io-1.4.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/CustomQueryExtract.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/jsch-0.1.55.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/commons-vfs.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/commons-io-1.4.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/bcpg-jdk16-146.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/bcprov-jdk16-146.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/guava-13.0.1.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/httpclient-4.5.1.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/httpcore-4.4.3.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/commons-codec-1.9.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/sshd-cli-2.5.1.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/sshd-common-2.5.1.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/sshd-core-2.5.1.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/sshd-scp-2.5.1.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/sshd-sftp-2.5.1.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/poi-5.0.0.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/poi-ooxml-5.0.0.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/poi-ooxml-lite-5.0.0.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/commons-collections4-4.4.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/commons-compress-1.20.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/xmlbeans-4.0.0.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/json-20180813.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/javaee-api-7.0.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/javaee-web-api-7.0.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/javax-websocket-client-impl-9.4.12.v20180830.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/jetty-client-9.4.12.RC2.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/jetty-http-9.4.12.RC2.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/jetty-io-9.4.12.RC2.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/jetty-util-9.4.12.v20180830.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/websocket-api-9.4.12.RC2.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/websocket-client-9.4.12.v20180830.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/websocket-common-9.4.12.RC2.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/azure-relay-0.0.3.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/slf4j-api-1.7.31.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/slf4j-log4j12-1.7.31.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/log4j-1.2.16.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/jackson-annotations-2.13.0-rc1.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/jackson-core-2.13.0-rc1.jar
SET CLASSPATH=%CLASSPATH%;%ETL_ROOT%/lib/jackson-databind-2.13.0-rc1.jar
set CLASSPATH=%CLASSPATH%;%ETL_ROOT%/resources/
set CLASSPATH=%CLASSPATH%;%ETL_ROOT%/resources/templates
set CLASSPATH=%CLASSPATH%;%ETL_ROOT%/resources/tests
set CLASSPATH=%CLASSPATH%;%ETL_ROOT%/resources/schemas
set JAVA_EXE="%JAVA_HOME%\bin\java"

set JVM_ARGS=-Xms128m -Xmx3072m
set MAIN_CLASS=com.transact.aci.test.ACITestDispatcher
set COMMAND=%JAVA_EXE% 

set JDK=%JAVA_HOME:.=_%
echo %JDK%
for /f "tokens=* delims=\" %%a in ("%JDK%") do set d=%%~na
set JDK=%d%

if "%JDK:~0,5%"=="jdk-9" goto JDK9
IF "%JDK:~0,6%"=="jdk1_8" goto JDK8
IF "%JDK:~0,6%"=="jdk1_7" goto JDK7
IF "%JDK:~0,6%"=="jdk1_6" goto JDK6
IF "%JDK:~0,6%"=="jdk1_5" goto JDK5
goto javaunknown

:JDK9
set JAVA_VERSION=%JDK:~4,1%
set COMMAND=%COMMAND% --add-modules java.xml.bind
goto execute

:JDK8
set JAVA_VERSION=%JDK:~5,1%
goto execute

:JDK7
set JAVA_VERSION=%JDK:~5,1%
goto notsupported

:JDK6
set JAVA_VERSION=%JDK:~5,1%
goto notsupported

:JDK5
set JAVA_VERSION=%JDK:~5,1%
goto notsupported

:javaunknown
echo Unable to determine java version from %JDK%. It is likely either the JAVA_HOME environment variable is unset, java is installed in a non-standard location, or the java version is very old or higher than 9.
goto end

:notsupported
echo JAVA version %JAVA_VERSION% is not supported. Upgrade to at least Java 8.
goto end

:execute
echo JAVA version: %JAVA_VERSION%
set COMMAND=%COMMAND% %JVM_ARGS% -classpath %CLASSPATH% %MAIN_CLASS% %ETL_ROOT%

:loop
IF [%1]==[] GOTO continue
    set COMMAND=%COMMAND% %1
SHIFT
GOTO loop
:continue

echo on
%COMMAND%
goto end

:end
