/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transact.aci;

import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.process.AbstractETLProcessor;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Class responsible for preparing all the ETL job definitions for use. Once a definition is loaded, it is not re-loaded again
 * during the lifetime of this containing JVM instance.
 * 
 * @author crusnak
 */
public class ETLManager 
{
    private static ETLManager manager = null;
    
    private File appRoot = null;
    private Map<String,ETLJobWrapper> wrapperMap = new HashMap();
    
    private ETLManager(String r)
    {
        appRoot = new File(".");
        if ( r != null )
        {
            appRoot = new File(r);
        }
    }
    
    public static ETLManager getManager(String root)
    {
        if ( manager == null )
        {
            manager = new ETLManager(root);
        }
        
        return manager;
    }
    
    public ETLJobWrapper getETLWrapper(String name)
        throws ETLException
    {
        ETLJobWrapper wrapper = null;
        if ( !wrapperMap.containsKey(name) )
        {
            wrapper = AbstractETLProcessor.initializeETLWrapper(appRoot, name);
            wrapperMap.put(name, wrapper);
        }
        else
        {
            wrapper = wrapperMap.get(name);
        }
        
        return wrapper;
    }

}
