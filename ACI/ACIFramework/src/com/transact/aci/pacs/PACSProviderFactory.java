/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transact.aci.pacs;

import com.transact.aci.exception.ACIException;
import java.util.List;

/**
 * This is a static only class to help build the PACSProvider object based on the ACISubscription object.
 * 
 * @author crusnak
 */
public class PACSProviderFactory 
{
    public static PACSProvider getProvider(String type, String name, List<String> props)
        throws IllegalArgumentException, ACIException
    {
        return getProvider(PACSType.valueOf(type), name, props);
    }
    
    public static PACSProvider getProvider(PACSType type, String name, List<String> props)
        throws IllegalArgumentException, ACIException
    {
        PACSProvider provider = null;
        
        switch ( type )
        {
            case RS2:
                provider = new RS2Provider(type, name, props);
                break;
            case S2:
                provider = new S2Provider(type, name, props);
                break;
            default:
                throw new IllegalArgumentException("Invalid PACSType passed: " + type);
        }
        
        return provider;
    }
}
