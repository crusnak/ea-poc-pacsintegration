/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transact.aci.pacs;

import com.transact.aci.ACISubscription.CardFormat;
import com.transact.aci.EGEvent;
import com.transact.aci.EGEventClass;
import com.transact.aci.exception.ACIException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author crusnak
 */
public class S2Provider extends AbstractPACSProvider implements PACSProvider
{
    public S2Provider(PACSType t, String name, List<String> props)
        throws ACIException
    {
        super(t, name, props);
    }

    public S2Provider(String t, String name, List<String> props)
        throws ACIException
    {
        super(t, name, props);
    }

    @Override
    public Map<String, String> getProviderParameters(EGEvent event) 
    {
        HashMap<String,String> params = new HashMap();
        
        params.put(ARG_CONNECTION_NAME, getConnectionName());
        params.put(ARG_EVENT_DATA, event.getEventPayload());
        
        if ( event.getEventClass() == EGEventClass.Card )
        {
            params.put(ARG_CARD_FORMAT, event.getCardFormat());
        }
        
        return params;
    }

    @Override
    protected void parseProperties() 
        throws ACIException
    {
        logger.info("Parsing properties: " + properties);
        //formatMap = new HashMap();
        for ( String prop: properties )
        {
            String[] nvp = prop.split("\\=");
            if ( nvp.length < 2 )
            {
                throw new ACIException("Provider specific property values cannot be undefined: " + prop);
            }
            
            String name = nvp[0];
            String val = nvp[1];
            String pname = null;

            if ( name != null && name.trim().length() > 0 )
            {
                pname = name.trim().substring(name.trim().lastIndexOf(".") + 1);
            }

            // If the property name is not null
            if ( pname != null )
            {
                logger.debug("Parsing property: " + pname);
                CardFormat format = null;
                try
                {
                    format = CardFormat.valueOf(pname);
                }
                catch (IllegalArgumentException e) {}
                
                // Setup format list for the current format type
                if ( format != null )
                {
                    String[] formats = new String[0];
                    List<String> flist = formatMap.get(format);
                    
                    formats = val.trim().split("\\,");
                    for ( String f: formats )
                    {
                        logger.debug(format + "=" + f);
                        flist.add(f);
                    }
                }
                // Parse any non-CardFormat provider properties here
                else
                {
                    
                }
            }
        }
        
        // Validate that both mobile and plastic formats are defined for S2
        if ( formatMap.get(CardFormat.mobile).isEmpty() || formatMap.get(CardFormat.plastic).isEmpty() )
        {
            throw new ACIException("Both mobile and plastic card formats must be declared for a subscription with an S2 provider: " + connectionName);
        }
        
        logger.info("CardFormatMap=" + formatMap);
    }
    
    
    
    @Override
    public synchronized String toString() 
    {
        return "{type=" + type + ", connName=" + connectionName + ", namedKey=" + getNamedKey() + ", formats=" + formatMap + "}";
    }
    
}
