/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transact.aci.pacs;

import com.transact.aci.EGEvent;
import java.util.List;
import java.util.Map;

/**
 *
 * @author crusnak
 */
public interface PACSProvider 
{
    public static final String ARG_CONNECTION_NAME =            "PACSConnection";
    public static final String ARG_CARD_FORMAT =                "CardFormat";
    public static final String ARG_EVENT_DATA =                 "EventData";
    
    public PACSType getType();
    public void setType(PACSType type);
    public String getConnectionName();
    public void setConnectionName(String name);
    public Map<String,String> getProviderParameters(EGEvent event);
    public List<EGEvent> replicateEventsByCardFormat(EGEvent event);
}
