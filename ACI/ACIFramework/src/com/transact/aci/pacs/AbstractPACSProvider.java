/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transact.aci.pacs;

import com.blackboard.services.etl.data.JacksonJsonContainer;
import com.blackboard.services.security.object.NamedKey;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.transact.aci.ACISubscription.CardFormat;
import com.transact.aci.ACISubscriptions;
import com.transact.aci.EGEvent;
import com.transact.aci.EGEventClass;
import com.transact.aci.exception.ACIException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.impl.Log4jLoggerAdapter;

/**
 * This class is part of the ACISubscription object identifying what PACS provider connection information is used. In the POC, this info is stored
 * in the WebConnection object of the security manager.
 * 
 * @author crusnak
 */
public abstract class AbstractPACSProvider implements PACSProvider
{  
    Log4jLoggerAdapter logger = (Log4jLoggerAdapter)LoggerFactory.getLogger(AbstractPACSProvider.class);    
    
    protected PACSType type = null;
    protected String connectionName = null;
    protected List<String> properties = null;
    protected Map<CardFormat,List<String>> formatMap = new HashMap();

    protected AbstractPACSProvider(PACSType t, String name, List<String> props)
        throws ACIException
    {
        type = t;
        connectionName = name;
        properties = props;
        initializeCardFormatMap();
        parseProperties();
    }

    protected AbstractPACSProvider(String t, String name, List<String> props)
        throws IllegalArgumentException, ACIException
    {
        this(PACSType.valueOf(t), name, props);
    }
    
    private void initializeCardFormatMap()
    {
        formatMap = new HashMap();
        for ( CardFormat format: CardFormat.values() )
        {
            formatMap.put(format, new ArrayList());
        }
    }
    
    public PACSType getType() 
    {
        return type;
    }

    public void setType(PACSType type) 
    {
        this.type = type;
    }
    
    public void setType(String type)
        throws IllegalArgumentException
    {
        setType(PACSType.valueOf(type));
    }

    public String getConnectionName() 
    {
        return connectionName;
    }

    public void setConnectionName(String connectionName) 
    {
        this.connectionName = connectionName;
    }

    public NamedKey getNamedKey()
    {
        return new NamedKey(String.valueOf(type), connectionName);
    }
    
    public List<String> getPropertyList()
    {
        return properties;
    }

    public List<EGEvent> replicateEventsByCardFormat(EGEvent event) 
    {
        List<EGEvent> events = new ArrayList();
        
        if ( event != null && event.getEventClass() == EGEventClass.Card )
        {
            // If card is mobile, only replicate for each mobile format declared
            if ( event.isCardMobile() )
            {
                // Add mobile formats
                List<String> formats = formatMap.get(CardFormat.mobile);
                if ( !formats.isEmpty() )
                {
                    for ( String format: formats )
                    {
                        try
                        {
                            EGEvent newEvent = new EGEvent(event.getPayload());
                            newEvent.setCardFormat(format);
                            events.add(newEvent);
                        }
                        catch (JsonProcessingException e)
                        {
                            logger.warn("Error replicating event: " + event, e);
                        }
                    }
                }
            }
            // If card is prox, replicated for each plastic and prox format declared
            else if ( event.isCardProx() )
            {
                // Add plastic formats
                List<String> formats = formatMap.get(CardFormat.plastic);
                for ( String format: formats )
                {
                    try
                    {
                        EGEvent newEvent = new EGEvent(event.getPayload());
                        newEvent.setCardFormat(format);
                        events.add(newEvent);
                    }
                    catch (JsonProcessingException e)
                    {
                        logger.warn("Error replicating event: " + event, e);
                    }
                }

                // Add prox formats, replacing card number with serial number
                formats = formatMap.get(CardFormat.prox);
                for ( String format: formats )
                {
                    try
                    {
                        EGEvent newEvent = new EGEvent(event.getPayload());
                        JacksonJsonContainer jjc = newEvent.getPayload();
                        String idm = jjc.getStringFieldValue(EGEvent.PATH_EVENT_CARD_IDM);
                        jjc.setFieldValue(EGEvent.PATH_EVENT_CARDNUM, idm);
                        newEvent.setCardFormat(format);
                        events.add(newEvent);
                    }
                    catch (JsonProcessingException e)
                    {
                        logger.warn("Error replicating event: " + event, e);
                    }
                }
            }
            // Otherwise, only replicated for each plastic format declared
            else
            {
                List<String> formats = formatMap.get(CardFormat.plastic);
                for ( String format: formats )
                {
                    try
                    {
                        EGEvent newEvent = new EGEvent(event.getPayload());
                        newEvent.setCardFormat(format);
                        events.add(newEvent);
                    }
                    catch (JsonProcessingException e)
                    {
                        logger.warn("Error replicating event: " + event, e);
                    }
                }
            }
        }
        else
        {
            events.add(event);
        }
        
        return events;
    }

    
    
    @Override
    public abstract Map<String, String> getProviderParameters(EGEvent event);
    protected abstract void parseProperties() throws ACIException;
    
    @Override
    public String toString() 
    {
        return "{type=" + type + ", connName=" + connectionName + ", namedKey=" + getNamedKey() + ", properties: " + properties + "}";
    }
    
    
}
