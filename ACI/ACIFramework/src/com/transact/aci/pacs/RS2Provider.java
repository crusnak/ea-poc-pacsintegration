/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transact.aci.pacs;

import com.transact.aci.ACISubscription.CardFormat;
import com.transact.aci.EGEvent;
import com.transact.aci.exception.ACIException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author crusnak
 */
public class RS2Provider extends AbstractPACSProvider implements PACSProvider
{
    public static final String PROP_KEY_SUFFIX =            "pacs.rs2.key";

    public static final String ARG_PUBLIC_KEY =             "PublicKey";
    
    private String publicKey = null;
    
    public RS2Provider(PACSType t, String name, List<String> props)
        throws ACIException
    {
        super(t, name, props);
    }

    public RS2Provider(String t, String name, List<String> props)
        throws IllegalArgumentException, ACIException
    {
        super(t, name, null);
    }

    public String getPublicKey() 
    {
        return publicKey;
    }

    public void setPublicKey(String publicKey) 
    {
        this.publicKey = publicKey;
    }

    @Override
    protected void parseProperties() 
        throws ACIException
    {
        logger.info("Parsing properties: " + properties);
        
        // For RS2, iniitalize the format map to have empty values as card formats aren't used for RS2 currently
        for ( CardFormat f: CardFormat.values() )
        {
            List<String> flist = formatMap.get(f);
            flist.add("");
        }
        
        for ( String prop: properties )
        {
            String[] nvp = prop.split("\\=");
            if ( nvp.length < 2 )
            {
                throw new ACIException("Provider specific property values cannot be undefined: " + prop);
            }
            
            String name = nvp[0];
            String val = nvp[1];
            String pname = null;

            if ( name != null && name.trim().length() > 0 )
            {
                pname = name.trim().substring(name.trim().lastIndexOf(".") + 1);
            }

            // If the property name is not null
            if ( pname != null )
            {
                logger.debug("Parsing property: " + pname);
                CardFormat format = null;
                try
                {
                    format = CardFormat.valueOf(pname);
                }
                catch (IllegalArgumentException e) {}
                
                // Setup format list for the current format type. For RS2 no card formats are used currently.
                if ( format != null )
                {
                    // LOGIC for adding card format config data here
                }
                // Parse any non-CardFormat provider properties here
                else
                {
                    // Add public key
                    if ( pname.endsWith(PROP_KEY_SUFFIX) )
                    {
                        publicKey = val.trim();
                    }
                }
            }
        }
        
        // Validate that both mobile and plastic formats are defined for RS2
        //if ( formatMap.get(CardFormat.mobile).isEmpty() || formatMap.get(CardFormat.plastic).isEmpty() )
        //{
        //    throw new ACIException("Both mobile and plastic card formats must be declared for a subscription with an S2 provider: " + connectionName);
        //}
        
        logger.info("CardFormatMap=" + formatMap);
    }

    @Override
    public Map<String, String> getProviderParameters(EGEvent event) 
    {
        HashMap<String,String> params = new HashMap();
        
        params.put(ARG_CONNECTION_NAME, getConnectionName());
        params.put(ARG_EVENT_DATA, event.getEventPayload());
        params.put(ARG_PUBLIC_KEY, getPublicKey());
        
        return params;
    }
    
    
    
    @Override
    public synchronized String toString() 
    {
        return "{type=" + type + ", connName=" + connectionName + ", publicKey=" + publicKey + ", namedKey=" + getNamedKey() + "}";
    }
    
}
