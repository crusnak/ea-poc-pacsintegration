/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transact.aci;

/**
 *
 * @author crusnak
 */
public enum EGEventType 
{
    CustomerCreated,
    CustomerChanged,
    CardActivated,
    CardSuspended,
    CardRetired,
    CardUnassigned,
    AccessAssigned,
    AccessRevoked,
    CustomerExtensionsAssigned
}
