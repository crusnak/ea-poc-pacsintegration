/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.transact.aci;

import com.transact.aci.exception.ACIException;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author crusnak
 */
public class ACIStatistics
{
    private static final String NAME_TASK =         "TaskName";
    private static final String NAME_IN =           "IncomingRows";
    private static final String NAME_OUT =          "OutgoingRows";
    private static final String NAME_ERR =          "ErrorRows";
    private static final String NAME_TIME =         "ExecutionTime (s)";

    private static String LOG_DIR =                 "../logs/stats";
    private static String LOG_SUFFIX =              "-statistics";
    private static String LOG_EXT =                 ".txt";

    private Logger logger = null;
    private File appRoot = null;
    private File statsFile = null;
    private Map<String,Entry> entryMap = new LinkedHashMap();
    private String institution;

    // need to store for each task
    // - number of input rows
    // - number of output rows
    // - number of error rows
    // - task execution time

    public ACIStatistics(File root, String inst)
        throws ACIException
    {
        try
        {
            logger = Logger.getLogger(this.getClass());
            appRoot = root;
            institution = inst;
            File logdir = new File(appRoot, LOG_DIR);
            logdir.mkdirs();
            statsFile = new File(logdir, institution + LOG_SUFFIX + LOG_EXT);
            statsFile.createNewFile();
        }
        catch (IOException e)
        {
            throw new ACIException("Failed to create ACIStatistics object for " + institution, e);
        }
    }
    
    public void destroy()
    {
        logger.info("Cleaning up ACIStatistics");
        entryMap.clear();
    }

    public String getInstitutionId()
    {
        return institution;
    }

    public Entry registerEntry(String id, long time)
    {
        Entry e = null;
        
        if ( entryMap.containsKey(id))
        {
            e = entryMap.get(id);
            e.reset(time);
        }
        else
        {
            e = new Entry(id, time);
            entryMap.put(id, e);
        }
        
        return e;
    }
    
    public Entry processEntry(String id)
        throws ACIException
    {
        Entry e = null;
        
        if ( entryMap.containsKey(id) )
        {
            e = entryMap.get(id);
            e.setStatus(Entry.Status.processing);
            e.setProcess(new Date().getTime());
        }
        else
        {
            throw new ACIException("Attempt to add process statistics entry before register for " + institution + ":"  + id);
        }
        
        return e;
        
    }

    public Entry ignoreEntry(String id)
        throws ACIException
    {
        Entry e = null;
        
        if ( entryMap.containsKey(id))
        {
            e = entryMap.get(id);
            e.setStatus(Entry.Status.ignored);
            e.setComplete(new Date().getTime());
            
            outputEntry(id);
        }
        else
        {
            throw new ACIException("Attempt to add ignore statistics entry before register for " + institution + ":"  + id);
        }
        
        return e;   
    }
    
    public Entry successEntry(String id)
        throws ACIException
    {
        Entry e = null;
        
        if ( entryMap.containsKey(id))
        {
            e = entryMap.get(id);
            e.setStatus(Entry.Status.success);
            e.setComplete(new Date().getTime());
            
            outputEntry(id);
        }
        else
        {
            throw new ACIException("Attempt to add success statistics entry before register for " + institution + ":"  + id);
        }
        
        return e;   
    }

    public Entry errorEntry(String id)
        throws ACIException
    {
        Entry e = null;
        
        if ( entryMap.containsKey(id))
        {
            e = entryMap.get(id);
            e.setStatus(Entry.Status.error);
            e.setComplete(new Date().getTime());
            
            outputEntry(id);
        }
        else
        {
            throw new ACIException("Attempt to add error statistics entry before register for " + institution + ":"  + id);
        }
        
        return e;   
    }
    
    private void outputEntry(String id)
        throws ACIException
    {
        
    }
    
    private String getReadableTime(String id, long elapsed, int units)
    {
        String time = "";
        if ( elapsed == -1 )
        {
            time = "No statistics entry found for: " + institution + ":" + id;
        }
        else if ( elapsed == -2 )
        {
            time = "Statistics entry did not define process start time for: " + institution + ":" + id;
        }
        else if ( elapsed == -3 )
        {
            time = "Statistics entry did not define ignore/success/error time for: " + institution + ":" + id;
        }
        else if ( elapsed == -4 )
        {
            time = "Statistics entry did not define event time for: " + institution + ":" + id;
        }
        else
        {
            switch ( units )
            {
                case Calendar.SECOND:
                    time = ((double)elapsed)/1000.0 + " s";
                    break;
                case Calendar.MINUTE:
                    time = ((double)elapsed)/60000.0 + " m";
                    break;
                case Calendar.HOUR_OF_DAY:
                    time = ((double)elapsed)/3600000.0 + " h";
                    break;
                case Calendar.MILLISECOND:
                    time = elapsed + " ms";
                    break;
                default:
                    if ( elapsed < 1000 )
                    {
                        time = elapsed + " ms";
                    }
                    else if ( elapsed < 60000 )
                    {
                        time = ((double)elapsed)/1000.0 + " s";
                    }
                    else if ( elapsed < 3600000 )
                    {
                        time = ((double)elapsed)/60000.0 + " m";
                    }
                    else
                    {
                        time = ((double)elapsed)/3600000.0 + " h";
                    }
            }
        }
        
        return time;
    }

    public long getProcessTime(String id)
    {
        long time = 0;
        if ( entryMap.containsKey(id) )
        {
            Entry e = entryMap.get(id);
            long start = e.getProcess();
            long end = e.getComplete();
            if ( start == -1 )
            {
                time = -2;
            }
            else if ( end == -1 )
            {
                time = -3;
            }
            else
            {
                time = end - start;
            }
        }
        else
        {
            time = -1;
        }
        
        return time;
    }
    
    public String getReadableProcessTime(String id, int units)
    {
        return getReadableTime(id, getProcessTime(id), units);
    }

    public String getReadableProcessTime(String id)
    {
        return getReadableTime(id, getProcessTime(id), -1);
    }

    public long getLatencyTime(String id)
    {
        long time = 0;
        if ( entryMap.containsKey(id) )
        {
            Entry e = entryMap.get(id);
            long start = e.getStart();
            long end = e.getProcess();
            if ( start == -1 )
            {
                time = -4;
            }
            else if ( end == -1 )
            {
                time = -2;
            }
            else
            {
                time = end - start;
            }
        }
        else
        {
            time = -1;
        }
        
        return time;
    }
    
    public String getReadableLatencyTime(String id, int units)
    {
        return getReadableTime(id, getLatencyTime(id), units);
    }

    public String getReadableLatencyTime(String id)
    {
        return getReadableTime(id, getLatencyTime(id), -1);
    }

    public long getTotalTime(String id)
    {
        long time = 0;
        if ( entryMap.containsKey(id) )
        {
            Entry e = entryMap.get(id);
            long start = e.getStart();
            long end = e.getComplete();
            if ( start == -1 )
            {
                time = -4;
            }
            else if ( end == -1 )
            {
                time = -3;
            }
            else
            {
                time = end - start;
            }
        }
        else
        {
            time = -1;
        }
        
        return time;
    }
    
    public String getReadableTotalTime(String id, int units)
    {
        return getReadableTime(id, getTotalTime(id), units);
    }

    public String getReadableTotalTime(String id)
    {
        return getReadableTime(id, getTotalTime(id), -1);
    }

    public static class Entry implements Comparable<Entry>
    {
        public enum Status { created, processing, ignored, success, error };
        
        private Status status = Status.created;
        private String identifier;
        private long start = -1;
        private long process = -1;
        private long complete = -1;
        
        public Entry(String id, long s)
        {
            identifier = id;
            start = s;
            status = Status.created;
        }
        
        public void reset(long s)
        {
            start = s;
        }

        public Status getStatus() 
        {
            return status;
        }

        public void setStatus(Status status) 
        {
            this.status = status;
        }

        public String getIdentifier() 
        {
            return identifier;
        }

        public void setIdentifier(String identifier) 
        {
            this.identifier = identifier;
        }

        public long getStart() 
        {
            return start;
        }

        public void setStart(long start) 
        {
            this.start = start;
        }

        public long getProcess() 
        {
            return process;
        }

        public void setProcess(long process) 
        {
            this.process = process;
        }

        public long getComplete() 
        {
            return complete;
        }

        public void setComplete(long complete) 
        {
            this.complete = complete;
        }
        
        @Override
        public int compareTo(Entry o) 
        {
            int i = 0;
            
            if ( o != null && o.getIdentifier() != null && identifier != null )
            {
                i = identifier.compareTo(o.getIdentifier());
            }
            else if ( o != null && o.getIdentifier() != null && identifier == null )
            {
                i = -1;
            }
            else
            {
                i = 1;
            }
            
            return i;
        }

        @Override
        public String toString() 
        {
            return identifier + "(" + status + "): {start=" + start + ", process=" + process + ", complete=" + complete + "}";  
        }
        
        
    }

    public static void main(String[] args)
    {
        try
        {
            ACIStatistics stats = new ACIStatistics(new File("."), "TEST");
            System.out.println(stats.registerEntry("123", 1636044150466L));
            Thread.currentThread().sleep(1000);
            System.out.println(stats.processEntry("123"));
            Thread.currentThread().sleep(100);
            System.out.println(stats.successEntry("123"));
            System.out.println("Process time=" + stats.getReadableProcessTime("123", Calendar.MILLISECOND));
            System.out.println("Latency time=" + stats.getReadableLatencyTime("123", Calendar.MILLISECOND));
            System.out.println("Total time=" + stats.getReadableTotalTime("123", Calendar.MILLISECOND));
            System.out.println("Process time=" + stats.getReadableProcessTime("123"));
            System.out.println("Latency time=" + stats.getReadableLatencyTime("123"));
            System.out.println("Total time=" + stats.getReadableTotalTime("123"));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
