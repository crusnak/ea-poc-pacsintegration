/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transact.aci.exception;

/**
 *
 * @author crusnak
 */
public class ACIException extends Exception
{
    Object metadata = null;
    
    public ACIException()
    {
        super();
    }

    public ACIException(String message)
    {
        super(message);
    }

    public ACIException(Throwable cause)
    {
        super(cause);
    }

    public ACIException(String message, Throwable cause)
    {
        super(message, cause);
    }
    
    public ACIException(String message, Object meta)
    {
        super(message);
        metadata = meta;
    }
    
    public ACIException(Throwable cause, Object meta)
    {
        super(cause);
        metadata = meta;
    }
     
    public ACIException(String message, Throwable cause, Object meta)
    {
        super(message, cause);
        metadata = meta;
    }
    
    public void setMetadata(Object o)
    {
        metadata = o;
    }
    
    public Object getMetadata()
    {
        return metadata;
    }
    
    @Override
    public String toString()
    {
        String s = getClass().getName();
        String message = getLocalizedMessage();
        
        return (message != null) ? (s + ": " + message + (metadata!=null?": " + metadata:"")) : s + (metadata!=null?": " + metadata:"");
    }    
}
