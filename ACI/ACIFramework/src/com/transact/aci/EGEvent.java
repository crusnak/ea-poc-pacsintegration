/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transact.aci;

import com.blackboard.services.etl.data.JacksonJsonContainer;
import com.blackboard.services.etl.exception.JacksonNodeException;
import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.exc.InputCoercionException;
import com.fasterxml.jackson.databind.JsonNode;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.InvalidPathException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class is a POJO helper class built around the JSON container object that is passed as part of the EventGrid event.
 * 
 * @author crusnak
 */
public class EGEvent implements Serializable
{
    private static final long serialVersionUID =        735623421236L;

    private static String PATH_EVENT_SOURCE =           "/0/source";
    private static String PATH_EVENT_ID =               "/0/id";
    private static String PATH_EVENT_TYPE =             "/0/type";
    private static String PATH_EVENT_TIME =             "/0/time";
    public static String PATH_EVENT_CARD_TYPE =         "/0/data/type";
    public static String PATH_EVENT_CARD_IDM =          "/0/data/cardSerialNumber";
    public static String PATH_EVENT_CARDNUM =           "/0/data/cardNumber";

    public static SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
    
    private JacksonJsonContainer payload;
    
    private String source = null;
    private String id = null;
    private EGEventType type = null;
    private EGEventClass clazz = null;
    private Date time = null;
    private String cardFormat = "";
    
    public EGEvent(String json)
        throws JsonProcessingException, IOException
    {
        payload = new JacksonJsonContainer(json);
        processEvent();
    }
    
    public EGEvent(JsonNode json)
        throws JsonProcessingException
    {
        payload = new JacksonJsonContainer(json);
        processEvent();
    }
    
    public EGEvent(JacksonJsonContainer json)
        throws JsonProcessingException
    {
        payload = new JacksonJsonContainer(json);
        processEvent();
    }
    
    public String getEventSource()
    {
        return source;
    }

    public String getEventId()
    {
        return id;
    }
    
    public EGEventType getEventType()
    {
        return type;
    }
    
    public Date getEventTime()
    {
        return time;
    }
    
    public EGEventClass getEventClass()
    {
        return clazz;
    }
    
    public String getEventPayload()
    {
        return payload.toPrettyString();
    }
    
    public JacksonJsonContainer getPayload()
    {
        return payload;
    }
    
    private void setEventClass(EGEventType etype)
    {
        switch (etype)
        {
            case AccessAssigned:
            case AccessRevoked:
                clazz = EGEventClass.Access;
                break;
            case CardActivated:
            case CardRetired:
            case CardSuspended:
            case CardUnassigned:
                clazz = EGEventClass.Card;
                break;
            case CustomerChanged:
            case CustomerCreated:
            case CustomerExtensionsAssigned:
                clazz = EGEventClass.Customer;
                break;
        }
    }
    
    public boolean isCardProx()
    {
        boolean prox = false;
        try
        {
            String snum = payload.getStringFieldValue(PATH_EVENT_CARD_IDM);
            prox = snum != null && snum.trim().length() > 0;
        }
        catch (JsonProcessingException e) {}
        catch (InvalidPathException e) {}
        
        return prox;
    }
    
    public boolean isCardMobile()
    {
        boolean mobile = false;
        try
        {
            String type = payload.getStringFieldValue(PATH_EVENT_CARD_TYPE);
            mobile = type != null && "mobile".equals(type.trim().toLowerCase());
        }
        catch (JsonProcessingException e) {}
        catch (InvalidPathException e) {}
        
        return mobile;
    }
    
    public String getCardFormat()
    {
        return cardFormat;
    }
    
    public void setCardFormat(String f)
    {
        cardFormat = f;
    }
    
    private void processEvent()
        throws JsonProcessingException
    {
        try
        {
            String es = payload.getStringFieldValue(PATH_EVENT_SOURCE);
            if ( es == null || es.trim().length() == 0 )
            {
                throw new JacksonNodeException("Unable to extract \"source\" from event payload: " + PATH_EVENT_SOURCE, payload.getRootNode());
            }
            source = es;

            String eid = payload.getStringFieldValue(PATH_EVENT_ID);
            if ( eid == null || eid.trim().length() == 0 )
            {
                throw new JacksonNodeException("Unable to extract \"id\" from event payload: " + PATH_EVENT_ID, payload.getRootNode());
            }
            id = eid;

            String etype = payload.getStringFieldValue(PATH_EVENT_TYPE);
            if ( etype == null || etype.trim().length() == 0 )
            {
                throw new JacksonNodeException("Unable to extract \"type\" from event payload: " + PATH_EVENT_TYPE, payload.getRootNode());
            }
            type = EGEventType.valueOf(etype);
            setEventClass(type);

            String etime = payload.getStringFieldValue(PATH_EVENT_TIME);
            if ( etime == null || etime.trim().length() == 0 )
            {
                throw new JacksonNodeException("Unable to extract \"time\" from event payload: " + PATH_EVENT_TIME, payload.getRootNode());
            }
            time = timeFormat.parse(etime);
        }
        catch (ParseException e)
        {
            throw new JacksonNodeException("Failed to parse date using format \"" + timeFormat.toPattern() + "\" from event payload: " + PATH_EVENT_TIME, payload.getRootNode(), e);
        }
    } 
    
    public String toString()
    {
        StringBuilder out = new StringBuilder();
        
        out.append("\n{");
        out.append("EventClass=" + clazz);
        out.append(", EventType=" + type);
        out.append(", EventSource=" + source);
        out.append(", EventId=" + id);
        out.append(", EventTime=" + time);
        out.append(", IsCardMobile=" + isCardMobile());
        out.append(", IsCardProx=" + isCardProx());
        out.append(", CardFormat=" + cardFormat);
        out.append(", EventPayload=" + payload);
        out.append("}");
        
        return out.toString();
    }
    
    public static void main(String[] args)
    {
        String j = "	[{\n" +
"		\"id\":\"fbc210ba-b306-4b83-ad45-80f168ee4a87\",\n" +
"        	\"correlationId\":\"fbc210ba-b306-4b83-ad45-80f168ee4a87\",\n" +
"        	\"type\":\"CardActivated\",\n" +
"        	\"time\":\"2021-09-127T15:00:10.855+00:00\",\n" +
"        	\"source\":\"qa1v1chrisr-72fda6e7-d496-47aa-8185-662d7ea96d98\",\n" +
"        	\"subject\":\"qa1v1chrisr-72fda6e7-d496-47aa-8185-662d7ea96d98\",\n" +
"        	\"specversion\":\"v1.0\",\n" +
"        	\"dataschema\":\"v1.1\",\n" +
"        	\"subscribers\":\"qa1v1chrisr-72fda6e7-d496-47aa-8185-662d7ea96d98\",\n" +
"        	\"replayAttempt\":null,\n" +
"        	\"data\":{\n" +
"        		\"cardNumber\":\"00000990000000000000\",\n" +
"		   	\"cardSerialNumber\":\"123\",\n" +
"		   	\"status\":\"active\",\n" +
"		   	\"type\":\"mobile\",\n" +
"		   	\"isPrimary\":false,\n" +
"		   	\"activeStartDate\":\"1899-12-30T07:00:00+00:00\",\n" +
"		   	\"activeEndDate\":\"2173-10-13T07:00:00+00:00\",\n" +
"		   	\"deviceType\":\"apple:phone\",\n" +
"		   	\"customerNumber\":\"0000000000000000100001\"}\n" +
"		}]";
        
        try
        {
            System.out.println(j);
            EGEvent event = new EGEvent(j);
            System.out.println(event.getEventSource());
            System.out.println(event.getEventId());
            System.out.println(event.getEventTime());
            System.out.println(event.getEventClass());
            System.out.println(event.getEventType());
            System.out.println(event);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
