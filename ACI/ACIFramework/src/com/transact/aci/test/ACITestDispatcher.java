/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transact.aci.test;

import com.blackboard.services.utils.ResourceLoader;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.transact.aci.ACIResponse;
import com.transact.aci.ACISubscription;
import com.transact.aci.ACISubscriptions;
import com.transact.aci.EGEvent;
import com.transact.aci.MostRecentEventManager;
import com.transact.aci.exception.ACIException;
import com.transact.aci.pacs.PACSType;
import com.transact.aci.websocket.EGConsumer;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author crusnak
 */
public class ACITestDispatcher 
{
    private ACISubscriptions subscriptions = null;
    private File appRoot = null;
    private File testFile = null;
    private String jsonString = null;
    private JsonNode json = null;
    
    public ACITestDispatcher(String file)
        throws IOException, ACIException
    {
        this(file, "..");
    }
    
    public ACITestDispatcher(String file, String root)
        throws IOException, ACIException
    {
        appRoot = new File(root);
        System.out.println("AppRoot: " + appRoot.getCanonicalPath());
        testFile = ResourceLoader.findResourceAsFile(null, appRoot, file);
        subscriptions = new ACISubscriptions(testFile);
        if ( testFile != null && testFile.exists() )
        {
            System.out.println("Loading test file: " + testFile.getCanonicalPath());
            BufferedReader br = new BufferedReader(new FileReader(testFile));
            jsonString = "";
            
            try
            {
                while ( br.ready() )
                {
                    String line = br.readLine();
                    jsonString += line + "\n";
                }
            }
            finally 
            {
                br.close();
            }
            
            ObjectMapper mapper = new ObjectMapper();
            json = mapper.readTree(jsonString);
            
            System.out.println(json);
        }
        else
        {
            throw new IOException("File does not exist: " + testFile.getCanonicalPath());
        }
    }
    
    public List<EGEvent> getTestEvents()
        throws ACIException
    {
        ArrayList<EGEvent> events = new ArrayList();
        JsonNode jn = null;
        
        try
        {
            if ( json.isArray() )
            {
                Iterator<JsonNode> jit = json.iterator();
                while ( jit.hasNext() )
                {
                    jn = jit.next();
                    events.add(new EGEvent("[" + jn + "]"));
                }
            }
            else if ( json.isObject() )
            {
                jn = json;
                events.add(new EGEvent("[" + json + "]"));
            }
            else
            {
                throw new ACIException("Test JSON data is scalar. Can only support a single JSON object or an array of JSON objects.");
            }
        }
        catch (JsonProcessingException e)
        {
            throw new ACIException("Failed to process incoming json test as valid EGEvent: " + jn, e);
            
        }
        catch (IOException e)
        {
            throw new ACIException("Failed to process incoming json test as valid EGEvent: " + jn, e);
        }
        
        return events;
    }
    
    public void execute()
        throws ACIException
    {
        List<EGEvent> events = getTestEvents();
        List<EGEvent> errors = processEvents(events);
        
        // Run a single pass of error events after all events have been attempted
        System.out.println("Handling error events in random order");
        Collections.shuffle(errors);
        processEvents(errors);
    }
    
    private List<EGEvent> processEvents(List<EGEvent> events)
        throws ACIException
    {
        List<EGEvent> errors = new ArrayList();
        System.out.println("Test EGEvents: " + events);
        for ( EGEvent event: events )
        {
            String id = event.getEventSource();
            ACISubscription sub = subscriptions.getSubscription(id);
            if ( sub == null )
            {
                throw new ACIException("Failed to find subscription with id: " + id, null, subscriptions);
            }
            
            EGConsumer consumer = new EGConsumer(sub, appRoot);
            System.out.println(consumer);
            
            System.out.println("Proccessing event");
            ACIResponse response = consumer.dispatchEvent(event.getEventPayload());
            System.out.println("HELLO");
            System.out.println("ACIResponse: " + response);
            if ( response.getStatusCode() >= 500 )
            {
                // Add errored event to the end of the list so it will process after any subsequent events
                System.out.println("ACIReponse error code > 500. Adding event to the end of the list. " + response.getDescription());
                errors.add(event);
            }
        }
        
        return errors;
    }
    
    public static void main(String[] args)
    {
        try
        {
            if ( args.length != 2 )
            {
                throw new IllegalArgumentException("Usage testACI.bat <install root> <test filename from install root>");
            }
            
            String root = args[0];
            String file = args[1];
            System.out.println("Testing: " + file);
            File lf = new File(root, "resources/log4j.properties");
            PropertyConfigurator.configure(lf.getAbsolutePath());
            ACITestDispatcher test = new ACITestDispatcher(file, root);
            test.execute();            
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.exit(-1);
        }
    }
}
