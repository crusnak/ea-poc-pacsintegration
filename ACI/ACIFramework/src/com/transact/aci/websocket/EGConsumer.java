/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transact.aci.websocket;

import com.blackboard.services.etl.data.JacksonJsonContainer;
import com.blackboard.services.etl.exception.ETLException;
import com.blackboard.services.etl.process.CommandLineProcessor;
import com.blackboard.services.etl.process.InlineProcessor;
import com.blackboard.services.etl.process.Processor;
import com.blackboard.services.etl.wrapper.ETLJobWrapper;
import com.fasterxml.jackson.databind.JsonNode;
import com.microsoft.azure.relay.HybridConnectionListener;
import com.microsoft.azure.relay.RelayedHttpListenerResponse;
import com.transact.aci.ACIResponse;
import com.transact.aci.ACISubscription;
import com.transact.aci.ACISubscriptions;
import com.transact.aci.EGEvent;
import com.transact.aci.EGEventType;
import com.transact.aci.ETLManager;
import com.transact.aci.MostRecentEventManager;
import com.transact.aci.exception.ACIException;
import com.transact.aci.pacs.PACSProvider;
import com.transact.aci.pacs.PACSType;
import com.transact.aci.pacs.RS2Provider;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.nio.file.InvalidPathException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.websocket.ClientEndpoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.impl.Log4jLoggerAdapter;

/**
 *
 * @author crusnak
 */
@ClientEndpoint
public class EGConsumer 
{
    public static final String SYS_PROP_PACS_ID =                   "log.pacs.id";
    public static final String SYS_PROP_JOB_NAME =                  "log.job.name";

    public static final String JSON_ROOT =                          "event";
    //public static final String PATH_EVENT_TYPE =                    "/0/type";
    //public static final String PATH_EVENT_TIME =                    "/0/time";

    private static Log4jLoggerAdapter logger = (Log4jLoggerAdapter)LoggerFactory.getLogger(EGConsumer.class);

    private File appRoot = null;
    private HybridConnectionListener listener;
    private ACISubscription subscription;
    private String accessKeyName;
    
    
    public EGConsumer(ACISubscription sub, File root)
    {
        appRoot = root;
        
        if ( sub != null )
        {
            subscription = sub;
            accessKeyName = "ListenOnlyRule";
        }
        else
        {
            throw new IllegalArgumentException("ACISubscription cannot be null");
        }
    }
    
    public String getEndpoint() 
    {
        return subscription.getConnectionEndpoint();
    }

    protected void setEndpoint(String endpoint) 
    {
        subscription.setConnectionEndpoint(endpoint);
    }

    public String getAccessKey() 
    {
        return subscription.getSharedAccessKey();
    }

    protected void setAccessKey(String accessKey) 
    {
        subscription.setSharedAccessKey(accessKey);
    }

    public String getAccessKeyName() 
    {
        return accessKeyName;
    }

    public void setAccessKeyName(String accessKeyName) 
    {
        this.accessKeyName = accessKeyName;
    }

    public String getEntityPath() 
    {
        return subscription.getInstitutionId();
    }

    protected void setEntityPath(String entityPath)
        throws ACIException
    {
        subscription.setInstitutionId(appRoot, entityPath);
    }
    
    public String getHybridConnectionEndpoint()
    {
        StringBuilder out = new StringBuilder();
        
        out.append("Endpoint=");
        out.append(getEndpoint());
        out.append(";SharedAccessKeyName=");
        out.append(accessKeyName);
        out.append(";SharedAccessKey=");
        out.append(getAccessKey());
        out.append(";EntityPath=");
        out.append(getEntityPath());
        
        return out.toString();
    }

    public HybridConnectionListener getListener()
    {
        return listener;
    }
 
    public void openAsync(Duration d)
        throws ACIException
    {
        try
        {
            listener = new HybridConnectionListener(getHybridConnectionEndpoint());
            listener.openAsync(d).join(); 
            
            listener.setRequestHandler((context) -> 
            {
                try
                {
                    System.out.println("Got incoming request");
                    logger.info("Got incoming request");
                    ACIResponse acir = new ACIResponse(204, "No data in incoming event");
                    InputStreamReader isr = new InputStreamReader(context.getRequest().getInputStream());
                    BufferedReader br = new BufferedReader(isr);
                    while ( br.ready() )
                    {
                        // The handler here is reponsible for calling the ACIDispatcher with the event 
                        String line = br.readLine();
                        logger.debug("Request: " + line);
                        System.out.println("Request: " + line);
                        
                        acir = dispatchEvent(line);
                    }

                    System.out.println("Relay response: " + acir);
                    RelayedHttpListenerResponse response = context.getResponse();
                    response.setStatusCode(acir.getStatusCode());
                    response.setStatusDescription(acir.getDescription());
                    response.close();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            });
        }
        catch (URISyntaxException e)
        {
            throw new ACIException("Invalid connection string", e);
        }
        
    }
    
    public ACIResponse dispatchEvent(String incoming)
    {
        ACIResponse response = new ACIResponse(200, "");
        
        try
        {
            MostRecentEventManager mrem = MostRecentEventManager.getManager(appRoot.getAbsolutePath());
            ETLManager manager = ETLManager.getManager(appRoot.getAbsolutePath());
            JacksonJsonContainer json = new JacksonJsonContainer(incoming);
            EGEvent incomingEvent = new EGEvent(json);
            System.out.println("Decoding event: " + json);
            System.out.println("EventTime: " + incomingEvent.getEventTime());
            System.out.println("EventType: " + incomingEvent.getEventType());
            
            List<ACIResponse> etlResponses = new ArrayList();
            if ( subscription.getSubscribedEvents().contains(incomingEvent.getEventType()) )
            {
                // Process event for each subscribed provider
                for ( PACSProvider provider: subscription.getPACSProviders() )
                {
                    // Replicate event by card format for all Card event classes
                    for ( EGEvent event: provider.replicateEventsByCardFormat(incomingEvent) )
                    {
                        // Ensure this event is processable for this pacs and the appropriate event identifiers
                        if ( mrem.isEventProcessable(subscription, provider.getType(), event) )
                        {
                            String jobname = getETLJobName(provider.getType(), event.getEventType());
                            System.out.println("Processing event " + event.getEventType() + " for " + provider);
                            System.out.println("Preparing to load etl job: " + jobname);

                            Map<String,String> params = new HashMap();
                            params.putAll(provider.getProviderParameters(event));

                            ACIResponse etlresp = new ACIResponse(200, jobname + "=OK", provider.getType());
                            etlResponses.add(etlresp);

                            try
                            {
                                logger.info("AppRoot: " + appRoot);
                                logger.info("ETLProcessor params: " + params);
                                //ETLJobWrapper wrapper = manager.getETLWrapper(jobname);
                                //InlineProcessor ilp = new InlineProcessor(wrapper, params);
                                
                                logger.info("Executing etl job " + jobname + " for job number: " + event.getEventId());
                                InlineProcessor ilp = new InlineProcessor(appRoot.getAbsolutePath(), jobname, params, provider.getConnectionName() + "." + jobname + ".log", event.getEventId());
                                ilp.process();

                                // No fatal errors means success so update the MREM
                                mrem.updateSuccessfulEvent(subscription, provider.getType(), event);
                            }
                            catch (ETLException e)
                            {
                                e.printStackTrace();
                                logger.error("Error in ETL call: " + jobname, e);
                                etlresp.setStatusCode(500);
                                etlresp.setDescription("Failure executing etl " + jobname + ": " + e.getMessage());
                            }
                        }
                        else
                        {
                            if ( mrem.isLastCardEventRetired(subscription, provider.getType(), event) )
                            {
                                System.out.println("Last processed Card event is retired for: " + mrem.extractMREIdentifier(event));                            
                            }
                            else
                            {
                                System.out.println("Incoming event is earlier than last processed event for: " + mrem.extractMREIdentifier(event));
                            }
                        }
                    }
                }
                
                // Check the etl response calls for all subscribed pacs providers
                for ( ACIResponse aci: etlResponses )
                {
                    if ( !(aci.getStatusCode() >=200 && aci.getStatusCode() < 300) )
                    {
                        response.setStatusCode(aci.getStatusCode());
                        response.setDescription(response.getDescription() + " | " + aci.getDescription());
                    }
                    else
                    {
                        response.setDescription("OK");
                    }
                }
            }
            else
            {
                response.setStatusCode(202);
                response.setDescription("EventType " + incomingEvent.getEventType() + " not subscribed; ignored");
            }
            
            //Calendar ecal = Calendar.incomingEvent.getEventTime();
            //Date now = new Date();
            
        }
        catch (Exception e)
        {
            e.printStackTrace();
            logger.error("FATAL error processing events", e);
            response.setStatusCode(500);
            response.setDescription(e.getMessage());
        }
        
        
        logger.info("################################################################################");
        
        return response;
    }
    
    private String getETLJobName(PACSType ptype, EGEventType egtype)
    {
        return ptype + "-" + egtype;
    }
    
    @Override
    public String toString() 
    {
        return getHybridConnectionEndpoint();
    }
    
    
}
