/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transact.aci.websocket;

import com.microsoft.azure.relay.HybridConnectionChannel;
import com.microsoft.azure.relay.HybridConnectionListener;
import com.microsoft.azure.relay.RelayedHttpListenerResponse;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.time.Duration;
import javax.websocket.ClientEndpoint;
import javax.websocket.ContainerProvider;
import javax.websocket.MessageHandler;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import org.eclipse.jetty.websocket.client.WebSocketClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author crusnak
 */
@ClientEndpoint
public class WebSocketConsumer 
{
    Session userSession = null;
    private MessageHandler messageHandler;
    
    public WebSocketConsumer(URI endpointURI)
    {
        try 
        {
            WebSocketContainer container = ContainerProvider.getWebSocketContainer();
            container.connectToServer(this, endpointURI);
        } 
        catch (Exception e) 
        {
            throw new RuntimeException(e);
        }
        
    }
    
    public static void main(String[] args)
    {
        try
        {
            Logger logger = LoggerFactory.getLogger("com.transact.aci.websocket.WebSocketConsumer");
            HybridConnectionListener listener = new HybridConnectionListener("Endpoint=sb://relay-qa-us-west-camden.servicebus.windows.net/;SharedAccessKeyName=ListenOnlyRule;SharedAccessKey=MJ8BK91P5BjitfHiKRQqTBgDi0/bja9j0fLoeLZDN3A=;EntityPath=qa1v1chrisr-72fda6e7-d496-47aa-8185-662d7ea96d98"); 
            listener.openAsync(Duration.ofSeconds(60)).join(); 

            listener.setRequestHandler((context) -> 
            {
                try
                {
                    logger.info("Got incoming request");
                    System.out.println("Got incoming request");
                    InputStreamReader isr = new InputStreamReader(context.getRequest().getInputStream());
                    BufferedReader br = new BufferedReader(isr);
                    while ( br.ready() )
                    {
                        // The handler here is reponsible for calling the ACIDispatcher with the event 
                        String line = br.readLine();
                        logger.debug("Request: " + line);
                        System.out.println("Request: " + line);
                    }

                    RelayedHttpListenerResponse response = context.getResponse();
                    response.setStatusCode(202);
                    response.setStatusDescription("OK");
                    response.close();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            });

            
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
