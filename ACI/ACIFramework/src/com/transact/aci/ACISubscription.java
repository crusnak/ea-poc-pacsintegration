/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transact.aci;

import com.transact.aci.exception.ACIException;
import com.transact.aci.pacs.AbstractPACSProvider;
import com.transact.aci.pacs.PACSProvider;
import com.transact.aci.pacs.PACSProviderFactory;
import com.transact.aci.pacs.PACSType;
import com.transact.aci.pacs.RS2Provider;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author crusnak
 */
public class ACISubscription 
{
    public enum CardFormat { plastic, prox, mobile };

    private String instId;
    private String connEndpoint;
    private String accessKey;
    private String subEvents;
    private String pacsProviders;
    //private String rs2Key;
    private Map<PACSType,PACSProvider> providerMap = new HashMap();
    private ACIStatistics stats;
    
    public ACISubscription()
    {        
    }
    
    public String getInstitutionId()
    {
        return instId;
    }
    
    public void setInstitutionId(File root, String id)
        throws ACIException
    {
        instId = id;
        stats = new ACIStatistics(root, instId);
    }
    
    public ACIStatistics getStatistics()
    {
        return stats;
    }
    
    public String getConnectionEndpoint()
    {
        return connEndpoint;
    }
    
    public void setConnectionEndpoint(String ep)
    {
        connEndpoint = ep;
    }
    
    public String getSharedAccessKey()
    {
        return accessKey;
    }
    
    public void setSharedAccessKey(String key)
    {
        accessKey = key;
    }
    
    public String getPACSProvidersString()
    {
        return pacsProviders;
    }

    public List<String> getPACSProvidersNames()
    {
        List<String> names = new ArrayList();
        String[] pacs = pacsProviders.split("\\,");
        for ( String ptype: pacs )
        {
            names.add(ptype);
        }
        
        return names;
    }
    
    public PACSProvider addPACSProvider(String pacs, List<String> props)
        throws ACIException
    {
        PACSProvider provider = PACSProviderFactory.getProvider(pacs.toUpperCase(), getInstitutionId(), props);
        providerMap.put(PACSType.valueOf(pacs.toUpperCase()), provider);
        
        return provider;
    }
    
    public List<PACSProvider> getPACSProviders()
    {
        return new ArrayList(providerMap.values());
    }
    
    /*public List<PACSProvider> getPACSProviders()
    {
        List<PACSProvider> providers = new ArrayList();
        String pstring = getPACSProvidersString();
        
        if ( pstring != null )
        {
            String[] pacs = pstring.split(",");
            for ( String ptype: pacs )
            {
                PACSProvider provider = PACSProviderFactory.getProvider(ptype, getInstitutionId());
                providers.add(provider);
                
                switch ( provider.getType() )
                {
                    case RS2:
                        // Setup RS2 specific provider config data
                        String key = getRS2PublicKey();
                        if ( key == null || key.trim().length() == 0 )
                        {
                            throw new IllegalArgumentException("RS2 providers must declare a PublicKey value for API calls.");
                        }
                        
                        ((RS2Provider)provider).setPublicKey(key);
                        break;
                    case S2:
                        // No S2 specific config data at this time
                        break;
                    default:
                        throw new UnsupportedOperationException("Unsupported provider: " + provider);
                }
            }
        }
        
        return providers;
    }*/
    
    public void setPACSProvidersString(String p)
    {
        pacsProviders = p;
    }
    
    /*public String getRS2PublicKey()
    {
        return rs2Key;
    }
    
    public void setRS2PublicKey(String key)
    {
        rs2Key = key;
    } */   
    
    public String getSubscribedEventsString()
    {
        return subEvents;
    }
    
    public List<EGEventType> getSubscribedEvents()
    {
        List<EGEventType> events = new ArrayList<>();
        String estring = getSubscribedEventsString();
        
        if ( estring != null )
        {
            String[] evs = estring.split(",");
            for ( String event: evs )
            {
                events.add(EGEventType.valueOf(event));
            }
        }
        
        return events;
    }
    
    public void setSubscribedEventsString(String events)
    {
        subEvents = events;
    }
    
    public String toString()
    {
        StringBuilder out = new StringBuilder();
        
        out.append("{");
        out.append("InstitutionId=" + instId);
        out.append(", ConnectionEndpoint=" + connEndpoint);
        out.append(", AccessKey=" + accessKey);
        out.append(", SubscribedEvents=" + subEvents);
        out.append(", PACSProviders=" + getPACSProviders());
        out.append("}");
        
        return out.toString();
    }
    
}
