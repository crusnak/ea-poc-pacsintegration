/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transact.aci;

import com.blackboard.services.etl.data.JacksonJsonContainer;
import com.transact.aci.exception.ACIException;
import com.transact.aci.pacs.PACSType;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.impl.Log4jLoggerAdapter;

/**
 *
 * @author crusnak
 */
public class MostRecentEventManager implements Serializable
{
    private static final long serialVersionUID =        88932341136L;
    
    private static final String MREM_FILE_PATH =        "resources/mrem.bin";
    private static final String PATH_CUSTNUM_ARRAY =    "/0/data/customerNumber";
    private static final String PATH_CARDNUM_ARRAY =    "/0/data/cardNumber";
    private static final String PATH_ACCESS_ARRAY =     "/0/data/accessName";

    private static final String PATH_CUSTNUM_OBJECT =   "/data/customerNumber";
    private static final String PATH_CARDNUM_OBJECT =   "/data/cardNumber";
    private static final String PATH_ACCESS_OBJECT =    "/data/accessName";

    private Map<String,Map<PACSType,Map<String,EGEvent>>> mreMap = new HashMap();
    
    private static MostRecentEventManager manager = null;
    
    private File appRoot = null;
    private File file = null;
    private static Log4jLoggerAdapter logger = (Log4jLoggerAdapter)LoggerFactory.getLogger(MostRecentEventManager.class);
    //private Logger logger = LoggerFactory.getLogger(MostRecentEventManager.class);
    
    private MostRecentEventManager(String root)
        throws ACIException
    {
        appRoot = new File(root);
        file = new File(appRoot, MREM_FILE_PATH);
        mreMap = readDataFromBinaryFile(file);
        System.out.println("Loaded from " + file.getAbsolutePath() + ": " + mreMap);
    }
    
    public static MostRecentEventManager getManager(String root)
        throws ACIException
    {
        if ( manager == null )
        {
            manager = new MostRecentEventManager(root);
        }
        
        return manager;
    }
    
    public static String extractMREIdentifier(EGEvent e)
        throws IOException
    {
        String id = null;
        
        if ( e != null )
        {
            JacksonJsonContainer json = new JacksonJsonContainer(e.getEventPayload());
            String pcust = null;
            String pcard = null;
            String paccess = null;
            
            // Assign appropriate path based on if incoming event payload is stored as a singleton array or single object
            if ( json.getRootNode().isArray() )
            {
                pcust = PATH_CUSTNUM_ARRAY;
                pcard = PATH_CARDNUM_ARRAY;
                paccess = PATH_ACCESS_ARRAY;
            }
            else if ( json.getRootNode().isObject() )
            {
                pcust = PATH_CUSTNUM_OBJECT;
                pcard = PATH_CARDNUM_OBJECT;
                paccess = PATH_ACCESS_OBJECT;
            }

            switch (e.getEventClass())
            {
                case Customer:
                    id = e.getEventClass() + "|" + json.getStringFieldValue(pcust);
                    break;
                case Card:
                    String card = json.getStringFieldValue(pcard);
                    if ( e.getCardFormat() != null )
                    {
                        card += "-" + e.getCardFormat();
                    }
                    id = e.getEventClass() + "|" + json.getStringFieldValue(pcust) + "|" + card;
                    break;
                case Access:
                    id = e.getEventClass() + "|" + json.getStringFieldValue(pcust) + "|" + json.getStringFieldValue(paccess);
                    break;
            }
        }
        
        return id;
    }
    
    public Map<String,Map<PACSType,Map<String,EGEvent>>> getCurrentMap()
    {
        return mreMap;
    }
    
    public synchronized boolean isEventProcessable(ACISubscription sub, PACSType pacs, EGEvent event)
        throws IOException
    {
        boolean valid = true;
        
        synchronized (mreMap)
        {
            if ( sub != null && event != null )
            {
                initializeSubscriptionAndPACS(sub, pacs);
                Map<String,EGEvent> eventMap = mreMap.get(sub.getInstitutionId()).get(pacs);
                String id = extractMREIdentifier(event);
                logger.trace("EventMap for " + sub.getInstitutionId() + ": " + eventMap);
                logger.debug("Finding previous event " + id);
                if ( eventMap.containsKey(id) )
                {
                    EGEvent lastEvent = eventMap.get(id);
                    logger.info("Comparing " + event.getEventTime() + " to " + lastEvent.getEventTime());
                    switch ( event.getEventClass() )
                    {
                        case Customer:
                        case Access:
                            // If the new event is earlier than the last processed event, it's not processable
                            if ( event.getEventTime().getTime() <= lastEvent.getEventTime().getTime() )
                            {
                                valid = false;
                            }
                            break;
                        case Card:
                            if ( lastEvent.getEventType() == EGEventType.CardRetired )
                            {
                                // If the last card event for this identifier is retired, then nothing else can happen to that card
                                valid = false;
                            }
                            else if ( event.getEventTime().getTime() <= lastEvent.getEventTime().getTime() )
                            {
                                valid = false;
                            }

                            break;
                    }
                }
            }
        }
        
        return valid;
    }
    
    public synchronized boolean isLastCardEventRetired(ACISubscription sub, PACSType pacs, EGEvent event)
        throws IOException
    {
        boolean retired = false;
        
        synchronized (mreMap)
        {
            if ( sub != null && event != null )
            {
                initializeSubscriptionAndPACS(sub, pacs);
                Map<String,EGEvent> eventMap = mreMap.get(sub.getInstitutionId()).get(pacs);
                String id = extractMREIdentifier(event);
                if ( eventMap.containsKey(id) )
                {
                    EGEvent lastEvent = eventMap.get(id);
                    switch ( event.getEventClass() )
                    {
                        case Card:
                            if ( lastEvent.getEventType() == EGEventType.CardRetired )
                            {
                                // If the last card event for this identifier is retired, then nothing else can happen to that card
                                retired = true;
                            }
                            break;
                    }
                }
            }
        }

        return retired;
    }
    
    public synchronized void updateSuccessfulEvent(ACISubscription sub, PACSType pacs, EGEvent event)
        throws IOException, ACIException
    {
        synchronized (mreMap)
        {
            if ( sub != null )
            {
                initializeSubscriptionAndPACS(sub, pacs);
                if ( event != null )
                {
                    mreMap.get(sub.getInstitutionId()).get(pacs).put(extractMREIdentifier(event), event);
                }
            }
            
            writeDataToBinaryFile(file);
        }
    }
    
    private synchronized void initializeSubscriptionAndPACS(ACISubscription sub, PACSType pacs)
    {
        synchronized (mreMap)
        {
            Map smap = null;
            if ( sub != null && !mreMap.containsKey(sub.getInstitutionId()) )
            {
                smap = new HashMap();
                mreMap.put(sub.getInstitutionId(), smap);
            }
            else
            {
                smap = mreMap.get(sub.getInstitutionId());
            }

            if ( !smap.containsKey(pacs) )
            {
                smap.put(pacs, new HashMap());
            }       
        }
    }
    
    private synchronized void writeDataToBinaryFile(File file)
        throws ACIException, IOException
    {
        synchronized (mreMap)
        {
            try
            {
                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
                ObjectOutputStream oos = new ObjectOutputStream(bos);
                oos.writeObject(mreMap);
                oos.flush();
                oos.close();
            }
            catch (IOException e)
            {
                throw new ACIException("Failed to write to MREM file", e, file);
            }
        }
    }
    
    private synchronized Map<String,Map<PACSType,Map<String,EGEvent>>> readDataFromBinaryFile(File file)
        throws ACIException
    {
        Map<String,Map<PACSType,Map<String,EGEvent>>> map = new HashMap();
        synchronized (mreMap)
        {
            try
            {
                if ( file.exists() )
                {
                    BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
                    ObjectInputStream ois = new ObjectInputStream(bis);
                    map = (Map<String,Map<PACSType,Map<String,EGEvent>>>)ois.readObject();
                    ois.close();
                }
            }
            catch (IOException e)
            {
                throw new ACIException("Failed to load MREM file", e, file);
            }
            catch (ClassNotFoundException e)
            {
                throw new ACIException("Failed to read MREM data object", e);
            }
        }
        return map;
    }
    
    public static void main(String[] args)
    {
        // For Customer events - identifier is Customer | CustomerNumber
        // For Card events - identifier is Card | CustomerNumber | CardNumber
        // For Access events - identifier is Access | CustomerNumber | AccessName
        
        String cust = "        [{\"data\":				           	                \n" +
"			{\"firstName\":\"Chris\",\n" +
"		   \"lastName\":\"Rusnak\",\n" +
"		   \"endDate\":\"2061-02-25T07:00:00+00:00\",\n" +
"		   \"middleName\":\"J\",\n" +
"		   \"isActive\":true,\n" +
"		   \"customerNumber\":\"0000000000000000100001\",\n" +
"		   \"startDate\":\"2012-01-01T07:00:00+00:00\"},\n" +
"       \"subject\":\"d18f30df-f17f-44e9-9a35-33dde5652cb9\",\n" +
"       \"subscribers\":\"d18f30df-f17f-44e9-9a35-33dde5652cb9\",\n" +
"       \"replayAttempt\":null,\n" +
"       \"correlationId\":\"33838abd-6692-4ede-8278-24c7d3225278\",\n" +
"       \"specversion\":\"v1.0\",\n" +
"       \"id\":\"33838abd-6692-4ede-8278-24c7d3225278\",\n" +
"       \"time\":\"2021-07-14T22:44:31.638+00:00\",\n" +
"       \"source\":\"d18f30df-f17f-44e9-9a35-33dde5652cb9\",\n" +
"       \"type\":\"CustomerChanged\",\n" +
"       \"dataschema\":\"v1.0\"}]";
        
        String card = "		[{\"id\":\"1ec0b8a8-924f-4258-9128-0693038ce746\",\n" +
"		\"correlationId\":\"1ec0b8a8-924f-4258-9128-0693038ce746\",\n" +
"		\"type\":\"CardRetired\",\n" +
"		\"time\":\"2021-07-16T16:17:18.799+00:00\",\n" +
"		\"source\":\"d18f30df-f17f-44e9-9a35-33dde5652cb9\",\n" +
"		\"subject\":\"d18f30df-f17f-44e9-9a35-33dde5652cb9\",\n" +
"		\"specversion\":\"v1.0\",\n" +
"		\"dataschema\":\"v1.1\",\n" +
"		\"subscribers\":\"d18f30df-f17f-44e9-9a35-33dde5652cb9\",\n" +
"		\"replayAttempt\":null,\n" +
"		\"data\":{\n" +
"			\"cardNumber\":\"00000990000000000000\",\n" +
"			\"cardSerialNumber\":\"\",\n" +
"			\"status\":\"Retired\",\n" +
"			\"type\":\"mobile\",\n" +
"			\"isPrimary\":false,\n" +
"			\"activeStartDate\":\"1899-12-30T07:00:00+00:00\",\n" +
"			\"activeEndDate\":\"2173-10-13T07:00:00+00:00\",\n" +
"			\"deviceType\":\"apple:phone\",\n" +
"			\"customerNumber\":\"0000000000000000100001\"}}]";

        String access = "        [{\"data\":				           	                \n" +
"			{\"customerNumber\":\"0000000000000000800017\",\n" +
"			\"accessName\": \"UNIV-STAFF\",\n" +
"			\"startDate\": \"2019-02-01T08:00:00Z\",\n" +
"			\"endDate\": \"2019-05-15T17:00:00Z\"},\n" +
"       \"subject\":\"d18f30df-f17f-44e9-9a35-33dde5652cb9\",\n" +
"       \"subscribers\":\"d18f30df-f17f-44e9-9a35-33dde5652cb9\",\n" +
"       \"replayAttempt\":null,\n" +
"       \"correlationId\":\"33838abd-6692-4ede-8278-24c7d3225278\",\n" +
"       \"specversion\":\"v1.0\",\n" +
"       \"id\":\"33838abd-6692-4ede-8278-24c7d3225278\",\n" +
"       \"time\":\"2021-07-14T22:44:31.638+00:00\",\n" +
"       \"source\":\"d18f30df-f17f-44e9-9a35-33dde5652cb9\",\n" +
"       \"type\":\"AccessAssigned\",\n" +
"       \"dataschema\":\"v1.0\"}]";
        
        try
        {
            EGEvent eventCust = new EGEvent(cust);
            EGEvent eventCard = new EGEvent(card);
            EGEvent eventAccess = new EGEvent(access);
            
            System.out.println(MostRecentEventManager.extractMREIdentifier(eventCust));
            System.out.println(MostRecentEventManager.extractMREIdentifier(eventCard));
            System.out.println(MostRecentEventManager.extractMREIdentifier(eventAccess));
        }
        catch (Exception e )
        {
            e.printStackTrace();
        }
    }
}
