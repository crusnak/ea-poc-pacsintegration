/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transact.aci;

import com.transact.aci.pacs.PACSProvider;
import com.transact.aci.pacs.PACSType;

/**
 *
 * @author crusnak
 */
public class ACIResponse 
{
    private int statusCode = 0;
    private String description = "";
    private PACSType type = null;
    
    public ACIResponse(int code, String desc)
    {
        statusCode = code;
        description = desc;
    }

    public ACIResponse(int code, String desc, PACSType prov)
    {
        statusCode = code;
        description = desc;
        type = prov;
    }

    public PACSType getPACSType() 
    {
        return type;
    }

    public void setPACSType(PACSType provider) 
    {
        this.type = provider;
    }

    public int getStatusCode() 
    {
        return statusCode;
    }

    public void setStatusCode(int statusCode) 
    {
        this.statusCode = statusCode;
    }

    public String getDescription() 
    {
        return description;
    }

    public void setDescription(String description) 
    {
        this.description = description;
    }

    @Override
    public String toString() 
    {
        return type + "=" + statusCode + "[" + description + "]";
    }
    
    
}
