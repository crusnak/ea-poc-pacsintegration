/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transact.aci;

import com.blackboard.services.utils.BaseLogger;
import com.blackboard.services.utils.BbBaseProperties;
import com.transact.aci.exception.ACIException;
import com.transact.aci.pacs.AbstractPACSProvider;
import com.transact.aci.pacs.PACSProvider;
import com.transact.aci.pacs.PACSProviderFactory;
import com.transact.aci.pacs.PACSType;
import com.transact.aci.pacs.RS2Provider;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.PropertyConfigurator;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.impl.Log4jLoggerAdapter;


/**
 *
 * @author crusnak
 */
public class ACISubscriptions extends BbBaseProperties
{
    public static final String propertyPath =                   "resources/subscriptions.txt";
    
    public static final String PROP_SUB_PREFIX =                "aci.sub([0-9]+)";
    public static final String PROP_PACS_PREFIX =               "aci.sub[0-9]+.pacs.([0-9a-z]+)";
    public static final String PROP_INST_ID_SUFFIX =            "institution";
    public static final String PROP_CONN_ENDPOINT_SUFFIX =      "conn.endpoint";
    public static final String PROP_CONN_SHARED_KEY_SUFFIX =    "conn.key";
    public static final String PROP_PACS_SUFFIX =               "pacs";
    public static final String PROP_EVENTS_SUFFIX =             "events";
   
    private File appRoot = null;
    private Map<Integer,ACISubscription> subscriptionMap = new LinkedHashMap();
    private Map<String,Integer> institutionMap = new HashMap<>();

    //private static Logger logger = null;
    private static Log4jLoggerAdapter logger = (Log4jLoggerAdapter)LoggerFactory.getLogger(ACISubscriptions.class);
    
    public ACISubscriptions(File root)
        throws IOException, ACIException
    {
        super(null, root, propertyPath);
        //logger = LoggerFactory.getLogger(ACISubscriptions.class);
        logger.debug("ACISubscription app root: " + appRoot);
        logger.debug("ACISubscription loaded: " + propertyURL);
        if ( propertyURL != null )
        {
            logger.debug("ACISubscription path: " + propertyURL.getFile());
        }
        
        appRoot = root;
        
        parseProperties();
    }

    private void parseProperties()
        throws ACIException
    {
        Map<Integer,Map<String,List<String>>> pacsPropsMap = new HashMap();
        Enumeration props = propertyNames();
        while ( props.hasMoreElements() )
        {
            ACISubscription sub = null;
            String prop = String.valueOf(props.nextElement());
            logger.debug("Parsing: " + prop);
            Matcher m = Pattern.compile(PROP_SUB_PREFIX).matcher(prop);
            if ( m.find() ) 
            {
                // Extract subscription number to assign to subscription map
                int subnum = Integer.parseInt(m.group(1));
                logger.debug(prop + " subnum: " + subnum);
                if ( subscriptionMap.containsKey(subnum) )
                {
                    sub = subscriptionMap.get(subnum);
                }
                else
                {
                    sub = new ACISubscription();
                    subscriptionMap.put(subnum, sub);
                }
                
                // Assign property values to ACISubscription, also adding to the institution map when assigning institution id
                if ( prop.endsWith(PROP_INST_ID_SUFFIX) )
                {
                    sub.setInstitutionId(appRoot, getProperty(prop));
                    institutionMap.put(sub.getInstitutionId(), subnum);
                }
                else if ( prop.endsWith(PROP_CONN_ENDPOINT_SUFFIX) )
                {
                    sub.setConnectionEndpoint(getProperty(prop));
                }
                else if ( prop.endsWith(PROP_CONN_SHARED_KEY_SUFFIX) )
                {
                    sub.setSharedAccessKey(getProperty(prop));
                }
                else if ( prop.endsWith(PROP_EVENTS_SUFFIX) )
                {
                    sub.setSubscribedEventsString(getProperty(prop));
                }
                else if ( prop.endsWith(PROP_PACS_SUFFIX) )
                {
                    sub.setPACSProvidersString(getProperty(prop));
                }
                /*else if ( prop.endsWith(PROP_PACS_RS2_KEY_SUFFIX) )
                {
                    sub.setRS2PublicKey(getProperty(prop));
                }*/
                else
                {
                    Map<String,List<String>> pacsMap = null;
                    List<String> pacsProps = null;
                    
                    // Initialize map storing pacs specific properties
                    if ( pacsPropsMap.containsKey(subnum) )
                    {
                        pacsMap = pacsPropsMap.get(subnum);
                    }
                    else
                    {
                        pacsMap = new HashMap();
                        pacsPropsMap.put(subnum, pacsMap);
                    }

                    m = Pattern.compile(PROP_PACS_PREFIX).matcher(prop);
                    if ( m.find() ) 
                    {
                        // Extract subscription number to assign to subscription map
                        String pacs = m.group(1);
                        logger.debug(prop + " pacs: " + pacs);
                        if ( pacsMap.containsKey(pacs) )
                        {
                            pacsProps = pacsMap.get(pacs);
                        }
                        else
                        {
                            pacsProps = new ArrayList();
                            pacsMap.put(pacs, pacsProps);
                        }

                        pacsProps.add(prop + "=" + getProperty(prop));
                    }
                }
            }
            else
            {
                logger.warn(prop + " not in proper numeric subscription property format; Ignoring.");
            }
            
        }
            
        logger.info("SubscriptionMap: " + subscriptionMap);
        logger.info("InsitutionMap: " + institutionMap);
        logger.info("PacsPropsMap: " + pacsPropsMap);
        
        for ( Map.Entry<Integer,Map<String,List<String>>> entry: pacsPropsMap.entrySet() )
        {
            int subnum = entry.getKey();
            ACISubscription sub = subscriptionMap.get(subnum);
            Map<String,List<String>> pacsMap = entry.getValue();
            //for ( Map.Entry<String,List<String>> pentry: pacsMap.entrySet() )
            for ( String pacs: sub.getPACSProvidersNames() )
            {
                //String pacs = pentry.getKey();
                logger.debug("Parsing: " + pacs);
                //List<String> pacsProps = pentry.getValue();
                List<String> pacsProps = pacsMap.get(pacs.toLowerCase());
                
                logger.debug("Adding pacs specific properties to ACISubscription: " + sub);
                sub.addPACSProvider(pacs, pacsProps);
            }
        }
    }
    
    public File getApplicationRoot() 
    {
        return appRoot;
    }

    public boolean containsSubscription(String id)
    {
        return institutionMap.containsKey(id);
    }
    
    public ACISubscription getSubscription(String id)
    {
        return subscriptionMap.get(institutionMap.get(id));
    }
    
    public List<ACISubscription> getSubscriptions()
    {
        return new ArrayList(subscriptionMap.values());
    }
    
    public List<String> getSubscribedInstitutions()
    {
        return new ArrayList(institutionMap.keySet());
    }
    
    public void resetSubscriptions()
    {
        subscriptionMap.clear();
        institutionMap.clear();
    }
    
    public String toString()
    {
        return subscriptionMap.toString();
    }
    
    public static void main(String[] args)
    {
        String j = "	[{\n" +
"		\"id\":\"fbc210ba-b306-4b83-ad45-80f168ee4a87\",\n" +
"        	\"correlationId\":\"fbc210ba-b306-4b83-ad45-80f168ee4a87\",\n" +
"        	\"type\":\"CardActivated\",\n" +
"        	\"time\":\"2021-09-127T15:00:10.855+00:00\",\n" +
"        	\"source\":\"qa1v1chrisr-72fda6e7-d496-47aa-8185-662d7ea96d98\",\n" +
"        	\"subject\":\"qa1v1chrisr-72fda6e7-d496-47aa-8185-662d7ea96d98\",\n" +
"        	\"specversion\":\"v1.0\",\n" +
"        	\"dataschema\":\"v1.1\",\n" +
"        	\"subscribers\":\"qa1v1chrisr-72fda6e7-d496-47aa-8185-662d7ea96d98\",\n" +
"        	\"replayAttempt\":null,\n" +
"        	\"data\":{\n" +
"        		\"cardNumber\":\"00000990000000000000\",\n" +
"		   	\"cardSerialNumber\":\"\",\n" +
"		   	\"status\":\"active\",\n" +
"		   	\"type\":\"standard\",\n" +
"		   	\"isPrimary\":false,\n" +
"		   	\"activeStartDate\":\"1899-12-30T07:00:00+00:00\",\n" +
"		   	\"activeEndDate\":\"2173-10-13T07:00:00+00:00\",\n" +
"		   	\"deviceType\":\"apple:phone\",\n" +
"		   	\"customerNumber\":\"0000000000000000100001\"}\n" +
"		}]";
        
        try
        {
            PropertyConfigurator.configure("../resources/log4j.properties");
            ACISubscriptions subs = new ACISubscriptions(new File(".."));
            ACISubscription sub1 = subs.getSubscription("qa1v1chrisr-72fda6e7-d496-47aa-8185-662d7ea96d98");
            
            logger.info(sub1.toString());
            for ( PACSProvider p: sub1.getPACSProviders() )
            {
                List<EGEvent> events = p.replicateEventsByCardFormat(new EGEvent(j));
                logger.info(p.getType() + " replicated=" + events);
            }
            
            //sub.logger.info("Subscription: " + sub.getSubscription("qa1v1chrisr-72fda6e7-d496-47aa-8185-662d7ea96d98"));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
