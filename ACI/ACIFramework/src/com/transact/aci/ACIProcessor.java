/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transact.aci;

import static com.blackboard.services.etl.process.CommandLineProcessor.SYS_PROP_LOG_FILE;
import static com.blackboard.services.etl.process.Processor.LOG_DIR;
import static com.blackboard.services.etl.process.Processor.LOG_EXT;
import com.blackboard.services.utils.ResourceLoader;
import com.transact.aci.exception.ACIException;
import com.transact.aci.websocket.EGConsumer;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.impl.Log4jLoggerAdapter;

/**
 * This class is the entry point for the ACI Framework. The processor will read all active ACISubscription and create a WebSocketConsumer
 * against the EventGrid relay for each one. 
 * 
 * @author crusnak
 */
public class ACIProcessor 
{
    private static final String SUBSCRIPTION_FILE = "resources/subscriptions.txt";
    private static Log4jLoggerAdapter logger = (Log4jLoggerAdapter)LoggerFactory.getLogger(ACISubscriptions.class);
    
    private ACISubscriptions subscriptions = null;
    private File logFile = null;
    private File appRoot = null;
    
    public ACIProcessor(String root, String logfilename)
        throws IOException, ACIException
    {
        appRoot = new File(".");
        if ( root != null )
        {
            appRoot = new File(root);
        }

        File logdir = new File(appRoot, LOG_DIR);
        logFile = new File(logdir, logfilename + LOG_EXT);
        
        File subFile = ResourceLoader.loadResourceAsFile(appRoot, SUBSCRIPTION_FILE);
        if ( subFile == null || !subFile.exists() )
        {
            throw new MissingResourceException("Missing required resource file on classpath: " + SUBSCRIPTION_FILE, null, null);
        }
        
        // Load subscriptions 
        // TODO: handle loading multiple subscriptions
        ACISubscriptions subs = new ACISubscriptions(appRoot);
        subscriptions = subs;
        
        logger.info("Subscriptions: " + subscriptions);
        System.out.println("Subscriptions: " + subscriptions);
        System.out.println("Subscribed Institutions: " + subscriptions.getSubscribedInstitutions());
        System.out.println("--------------------------------------------------------------------------------");
        for ( ACISubscription sub: subscriptions.getSubscriptions() )
        {
            System.out.println("InstitutionId: " + sub.getInstitutionId());
            System.out.println("SubscribedEvents: " + sub.getSubscribedEvents());
            System.out.println("ImplementedPACS: " + sub.getPACSProviders());
            System.out.println("--------------------------------------------------------------------------------");
        }
    }
    
    public void process()
        throws ACIException
    {
        List<EGConsumer> consumers = new ArrayList();
        boolean activeThreads = true;
        
        // For each subscription create an event grid consumer object to listen to the event grid
        for ( ACISubscription sub: subscriptions.getSubscriptions() )
        {
            System.out.println("Processing sub: " + sub);
            EGConsumer consumer = new EGConsumer(sub, appRoot);
            System.out.println("Opening async(60): " + consumer);
            consumer.openAsync(Duration.ofSeconds(60));
            consumers.add(consumer);
        }
        
        /*try
        {
            while ( activeThreads )
            {
                Thread.sleep(60000L);
                System.out.println("Checking active consumers");
                activeThreads = false;
                for ( EGConsumer consumer: consumers )
                {
                    if ( consumer != null && consumer.getListener() != null )
                    {
                        activeThreads |= consumer.getListener().isOnline();
                        System.out.println(new Date() + ": Consumer " + consumer.getEntityPath() + " isOnline: " + consumer.getListener().isOnline());
                    }
                    else
                    {
                        System.out.println("Consumer or listener is null");
                    }
                }
            }
        }
        catch (InterruptedException e)
        {
            throw new ACIException("EGConsumer interrupted", e);
        }*/
    }
    
    public static void main(String[] args)
    {
        ACIProcessor processor = null;
        
        try
        {
            if ( args.length == 0 )
            {
                throw new IllegalArgumentException("USAGE: ACIProcessor <proc_root>");
            }
            else
            {
                File lf = new File(new File(args[0]), "resources/log4j.properties");
                PropertyConfigurator.configure(lf.getAbsolutePath());
                processor = new ACIProcessor(args[0], "aci");
                processor.process();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
