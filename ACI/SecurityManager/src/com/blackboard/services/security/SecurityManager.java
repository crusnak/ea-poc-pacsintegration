/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.security;

import com.blackboard.services.installer.PasswordField;
import com.blackboard.services.security.exception.AccessException;
import com.blackboard.services.security.exception.BbTSSecurityException;
import com.blackboard.services.security.object.EncryptedMap;
import com.blackboard.services.security.object.NamedKey;
import com.blackboard.services.utils.BaseLogger;
import com.blackboard.services.utils.ResourceLoader;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import javax.servlet.ServletContext;
import org.apache.log4j.Level;

/**
 * This is the main class responsible for initializing the SecurityManager component.
 * It also manages the password used for interactive access to all security components.
 * @author crusnak
 */
public class SecurityManager
{
    public static final NamedKey passwordKey = new NamedKey(SecuredObjectManager.class.getName(), "interactivePassword");

    private static SecurityManager instance = null;

    private ServletContext context = null;
    private File appRoot = null;
    private SecurityConfiguration config = null;

    static
    {
        BaseLogger.setLogLevel(Level.INFO);
    }

    /**
     * The default constructor.
     * @param ctx The ServletContext for finding all files associated with SecurityManager
     * in a web context.
     * @param root The application root directory used as a fallback if files are not found
     * within the web context.
     * @throws BbTSSecurityException thrown if the SecurityManager is unable to be initialized.
     */
    private SecurityManager(ServletContext ctx, File root)
        throws BbTSSecurityException
    {
        context = ctx;
        appRoot = root;

        try
        {
            config = SecurityConfiguration.initialize(context, appRoot);
        }
        catch (IOException e)
        {
            throw new BbTSSecurityException("Unable to initialize SecurityManager." , e);
        }
    }

    /**
     * Method used to initialize the SecurityManager for installation access.  If
     * the SecurityManager has already been installed for the solution using it, initialize should
     * be called instead.
     * @param root The application root directory used as a fallback if files are not found
     * within the web context.
     * @return The SecurityManager singleton.
     * @throws BbTSSecurityException thrown if there is any error initializing the SecurityManager.
     * @see #initialize(java.io.File)
     */
    public static SecurityManager prepareInstallation(File root)
        throws BbTSSecurityException
    {
        return prepareInstallation(null, root);
    }

    /**
     * Method used to initialize the SecurityManager for installation access.  If
     * the SecurityManager has already been installed for the solution using it, initialize should
     * be called instead.
     * @param ctx The ServletContext for finding all files associated with SecurityManager
     * in a web context.
     * @param root The application root directory used as a fallback if files are not found
     * within the web context.
     * @return The SecurityManager singleton.
     * @throws BbTSSecurityException thrown if there is any error initializing the SecurityManager.
     * @see #initialize(javax.servlet.ServletContext, java.io.File)
     */
    public static SecurityManager prepareInstallation(ServletContext ctx, File root)
        throws BbTSSecurityException
    {
        File configFile = null;
        BaseLogger.info("Preparing SecurityManager for installation.");
        try
        {
            configFile = ResourceLoader.findResourceAsFile(ctx, root, SecurityConfiguration.propertyFile);
        }
        catch (IOException e)
        {
            configFile = new File(root, SecurityConfiguration.propertyFile);
        }

        if ( !configFile.exists() )
        {
            BaseLogger.info(SecurityConfiguration.propertyFile + " doesn't exist. Creating empty file for installation.");
            try
            {
                configFile.createNewFile();
            }
            catch (IOException e) { throw new BbTSSecurityException("FATAL error: unable to create SecurityManager configuration file: " + configFile.getAbsolutePath()); }
        }

        return initialize(ctx, root);
    }

    /**
     * Method used to initialize the SecurityManager with the proper contexts.  This is needed so
     * the keystore, secret key, and encrypted map files can be found and processed.  If these
     * cannot be found then the security manager will be unable to work and may require a new installation.
     * @param root The application root directory used as a fallback if files are not found
     * within the web context.
     * @return The SecurityManager singleton.
     * @throws BbTSSecurityException thrown if there is any error initializing the SecurityManager.
     */
    public static SecurityManager initialize(File root)
        throws BbTSSecurityException
    {
        return initialize(null, root);
    }

    /**
     * Method used to initialize the SecurityManager with the proper contexts.  This is needed so
     * the keystore, secret key, and encrypted map files can be found and processed.  If these
     * cannot be found then the security manager will be unable to work and may require a new installation.
     * @param ctx The ServletContext for finding all files associated with SecurityManager
     * in a web context.
     * @param root The application root directory used as a fallback if files are not found
     * within the web context.
     * @return The SecurityManager singleton.
     * @throws BbTSSecurityException thrown if there is any error initializing the SecurityManager.
     */
    public static SecurityManager initialize(ServletContext ctx, File root)
        throws BbTSSecurityException
    {
        if ( instance == null )
        {
            instance = new SecurityManager(ctx, root);
        }
        return instance;
    }

    /**
     * Parameterless singleton accessor limited to package access.  Only classes within
     * this package should use this since it requires initialize or prepareInstallation to
     * have been called prior to this method access.
     * @return The SecurityManager singleton.
     * @throws BbTSSecurityException thrown if the SecurityManager hasn't been initialized properly.
     * @see #prepareInstallation(javax.servlet.ServletContext, java.io.File)
     * @see #initialize(javax.servlet.ServletContext, java.io.File)
     */
    static SecurityManager getInstance()
        throws BbTSSecurityException
    {
        if ( instance == null )
        {
            throw new BbTSSecurityException("SecurityManager not initialized.  Please initialize the " +
                                            "SecurityManager by calling SecurityManager.initialize(ServletContext,File)");
        }
        return instance;
    }

    /**
     * This method is used to get the hashed value of the interactive password defined to
     * password protect access to any SecurityManager constructs.  If this is null, it needs
     * to be defined during the installation process.  This is used to compare
     * @return The hashed password in hex format
     * @throws BbTSSecurityException thrown if the stored password is null (must be defined to enable interactive
     * access to the SecurityManager) or if there's an error accessing the encrypted map.
     */
    public String getHashedInteractivePassword()
        throws BbTSSecurityException
    {
        String pass = null;

        if ( instance == null )
        {
            throw new BbTSSecurityException("SecurityManager has not been initialized. " +
                                            "Please call SecurityManager.initialize(ServletContext, File) " +
                                            "to initialize.");
        }

        EncryptionManager eman = EncryptionManager.getInstance();
        EncryptedMap map = eman.getEncryptedMap();
        if ( map != null )
        {
            pass = (String)map.get(passwordKey);
        }

        return pass;
    }

    /**
     * This compares the given password with the stored interactive password for the
     * SecurityManager to determine if access should be granted.
     * @param password The password field object to obfuscate the cleartext password
     * @return <tt>true</tt> if the password matches the SecurityManager password.
     * @throws BbTSSecurityException thrown if there is no password defined (installation error).
     */
    public boolean verifyPassword(PasswordField password)
        throws BbTSSecurityException
    {
        boolean verified = false;
        String hash = getHashedInteractivePassword();
        if ( hash == null )
        {
            throw new AccessException("No password set for the SecurityManager, please reinstall");
        }

        String hashedpass = null;
        if ( password != null )
        {
            hashedpass = password.getHashedPasswordHexString();
        }
        verified = hash.equalsIgnoreCase(hashedpass);

        return verified;
    }

    /**
     * This method reloads the encryption manager.  This should be called if changes are made to
     * the SecurityConfiguration that require the EncryptionManager to be reloaded.
     * @throws BbTSSecurityException
     */
    public void refreshEncryptionManager()
        throws BbTSSecurityException
    {
        if ( instance == null )
        {
            throw new BbTSSecurityException("SecurityManager has not been initialized. " +
                                            "Please call SecurityManager.initialize(ServletContext, File) " +
                                            "to initialize.");
        }

        EncryptionManager eman = EncryptionManager.getInstance();
        eman.refresh();
    }

    /**
     * Encapsulates BbUtilities' ResourceLoader methods with specific exception handling.
     * @param resource The resource to load
     * @return the URL pointing to the resource.
     */
    public URL findResourceAsURL(String resource)
    {
        URL url = null;
        try
        {
            url = ResourceLoader.findResourceAsURL(context, appRoot, resource);
        }
        catch (IOException e)
        {
            BaseLogger.warn("Unable to find resource: " + resource);
        }
        return url;
    }

    /**
     * Encapsulates BbUtilities' ResourceLoader methods with specific exception handling.
     * @param resource The resource to load
     * @return the File pointing to the resource.
     */
    public File findResourceAsFile(String resource)
    {
        File file = null;
        try
        {
            file = ResourceLoader.findResourceAsFile(context, appRoot, resource);
        }
        catch (IOException e)
        {
            BaseLogger.warn("Unable to find resource: " + resource);
            file = new File(appRoot, resource);
        }
        return file;
    }

    /**
     * Accessor to the application root location defined during initialization.
     * @return The application root.
     */
    public File getApplicationRoot()
    {
        return appRoot;
    }

    /**
     * Accessor to the web servlet context defined during initialization.
     * @return The servlet context.
     */
    public ServletContext getServletContext()
    {
        return context;
    }

    /**
     * Accessor to the SecurityConfiguration class managing all security configuration options.
     * @return The SecurityConfiguration class.
     */
    public SecurityConfiguration getConfig()
    {
        return config;
    }
}
