/* ****************************************************************************
 * (C) Copyright Blackboard Inc. 1998-2005 - All Rights Reserved
 *
 * Permission to use, copy, modify, and distribute this software
 * without prior explicit written approval is strictly prohibited.
 * Please refer to the file "copyright.html" for further important
 * copyright and licensing information.
 *
 * BLACKBOARD MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY
 * OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE, OR NON-INFRINGEMENT. BLACKBOARD SHALL NOT BE LIABLE FOR ANY
 * DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 *
 * @(#)$Id: //BbETL/Mainline/SecurityManager/src/com/blackboard/services/security/AESEncryptionUtility.java#1 $
 * Last Revised By   : $Author: crusnak $
 * Last Checked In   : $Date: 2012/10/17 $
 * Last Version      : $Revision: #1 $
 *
 * Original Author   : Brent Ryan -- bryan@blackboard.com
 * Origin            : Blackboard, inc.
 * Notes             :
 *
 * ***************************************************************************/

package com.blackboard.services.security;
import java.security.GeneralSecurityException;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * AESEncryption is used to encrypt a string using AES/CBC/PKCS5Padding
 * with a <CODE>IvParameterSpec</CODE> initialized to 0.  The string is first
 * converted to bytes, then encrypted.
 *
 * @version $Revision: #1 $, $Date: 2012/10/17 $
 * @author Blackboard, inc. --
 *           <A HREF="mailto:bryan@blackboard.com">Brent Ryan</A>
 */
public class AESEncryptionUtility {
     
    /**
     * This method will use 128 bit AES encryption with a IV parameter of 0
     * to encrypt a message using the key.
     * @param message The message to encrypt.
     * @return The encrypted byte array.
     */
    public static byte[] encrypt(String message, String key) throws GeneralSecurityException {
        byte[] encrypted = null;
        
        if(key != null && message != null) {
            // Convert the key into hex bytes
            byte[] encryption_key = AESEncryptionUtility.fromHexString(key);
            
            SecretKeySpec skeySpec = new SecretKeySpec(encryption_key, "AES");
            
            try {
                // Instantiate the cipher
                Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
                byte[] iv_spec = {
                    (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
                    (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
                    (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
                    (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00
                };
                
                IvParameterSpec iv = new IvParameterSpec(iv_spec);
                
                cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
                encrypted = cipher.doFinal(message.getBytes());
            } catch (Exception err) {
                throw new GeneralSecurityException(err);
            } //END try,catch for encryption
        } //END if(key != null)
        
        return encrypted;
    }
    
    /**
     * This method will use 128 bit AES decryption with a IV parameter of 0
     * to decrypt a message using the key.
     * @param message The message to decrypt.
     * @return The decrypted string.
     */
    public static String decrypt(byte[] message, String key) throws GeneralSecurityException {
        String decrypted = null;
        
        if(key != null && message != null) {
            // Convert the key into hex bytes
            byte[] encryption_key = AESEncryptionUtility.fromHexString(key);
            
            SecretKeySpec skeySpec = new SecretKeySpec(encryption_key, "AES");
            
            try {
                Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
                
                byte[] iv_spec = {
                    (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
                    (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
                    (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
                    (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00
                };
                
                IvParameterSpec iv = new IvParameterSpec(iv_spec);
                
                cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
                decrypted = new String(cipher.doFinal(message));
            } catch (Exception err) {
                throw new GeneralSecurityException(err);
            } //END try,catch for encryption
        } //END if(key != null)
        
        return decrypted;
    }
    
    /**
     * A utility method to convert a byte array to a HEX string.
     * @param bytes The bytes to convert to a HEX string.
     * @return The byte array returned as a HEX string.
     */
    public static String toHexString(byte bytes[]) {
        StringBuffer buffer = new StringBuffer(bytes.length * 2);
        
        for (int i = 0; i < bytes.length; i++) {
            if (((int) bytes[i] & 0xff) < 0x10)
                buffer.append("0");
            
            buffer.append(Long.toString((int) bytes[i] & 0xff, 16));
        } //END for (int i = 0; i < bytes.length; i++)
        
        return buffer.toString().toUpperCase();
        
    }
    
    /**
     * A utility method to convert a HEX string to a byte array.
     * @param hex The HEX string to convert.
     * @return The converted HEX string as a byte array.
     */
    public static byte[] fromHexString(String hex) {
        if(hex.length()==1) { hex = "0" + hex; }
        byte[] data = new byte[hex.length() / 2];
        
        for (int i = 0; i < data.length; i++) {
            String subStr = hex.substring(2 * i, 2 * i + 2);
            data[i] = (byte) Integer.parseInt(subStr, 16);
        } //END for (int i = 0; i < data.length; i++)
        
        return data;
    }
        
} //END public class AESEncryption
