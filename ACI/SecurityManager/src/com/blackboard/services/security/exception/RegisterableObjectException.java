/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.security.exception;

/**
 *
 * @author crusnak
 */
public class RegisterableObjectException extends BbTSSecurityException
{
    public RegisterableObjectException()
    {
        super();
    }

    public RegisterableObjectException(String message)
    {
        super(message);
    }

    public RegisterableObjectException(Throwable cause)
    {
        super(cause);
    }

    public RegisterableObjectException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
