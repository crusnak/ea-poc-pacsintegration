/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.security.exception;

/**
 *
 * @author crusnak
 */
public class BbTSSecurityException extends Exception
{
    public BbTSSecurityException()
    {
        super();
    }

    public BbTSSecurityException(String message)
    {
        super(message);
    }

    public BbTSSecurityException(Throwable cause)
    {
        super(cause);
    }

    public BbTSSecurityException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
