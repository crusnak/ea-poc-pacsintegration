/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.security.exception;

/**
 *
 * @author crusnak
 */
public class RegisterableCastException extends BbTSSecurityException
{
    public RegisterableCastException()
    {
        super();
    }

    public RegisterableCastException(String message)
    {
        super(message);
    }

    public RegisterableCastException(Throwable cause)
    {
        super(cause);
    }

    public RegisterableCastException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
