/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.security.exception;

/**
 *
 * @author crusnak
 */
public class AccessException extends BbTSSecurityException
{
    public AccessException()
    {
        super();
    }

    public AccessException(String message)
    {
        super(message);
    }

    public AccessException(Throwable cause)
    {
        super(cause);
    }

    public AccessException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
