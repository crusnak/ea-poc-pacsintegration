/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.security.exception;

/**
 *
 * @author crusnak
 */
public class RegistrationException extends BbTSSecurityException
{

    public RegistrationException()
    {
        super();
    }

    public RegistrationException(String message)
    {
        super(message);
    }

    public RegistrationException(Throwable cause)
    {
        super(cause);
    }

    public RegistrationException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
