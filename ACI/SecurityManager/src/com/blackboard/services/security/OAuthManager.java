/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.security;

import com.blackboard.services.security.object.WebConnection;
import com.blackboard.services.security.object.oauth.OAuthParameters;
import com.blackboard.services.security.object.oauth.OAuthSignatureParameters;
import com.blackboard.services.utils.ComparableHeader;
import com.blackboard.services.utils.ComparableNameValuePair;
import com.blackboard.services.utils.JavaUtils;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicHeaderElement;

/**
 *
 * @author crusnak
 */
public class OAuthManager 
{
    public static enum Version { oauth1, oauth2 };
    public static enum RequestMethod { GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE };
    
    public static final String ENCODING_UTF8 =              "UTF-8";
    public static final String OAUTH_HEADER_NAME =          "Authorization";
    public static final String OAUTH_HEADER_VALUE =         "OAuth";
    
    /**
     * Method to retrieve a complete OAuth temp authorization header for making a call to receive temporary credentials (token and token secret response).
     * 
     * @param v The OAuth version of the header to return. Only supports OAuth1 currently.
     * @param sigParams The OAuth signature parameters to be used to generate the authorization header.
     * @return Authorization header as a Header.
     */
    public static Header getOAuthTempAuthorizationHeader(Version v, OAuthSignatureParameters sigParams)
    {
        Header header = null;
        
        try
        {
            if ( sigParams == null )
            {
                throw new IllegalArgumentException("OAuthSignatureParameters cannot be null");
            }

            switch (v)
            {
                case oauth1:
                    OAuthParameters oparams = sigParams.getOauthParams();
                    oparams.validate();
                    
                    String baseSig = generateBaseSignature(sigParams);

                    switch ( OAuthParameters.getSignatureMethod(oparams.getSignatureMethod()) )
                    {
                        case HMAC_SHA1:
                            String secret = JavaUtils.percentEncode(sigParams.getConsumerSecret(), ENCODING_UTF8, false) + "&" + JavaUtils.percentEncode(sigParams.getTokenSecret(), ENCODING_UTF8, false);
                            oparams.setSignature(JavaUtils.percentEncode(BbKeyGenerator.HMACSHA1Hash(secret, baseSig), ENCODING_UTF8, false));
                            break;
                        case PLAINTEXT:
                            oparams.setSignature(JavaUtils.percentEncode(sigParams.getConsumerSecret(), ENCODING_UTF8, false) + "&" + JavaUtils.percentEncode(sigParams.getTokenSecret(), ENCODING_UTF8, false));
                            break;
                        case RSA_SHA1:
                            throw new UnsupportedOperationException(oparams.getSignatureMethod() + " encoding not currently supported for OAuth1.0");
                    }

                    StringBuilder headerValue = new StringBuilder();

                    headerValue.append(OAUTH_HEADER_VALUE + " ");
                    headerValue.append((oparams.getRealmHeaderString()!=null)?oparams.getRealmHeaderString() + ",":"");
                    headerValue.append(oparams.getConsumerKeyHeaderString() + ",");
                    headerValue.append(oparams.getSignatureMethodHeaderString() + ",");
                    headerValue.append(oparams.getTimestampHeaderString() + ",");
                    headerValue.append(oparams.getNonceHeaderString() + ",");
                    headerValue.append((oparams.getCallbackURIHeaderString()!=null)?oparams.getCallbackURIHeaderString() + ",":"");
                    headerValue.append((oparams.getVersionHeaderString()!=null)?oparams.getVersionHeaderString() + ",":"");
                    headerValue.append(oparams.getSignatureHeaderString());

                    header = new ComparableHeader(OAUTH_HEADER_NAME, headerValue.toString());

                    break;
                case oauth2:
                    throw new UnsupportedOperationException("OAUTH2 support not implemented yet");
            }
        }
        catch (UnsupportedEncodingException e)
        {
            throw new UnsupportedOperationException(e); // Should not be thrown since encoding defaults to UTF-8
        }
        
        return header;
    }
    
    /**
     * Alternative method to retrieve a complete OAuth temp authorization header for making a call to receive temporary credentials (token and token secret response).
     * 
     * @param rm The request method (i.e. GET, POST, etc)
     * @param v The OAuth version of the header to return. Only supports OAuth1 currently.
     * @param webinfo WebConnection object containing protected information to dynamically create the OAuthSignatureParameters needed to build the header.
     * @param contentType The content type of the request.
     * @param formData List of NameValuePairs containing the form data of the request.
     * @throws UnsupportedOperationException Exception thrown if there's a problem in building the full request URI from the WebConnection's authorization URI and temp token request URI
     * @return Authorization header as a Header.
     */
    public static Header getOAuthTempAuthorizationHeader(RequestMethod rm, Version v, WebConnection webinfo, String contentType, List<ComparableNameValuePair> formData)
    {
        Header header = null;
        
        if ( webinfo == null )
        {
            throw new IllegalArgumentException("WebConnection parameter cannot be null");
        }
        
        if ( webinfo.getTempTokenAuthorizationURI() == null )
        {
            throw new IllegalArgumentException("Authorization URI cannot be null");
        }
        
        switch (v)
        {
            case oauth1:
                OAuthParameters params = new OAuthParameters();
                params.setConsumerKey(webinfo.getConsumerKey());
                params.setSignatureMethod(webinfo.getSignatureMethod());
                params.createNonce();
                params.createTimestamp();
                params.setCallbackURI(OAuthParameters.OUT_OF_BAND);
                //params.setToken(webinfo.getTokenKey());
                params.setVersion(OAuthParameters.VERSION1);
                
                //URI fullURI = appendURIPathToURI(webinfo.getAuthorizationURI(), webinfo.getTempTokenRequestURI().toString());
                OAuthSignatureParameters sigParams = new OAuthSignatureParameters(String.valueOf(rm), webinfo.getTempTokenAuthorizationURI(), contentType, params, webinfo.getConsumerSecret(), null, formData);
                header = getOAuthTempAuthorizationHeader(Version.oauth1, sigParams);
                break;
            case oauth2:
                throw new UnsupportedOperationException("OAUTH2 support not implemented yet");
        }
        
        return header;
    }
    
    /**
     * Method to retrieve a complete OAuth real authorization header for making a call to receive temporary credentials (token and token secret response).
     * 
     * @param v The OAuth version of the header to return. Only supports OAuth1 currently.
     * @param sigParams The OAuth signature parameters to be used to generate the authorization header.
     * @return Authorization header as a Header.
     */
    public static Header getOAuthRealAuthorizationHeader(Version v, OAuthSignatureParameters sigParams)
    {
        Header header = null;
        
        try
        {
            if ( sigParams == null )
            {
                throw new IllegalArgumentException("OAuthSignatureParameters cannot be null");
            }

            switch (v)
            {
                case oauth1:
                    OAuthParameters oparams = sigParams.getOauthParams();
                    oparams.validate();
                    
                    String baseSig = generateBaseSignature(sigParams);

                    switch ( OAuthParameters.getSignatureMethod(oparams.getSignatureMethod()) )
                    {
                        case HMAC_SHA1:
                            String secret = JavaUtils.percentEncode(sigParams.getConsumerSecret(), ENCODING_UTF8, false) + "&" + JavaUtils.percentEncode(sigParams.getTokenSecret(), ENCODING_UTF8, false);
                            oparams.setSignature(JavaUtils.percentEncode(BbKeyGenerator.HMACSHA1Hash(secret, baseSig), ENCODING_UTF8, false));
                            break;
                        case PLAINTEXT:
                            oparams.setSignature(JavaUtils.percentEncode(sigParams.getConsumerSecret(), ENCODING_UTF8, false) + "&" + JavaUtils.percentEncode(sigParams.getTokenSecret(), ENCODING_UTF8, false));
                            break;
                        case RSA_SHA1:
                            throw new UnsupportedOperationException(oparams.getSignatureMethod() + " encoding not currently supported for OAuth1.0");
                    }

                    StringBuilder headerValue = new StringBuilder();

                    headerValue.append(OAUTH_HEADER_VALUE + " ");
                    headerValue.append((oparams.getRealmHeaderString()!=null)?oparams.getRealmHeaderString() + ",":"");
                    headerValue.append(oparams.getConsumerKeyHeaderString() + ",");
                    headerValue.append(oparams.getTokenHeaderString() + ",");
                    headerValue.append(oparams.getSignatureMethodHeaderString() + ",");
                    headerValue.append(oparams.getTimestampHeaderString() + ",");
                    headerValue.append(oparams.getNonceHeaderString() + ",");
                    headerValue.append((oparams.getVersionHeaderString()!=null)?oparams.getVersionHeaderString() + ",":"");
                    headerValue.append(oparams.getSignatureHeaderString());

                    header = new ComparableHeader(OAUTH_HEADER_NAME, headerValue.toString());

                    break;
                case oauth2:
                    throw new UnsupportedOperationException("OAUTH2 support not implemented yet");
            }
        }
        catch (UnsupportedEncodingException e)
        {
            throw new UnsupportedOperationException(e); // Should not be thrown since encoding defaults to UTF-8
        }
        
        return header;
    }

    /**
     * Alternative method to retrieve a complete OAuth real authorization header for making a call to receive temporary credentials (token and token secret response).
     * 
     * @param rm The request method (i.e. GET, POST, etc)
     * @param v The OAuth version of the header to return. Only supports OAuth1 currently.
     * @param webinfo WebConnection object containing protected information to dynamically create the OAuthSignatureParameters needed to build the header.
     * @param contentType The content type of the request.
     * @param formData List of NameValuePairs containing the form data of the request.
     * @throws UnsupportedOperationException Exception thrown if there's a problem in building the full request URI from the WebConnection's authorization URI and temp token request URI
     * @return Authorization header as a Header.
     */
    public static Header getOAuthRealAuthorizationHeader(RequestMethod rm, Version v, WebConnection webinfo, String contentType, List<ComparableNameValuePair> formData)
    {
        Header header = null;
        
        if ( webinfo == null )
        {
            throw new IllegalArgumentException("WebConnection parameter cannot be null");
        }
        
        if ( webinfo.getTempTokenAuthorizationURI() == null )
        {
            throw new IllegalArgumentException("Authorization URI cannot be null");
        }
        
        switch (v)
        {
            case oauth1:
                OAuthParameters params = new OAuthParameters();
                params.setConsumerKey(webinfo.getConsumerKey());
                params.setSignatureMethod(webinfo.getSignatureMethod());
                params.createNonce();
                params.createTimestamp();
                params.setToken(webinfo.getTokenKey());
                params.setVersion(OAuthParameters.VERSION1);
                
                //URI fullURI = appendURIPathToURI(webinfo.getAuthorizationURI(), webinfo.getTempTokenRequestURI().toString());
                OAuthSignatureParameters sigParams = new OAuthSignatureParameters(String.valueOf(rm), webinfo.getRealTokenAuthorizationURI(), contentType, params, webinfo.getConsumerSecret(), webinfo.getTokenSecret(), formData);
                header = getOAuthRealAuthorizationHeader(Version.oauth1, sigParams);
                break;
            case oauth2:
                throw new UnsupportedOperationException("OAUTH2 support not implemented yet");
        }
        
        return header;
    }

    /**
     * Alternative method to retrieve a complete OAuth real authorization header for making a call to receive temporary credentials (token and token secret response).
     * 
     * @param rm The request method (i.e. GET, POST, etc)
     * @param v The OAuth version of the header to return. Only supports OAuth1 currently.
     * @param webinfo WebConnection object containing protected information to dynamically create the OAuthSignatureParameters needed to build the header.
     * @param contentType The content type of the request.
     * @param formData List of NameValuePairs containing the form data of the request.
     * @throws UnsupportedOperationException Exception thrown if there's a problem in building the full request URI from the WebConnection's authorization URI and temp token request URI
     * @return Authorization header as a Header.
     */
    public static Header getOAuthAPIAuthorizationHeader(RequestMethod rm, Version v, URI apiURI, WebConnection webinfo, String contentType, List<ComparableNameValuePair> formData)
    {
        Header header = null;
        
        if ( webinfo == null )
        {
            throw new IllegalArgumentException("WebConnection parameter cannot be null");
        }
        
        if ( webinfo.getTempTokenAuthorizationURI() == null )
        {
            throw new IllegalArgumentException("Authorization URI cannot be null");
        }
        
        switch (v)
        {
            case oauth1:
                OAuthParameters params = new OAuthParameters();
                params.setConsumerKey(webinfo.getConsumerKey());
                params.setSignatureMethod(webinfo.getSignatureMethod());
                params.createNonce();
                params.createTimestamp();
                params.setToken(webinfo.getTokenKey());
                params.setVersion(OAuthParameters.VERSION1);
                
                //URI fullURI = appendURIPathToURI(webinfo.getAuthorizationURI(), webinfo.getTempTokenRequestURI().toString());
                OAuthSignatureParameters sigParams = new OAuthSignatureParameters(String.valueOf(rm), apiURI, contentType, params, webinfo.getConsumerSecret(), webinfo.getTokenSecret(), formData);
                header = getOAuthRealAuthorizationHeader(Version.oauth1, sigParams);
                break;
            case oauth2:
                throw new UnsupportedOperationException("OAUTH2 support not implemented yet");
        }
        
        return header;
    }

    private static String extractBaseStringURI(URI uri)
    {
        StringBuilder out = new StringBuilder();
        
        out.append(uri.getScheme().toLowerCase() + "://");
        out.append(uri.getHost().toLowerCase());
        
        switch (uri.getScheme().toLowerCase())
        {
            case "https":
                if ( uri.getPort() != -1 && uri.getPort() != 443 )
                out.append(":" + uri.getPort());
                break;
            case "http":
                if ( uri.getPort() != -1 && uri.getPort() != 80 )
                out.append(":" + uri.getPort());
                break;
            default:
                // No port for other protocols since oauth is an http authorization method
        }
        
        out.append(uri.getPath());
        
        return out.toString();
    }
    
    private static URI appendURIPathToURI(URI source, String path)
        throws URISyntaxException
    {
        return new URI(source.getScheme(), source.getUserInfo(), source.getHost(), source.getPort(), source.getPath() + path, source.getRawQuery(), source.getFragment());
    }
    
    private static String generateBaseSignature(OAuthSignatureParameters params)
        throws UnsupportedEncodingException
    {
        StringBuilder out = new StringBuilder();
        
        // Append request method in Uppercase
        out.append(JavaUtils.percentEncode(params.getHttpMethod().toUpperCase(), ENCODING_UTF8));
        out.append("&");
        
        // The base string uri after encoding
        out.append(JavaUtils.percentEncode(extractBaseStringURI(params.getServerURI()), ENCODING_UTF8));
        out.append("&");
        
        // Sort all parameters including authorization header, query params, and form params
        List<Comparable> allParams = params.getOauthParams().getSortedParameters();
        allParams.addAll(params.getFormData());
        allParams.addAll(params.getQueryData());
        Collections.sort(allParams);
        
        String paramString = "";
        for ( int i=0; i<allParams.size(); i++ )
        {
            String p = String.valueOf(allParams.get(i));
            paramString += p;
            
            if ( i < allParams.size()-1 )
            {
                paramString += "&";
            }
        }
        
        // Encode the entire parameter string
        out.append(JavaUtils.percentEncode(paramString, ENCODING_UTF8, false));
                
        return out.toString();
    }
    
    public static void main(String[] args)
    {
        try
        {
            System.out.println(System.getProperty("java.protocol.handler.pkgs"));
            
            URL url = new URL("Https://BBTSservices.TransactSP.net:885/bbts/api");
            //URI uri = new URI("Https://BBTSservices.TransactSP.net:885/bbts/api?a=1&b=2&c");
            URI uri = new URI("https://bbtsservices.transactsp.net/bbts/api");
            
            System.out.println(uri.getPath());
            System.out.println(uri.getHost());
            System.out.println(uri.getScheme());
            System.out.println(uri.getPort());
            System.out.println(uri.getQuery());
            
            System.out.println(extractBaseStringURI(uri));
            
            WebConnection webinfo = new WebConnection("Blackboard", "TransactSP");
            webinfo.setAuthType(WebConnection.AuthType.oauth1);
            webinfo.setSignatureMethod(OAuthParameters.SignatureMethod.HMAC_SHA1);
            webinfo.setTempTokenAuthorizationURI("https://bbtsservices.transactsp.net/bbts/api/initiate");
            webinfo.setRealTokenAuthorizationURI("https://bbtsservices.transactsp.net/bbts/api/token");
            webinfo.setConsumerKey("E92C38AD-CDED-4411-B562-CA13C77E7EE5");
            webinfo.setConsumerSecret("dNS/RST5HTz9Pt2BlGQv");
            
            System.out.println(getOAuthTempAuthorizationHeader(RequestMethod.GET, Version.oauth1, webinfo, "application/x-www-form-urlencoded", new ArrayList()));
            
            OAuthParameters params = new OAuthParameters();
            params.setConsumerKey("E92C38AD-CDED-4411-B562-CA13C77E7EE5");
            params.setSignatureMethod(OAuthParameters.SignatureMethod.HMAC_SHA1);
            params.setTimestamp("1449592588");
            params.setNonce("TFatTX");
            params.setVersion(OAuthParameters.VERSION1);
            //params.setToken("kkk9d7dh3k39sjv7");
            
            ArrayList<ComparableNameValuePair> formData = new ArrayList();
                        
            OAuthSignatureParameters sigParams = new OAuthSignatureParameters("POST", new URI(uri.toASCIIString() + "/initiate"), "application/json", params, "dNS/RST5HTz9Pt2BlGQv", null, formData);
            
            System.out.println(getOAuthTempAuthorizationHeader(Version.oauth1, sigParams));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
        /*HeaderElement he = new BasicHeaderElement("oauth_consumer_key","dpf43f3p2l4k3l03");
        System.out.println(he);
        
        ArrayList<HeaderElement> elems = new ArrayList();
        elems.add(he);
        
        he = new BasicHeaderElement("oauth_token", "nnch734d00sl2jdk");
        elems.add(he);
        
        String hstring = "";
        for ( int i=0; i<elems.size(); i++ )
        {
            HeaderElement e = elems.get(i);
            hstring += e.getName() + "=\"" + e.getValue() + "\"" + (i<elems.size()-1?", ":"");
        }

        Header h = new BasicHeader("Authorization", "OAuth " + hstring);
        System.out.println(h);
        System.out.println(h.getValue());
        
        for ( HeaderElement elem: h.getElements() )
        {
            System.out.println(elem);
        }*/
    }
}
