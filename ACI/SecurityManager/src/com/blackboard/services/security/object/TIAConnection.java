/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.security.object;

import com.blackboard.services.security.exception.RegisterableObjectException;
import com.blackboard.services.utils.BaseLogger;
import java.io.Serializable;
import java.util.HashMap;

/**
 *
 * @author crusnak
 */
public class TIAConnection implements Serializable, RegisterableConnection
{
    private static final long serialVersionUID = 49046723452L;

    private NamedKey connection;
    private String tiaHost;
    private int tiaPort;
    private int terminalNumber;
    private int vendorNumber;
    private String encryptionKey;
    private boolean encryptionEnabled = false;

    public TIAConnection(String name, String key)
    {
        connection = new NamedKey(name, key);
    }

    public TIAConnection(String name, String key, String host, int port, int terminal, int vendor, String encrypt)
        throws IllegalArgumentException
    {
        this(name, key);
        tiaHost = host;
        tiaPort = port;
        terminalNumber = terminal;
        vendorNumber = vendor;
        setEncryptionKey(encrypt);
    }

    public TIAConnection(String name, String key, String host, String port, String terminal, String vendor, String encrypt)
        throws IllegalArgumentException, RegisterableObjectException
    {
        this(name, key);
        tiaHost = host;
        setTIAPort(port);
        setTerminalNumber(terminal);
        setVendorNumber(vendor);
        setEncryptionKey(encrypt);
    }

    public ConnectionType getConnectionType()
    {
        return ConnectionType.tia;
    }
    
    public NamedKey getKey()
    {
        return connection;
    }

    public void setKey(NamedKey key)
    {
        connection = key;
    }

    public void setKey(String name, String key)
    {
        setKey(new NamedKey(name, key));
    }

    public String getTIAHost()
    {
        return tiaHost;
    }

    public void setTIAHost( String host )
    {
        this.tiaHost = host;
    }

    public int getTIAPort()
    {
        return tiaPort;
    }

    public void setTIAPort( String port )
        throws RegisterableObjectException
    {
        try
        {
            this.tiaPort = Integer.parseInt(port);
        }
        catch (NumberFormatException e)
        {
            throw new RegisterableObjectException("Unable to assign TIA port as: " + port, e);
        }
    }

    public void setTIAPort( int port )
    {
        this.tiaPort = port;
    }

    public int getTerminalNumber()
    {
        return terminalNumber;
    }

    public void setTerminalNumber( String num )
        throws RegisterableObjectException
    {
        try
        {
            this.terminalNumber = Integer.parseInt(num);
        }
        catch (NumberFormatException e)
        {
            throw new RegisterableObjectException("Unable to assign terminal number as: " + num, e);
        }
    }

    public void setTerminalNumber( int num )
    {
        this.terminalNumber = num;
    }

    public int getVendorNumber()
    {
        return vendorNumber;
    }

    public void setVendorNumber( String num )
        throws RegisterableObjectException
    {
        try
        {
            this.vendorNumber = Integer.parseInt(num);
        }
        catch (NumberFormatException e)
        {
            throw new RegisterableObjectException("Unable to assign vendor number as: " + num, e);
        }
    }

    public void setVendorNumber( int num )
    {
        this.vendorNumber = num;
    }

    public String getEncryptionKey()
    {
        return encryptionKey;
    }

    public void setEncryptionKey( String key )
    {
        if ( key != null && key.trim().length() == 32 )
        {
            encryptionKey = key;
            encryptionEnabled = true;
        }
        else if ( key == null || "null".equals(key) || (key != null && key.trim().length() == 0)  )
        {
            encryptionKey = null;
            encryptionEnabled = false;
        }
        else
        {
            throw new IllegalArgumentException("Key must be either null, empty, or a 32 hex char string: " + key);
        }
    }

    public boolean isEncryptionEnabled()
    {
        return encryptionEnabled;
    }

    @Override
    public String toString()
    {
        return tiaHost + ":" + tiaPort + "|" + terminalNumber + "|" + vendorNumber + "|" + (encryptionEnabled?"encrypted(" + encryptionKey + ")":"");
    }
}
