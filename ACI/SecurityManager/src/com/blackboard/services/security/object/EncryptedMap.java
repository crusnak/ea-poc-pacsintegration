/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.security.object;

import java.io.Serializable;
import java.util.HashMap;

import java.security.KeyStore;
import javax.crypto.Cipher;

/**
 *
 * @author crusnak
 */
public class EncryptedMap extends HashMap implements Serializable
{
    private static final long serialVersionUID = 839087667342L;

    public EncryptedMap()
    {
        super();
    }

    public Object put(String name, String key, Object value)
    {
        return put(new NamedKey(name, key), value);
    }

    public Object put(NamedKey nkey, Object value)
    {
        return super.put(nkey, value);
    }

    public Object get(String name, String key)
    {
        return get(new NamedKey(name, key));
    }

    public Object get(NamedKey nkey)
    {
        return super.get(nkey);
    }

    public boolean containsKey(String name, String key)
    {
        return containsKey(new NamedKey(name, key));
    }

    public boolean containsKey(NamedKey nkey)
    {
        return super.containsKey(nkey);
    }

    public Object remove(String name, String key)
    {
        return remove(new NamedKey(name, key));
    }

    public Object remove(NamedKey nkey)
    {
        return super.remove(nkey);
    }

    /*public String toString()
    {
        String out = "";
        try
        {
            EncryptionManager eman = EncryptionManager.getInstance();
            Cipher c = BbKeyGenerator.getSymmetricCipher(Cipher.ENCRYPT_MODE, eman.getSecretKey());
            out = new String(c.doFinal(super.toString().getBytes()));
        }
        catch (Exception e) {}

        return out;
    }*/

    public static void main(String[] args)
    {
        EncryptedMap m = new EncryptedMap();
        //m.put("App1", "conn1", new com.blackboard.services.security.object.DBConnection());
        //m.put("App1", "conn2", new com.blackboard.services.security.object.DBConnection());
        System.out.println(m);
        System.out.println(m.get("App1", "conn2"));
    }
}
