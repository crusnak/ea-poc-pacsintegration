/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.security.object.oauth;

import com.blackboard.services.utils.ComparableNameValuePair;
import com.blackboard.services.utils.JavaUtils;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;

/**
 *
 * @author crusnak
 */
public class OAuthSignatureParameters 
{
    String httpMethod;
    URI serverURI;
    String contentType;
    OAuthParameters oauthParams;
    String consumerSecret;
    String tokenSecret;
    List<ComparableNameValuePair> queryData;
    List<ComparableNameValuePair> formData;
    
    public OAuthSignatureParameters(String httpMethod, URI serverURI, String contentType, OAuthParameters params, String consumerSecret, 
                                    String tokenSecret, List<ComparableNameValuePair> formData)
    {
        this.httpMethod = httpMethod;
        this.serverURI = serverURI;
        this.contentType = contentType;
        this.oauthParams = params;
        this.consumerSecret = consumerSecret;
        this.tokenSecret = tokenSecret;
        this.queryData = queryData;
        this.formData = formData;
    }

    public String getConsumerSecret() 
    {
        return (consumerSecret!=null)?consumerSecret:"";
    }

    public String getContentType() 
    {
        return contentType;
    }

    public List<Comparable> getFormData() 
    {
        ArrayList<Comparable> fd = new ArrayList();
        
        try
        {
            for ( ComparableNameValuePair pair: formData )
            {
                fd.add(new ComparableNameValuePair(JavaUtils.percentEncode(pair.getName(), "UTF-8"), JavaUtils.percentEncode(pair.getValue(), "UTF-8")));
            }
        }
        catch (UnsupportedEncodingException e)
        {
            throw new RuntimeException(e); // Should not happen at runtime
        }
        
        return fd;
    }

    public String getHttpMethod() 
    {
        return httpMethod;
    }

    public OAuthParameters getOauthParams() 
    {
        return oauthParams;
    }

    public List<Comparable> getQueryData() 
    {
        return new ArrayList<Comparable>(parseQueryString(serverURI));
    }

    public URI getServerURI() 
    {
        return serverURI;
    }

    public String getTokenSecret() 
    {
        return (tokenSecret!=null)?tokenSecret:"";
    } 
 
    public static List<ComparableNameValuePair> parseQueryString(URI uri)
    {
        ArrayList<ComparableNameValuePair> queryParams = new ArrayList();
        
        String query = uri.getQuery();
        if ( query != null )
        {
            String[] params = query.split("&");
            for ( String p: params )
            {
                try
                {
                    ComparableNameValuePair nvpair = new ComparableNameValuePair(p);
                    queryParams.add(new ComparableNameValuePair(JavaUtils.percentEncode(nvpair.getName(), "UTF-8", false), JavaUtils.percentEncode(nvpair.getValue(), "UTF-8", false)));
                }
                catch (UnsupportedEncodingException e)
                {
                    throw new RuntimeException(e); // Should not happen at runtime
                }
            }
        }
        
        return queryParams;
    }
       
}
