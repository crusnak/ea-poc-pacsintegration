/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.security.object;

import com.blackboard.services.security.object.oauth.OAuthParameters;
import com.blackboard.services.utils.BaseLogger;
import com.blackboard.services.utils.JavaUtils;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author crusnak
 */
public class WebConnection implements Serializable, RegisterableConnection
{
    public static enum AuthType { basic, digest, oauth1, s2 };

    private static final long serialVersionUID = 87697643452L;
    
    private NamedKey connection;
    private AuthType authType;
    private String username;
    private String password;
    private URI tempTokenURI;
    private URI realTokenURI;
    private URI baseURI;
    private String key;
    private String secret;
    private String tokenKey;
    private String tokenSecret;
    private boolean strictSSLCheck = true;
    private OAuthParameters.SignatureMethod sigMethod;
    
    public WebConnection()
    {
        this("", "");
    }
    
    public WebConnection(String name, String key)
    {
        connection = new NamedKey(name, key);
    }

    public WebConnection(String name, String key, String auth, String p1, String p2)
        throws IllegalArgumentException, URISyntaxException
    {
        this(name, key);
        setAuthType(auth);
        switch (authType)
        {
            case oauth1:
                tempTokenURI = new URI(p1);
                realTokenURI = new URI(p2);
                break;
            default:
                username = p1;
                password = p2;
        }
    }
    
    public ConnectionType getConnectionType()
    {
        return ConnectionType.web;
    }
    
    public AuthType getAuthType()
    {
        return authType;
    }
    
    public void setAuthType(AuthType t)
    {
        authType = t;
    }

    public void setAuthType( String auth )
        throws IllegalArgumentException
    {
        try
        {
            authType = AuthType.valueOf(auth);
        }
        catch (IllegalArgumentException e)
        {
            throw new IllegalArgumentException("Unsupported authorization type: " + auth + ", supported types: " + JavaUtils.enumToList(AuthType.class));
        }
    }
    
    public URI getTempTokenAuthorizationURI()
    {
        return tempTokenURI;
    }
    
    public void setTempTokenAuthorizationURI(URI uri)
    {
        tempTokenURI = uri;
    }
    
    public void setTempTokenAuthorizationURI(String uri)
        throws URISyntaxException
    {
        setTempTokenAuthorizationURI(new URI(uri));
    }
    
    public URI getRealTokenAuthorizationURI()
    {
        return realTokenURI;
    }
    
    public void setRealTokenAuthorizationURI(URI uri)
    {
        realTokenURI = uri;
    }
    
    public void setRealTokenAuthorizationURI(String uri)
        throws URISyntaxException 
    {
        setRealTokenAuthorizationURI(new URI(uri));
    }
    
    public URI getBaseURI()
    {
        return baseURI;
    }
    
    public void setBaseURI(URI uri)
    {
        baseURI = uri;
    }
    
    public void setBaseURI(String uri)
        throws URISyntaxException 
    {
        setBaseURI(new URI(uri));
    }

    public NamedKey getKey()
    {
        return connection;
    }

    public void setKey(NamedKey key)
    {
        connection = key;
    }

    public void setKey(String name, String key)
    {
        setKey(new NamedKey(name, key));
    }

    
    public String getPassword()
    {
        return password;
    }

    public String getStarredPassword()
    {
        return password.replaceAll(".", "*");
    }

    public void setPassword( String password )
    {
        this.password = password;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername( String username )
    {
        this.username = username;
    }
    
    public String getConsumerKey()
    {
        return key;
    }
    
    public void setConsumerKey(String k)
    {
        key = k;
    }
    
    public String getConsumerSecret()
    {
        return secret;
    }
    
    public void setConsumerSecret(String s)
    {
        secret = s;
    }
    
    public String getTokenKey()
    {
        return (tokenKey!=null)?tokenKey:"";
    }
    
    public void setTokenKey(String t)
    {
        tokenKey = t;
    }
    
    public String getTokenSecret()
    {
        return tokenSecret;
    }
    
    public void setTokenSecret(String ts)
    {
        tokenSecret = ts;
    }
    
    public boolean isStrictSSLCheckUsed()
    {
        return strictSSLCheck;
    }
    
    public void setStrictSSLCheckUsed(boolean s)
    {
        strictSSLCheck = s;
    }
    
    public OAuthParameters.SignatureMethod getSignatureMethod()
    {
        return sigMethod;
    }
    
    public void setSignatureMethod(OAuthParameters.SignatureMethod m)
    {
        sigMethod = m;
    }
    
    public void setSignatureMethod( String m )
        throws IllegalArgumentException
    {
        sigMethod = OAuthParameters.getSignatureMethod(m);
        if ( sigMethod == null )
        {
            throw new IllegalArgumentException("Unsupported signature method: " + m + ", supported types: " + JavaUtils.enumToList(OAuthParameters.SignatureMethod.class));
        }
    }
    
    public void setUnprocessedSignatureMethod( String m )
        throws IllegalArgumentException
    {
        try
        {
            setSignatureMethod(OAuthParameters.SignatureMethod.valueOf(m));
        }
        catch (Exception e)
        {
            throw new IllegalArgumentException("Unsupported signature method: " + m + ", supported types: " + JavaUtils.enumToList(OAuthParameters.SignatureMethod.class));
        }
    }

    public String toString()
    {
        StringBuilder s = new StringBuilder();
        
        switch (authType)
        {
            case oauth1:
                s.append(String.valueOf(authType) + "; sigMethod=" + sigMethod + "; baseURI=" + baseURI + "; tempTokenURI=" + tempTokenURI + "; realTokenURI=" + realTokenURI + "; consumerKey=" + key + "; consumerSecret=" + secret + "; strictSSL=" + strictSSLCheck);
                break;
            default:
                s.append(String.valueOf(authType) + "; baseURI=" + baseURI + "; strictSSL=" + strictSSLCheck);
        }
        
        return s.toString();
    }
}
