/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.security.object;

/**
 *
 * @author crusnak
 */
public interface RegisterableConnection extends RegisterableObject
{
    public ConnectionType getConnectionType();
}
