/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.security.object;

import com.blackboard.services.security.exception.RegisterableObjectException;
import com.blackboard.services.utils.BaseLogger;
import java.io.Serializable;
import java.util.HashMap;

/**
 *
 * @author crusnak
 */
public class RTIConnection implements Serializable, RegisterableConnection
{
    private static final long serialVersionUID = 129832650213L;

    private NamedKey connection;
    private String rtiHost;
    private int rtiPort;
    private int versionNumber;
    private char clientPrefix;
    private String encryptionKey;
    private String username;
    private String password;
    private int amountLength;

    public RTIConnection(String name, String key)
    {
        connection = new NamedKey(name, key);
    }

    public RTIConnection(String name, String key, String host, int port, int version, 
                         char prefix, String encrypt, String user, String pass, int len)
        throws IllegalArgumentException
    {
        this(name, key);
        rtiHost = host;
        rtiPort = port;
        versionNumber = version;
        clientPrefix = prefix;
        setEncryptionKey(encrypt);
        username = user;
        password = pass;
        amountLength = len;
    }

    public RTIConnection(String name, String key, String host, String port, String terminal, 
                         String vendor, String encrypt, String user, String pass, String len)
        throws IllegalArgumentException, RegisterableObjectException
    {
        this(name, key);
        rtiHost = host;
        setRTIPort(port);
        setVersionNumber(terminal);
        setClientPrefix(vendor);
        setEncryptionKey(encrypt);
        username = user;
        password = pass;
        setAmountFieldLength(len);
    }
 
    public ConnectionType getConnectionType()
    {
        return ConnectionType.rti;
    }
    
    public NamedKey getKey()
    {
        return connection;
    }

    public void setKey(NamedKey key)
    {
        connection = key;
    }

    public void setKey(String name, String key)
    {
        setKey(new NamedKey(name, key));
    }

    public String getRTIHost()
    {
        return rtiHost;
    }

    public void setRTIHost( String host )
    {
        this.rtiHost = host;
    }

    public int getRTIPort()
    {
        return rtiPort;
    }

    public void setRTIPort( String port )
        throws RegisterableObjectException
    {
        try
        {
            this.rtiPort = Integer.parseInt(port);
        }
        catch (NumberFormatException e)
        {
            throw new RegisterableObjectException("Unable to assign RTI port as: " + port, e);
        }
    }

    public void setRTIPort( int port )
    {
        this.rtiPort = port;
    }

    public int getVersionNumber()
    {
        return versionNumber;
    }

    public void setVersionNumber( String num )
        throws RegisterableObjectException
    {
        try
        {
            this.versionNumber = Integer.parseInt(num);
        }
        catch (NumberFormatException e)
        {
            throw new RegisterableObjectException("Unable to assign version number as: " + num, e);
        }
    }

    public void setVersionNumber( int num )
    {
        this.versionNumber = num;
    }

    public char getClientPrefix()
    {
        return clientPrefix;
    }

    public void setClientPrefix( String c )
        throws RegisterableObjectException
    {
        try
        {
            this.clientPrefix = c.charAt(0);
        }
        catch (Exception e)
        {
            throw new RegisterableObjectException("Unable to assign client prefix as: " + c, e);
        }
    }

    public void setClientPrefix( char c )
    {
        this.clientPrefix = c;
    }

    public String getEncryptionKey()
    {
        return encryptionKey;
    }

    public void setEncryptionKey( String key )
    {
        if ( key != null && key.trim().length() == 4 )
        {
            encryptionKey = key;
        }
        else 
        {
            throw new IllegalArgumentException("Key must be 4 char string: " + key);
        }
    }

    public String getPassword()
    {
        return password;
    }

    public String getStarredPassword()
    {
        return password.replaceAll(".", "*");
    }

    public void setPassword( String password )
    {
        this.password = password;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername( String username )
    {
        this.username = username;
    }
    
    public int getAmountFieldLength()
    {
        return amountLength;
    }
    
    public void setAmountFieldLength(int len)
    {
        amountLength = len;
    }

    public void setAmountFieldLength(String len)
        throws RegisterableObjectException
    {
        try
        {
            this.amountLength = Integer.parseInt(len);
        }
        catch (NumberFormatException e)
        {
            throw new RegisterableObjectException("Unable to assign amount field length as: " + len, e);
        }
    }
    
    @Override
    public String toString()
    {
        return rtiHost + ":" + rtiPort + "|" + versionNumber + "|" + clientPrefix + "|" + "encrypted(" + encryptionKey + ")";
    }
}
