/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.security.object.oauth;

import com.blackboard.services.utils.ComparableNameValuePair;
import com.blackboard.services.utils.JavaUtils;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.SortedSet;
import java.util.TreeSet;
import org.apache.http.NameValuePair;

/**
 *
 * @author crusnak
 */
public class OAuthParameters 
{
    public static final String OAUTH_REALM =                "realm";
    public static final String OAUTH_CONSUMER_KEY =         "oauth_consumer_key";
    public static final String OAUTH_SIGNATURE_METHOD =     "oauth_signature_method";
    public static final String OAUTH_TIMESTAMP =            "oauth_timestamp";
    public static final String OAUTH_NONCE =                "oauth_nonce";
    public static final String OAUTH_CALLBACK =             "oauth_callback";
    public static final String OAUTH_SIGNATURE =            "oauth_signature";
    public static final String OAUTH_TOKEN =                "oauth_token";
    public static final String OAUTH_TOKEN_SECRET =         "oauth_token_secret";
    public static final String OAUTH_VERSION =              "oauth_version";
    
    public static final String OUT_OF_BAND =                "oob";
    public static final String VERSION1 =                   "1.0";

    public static enum SignatureMethod { HMAC_SHA1, RSA_SHA1, PLAINTEXT };

    private static BiMap<String,SignatureMethod> sigMethodMap = HashBiMap.create();
    
    private Map<String,String> params = new HashMap();
    
    static
    {
        sigMethodMap.put("HMAC-SHA1", SignatureMethod.HMAC_SHA1);
        sigMethodMap.put("RSA-SHA1", SignatureMethod.RSA_SHA1);
        sigMethodMap.put("PLAINTEXT", SignatureMethod.PLAINTEXT);
    }
    
    public OAuthParameters()
    {
    }
    
    public List<Comparable> getSortedParameters()
    {
        TreeSet<ComparableNameValuePair> sorted = new TreeSet();
        
        try
        {
            for ( Map.Entry<String,String> entry: params.entrySet() )
            {
                sorted.add(new ComparableNameValuePair(JavaUtils.percentEncode(entry.getKey(), "UTF-8"), JavaUtils.percentEncode(entry.getValue(), "UTF-8")));
            }
        }
        catch (UnsupportedEncodingException e)
        {
            throw new RuntimeException(e); // Should not happen at runtime
        }
        
        return new ArrayList<Comparable>(sorted);
    }
    
    public String getRealm()
    {
        String realm = params.get(OAUTH_REALM);
        return (realm!=null)?realm:"";
    }
    
    public String getRealmHeaderString()
    {
        String h = null;
        
        // Realm is optional
        if ( getRealm().trim().length() > 0 )
        {
            h = OAUTH_REALM + "=\"" + getRealm() + "\"";
        }
        
        return h;
    }
    
    public void setRealm(String r)
    {
        params.put(OAUTH_REALM, r);
    }
    
    public String getConsumerKey()
    {
        String key = params.get(OAUTH_CONSUMER_KEY);
        return (key!=null)?key:"";
    }
    
    public String getConsumerKeyHeaderString()
    {
        return OAUTH_CONSUMER_KEY + "=\"" + getConsumerKey() + "\"";
    }
    
    public void setConsumerKey(String key)
    {
        params.put(OAUTH_CONSUMER_KEY, key);
    }
    
    public String getSignatureMethod()
    {
        String method = params.get(OAUTH_SIGNATURE_METHOD);
        return (method!=null)?method:"";
    }
    
    public String getSignatureMethodHeaderString()
    {
        return OAUTH_SIGNATURE_METHOD + "=\"" + getSignatureMethod() + "\"";              
    }
    
    public static SignatureMethod getSignatureMethod(String m)
    {
        return sigMethodMap.get(m);
    }
    
    public void setSignatureMethod(String method)
    {
        params.put(OAUTH_SIGNATURE_METHOD, method);
    }
    
    public void setSignatureMethod(SignatureMethod method)
    {
        setSignatureMethod(sigMethodMap.inverse().get(method));
    }
    
    public String getTimestamp()
    {
        String ts = params.get(OAUTH_TIMESTAMP);
        return (ts!=null)?ts:"";
    }
    
    public String getTimestampHeaderString()
    {
        return OAUTH_TIMESTAMP + "=\"" + getTimestamp() + "\"";
    }
    
    public void createTimestamp()
    {
        setTimestamp(String.valueOf(new Date().getTime()/1000));
    }
    
    public void setTimestamp(String ts)
    {
        params.put(OAUTH_TIMESTAMP, ts);
    }
    
    public String getNonce()
    {
        String n = params.get(OAUTH_NONCE);
        return (n!=null)?n:"";
    }
    
    public String getNonceHeaderString()
    {
        return OAUTH_NONCE + "=\"" + getNonce() + "\"";
    }
    
    public void createNonce()
    {
        setNonce(String.valueOf(new Random().nextInt(Integer.MAX_VALUE)));
    }
    
    public void setNonce(String n)
    {
        params.put(OAUTH_NONCE, n);
    }
    
    public String getCallbackURI()
    {
        String cb = params.get(OAUTH_CALLBACK);
        return (cb!=null)?cb:"";
    }
    
    public String getCallbackURIHeaderString()
    {
        String h = null;
        
        // Callback is optional
        if ( getCallbackURI().trim().length() > 0 )
        {
            h = OAUTH_CALLBACK + "=\"" + getCallbackURI() + "\"";
        }
        
        return h;
    }
    
    public void setCallbackURI(String uri)
    {
        if ( !OUT_OF_BAND.equals(uri) )
        {
            try
            {
                new URI(uri);
            }
            catch (URISyntaxException e)
            {
                throw new IllegalArgumentException(uri + " is not a valid URI.", e);
            }
        }
        
        params.put(OAUTH_CALLBACK, uri);
    }
    
    public String getSignature()
    {
        String sig = params.get(OAUTH_SIGNATURE);
        return (sig!=null)?sig:"";
    }
    
    public String getSignatureHeaderString()
    {
        return OAUTH_SIGNATURE + "=\"" + getSignature() + "\"";
    }
    
    public void setSignature(String sig)
    {
        params.put(OAUTH_SIGNATURE, sig);
    }
    
    public String getToken()
    {
        String tok = params.get(OAUTH_TOKEN);
        return (tok!=null)?tok:"";
    }
    
    public String getTokenHeaderString()
    {
        String h = null;
        
        // Token is optional
        if ( getToken().trim().length() > 0 )
        {
            h = OAUTH_TOKEN + "=\"" + getToken() + "\"";
        }
        
        return h;
    }
    
    public void setToken(String tok)
    {
        params.put(OAUTH_TOKEN, tok);
    }
    
    public String getVersion()
    {
        String v = params.get(OAUTH_VERSION);
        return (v!=null)?v:"";
    }
    
    public String getVersionHeaderString()
    {
        String h = null;
        
        // Version is optional
        if ( getVersion().trim().length() > 0 )
        {
            h = OAUTH_VERSION + "=\"" + getVersion() + "\"";
        }
        
        return h;
    }
    
    public void setVersion(String v)
    {
        if ( !VERSION1.equals(v) )
        {
            throw new UnsupportedOperationException("Only OAUTH Version 1.0 is currently supported");
        }
        
        params.put(OAUTH_VERSION, v);
    }
    
    public String getTokenStoreId()
    {
        return String.format(Locale.getDefault(), "%1$s|%2$s|%3$s", getTimestamp(), getNonce(), getToken());
    }
    
    public void validate()
        throws IllegalArgumentException
    {
        ArrayList<String> errors = new ArrayList();
        
        if ( getConsumerKey().trim().length() == 0 )
        {
            errors.add(OAUTH_CONSUMER_KEY + " cannot be null or empty");
        }
        
        if ( getSignatureMethod().trim().length() == 0 )
        {
            errors.add(OAUTH_SIGNATURE_METHOD + " cannot be null or empty");
        }
        
        if ( getTimestamp().trim().length() == 0 )
        {
            errors.add(OAUTH_TIMESTAMP + " cannot be null or empty");
        }
        
        if ( getNonce().trim().length() == 0 )
        {
            errors.add(OAUTH_NONCE + " cannot be null or empty");
        }
        
        if ( errors.size() > 0 )
        {
            throw new IllegalArgumentException(String.valueOf(errors).replaceAll("[\\[\\]]", ""));
        }
    }
    
    public String toString()
    {
        StringBuilder out = new StringBuilder();
        
        out.append("ConsumerKey: " + getConsumerKey() + ", ");
        out.append("SignatureMethod: " + getSignatureMethod() + ", ");
        out.append("Timestamp: " + getTimestamp() + ", ");
        out.append("Nonce: " + getNonce() + ", ");
        out.append("CallbackUri: " + getCallbackURI() + ", ");
        out.append("Signature: " + getSignature() + ", ");
        out.append("Token: " + getToken() + ", ");
        out.append("Version: " + getVersion());
        
        return out.toString();
    }
    
    public static void main(String[] args)
    {
        OAuthParameters oparams = new OAuthParameters();
        oparams.setConsumerKey("TEST");
        //oparams.createTimestamp();
        //oparams.createNonce();
        oparams.setSignatureMethod(SignatureMethod.HMAC_SHA1);
        oparams.setVersion(VERSION1);
       // oparams.setCallbackURI(OUT_OF_BAND);
        
        System.out.println(oparams);
        System.out.println(oparams.getSortedParameters());
        System.out.println(oparams.getTokenStoreId());
        
        //Date d = new Date(Long.parseLong(oparams.getTimestamp())*1000);
        //System.out.println(d);
        
        System.out.println(oparams.getSignatureHeaderString());
        oparams.validate();
    }
}