/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.security.object;

/**
 *
 * @author crusnak
 */
public enum ConnectionType
{
    database,
    email,
    tia,
    rti,
    ssh,
    web
}
