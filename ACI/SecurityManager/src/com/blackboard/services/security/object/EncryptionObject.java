/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.security.object;

import com.blackboard.services.utils.JavaUtils;
import com.blackboard.services.utils.RadixConversion;
import java.io.File;
import java.io.Serializable;
import java.nio.charset.Charset;

/**
 *
 * @author crusnak
 */
public class EncryptionObject implements Serializable, RegisterableObject
{
    public static enum Type { AES, DES3, PGP, Blowfish };
    
    private static final long serialVersionUID = 342358799735L;

    private NamedKey nkey = null;
    private Type type = null;
    private byte[] ekey = new byte[0];
    private String passphrase = "";
    private String publicKeyRingFile = null;
    private String secretKeyRingFile = null;
    
    public EncryptionObject(NamedKey nk, Type t, byte[] k, String pkfile, String skfile, String pass)
    {
        nkey = nk;
        type = t;
        ekey = k;
        publicKeyRingFile = pkfile;
        secretKeyRingFile = skfile;
        passphrase = pass;
    }
    
    public EncryptionObject(NamedKey nk, String t, String hk, String pkfile, String skfile, String pass)
        throws IllegalArgumentException
    {
        nkey = nk;
        setEncryptionType(t);
        setEncryptionHexKey(hk);
        publicKeyRingFile = pkfile;
        secretKeyRingFile = skfile;
        passphrase = pass;
    }
    
    public EncryptionObject(NamedKey nk, String t, byte[] k)
        throws IllegalArgumentException
    {
        nkey = nk;
        setEncryptionType(t);
        ekey = k;
    }
    
    public NamedKey getKey()
    {
        return nkey;
    }

    public void setKey(NamedKey key)
    {
        this.nkey = key;
    }

    public void setKey(String name, String key)
    {
        setKey(new NamedKey(name, key));
    }

    public Type getEncryptionType()
    {
        return type;
    }
    
    public String getEncryptionTypeString()
    {
        return String.valueOf(type);
    }

    public void setEncryptionType(Type t)
    {
        type = t;
    }
    
    public void setEncryptionType(String t)
        throws IllegalArgumentException
    {
        try
        {
            setEncryptionType(Type.valueOf(t));
        }
        catch (Exception e)
        {
            throw new IllegalArgumentException("Invalid encryption type: " + t + ", expected: " + JavaUtils.enumToList(Type.class));
        }
    }
    
    public byte[] getEncryptionKey()
    {
        return ekey;
    }
    
    public String getEncryptionHexKey()
    {
        return RadixConversion.bytesToHexString(ekey, false);
    }
    
    public void setEncryptionHexKey(String s)
    {
        if ( s != null )
        {
            ekey = RadixConversion.convertHexStringToBytes(s);
        }
        else
        {
            ekey = null;
        }
    }

    public String getPassphrase()
    {
        return passphrase;
    }

    public void setPassphrase(String passphrase)
    {
        this.passphrase = passphrase;
    }

    public String getPublicKeyRingFile()
    {
        return publicKeyRingFile;
    }

    public void setPublicKeyRingFile(String publicKeyRingFile)
    {
        this.publicKeyRingFile = publicKeyRingFile;
    }

    public String getSecretKeyRingFile()
    {
        return secretKeyRingFile;
    }

    public void setSecretKeyRingFile(String secretKeyRingFile)
    {
        this.secretKeyRingFile = secretKeyRingFile;
    }
    
    public String getStarredPassphrase()
    {
        return passphrase.replaceAll(".", "*");
    }
    
    public String toString()
    {
        return type + "=" + getEncryptionHexKey() + 
               (type==Type.PGP?"|PublicKeyRing=" + publicKeyRingFile + "|SecretKeyRing=" + secretKeyRingFile:"");
    }
}
