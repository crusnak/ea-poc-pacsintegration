/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.security.object;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.X509Certificate;

/**
 *
 * @author crusnak
 */
public class KeyPairCert
{
    private PublicKey publicKey;
    private PrivateKey privateKey;
    private X509Certificate cert;
    
    public KeyPairCert(PublicKey pub, PrivateKey priv, X509Certificate c)
    {
        publicKey = pub;
        privateKey = priv;
        cert = c;
    }

    public X509Certificate getCertificate()
    {
        return cert;
    }

    public void setCertificate( X509Certificate cert )
    {
        this.cert = cert;
    }

    public PrivateKey getPrivateKey()
    {
        return privateKey;
    }

    public void setPrivateKey( PrivateKey privateKey )
    {
        this.privateKey = privateKey;
    }

    public PublicKey getPublicKey()
    {
        return publicKey;
    }

    public void setPublicKey( PublicKey publicKey )
    {
        this.publicKey = publicKey;
    }
}
