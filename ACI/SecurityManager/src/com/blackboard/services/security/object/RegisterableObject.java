/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.security.object;

/**
 *
 * @author crusnak
 */
public interface RegisterableObject extends Registerable
{
    public NamedKey getKey();
    public void setKey(NamedKey key);
    public void setKey(String name, String key);
}
