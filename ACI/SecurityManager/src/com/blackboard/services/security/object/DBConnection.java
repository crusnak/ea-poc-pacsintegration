/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.security.object;

import com.blackboard.services.utils.BaseLogger;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author crusnak
 */
public class DBConnection implements Serializable, RegisterableConnection
{
    public static enum DriverType { Oracle, OracleRAC, MySQL, SQLServer, jTDS, Unidata, Postgres, Informix };

    private static final long serialVersionUID = 49046723452L;

    public static final String ORACLE_DRIVER =     "oracle.jdbc.driver.OracleDriver";
    public static final String MYSQL_DRIVER =      "com.mysql.jdbc.Driver";
    public static final String SQLSERVER_DRIVER =  "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    public static final String JTDS_DRIVER =       "net.sourceforge.jtds.jdbc.Driver";
    public static final String UNIDATA_DRIVER =    "com.ibm.u2.jdbc.UniJDBCDriver";
    public static final String POSTGRES_DRIVER =   "org.postgresql.Driver";
    public static final String INFORMIX_DRIVER =   "com.informix.jdbc.IfxDriver";

    private static HashMap<DriverType,String> driverMap = new HashMap();

    static
    {
        driverMap.put(DriverType.Oracle, ORACLE_DRIVER);
        driverMap.put(DriverType.OracleRAC, ORACLE_DRIVER);
        driverMap.put(DriverType.MySQL, MYSQL_DRIVER);
        driverMap.put(DriverType.SQLServer, SQLSERVER_DRIVER);
        driverMap.put(DriverType.jTDS, JTDS_DRIVER);
        driverMap.put(DriverType.Unidata, UNIDATA_DRIVER);
        driverMap.put(DriverType.Postgres, POSTGRES_DRIVER);
        driverMap.put(DriverType.Informix, INFORMIX_DRIVER);
    }

    private NamedKey connection;
    private DriverType jdbcDriverType;
    private String jdbcDriver;
    private String databaseHost;
    private String databasePort;
    private String databaseSid;
    private String databaseName;
    private String windowsDomain;
    private String username;
    private String password;
    private String connectionRACURL;
    private ArrayList<String> properties = new ArrayList();

    public DBConnection(String name, String key)
    {
        connection = new NamedKey(name, key);
    }

    public DBConnection(String name, String key, String driver, String host, String port, 
                        String sid, String url, String user, String pass)
        throws IllegalArgumentException
    {
        this(name, key);
        setJdbcDriver(driver);
        databaseHost = host;
        databasePort = port;
        databaseSid = sid;
        username = user;
        password = pass;
        connectionRACURL = url;
    }

    public DBConnection(String name, String key, String driver, String host, String port, 
                        String sid, String dbname, String url, String user, String pass)
        throws IllegalArgumentException
    {
        this(name, key);
        setJdbcDriver(driver);
        databaseHost = host;
        databasePort = port;
        databaseSid = sid;
        databaseName = dbname;
        username = user;
        password = pass;
        if ( jdbcDriverType == DriverType.OracleRAC )
        {
            connectionRACURL = url;
        }
        else if ( jdbcDriverType == DriverType.jTDS || jdbcDriverType == DriverType.SQLServer )
        {
            windowsDomain = url;
        }
            
    }
    public ConnectionType getConnectionType()
    {
        return ConnectionType.database;
    }
    
    public DriverType getDriverType()
    {
        return jdbcDriverType;
    }

    public NamedKey getKey()
    {
        return connection;
    }

    public void setKey(NamedKey key)
    {
        connection = key;
    }

    public void setKey(String name, String key)
    {
        setKey(new NamedKey(name, key));
    }

    public String getDatabaseHost()
    {
        return databaseHost;
    }

    public void setDatabaseHost( String databaseHost )
    {
        this.databaseHost = databaseHost;
    }

    public String getDatabasePort()
    {
        return databasePort;
    }

    public void setDatabasePort( String databasePort )
    {
        this.databasePort = databasePort;
    }

    /**
     * @deprecated Use {@link #getDatabaseInstance()}
     * @return
     */
    public String getDatabaseSid()
    {
        return databaseSid;
    }

    /**
     * @deprecated Use {@link #setDatabaseInstance(java.lang.String)}
     * @param databaseSid
     */
    public void setDatabaseSid( String databaseSid )
    {
        this.databaseSid = databaseSid;
    }

    public String getDatabaseInstance()
    {
        return databaseSid;
    }

    public void setDatabaseInstance( String databaseSid )
    {
        this.databaseSid = databaseSid;
    }
    
    public String getDatabaseName()
    {
        return databaseName;
    }
    
    public void setDatabaseName( String name )
    {
        this.databaseName = name;
    }

    public String getJdbcDriver()
    {
        return jdbcDriver;
    }

    public void setJdbcDriver( String driver )
        throws IllegalArgumentException
    {
        try
        {
            jdbcDriverType = DriverType.valueOf(driver);
            jdbcDriver = driverMap.get(jdbcDriverType);
        }
        catch (IllegalArgumentException e)
        {
            throw new IllegalArgumentException("Unsupported jdbc driver type: " + driver + ", supported types: " + driverMap.keySet());
        }
    }

    public String getWindowsDomain()
    {
        return windowsDomain;
    }
    
    public void setWindowsDomain(String d)
    {
        windowsDomain = d;
    }
    
    public String getPassword()
    {
        return password;
    }

    public String getStarredPassword()
    {
        return password.replaceAll(".", "*");
    }

    public void setPassword( String password )
    {
        this.password = password;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername( String username )
    {
        this.username = username;
    }
    
    public List<String> getProperties()
    {
        return properties;
    }
    
    public void addProperty(String prop)
    {
        this.properties.add(prop);
    }
    
    public void setProperties(List<String> props)
    {
        this.properties.clear();
        this.properties.addAll(props);
    }

    public String getConnectionURL()
    {
        String url = null;
        try
        {
            switch ( jdbcDriverType )
            {
                case Oracle:
                    if ( databaseSid != null && databaseSid.trim().length() > 0 )
                    {
                        url = "jdbc:oracle:thin:@" + databaseHost + ":" + databasePort + ":" + databaseSid;
                    }
                    else
                    {
                        url = "jdbc:oracle:thin:@" + databaseHost + ":" + databasePort + "/" + databaseName;
                    }
                    break;
                case OracleRAC:
                    url = "jdbc:oracle:thin:@" + connectionRACURL;
                    break;
                case MySQL:
                    url = "jdbc:mysql://" + databaseHost + ":" + databasePort + "/" + databaseSid;
                    break;
                case SQLServer:
                    String database = (databaseName != null && databaseName.trim().length() > 0 ? ";database=" + databaseName:"");
                    String instance = (databaseSid != null && databaseSid.trim().length() > 0 ? ";instanceName=" + databaseSid:"");
                    String domain = (windowsDomain != null && windowsDomain.trim().length() > 0 ? ";domain=" + windowsDomain:"");
                    url = "jdbc:sqlserver://" + databaseHost + ":" + databasePort + instance + database + domain + (instance.trim().length()>0 || database.trim().length()>0 || domain.trim().length()>0?";":"");
                    for (String s: properties)
                    {
                        url += s + ";";
                    }
                    break;
                case jTDS:
                    database = (databaseName != null && databaseName.trim().length() > 0 ? "/" + databaseName:"");
                    instance = (databaseSid != null && databaseSid.trim().length() > 0 ? ";instance=" + databaseSid:"");
                    domain = (windowsDomain != null && windowsDomain.trim().length() > 0 ? ";domain=" + windowsDomain:"");
                    url = "jdbc:jtds:sqlserver://" + databaseHost + ":" + databasePort + database + instance + domain + (instance.trim().length()>0 || database.trim().length()>0 || domain.trim().length()>0?";":"");
                    for (String s: properties)
                    {
                        url += s + ";";
                    }
                    break;
                case Unidata:
                    url = "jdbc:ibm-u2://" + databaseHost + ":" + databasePort + "/" + databaseName + ";dbmstype=UNIDATA";
                    break;
                case Postgres:
                    url = "jdbc:postgresql://" + databaseHost + ":" + databasePort + "/" + databaseName;
                    break;
                case Informix:
                    url = "jdbc:informix-sqli://" + databaseHost + ":" + databasePort + "/" + databaseName + ":INFORMIXSERVER=" + databaseSid;
                    break;
                default:
                    throw new IllegalArgumentException("Unsupported driver type: " + jdbcDriver);
            }
        }
        catch (NullPointerException e)
        {
            System.out.println(connection + " must be rebuilt since database connection objects have changed too much.");
        }

        return url;
    }
    
    public String getRACConnectionURL()
    {
        return connectionRACURL;
    }
    
    public void setRACConnectionURL(String url)
    {
        connectionRACURL = url;
    }

    public String toString()
    {
        return getConnectionURL();
    }
}
