/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.security.object;

import com.blackboard.services.security.exception.RegisterableObjectException;
import java.io.Serializable;

/**
 *
 * @author crusnak
 */
public class EmailConnection implements Serializable, RegisterableConnection
{
    private static final long serialVersionUID = 9034546723452L;
    
    private NamedKey connection;
    private String host = "";
    private int port = 80;
    private String user = null;
    private String pass = null;
    private boolean startTLS = false;
    private boolean sendPartial = false;
    private boolean sslEnabled = false;

    public EmailConnection(String name, String key)
    {
        connection = new NamedKey(name, key);
    }

    public EmailConnection(String name, String key, String h, int p, boolean tls,
                           boolean partial, boolean ssl, String username, String password)
        throws IllegalArgumentException
    {
        this(name, key);
        host = h;
        port = p;
        startTLS = tls;
        sendPartial = partial;
        sslEnabled = ssl;
        user = username;
        pass = password;
    }

    public EmailConnection(String name, String key, String h, String p, String tls,
                           String partial, String ssl, String username, String password)
        throws IllegalArgumentException, RegisterableObjectException
    {
        this(name, key);
        host = h;
        setPort(p);
        startTLS = Boolean.valueOf(tls);
        sendPartial = Boolean.valueOf(partial);
        sslEnabled = Boolean.valueOf(ssl);
        user = username;
        pass = password;
    }

    public ConnectionType getConnectionType()
    {
        return ConnectionType.email;
    }
    
    public NamedKey getKey()
    {
        return connection;
    }

    public void setKey(NamedKey connection)
    {
        this.connection = connection;
    }

    public void setKey(String name, String key)
    {
        setKey(new NamedKey(name, key));
    }
    
    public String getHost()
    {
        return host;
    }

    public void setHost(String host)
    {
        this.host = host;
    }

    public String getPassword()
    {
        return pass;
    }

    public void setPassword(String pass)
    {
        this.pass = pass;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public void setPort( String port )
        throws RegisterableObjectException
    {
        try
        {
            this.port = Integer.parseInt(port);
        }
        catch (NumberFormatException e)
        {
            throw new RegisterableObjectException("Unable to assign email port as: " + port, e);
        }
    }

    public boolean isSendPartial()
    {
        return sendPartial;
    }

    public void setSendPartial(boolean sendPartial)
    {
        this.sendPartial = sendPartial;
    }

    public boolean isSSLEnabled()
    {
        return sslEnabled;
    }

    public void setSSLEnabled(boolean sslEnabled)
    {
        this.sslEnabled = sslEnabled;
    }

    public boolean isStartTLS()
    {
        return startTLS;
    }

    public void setStartTLS(boolean startTLS)
    {
        this.startTLS = startTLS;
    }

    public String getUsername()
    {
        return user;
    }

    public void setUsername(String user)
    {
        this.user = user;
    }
    
    public boolean isAuthenticationRequired()
    {
        return ( user != null && user.trim().length() > 0 && pass != null && pass.trim().length() > 0 );
    }
    
    @Override
    public String toString()
    {
        return host + ":" + port + (startTLS?"|startTLS":"") + (sendPartial?"|sendPartial":"") + (sslEnabled?"|sslEnabled":"") + (isAuthenticationRequired()?"|authRequired":"");
    }
    
}
