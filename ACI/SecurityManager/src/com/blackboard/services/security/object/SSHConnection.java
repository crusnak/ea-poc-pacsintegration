/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blackboard.services.security.object;

import com.blackboard.services.security.exception.RegisterableObjectException;
import com.blackboard.services.utils.JavaUtils;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.KeyPair;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;

/**
 *
 * @author crusnak
 */
public class SSHConnection implements Serializable, RegisterableConnection
{
    private static final long serialVersionUID = 983759327341L;
    
    public static enum ImplementationType { jsch, sshd };
    public static enum AuthType { user, key };
    
    private NamedKey connection;
    private ImplementationType impType = ImplementationType.jsch;
    private String host;
    private int port = 22;
    private String publicKeyPath;
    private String privateKeyPath;
    private String username;
    private String password;
    private String hostKey;
    private boolean hostKeyChecking = false;
    
    public SSHConnection(String name, String key)
    {
        connection = new NamedKey(name, key);
    }

    public SSHConnection(String app, String key, String h, String p, String user, String pass, String pubp, String privp, String hostk, boolean check)
        throws RegisterableObjectException
    {
        setKey(app, key);
        host = h;
        setPort(p);
        username = user;
        password = pass;
        publicKeyPath = pubp;
        privateKeyPath = privp;
        hostKey = hostk;
        hostKeyChecking = check;
    }
    
    public SSHConnection(String app, String key, String h, String p, String user, String pass)
        throws RegisterableObjectException
    {
        setKey(app, key);
        host = h;
        setPort(p);
        username = user;
        password = pass;
    }
    
    public ConnectionType getConnectionType()
    {
        return ConnectionType.ssh;
    }
    
    public ImplementationType getImplementationType()
    {
        return impType;
    }
    
    public void setImplementationType(ImplementationType type)
    {
        this.impType = type;
    }
    
    public void setImplementationType(String type)
        throws IllegalArgumentException
    {
        try
        {
            this.impType = ImplementationType.valueOf(type);
        }
        catch (IllegalArgumentException e)
        {
            throw new IllegalArgumentException("Invalid implementation type: " + type + ". Supported types: " + JavaUtils.enumToList(ImplementationType.class));
        }
    }

    public NamedKey getKey()
    {
        return connection;
    }

    public void setKey(NamedKey key)
    {
        connection = key;
    }

    public void setKey(String name, String key)
    {
        connection = new NamedKey(name, key);
    }
    
    public String getPublicKeyString()
        throws UnsupportedEncodingException, JSchException
    {
        String blob = "";
        if ( isKeyBasedAuthentication() )
        {
            KeyPair keyPair = KeyPair.load(new JSch(), privateKeyPath, publicKeyPath);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            keyPair.writePublicKey(baos, "");
            blob = new String(baos.toByteArray(), "UTF-8");
        }
        
        return blob;
    }
    
    public String getUsername()
    {
        return username;
    }
    
    public void setUsername(String name)
    {
        username = name;
    }
    
    public String getPassword()
    {
        return password;
    }
    
    public String getPassphrase()
    {
        return password;
    }

    public String getStarredPassword()
    {
        return (password != null ? password.replaceAll(".", "*") : "");
    }

    public void setPassword( String password )
    {
        this.password = password;
    }
    
    public void setPassPhrase( String phrase )
    {
        this.password = phrase;
    }

    public String getPublicKeyPath()
    {
        return publicKeyPath;
    }
    
    public void setPublicKeyPath(String path)
    {
        this.publicKeyPath = path;
    }
    
    public String getPrivateKeyPath()
    {
        return privateKeyPath;
    }
    
    public void setPrivateKeyPath(String path)
    {
        this.privateKeyPath = path;
    }
    
    public String getHost()
    {
        return host;
    }

    public void setHost(String host)
    {
        this.host = host;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }
    
    public String getHostKeyString()
    {
        return hostKey;
    }
    
    public void setHostKeyString(String key)
    {
        hostKey = key;
    }

    public boolean isStrictHostKeyCheckingEnabled()
    {
        return hostKeyChecking;
    }
    
    public void setStrictHostKeyCheckingEnabled(boolean check)
    {
        hostKeyChecking = check;
    }
    
    public boolean isAuthenticationRequired()
    {
        return !isKeyBasedAuthentication() && password != null && password.trim().length() > 0;
    }
    
    public boolean isKeyBasedAuthentication()
    {
        return publicKeyPath != null && privateKeyPath != null;
    }
    
    public void setPort( String port )
        throws RegisterableObjectException
    {
        try
        {
            this.port = Integer.parseInt(port);
        }
        catch (NumberFormatException e)
        {
            throw new RegisterableObjectException("Unable to assign ssh port as: " + port, e);
        }
    }
    
    public AuthType getAuthenticationType()
    {
        return ( isKeyBasedAuthentication() ? AuthType.key : AuthType.user);
    }

    @Override
    public String toString()
    {
        return host + ":" + port + "|" + impType + (isKeyBasedAuthentication()?"|KeyBased":"|UserBased") + (isAuthenticationRequired()?"|AuthRequired":"");
    }
}
