/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.security.object;

import java.io.Serializable;

/**
 *
 * @author crusnak
 */
public class NamedKey implements Serializable, Comparable
{
    private static final long serialVersionUID = 2135483724334L;

    private String name;
    private String key;

    public NamedKey(String n, String k)
    {
        name = n;
        key = k;
    }

    public String getKey()
    {
        return key;
    }

    public void setKey( String key )
    {
        this.key = key;
    }

    public String getName()
    {
        return name;
    }

    public void setName( String name )
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return name + "|" + key;
    }

    @Override
    public boolean equals( Object obj )
    {
        boolean equal = false;
        if ( obj instanceof NamedKey )
        {
            NamedKey that = (NamedKey)obj;
            equal = this.name.equals(that.name) && this.key.equals(that.key);
        }
        else
        {
            equal = super.equals( obj );
        }

        return equal;
    }

    @Override
    public int hashCode()
    {
        return name.hashCode() + key.hashCode();
    }

    @Override
    public int compareTo(Object o)
    {
        int comp = 0;
        if ( o instanceof NamedKey )
        {
            NamedKey that = (NamedKey)o;
            comp = (this.name + "|" + this.key).compareTo(that.name + "|" + that.key);
        }

        return comp;
    }

}
