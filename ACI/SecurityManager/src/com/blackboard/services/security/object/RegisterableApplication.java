/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.security.object;

import java.io.Serializable;

/**
 *
 * @author crusnak
 */
public class RegisterableApplication implements Registerable, Serializable
{
    private static final long serialVersionUID = 8462135008342L;

    private String appName = "";

    public RegisterableApplication(String name)
    {
        appName = name;
    }

    public String getApplicationName()
    {
        return appName;
    }

    public String toString()
    {
        return "APPLICATION:" + appName;
    }
}
