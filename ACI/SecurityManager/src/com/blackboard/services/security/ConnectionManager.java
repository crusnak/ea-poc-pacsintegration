/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.security;

import com.blackboard.services.installer.PasswordField;
import com.blackboard.services.security.object.EncryptedMap;
import com.blackboard.services.security.exception.AccessException;
import com.blackboard.services.security.exception.BbTSSecurityException;
import com.blackboard.services.security.exception.RegisterableCastException;
import com.blackboard.services.security.object.DBConnection;
import com.blackboard.services.security.object.EmailConnection;
import com.blackboard.services.security.object.NamedKey;
import com.blackboard.services.security.object.RTIConnection;
import com.blackboard.services.security.object.RegisterableApplication;
import com.blackboard.services.security.object.RegisterableConnection;
import com.blackboard.services.security.object.SSHConnection;
import com.blackboard.services.security.object.TIAConnection;
import com.blackboard.services.security.object.WebConnection;
import com.blackboard.services.utils.BaseLogger;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;


/**
 *
 * @author crusnak
 */
public class ConnectionManager
{
    private static final String[] menuOptions = { "Register Connection",
                                                  "DeRegister Connection",
                                                  "Verify Connection",
                                                  "Execute Query on Connection",
                                                  "List Registered Connections",
                                                  "Exit" };

    private static ConnectionManager instance = null;
    private SecuredObjectManager secman = null;

    private ConnectionManager()
        throws BbTSSecurityException
    {
    }

    public static ConnectionManager getInstance(PasswordField password)
        throws BbTSSecurityException
    {
        if ( instance == null )
        {
            instance = new ConnectionManager();
        }

        instance.secman = SecuredObjectManager.getInstance(password);
        return instance;
    }

    public static ConnectionManager getInstance(RegisterableApplication app)
        throws BbTSSecurityException
    {
        if ( instance == null )
        {
            instance = new ConnectionManager();
        }

        instance.secman = SecuredObjectManager.getInstance(app);
        return instance;
    }

    public SecuredObjectManager getSecuredObjectManager()
    {
        return secman;
    }

    public boolean testDBConnection(NamedKey key)
    {
        return testDBConnection(key.getName(), key.getKey());
    }

    public boolean testDBConnection(String owner, String key)
    {
        boolean success = false;

        try
        {
            instantiateDBConnection(owner, key);
            success = true;
        }
        catch (SQLException e) {System.out.println(e.getMessage());}
        catch (BbTSSecurityException e) {System.out.println(e.getMessage());}

        return success;
    }

    public Connection instantiateDBConnection(NamedKey key)
        throws BbTSSecurityException, SQLException
    {
        Connection conn = null;

        try
        {
            DBConnection db = getDBConnection(key);
            if ( db != null )
            {
                Class.forName(db.getJdbcDriver());
                conn = DriverManager.getConnection(db.getConnectionURL(), db.getUsername(), db.getPassword());
            }
            else
            {
                throw new AccessException("Unable to find database connection: " + key);            }
        }
        catch (BbTSSecurityException e) {throw new AccessException(e);}
        catch (ClassNotFoundException e){throw new AccessException(e);}

        return conn;
    }

    public Connection instantiateDBConnection(String owner, String key)
        throws BbTSSecurityException, SQLException
    {
        return instantiateDBConnection(new NamedKey(owner, key));
    }


    public <E extends RegisterableConnection> E getConnection(Class<E> clazz, NamedKey key)
        throws BbTSSecurityException
    {
        E conn = null;
        try
        {
            EncryptionManager eman = EncryptionManager.getInstance();
            EncryptedMap map = eman.getEncryptedMap();
            Object robj = map.get(key);
            if ( robj != null && clazz.isAssignableFrom(robj.getClass()) )
            {
                conn = (E)robj;
            }
            else
            {
                throw new RegisterableCastException("Registerable object stored as " +
                        key + " is not of type " + clazz.getSimpleName() + ": " +
                        (robj==null ? null:robj.getClass().getName()));
            }
        }
        catch (BbTSSecurityException e) {throw new AccessException(e);}
        return conn;
    }
    
    public <E extends RegisterableConnection> E getConnection(Class<E> clazz, String owner, String key)
        throws BbTSSecurityException
    {
        return getConnection(clazz, new NamedKey(owner, key));
    }

    public <E extends RegisterableConnection> boolean containsConnection(Class<E> clazz, NamedKey key)
    {
        boolean exists = false;
        try
        {
            EncryptionManager eman = EncryptionManager.getInstance();
            EncryptedMap map = eman.getEncryptedMap();
            Object obj = map.get(key);
            if ( obj != null )
            {
                exists = clazz.isAssignableFrom(obj.getClass());
            }
        }
        catch (BbTSSecurityException e) {BaseLogger.warn(e.getMessage());}

        return exists;
    }

    public <E extends RegisterableConnection> boolean containsConnection(Class<E> clazz, String owner, String key)
    {
        return containsConnection(clazz, new NamedKey(owner, key));
    }

    public <E extends RegisterableConnection> boolean registerConnection(String owner, String key, E conn)
        throws BbTSSecurityException
    {
        return secman.registerObject(owner, key, conn);
    }

    public boolean deregisterConnection(String owner, String key)
        throws BbTSSecurityException
    {
        return secman.deregisterObject(owner, key);
    }

    public boolean deregisterConnection(NamedKey key)
        throws BbTSSecurityException
    {
        return secman.deregisterObject(key);
    }

    public DBConnection getDBConnection(NamedKey key)
        throws BbTSSecurityException
    {
        DBConnection conn = null;
        try
        {
            EncryptionManager eman = EncryptionManager.getInstance();
            EncryptedMap map = eman.getEncryptedMap();
            Object robj = map.get(key);
            if ( robj instanceof DBConnection )
            {
                conn = (DBConnection)robj;
            }
            else
            {
                throw new RegisterableCastException("Registerable object stored as " +
                        key + " is not of type DBConnection: " +
                        (robj==null ? null:robj.getClass().getName()));
            }
        }
        catch (BbTSSecurityException e) {throw new AccessException(e);}
        return conn;
    }

    public DBConnection getDBConnection(String owner, String key)
        throws BbTSSecurityException
    {
        return getDBConnection(new NamedKey(owner, key));
    }

    public boolean containsDBConnection(NamedKey key)
    {
        boolean exists = false;
        try
        {
            EncryptionManager eman = EncryptionManager.getInstance();
            EncryptedMap map = eman.getEncryptedMap();
            Object obj = map.get(key);
            exists = obj instanceof DBConnection;
        }
        catch (BbTSSecurityException e) {BaseLogger.warn(e.getMessage());}

        return exists;
    }

    public boolean containsDBConnection(String owner, String key)
    {
        return containsDBConnection(new NamedKey(owner, key));
    }

    public boolean registerDBConnection(String owner, String key, String driver, String host,
                                        String port, String sid, String racURL, String user, PasswordField pass)
        throws BbTSSecurityException
    {
        DBConnection conn = new DBConnection(owner, key, driver, host, port, sid, racURL, user, pass.getCleartextPassword());
        return secman.registerObject(owner, key, conn);
    }

    public boolean registerDBConnection(String owner, String key, String driver, String host,
                                        String port, String sid, String name, String racURL, String user, PasswordField pass)
        throws BbTSSecurityException
    {
        DBConnection conn = new DBConnection(owner, key, driver, host, port, sid, name, racURL, user, pass.getCleartextPassword());
        return secman.registerObject(owner, key, conn);
    }

    public boolean registerDBConnection(String owner, String key, DBConnection conn)
        throws BbTSSecurityException
    {
        return secman.registerObject(owner, key, conn);
    }

    public boolean deregisterDBConnection(String owner, String key)
        throws BbTSSecurityException
    {
        return secman.deregisterObject(owner, key);
    }

    public boolean deregisterDBConnection(NamedKey key)
        throws BbTSSecurityException
    {
        return secman.deregisterObject(key);
    }

    public TIAConnection getTIAConnection(NamedKey key)
        throws BbTSSecurityException
    {
        TIAConnection conn = null;
        try
        {
            EncryptionManager eman = EncryptionManager.getInstance();
            EncryptedMap map = eman.getEncryptedMap();
            Object robj = map.get(key);
            if ( robj instanceof TIAConnection )
            {
                conn = (TIAConnection)robj;
            }
            else
            {
                throw new RegisterableCastException("Registerable object stored as " +
                        key + " is not of type TIAConnection: " +
                        (robj==null ? null:robj.getClass().getName()));
            }
        }
        catch (BbTSSecurityException e) {throw new AccessException(e);}
        return conn;
    }

    public TIAConnection getTIAConnection(String owner, String key)
        throws BbTSSecurityException
    {
        return getTIAConnection(new NamedKey(owner, key));
    }

    public boolean containsTIAConnection(NamedKey key)
    {
        boolean exists = false;
        try
        {
            EncryptionManager eman = EncryptionManager.getInstance();
            EncryptedMap map = eman.getEncryptedMap();
            Object obj = map.get(key);
            exists = obj instanceof TIAConnection;
        }
        catch (BbTSSecurityException e) {BaseLogger.warn(e.getMessage());}

        return exists;
    }

    public boolean containsTIAConnection(String owner, String key)
    {
        return containsTIAConnection(new NamedKey(owner, key));
    }

    public boolean registerTIAConnection(String owner, String key, String host, String port,
                                        String terminal, String vendor, String encryptKey)
        throws BbTSSecurityException
    {
        TIAConnection conn = new TIAConnection(owner, key, host, port, terminal, vendor, encryptKey);
        return secman.registerObject(owner, key, conn);
    }

    public boolean registerTIAConnection(String owner, String key, TIAConnection conn)
        throws BbTSSecurityException
    {
        return secman.registerObject(owner, key, conn);
    }

    public boolean deregisterTIAConnection(String owner, String key)
        throws BbTSSecurityException
    {
        return secman.deregisterObject(owner, key);
    }

    public boolean deregisterTIAConnection(NamedKey key)
        throws BbTSSecurityException
    {
        return secman.deregisterObject(key);
    }

    public EmailConnection getEmailConnection(NamedKey key)
        throws BbTSSecurityException
    {
        return getConnection(EmailConnection.class, key);
    }

    public EmailConnection getEmailConnection(String owner, String key)
        throws BbTSSecurityException
    {
        return getEmailConnection(new NamedKey(owner, key));
    }

    public boolean containsEmailConnection(NamedKey key)
    {
        return containsConnection(EmailConnection.class, key);
    }

    public boolean containsEmailConnection(String owner, String key)
    {
        return containsEmailConnection(new NamedKey(owner, key));
    }

    public boolean registerEmailConnection(String owner, String key, String host, int port,
                                           boolean tls, boolean partial, boolean ssl, String user, Object pass)
        throws BbTSSecurityException
    {
        EmailConnection conn = new EmailConnection(owner, key, host, port, tls, partial, ssl, user, 
                (pass instanceof PasswordField?((PasswordField)pass).getCleartextPassword():null));
        return registerConnection(owner, key, conn);
    }

    public boolean registerEmailConnection(String owner, String key, EmailConnection conn)
        throws BbTSSecurityException
    {
        return registerConnection(owner, key, conn);
    }

    public WebConnection getWebConnection(NamedKey key)
        throws BbTSSecurityException
    {
        WebConnection conn = null;
        try
        {
            EncryptionManager eman = EncryptionManager.getInstance();
            EncryptedMap map = eman.getEncryptedMap();
            Object robj = map.get(key);
            if ( robj instanceof WebConnection )
            {
                conn = (WebConnection)robj;
            }
            else
            {
                throw new RegisterableCastException("Registerable object stored as " +
                        key + " is not of type WebConnection: " +
                        (robj==null ? null:robj.getClass().getName()));
            }
        }
        catch (BbTSSecurityException e) {throw new AccessException(e);}
        return conn;
    }

    public WebConnection getWebConnection(String owner, String key)
        throws BbTSSecurityException
    {
        return getWebConnection(new NamedKey(owner, key));
    }

    public boolean containsWebConnection(NamedKey key)
    {
        boolean exists = false;
        try
        {
            EncryptionManager eman = EncryptionManager.getInstance();
            EncryptedMap map = eman.getEncryptedMap();
            Object obj = map.get(key);
            exists = obj instanceof WebConnection;
        }
        catch (BbTSSecurityException e) {BaseLogger.warn(e.getMessage());}

        return exists;
    }

    public boolean containsWebConnection(String owner, String key)
    {
        return containsWebConnection(new NamedKey(owner, key));
    }

    public boolean registerWebConnection(String owner, String key, String auth, String user, String pass)
        throws BbTSSecurityException, URISyntaxException
    {
        WebConnection conn = new WebConnection(owner, key, auth, user, pass);
        return secman.registerObject(owner, key, conn);
    }

    public boolean registerWebConnection(String owner, String key, WebConnection conn)
        throws BbTSSecurityException
    {
        return secman.registerObject(owner, key, conn);
    }

    public boolean deregisterWebConnection(String owner, String key)
        throws BbTSSecurityException
    {
        return secman.deregisterObject(owner, key);
    }

    public boolean deregisterWebConnection(NamedKey key)
        throws BbTSSecurityException
    {
        return secman.deregisterObject(key);
    }

    public <E extends RegisterableConnection> List<E> getRegisteredConnections(Class<E> clazz)
        throws BbTSSecurityException
    {
        return (List<E>)secman.getRegisteredObjects(clazz);
    }
    
    public <E extends RegisterableConnection> String[] getConnectionInfo(Class<E> clazz, String owner, String name)
    {
        String[] info = new String[0];
        try
        {
            E conn = getConnection(clazz, owner, name);
            switch ( conn.getConnectionType() )
            {
                case database:
                    DBConnection dbconn = (DBConnection)conn;
                    info = new String[8];
                    info[0] = String.valueOf(dbconn.getDriverType());
                    info[1] = dbconn.getDatabaseHost();
                    info[2] = dbconn.getDatabasePort();
                    info[3] = dbconn.getDatabaseInstance();
                    info[4] = dbconn.getUsername();
                    info[5] = dbconn.getPassword();
                    info[6] = dbconn.getDatabaseName();
                    info[7] = dbconn.getRACConnectionURL();
                    break;
                case tia:
                    TIAConnection tiaconn = (TIAConnection)conn;
                    info = new String[5];
                    info[0] = tiaconn.getTIAHost();
                    info[1] = String.valueOf(tiaconn.getTIAPort());
                    info[2] = String.valueOf(tiaconn.getTerminalNumber());
                    info[3] = String.valueOf(tiaconn.getVendorNumber());
                    info[4] = tiaconn.getEncryptionKey();
                    break;
                case rti:
                    RTIConnection rticonn = (RTIConnection)conn;
                    info = new String[8];
                    info[0] = rticonn.getRTIHost();
                    info[1] = String.valueOf(rticonn.getRTIPort());
                    info[2] = rticonn.getUsername();
                    info[3] = rticonn.getPassword();
                    info[4] = rticonn.getEncryptionKey();
                    info[5] = String.valueOf(rticonn.getClientPrefix());
                    info[6] = String.valueOf(rticonn.getVersionNumber());
                    info[7] = String.valueOf(rticonn.getAmountFieldLength());
                case email:
                    EmailConnection econn = (EmailConnection)conn;
                    info = new String[7];
                    info[0] = econn.getHost();
                    info[1] = String.valueOf(econn.getPort());
                    info[2] = econn.getUsername();
                    info[3] = econn.getPassword();
                    info[4] = String.valueOf(econn.isStartTLS());
                    info[5] = String.valueOf(econn.isSendPartial());
                    info[6] = String.valueOf(econn.isSSLEnabled());
                    break;
                case ssh:
                    SSHConnection sconn = (SSHConnection)conn;
                    info = new String[4];
                    info[0] = sconn.getHost();
                    info[1] = String.valueOf(sconn.getPort());
                    info[2] = sconn.getUsername();
                    info[3] = sconn.getPassphrase();
                    break;
                case web:
                    WebConnection wconn = (WebConnection)conn;
                    info = new String[8];
                    info[0] = String.valueOf(wconn.getAuthType());
                    info[1] = String.valueOf(wconn.getSignatureMethod());
                    info[2] = wconn.getConsumerKey();
                    info[3] = wconn.getConsumerSecret();
                    info[4] = String.valueOf(wconn.getTempTokenAuthorizationURI());
                    info[5] = String.valueOf(wconn.getRealTokenAuthorizationURI());
                    info[6] = wconn.getUsername();
                    info[7] = wconn.getPassword();
                    break;
                default:
                    throw new IllegalArgumentException("Unsupported RegisterableConnection: " + clazz);
            }
        }
        catch (BbTSSecurityException e) {}
        

        return info;
    }
    
    public String[] getDBConnectionInfo(String owner, String name)
    {
        return getConnectionInfo(DBConnection.class, owner, name);
    }

    public String[] getTIAConnectionInfo(String owner, String name)
    {
        return getConnectionInfo(TIAConnection.class, owner, name);
    }

    public String[] getRTIConnectionInfo(String owner, String name)
    {
        return getConnectionInfo(RTIConnection.class, owner, name);
    }

    public String[] getEmailConnectionInfo(String owner, String name)
    {
        return getConnectionInfo(EmailConnection.class, owner, name);
    }

    public String[] getWebConnectionInfo(String owner, String name)
    {
        return getConnectionInfo(WebConnection.class, owner, name);
    }

    /*public static void interactiveRegistration(String password)
        throws BbTSSecurityException, IOException
    {
        BufferedReader entry = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();
        System.out.println("Register a database connection. Please answer the prompts below.");
        System.out.println();
        
        // If password is empty, have user enter password
        if ( password == null || password.trim().length() == 0 )
        {
            password = SecuredObjectManager.interactiveEnterSecurePassword();
            System.out.println();
        }

        System.out.print("Name of the application: ");
        String appname = entry.readLine();
        System.out.print("Name of the connection: ");
        String connname = entry.readLine();
        interactiveRegistration(appname, connname, password);
    }*/

    /*public static void interactiveRegistration(String appname, String connname, String password)
        throws BbTSSecurityException, IOException
    {
        BufferedReader entry = new BufferedReader(new InputStreamReader(System.in));

        // If password is empty, have user enter password
        if ( password == null || password.trim().length() == 0 )
        {
            password = SecuredObjectManager.interactiveEnterSecurePassword();
            System.out.println();
        }

        ConnectionManager conman = ConnectionManager.getInstance(new PasswordField(password));
        if ( appname.trim().equals("") || connname.trim().equals("") )
        {
            throw new BbTSSecurityException("Neither application name nor connection name can be empty.");
        }

        if ( !conman.secman.isApplicationRegistered(appname) )
        {
            throw new AccessException("Application " + appname + " has not been registered. Please register " +
                                      "application before registering secured objects against this application.");
        }

        String defaultDriver = "oracle.jdbc.driver.OracleDriver";
        String defaultHost = "";
        String defaultPort = "1521";
        String defaultSid = "";
        String defaultUser = "";
        String defaultPass = "";

        DBConnection conn = null;
        boolean connexists = conman.containsDBConnection(appname, connname);
        if ( connexists )
        {
            conn = conman.getDBConnection(appname, connname);
            System.out.println();
            System.out.println("DB connection already exists for: " + new NamedKey(appname, connname));
            System.out.print("Do you want to overwrite? (Y/N) ");
            String answer = entry.readLine();
            if ( answer.trim().equals("Y") || answer.trim().equals("y") )
            {
                defaultDriver = conn.getJdbcDriver();
                defaultHost = conn.getDatabaseHost();
                defaultPort = conn.getDatabasePort();
                defaultSid = conn.getDatabaseSid();
                defaultUser = conn.getUsername();
                defaultPass = conn.getStarredPassword();
            }
            else
            {
                return;
            }
        }

        System.out.println();
        System.out.print("Database driver: <" + defaultDriver + "> " );
        String driver = entry.readLine();
        if ( driver.trim().equals("") ) driver = defaultDriver;

        System.out.print("Database hostname/IP: <" + defaultHost + "> " );
        String host = entry.readLine();
        if ( host.trim().equals("") ) host = defaultHost;

        System.out.print("Database port: <" + defaultPort + "> " );
        String port = entry.readLine();
        if ( port.trim().equals("") ) port = defaultPort;

        System.out.print("Database SID: <" + defaultSid + "> " );
        String sid = entry.readLine();
        if ( sid.trim().equals("") ) sid = defaultSid;

        System.out.print("Database username: <" + defaultUser + "> " );
        String user = entry.readLine();
        if ( user.trim().equals("") ) user = defaultUser;

        PasswordField pass = new PasswordField();
        String prompt = "Database password: <" + defaultPass + "> ";
        BufferedInputStream bis = new BufferedInputStream(System.in, 1024);
        for ( int i=0; i<3; i++ )
        {
            try
            {
                pass.enterPassword(bis, '\040', prompt, true);
                i=3;
            }
            catch (IOException e)
            {
                System.out.println(e.getMessage());
            }
        }

        // If not valid password given at this point break out
        if ( pass.getCleartextPassword().trim().equals("") )
        {
            throw new BbTSSecurityException("Unable to get a password after three tries");
        }

        System.out.println();
        DBConnection newconn = new DBConnection(appname, connname, driver, host, port, sid, user, pass.getCleartextPassword());
        boolean registered = conman.registerDBConnection( appname, connname, newconn);
        if ( registered )
        {
            boolean tested = conman.testDBConnection(appname, connname);
            if ( tested )
            {
                System.out.println("SUCCESSFUL configuration for this database connection");
            }
            else
            {
                System.out.println("Unable to connect to this database connection");
                System.out.print("Do you want to store this database connection anyway? (Y/N) ");
                String answer = entry.readLine();
                if ( !answer.trim().equals("Y") && !answer.trim().equals("y") )
                {
                    if ( connexists )
                    {
                        System.out.println("Reverting to previously stored connection");
                    }
                    else
                    {
                        conman.deregisterDBConnection(appname, connname);
                        System.out.println("Database connection not registered");
                    }
                    return;
                }
            }

            System.out.println("Successfully registered database connection: " + new NamedKey(appname, connname));
        }
        else
        {
            System.out.println("Unable to register database connection: " + new NamedKey(appname, connname));
        }
    }*/

    /*public static void interactiveDeRegistration(String password)
        throws IOException, BbTSSecurityException
    {
        BufferedReader entry = new BufferedReader(new InputStreamReader(System.in));

        System.out.println();
        System.out.println("Deregistering a database connection. Please answer the prompts below.");

        // If password is empty, have user enter password
        if ( password == null || password.trim().length() == 0 )
        {
            password = SecuredObjectManager.interactiveEnterSecurePassword();
            System.out.println();
        }

        System.out.println();
        ConnectionManager conman = ConnectionManager.getInstance(new PasswordField(password));

        System.out.print("Name of the application: ");
        String appname = entry.readLine();
        System.out.print("Name of the connection: ");
        String connname = entry.readLine();

        if ( appname.trim().equals("") || connname.trim().equals("") )
        {
            throw new BbTSSecurityException("Neither application name nor connection name can be empty.");
        }

        boolean registered = conman.deregisterDBConnection( appname, connname);
        if ( registered )
        {
            System.out.println("Successfully deregistered database connection: " + new NamedKey(appname, connname));
        }
        else
        {
            System.out.println("Unable to deregister database connection: " + new NamedKey(appname, connname));
        }
    }*/

    /*public static void interactiveConnectionTest(String password)
        throws IOException, BbTSSecurityException
    {
        BufferedReader entry = new BufferedReader(new InputStreamReader(System.in));

        // If password is empty, have user enter password
        if ( password == null || password.trim().length() == 0 )
        {
            password = SecuredObjectManager.interactiveEnterSecurePassword();
            System.out.println();
        }

        ConnectionManager conman = ConnectionManager.getInstance(new PasswordField(password));
        System.out.println();
        System.out.println("Verify a database connection. Please answer the prompts below.");
        System.out.println();
        System.out.print("Name of the application: ");
        String appname = entry.readLine();
        System.out.print("Name of the connection: ");
        String connname = entry.readLine();

        DBConnection conn = conman.getDBConnection(appname, connname);
        if ( conn != null )
        {
            System.out.println();
            System.out.println("Verifying database connection: " + conn);
            boolean valid = conman.testDBConnection(appname, connname);
            if ( valid )
            {
                System.out.println("SUCCESSFUL configuration for this database connection");
            }
            else
            {
                System.out.println("Unable to connect to this database connection");
            }
        }
        else
        {
            System.out.println();
            System.out.println("Could not find a database connection for: " + new NamedKey(appname, connname));
        }
    }*/

    /*public static void interactiveListConnections(String password)
        throws BbTSSecurityException, IOException
    {
        // If password is empty, have user enter password
        if ( password == null || password.trim().length() == 0 )
        {
            password = SecuredObjectManager.interactiveEnterSecurePassword();
            System.out.println();
        }
        
        ConnectionManager conman = ConnectionManager.getInstance(new PasswordField(password));
        List<NamedKey> keys = conman.secman.getRegisteredNamedKeys(DBConnection.class);

        System.out.println();
        System.out.println("Listing all registered connections.");
        System.out.println();

        Iterator<NamedKey> it = keys.iterator();

        if ( it.hasNext() )
        {
            while ( it.hasNext() )
            {
                NamedKey key = it.next();
                Registerable robj = conman.secman.getRegisteredObject(key);
                System.out.println(key + " = " + robj);
            }
        }
        else
        {
            System.out.println("<No database connections have been registered>");
        }
    }*/

    /*private static int readMenuOption(BufferedReader entry)
        throws IOException
    {
        System.out.println();
        for( int i=1; i<=menuOptions.length; i++ )
        {
            System.out.println("\t" + i + ")" + menuOptions[i-1]);
        }
        System.out.println();
        System.out.print("Choose your option: ");

        boolean validOption = false;
        int option = 0;
        while ( !validOption )
        {
            String answer = entry.readLine();
            try
            {
                option = Integer.parseInt(answer);
            }
            catch (Exception e) {}

            if ( option > 0 && option <= menuOptions.length )
            {
                validOption = true;
            }
            else
            {
                System.out.print("Invalid option, choose again: ");
            }
        }

        return option;
    }*/

    /*private static boolean processMenuOption(String password, int option, BufferedReader entry)
        throws IOException, BbTSSecurityException
    {
        boolean exit = false;

        switch(option)
        {
            case 1:
            {
                interactiveRegistration(password);
                break;
            }
            case 2:
            {
                interactiveDeRegistration(password);
                break;
            }
            case 3:
            {
                interactiveConnectionTest(password);
                break;
            }
            case 4:
            {
                System.out.println();
                System.out.println("Not implemented yet");
                /*System.out.println("Querying against a database connection. Please answer the prompts below.");
                System.out.println();
                System.out.print("Name of the application: ");
                String appname = entry.readLine();
                System.out.print("Name of the connection: ");
                String connname = entry.readLine();
                System.out.print("Username for the connection: ");
                String user = entry.readLine();
                System.out.print("Password for the connection: ");
                String pass = entry.readLine();
                break;
            }
            case 5:
            {
                interactiveListConnections(password);
                break;
            }
            default:
                exit = true;
        }

        return exit;
    }*/

    /*public static void main(String[] args)
    {
        try
        {
            if ( args.length != 1 )
            {
                throw new IllegalArgumentException("Usage: ConnectionManager <application root>");
            }

            File root = new File(args[0]);
            SecurityManager.initialize(null, root);
            CommandLineInstaller.clearScreen();
            BufferedReader entry = new BufferedReader(new InputStreamReader(System.in));
            String password = SecuredObjectManager.interactiveEnterSecurePassword();

            System.out.println();
            System.out.println("BbTS Connection Manager");
            boolean exit = false;
            while ( !exit )
            {
                int option = readMenuOption(entry);
                exit = processMenuOption(password, option, entry);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }*/
}
