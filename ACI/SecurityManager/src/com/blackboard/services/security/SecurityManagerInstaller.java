/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.security;

import com.blackboard.services.installer.PasswordField;
import com.blackboard.services.installer.exception.InstallationException;
import com.blackboard.services.installer.view.CommandLineInstaller;
import com.blackboard.services.security.exception.BbTSSecurityException;
import com.blackboard.services.security.object.EncryptedMap;
import com.blackboard.services.security.object.KeyPairCert;
import com.blackboard.services.utils.BaseLogger;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.rmi.AccessException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.crypto.SecretKey;
import javax.servlet.ServletContext;

/**
 *
 * @author crusnak
 */
public class SecurityManagerInstaller
{
    private static final String DEFAULT_KEYSTORE =              "bbkeystore.jceks";
    private static final String DEFAULT_SECRETKEY =             "sk.rsa";
    private static final String DEFAULT_MAP =                   "encrypt.map";

    public  static final SimpleDateFormat format =              new SimpleDateFormat("yyyy-MM-dd hh:mm:ss:SS");

    private static final int REGEN_MAP =                        1;
    private static final int REGEN_SK =                         2;
    private static final int REGEN_KS =                         4;


    public static int validateKeystore(String keystore)
    {
        int mask = 0;
        URL ksURL = ClassLoader.getSystemResource(SecurityConfiguration.CONMAN_PROPS_PATH + "/" + keystore);
        if ( ksURL == null )
        {
            mask |= REGEN_KS;
        }

        return mask;
    }

    public static long validateSecretKey(String secretkey, long mask)
    {
        URL skURL = ClassLoader.getSystemResource(SecurityConfiguration.CONMAN_PROPS_PATH + "/" + secretkey);
        if ( skURL == null )
        {
            mask |= REGEN_SK;
        }

        return mask;
    }

    public static long validateEncryptedMap(String map, long mask)
    {
        URL skURL = ClassLoader.getSystemResource(SecurityConfiguration.CONMAN_PROPS_PATH + "/" + map);
        if ( skURL == null )
        {
            mask |= REGEN_MAP;
        }

        return mask;
    }

    public static boolean[] extractBooleansFromBitMask(long mask)
    {
        // 000 - nothing                    - 0, 0, 0
        // 001 - regenmap                   - 0, 0, 1
        // 010 - regensk, regenmap          - 0, 1, 1
        // 011 - regensk, regenmap          - 0 ,1, 1
        // 100 - regenks, regensk, regenmap - 1, 1, 1
        // 101 - regenks, regensk, regenmap - 1, 1, 1
        // 110 - regenks, regensk, regenmap - 1, 1, 1
        // 111 - regenks, regensk, regenmap - 1, 1, 1
        boolean regenKS = (mask & REGEN_KS) == REGEN_KS;
        boolean regenSK = regenKS || (mask & REGEN_SK) == REGEN_SK;
        boolean regenMap = regenSK || (mask & REGEN_MAP) == REGEN_MAP;
        boolean[] vals = { regenKS, regenSK, regenMap };

        return vals;
    }

    public static long installUtility(PasswordField pass, PasswordField newpass, String keyStoreName, 
                                      String secretKeyName, String encryptedMapName, int bitMask)
        throws Exception
    {
        boolean[] bools = extractBooleansFromBitMask(bitMask);
        return installUtility(newpass, pass, keyStoreName, secretKeyName, encryptedMapName, 
                              bools[0], bools[1], bools[2]);
    }


    public static long installUtility(PasswordField pass, PasswordField newpass, String keyStoreName, 
                                      String secretKeyName, String encryptedMapName, 
                                      boolean regenKS, boolean regenSK, boolean regenMap)
        throws Exception
    {
        SecurityManager secman = SecurityManager.getInstance();
        SecurityConfiguration config = secman.getConfig();
        String hashedpass = secman.getHashedInteractivePassword();
        boolean newPassword = false;

        // If password exists, verify against it to prevent unauthorized access
        if ( hashedpass == null || 
             ( pass != null && secman.verifyPassword(pass) ) )
        {
            // Set new password if defined
            if ( newpass != null && newpass.getCleartextPassword().trim().length() > 0 )
            {
                newPassword = true;
                byte[] hashbytes = BbKeyGenerator.MD5Hash(newpass.getCleartextPassword().getBytes());
                hashedpass = BbKeyGenerator.toHexString(hashbytes);
            }
            else if ( hashedpass == null )
            {
                throw new BbTSSecurityException("No password defined and missing new password.");
            }
        }
        else 
        {
            throw new BbTSSecurityException("Invalid security password, aborting installation.");
        }

        config.setKeystoreName(keyStoreName);
        config.setSecretKeyName(secretKeyName);
        config.setEncryptedMapName(encryptedMapName);
        config.setInstallationTime(getInstallationTime());
        config.save("Security Manager properties file");

        Date installDate = format.parse(config.getInstallationTime());
        long itime = installDate.getTime();

        // Regenerate the keystore
        if ( regenKS )
        {
            BaseLogger.info("Creating new KeyStore and new KeyPair and Certificate");
            KeyStore ks = BbKeyGenerator.loadKeyStore(config.getKeystorePath(), config.getInstallationTime());
            KeyPairCert kpc = BbKeyGenerator.generateRSAKeyPairAndCertificate();
            Certificate[] chain = { kpc.getCertificate() };

            String kspass = BbKeyGenerator.getKeyStorePassword(installDate);
            String pkpass = BbKeyGenerator.getRSAPrivateKeyPassword(installDate);
            ks.setKeyEntry( EncryptionManager.KEYPAIR_KS_ALIAS, kpc.getPrivateKey(),
                            pkpass.toCharArray(), chain );
            BbKeyGenerator.saveKeyStore(ks);
        }

        // Get the latest keystore
        KeyStore ks = BbKeyGenerator.loadKeyStore(config.getKeystorePath(), config.getInstallationTime());
        PublicKey publicKey = ks.getCertificate(EncryptionManager.KEYPAIR_KS_ALIAS).getPublicKey();
        PrivateKey privateKey = (PrivateKey)ks.getKey(EncryptionManager.KEYPAIR_KS_ALIAS, BbKeyGenerator.getRSAPrivateKeyPassword(installDate).toCharArray());

        // Regenerate the secret key
        if ( regenSK )
        {
            BaseLogger.info("Creating new SecretKey");
            SecretKey skey = BbKeyGenerator.generateSecretKey();
            EncryptionManager.storeSecretKey(publicKey, skey);
        }

        // Load the latest secretKey
        SecretKey secretKey = EncryptionManager.loadSecretKey(privateKey);

        // Regenerate the encrypted map
        if ( regenMap )
        {
            BaseLogger.info("Creating new EncryptedMap");
            EncryptedMap map = new EncryptedMap();
            EncryptionManager.storeEncryptedMap(secretKey, map);
        }

        // Load the lastest map
        EncryptedMap encryptMap = EncryptionManager.loadEncryptedMap(secretKey);

        // Regenerate new hashed password
        if ( newPassword )
        {
            BaseLogger.info("Storing new security password");
            encryptMap.put(SecurityManager.passwordKey, hashedpass);
            EncryptionManager.storeEncryptedMap(secretKey, encryptMap);
        }

        // Refresh the instance variables if any keys were changed
        /*if ( regenKS || regenMap || regenSK || newPassword )
        {
            EncryptionManager.getInstance().refresh();
        }*/

        return itime;
    }

    /*public static File createResourceOnClasspath(String resource, int cpindex)
        throws IOException
    {
        String cp = System.getProperty("java.class.path");
        String[] classpaths = cp.split(";");
        if ( cpindex >= classpaths.length ) cpindex = classpaths.length-1;
        return new File(classpaths[cpindex], resource);
    }

    public static File getKeyStoreFile(int cpindex)
        throws IOException
    {
        SecurityConfiguration config = SecurityConfiguration.getInstance();
        return createResourceOnClasspath(config.getKeystorePath(), cpindex);
    }

    public static File getSecretKeyFile(int cpindex)
        throws IOException
    {
        SecurityConfiguration config = SecurityConfiguration.getInstance();
        return createResourceOnClasspath(config.getSecretKeyPath(), cpindex);
    }

    public static File getEncryptedMapFile(int cpindex)
        throws IOException
    {
        SecurityConfiguration config = SecurityConfiguration.getInstance();
        return createResourceOnClasspath(config.getEncryptedMapPath(), cpindex);
    }*/

    public static String getInstallationTime()
        throws IOException
    {
        String installed = null;
        SecurityConfiguration config = SecurityConfiguration.getInstance();
        installed = config.getInstallationTime();
        if ( installed == null )
        {
            BaseLogger.info("First time installing ConnectionManager, storing installation date.");
            Date installDate = new Date();
            installed = BbKeyGenerator.convertToInstalledTime(installDate);
        }

        return installed;
    }

    public static void main(String[] args)
        throws InstallationException
    {
        String[] a = { "install-connection-manager.xml" };
        CommandLineInstaller.main(a);

        try
        {
            /*BufferedReader entry = new BufferedReader(new InputStreamReader(System.in));
            ConnectionManagerConfiguration config = ConnectionManagerConfiguration.getInstance();
            int regenBM = 0;

            boolean regenKeystore = false;
            boolean regenSecretKey = false;
            boolean regenMap = false;

            String defaultKeystore = config.getKeystoreName();
            String defaultSecretKey = config.getSecretKeyName();
            String defaultMap = config.getEncryptedMapName();
            String password = SecuredObjectManager.getHashedInteractivePassword();

            if ( defaultKeystore == null ) defaultKeystore = DEFAULT_KEYSTORE;
            if ( defaultSecretKey == null ) defaultSecretKey = DEFAULT_SECRETKEY;
            if ( defaultMap == null ) defaultMap = DEFAULT_MAP;

            System.out.println("ConnectionManager installation:");
            System.out.println();
            System.out.println("Please note that you must have an existing resources directory");
            System.out.println("located anywhere within the classpath used to launch this installer.");
            System.out.println();

            PasswordField pass = new PasswordField();
            PasswordField newpass = new PasswordField();
            if ( password == null )
            {
                String prompt = "Enter the security password to use for all interactions: ";
                BufferedInputStream bis = new BufferedInputStream(System.in, 1024);
                for ( int i=0; i<3; i++ )
                {
                    try
                    {
                        newpass.enterPassword(bis, '\040', prompt, true);
                        i=3;
                    }
                    catch (IOException e)
                    {
                        System.out.println(e.getMessage());
                    }
                }

                // If not valid password given at this point break out
                if ( pass.getCleartextPassword().equals("") )
                {
                    throw new Exception("Unable to get a password after three tries");
                }
            }
            else
            {
                System.out.println("Security password already defined.");
                System.out.print("Do you wish to change it? (Y/N) ");
                String answer = entry.readLine();
                String prompt = "Enter existing security password: ";
                BufferedInputStream bis = new BufferedInputStream(System.in, 1024);
                boolean valid = false;
                for ( int i=0; i<3; i++ )
                {
                    try
                    {
                        pass.enterPassword(bis, '\040', prompt, false);
                        if ( (valid = SecuredObjectManager.verifyPassword(pass.getCleartextPassword())) )
                        {
                            i=3;
                        }
                        else
                        {
                            prompt = "Incorrect password, enter again: ";
                        }
                    }
                    catch (IOException e)
                    {
                        System.out.println(e.getMessage());
                    }
                }

                // If the password doesn't match after three tries break out
                if ( !valid )
                {
                    throw new Exception("Incorrect password after three tries");
                }

                // Change password
                if ( answer.trim().equals("Y") || answer.trim().equals("y") )
                {
                    prompt = "Enter the security password to use for all interactions: ";
                    for ( int i=0; i<3; i++ )
                    {
                        try
                        {
                            newpass.enterPassword(bis, '\040', prompt, true);
                            i=3;
                        }
                        catch (IOException e)
                        {
                            System.out.println(e.getMessage());
                        }
                    }

                    // If not valid password given at this point break out
                    if ( pass.getCleartextPassword().equals("") )
                    {
                        throw new Exception("Unable to get a password after three tries");
                    }
                }
            }

            System.out.print("Enter the name of the keystore to use: <" + defaultKeystore +"> ");
            String ks = entry.readLine();
            if ( ks.trim().equals("") ) ks = defaultKeystore;
            URL ksURL = ClassLoader.getSystemResource(ConnectionManagerConfiguration.CONMAN_PROPS_PATH + "/" + ks);
            if ( ksURL == null )
            {
                System.out.println();
                System.out.println("Keystore does not exist. If you want to use this new keystore");
                System.out.println("you will need to regenerate all keys. It is recommended that");
                System.out.println("you also use a different secret key and encrypted mapping file");
                System.out.println("or you will never be able to retrieve your previously defined");
                System.out.println("encrypted data.");
                System.out.print("Do you want to proceed with this new keystore? (Y/N) ");
                String answer = entry.readLine();
                if ( !answer.trim().equals("Y") && !answer.trim().equals("y") )
                {
                    throw new InstallationException("ConnectionManagerInstaller exited before completeion");
                }

            }

            // Creating a new keystore requires creating new secret keys and encrypted maps
            if ( ksURL == null )
            {
                regenBM |= REGEN_KS | REGEN_SK | REGEN_MAP;
            }

            System.out.println();
            System.out.print("Enter the name of the secret key file: <" + defaultSecretKey + "> ");
            String sk = entry.readLine();
            if ( sk.trim().equals("") ) sk = defaultSecretKey;
            URL skURL = ClassLoader.getSystemResource(ConnectionManagerConfiguration.CONMAN_PROPS_PATH + "/" + sk);
            if ( ksURL == null && skURL != null )
            {
                System.out.println();
                System.out.println("You have chosen to create a new keystore while still using an existing");
                System.out.println("secret key file. This will cause the secret key generated previously");
                System.out.println("to be lost causing the encrypted mapping file to never be accessible");
                System.out.println("again.");
                System.out.print("Are you sure you want to proceed? (Y/N)");
                String answer = entry.readLine();
                if ( !answer.trim().equals("Y") && !answer.trim().equals("y") )
                {
                    throw new InstallationException("ConnectionManagerInstaller exited before complteion");
                }
            }
            else if ( ksURL != null && skURL == null )
            {
                System.out.println();
                System.out.println("Secret key file doesn't exist. A new secret key file will need");
                System.out.println("to be generated. You will also need to create a new encrypted mapping");
                System.out.println("file as well since the newly generated secret key will be unable");
                System.out.println("to encrypt an existing mapping file.");
                System.out.print("Do you want to proceed? (Y/N)");
                String answer = entry.readLine();
                if ( !answer.trim().equals("Y") && !answer.trim().equals("y") )
                {
                    throw new InstallationException("ConnectionManagerInstaller exited before completion");
                }
            }

            // Creating a new secret key requires creating a new encrypted map
            if ( skURL == null )
            {
                regenBM |= REGEN_SK | REGEN_MAP;
            }

            System.out.println();
            System.out.print("Enter the name of the encrypted mapping file: <" + defaultMap + "> ");
            String map = entry.readLine();
            if ( map.trim().equals("") ) map = defaultMap;
            URL mapURL = ClassLoader.getSystemResource(ConnectionManagerConfiguration.CONMAN_PROPS_PATH + "/" + map);
            if ( ksURL == null && mapURL != null || skURL == null && mapURL != null )
            {
                System.out.println();
                System.out.println("You have chosen either to create a new keystore or a new secret");
                System.out.println("key file while using an existing encrypted map file.  This will cause");
                System.out.println("the map data previously defined to be overwritten. This data will be");
                System.out.println("completely unrecoverable.");
                System.out.print("Do you want to proceed? (Y/N)");
                String answer = entry.readLine();
                if ( !answer.trim().equals("Y") && !answer.trim().equals("y") )
                {
                    throw new InstallationException("ConnectionManagerInstaller exited before completion");
                }
            }

            // Create a new encrypted map
            if ( mapURL == null )
            {
                regenBM |= REGEN_MAP;
            }

            System.out.println();
            System.out.println();
            installUtility(pass, newpass, ks.trim(), sk.trim(), map.trim(), regenBM);*/
        }
        catch (Exception e)
        {
            if ( !(e instanceof InstallationException) )
            {
                throw new InstallationException(e);
            }
            else
            {
                throw (InstallationException)e;
            }
        }
    }
}
