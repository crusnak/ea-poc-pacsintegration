/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.security;

import com.blackboard.services.installer.PasswordField;
import com.blackboard.services.security.object.EncryptedMap;
import com.blackboard.services.security.exception.AccessException;
import com.blackboard.services.security.exception.BbTSSecurityException;
import com.blackboard.services.security.exception.RegisterableCastException;
import com.blackboard.services.security.exception.RegistrationException;
import com.blackboard.services.security.object.DBConnection;
import com.blackboard.services.security.object.NamedKey;
import com.blackboard.services.security.object.Registerable;
import com.blackboard.services.security.object.RegisterableApplication;
import com.blackboard.services.security.object.RegisterableObject;
import com.blackboard.services.utils.BaseLogger;
import java.io.BufferedInputStream;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.security.GeneralSecurityException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;


/**
 * This is the main class responsible for managing all secured objects added to the
 * encrypted map in the SecurityManager.  All specific secured object managers should
 * extends this class (e.g. {@link ConnectionManager} for managing {@link DBConnection} objects).
 * @author crusnak
 */
public class SecuredObjectManager
{
    /**
     * Available options used by the interactive tool.  This needs to be migrated to
     * the installer framework.
     * @deprecated 
     */
    private static final String[] menuOptions = { "Register Application",
                                                  "Deregister Application",
                                                  "Deregister Object",
                                                  "List Registered Applications",
                                                  "List Registered Objects",
                                                  "Exit" };

    private static SecuredObjectManager instance = null;
    private SecurityManager secman = null;

    /**
     * Private default constructor.
     * @throws BbTSSecurityException thrown if the SecurityManager has not been initialized prior
     * to constructing this class.
     */
    protected SecuredObjectManager()
        throws BbTSSecurityException
    {
        secman = SecurityManager.getInstance();
    }

    /**
     * Public accessor to this singleton class instance by passing in the password to the
     * SecurityManager.
     * @param password the PasswordField object hiding the cleartext password.
     * @return the singleton class instance.
     * @throws BbTSSecurityException thrown if the SecurityManager has not previously
     * been initialized or the password given does not match the one defined.
     * @see SecurityManager#verifyPassword(com.blackboard.services.installer.PasswordField)
     */
    public static SecuredObjectManager getInstance(PasswordField password)
        throws BbTSSecurityException
    {
        if ( instance == null )
        {
            instance = new SecuredObjectManager();
        }

        // Empty password or unmatched password causes access violation
        if ( password == null || !instance.secman.verifyPassword(password) )
        {
            throw new AccessException("Access to SecuredObjectManager denied: invalid password");
        }
        return instance;
    }

    /**
     * Public accessor to this singleton class instance by passing in a registerable
     * application object.  This is used for non-interactive access to SecurityManager
     * components.
     * @param app RegisterableApplication attempting to access the SecurityManager.
     * @return the singleton class instance.
     * @throws BbTSSecurityException thrown if the SecurityManager has not previously
     * been initialized or the application passed has not previously been registered
     * in an interactive session.
     * @see #isApplicationRegistered(java.lang.String)
     */
    public static SecuredObjectManager getInstance(RegisterableApplication app)
        throws BbTSSecurityException
    {
        if ( instance == null )
        {
            instance = new SecuredObjectManager();
        }

        if ( app == null || !instance.isApplicationRegistered(app.getApplicationName()) )
        {
            throw new AccessException("Application " + app + " has not been registered. Please register " +
                                      "application before accessing secured objects in non-interactive mode.");
        }

        return instance;
    }

    /**
     * Accessor to the list of all currently registered applications defined in the SecurityManager.
     * @return List of RegisterableApplications.
     * @throws BbTSSecurityException thrown if there is an error accessing the encrypted objects.
     */
    public List<RegisterableApplication> getRegisteredApplications()
        throws BbTSSecurityException
    {
        return getRegisteredObjects(RegisterableApplication.class);
    }

    /**
     * Determines if the object defined by the given NamedKey and Class exists in
     * the encrypted map.
     * @param key The NamedKey (RegisterableApplication+UniqueKey) to lookup.
     * @param clazz The expected Registerable object Class.
     * @return <tt>true</tt> only if an object exists with the given NamedKey and
     * it is an instance of the given Class.
     */
    public boolean containsRegisteredObject(NamedKey key, Class clazz)
    {
        boolean exists = false;
        try
        {
            EncryptionManager eman = EncryptionManager.getInstance();
            EncryptedMap map = eman.getEncryptedMap();
            Object obj = map.get(key);
            exists = clazz != null && clazz.isInstance(obj);
        }
        catch ( BbTSSecurityException e) { BaseLogger.warn(e.getMessage()); }
        return exists;
    }

    /**
     * Determines if the object defined by the given NamedKey and Class exists in
     * the encrypted map.
     * @param owner The RegisterableApplication the object was registered with.
     * @param key The unique key identifying this specific object.
     * @param clazz The expected Registerable object Class.
     * @return <tt>true</tt> only if an object exists with the given NamedKey and
     * it is an instance of the given Class.
     */
    public boolean containsRegisteredObject(String owner, String key, Class clazz)
    {
        return containsRegisteredObject(new NamedKey(owner, key), clazz);
    }

    /**
     * Determines if the given application object was previously registered with
     * the SecurityManager.
     * @param appname The name of the application.
     * @return <tt>true</tt> only if the application was previously registered.
     */
    public boolean isApplicationRegistered(String appname)
    {
        return containsRegisteredObject(appname, "", RegisterableApplication.class);
    }

    /**
     * This method registers the given application with the SecurityManager.
     * @param appname The application to register.
     * @param password The interactive password to ensure that applications can only
     * be registered if the unhashed SecurityManager password is known.
     * @return <tt>true</tt> only if the application was successfully registered.
     * @throws BbTSSecurityException thrown if there was an error accessing any SecurityManager
     * components.
     */
    public boolean registerApplication(String appname, String password)
        throws BbTSSecurityException
    {
        boolean registered = false;
        if ( password != null )
        {
            registered = registerApplication(appname, new PasswordField(password));
        }
        return registered;
    }

    /**
     * This method registers the given application with the SecurityManager.
     * @param appname The application to register.
     * @param password The interactive password to ensure that applications can only
     * be registered if the unhashed SecurityManager password is known.
     * @return <tt>true</tt> only if the application was successfully registered.
     * @throws BbTSSecurityException thrown if there was an error accessing any SecurityManager
     * components.
     */
    public boolean registerApplication(String appname, PasswordField password)
        throws BbTSSecurityException
    {
        boolean success = false;
        if ( appname != null && secman.verifyPassword(password) )
        {
            try
            {
                RegisterableApplication app = new RegisterableApplication(appname);
                NamedKey key = new NamedKey(app.getApplicationName(), "");
                //System.out.println("Registering {" + app + "} as " + key);
                EncryptionManager eman = EncryptionManager.getInstance();
                EncryptedMap map = eman.getEncryptedMap();
                map.put(key, app);
                eman.storeEncryptedMap(eman.getSecretKey(), map);
                success = true;
            }
            catch (BbTSSecurityException e) {throw new RegistrationException(e);}
            catch (IOException e) {throw new RegistrationException(e);}
            catch (GeneralSecurityException e) {throw new RegistrationException(e);}
        }

        return success;
    }

    /**
     * This method registers the given Registerable object with the SecurityManager.
     * @param owner The RegisterableApplication the object was registered with.
     * @param key The unique key identifying this specific object.
     * @param obj The Registerable object to register.
     * @return <tt>true</tt> only if the object was successfully registered.
     * @throws BbTSSecurityException thrown if there was an error accessing any SecurityManager
     * components, if the object isn't serializable, or the application given hasn't been
     * previously registered with the SecurityManager.
     */
    public boolean registerObject(String owner, String key, Registerable obj)
        throws BbTSSecurityException
    {
        boolean success = false;
        if ( !(obj instanceof Serializable) )
        {
            throw new RegistrationException("Object must implement Serializable to be registered");
        }

        if ( !isApplicationRegistered(owner) )
        {
            throw new RegistrationException("Application " + owner + " has not been registered. Cannot register " + obj);
        }

        try
        {
            BaseLogger.info("Registering {" + obj + "} as " + new NamedKey(owner, key));
            EncryptionManager eman = EncryptionManager.getInstance();
            EncryptedMap map = eman.getEncryptedMap();
            map.put(owner, key, obj);
            eman.storeEncryptedMap(eman.getSecretKey(), map);
            success = true;
        }
        catch (BbTSSecurityException e) {throw new RegistrationException(e);}
        catch (IOException e) {throw new RegistrationException(e);}
        catch (GeneralSecurityException e) {throw new RegistrationException(e);}

        return success;
    }

    /**
     * This method deregisters the given Registerable object with the SecurityManager.
     * @param app The RegisterableApplication the object to be removed was registered with.
     * @return <tt>true</tt> only if the object was successfully deregistered.
     * @throws BbTSSecurityException thrown if there was an error accessing any SecurityManager
     * components.
     */
    public boolean deregisterApplication(RegisterableApplication app)
        throws BbTSSecurityException
    {
        if ( app == null )
        {
            throw new RegistrationException("Cannot deregister null application");
        }

        return deregisterObject(app.getApplicationName(), "");
    }

    public boolean deregisterObject(NamedKey key)
        throws BbTSSecurityException
    {
        return deregisterObject(key.getName(), key.getKey());
    }

    /**
     * This method deregisters the given Registerable object with the SecurityManager.
     * @param owner The RegisterableApplication the object to be removed was registered with.
     * @param key The unique key identifying this specific object to be removed.
     * @return <tt>true</tt> only if the object was successfully deregistered.
     * @throws BbTSSecurityException thrown if there was an error accessing any SecurityManager
     * components.
     */
    public boolean deregisterObject(String owner, String key)
        throws BbTSSecurityException
    {
        boolean success = false;

        try
        {
            BaseLogger.info("DeRegistering " + new NamedKey(owner, key));
            EncryptionManager eman = EncryptionManager.getInstance();
            EncryptedMap map = eman.getEncryptedMap();
            success = map.remove(owner, key) != null;
            eman.storeEncryptedMap(eman.getSecretKey(), map);
        }
        catch (BbTSSecurityException e) {throw new RegistrationException(e);}
        catch (IOException e) {throw new RegistrationException(e);}
        catch (GeneralSecurityException e) {throw new RegistrationException(e);}

        return success;
    }

    /**
     * Accessor to the list of all NamedKeys that have objects registered with them.
     * @return A list of NamedKeys
     * @throws BbTSSecurityException thrown if there was an error accessing any SecurityManager
     * components.
     */
    public List<NamedKey> getAllRegisteredNamedKeys()
        throws BbTSSecurityException
    {
        return getRegisteredNamedKeys(Registerable.class);
    }

    /**
     * Accessor to the list of NamedKeys with Registerable objects of the given class.
     * @param clazz The Class implementing Registerable to restrict the result set with.
     * @return A list of NamedKeys whose registered objects are of the given class.
     * @throws BbTSSecurityException thrown if there was an error accessing any SecurityManager
     * components.
     */
    public List<NamedKey> getRegisteredNamedKeys(Class clazz)
        throws BbTSSecurityException
    {
        ArrayList keys = new ArrayList();
        EncryptionManager eman = EncryptionManager.getInstance();
        EncryptedMap map = eman.getEncryptedMap();
        Iterator<Map.Entry> it = map.entrySet().iterator();
        while ( it.hasNext() )
        {
            Map.Entry entry = it.next();
            if ( clazz != null && clazz.isInstance(entry.getValue()) )
            {
                keys.add(entry.getKey());
            }
        }

        return keys;
    }

    /**
     * Accessor to a list of all objects registered (name/value pairs).
     * @return A list of Map.Entry objects containing the NamedKeys and Registerable
     * objects.
     * @throws BbTSSecurityException thrown if there was an error accessing any SecurityManager
     * components.
     */
    public List<Map.Entry> getAllRegisteredObjectPairs()
        throws BbTSSecurityException
    {
        return getRegisteredObjectPairs(Registerable.class);
    }

    /**
     * Accessor to a list of all objects registered.
     * @return A list of Registerable objects.
     * @throws BbTSSecurityException thrown if there was an error accessing any SecurityManager
     * components.
     */
    public List<Registerable> getAllRegisteredObjects()
        throws BbTSSecurityException
    {
        return getRegisteredObjects(Registerable.class);
    }

    /**
     * Accessor to a list of all objects registered (name/value pairs) restricted to the given class.
     * @param clazz The Class implementing Registerable to restrict the result set with.
     * @return A list of Map.Entry objects containing the NamedKeys and Registerable objects.
     * @throws BbTSSecurityException thrown if there was an error accessing any SecurityManager
     * components.
     */
    public List<Map.Entry> getRegisteredObjectPairs(Class clazz)
        throws BbTSSecurityException
    {
        ArrayList<Map.Entry> pairs = new ArrayList();
        EncryptionManager eman = EncryptionManager.getInstance();
        EncryptedMap map = eman.getEncryptedMap();
        Iterator<Map.Entry> it = map.entrySet().iterator();
        while ( it.hasNext() )
        {
            Map.Entry entry = it.next();
            if ( clazz != null && clazz.isInstance(entry.getValue()) )
            {
                pairs.add(entry);
            }
        }

        return pairs;
    }

    /**
     * Accessor to a list of all objects registered restricted to the given class.
     * @param clazz The Class implementing Registerable to restrict the result set with.
     * @return A list of Map.Entry objects containing the NamedKeys and Registerable objects.
     * @throws BbTSSecurityException thrown if there was an error accessing any SecurityManager
     * components.
     */
    public <E extends Registerable> List<E> getRegisteredObjects(Class<E> clazz)
        throws BbTSSecurityException
    {
        ArrayList conns = new ArrayList();
        EncryptionManager eman = EncryptionManager.getInstance();
        EncryptedMap map = eman.getEncryptedMap();
        Iterator<Map.Entry> it = map.entrySet().iterator();
        while ( it.hasNext() )
        {
            Map.Entry entry = it.next();
            if ( clazz != null && clazz.isInstance(entry.getValue()) )
            {
                conns.add(entry.getValue());
            }
        }
        return conns;
    }

    public <E extends RegisterableObject> E getRegisteredObject(Class<E> clazz, NamedKey key)
        throws BbTSSecurityException
    {
        E conn = null;
        try
        {
            EncryptionManager eman = EncryptionManager.getInstance();
            EncryptedMap map = eman.getEncryptedMap();
            Object robj = map.get(key);
            if ( robj != null && clazz.isAssignableFrom(robj.getClass()) )
            {
                conn = (E)robj;
            }
            else
            {
                throw new RegisterableCastException("Registerable object stored as " +
                        key + " is not of type " + clazz.getSimpleName() + ": " +
                        (robj==null ? null:robj.getClass().getName()));
            }
        }
        catch (BbTSSecurityException e) {throw new AccessException(e);}
        return conn;
    }

    public <E extends RegisterableObject> E getRegisteredObject(Class<E> clazz, String app, String key)
        throws BbTSSecurityException
    {
        return getRegisteredObject(clazz, new NamedKey(app, key));
    }
    
    /**
     * Accessor to an individual Registerable object in the SecurityManager.
     * @param owner The RegisterableApplication the object was registered with.
     * @param key The unique key identifying this specific object .
     * @return The registered object.
     * @throws BbTSSecurityException thrown if there was an error accessing any SecurityManager
     * components.
     */
    public Registerable getRegisteredObject(String owner, String key)
        throws BbTSSecurityException
    {
        return getRegisteredObject(new NamedKey(owner, key));
    }

    /**
     * Accessor to an individual Registerable object in the SecurityManager.
     * @param nk The NamedKey (RegisterableApplication+UniqueKey) to lookup.
     * @return The registered object.
     * @throws BbTSSecurityException thrown if there was an error accessing any SecurityManager
     * components.
     */
    public Registerable getRegisteredObject(NamedKey nk)
        throws BbTSSecurityException
    {
        Registerable o;
        EncryptionManager eman = EncryptionManager.getInstance();
        EncryptedMap map = eman.getEncryptedMap();
        return (Registerable)map.get(nk);
    }

    /**
     * Method to enter the security manager password from the command line interactively.
     * @throws BbTSSecurityException thrown if there is no password defined (installation error).
     * @throws IOException If there is an error reading the user input.
     * @deprecated Needs to be moved to the installer framework.
     */
    /*public static String interactiveEnterSecurePassword()
        throws BbTSSecurityException, IOException
    {
        PasswordField pass = new PasswordField();
        String prompt = "Enter secure password to access secured objects: ";
        BufferedInputStream bis = new BufferedInputStream(System.in, 1024);
        boolean valid = false;
        for ( int i=0; i<3; i++ )
        {
            try
            {
                pass.enterPassword(bis, '\040', prompt, false);
                if ( (valid = SecurityManager.getInstance().verifyPassword(pass)) )
                {
                    i=3;
                }
                else
                {
                    prompt = "Incorrect password, enter again: ";
                }
            }
            catch (IOException e)
            {
                BaseLogger.warn(e.getMessage());
            }
        }

        // If the password doesn't match after three tries break out
        if ( !valid )
        {
            throw new IOException("Incorrect password after three tries");
        }

        return pass.getCleartextPassword();
    }*/

    /**
     * Method to interactively register an application from the command line.
     * @param password The password for the SecurityManager.
     * @throws BbTSSecurityException thrown if there was an error accessing any SecurityManager
     * components.
     * @throws IOException If there is an error reading the user input.
     * @deprecated Needs to be moved to the installer framework.
     */
    /*public static void interactiveAppRegistration(String password)
        throws IOException, BbTSSecurityException
    {
        BufferedReader entry = new BufferedReader(new InputStreamReader(System.in));
        // If password is empty, have user enter password
        if ( password == null || password.trim().length() == 0 )
        {
            password = SecuredObjectManager.interactiveEnterSecurePassword();
            System.out.println();
        }

        SecuredObjectManager secman = SecuredObjectManager.getInstance(new PasswordField(password));
        System.out.println();
        System.out.println("Registering an application for non-interactive access to secured objects.");
        System.out.println("Please enter the prompts below:");
        System.out.println();

        System.out.println();
        System.out.print("Name of the application to register: ");
        String appname = entry.readLine();

        if ( appname.trim().equals("") )
        {
            throw new BbTSSecurityException("Application name cannot be empty.");
        }

        RegisterableApplication app = new RegisterableApplication(appname);
        boolean registered = secman.registerApplication(appname, password);
        if ( registered )
        {
            System.out.println("Successfully registered application for secured object access: " + app);
        }
        else
        {
            System.out.println("Unable to register application for secured object access: " + app);
        }
    }*/

    /**
     * Method to interactively deregister an application from the command line.
     * @param password The password for the SecurityManager.
     * @throws BbTSSecurityException thrown if there was an error accessing any SecurityManager
     * components.
     * @throws IOException If there is an error reading the user input.
     * @deprecated Needs to be moved to the installer framework.
     */
    /*public static void interactiveAppDeRegistration(String password)
        throws IOException, BbTSSecurityException
    {
        BufferedReader entry = new BufferedReader(new InputStreamReader(System.in));
        // If password is empty, have user enter password
        if ( password == null || password.trim().length() == 0 )
        {
            password = SecuredObjectManager.interactiveEnterSecurePassword();
            System.out.println();
        }

        SecuredObjectManager secman = SecuredObjectManager.getInstance(new PasswordField(password));
        System.out.println();
        System.out.println("Deregistering an application. Please answer the prompts below:");
        System.out.print("Name of the application: ");
        String appname = entry.readLine();

        if ( appname.trim().equals("") )
        {
            throw new BbTSSecurityException("Application name cannot be empty.");
        }

        RegisterableApplication app = new RegisterableApplication(appname);
        boolean registered = secman.deregisterApplication(app);
        if ( registered )
        {
            System.out.println("Successfully deregistered application: " + app);
        }
        else
        {
            System.out.println("Unable to deregister application: " + app);
        }
    }*/

    /**
     * Method to interactively deregister a registered object from the command line.
     * @param password The password for the SecurityManager.
     * @throws BbTSSecurityException thrown if there was an error accessing any SecurityManager
     * components.
     * @throws IOException If there is an error reading the user input.
     * @deprecated Needs to be moved to the installer framework.
     */
    /*public static void interactiveDeRegistration(String password)
        throws IOException, BbTSSecurityException
    {
        BufferedReader entry = new BufferedReader(new InputStreamReader(System.in));
        // If password is empty, have user enter password
        if ( password == null || password.trim().length() == 0 )
        {
            password = SecuredObjectManager.interactiveEnterSecurePassword();
            System.out.println();
        }

        SecuredObjectManager secman = SecuredObjectManager.getInstance(new PasswordField(password));
        System.out.println();
        System.out.println("Deregistering an secured object. Please answer the prompts below:");
        System.out.print("Name of the application: ");
        String appname = entry.readLine();
        System.out.print("Name of the object: ");
        String objname = entry.readLine();

        if ( appname.trim().equals("") || objname.trim().equals("") )
        {
            throw new BbTSSecurityException("Neither application name nor object name can be empty.");
        }

        boolean registered = secman.deregisterObject(appname, objname);
        if ( registered )
        {
            System.out.println("Successfully deregistered secured object: " + new NamedKey(appname, objname));
        }
        else
        {
            System.out.println("Unable to deregister secured object: " + new NamedKey(appname, objname));
        }
    }*/

    /**
     * Method to interactively list all registered applications from the command line.
     * @param password The password for the SecurityManager.
     * @throws BbTSSecurityException thrown if there was an error accessing any SecurityManager
     * components.
     * @throws IOException If there is an error reading the user input.
     * @deprecated Needs to be moved to the installer framework.
     */
    /*public static void interactiveListApplications(String password)
        throws BbTSSecurityException, IOException
    {
        // If password is empty, have user enter password
        if ( password == null || password.trim().length() == 0 )
        {
            password = SecuredObjectManager.interactiveEnterSecurePassword();
            System.out.println();
        }

        SecuredObjectManager secman = SecuredObjectManager.getInstance(new PasswordField(password));
        List<NamedKey> keys = secman.getRegisteredNamedKeys(RegisterableApplication.class);

        System.out.println();
        System.out.println("Listing all registered applications:");
        System.out.println();

        Iterator<NamedKey> it = keys.iterator();

        if ( it.hasNext() )
        {
            while ( it.hasNext() )
            {
                NamedKey key = it.next();
                Registerable robj = secman.getRegisteredObject(key);
                System.out.println(key + " = " + robj);
            }
        }
        else
        {
            System.out.println("<No applications have been registered>");
        }
    }*/

    /**
     * Method to interactively list all registered objects from the command line.
     * @param password The password for the SecurityManager.
     * @throws BbTSSecurityException thrown if there was an error accessing any SecurityManager
     * components.
     * @throws IOException If there is an error reading the user input.
     * @deprecated Needs to be moved to the installer framework.
     */
    /*public static void interactiveListObjects(String password)
        throws BbTSSecurityException, IOException
    {
        // If password is empty, have user enter password
        if ( password == null || password.trim().length() == 0 )
        {
            password = SecuredObjectManager.interactiveEnterSecurePassword();
            System.out.println();
        }

        SecuredObjectManager secman = SecuredObjectManager.getInstance(new PasswordField(password));
        List<Map.Entry> pairs = secman.getAllRegisteredObjectPairs();

        System.out.println();
        System.out.println("Listing all registered secured objects:");
        System.out.println();

        Iterator<Map.Entry> it = pairs.iterator();

        if ( it.hasNext() )
        {
            while ( it.hasNext() )
            {
                Map.Entry entry = it.next();
                System.out.println(entry);
            }
        }
        else
        {
            System.out.println("<No secured objects have been registered>");
        }
    }*/

    /**
     * Method to read the user input and match it up with the associated interactive method.
     * @param entry the input stream.
     * @throws BbTSSecurityException thrown if there was an error accessing any SecurityManager
     * components.
     * @throws IOException If there is an error reading the user input.
     * @deprecated Needs to be moved to the installer framework.
     * @see #processMenuOption(java.lang.String, int, java.io.BufferedReader)
     */
    /*private static int readMenuOption(BufferedReader entry)
        throws IOException
    {
        System.out.println();
        for( int i=1; i<=menuOptions.length; i++ )
        {
            System.out.println("\t" + i + ")" + menuOptions[i-1]);
        }
        System.out.println();
        System.out.print("Choose your option: ");

        boolean validOption = false;
        int option = 0;
        while ( !validOption )
        {
            String answer = entry.readLine();
            try
            {
                option = Integer.parseInt(answer);
            }
            catch (Exception e) {}

            if ( option > 0 && option <= menuOptions.length )
            {
                validOption = true;
            }
            else
            {
                System.out.print("Invalid option, choose again: ");
            }
        }

        return option;
    }*/

    /**
     * Method to read the user input and match it up with the associated interactive method.
     * @param password The password for the SecurityManager.
     * @param option The cardinal value of the selected {@link  MenuOptions}
     * @param entry the input stream
     * @throws BbTSSecurityException thrown if there was an error accessing any SecurityManager
     * components.
     * @throws IOException If there is an error reading the user input.
     * @deprecated Needs to be moved to the installer framework.
     * @see MenuOptions
     */
    /*private static boolean processMenuOption(String password, int option, BufferedReader entry)
        throws IOException, BbTSSecurityException
    {
        boolean exit = false;

        switch(option)
        {
            case 1:
            {
                interactiveAppRegistration(password);
                break;
            }
            case 2:
            {
                interactiveAppDeRegistration(password);
                break;
            }
            case 3:
            {
                interactiveDeRegistration(password);
                break;
            }
            case 4:
            {
                interactiveListApplications(password);
                break;
            }
            case 5:
            {
                interactiveListObjects(password);
                break;
            }
            default:
                exit = true;
        }

        return exit;
    }*/

    /**
     * Main entry point to access the Manager from the command line.
     * @param args Requires one parameter (application root)
     */
    /*public static void main(String[] args)
    {
        try
        {
            if ( args.length != 1 )
            {
                throw new IllegalArgumentException("Usage SecuredObjectManager <application root>");
            }
            
            File root = new File(args[0]);
            SecurityManager.initialize(null, root);
            CommandLineInstaller.clearScreen();
            BufferedReader entry = new BufferedReader(new InputStreamReader(System.in));
            String password = interactiveEnterSecurePassword();

            System.out.println();
            System.out.println("BbTS Secured Object Manager");
            boolean exit = false;
            while ( !exit )
            {
                int option = readMenuOption(entry);
                exit = processMenuOption(password, option, entry);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }*/
}
