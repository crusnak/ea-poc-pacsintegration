package com.blackboard.services.security;

import com.blackboard.services.security.exception.BbTSSecurityException;
import com.blackboard.services.utils.BaseLogger;
import com.blackboard.services.utils.BbBaseProperties;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import javax.servlet.ServletContext;

/**
 * This class is used to define specific properties associated with the SecurityManager
 * component. It is used to manage the file locations for the SecurityManager's encrypted
 * objects: the keystore, the secret key, and the encrypted map.
 * @author crusnak
 */
public class SecurityConfiguration extends BbBaseProperties
{
    public static final String propertyFile =           "resources/conman.properties";
    //public static final String CONMAN_PROPS_FILE =      "conman.properties";
    public static final String CONMAN_PROPS_PATH =      "resources";

    public static final String PROP_TIME_INSTALLED =    "conman.install.time";
    public static final String PROP_KEYSTORE =          "conman.name.keystore";
    public static final String PROP_KEY_LOC =           "conman.name.secretkey";
    public static final String PROP_MAP_LOC =           "conman.name.map";

    private static SecurityConfiguration instance = null;

    /**
     * Protected constructor that the singleton calls to initialize the properties class.
     * @param ctx The ServletContext for finding all resources associated with this class
     * in a web context.
     * @param root The application root directory used as a fallback if files are not found
     * within the web context.
     * @throws IOException thrown if the class resource is not able to be found.  <tt>Note:</tt> this
     * means that if you are generating properties files on the fly this class will not work
     * unless you create an empty file to work from.  Best practice is to create an empty
     * file at installation or build time to ensure that the underlying properties class can
     * find it's properties file and update it upon saving.
     */
    protected SecurityConfiguration(ServletContext ctx, File root)
        throws IOException
    {
        super(ctx, root, propertyFile);
    }

    /**
     * Singleton accessor to this class instance.
     * @return the singleton class instance
     * @throws IOException thrown if this configuration has not been previously initialized.
     * @see #initialize(javax.servlet.ServletContext, java.io.File)
     */
    public static SecurityConfiguration getInstance()
        throws IOException
    {
        if ( instance == null )
        {
            throw new IOException("SecurityConfiguration not properly initialized, please call " +
                                  "SecurityConfiguration.initialize(ServletContext,File)");
        }
        return instance;
    }

    /**
     * Initializes the class by setting the required contexts in order for the ResourceLoader
     * to find the class resources properly.
     * @param ctx The ServletContext for finding all resources associated with this class
     * in a web context.
     * @param root The application root directory used as a fallback if files are not found
     * within the web context.
     * @return the singleton class instance
     * @throws IOException thrown if the class resource is not able to be found.  <tt>Note:</tt> this
     * means that if you are generating properties files on the fly this class will not work
     * unless you create an empty file to work from.  Best practice is to create an empty
     * file at installation or build time to ensure that the underlying properties class can
     * find it's properties file and update it upon saving.
     */
    public static SecurityConfiguration initialize(ServletContext ctx, File root)
        throws IOException
    {
        BaseLogger.info("Retrieving SecurityConfiguration from: " + propertyFile);
        if ( instance == null )
        {
            instance = new SecurityConfiguration(ctx, root);
        }
        return instance;
    }

    /**
     * Initializes the class by setting the required contexts in order for the ResourceLoader
     * to find the class resources properly.
     * @param root The application root directory used as a fallback if files are not found
     * within the web context.
     * @return the singleton class instance
     * @throws IOException thrown if the class resource is not able to be found.  <tt>Note:</tt> this
     * means that if you are generating properties files on the fly this class will not work
     * unless you create an empty file to work from.  Best practice is to create an empty
     * file at installation or build time to ensure that the underlying properties class can
     * find it's properties file and update it upon saving.
     */
    public static SecurityConfiguration initialize(File root)
        throws IOException
    {
        return initialize(null, root);
    }

    /**
     * Accessor the properties file resource as a URL.
     * @return the URL of the properties file resource.
     */
    public URL getPropertyURL()
    {
        return propertyURL;
    }

    /*public void save()
        throws IOException
    {
        FileOutputStream out = null;
        if ( propLocation != null )
        {
            out = new FileOutputStream(propLocation.getFile());
        }
        else
        {
            System.out.println("Configuration file doesn't exist, creating for the first time");
            String cp = System.getProperty("java.class.path");
            String[] classpaths = cp.split(";");
            File f = new File(classpaths[0], CONMAN_PROPS_PATH + File.separator + CONMAN_PROPS_FILE);
            out = new FileOutputStream(f);
        }

        this.store(out, "Properties specific to ConnectionManager");
        out.close();
        propLocation = ClassLoader.getSystemResource( CONMAN_PROPS_PATH + "/" + CONMAN_PROPS_FILE );
    }*/

    /*public String getPropertiesFileLocation()
    {
        return this.getProperty(CONMAN_PROPS_FILE);
    }

    public void setPropertiesFileLocation(String loc)
    {
        this.setProperty(CONMAN_PROPS_FILE, loc);
    }*/

    /**
     * Accessor to the installation time of the SecurityManager.  This is needed to derive the
     * keystore password to access the SecurityManager constructs.
     * @return The installation time property.
     */
    public String getInstallationTime()
    {
        return this.getProperty(PROP_TIME_INSTALLED);
    }

    /**
     * Accessor to the installation time of the SecurityManager.  This is needed to derive the
     * keystore password to access the SecurityManager constructs.
     * @param date The new installation time.
     */
    public void setInstallationTime(String date)
    {
        this.setProperty(PROP_TIME_INSTALLED, date);
    }

    /**
     * Accessor to the relative resource path of the keystore file.
     * @return The resource path of the keystore.
     */
    public String getKeystorePath()
    {
        return CONMAN_PROPS_PATH + "/" + this.getProperty(PROP_KEYSTORE);
    }

    /**
     * Accessor to the name of the keystore file.
     * @return The name of the keystore file.
     */
    public String getKeystoreName()
    {
        return this.getProperty(PROP_KEYSTORE);
    }

    /**
     * Accessor to the name of the keystore file.
     * @param loc The name of the keystore file.
     */
    public void setKeystoreName(String loc)
    {
        this.setProperty(PROP_KEYSTORE, loc);
    }

    /**
     * Accessor to the relative resource path of the secret key file.
     * @return The resource path of the secret key.
     */
    public String getSecretKeyPath()
    {
        return CONMAN_PROPS_PATH + "/" + this.getProperty(PROP_KEY_LOC);
    }

    /**
     * Accessor to the name of the secret key file.
     * @return The name of the secret key file.
     */
    public String getSecretKeyName()
    {
        return this.getProperty(PROP_KEY_LOC);
    }

    /**
     * Accessor to the name of the secret key file.
     * @param loc The name of the secret key file.
     */
    public void setSecretKeyName(String loc)
    {
        this.setProperty(PROP_KEY_LOC, loc);
    }

    /**
     * Accessor to the relative resource path of the encrypted map file.
     * @return The resource path of the encrypted map.
     */
    public String getEncryptedMapPath()
    {
        return CONMAN_PROPS_PATH + "/" + this.getProperty(PROP_MAP_LOC);
    }

    /**
     * Accessor to the name of the encrypted map file.
     * @return The name of the secret key file.
     */
    public String getEncryptedMapName()
    {
        return this.getProperty(PROP_MAP_LOC);
    }

    /**
     * Accessor to the name of the encrypted map file.
     * @param loc The name of the secret key file.
     */
    public void setEncryptedMapName(String loc)
    {
        this.setProperty(PROP_MAP_LOC, loc);
    }

    public static void main(String[] args)
    {
        try
        {
            //String cp = System.getProperty("java.class.path");
            //cp += ";C:\\Perforce\\crusnak_transact\\Sandbox\\crusnak\\SecureConnectionManager";
            //System.setProperty("java.class.path", cp);
            System.out.println(System.getProperty("java.class.path"));
            SecurityConfiguration config = SecurityConfiguration.getInstance();
            System.out.println(config);

            //config.setInstallationDate(String.valueOf(new java.util.Date().getTime()));
            //config.save();

            //config.refresh();
            System.out.println(config);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

}
