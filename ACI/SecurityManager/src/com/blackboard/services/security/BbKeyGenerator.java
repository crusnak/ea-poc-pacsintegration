/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.security;


import com.blackboard.services.security.exception.BbTSSecurityException;
import com.blackboard.services.security.object.KeyPairCert;
import com.blackboard.services.utils.BaseLogger;
import com.blackboard.services.utils.RadixConversion;
import com.blackboard.services.utils.ResourceLoader;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.security.spec.AlgorithmParameterSpec;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.ServletContext;
import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.bcpg.CompressionAlgorithmTags;
import org.bouncycastle.bcpg.HashAlgorithmTags;
import org.bouncycastle.bcpg.SymmetricKeyAlgorithmTags;
import org.bouncycastle.bcpg.sig.KeyFlags;
import sun.security.x509.X500Name;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.PGPCompressedData;
import org.bouncycastle.openpgp.PGPCompressedDataGenerator;
import org.bouncycastle.openpgp.PGPEncryptedData;
import org.bouncycastle.openpgp.PGPEncryptedDataGenerator;
import org.bouncycastle.openpgp.PGPEncryptedDataList;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPKeyPair;
import org.bouncycastle.openpgp.PGPKeyRingGenerator;
import org.bouncycastle.openpgp.PGPLiteralData;
import org.bouncycastle.openpgp.PGPObjectFactory;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyEncryptedData;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import org.bouncycastle.openpgp.PGPSecretKeyRingCollection;
import org.bouncycastle.openpgp.PGPSignature;
import org.bouncycastle.openpgp.PGPSignatureSubpacketGenerator;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.examples.KeyBasedLargeFileProcessor;
import sun.security.tools.keytool.CertAndKeyGen;


/**
 * This is a convenience class for mananging all key generation and basic
 * encryption/decryption of various algorithms.  Currently, the following algorithms are
 * supported: <tt>RSA, MD5WithRSA, AES</tt>.  The following transformations are supported:
 * <tt>AES/CBC/NoPadding, AES/CBC/PKCS5Padding</tt>.
 * @author crusnak
 */
public class BbKeyGenerator 
{

    public static final String HMAC_SHA1_CIPHER_ALGORITHM =         "HmacSHA1";
    public static final String KEYSTORE_CIPHER_ALGORITH =           "RSA";
    public static final String SIGNATURE_ALGORITH =                 "MD5WithRSA";
    public static final String KEYSTORE_TYPE =                      "JCEKS";
    public static final String TIA_CIPHER_TRANSFORMATION =          "AES/CBC/NoPadding";
    public static final String AES_CIPHER_TRANSFORMATION =          "AES/CBC/PKCS5Padding";
    public static final String DES3_CIPHER_TRANSFORMATION =         "DESede/CBC/PKCS5Padding";
    public static final String TIA_CIPHER_ALGORITHM =               "AES";
    public static final String BLOWFISH_CIPHER_ALGORITHM =          "Blowfish";
    public static final String BLOWFISH_CIPHER_TRANSFORMATION =     "Blowfish/CBC/PKCS5Padding";
    //public static final String PGP_CIPHER_ALGORITH =                "
    public static final int TIA_BLOCK_SIZE =                        16;

    private static final SimpleDateFormat dateFormat =              new SimpleDateFormat("yyyy-MM-dd hh:mm:ss:SS");

    static
    {
        Security.addProvider(new BouncyCastleProvider());
    }
    
    /**
     * This method is used to load a keystore from a given resource location.
     * @param resource The resource location of the keystore to load.
     * @param installed The installation datetime (part of the keystore password) of the security manager.
     * It should be in the following date format: <tt>yyyy-MM-dd hh:mm:ss:SS</tt>
     * @return The loaded KeyStore
     * @throws BbTSSecurityException thrown if the SecurityManager hasn't been initialized
     * @throws IOException thrown if the resource is not found or a there's a problem loading it
     * @throws GeneralSecurityException thrown if the derived password for the keystore is incorrect
     * @throws ParseException thrown if the installation date string cannot be parsed using the aforementioned
     * date format.
     */
    public static KeyStore loadKeyStore(String resource, String installed)
        throws BbTSSecurityException, IOException, GeneralSecurityException, ParseException
    {
        SecurityManager secman = SecurityManager.getInstance();
        KeyStore ks = KeyStore.getInstance(KEYSTORE_TYPE, EncryptionManager.SECURITY_PROVIDER);
        File ksFile = secman.findResourceAsFile(resource);
        //URL ksURL = ClassLoader.getSystemResource(config.getKeystorePath());
        //String installed = config.getInstallationTime();
        Date installDate = convertFromInstalledTime(installed);
        BaseLogger.debug("SecurityManager installed at: " + installed);
                
        String password = getKeyStorePassword(installDate);
        if ( !ksFile.exists() )
        {
            BaseLogger.info("KeyStore doesn't exist, creating for the first time");
            //File f = SecurityManagerInstaller.getKeyStoreFile(0);
            //File f = new File(secman.getApplicationRoot(), resource);
            ks.load(null, password.toCharArray());
            ks.store(new FileOutputStream(ksFile), password.toCharArray());
        }
        else
        {
            BaseLogger.info("Existing KeyStore found, loading...");
            ks.load(new FileInputStream(ksFile), password.toCharArray());
        }

        return ks;
    }

    /**
     * This method saves the given keystore to the keystore path defined in the SecurityConfiguration object.
     * @param ks The keystore to store
     * @throws IOException thrown if there's an I/O error saving the keystore or accessing the
     * SecurityConfiguration object.
     * @throws GeneralSecurityException thrown if the derived password for the keystore is incorrect
     * @throws ParseException thrown if there's an error parsing the installed date string from the
     * SecurityConfiguration object
     * @see SecurityConfiguration#getKeystorePath()
     */
    public static void saveKeyStore(KeyStore ks)
        throws IOException, GeneralSecurityException, ParseException
    {
        SecurityConfiguration config = SecurityConfiguration.getInstance();
        URL ksURL = ClassLoader.getSystemResource(config.getKeystorePath());
        File ksFile = new File(ksURL.getPath());
        FileOutputStream out = new FileOutputStream(ksFile);
        ks.store(out, getKeyStorePassword(convertFromInstalledTime(config.getInstallationTime())).toCharArray());
        out.close();
    }

    /**
     * This method is used to generate an RSA key pair and SSL certificate to use as the
     * secured password to access the secret key used to encrypt the secured object map.
     * @return The KeyPairCert object containing the RSA public and private key, as well
     * as the SSL cert.
     * @throws IOException thrown if there is an error creating the X500Name object for the cert.
     * @throws GeneralSecurityException thrown if there is an error generating the key pair or
     * the SSL certificate.
     */
    public static KeyPairCert generateRSAKeyPairAndCertificate()
        throws IOException, GeneralSecurityException
    {
        CertAndKeyGen cakg = new CertAndKeyGen(KEYSTORE_CIPHER_ALGORITH, SIGNATURE_ALGORITH);
        X500Name name = new X500Name("ConnectionManager", "Blackboard", "Consulting Services",
                                     "Phoenix", "AZ", "US");
        cakg.generate(2056);
        PublicKey pub = cakg.getPublicKey();
        PrivateKey priv = cakg.getPrivateKey();
        X509Certificate cert = cakg.getSelfCertificate(name, new Date(), 3000);
        return new KeyPairCert(pub, priv, cert);
    }

    /**
     * Generates a secret key used to encrypt/decrypt the secured object map.
     * @return The generated SecretKey
     * @throws GeneralSecurityException thrown if there is an error generating the SecretKey.
     */
    public static SecretKey generateSecretKey()
        throws GeneralSecurityException
    {
        KeyGenerator kg = KeyGenerator.getInstance(EncryptionManager.SECRETKEY_CIPHER_ALGORITH,
                                                   EncryptionManager.SECURITY_PROVIDER);
        kg.init(new SecureRandom());
        return kg.generateKey();
    }

    /**
     * Convenience method used to create an MD5 hash from the incoming byte array.
     * @param inc Cleartext byte array needing to be encrypted.
     * @return The hashed byte array.
     * @throws GeneralSecurityException thrown if there is an error hashing the incoming byte array.
     */
    public static byte[] MD5Hash(byte[] inc)
        throws GeneralSecurityException
    {
        MessageDigest md = MessageDigest.getInstance("MD5");
        return md.digest(inc);
    }

    /**
     * Convenience method used to create an SHA256 hash from the incoming byte array.
     * @param inc Cleartext byte array needing to be encrypted.
     * @return The hashed byte array.
     * @throws GeneralSecurityException thrown if there is an error hashing the incoming byte array.
     */
    public static byte[] SHA256Hash(byte[] inc)
        throws GeneralSecurityException
    {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        return md.digest(inc);
    }
    
    public static String SHA256Hash(String inc)
        throws GeneralSecurityException
    {
        String hex = null;
        if ( inc != null )
        {
            hex = RadixConversion.bytesToHexString(SHA256Hash(RadixConversion.charsToBytes(inc.toCharArray())), false);
        }

        return hex;
    }
    
    public static String HMACSHA1Hash(String key, String inc)
            
    {
        String hash = null;
        try
        {
            Key signingKey = new SecretKeySpec(key.getBytes(), HMAC_SHA1_CIPHER_ALGORITHM);
            Mac mac = Mac.getInstance(HMAC_SHA1_CIPHER_ALGORITHM);
            mac.init(signingKey);
            byte[] raw = mac.doFinal(inc.getBytes());
            hash = Base64.encodeBase64String(raw);
        }
        catch (NoSuchAlgorithmException e) { e.printStackTrace(); /* This should not occur at runtime */ }
        catch (InvalidKeyException e ) { e.printStackTrace(); /* This should not occur at runtime */ }
        
        return hash;
    }

    /**
     * Converts a byte array to hex string
     * @param block the byte array to convert to hex string.
     * @return the hex string representation of the byte array.
     */
    public static String toHexString(byte[] block)
    {
        StringBuffer buf = new StringBuffer();

        int len = block.length;

        for (int i = 0; i < len; i++) {
            byte2hex(block[i], buf);
            //buf.append(String.valueOf(block[i]));
            /* if (i < len-1) {
                 buf.append(":");
             }*/
        }
        return buf.toString();
    }

    /**
     * Derives the keystore password from the given installation date and returns a hashed
     * representation of it.
     * @param installDate The installation date of the SecurityManager
     * @return The hashed keystore password.
     * @throws GeneralSecurityException thrown if there is an error hashing the password.
     */
    public static String getKeyStorePassword(Date installDate)
        throws GeneralSecurityException
    {
        String aux = "|password";
        return toHexString(MD5Hash((installDate.getTime() + aux).getBytes()));
    }

    /**
     * Derives the RSA cert password from the given installation date and returns a
     * hashed representation of it.  Certs containing private keys stored in a keystore require
     * their own password to protect access to it.
     * @param installDate The installation date of the SecurityManager
     * @return The hashed private key password.
     * @throws GeneralSecurityException thrown if there is an error hashing the password.
     */
    public static String getRSAPrivateKeyPassword(Date installDate)
        throws GeneralSecurityException
    {
        String aux = "|" + EncryptionManager.KEYPAIR_KS_ALIAS;
        return toHexString(MD5Hash((installDate.getTime() + aux).getBytes()));
    }

    /**
     * This method creates an RSA cipher using the given mode and key.
     * @param mode the operation mode of this cipher (this is one of the following:
     * <tt>ENCRYPT_MODE, DECRYPT_MODE, WRAP_MODE or UNWRAP_MODE</tt>)
     * @param key The encryption key used
     * @return The Cipher object ready to perform the assigned mode.
     * @throws GeneralSecurityException thrown if there is an error with the given key and the
     * RSA cipher algorithm:
     */
    public static Cipher getRSACipher(int mode, Key key)
        throws GeneralSecurityException
    {
        Cipher c = Cipher.getInstance(KEYSTORE_CIPHER_ALGORITH);
        c.init(mode, key);
        return c;
    }

    /**
     * This method creates an AES cipher using the given mode and secret key.
     * @param mode the operation mode of this cipher (this is one of the following:
     * <tt>ENCRYPT_MODE, DECRYPT_MODE, WRAP_MODE or UNWRAP_MODE</tt>)
     * @param key The encryption key used
     * @return The Cipher object ready to perform the assigned mode.
     * @throws GeneralSecurityException thrown if there is an error with the given key and the
     * AES cipher algorithm:
     */
    public static Cipher getSymmetricCipher(int mode, SecretKey key)
        throws GeneralSecurityException
    {
        Cipher c = Cipher.getInstance(EncryptionManager.SECRETKEY_CIPHER_ALGORITH,
                                      EncryptionManager.SECURITY_PROVIDER);
        c.init(mode, key);
        return c;
    }

    /**
     * This method creates an AES cipher using the given mode and key in hex form. It uses
     * the <tt>AES/CBC/NoPadding</tt> transformation algorith in defining the keyspec to convert
     * the hex string into a Key object.  <tt>Note: the TIA interface handles padding
     * itself which is why no padding is done here.</tt>
     * @param mode the operation mode of this cipher (this is one of the following:
     * <tt>ENCRYPT_MODE, DECRYPT_MODE, WRAP_MODE or UNWRAP_MODE</tt>)
     * @param key The hex string representation of the encryption key.
     * @return The Cipher object ready to perform the assigned mode.
     * @throws GeneralSecurityException thrown if there is an error with the given key and the
     * AES cipher or transformation algorithm.
     */
    public static Cipher getTIACipher(int mode, String key)
        throws GeneralSecurityException
    {
        Cipher c = null;
        byte[] keybytes = RadixConversion.convertHexStringToBytes(key);;
        SecretKeySpec skeySpec = new SecretKeySpec(keybytes, TIA_CIPHER_ALGORITHM);
        c = Cipher.getInstance(TIA_CIPHER_TRANSFORMATION);
        IvParameterSpec iv = new IvParameterSpec(new byte[TIA_BLOCK_SIZE]);
        c.init(mode, skeySpec, iv);
        return c;
    }
    
    /**
     * This method creates an AES cipher using the given mode and key in hex form. It uses
     * the <tt>AES/CBC/PKCS5Padding</tt> transformation algorith in defining the keyspec to convert
     * the hex string into a Key object.
     * @param mode the operation mode of this cipher (this is one of the following:
     * <tt>ENCRYPT_MODE, DECRYPT_MODE, WRAP_MODE or UNWRAP_MODE</tt>)
     * @param key The hex string representation of the encryption key.
     * @return The Cipher object ready to perform the assigned mode.
     * @throws GeneralSecurityException thrown if there is an error with the given key and the
     * AES cipher or transformation algorithm.
     */
    public static Cipher getAESCipher(int mode, String key)
        throws GeneralSecurityException
    {
        Cipher c = null;
        byte[] keybytes = RadixConversion.convertHexStringToBytes(key);;
        SecretKeySpec skeySpec = new SecretKeySpec(keybytes, TIA_CIPHER_ALGORITHM);
        c = Cipher.getInstance(AES_CIPHER_TRANSFORMATION);
        IvParameterSpec iv = new IvParameterSpec(new byte[TIA_BLOCK_SIZE]);
        c.init(mode, skeySpec, iv);
        return c;
    }

    public static Cipher getBlowfishCipher(int mode, String key)
        throws GeneralSecurityException
    {
        Cipher c = null;
        byte[] keybytes = RadixConversion.convertHexStringToBytes(key);
        SecretKeySpec skeySpec = new SecretKeySpec(keybytes, BLOWFISH_CIPHER_ALGORITHM);
        c = Cipher.getInstance(BLOWFISH_CIPHER_TRANSFORMATION);
        IvParameterSpec iv = new IvParameterSpec(new byte[8]);
        c.init(mode, skeySpec, iv);
        return c;
    }

    public static Cipher getDES3Cipher(int mode, String key)
        throws GeneralSecurityException
    {
        Cipher c = null;
        byte[] t = MD5Hash(RadixConversion.convertHexStringToBytes(key));
        System.out.println(RadixConversion.bytesToHexString(t));
        System.out.println(t.length);
        byte[] tempbytes = RadixConversion.convertHexStringToBytes(key);
        System.out.println(RadixConversion.bytesToHexString(tempbytes));
        System.out.println(tempbytes.length);
        System.out.println(tempbytes.length % 24 == 0?tempbytes.length:(tempbytes.length/24 + 1)*24);
        byte[] keybytes = Arrays.copyOf(tempbytes, (tempbytes.length % 24 == 0?tempbytes.length:(tempbytes.length/24 + 1)*24));
        System.out.println(RadixConversion.bytesToHexString(keybytes));
        System.out.println(keybytes.length);
        SecretKeySpec sKeySpec = new SecretKeySpec(keybytes, "DESede");
        c = Cipher.getInstance(DES3_CIPHER_TRANSFORMATION);
        IvParameterSpec iv = new IvParameterSpec(new byte[8]);
        c.init(mode, sKeySpec, iv);
        return c;
    }
    
    /**
     * Convenience method to encrypt the given string using an AES cipher initialized
     * with the given encryption key in hex form.
     * @param s the string to encrypt
     * @param key the encryption key in hex form
     * @return the encrypted string in hex form
     * @throws GeneralSecurityException thrown if there is an error creating the cipher or
     * encrypting the incoming string.
     */
    public static String encryptAESString(String s, String key)
        throws GeneralSecurityException
    {
        byte[] bytes = s.getBytes();
        Cipher cipher = BbKeyGenerator.getAESCipher(Cipher.ENCRYPT_MODE, key);
        byte[] encrypted = cipher.doFinal(bytes);
        return RadixConversion.bytesToHexString(encrypted, false);
    }

    /**
     * Convenience method to decrypt the given string using an AES cipher initialized
     * with the given encryption key in hex form.
     * @param hex the string to decrypt in hex form
     * @param key the encryption key in hex form
     * @return the decrypted string in cleartext
     * @throws GeneralSecurityException thrown if there is an error creating the cipher or
     * decrypting the incoming string.
     */
    public static String decryptAESString(String hex, String key)
        throws GeneralSecurityException
    {
        byte[] bytes = RadixConversion.convertHexStringToBytes(hex);
        Cipher cipher = BbKeyGenerator.getAESCipher(Cipher.DECRYPT_MODE, key);
        byte[] decrypted = cipher.doFinal(bytes);
        return new String(decrypted);
    }
    
    /**
     * Convenience method to encrypt the given string using a Blowfish cipher initialized
     * with the given encryption key in hex form.
     * @param s the string to encrypt
     * @param key the encryption key in hex form
     * @return the encrypted string in hex form
     * @throws GeneralSecurityException thrown if there is an error creating the cipher or
     * encrypting the incoming string.
     */
    public static String encryptBlowfishString(String s, String key)
        throws GeneralSecurityException
    {
        byte[] bytes = s.getBytes();
        Cipher cipher = BbKeyGenerator.getBlowfishCipher(Cipher.ENCRYPT_MODE, key);
        byte[] encrypted = cipher.doFinal(bytes);
        return RadixConversion.bytesToHexString(encrypted, false);
    }

    /**
     * Convenience method to decrypt the given string using a Blowfish cipher initialized
     * with the given encryption key in hex form.
     * @param hex the string to decrypt in hex form
     * @param key the encryption key in hex form
     * @return the decrypted string in cleartext
     * @throws GeneralSecurityException thrown if there is an error creating the cipher or
     * decrypting the incoming string.
     */
    public static String decryptBlowfishString(String hex, String key)
        throws GeneralSecurityException
    {
        byte[] bytes = RadixConversion.convertHexStringToBytes(hex);
        Cipher cipher = BbKeyGenerator.getBlowfishCipher(Cipher.DECRYPT_MODE, key);
        byte[] decrypted = cipher.doFinal(bytes);
        return new String(decrypted);
    }
    
    public static PGPSecretKeyRing createPGPSecretKeyRing(String id, String passphrase)
        throws GeneralSecurityException
    {
        PGPSecretKeyRing skr=null;

        try
        {
            KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance("RSA", "BC");
            keyPairGen.initialize(4096);
            KeyPair keyPair = keyPairGen.generateKeyPair();
            KeyPair keyPair2 = keyPairGen.generateKeyPair();

            PGPSignatureSubpacketGenerator hashedGen = new PGPSignatureSubpacketGenerator();
            hashedGen.setKeyFlags(true, KeyFlags.CERTIFY_OTHER | KeyFlags.SIGN_DATA
                                         | KeyFlags.ENCRYPT_COMMS | KeyFlags.ENCRYPT_STORAGE);
            hashedGen.setPreferredCompressionAlgorithms(false, new int[] {CompressionAlgorithmTags.ZIP});
            hashedGen.setPreferredHashAlgorithms(false, new int[] {HashAlgorithmTags.SHA1} );
            hashedGen.setPreferredSymmetricAlgorithms(false, new int[] { SymmetricKeyAlgorithmTags.AES_256});

            PGPKeyPair secretKey = new PGPKeyPair(PGPPublicKey.RSA_GENERAL, keyPair, new Date());	
            PGPKeyPair secretKey2 = new PGPKeyPair(PGPPublicKey.RSA_GENERAL, keyPair2, new Date());		

            PGPKeyRingGenerator keyRingGen = new PGPKeyRingGenerator(PGPSignature.POSITIVE_CERTIFICATION, 
                                                                     secretKey,
                            id, PGPEncryptedData.AES_256, passphrase.toCharArray(), true, 
                            hashedGen.generate(), null, new SecureRandom(), "BC"); 

            keyRingGen.addSubKey(secretKey2);
            skr = keyRingGen.generateSecretKeyRing();
        }
        catch (PGPException e)
        {
            throw new GeneralSecurityException("Unable to generate SecretKeyRing for PGP", e);
        }
			
        return skr;
    }
    
    public static String encryptPGPMessageUsingSKRFile(String msg, File skfile)
        throws GeneralSecurityException
    {
        String armored = "";
        
        try
        {
            PGPSecretKeyRing skr = new PGPSecretKeyRing(PGPUtil.getDecoderStream(new FileInputStream(skfile)));
            armored = encryptPGPMessage(msg, skr.getPublicKey());
        }
        catch (IOException e)
        {
            throw new GeneralSecurityException("Unable to encrypt PGP message, can't find secret key ring file", e);
        }
        catch (PGPException e)
        {
            throw new GeneralSecurityException("Failed to encrypt PGP message due to PGP error", e.getUnderlyingException());
        }
        
        return armored;
    }
    
    public static String encryptPGPMessageUsingPKRFile(String msg, File pkfile)
        throws GeneralSecurityException
    {
        String armored = "";
        
        try
        {
            PGPPublicKeyRing pkr = new PGPPublicKeyRing(PGPUtil.getDecoderStream(new FileInputStream(pkfile)));
            armored = encryptPGPMessage(msg, pkr.getPublicKey());
        }
        catch (IOException e)
        {
            throw new GeneralSecurityException("Unable to encrypt PGP message, can't find public key ring file", e);
        }

        return armored;
    }

    public static String encryptPGPMessage(String msg, PGPPublicKey pkey)
        throws GeneralSecurityException
    {
        String armored = "";
        File in = null;
        File out = null;
        
        try
        {
            in = File.createTempFile("encrypt-in", ".txt");
            out = File.createTempFile("encrypt-out", ".asc");
            
            FileWriter w = new FileWriter(in);
            w.write(msg);
            w.close();
            
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(out));
            PGPFileUtils.encryptFile(bos, in, pkey, true, true);
            
            BufferedReader br = new BufferedReader(new FileReader(out));
            String s = null;
            while ( (s = br.readLine()) != null )
            {
                armored += s + "\n";
            }
            
            br.close();
            bos.close();
            
            if ( armored.trim().length() == 0 )
            {
                armored = null;
            }
        }
        catch (IOException e)
        {
            throw new GeneralSecurityException("Unable to encrypt PGP message", e);
        }
        finally
        {
            if ( in != null )
            {
                in.deleteOnExit();
                out.deleteOnExit();
            }
        }
        
        return armored;
    }
    
    public static String decryptPGPMessage(String armored, String passphrase, File skfile)
        throws GeneralSecurityException
    {
        String decrypted = "";
        File in = null;
        File out = null;
        
        try
        {
            in = File.createTempFile("decrypt-in", ".asc");
            out = File.createTempFile("decrypt-in", ".txt");
            
            FileWriter w = new FileWriter(in);
            w.write(armored);
            w.close();
            
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(in));
            InputStream is = PGPUtil.getDecoderStream(new FileInputStream(skfile));
            PGPFileUtils.decryptFile(bis, is, passphrase.toCharArray(), out);
            
            BufferedReader br = new BufferedReader(new FileReader(out));
            String s = "";
            while ( (s = br.readLine()) != null )
            {
                decrypted += s + "\n";
            }
            
            br.close();
            bis.close();
            is.close();
            
            if ( decrypted.trim().length() == 0 )
            {
                decrypted = null;
            }
        }
        catch (IOException e)
        {
            throw new GeneralSecurityException("Unable to decrypt PGP message", e);
        }
        finally
        {
            if ( in != null )
            {
                in.deleteOnExit();
                out.deleteOnExit();
            }
        }

        return decrypted;
    }
    
    /*
     * Converts a byte to hex digit and writes to the supplied buffer
     */
    private static void byte2hex(byte b, StringBuffer buf)
    {
        char[] hexChars = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
                            '9', 'A', 'B', 'C', 'D', 'E', 'F' };
        int high = ((b & 0xf0) >> 4);
        int low = (b & 0x0f);
        buf.append(hexChars[high]);
        buf.append(hexChars[low]);
    }

    /**
     * Convenience method to convert the given date string into a Date object using
     * the date format: <tt>yyyy-MM-dd hh:mm:ss:SS</tt>
     * @param installTime The date string to convert.
     * @return The Date object representation of the date string.
     * @throws ParseException thrown if the date string does not match the date format.
     */
    public static Date convertFromInstalledTime(String installTime)
        throws ParseException
    {
        return dateFormat.parse(installTime);
    }

    /**
     * Convenience method to convert the given Date object into a date string using
     * the date format: <tt>yyyy-MM-dd hh:mm:ss:SS</tt>
     * @param installDate The Date object to convert.
     * @return The date string representation of the Date object.
     */
    public static String convertToInstalledTime(Date installDate)
    {
        return dateFormat.format(installDate);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        try
        {
            //PGPSecretKeyRing skr = createPGPSecretKeyRing("crusnak <crusnak@blackboard.com>", "");
            //PGPFileUtils.exportSecretKeyRingToFile(skr, "bcpgp", true);
            
            //PGPFileUtils.
            
            /*for ( Provider p: Security.getProviders() )
            {
                for ( Provider.Service s: p.getServices() )
                {
                    System.out.println(s.getProvider() + "." + s.getAlgorithm());
                }
            }*/
            //PGPPublicKeyRing pkr = 
            
            /*PGPSecretKeyRing skr = createPGPSecretKeyRing("crusnak <crusnak@blackboard.com", "");
            FileOutputStream f1 = new FileOutputStream("skr.asc");
            FileOutputStream f2 = new FileOutputStream("pgp.pk");
            ArmoredOutputStream aout = new ArmoredOutputStream(f1);
            skr.encode(aout);
            aout.close();
            //skr.getPublicKey().encode(f2);*/
            
            /*PGPSecretKeyRing skr = new PGPSecretKeyRing(PGPUtil.getDecoderStream(new FileInputStream("skr.asc")));
            ArmoredOutputStream aout = new ArmoredOutputStream(new FileOutputStream("pkr.asc"));
            skr.getPublicKey().encode(aout);
            aout.close();
            Iterator it = skr.getExtraPublicKeys();
            while ( it.hasNext() )
            {
                System.out.println("Outputing extra keys");
                ArmoredOutputStream a = new ArmoredOutputStream(new FileOutputStream("pkr.asc", true));
                ((PGPPublicKey)it.next()).encode(a);
                a.close();
            }*/
            
            //PGPPublicKeyRing pkr = new PGPPublicKeyRing(PGPUtil.getDecoderStream(new FileInputStream("pkr.asc")));
            
            //PGPFileUtils.encryptFile("decrypt.txt.asc", "decrypt.txt", "pkr.asc", true, true);
            //System.out.println(PGPFileUtils.decryptFile("decrypt.txt.asc", "skr.asc", "".toCharArray(), "decrypt.out"));
            /*String enc = encryptPGPMessageUsingPKRFile("This is a test", new File("pkr.asc"));
            System.out.println(enc);
            String dec = decryptPGPMessage(enc, "", new File("skr.asc"));
            System.out.println(dec);
            
            System.exit(1);
            
            
            File skfile = new File("pgp.skr");
            FileInputStream fis = new FileInputStream(skfile);
            PGPSecretKeyRing skr2 = new PGPSecretKeyRing(fis);
            System.out.println(skr2.getPublicKey().isEncryptionKey());
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ArmoredOutputStream aos = new ArmoredOutputStream(baos);
            skr2.getPublicKey().encode(aos);
            aos.endClearText();
            System.out.println(baos);
            
            System.exit(1);
            
            String text = "Please encrypt this message";
            //File input = File.createTempFile("decrypt", "txt");
            //File output = File.createTempFile("encrypt", "txt");
            File encrypt = new File("encrypt.bpg");
            File decrypt = new File("decrypt.txt");
            
            FileWriter fw = new FileWriter(encrypt);
            fw.write(text);
            fw.close();
            
            FileOutputStream fos = new FileOutputStream(encrypt);
            PGPEncryptedDataGenerator cPk = new PGPEncryptedDataGenerator(PGPEncryptedData.CAST5, true, new SecureRandom(), "BC");
            cPk.addMethod(skr2.getPublicKey());
            OutputStream out = cPk.open(fos, new byte[1 << 16]);
            PGPCompressedDataGenerator  comData = new PGPCompressedDataGenerator(PGPCompressedData.ZIP);
            PGPUtil.writeFileToLiteralData(comData.open(out), PGPLiteralData.BINARY, skfile, new byte[1 << 16]);
            comData.close();
            out.close();
            
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(encrypt));
            //PGPFileUtils.decryptFile(bis, fis, new char[0], "decrypt.out");
            
            System.exit(2);
            
            String encrypted = "c1b93e386b2ec56554bb367156c306bc";
            String key = "07adf0987adsjljkg";
            String hexkey = RadixConversion.bytesToHexString(key.getBytes(), false);
            String decrypted = "0007934851";
            
            Cipher c = getDES3Cipher(Cipher.DECRYPT_MODE, hexkey);
            //Cipher c = Cipher.getInstance(DES3_CIPHER_TRANSFORMATION);*/

            // Login algorithm to the Transact Users table:
            // 1. Extract PASSWORD_HASH_SALT from Users table
            // 2. Concatenate with cleartext password,
            // 3. SHA-256 Hash this string
            // 4. Compare against PASSWORD_HASH in Users table
            String pass = "Changeme#2";
            String old = "DED3646FB4CD55EAC22490FA78698E4646B04189E9D4EF6D3E39FA34836B2E77";
            String salt = "uhouemfrkdjarqzhexkw";
            String hash = RadixConversion.bytesToHexString(SHA256Hash(RadixConversion.charsToBytes((salt + pass).toCharArray())), false);
            System.out.println(hash);

            String key = "kd94hf93k423kf44&pfkkdhi9sl3r4s00";
            String udata = "GET&http%3A%2F%2Fphotos.example.net%2Fphotos&file%3Dvacation.jpg%26oauth_consumer_key%3Ddpf43f3p2l4k3l03%26oauth_nonce%3Dkllo9940pd9333jh%26oauth_signature_method%3DHMAC-SHA1%26oauth_timestamp%3D1191242096%26oauth_token%3Dnnch734d00sl2jdk%26oauth_version%3D1.0%26size%3Doriginal";
            hash = HMACSHA1Hash(key, udata);
            System.out.println(hash);
            
            //String unencrypted = "MIIEowIBAAKCAQEAge5W8Iv86SagFEGoNCiODTxv1wd/X9pRu+vH8vo6xcYUBC7k5vUdldJdwdq7gxG6IZ9nlRRs08qYPElHAtsm8rT2IpKA37BWOeRU2CjQCgipPkNHt+pbFBWLkChnZf4xee5lB/eNDp7R2NAIntwA7xSkFI0OH3fJaBg3cgW8xrFGE5AXXxlKlTfBWrzAdq5RZcHl4fwBuNGPQ50HibnfhevAYWgMRNm5+9Z8AePK7+cl2kFzwLUYBH6iFu9XrmGeKPTpLQ9M0LV6Kc9XRY7PhEjU6Der22V2JijPdaelhCQxRfWW122HIrltJm/DNHyxxkrMJEdolZ5tvHksaDgNvQIDAQABAoIBAFIArJown7Q+jnV6YsBYgB7Wb/eUYc2LKl6iFpXBW+qKcwOY3RruxHdUR5wbgpDcVU73Hha2x2B70szpqomg6tnvYBC9ydxjnGf5NZsfVv2eLcKAD/mBTW5Fksmu1mNGpM9BqD34/OunTOW7llU8oNJDgrL9u1Yc8d5iukuaV2RGdsyJZMpDGDaUnWq4BhTp/NyKa/fFXyuNHjVv9+yyouKt5FnO6kG7MLk7H97WWVISxICmCF+nny7eatlpyK1khZ4fS9P3Sj0aBFXULQwsKPJ0jfk0hnqkc3cqrd/+1JNitDVfDyfryeRryoQoQKJuiGpM3uyWkKR4CwZ5deRe1UECgYEA/nXlEKVXypiSqWd+WQActWm7CfT0cttiLIDpqZybY6imn6qJkAWBxXtdKyMPC/p3wcrJPoKRiBoevwXP4mkRo+X5o7zLL4lib05azKheJZbFjCOdUtFbjc6M/FpuZafVrebuifkTKX09y49oMJwTBEMxzCzEE7IJJILusM1HVlUCgYEAgreTOf/hftZzumKXiW+17ePLSmDPURDUMqd6rWE53iETH+YyWW9gKMLl3l6Tb5j0aCL2YDXh6eqpZ5rK8w3Mu/5mH3QViGMv1DtZMSCqy64gJGBrqHPYLrQ8d21J7qGmPgBl+lHKBWk9Gf797+/+9ReFOuEpHADapV4BSh3zMckCgYEAnFFIesVX/TllP3Q4hewvtrJRGHjGgpiK4tP+txdhzgMY+RUGlNcNNYfnYEyVPcKMNxygLCqBxJ5nLOohHvF0yL7KcO/V0koPklkMJpV34JeSm9h1GR9Lov20VoInN4O3HLG08EV2pBIwGBDs7I+0jvXIbalCeubv/uIAR9STclECgYAikEnkry0/7ZKiOQmatKglY/NQEnMfG1UQo8pDaA72FkbJS4QKs/F4Nbi0aMuRWZ9encgJvpWi1YK/u2VoiUCWgPkd3gMHkjqGYr79x4aB3uPu2nP9Ecya5NuMNKC08ZdUk6RjYRcLAT2/zvX0aMBArFx1Ki5FklA+I7Ixl+PDSQKBgAxPJs62ZXPxBIdztE5FruSHPkUxZFx4rRlAVj0Qz9eTXdFm1G1TT5EvsRNKUlrCunedcXROTuNWI7HBxZpT3zSX4a2nw+qTWqxuVSCAFR5y+9YF/idhhqMBdccMOqZwJJPo2h7PqPQvFoGF1HG7iWV5qkkW60rYUO7IPks90NOR";
            //String encrypted = "Yhp5MLdggJ27Exm1DHAWymJnB/HgXSGTFBXDWHzZdRS55INKXzWxw3fBu9KInwECD5xj2pqnZGDGlMkPNhzo2Nst1x6vomh0fnWhcy/9HXegplTEd8VWlUMQSpMqgzH/ATSLt82xxWxtoqpBKFNP6PiK7Ya7tUp54sE1PvU/g7JO9Wpc8ZYlNAWNytRA4zlCBXFyRP25awvvmBtti6Ne69BukX/HCcBGhFD/X1/Hr0Yf093pBGcE7HtlQEzAkShmZYnuTBYSAy6t7VDz/e5FVQOMP035Gh4jO3RswNkfLovW0IF0YwK8caM06jDt9N7Pk4qZbTMfaY+sdAK3zkiaCFRFW9UWUu8/jMwUgJQWWzjGhj0AmWFGMgjEkvdrpxK62MSpyNBXDuian+Ru/oWc1L9ny6SbEUJgvFQENvxSFPMCG9rWq6X9MbcoTsBeXMg2gaF1dArrJk4jSO1ZjOthV/6xP9oSFP0UuSrm95t1LBOVWrnvu/IuRI6BwpphxWTrZZRxyG8FwRv4ey1NDKi/XACdr0JGkC12ViQgDe44hF0LLAr4ndvMtwnJanM6K3t3VZsstElHgqUl5eG7fOdHinmUgRkNpU7k6A92MrK7ScwtYCWyf33yLEZgMJ7R6g9T8ov6CvgoIpQAM6p5HUPVzSYtrPWsxna/uy2WLaxgUNG5eJYEIfJZUBB1EfaOxZ3OO9BpmDWBLDJhx1uu8xPEYx7o9AheaK+wyrm6SjmOSvVekfHRcjmu7iB0YLDWTDcjpYUCF3i3D9ofCzE/7wjEAyBOriDgLKZav6dj43Luk90o1aJTtTY+KUcGuZ7IRfLmSS4KO0+jzsNe/va2LSCW9BAAUluPkf1PpuUha2QZEdyvtSXAy9xMVArLz/YpVO65YmcYkhfNqge+XM46tDCGL9QEg9R2tQbnFwcoEWzvyK1bMvFzpIJ6f1O4f7LCowiDcZkWyiGI71uhWC2nThQ99bermicjl2rP+TIknhBYZBXkZFBLmPQfWMz3aw96WINkZuOX4lCreSShWUGXQMVNtki6zZhEWhYa4W2etji59Gb6CJ65XgYwwbCwDxRHIVP9DfXX60yaIzZCHY/zzPz13BoDEGL6TBvVTqVTJj4h+W4GAZDYN1qeWYeO3mAhuG8YnqYaq3tgKkBsoGFjTJHpMwW/dMB0OQwzzpaskJluke/rSosuBOr5Vc/GaHYC3lsuXUo/9Xh2eaUqrdqavmXiZ+iSU8ecY0OKMGyjbhEUk4MNOqUy7eWcxvUo3Q9c4jRPWgHxqfNOSsXot41AktStqPFGVKeolq9HHH/rBI9MnM1N/pYU/c6TPrFhB+OVuVLTGbmGJsRZnxmPusVvTqp3T6l9S4WiVnVo46TylsbmwCr2ID/O4B+Ju/Jiu5g1g8Ja8bIol1ij4sHchOEY9dnNznCkrngZ5ugqOr4O47oVlqOs575jRVoNEYC/qmwOFrkvoKyzKH6MOOb71lJEZZ/i/gpwp3DG6JktJaDYBHhjlOiifB0ga8bGUlCpmR9jRtqnuwWSrSyHO9dvTzXcYgVD3/QGSNqiEh1vBG1oNMycNvRP+5A+OJ3I9g==";            
            /*Cipher c = null;
            //byte[] keybytes = RadixConversion.convertHexStringToBytes(key);;
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), BLOWFISH_CIPHER_ALGORITHM);
            c = Cipher.getInstance(BLOWFISH_CIPHER_TRANSFORMATION);
            IvParameterSpec iv = new IvParameterSpec(new byte[8]);
            c.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
            byte[] d = c.doFinal(decrypted.getBytes());
            System.out.println(new String(d));
            System.out.println(RadixConversion.bytesToHexString(d, false));*/

            //Cipher c = getBlowfishCipher(Cipher.DECRYPT_MODE, hexkey);
            //byte[] d = c.doFinal(encrypted.getBytes());
            //System.out.println(new String(d));
            //System.out.println(RadixConversion.bytesToHexString(d, false));
            /*String key = "ce69d84c5de5c50b8f3bd20303f306bf0e3c27e3a2f37b872bd6138c6dcce98d";
            String data = "This is a test";

            String encrypted = encryptAESString(data, key);
            String decrypted = decryptAESString(encrypted, key);
            System.out.println(encrypted);
            System.out.println(decrypted);
            System.out.println(data.equals(decrypted));
            System.exit(0);

            //String key = RadixConversion.bytesToHexString(new SecureRandom().generateSeed(16), false);
            System.out.println(key);
            byte[] hash = BbKeyGenerator.MD5Hash(RadixConversion.convertHexStringToBytes(key));
            System.out.println(RadixConversion.bytesToHexString(hash, false));
            System.out.println(RadixConversion.compareBytes(hash, BbKeyGenerator.MD5Hash(RadixConversion.convertHexStringToBytes(key))));
            System.exit(0);

            KeyGenerator kg = KeyGenerator.getInstance("AES");
            System.out.println(kg);
            kg.init(128, new SecureRandom());*/

            /*
            String key = "4c7d415a764b2d7d5c51724408491a11";
            byte[] encryption_key = RadixConversion.convertHexStringToBytes(key);
             
            SecretKeySpec skeySpec = new SecretKeySpec(encryption_key, "AES");

            // Instantiate the cipher
            Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
            byte[] iv_spec = {
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00
            };

            IvParameterSpec iv = new IvParameterSpec(iv_spec);
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
            */

            /*Cipher cipher = getTIACipher(Cipher.ENCRYPT_MODE, key);
            byte[] input = ("2~1~000004~2~20040219144203~T~125~USD~F~566010004852620~0~0~").getBytes();
            System.out.println(input.length);
            int mult = input.length / 16;
            if ( input.length % 16 > 0 )
            {
                mult++;
            }

            byte[] cont = new byte[16*mult];
            System.arraycopy(input, 0, cont, 0, input.length);
            System.out.println(cont.length);
            for ( byte b: cont )
            {
                System.out.print(RadixConversion.byteToHexString(b) + " ");
            }

            System.out.println();

            for ( byte b: input )
            {
                System.out.print(RadixConversion.byteToHexString(b) + " ");
            }

            byte[] encrypted = cipher.doFinal(cont);
            System.out.println();
            for ( byte b: encrypted )
            {
                System.out.print(RadixConversion.byteToHexString(b) + " ");
            }

            System.out.println();

System.exit(1);
            byte[] ec = AESEncryptionUtility.encrypt("2~1~000004~2~20040219144203~T~125~USD~F~566010004852620~0~0~", key);
            for ( byte b: ec )
                System.out.print(b + " ");


            Cipher c = Cipher.getInstance("AES/CBC/NoPadding");
            System.out.println(c);
            //Key k = c.unwrap(key.getBytes(), Cipher.SECRET_KEY, Cipher.UNWRAP_MODE);

            /*KeyStore ks = loadKeyStore();
            ConnectionManagerConfiguration config = ConnectionManagerConfiguration.getInstance();
            Date installDate = convertFromInstalledTime(config.getInstallationTime());
            Enumeration aliases = ks.aliases();
            while ( aliases.hasMoreElements() )
            {
                String alias = (String)aliases.nextElement();
                System.out.println(alias + " Certificate=" + ks.getCertificate(alias));
                System.out.println(alias + " PrivateKey=" + ks.getKey(alias, getRSAPrivateKeyPassword(installDate).toCharArray()));
            }*/

            /*EncryptedMap map = EncryptedMap.getInstance();
            DBConnection conn = new DBConnection();
            conn.setJdbcDriver("oracle.jdbc.driver.OracleDriver");
            conn.setDatabaseHost("10.20.122.71");
            conn.setDatabasePort("1521");
            conn.setDatabaseSid("bbts");
            map.put("Deblobber", "BBTS", conn);
            String test = "This is only a test";

            // TODO code application logic here
            Provider[] providers = Security.getProviders();
            for ( int i=0; i<providers.length; i++ )
            {
                System.out.println(providers[i].getName());
            }

            KeyGenerator kg = KeyGenerator.getInstance(CIPHER_ALGORITH, SECURITY_PROVIDER);
            System.out.println(kg.getProvider().getName());
            kg.init(new SecureRandom());
            SecretKey skey = kg.generateKey();
            System.out.println(kg);
            System.out.println(skey.getEncoded());

            Cipher c = Cipher.getInstance(CIPHER_ALGORITH, SECURITY_PROVIDER);
            c.init(Cipher.ENCRYPT_MODE, skey);
            byte[] encrypted = c.doFinal(test.getBytes());
            c.init(Cipher.DECRYPT_MODE, skey);
            System.out.println(c);
            System.out.println("Encrypting: " + test);
            System.out.println(new String(encrypted));
            byte[] decrypted = c.doFinal(encrypted);
            String output = new String(decrypted);
            System.out.println("Decrypted: " + output);

            KeyPairGenerator kpg = KeyPairGenerator.getInstance(KEYAGREEMENT_ALGORITH, SECURITY_PROVIDER);
            kpg.initialize( 1024, new SecureRandom());
            System.out.println(kpg);
            KeyPair kpserver = kpg.generateKeyPair();
            kpg.initialize( 1024, new SecureRandom());
            KeyPair kpclient = kpg.generateKeyPair();
            System.out.println("ServerPublic: " + kpserver.getPublic().getEncoded());
            System.out.println("ServerPrivate: " + kpserver.getPrivate().getEncoded());
            System.out.println("ClientPublic: " + kpclient.getPublic().getEncoded());
            System.out.println("ClientPrivate: " + kpclient.getPrivate().getEncoded());

            KeyAgreement ka = KeyAgreement.getInstance(KEYAGREEMENT_ALGORITH, SECURITY_PROVIDER);
            ka.init(kpserver.getPrivate(), new SecureRandom());
            System.out.println(ka);
            ka.doPhase(kpclient.getPublic(), true);
            SecretKey kas = ka.generateSecret(CIPHER_ALGORITH);
            System.out.println(kas.getEncoded());

            /*CertAndKeyGen cakg = new CertAndKeyGen("RSA", "MD5WithRSA");
            X500Name name = new X500Name("ConnectionManager", "Blackboard", "Consulting Services",
                                         "Phoenix", "AZ", "US");
            System.out.println(cakg);
            cakg.generate(2056);
            PublicKey pubkey = cakg.getPublicKey();
            PrivateKey privkey = cakg.getPrivateKey();
            Certificate cert = cakg.getSelfCertificate(name, new java.util.Date(), 3000L);
            System.out.println(pubkey);
            System.out.println(privkey);
            System.out.println(cert);


            KeyStore ks = KeyStore.getInstance("JCEKS", SECURITY_PROVIDER);
            ks.load(null);
            Certificate[] chain = { cert };
            ks.setKeyEntry( "CMKeyPair", privkey, "123456".toCharArray(), chain );
            ks.store(new FileOutputStream("bbkeystore.jceks"), "password".toCharArray());*/

            /*KeyStore ks = KeyStore.getInstance("JCEKS", SECURITY_PROVIDER);
            kpg = KeyPairGenerator.getInstance("RSA");
            kpg.initialize( 1024, new SecureRandom());
            System.out.println(kpg.getProvider().getName());
            KeyPair rsa = kpg.generateKeyPair();
            System.out.println(rsa);*/

            /*KeyStore ks = KeyStore.getInstance("JCEKS", SECURITY_PROVIDER);
            ks.load(new FileInputStream("bbkeystore.jceks"), "password".toCharArray());
            System.out.println("Keystore: " + ks);
            Key key = ks.getKey("CMKeyPair", "123456".toCharArray());
            PrivateKey privkey = null;
            PublicKey pubkey = null;
            if ( key instanceof PrivateKey )
            {
                 Certificate cert = ks.getCertificate("CMKeyPair");
                 pubkey = cert.getPublicKey();
                 privkey = (PrivateKey)key;
            }
            //X509Certificate cert = X509Certificate.getInstance(kpserver.getPrivate().getEncoded());
            //X509Certificate[] chain = { cert };
            //ks.setKeyEntry("CMPublicKey", kpserver.getPublic().getEncoded(), null);
            //ks.setKeyEntry( "CMPrivateKey", kpserver.getPrivate().getEncoded(), chain);
            //ks.set


            //rsa.getPrivate();
            Cipher rc = Cipher.getInstance("RSA");
            //AlgorithmParameterSpec spec =
            rc.init(Cipher.ENCRYPT_MODE, pubkey);
            encrypted = rc.doFinal(test.getBytes());
            System.out.println("Encrypted: " + new String(encrypted));
            rc.init(Cipher.DECRYPT_MODE, privkey);
            decrypted = rc.doFinal(encrypted);
            System.out.println("Decrypted: " + new String(decrypted));


            // Create stream
            /*rc.init(Cipher.ENCRYPT_MODE, pubkey);
            FileOutputStream fos = new FileOutputStream("secret.key");
            BufferedOutputStream bos = new BufferedOutputStream(fos);
            CipherOutputStream cos = new CipherOutputStream(bos, rc);
            ObjectOutputStream oos = new ObjectOutputStream(cos);

            // Write objects
            oos.writeObject(skey);
            oos.flush();
            oos.close();*/

            //Create stream
            /*rc.init(Cipher.DECRYPT_MODE, privkey);
            FileInputStream fis = new FileInputStream("secret.key");
            BufferedInputStream bis = new BufferedInputStream(fis);
            CipherInputStream cis = new CipherInputStream(bis, rc);
            ObjectInputStream ois = new ObjectInputStream(cis);

            // Read objects
            //EncryptedMap map2 = (EncryptedMap)ois.readObject();
            SecretKey k = (SecretKey)ois.readObject();
            ois.close();

            System.out.println(k);
            System.out.println(new String(k.getEncoded()));

            
            
            /*FileOutputStream fos = new FileOutputStream("pub.key");
            fos.write(rsa.getPublic().getEncoded());
            fos.close();
            fos = new FileOutputStream("priv.key");
            fos.write(rsa.getPrivate().getEncoded());
            fos.close();

            FileInputStream fis = new FileInputStream("pub.key");
            byte[] encpub = new byte[fis.available()];
            fis.read(encpub);
            fis = new FileInputStream("priv.key");
            byte[] encpriv = new byte[fis.available()];
            fis.read(encpriv);*/


            //X509EncodedKeySpec kspub = new X509EncodedKeySpec(encpub);
            //PKCS8EncodedKeySpec kspriv = new PKCS8EncodedKeySpec(encpriv);

/*
            X509EncodedKeySpec kspub = new X509EncodedKeySpec(pubkey.getEncoded());
            PKCS8EncodedKeySpec kspriv = new PKCS8EncodedKeySpec(privkey.getEncoded());
            KeyFactory kf = KeyFactory.getInstance("RSA");
            PublicKey puk = kf.generatePublic(kspub);
            PrivateKey prk = kf.generatePrivate(kspriv);

            rc.init(Cipher.ENCRYPT_MODE, puk);
            encrypted = rc.doFinal(test.getBytes());
            System.out.println("Encrypted: " + new String(encrypted));
            rc.init(Cipher.DECRYPT_MODE, prk);
            decrypted = rc.doFinal(encrypted);
            System.out.println("Decrypted: " + new String(decrypted));
*/
            //System.out.println(map);

            //Cipher crypt = Cipher.getInstance(CIPHER_ALGORITH, SECURITY_PROVIDER);
            /*crypt.init( Cipher.ENCRYPT_MODE, k);
            FileOutputStream fos = new FileOutputStream("encrypt.map");
            BufferedOutputStream bos = new BufferedOutputStream(fos);
            CipherOutputStream cos = new CipherOutputStream(bos, crypt);
            ObjectOutputStream oos = new ObjectOutputStream(cos);

            // Write objects
            oos.writeObject(map);
            oos.flush();
            oos.close();*/

            /*crypt.init( Cipher.DECRYPT_MODE, k);
            fis = new FileInputStream("encrypt.map");
            bis = new BufferedInputStream(fis);
            cis = new CipherInputStream(bis, crypt);
            ois = new ObjectInputStream(cis);

            // Read objects
            //EncryptedMap map2 = (EncryptedMap)ois.readObject();
            EncryptedMap m2 = (EncryptedMap)ois.readObject();
            ois.close();
            System.out.println("Decrypted: " + m2);*/
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

}
