/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.blackboard.services.security;

import com.blackboard.services.security.exception.BbTSSecurityException;
import com.blackboard.services.security.object.EncryptedMap;
import com.blackboard.services.utils.BaseLogger;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InvalidClassException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.text.ParseException;
import java.util.Date;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.SecretKey;
import javax.servlet.ServletContext;

/**
 * This class is used to manage access to the encrypted secured object map.  This map is
 * what all secured objects are stored to.  It is responsible for reading and writing both
 * the SecretKey used to encrypt the map as well as the map itself.
 * @author crusnak
 * @see SecuredObjectManager
 * @see EncryptedMap
 */
public class EncryptionManager
{
    public static final String SECRETKEY_CIPHER_ALGORITH =      "AES";
    public static final String KEYAGREEMENT_ALGORITH =          "DiffieHellman";
    public static final String SECURITY_PROVIDER =              "SunJCE";
    public static final String KEYPAIR_KS_ALIAS =               "CMRSAKeyPair";

    private static EncryptionManager instance = null;

    private KeyStore ks = null;
    private SecretKey secretKey = null;
    private PublicKey publicKey = null;
    private PrivateKey privateKey = null;
    private EncryptedMap map = null;

    /**
     * Accessor to the EncryptionManager singleton.
     * @return The EncrytionManager singleton
     * @throws BbTSSecurityException thrown if the SecurityConfiguration properties cannot
     * be found or the SecurityManager wasn't initialized properly.
     * @see SecurityManager#initialize(javax.servlet.ServletContext, java.io.File)
     */
    public static EncryptionManager getInstance()
        throws BbTSSecurityException
    {
        if ( instance == null )
        {
            instance = new EncryptionManager();
        }
        return instance;
    }

    /*public static EncryptionManager initialize(File appRoot)
        throws IOException, GeneralSecurityException, ParseException
    {
        return initialize(null, appRoot);
    }*/

    /*public static EncryptionManager initialize(ServletContext ctx, File root)
        throws IOException, GeneralSecurityException, ParseException
    {
        if ( instance == null )
        {
            instance = new EncryptionManager();
        }
        instance.init(ctx, root);
        return instance;
    }*/

    private EncryptionManager() 
        throws BbTSSecurityException
    {
        init();
    }

    /**
     * This method is used to initialize the EncryptionManager.  The SecurityConfiguration is loaded,
     * the private key to decrypt the secret key is extracted from the keystore, and the encrypted map
     * is decrypted from the secret key.
     * @throws BbTSSecurityException thrown if there is an error initializing the EncryptionManager
     */
    private void init()
        throws BbTSSecurityException
    {
        try
        {
            SecurityManager secman = SecurityManager.getInstance();
            SecurityConfiguration config = secman.getConfig();

            if ( config.getPropertyURL() == null )
            {
                throw new IOException("Unable to find " + SecurityConfiguration.propertyFile + " file in the classpath. " +
                        "Ensure that this file exists on the classpath or run the ConnectionManagerInstaller.");
            }

            if ( config.getInstallationTime() != null )
            {
                Date installDate = SecurityManagerInstaller.format.parse(config.getInstallationTime());
                ks = BbKeyGenerator.loadKeyStore(config.getKeystorePath(), config.getInstallationTime());
                if ( !ks.containsAlias(KEYPAIR_KS_ALIAS) )
                {
                    throw new GeneralSecurityException("Missing required keystore alias. Reinstall " +
                            "ConnectionManager with a new keystore to correct.");
                }

                Certificate cert = ks.getCertificate(KEYPAIR_KS_ALIAS);
                publicKey = cert.getPublicKey();
                privateKey = (PrivateKey)ks.getKey(KEYPAIR_KS_ALIAS, BbKeyGenerator.getRSAPrivateKeyPassword(installDate).toCharArray());
                secretKey = loadSecretKey(privateKey);
                map = loadEncryptedMap(secretKey);
            }
            else
            {
                BaseLogger.warn("SecurityManager not installed, EncryptionManager will not work until proper installation.");
            }
        }
        catch (IOException e) { throw new BbTSSecurityException("Unable to initialize EncryptionManager", e); }
        catch (GeneralSecurityException e) { throw new BbTSSecurityException("Unable to initialize EncryptionManager", e); }
        catch (ParseException e) { throw new BbTSSecurityException("Unable to initialize EncryptionManager", e); }
    }

    /**
     * Reinitilizes the EncryptionManager
     * @throws BbTSSecurityException thrown if there is an error initializing the EncryptionManager
     */
    public void refresh()
        throws BbTSSecurityException
    {
        init();
    }

    /**
     * Accessor to the PublicKey used to encrypt the SecretKey.
     * @return the RSA PublicKey.
     */
    public PublicKey getPublicKey()
    {
        return publicKey;
    }

    /**
     * Accessor to the PrivateKey used to decrypt the SecretKey.
     * @return the RSA PrivateKey
     */
    public PrivateKey getPrivateKey()
    {
        return privateKey;
    }

    /**
     * Accessor to the SecretKey used to encrypt/decrypt the EncryptedMap.
     * @return The AES SecretKey.
     */
    public SecretKey getSecretKey()
    {
        return secretKey;
    }

    /**
     * Accessor to the EncryptedMap used to store all secured objects.
     * @return The EncryptedMap.
     */
    public EncryptedMap getEncryptedMap()
    {
        return map;
    }

    /**
     * This method stores the secret key based upon the path info stored in the
     * SecurityConfiguration object.
     * @param pkey The RSA PublicKey used to encrypt the SecretKey.
     * @param skey The SecretKey to encrypt and store in a file.
     * @throws BbTSSecurityException thrown if the SecurityManager wasn't initialized properly.
     * @throws IOException thrown if there is an error writing the object output stream.
     * @throws GeneralSecurityException thrown if there are any cryptography related errors.
     * @see SecurityConfiguration#getSecretKeyPath()
     */
    public static void storeSecretKey(PublicKey pkey, SecretKey skey)
        throws BbTSSecurityException, IOException, GeneralSecurityException
    {
        // Create stream
        Cipher cipher = BbKeyGenerator.getRSACipher(Cipher.ENCRYPT_MODE, pkey);
        SecurityManager secman = SecurityManager.getInstance();
        SecurityConfiguration config = secman.getConfig();
        File keyFile = secman.findResourceAsFile(config.getSecretKeyPath());
        FileOutputStream fos = new FileOutputStream(keyFile);
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        CipherOutputStream cos = new CipherOutputStream(bos, cipher);
        ObjectOutputStream oos = new ObjectOutputStream(cos);

        // Write objects
        oos.writeObject(skey);
        oos.flush();
        oos.close();
    }

    /**
     * This method loads the secret key based upon the path info stored in the
     * SecurityConfiguration object.
     * @param pkey The RSA PrivateKey used to decrypt the SecretKey. The PrivateKey must
     * be a KeyPair of the PublicKey used to encrypt the SecretKey.
     * @throws BbTSSecurityException thrown if the SecurityManager wasn't initialized properly.
     * @throws IOException thrown if there is an error reading the object input stream.
     * @throws GeneralSecurityException thrown if there are any cryptography related errors.
     * @see SecurityConfiguration#getSecretKeyPath()
     */
    public static SecretKey loadSecretKey(PrivateKey pkey)
        throws BbTSSecurityException, IOException, GeneralSecurityException
    {
        SecretKey skey = null;
        SecurityManager secman = SecurityManager.getInstance();
        SecurityConfiguration config = secman.getConfig();

        // Create stream
        Cipher cipher = BbKeyGenerator.getRSACipher(Cipher.DECRYPT_MODE, pkey);
        URL skURL = secman.findResourceAsURL(config.getSecretKeyPath());
        if ( skURL == null )
        {
            throw new IOException("Could not locate secret key file on the classpath: " + config.getSecretKeyPath());
        }

        BufferedInputStream bis = new BufferedInputStream(skURL.openStream());
        CipherInputStream cis = new CipherInputStream(bis, cipher);
        ObjectInputStream ois = new ObjectInputStream(cis);

        // Read objects
        try
        {
            skey = (SecretKey)ois.readObject();
        }
        catch (ClassNotFoundException e) {throw new GeneralSecurityException(e);}
        ois.close();
        return skey;
    }

    /**
     * This method stores the encrypted map based upon the path info stored in the
     * SecurityConfiguration object.
     * @param skey The AES SecretKey used to encrypt the EncryptedMap.
     * @param map The secured object map to encrypt and store in a file.
     * @throws BbTSSecurityException thrown if the SecurityManager wasn't initialized properly.
     * @throws IOException thrown if there is an error writing the object output stream.
     * @throws GeneralSecurityException thrown if there are any cryptography related errors.
     * @see SecurityConfiguration#getEncryptedMapPath() ()
     */
    public static void storeEncryptedMap(SecretKey skey, EncryptedMap map)
        throws BbTSSecurityException, IOException, GeneralSecurityException
    {
        // Create stream
        Cipher cipher = BbKeyGenerator.getSymmetricCipher(Cipher.ENCRYPT_MODE, skey);
        SecurityManager secman = SecurityManager.getInstance();
        SecurityConfiguration config = secman.getConfig();
        File mapFile = secman.findResourceAsFile(config.getEncryptedMapPath());
        FileOutputStream fos = new FileOutputStream(mapFile);
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        CipherOutputStream cos = new CipherOutputStream(bos, cipher);
        ObjectOutputStream oos = new ObjectOutputStream(cos);

        // Write objects
        oos.writeObject(map);
        oos.flush();
        oos.close();
    }

    /**
     * This method loads the encrypted map based upon the path info stored in the
     * SecurityConfiguration object.
     * @param skey The AES SecretKey used to encrypt the EncryptedMap.
     * @throws BbTSSecurityException thrown if the SecurityManager wasn't initialized properly.
     * @throws IOException thrown if there is an error reading the object input stream.
     * @throws GeneralSecurityException thrown if there are any cryptography related errors.
     * @see SecurityConfiguration#getEncryptedMapPath() ()
     */
    public static EncryptedMap loadEncryptedMap(SecretKey skey)
        throws BbTSSecurityException, IOException, GeneralSecurityException
    {
        EncryptedMap map = null;
        SecurityManager secman = SecurityManager.getInstance();
        SecurityConfiguration config = secman.getConfig();

        // Create stream
        Cipher cipher = BbKeyGenerator.getSymmetricCipher(Cipher.DECRYPT_MODE, skey);
        URL mapURL = secman.findResourceAsURL(config.getEncryptedMapPath());
        if ( mapURL == null )
        {
            throw new IOException("Could not locate encrypted map file on the classpath: " + config.getEncryptedMapPath());
        }

        BufferedInputStream bis = new BufferedInputStream(mapURL.openStream());
        CipherInputStream cis = new CipherInputStream(bis, cipher);
        ObjectInputStream ois = new ObjectInputStream(cis);

        // Read objects
        try
        {
            map = (EncryptedMap)ois.readObject();
        }
        catch (ClassNotFoundException e) {throw new GeneralSecurityException(e);}
        catch (InvalidClassException e) {throw new IOException("EncryptedMap version in classpath differs from what was stored.", e);}
        ois.close();
        return map;
    }

    public static void main(String[] args)
    {
        try
        {
            EncryptionManager eman = EncryptionManager.getInstance();
            System.out.println();
            System.out.println("PublicKey: ");
            System.out.println(new String(eman.getPublicKey().getEncoded()));
            System.out.println();
            System.out.println("PrivateKey: ");
            System.out.println(new String(eman.getPrivateKey().getEncoded()));
            System.out.println();
            System.out.println("SecretKey: ");
            System.out.println(new String(eman.getSecretKey().getEncoded()));
            System.out.println();
            System.out.println("EncryptedMap: ");
            System.out.println(eman.getEncryptedMap());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.exit(-1);
        }

    }
}
